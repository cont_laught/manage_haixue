<?php
/**
 * This file is part of easycrm, created by PhpStorm.
 * Author: LouXin
 * Date: 2015/1/4 13:16
 * File: AgentController.php
 */

namespace Icsoc\CustomReportBundle\Controller;

use Icsoc\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;

class AgentController extends BaseController
{
    /**
     * 坐席天报表
     *
     * @return JsonResponse
     */
    public function agentDayAction()
    {
        $param = array(
            'items' => array(
                'inboundConnNum',
                'inboundConnTotalSecs',
                'inboundConnAvgSecs',
                'outboundTotalNum',
                'outboundConnNum',
                'outboundConnTotalSecs',
                'outboundConnAvgSecs',
                'validConnTotalNum',
                'inboundValidConnTotalNum',
                'outboundValidConnTotalNum',
                'connTotalNum',
                'connTotalSecs',
                'connAvgSecs',
                'validConnTotalSecs',
                'validConnAvgSecs',
                'inboundValidConnTotalSecs',
                'inboundValidConnAvgSecs',
                'outboundValidConnTotalSecs',
                'outboundValidConnAvgSecs',
                'internalConnNum',
                'internalConnTotalSecs',
                'internalConnAvgSecs',
                'waitTotalNum',
                'waitTotalSecs',
                'waitAvgSecs',
                'ringTotalNum',
                'ringTotalSecs',
                'ringAvgSecs',
                'consultNum',
                'consultTotalSecs',
                'consultAvgSecs',
                'holdNum',
                'hlodTotalSecs',
                'holdAvgSecs',
                'conferenceNum',
                'conferenceTotalSecs',
                'conferenceAvgSecs',
                'transferTotalNum',
                'transferedConnNum',
                'transferedConnTotalSecs',
                'transferedConnAvgSecs',
                'loginTotalSecs',
                'loginTotalNum',
                'readyTotalSecs',
                'busyTotalNum',
                'busyTotalSecs',
                'busyAvgSecs',
                'busyRate',
                'workRate',
            ),
            'query' => array(
                'vcc_id' => 249,
                'start_time' => 1420041600,
                'end_time' => 1420473599
            ),
            'options' => array(
                'validSecs' => 50
            )
        );

        $result = $this->get('icsoc_custom_report.model.agent_statistic')->getAgentDayStatisticData($param);

        return new JsonResponse($result);
    }

    /**
     * 坐席月报表
     *
     * @return JsonResponse
     */
    public function agentMonthAction()
    {
        $param = array(
            'items' => array(
                'inboundConnNum',
                'inboundConnTotalSecs',
                'inboundConnAvgSecs',
                'outboundTotalNum',
                'outboundConnNum',
                'outboundConnTotalSecs',
                'outboundConnAvgSecs',
                'validConnTotalNum',
                'inboundValidConnTotalNum',
                'outboundValidConnTotalNum',
                'connTotalNum',
                'connTotalSecs',
                'connAvgSecs',
                'validConnTotalSecs',
                'validConnAvgSecs',
                'inboundValidConnTotalSecs',
                'inboundValidConnAvgSecs',
                'outboundValidConnTotalSecs',
                'outboundValidConnAvgSecs',
                'internalConnNum',
                'internalConnTotalSecs',
                'internalConnAvgSecs',
                'waitTotalNum',
                'waitTotalSecs',
                'waitAvgSecs',
                'ringTotalNum',
                'ringTotalSecs',
                'ringAvgSecs',
                'consultNum',
                'consultTotalSecs',
                'consultAvgSecs',
                'holdNum',
                'hlodTotalSecs',
                'holdAvgSecs',
                'conferenceNum',
                'conferenceTotalSecs',
                'conferenceAvgSecs',
                'transferTotalNum',
                'transferedConnNum',
                'transferedConnTotalSecs',
                'transferedConnAvgSecs',
                'loginTotalSecs',
                'readyTotalSecs',
                'busyTotalNum',
                'busyTotalSecs',
                'busyAvgSecs',
            ),
            'query' => array(
                'vcc_id' => 249,
                'start_time' => 1419350400,
                'end_time' => 1420473599
            ),
            'options' => array(
                'validSecs' => 50
            )
        );

        $result = $this->get('icsoc_custom_report.model.agent_statistic')->getAgentMonthStatisticData($param);

        return new JsonResponse($result);
    }
}
