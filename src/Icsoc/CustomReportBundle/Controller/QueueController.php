<?php
/**
 * This file is part of easycrm, created by PhpStorm.
 * Author: LouXin
 * Date: 2015/1/7 13:24
 * File: QueueController.php
 */

namespace Icsoc\CustomReportBundle\Controller;

use Icsoc\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;

class QueueController extends BaseController
{
    /**
     * 技能组天报表
     *
     * @return JsonResponse
     */
    public function queueDayAction()
    {
        $param = array(
            'items' => array(
                'inboundTotalNum',
                'inboundConnNum',
                'inboundConnTotalSecs',
                'inboundConnAvgSecs',
                'inboundConnRate',
                'outboundTotalNum',
                'outboundConnNum',
                'outboundConnTotalSecs',
                'outboundConnAvgSecs',
                'outboundConnRate',
                'inboundConnInXSecsNum',
                'inboundConnInXSecsRate',
                'validConnTotalNum',
                'inboundValidConnTotalNum',
                'outboundValidConnTotalNum',
                'validConnTotalSecs',
                'validConnAvgSecs',
                'inboundValidConnTotalSecs',
                'inboundValidConnAvgSecs',
                'outboundValidConnTotalSecs',
                'outboundValidConnAvgSecs',
                'inboundAbandonTotalNum',
                'inboundAbandonTotalSecs',
                'inboundAbandonAvgSecs',
                'inboundAbandonInXSecsNum',
                'inboundAbandonInXSecsRate',
                'queueTotalNum',
                'queueTotalSecs',
                'queueAvgSecs',
                'connQueueTotalNum',
                'connQueueTotalSecs',
                'connQueueAvgSecs',
                'ringTotalNum',
                'ringTotalSecs',
                'ringAvgSecs',
                'waitTotalNum',
                'waitTotalSecs',
                'waitAvgSecs',
            ),
            'query' => array(
                'vcc_id' => 249,
                'start_time' => 1420041600,
                'end_time' => 1420473599
            ),
            'options' => array(
                'validSecs' => 50,
                'inboundConnInXSecsNum' => array(15, 20, 25, 30, 45),
                'inboundAbandonInXSecsNum' => array(15, 20, 25, 30, 45),
            )
        );

        $result = $this->get('icsoc_custom_report.model.queue_statistic')->getQueueDayStatisticData($param);

        return new JsonResponse($result);
    }

    /**
     * 坐席月报表
     *
     * @return JsonResponse
     */
    public function queueMonthAction()
    {
        $param = array(
            'items' => array(
                'inboundTotalNum',
                'inboundConnNum',
                'inboundConnTotalSecs',
                'inboundConnAvgSecs',
                'inboundConnRate',
                'outboundTotalNum',
                'outboundConnNum',
                'outboundConnTotalSecs',
                'outboundConnAvgSecs',
                'outboundConnRate',
                'inboundConnInXSecsNum',
                'inboundConnInXSecsRate',
                'validConnTotalNum',
                'inboundValidConnTotalNum',
                'outboundValidConnTotalNum',
                'validConnTotalSecs',
                'validConnAvgSecs',
                'inboundValidConnTotalSecs',
                'inboundValidConnAvgSecs',
                'outboundValidConnTotalSecs',
                'outboundValidConnAvgSecs',
                'inboundAbandonTotalNum',
                'inboundAbandonTotalSecs',
                'inboundAbandonAvgSecs',
                'inboundAbandonInXSecsNum',
                'inboundAbandonInXSecsRate',
                'queueTotalNum',
                'queueTotalSecs',
                'queueAvgSecs',
                'connQueueTotalNum',
                'connQueueTotalSecs',
                'connQueueAvgSecs',
                'ringTotalNum',
                'ringTotalSecs',
                'ringAvgSecs',
                'waitTotalNum',
                'waitTotalSecs',
                'waitAvgSecs',
            ),
            'query' => array(
                'vcc_id' => 249,
                'start_time' => 1420041600,
                'end_time' => 1420473599
            ),
            'options' => array(
                'validSecs' => 50,
                'inboundConnInXSecsNum' => array(15, 20, 25, 30, 45),
                'inboundAbandonInXSecsNum' => array(15, 20, 25, 30, 45),
            )
        );

        $result = $this->get('icsoc_custom_report.model.queue_statistic')->getQueueMonthStatisticData($param);

        return new JsonResponse($result);
    }
}
