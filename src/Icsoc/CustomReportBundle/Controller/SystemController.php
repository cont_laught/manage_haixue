<?php
/**
 * This file is part of easycrm, created by PhpStorm.
 * Author: LouXin
 * Date: 2015/1/4 13:16
 * File: AgentController.php
 */

namespace Icsoc\CustomReportBundle\Controller;

use Icsoc\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;

class SystemController extends BaseController
{
    /**
     * 呼叫中心天报表
     *
     * @return JsonResponse
     */
    public function systemDayAction()
    {
        $param = array(
            'items' => array(
                'ivrInboundTotalNum',
                'inboundTotalNum',
                'inboundConnNum',
                'inboundRate',
                'inboundConnNum',
                'inboundConnRate',
                'inboundConnTotalSecs',
                'inboundConnAvgSecs',
                'outboundTotalNum',
                'outboundConnNum',
                'outboundConnTotalSecs',
                'outboundConnAvgSecs',
                'outboundConnRate',
                'validConnTotalNum',
                'inboundValidConnTotalNum',
                'outboundValidConnTotalNum',
                'validConnTotalSecs',
                'validConnAvgSecs',
                'inboundValidConnTotalSecs',
                'inboundValidConnAvgSecs',
                'outboundValidConnTotalSecs',
                'outboundValidConnAvgSecs',
                'inboundConnInXSecsNum',
                'inboundConnInXSecsRate',
                'inboundAbandonTotalNum',
                'inboundAbandonTotalSecs',
                'inboundAbandonAvgSecs',
                'inboundAbandonRate',
                'inboundAbandonInXSecsNum',
                'inboundAbandonInXSecsRate',
                'queueTotalNum',
                'queueTotalSecs',
                'queueAvgSecs',
                'connQueueTotalNum',
                'connQueueTotalSecs',
                'connQueueAvgSecs',
                'ringTotalNum',
                'ringTotalSecs',
                'ringAvgSecs',
                'loginTotalSecs',
                'readyTotalSecs',
                'busyTotalNum',
                'busyTotalSecs',
                'busyAvgSecs',
                'waitTotalNum',
                'waitTotalSecs',
                'waitAvgSecs',
                'workRate',
                'onlineAgentNum',
            ),
            'query' => array(
                'vcc_id' => 249,
                'start_time' => 1420041600,
                'end_time' => 1420473599
            ),
            'options' => array(
                'validSecs' => 50,
                'inboundConnInXSecsNum' => array(15, 20, 25, 30, 45),
                'inboundAbandonInXSecsNum' => array(15, 20, 25, 30, 45),
            )
        );

        $result = $this->get('icsoc_custom_report.model.system_statistic')->getSystemDayStatisticData($param);

        return new JsonResponse($result);
    }

    /**
     * 呼叫中心月报表
     *
     * @return JsonResponse
     */
    public function systemMonthAction()
    {
        $param = array(
            'items' => array(
                'inboundConnNum',
                'inboundConnTotalSecs',
                'inboundConnAvgSecs',
                'outboundTotalNum',
                'outboundConnNum',
                'outboundConnTotalSecs',
                'outboundConnAvgSecs',
                'validConnTotalNum',
                'inboundValidConnTotalNum',
                'outboundValidConnTotalNum',
                'connTotalNum',
                'connTotalSecs',
                'connAvgSecs',
                'validConnTotalSecs',
                'validConnAvgSecs',
                'inboundValidConnTotalSecs',
                'inboundValidConnAvgSecs',
                'outboundValidConnTotalSecs',
                'outboundValidConnAvgSecs',
                'internalConnNum',
                'internalConnTotalSecs',
                'internalConnAvgSecs',
                'waitTotalNum',
                'waitTotalSecs',
                'waitAvgSecs',
                'ringTotalNum',
                'ringTotalSecs',
                'ringAvgSecs',
                'consultNum',
                'consultTotalSecs',
                'consultAvgSecs',
                'holdNum',
                'hlodTotalSecs',
                'holdAvgSecs',
                'conferenceNum',
                'conferenceTotalSecs',
                'conferenceAvgSecs',
                'transferTotalNum',
                'transferedConnNum',
                'transferedConnTotalSecs',
                'transferedConnAvgSecs',
                'loginTotalSecs',
                'readyTotalSecs',
                'busyTotalNum',
                'busyTotalSecs',
                'busyAvgSecs',
                'workRate',
                'onlineAgentNum',
            ),
            'query' => array(
                'vcc_id' => 249,
                'start_time' => 1419350400,
                'end_time' => 1420473599
            ),
            'options' => array(
                'validSecs' => 50
            )
        );

        $result = $this->get('icsoc_custom_report.model.system_statistic')->getSystemMonthStatisticData($param);

        return new JsonResponse($result);
    }
}
