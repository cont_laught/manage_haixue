<?php
/**
 * This file is part of services, created by PhpStorm.
 * Author: LouXin
 * Date: 2014/11/6 15:36
 * File: ReportController.php
 */

namespace Icsoc\CustomReportBundle\Controller;

use Icsoc\CoreBundle\Export\DataType\ArrayType;
use Icsoc\CustomReportBundle\Entity\Report;
use Icsoc\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ReportController
 * @package Icsoc\CustomReportBundle\Controller
 */
class ReportController extends BaseController
{
    /**
     * 统计的所有指标
     * text为指标的名称，parent用于当选择该指标时，必须要选中parent对应的父指标，否则指标无法显示
     *
     * @var array
     */
    private $reportItems = array(
        'agent' => array(
            'inboundConnNum' => array('text' => 'inbound Conn Num', 'parent' => ''),
            'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs', 'parent' => 'inboundConnNum'),
            'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs', 'parent' => 'inboundConnNum'),
            'outboundTotalNum' => array('text' => 'outbound Total Num', 'parent' => ''),
            'outboundConnNum' => array('text' => 'outbound Conn Num', 'parent' => ''),
            'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs', 'parent' => 'outboundConnNum'),
            'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs', 'parent' => 'outboundConnNum'),
            'validConnTotalNum' => array('text' => 'valid Conn Total Num', 'parent' => ''),
            'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num', 'parent' => ''),
            'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num', 'parent' => ''),
            'connTotalNum' => array('text' => 'conn Total Num', 'parent' => ''),
            'connTotalSecs' => array('text' => 'conn Total Secs', 'parent' => 'connTotalNum'),
            'connAvgSecs' => array('text' => 'conn Avg Secs', 'parent' => 'connTotalNum'),
            'validConnTotalSecs' => array('text' => 'valid Conn Total Secs', 'parent' => 'validConnTotalNum'),
            'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs', 'parent' => 'validConnTotalNum'),
            'inboundValidConnTotalSecs' => array(
                'text' => 'inbound Valid Conn Total Secs', 'parent' => 'inboundValidConnTotalNum',
            ),
            'inboundValidConnAvgSecs' => array(
                'text' => 'inbound Valid Conn Avg Secs', 'parent' => 'inboundValidConnTotalNum',
            ),
            'outboundValidConnTotalSecs' => array(
                'text' => 'outbound Valid Conn Total Secs', 'parent' => 'outboundValidConnTotalNum',
            ),
            'outboundValidConnAvgSecs' => array(
                'text' => 'outbound Valid Conn Avg Secs', 'parent' => 'outboundValidConnTotalNum',
            ),
            'internalConnNum' => array('text' => 'internal Conn Num', 'parent' => ''),
            'internalConnTotalSecs' => array('text' => 'internal Conn Total Secs', 'parent' => 'internalConnNum'),
            'internalConnAvgSecs' => array('text' => 'internal Conn Avg Secs', 'parent' => 'internalConnNum'),
            'waitTotalNum' => array('text' => 'wait Total Num', 'parent' => ''),
            'waitTotalSecs' => array('text' => 'wait Total Secs', 'parent' => 'waitTotalNum'),
            'waitAvgSecs' => array('text' => 'wait Avg Secs', 'parent' => 'waitTotalNum'),
            'ringTotalNum' => array('text' => 'ring Total Num', 'parent' => ''),
            'ringTotalSecs' => array('text' => 'ring Total Secs', 'parent' => 'ringTotalNum'),
            'ringAvgSecs' => array('text' => 'ring Avg Secs', 'parent' => 'ringTotalNum'),
            'consultNum' => array('text' => 'consult Num', 'parent' => ''),
            'consultTotalSecs' => array('text' => 'consult Total Secs', 'parent' => 'consultNum'),
            'consultAvgSecs' => array('text' => 'consult Avg Secs', 'parent' => 'consultNum'),
            'holdNum' => array('text' => 'hold Num', 'parent' => ''),
            'hlodTotalSecs' => array('text' => 'hlod Total Secs', 'parent' => 'holdNum'),
            'holdAvgSecs' => array('text' => 'hold Avg Secs', 'parent' => 'holdNum'),
            'conferenceNum' => array('text' => 'conference Num', 'parent' => ''),
            'conferenceTotalSecs' => array('text' => 'conference Total Secs', 'parent' => 'conferenceNum'),
            'conferenceAvgSecs' => array('text' => 'conference Avg Secs', 'parent' => 'conferenceNum'),
            'transferTotalNum' => array('text' => 'transfer Total Num', 'parent' => ''),
            'transferedConnNum' => array('text' => 'transfered Conn Num', 'parent' => ''),
            'transferedConnTotalSecs' => array('text' => 'transfered Conn Total Secs', 'parent' => 'transferedConnNum'),
            'transferedConnAvgSecs' => array('text' => 'transfered Conn Avg Secs', 'parent' => 'transferedConnNum'),
            'loginTotalNum' => array('text' => 'login Total Num', 'parent' => ''),
            'loginTotalSecs' => array('text' => 'login Total Secs', 'parent' => 'loginTotalNum'),
            'readyTotalSecs' => array('text' => 'ready Total Secs', 'parent' => ''),
            'busyTotalNum' => array('text' => 'busy Total Num', 'parent' => ''),
            'busyTotalSecs' => array('text' => 'busy Total Secs', 'parent' => 'busyTotalNum'),
            'busyAvgSecs' => array('text' => 'busy Avg Secs', 'parent' => 'busyTotalNum'),
            'busyRate' => array('text' => 'busy Rate', 'parent' => ''),
            'workRate' => array('text' => 'work Rate', 'parent' => ''),
        ),
        'queue' => array(
            'inboundTotalNum' => array('text' => 'inbound Total Num', 'parent' => ''),
            'inboundConnNum' => array('text' => 'inbound Conn Num', 'parent' => ''),
            'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs', 'parent' => 'inboundConnNum'),
            'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs', 'parent' => 'inboundConnNum'),
            'inboundConnRate' => array('text' => 'inbound Conn Rate', 'parent' => 'inboundConnNum'),
            'outboundTotalNum' => array('text' => 'outbound Total Num', 'parent' => ''),
            'outboundConnNum' => array('text' => 'outbound Conn Num', 'parent' => ''),
            'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs', 'parent' => 'outboundConnNum'),
            'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs', 'parent' => 'outboundConnNum'),
            'outboundConnRate' => array('text' => 'outbound Conn Rate', 'parent' => 'outboundConnNum'),
            'validConnTotalNum' => array('text' => 'valid Conn Total Num', 'parent' => ''),
            'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num', 'parent' => ''),
            'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num', 'parent' => ''),
            'validConnTotalSecs' => array('text' => 'valid Conn Total Secs', 'parent' => 'validConnTotalNum'),
            'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs', 'parent' => 'validConnTotalNum'),
            'inboundValidConnTotalSecs' => array(
                'text' => 'inbound Valid Conn Total Secs', 'parent' => 'inboundValidConnTotalNum',
            ),
            'inboundValidConnAvgSecs' => array(
                'text' => 'inbound Valid Conn Avg Secs', 'parent' => 'inboundValidConnTotalNum',
            ),
            'outboundValidConnTotalSecs' => array(
                'text' => 'outbound Valid Conn Total Secs', 'parent' => 'outboundValidConnTotalNum',
            ),
            'outboundValidConnAvgSecs' => array(
                'text' => 'outbound Valid Conn Avg Secs', 'parent' => 'outboundValidConnTotalNum',
            ),
            'inboundConnInXSecsNum' => array('text' => 'inbound Conn In %X% Secs Num', 'parent' => ''),
            'inboundConnInXSecsRate' => array(
                'text' => 'inbound Conn In %X% Secs Rate', 'parent' => 'inboundConnInXSecsNum',
            ),
            'inboundAbandonTotalNum' => array('text' => 'inbound Abandon Total Num', 'parent' => ''),
            'inboundAbandonTotalSecs' => array(
                'text' => 'inbound Abandon Total Secs', 'parent' => 'inboundAbandonTotalNum',
            ),
            'inboundAbandonAvgSecs' => array(
                'text' => 'inbound Abandon Avg Secs', 'parent' => 'inboundAbandonTotalNum',
            ),
            'abandonReasonTotalNum1' => array('text' => 'Inbound Abandon Reason1 Total Num', 'parent' => ''),
            'abandonReasonTotalSecs1' => array('text' => 'Inbound Abandon Reason1 Total Secs', 'parent' => ''),
            'abandonReasonAvgSecs1' => array('text' => 'Inbound Abandon Reason1 Avg Secs', 'parent' => ''),
            'abandonReasonTotalNum3' => array('text' => 'Inbound Abandon Reason3 Total Num', 'parent' => ''),
            'abandonReasonTotalSecs3' => array('text' => 'Inbound Abandon Reason3 Total Secs', 'parent' => ''),
            'abandonReasonAvgSecs3' => array('text' => 'Inbound Abandon Reason3 Avg Secs', 'parent' => ''),
            'abandonReasonTotalNum4' => array('text' => 'Inbound Abandon Reason4 Total Num', 'parent' => ''),
            'abandonReasonTotalSecs4' => array('text' => 'Inbound Abandon Reason4 Total Secs', 'parent' => ''),
            'abandonReasonAvgSecs4' => array('text' => 'Inbound Abandon Reason4 Avg Secs', 'parent' => ''),
            'abandonReasonTotalNum5' => array('text' => 'Inbound Abandon Reason5 Total Num', 'parent' => ''),
            'abandonReasonTotalSecs5' => array('text' => 'Inbound Abandon Reason5 Total Secs', 'parent' => ''),
            'abandonReasonAvgSecs5' => array('text' => 'Inbound Abandon Reason5 Avg Secs', 'parent' => ''),
            'inboundAbandonInXSecsNum' => array('text' => 'inbound Abandon In %X% Secs Num', 'parent' => ''),
            'inboundAbandonInXSecsRate' => array(
                'text' => 'inbound Abandon In %X% Secs Rate', 'parent' => 'inboundAbandonInXSecsNum',
            ),
            'queueTotalNum' => array('text' => 'queue Total Num', 'parent' => ''),
            'queueTotalSecs' => array('text' => 'queue Total Secs', 'parent' => 'queueTotalNum'),
            'queueAvgSecs' => array('text' => 'queue Avg Secs', 'parent' => 'queueTotalNum'),
            'connQueueTotalNum' => array('text' => 'conn Queue Total Num', 'parent' => ''),
            'connQueueTotalSecs' => array('text' => 'conn Queue Total Secs', 'parent' => 'connQueueTotalNum'),
            'connQueueAvgSecs' => array('text' => 'conn Queue Avg Secs', 'parent' => 'connQueueTotalNum'),
            'ringTotalNum' => array('text' => 'ring Total Num', 'parent' => ''),
            'ringTotalSecs' => array('text' => 'ring Total Secs', 'parent' => 'ringTotalNum'),
            'ringAvgSecs' => array('text' => 'ring Avg Secs', 'parent' => 'ringTotalNum'),
            'waitTotalNum' => array('text' => 'wait Total Num', 'parent' => ''),
            'waitTotalSecs' => array('text' => 'wait Total Secs', 'parent' => 'waitTotalNum'),
            'waitAvgSecs' => array('text' => 'wait Avg Secs', 'parent' => 'waitTotalNum'),
        ),
        'system' => array(
            'ivrInboundTotalNum' => array('text' => 'ivr Inbound Total Num', 'parent' => ''),
            'inboundTotalNum' => array('text' => 'inbound Total Num', 'parent' => ''),
            'inboundRate' => array('text' => 'inbound Rate', 'parent' => 'inboundTotalNum'),
            'inboundConnNum' => array('text' => 'inbound Conn Num', 'parent' => ''),
            'inboundConnRate' => array('text' => 'inbound Conn Rate', 'parent' => 'inboundConnNum'),
            'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs', 'parent' => 'inboundConnNum'),
            'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs', 'parent' => 'inboundConnNum'),
            'outboundTotalNum' => array('text' => 'outbound Total Num', 'parent' => ''),
            'outboundConnNum' => array('text' => 'outbound Conn Num', 'parent' => ''),
            'outboundConnRate' => array('text' => 'outbound Conn Rate', 'parent' => 'outboundConnNum'),
            'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs', 'parent' => 'outboundConnNum'),
            'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs', 'parent' => 'outboundConnNum'),
            'validConnTotalNum' => array('text' => 'valid Conn Total Num', 'parent' => ''),
            'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num', 'parent' => ''),
            'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num', 'parent' => ''),
            'validConnTotalSecs' => array('text' => 'valid Conn Total Secs', 'parent' => 'validConnTotalNum'),
            'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs', 'parent' => 'validConnTotalNum'),
            'inboundValidConnTotalSecs' => array(
                'text' => 'inbound Valid Conn Total Secs', 'parent' => 'inboundValidConnTotalNum',
            ),
            'inboundValidConnAvgSecs' => array(
                'text' => 'inbound Valid Conn Avg Secs', 'parent' => 'inboundValidConnTotalNum',
            ),
            'outboundValidConnTotalSecs' => array(
                'text' => 'outbound Valid Conn Total Secs', 'parent' => 'outboundValidConnTotalNum',
            ),
            'outboundValidConnAvgSecs' => array(
                'text' => 'outbound Valid Conn Avg Secs', 'parent' => 'outboundValidConnTotalNum',
            ),
            'inboundConnInXSecsNum' => array('text' => 'inbound Conn In %X% Secs Num', 'parent' => ''),
            'inboundConnInXSecsRate' => array(
                'text' => 'inbound Conn In %X% Secs Rate', 'parent' => 'inboundConnInXSecsNum',
            ),
            'inboundAbandonTotalNum' => array('text' => 'Lost Num', 'parent' => ''),
            /*'inboundAbandonTotalSecs' => array(
                'text' => 'inbound Abandon Total Secs', 'parent' => 'inboundAbandonTotalNum',
            ),
            'inboundAbandonAvgSecs' => array(
                'text' => 'inbound Abandon Avg Secs', 'parent' => 'inboundAbandonTotalNum',
            ),*/
            //'inboundAbandonRate' => array('text' => 'inbound Abandon Rate', 'parent' => 'inboundAbandonTotalNum'),
            //'inboundAbandonInXSecsNum' => array('text' => 'inbound Abandon In %X% Secs Num', 'parent' => ''),
            /*'inboundAbandonInXSecsRate' => array(
                'text' => 'inbound Abandon In %X% Secs Rate', 'parent' => 'inboundAbandonInXSecsNum',
            ),*/
            'queueTotalNum' => array('text' => 'queue Total Num', 'parent' => ''),
            'queueTotalSecs' => array('text' => 'queue Total Secs', 'parent' => 'queueTotalNum'),
            'queueAvgSecs' => array('text' => 'queue Avg Secs', 'parent' => 'queueTotalNum'),
            'connQueueTotalNum' => array('text' => 'conn Queue Total Num', 'parent' => ''),
            'connQueueTotalSecs' => array('text' => 'conn Queue Total Secs', 'parent' => 'connQueueTotalNum'),
            'connQueueAvgSecs' => array('text' => 'conn Queue Avg Secs', 'parent' => 'connQueueTotalNum'),
            'ringTotalNum' => array('text' => 'ring Total Num', 'parent' => ''),
            'ringTotalSecs' => array('text' => 'ring Total Secs', 'parent' => 'ringTotalNum'),
            'ringAvgSecs' => array('text' => 'ring Avg Secs', 'parent' => 'ringTotalNum'),
            'loginTotalNum' => array('text' => 'login Total Num', 'parent' => ''),
            'loginTotalSecs' => array('text' => 'login Total Secs', 'parent' => 'loginTotalNum'),
            'readyTotalSecs' => array('text' => 'ready Total Secs', 'parent' => ''),
            'busyTotalNum' => array('text' => 'busy Total Num', 'parent' => ''),
            'busyTotalSecs' => array('text' => 'busy Total Secs', 'parent' => 'busyTotalNum'),
            'busyAvgSecs' => array('text' => 'busy Avg Secs', 'parent' => 'busyTotalNum'),
            'waitTotalNum' => array('text' => 'wait Total Num', 'parent' => ''),
            'waitTotalSecs' => array('text' => 'wait Total Secs', 'parent' => 'waitTotalNum'),
            'waitAvgSecs' => array('text' => 'wait Avg Secs', 'parent' => 'waitTotalNum'),
            'workRate' => array('text' => 'work Rate', 'parent' => ''),
        ),

        'group' => array(
            'inboundConnNum' => array('text' => 'inbound Conn Num', 'parent' => ''),
            'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs', 'parent' => 'inboundConnNum'),
            'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs', 'parent' => 'inboundConnNum'),
            'outboundTotalNum' => array('text' => 'outbound Total Num', 'parent' => ''),
            'outboundConnNum' => array('text' => 'outbound Conn Num', 'parent' => ''),
            'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs', 'parent' => 'outboundConnNum'),
            'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs', 'parent' => 'outboundConnNum'),
            'validConnTotalNum' => array('text' => 'valid Conn Total Num', 'parent' => ''),
            'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num', 'parent' => ''),
            'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num', 'parent' => ''),
            'connTotalNum' => array('text' => 'conn Total Num', 'parent' => ''),
            'connTotalSecs' => array('text' => 'conn Total Secs', 'parent' => 'connTotalNum'),
            'connAvgSecs' => array('text' => 'conn Avg Secs', 'parent' => 'connTotalNum'),
            'validConnTotalSecs' => array('text' => 'valid Conn Total Secs', 'parent' => 'validConnTotalNum'),
            'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs', 'parent' => 'validConnTotalNum'),
            'inboundValidConnTotalSecs' => array(
                'text' => 'inbound Valid Conn Total Secs', 'parent' => 'inboundValidConnTotalNum',
            ),
            'inboundValidConnAvgSecs' => array(
                'text' => 'inbound Valid Conn Avg Secs', 'parent' => 'inboundValidConnTotalNum',
            ),
            'outboundValidConnTotalSecs' => array(
                'text' => 'outbound Valid Conn Total Secs', 'parent' => 'outboundValidConnTotalNum',
            ),
            'outboundValidConnAvgSecs' => array(
                'text' => 'outbound Valid Conn Avg Secs', 'parent' => 'outboundValidConnTotalNum',
            ),
            'internalConnNum' => array('text' => 'internal Conn Num', 'parent' => ''),
            'internalConnTotalSecs' => array('text' => 'internal Conn Total Secs', 'parent' => 'internalConnNum'),
            'internalConnAvgSecs' => array('text' => 'internal Conn Avg Secs', 'parent' => 'internalConnNum'),
            'waitTotalNum' => array('text' => 'wait Total Num', 'parent' => ''),
            'waitTotalSecs' => array('text' => 'wait Total Secs', 'parent' => 'waitTotalNum'),
            'waitAvgSecs' => array('text' => 'wait Avg Secs', 'parent' => 'waitTotalNum'),
            'ringTotalNum' => array('text' => 'ring Total Num', 'parent' => ''),
            'ringTotalSecs' => array('text' => 'ring Total Secs', 'parent' => 'ringTotalNum'),
            'ringAvgSecs' => array('text' => 'ring Avg Secs', 'parent' => 'ringTotalNum'),
            'consultNum' => array('text' => 'consult Num', 'parent' => ''),
            'consultTotalSecs' => array('text' => 'consult Total Secs', 'parent' => 'consultNum'),
            'consultAvgSecs' => array('text' => 'consult Avg Secs', 'parent' => 'consultNum'),
            'holdNum' => array('text' => 'hold Num', 'parent' => ''),
            'hlodTotalSecs' => array('text' => 'hlod Total Secs', 'parent' => 'holdNum'),
            'holdAvgSecs' => array('text' => 'hold Avg Secs', 'parent' => 'holdNum'),
            'conferenceNum' => array('text' => 'conference Num', 'parent' => ''),
            'conferenceTotalSecs' => array('text' => 'conference Total Secs', 'parent' => 'conferenceNum'),
            'conferenceAvgSecs' => array('text' => 'conference Avg Secs', 'parent' => 'conferenceNum'),
            'transferTotalNum' => array('text' => 'transfer Total Num', 'parent' => ''),
            'transferedConnNum' => array('text' => 'transfered Conn Num', 'parent' => ''),
            'transferedConnTotalSecs' => array('text' => 'transfered Conn Total Secs', 'parent' => 'transferedConnNum'),
            'transferedConnAvgSecs' => array('text' => 'transfered Conn Avg Secs', 'parent' => 'transferedConnNum'),
            'loginTotalNum' => array('text' => 'login Total Num', 'parent' => ''),
            'loginTotalSecs' => array('text' => 'login Total Secs', 'parent' => 'loginTotalNum'),
            'readyTotalSecs' => array('text' => 'ready Total Secs', 'parent' => ''),
            'busyTotalNum' => array('text' => 'busy Total Num', 'parent' => ''),
            'busyTotalSecs' => array('text' => 'busy Total Secs', 'parent' => 'busyTotalNum'),
            'busyAvgSecs' => array('text' => 'busy Avg Secs', 'parent' => 'busyTotalNum'),
            'busyRate' => array('text' => 'busy Rate', 'parent' => ''),
            'workRate' => array('text' => 'work Rate', 'parent' => ''),
        ),
    );

    /** 放弃量具体原因 */
    const ABANDON_REASON_CALLER = 1;
    /** 主叫放弃 */
    const ABANDON_REASON_QUEUE_TIME_OUT = 3;
    /** 排队超时 */
    const ABANDON_REASON_QUEUE_FULL = 4;
    /** 技能组满溢出 */
    const ABANDON_REASON_NONE_AGENT = 5;
    /** 无坐席 */

    private $abandonReason = array(
        self::ABANDON_REASON_CALLER,
        self::ABANDON_REASON_QUEUE_TIME_OUT,
        self::ABANDON_REASON_QUEUE_FULL,
        self::ABANDON_REASON_NONE_AGENT,
    );

    /**
     * 自定义报表
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $vccId = $this->getUser()->getVccId();
        $agents = $this->get('icsoc_data.model.agent')->getAgentNameKeyedByIdArray($vccId);
        $queues = $this->get('icsoc_data.model.queue')->getQueueNameKeyedByIdArray($vccId);

        /** 判断是否启用业务组 */
        $configs = $this->getDoctrine()->getRepository("IcsocSecurityBundle:CcCcodConfigs")
            ->findOneBy(array('vccId' => $vccId));
        $isEnableGroup = 0;
        if (!empty($configs)) {
            $isEnableGroup = $configs->getIsEnableGroup();
        }

        $groups = $this->get('icsoc_data.model.agent')->getQroupNameKeyedByIdArray($vccId);

        $result = $this->get('icsoc_data.model.role')->getReportConfig();

        $reportConfigs = isset($result['data']) ? $result['data'] : array();

        if (!empty($reportConfigs)) {
            foreach ($reportConfigs as $key => $reportConfig) {
                if (isset($reportConfig['customItems'])) {
                    $customItems = array();
                    if (isset($reportConfig['customItems']['custom'])) {
                        foreach ($reportConfig['customItems']['custom'] as $k => $customItem) {
                            $customItems[$k] = array('text' => $customItem, 'parent' => '');
                        }
                    } else {
                        foreach ($reportConfig['customItems'] as $k => $customItem) {
                            $customItems[$k] = array('text' => $customItem, 'parent' => '');
                        }
                    }
                    $this->reportItems[$key] = array_merge($this->reportItems[$key], $customItems);
                }
            }


            foreach ($this->reportItems as $key => $reportItems) {
                $customs = isset($reportConfigs[$key]['custom']) ? $reportConfigs[$key]['custom'] : array();
                if (!empty($customs)) {
                    foreach ($reportItems as $item => $reportItem) {
                        if (!in_array($item, $customs)) {
                            unset($this->reportItems[$key][$item]);
                        }
                    }
                }
            }
        }

        $user = $this->container->get("security.token_storage")->getToken()->getUser();
        $loginType = $user->getLoginType();
        $roles = array();
        if ($loginType != 2) {
            $roles = $this->getDoctrine()->getRepository('IcsocSecurityBundle:CcRoles')->findBy(array('vccId' => $vccId));
        }

        return $this->render('IcsocCustomReportBundle:Report:index.html.twig', array(
            'reportItems' => $this->reportItems,
            'agents' => $agents,
            'queues' => $queues,
            'groups' => $groups,
            'isEnableGroup' => $isEnableGroup,
            'roles' => $roles,
            'loginType' => $loginType,
        ));
    }

    /**
     * 生成报表数据
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function generateReportAction(Request $request)
    {
        $data = $request->get('data', '');
        $dataArray = json_decode($data, true);
        $items = empty($dataArray['items']) ? array() : $dataArray['items'];
        $type = empty($dataArray['type']) ? '' : $dataArray['type'];
        $startDate = empty($dataArray['start_date']) ? '' : $dataArray['start_date'];
        $endDate = empty($dataArray['end_date']) ? '' : $dataArray['end_date'];
        $connSecs = empty($dataArray['connSecs']) ? '' : $dataArray['connSecs'];
        $connSecsArray = empty($connSecs) ? array() : explode(',', $connSecs);
        $abandonSecs = empty($dataArray['abandonSecs']) ? '' : $dataArray['abandonSecs'];
        $abandonSecsArray = empty($abandonSecs) ? array() : explode(',', $abandonSecs);
        $roleId = empty($dataArray['role_id']) ? 0 : $dataArray['role_id'];
        $title = array();
        $caption = '';
        $vccId = $this->getUser()->getVccId();

        /** 过滤我的报表的配置字段，如果在角色管理-报表指标配置中删除了已有的某个配置，那么我的报表中也要删除此项配置*/
        $conn = $this->container->get('doctrine.dbal.default_connection');
        $latestReportConfig = $conn->fetchAll("select report_config from cc_roles where role_id = :roleId", array('roleId' => $roleId));
        if (!empty($latestReportConfig)) {
            $latestReportConfig = json_decode($latestReportConfig[0]['report_config'], true);
            if (!empty($latestReportConfig[$type]['custom'])) {
                $newItem = array();
                foreach ($items as $item) {
                    if (in_array($item, $latestReportConfig[$type]['custom'])) {
                        $newItem[] = $item;
                    }
                }
                $items = $newItem;
            }
        }

        //查询具体的示忙原因
        $conn = $this->get('doctrine.dbal.default_connection');
        $reasons = $conn->fetchAll(
            'SELECT id,stat_reason FROM win_agstat_reason WHERE vcc_id=:vcc_id',
            array(':vcc_id' => $vccId)
        );

        $result = $this->get('icsoc_data.model.role')->getReportConfig('', $roleId);

        $reportConfigs = isset($result['data']) ? $result['data'] : array();

        foreach ($reportConfigs as $key => $reportConfig) {
            if (isset($reportConfig['customItems'])) {
                $customItems = array();
                if (isset($reportConfig['customItems']['custom'])) {
                    foreach ($reportConfig['customItems']['custom'] as $k => $customItem) {
                        $customItems[$k] = array('text' => $customItem, 'parent' => '');
                    }
                } else {
                    foreach ($reportConfig['customItems'] as $k => $customItem) {
                        $customItems[$k] = array('text' => $customItem, 'parent' => '');
                    }
                }
                $this->reportItems[$key] = array_merge($this->reportItems[$key], $customItems);
            }
        }

        //根据不同的类型分别处理
        switch ($type) {
            case 'agent'://坐席相关报表
                $caption = 'Agent Report【%start_date% ~ %end_date%】';
                $title['agent'] = 'Agent';
                $title['date'] = 'Date';
                //处理表头
                foreach ($items as $item) {
                    switch ($item) {
                        case 'busyTotalNum'://示忙次数
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['agent'][$item]['text']) ?
                                    $this->reportItems['agent'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : $reason['stat_reason'].'次数';
                                }
                                $title[$item.'other'] = '其他示忙次数';
                            }
                            break;
                        case 'busyTotalSecs'://总示忙时长
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['agent'][$item]['text']) ?
                                    $this->reportItems['agent'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : '总'.$reason['stat_reason'].'时长';
                                }
                                $title[$item.'other'] = '其他示忙时长';
                            }
                            break;
                        case 'busyAvgSecs'://平均示忙时长
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['agent'][$item]['text']) ?
                                    $this->reportItems['agent'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : '平均'.$reason['stat_reason'].'时长';
                                }
                            }
                            break;
                        case 'busyRate'://示忙占比
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['agent'][$item]['text']) ?
                                    $this->reportItems['agent'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : $reason['stat_reason'].'占比';
                                }
                                $title[$item.'other'] = '其他示忙占比';
                            }
                            break;
                        case 'inboundConnInXSecsNum'://X秒接通量
                        case 'inboundConnInXSecsRate'://X秒接通率
                            //设置了查询X秒接通量，则处理
                            if (!empty($connSecsArray)) {
                                foreach ($connSecsArray as $secs) {
                                    $itemKey = $item.$secs;
                                    $itemValue = isset($this->reportItems['agent'][$item]['text']) ?
                                        $this->reportItems['agent'][$item]['text'] : '';
                                    $title[$itemKey] = $this->get('translator')
                                        ->trans($itemValue, array('%X%' => $secs));
                                }
                            }
                            break;
                        case 'inboundAbandonInXSecsNum':
                        case 'inboundAbandonInXSecsRate':
                            //设置了查询X秒放弃量，则处理
                            if (!empty($abandonSecsArray)) {
                                foreach ($abandonSecsArray as $secs) {
                                    $itemKey = $item.$secs;
                                    $itemValue = isset($this->reportItems['agent'][$item]['text']) ?
                                        $this->reportItems['agent'][$item]['text'] : '';
                                    $title[$itemKey] = $this->get('translator')
                                        ->trans($itemValue, array('%X%' => $secs));
                                }
                            }
                            break;
                        default:
                            $title[$item] = isset($this->reportItems['agent'][$item]['text']) ?
                                $this->reportItems['agent'][$item]['text'] : '';
                            break;
                    }
                }
                break;
            case 'queue'://技能组相关报表
                $caption = 'Queue Report【%start_date% ~ %end_date%】';
                $title['queue'] = 'Queue';
                $title['date'] = 'Date';
                //处理表头
                foreach ($items as $item) {
                    switch ($item) {
                        case 'busyTotalNum'://示忙次数
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['queue'][$item]['text']) ?
                                    $this->reportItems['queue'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : $reason['stat_reason'].'次数';
                                }
                            }
                            break;
                        case 'busyTotalSecs'://总示忙时长
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['queue'][$item]['text']) ?
                                    $this->reportItems['queue'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : '总'.$reason['stat_reason'].'时长';
                                }
                            }
                            break;
                        case 'busyAvgSecs'://平均示忙时长
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['queue'][$item]['text']) ?
                                    $this->reportItems['queue'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : '平均'.$reason['stat_reason'].'时长';
                                }
                            }
                            break;
                        case 'busyRate'://示忙占比
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['queue'][$item]['text']) ?
                                    $this->reportItems['queue'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : $reason['stat_reason'].'占比';
                                }
                            }
                            break;
                        case 'inboundConnInXSecsNum'://X秒接通量
                        case 'inboundConnInXSecsRate'://X秒接通率
                            //设置了查询X秒接通量，则处理
                            if (!empty($connSecsArray)) {
                                foreach ($connSecsArray as $secs) {
                                    $itemKey = $item.$secs;
                                    $itemValue = isset($this->reportItems['queue'][$item]['text']) ?
                                        $this->reportItems['queue'][$item]['text'] : '';
                                    $title[$itemKey] = $this->get('translator')
                                        ->trans($itemValue, array('%X%' => $secs));
                                }
                            }
                            break;
                        case 'inboundAbandonInXSecsNum'://X秒放弃量
                        case 'inboundAbandonInXSecsRate'://X秒放弃率
                            //设置了查询X秒放弃量，则处理
                            if (!empty($abandonSecsArray)) {
                                foreach ($abandonSecsArray as $secs) {
                                    $itemKey = $item.$secs;
                                    $itemValue = isset($this->reportItems['queue'][$item]['text']) ?
                                        $this->reportItems['queue'][$item]['text'] : '';
                                    $title[$itemKey] = $this->get('translator')
                                        ->trans($itemValue, array('%X%' => $secs));
                                }
                            }
                            break;
                        default:
                            $title[$item] = isset($this->reportItems['queue'][$item]['text']) ?
                                $this->reportItems['queue'][$item]['text'] : '';
                            break;
                    }
                }
                break;
            case 'system'://呼叫中心相关报表
                $caption = 'System Report【%start_date% ~ %end_date%】';
                $title['date'] = 'Date';
                //处理表头
                foreach ($items as $item) {
                    switch ($item) {
                        case 'busyTotalNum'://示忙次数
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['system'][$item]['text']) ?
                                    $this->reportItems['system'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : $reason['stat_reason'].'次数';
                                }
                            }
                            break;
                        case 'busyTotalSecs'://总示忙时长
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['system'][$item]['text']) ?
                                    $this->reportItems['system'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : '总'.$reason['stat_reason'].'时长';
                                }
                            }
                            break;
                        case 'busyAvgSecs'://平均示忙时长
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['system'][$item]['text']) ?
                                    $this->reportItems['system'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : '平均'.$reason['stat_reason'].'时长';
                                }
                            }
                            break;
                        case 'busyRate'://示忙占比
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['system'][$item]['text']) ?
                                    $this->reportItems['system'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : $reason['stat_reason'].'占比';
                                }
                            }
                            break;
                        case 'inboundConnInXSecsNum'://X秒接通量
                        case 'inboundConnInXSecsRate'://X秒接通率
                            //设置了查询X秒接通量，则处理
                            if (!empty($connSecsArray)) {
                                foreach ($connSecsArray as $secs) {
                                    $itemKey = $item.$secs;
                                    $itemValue = isset($this->reportItems['system'][$item]['text']) ?
                                        $this->reportItems['system'][$item]['text'] : '';
                                    $title[$itemKey] = $this->get('translator')
                                        ->trans($itemValue, array('%X%' => $secs));
                                }
                            }
                            break;
                        //case 'inboundAbandonInXSecsNum'://X秒放弃量
                        /*case 'inboundAbandonInXSecsRate'://X秒放弃率
                            //设置了查询X秒放弃量，则处理
                            if (!empty($abandonSecsArray)) {
                                foreach ($abandonSecsArray as $secs) {
                                    $itemKey = $item.$secs;
                                    $itemValue = isset($this->reportItems['system'][$item]['text']) ?
                                        $this->reportItems['system'][$item]['text'] : '';
                                    $title[$itemKey] = $this->get('translator')
                                        ->trans($itemValue, array('%X%' => $secs));
                                }
                            }
                            break;*/
                        default:
                            $title[$item] = isset($this->reportItems['system'][$item]['text']) ?
                                $this->reportItems['system'][$item]['text'] : '';
                            break;
                    }
                }
                break;
            case 'group'://业务组相关报表
                $caption = 'Group Report【%start_date% ~ %end_date%】';
                $title['group'] = 'Group';
                $title['date'] = 'Date';
                //处理表头
                foreach ($items as $item) {
                    switch ($item) {
                        case 'busyTotalNum'://示忙次数
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['group'][$item]['text']) ?
                                    $this->reportItems['group'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : $reason['stat_reason'].'次数';
                                }
                                $title[$item.'other'] = '其他示忙次数';
                            }
                            break;
                        case 'busyTotalSecs'://总示忙时长
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['group'][$item]['text']) ?
                                    $this->reportItems['group'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : '总'.$reason['stat_reason'].'时长';
                                }
                                $title[$item.'other'] = '其他示忙时长';
                            }
                            break;
                        case 'busyAvgSecs'://平均示忙时长
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['group'][$item]['text']) ?
                                    $this->reportItems['group'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : '平均'.$reason['stat_reason'].'时长';
                                }
                            }
                            break;
                        case 'busyRate'://示忙占比
                            if (empty($reasons)) {
                                //未设置示忙原因，则只处理示忙
                                $title[$item] = isset($this->reportItems['group'][$item]['text']) ?
                                    $this->reportItems['group'][$item]['text'] : '';
                            } else {
                                //设置了示忙原因，则处理示忙原因的显示
                                foreach ($reasons as $reason) {
                                    $reasonKey = $item.(empty($reason['id']) ? '' : $reason['id']);
                                    $title[$reasonKey] = empty($reason['stat_reason']) ?
                                        '' : $reason['stat_reason'].'占比';
                                }
                                $title[$item.'other'] = '其他示忙占比';
                            }
                            break;
                        case 'inboundConnInXSecsNum'://X秒接通量
                        case 'inboundConnInXSecsRate'://X秒接通率
                            //设置了查询X秒接通量，则处理
                            if (!empty($connSecsArray)) {
                                foreach ($connSecsArray as $secs) {
                                    $itemKey = $item.$secs;
                                    $itemValue = isset($this->reportItems['group'][$item]['text']) ?
                                        $this->reportItems['group'][$item]['text'] : '';
                                    $title[$itemKey] = $this->get('translator')
                                        ->trans($itemValue, array('%X%' => $secs));
                                }
                            }
                            break;
                        case 'inboundAbandonInXSecsNum':
                        case 'inboundAbandonInXSecsRate':
                            //设置了查询X秒放弃量，则处理
                            if (!empty($abandonSecsArray)) {
                                foreach ($abandonSecsArray as $secs) {
                                    $itemKey = $item.$secs;
                                    $itemValue = isset($this->reportItems['group'][$item]['text']) ?
                                        $this->reportItems['group'][$item]['text'] : '';
                                    $title[$itemKey] = $this->get('translator')
                                        ->trans($itemValue, array('%X%' => $secs));
                                }
                            }
                            break;
                        default:
                            $title[$item] = isset($this->reportItems['group'][$item]['text']) ?
                                $this->reportItems['group'][$item]['text'] : '';
                            break;
                    }
                }
                break;
            default:
                break;
        }

        return $this->render('IcsocCustomReportBundle:Report:customReport.html.twig', array(
            'title' => $title,
            'param' => $data,
            'caption' => $caption,
            'start_date' => $startDate,
            'end_date' => $endDate,
        ));
    }

    /**
     * 获取统计数据
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getGeneratedReportDataAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));
        $data = json_decode($data, true);
        //是否导出，title为表头
        $export = $request->get('export', '');
        $title = $request->get('title', '');
        $items = empty($data['items']) ? array() : $data['items'];
        $type = empty($data['type']) ? '' : $data['type'];
        $reportType = empty($data['report_type']) ? '' : $data['report_type'];
        $connSecs = empty($data['connSecs']) ? '' : $data['connSecs'];
        $abandonSecs = empty($data['abandonSecs']) ? '' : $data['abandonSecs'];
        $validSecs = empty($data['validSecs']) ? 0 : $data['validSecs'];
        $connSecsArray = empty($connSecs) ? array() : explode(',', $connSecs);
        $abandonSecsArray = empty($abandonSecs) ? array() : explode(',', $abandonSecs);
        $roleId = empty($data['role_id']) ? 0 : $data['role_id'];

        $vccId = $this->getUser()->getVccId();

        if ($reportType == 'day') {
            $startDate = empty($data['start_date']) ? '' : $data['start_date'];
            $endDate = empty($data['end_date']) ? '' : $data['end_date'].' 23:59:59';
        } else {
            //月
            $startDate = empty($data['start_date']) ? '' : $data['start_date'];
            $endDate = empty($data['end_date']) ? '' : (date("Y-m-t", strtotime($data['end_date'])).' 23:59:59');
        }
        //查询具体的示忙原因
        $conn = $this->get('doctrine.dbal.default_connection');
        $reasons = $conn->fetchAll(
            'SELECT id,stat_reason FROM win_agstat_reason WHERE vcc_id=:vcc_id',
            array(':vcc_id' => $vccId)
        );


        $result = $this->get('icsoc_data.model.role')->getReportConfig($type, $roleId);

        $reportConfigs = isset($result['data']) ? $result['data'] : array();

        if (isset($reportConfigs['customItems'])) {
            $customItems = array();
            if (isset($reportConfigs['customItems']['custom'])) {
                foreach ($reportConfigs['customItems']['custom'] as $k => $customItem) {
                    $customItems[$k] = array('text' => $customItem, 'parent' => '');
                }
            } else {
                foreach ($reportConfigs['customItems'] as $k => $customItem) {
                    $customItems[$k] = array('text' => $customItem, 'parent' => '');
                }
            }

            $this->reportItems[$type] = array_merge($this->reportItems[$type], $customItems);
        }

        $items = array_keys($this->reportItems[$type]);

        $param = array(
            'items' => $items,
            'query' => array(
                'vcc_id' => $vccId,
                'start_time' => strtotime($startDate),
                'end_time' => strtotime($endDate),
                'ag_id' => empty($data['ag_id']) ? '' : $data['ag_id'],
                'que_id' => empty($data['que_id']) ? '' : $data['que_id'],
                'group_id' => empty($data['group_id']) ? '' : $data['group_id'],
                'phones' => empty($data['phones']) ? '' : $data['phones'],
            ),
            'options' => array(
                'validSecs' => $validSecs,
                'inboundConnInXSecsNum' => $connSecsArray,
                'inboundAbandonInXSecsNum' => $abandonSecsArray,
            ),
            'role_id' => $roleId,
        );

        //根据不同的类型分别处理
        $gridData = array();
        switch ($type) {
            case 'agent'://坐席相关报表
                if ($reportType == 'day') {
                    $result = $this->get('icsoc_custom_report.model.agent_statistic')
                        ->getAgentDayStatisticData($param);
                } else {
                    $result = $this->get('icsoc_custom_report.model.agent_statistic')
                        ->getAgentMonthStatisticData($param);
                }

                //处理数据用于表格显示
                $agents = $this->get('icsoc_data.model.agent')->getAgentNameKeyedByIdArray($vccId);
                $index = 0;
                if (!empty($result['data'])) {
                    foreach ($result['data'] as $date => $dateResult) {
                        if (!empty($dateResult)) {
                            //下标为坐席包含所有指标统计数据的数组
                            foreach ($dateResult as $agKey => $agValue) {
                                if (!array_key_exists($agKey, $agents)) {
                                    continue;
                                }
                                $gridData[$index] = array();
                                $gridData[$index]['agent'] = empty($agents[$agKey]) ? $agKey : $agents[$agKey];
                                $gridData[$index]['date'] = $date;
                                //遍历所有指标
                                foreach ($agValue as $itemKey => $itemValue) {
                                    //处理示忙、X秒接通量、X秒放弃量指标
                                    switch ($itemKey) {
                                        case 'busyTotalNum':
                                        case 'busyTotalSecs':
                                        case 'busyAvgSecs':
                                        case 'busyRate'://示忙指标处理
                                            if (empty($reasons)) {
                                                //未设置示忙原因，则只处理示忙
                                                if (is_array($itemValue)) {
                                                    //统计数据不为空时，返回的示忙结果为数组
                                                    $gridData[$index][$itemKey] = isset($itemValue[0]) ?
                                                        $itemValue[0] : 0;
                                                } else {
                                                    //统计数据为空时，则只返回0，非数组格式
                                                    $gridData[$index][$itemKey] = $itemValue;
                                                }
                                            } else {
                                                //设置了示忙原因，则处理示忙原因的数据
                                                $buyReason = array();
                                                foreach ($reasons as $reason) {
                                                    $reasonId = empty($reason['id']) ? '' : $reason['id'];
                                                    $buyReason[] = $reasonId;
                                                    $reasonKey = $itemKey.$reasonId;
                                                    $gridData[$index][$reasonKey] = isset($itemValue[$reasonId]) ?
                                                        $itemValue[$reasonId] : 0;
                                                }
                                                $gridData[$index][$itemKey.'other'] = 0;
                                                $itemvalues = '0';
                                                if (is_array($itemValue)) {
                                                    foreach ($itemValue as $item => $value) {
                                                        if (!in_array($item, $buyReason)) {
                                                            if ($value[strlen($value) - 1] == '%') {
                                                                $itemvalues += $value;
                                                                $gridData[$index][$itemKey.'other'] = $itemvalues.'%';
                                                            } else {
                                                                $gridData[$index][$itemKey.'other'] += $value;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                        case 'inboundConnInXSecsNum'://X秒接通量
                                        case 'inboundConnInXSecsRate'://X秒接通率
                                            //设置了查询X秒接通量，则处理
                                            if (!empty($connSecsArray)) {
                                                foreach ($connSecsArray as $secs) {
                                                    $itemKeySecs = $itemKey.$secs;
                                                    $gridData[$index][$itemKeySecs] = isset($itemValue[$secs]) ?
                                                        $itemValue[$secs] : 0;
                                                }
                                            }
                                            break;
                                        case 'inboundAbandonInXSecsNum'://X秒放弃量
                                        case 'inboundAbandonInXSecsRate'://X秒放弃率
                                            //设置了查询X秒接通量，则处理
                                            if (!empty($abandonSecsArray)) {
                                                foreach ($abandonSecsArray as $secs) {
                                                    $itemKeySecs = $itemKey.$secs;
                                                    $gridData[$index][$itemKeySecs] = isset($itemValue[$secs]) ?
                                                        $itemValue[$secs] : 0;
                                                }
                                            }
                                            break;
                                        default://其他指标
                                            $gridData[$index][$itemKey] = $itemValue;
                                            break;
                                    }
                                }
                                if (!in_array('date', $items)) {
                                    array_unshift($items, 'agent', 'date');
                                }
                                unset($result['data'][$date][$agKey]);
                                $index++;
                            }
                        }
                    }
                }
                break;
            case 'queue'://技能组相关报表
                $param['items'][] = 'abandonReasonTotalNum';
                $param['items'][] = 'abandonReasonTotalSecs';
                $param['items'][] = 'abandonReasonAvgSecs';
                if ($reportType == 'day') {
                    $result = $this->get('icsoc_custom_report.model.queue_statistic')
                        ->getQueueDayStatisticData($param);
                } else {
                    $result = $this->get('icsoc_custom_report.model.queue_statistic')
                        ->getQueueMonthStatisticData($param);
                }

                //处理数据用于表格显示
                $queues = $this->get('icsoc_data.model.queue')->getQueueNameKeyedByIdArray($vccId);
                $index = 0;
                if (!empty($result['data'])) {
                    foreach ($result['data'] as $date => $dateResult) {
                        if (!empty($dateResult)) {
                            //下标为坐席包含所有指标统计数据的数组
                            foreach ($dateResult as $agKey => $agValue) {
                                if (!array_key_exists($agKey, $queues)) {
                                    continue;
                                }
                                $gridData[$index] = array();
                                $gridData[$index]['queue'] = empty($queues[$agKey]) ? $agKey : $queues[$agKey];
                                $gridData[$index]['date'] = $date;
                                //遍历所有指标
                                foreach ($agValue as $itemKey => $itemValue) {
                                    //处理示忙、X秒接通量、X秒放弃量指标
                                    switch ($itemKey) {
                                        case 'busyTotalNum':
                                        case 'busyTotalSecs':
                                        case 'busyAvgSecs':
                                        case 'busyRate'://示忙指标处理
                                            if (empty($reasons)) {
                                                //未设置示忙原因，则只处理示忙
                                                if (is_array($itemValue)) {
                                                    //统计数据不为空时，返回的示忙结果为数组
                                                    $gridData[$index][$itemKey] = isset($itemValue[0]) ?
                                                        $itemValue[0] : 0;
                                                } else {
                                                    //统计数据为空时，则只返回0，非数组格式
                                                    $gridData[$index][$itemKey] = $itemValue;
                                                }
                                            } else {
                                                //设置了示忙原因，则处理示忙原因的数据
                                                foreach ($reasons as $reason) {
                                                    $reasonId = empty($reason['id']) ? '' : $reason['id'];
                                                    $reasonKey = $itemKey.$reasonId;
                                                    $gridData[$index][$reasonKey] = isset($itemValue[$reasonId]) ?
                                                        $itemValue[$reasonId] : 0;
                                                }
                                            }
                                            break;
                                        case 'inboundConnInXSecsNum'://X秒接通量
                                        case 'inboundConnInXSecsRate'://X秒接通率
                                            //设置了查询X秒接通量，则处理
                                            if (!empty($connSecsArray)) {
                                                foreach ($connSecsArray as $secs) {
                                                    $itemKeySecs = $itemKey.$secs;
                                                    $gridData[$index][$itemKeySecs] = isset($itemValue[$secs]) ?
                                                        $itemValue[$secs] : 0;
                                                }
                                            }
                                            break;
                                        case 'inboundAbandonInXSecsNum'://X秒放弃量
                                        case 'inboundAbandonInXSecsRate'://X秒放弃率
                                            //设置了查询X秒接通量，则处理
                                            if (!empty($abandonSecsArray)) {
                                                foreach ($abandonSecsArray as $secs) {
                                                    $itemKeySecs = $itemKey.$secs;
                                                    $gridData[$index][$itemKeySecs] = isset($itemValue[$secs]) ?
                                                        $itemValue[$secs] : 0;
                                                }
                                            }
                                            break;
                                        case 'abandonReasonTotalNum':
                                        case 'abandonReasonTotalSecs':
                                        case 'abandonReasonAvgSecs':
                                            foreach ($this->abandonReason as $reason) {
                                                $reasonKey = $itemKey.$reason;
                                                $gridData[$index][$reasonKey] = isset($itemValue[$reason]) ?
                                                    $itemValue[$reason] : 0;
                                            }
                                            break;
                                        default://其他指标
                                            $gridData[$index][$itemKey] = $itemValue;
                                            break;
                                    }
                                }
                                if (!in_array('date', $items)) {
                                    array_unshift($items, 'queue', 'date');
                                }
                                unset($result['data'][$date][$agKey]);
                                $index++;
                            }
                        }
                    }
                }
                break;
            case 'system'://呼叫中心相关报表
                if ($reportType == 'day') {
                    $result = $this->get('icsoc_custom_report.model.system_statistic')
                        ->getSystemDayStatisticData($param);
                } else {
                    $result = $this->get('icsoc_custom_report.model.system_statistic')
                        ->getSystemMonthStatisticData($param);
                }
                //处理数据用于表格显示
                $index = 0;
                if (!empty($result['data'])) {
                    foreach ($result['data'] as $date => $dateResult) {
                        if (!empty($dateResult)) {
                            //下标为坐席包含所有指标统计数据的数组
                            foreach ($dateResult as $agKey => $agValue) {
                                $gridData[$index] = array();
                                $gridData[$index]['date'] = $date;
                                //遍历所有指标
                                foreach ($agValue as $itemKey => $itemValue) {
                                    //处理示忙、X秒接通量、X秒放弃量指标
                                    switch ($itemKey) {
                                        case 'busyTotalNum':
                                        case 'busyTotalSecs':
                                        case 'busyAvgSecs':
                                        case 'busyRate'://示忙指标处理
                                            if (empty($reasons)) {
                                                //未设置示忙原因，则只处理示忙
                                                if (is_array($itemValue)) {
                                                    //统计数据不为空时，返回的示忙结果为数组
                                                    $gridData[$index][$itemKey] = isset($itemValue[0]) ?
                                                        $itemValue[0] : 0;
                                                } else {
                                                    //统计数据为空时，则只返回0，非数组格式
                                                    $gridData[$index][$itemKey] = $itemValue;
                                                }
                                            } else {
                                                //设置了示忙原因，则处理示忙原因的数据
                                                foreach ($reasons as $reason) {
                                                    $reasonId = empty($reason['id']) ? '' : $reason['id'];
                                                    $reasonKey = $itemKey.$reasonId;
                                                    $gridData[$index][$reasonKey] = isset($itemValue[$reasonId]) ?
                                                        $itemValue[$reasonId] : 0;
                                                }
                                            }
                                            break;
                                        case 'inboundConnInXSecsNum'://X秒接通量
                                        case 'inboundConnInXSecsRate'://X秒接通率
                                            //设置了查询X秒接通量，则处理
                                            if (!empty($connSecsArray)) {
                                                foreach ($connSecsArray as $secs) {
                                                    $itemKeySecs = $itemKey.$secs;
                                                    $gridData[$index][$itemKeySecs] = isset($itemValue[$secs]) ?
                                                        $itemValue[$secs] : 0;
                                                }
                                            }
                                            break;
                                        case 'inboundAbandonInXSecsNum'://X秒放弃量
                                        case 'inboundAbandonInXSecsRate'://X秒放弃率
                                            //设置了查询X秒接通量，则处理
                                            if (!empty($abandonSecsArray)) {
                                                foreach ($abandonSecsArray as $secs) {
                                                    $itemKeySecs = $itemKey.$secs;
                                                    $gridData[$index][$itemKeySecs] = isset($itemValue[$secs]) ?
                                                        $itemValue[$secs] : 0;
                                                }
                                            }
                                            break;
                                        default://其他指标
                                            $gridData[$index][$itemKey] = $itemValue;
                                            break;
                                    }
                                }
                                if (!in_array('date', $items)) {
                                    array_unshift($items, 'date');
                                }
                                unset($result['data'][$date][$agKey]);
                                $index++;
                            }
                        }
                    }
                }
                break;
            case 'group':  //业务组报表
                if ($reportType == 'day') {
                    $result = $this->get('icsoc_custom_report.model.group_statistic')
                        ->getGroupDayStatisticData($param);
                } else {
                    $result = $this->get('icsoc_custom_report.model.group_statistic')
                        ->getGroupMonthStatisticData($param);
                }

                //处理数据用于表格显示
                $groups = $this->get('icsoc_data.model.agent')->getQroupNameKeyedByIdArray($vccId);
                $index = 0;
                if (!empty($result['data'])) {
                    foreach ($result['data'] as $date => $dateResult) {
                        if (!empty($dateResult)) {
                            //下标为坐席包含所有指标统计数据的数组
                            foreach ($dateResult as $grKey => $grValue) {
                                if (!array_key_exists($grKey, $groups)) {
                                    continue;
                                }
                                $gridData[$index] = array();
                                $gridData[$index]['group'] = empty($groups[$grKey]) ? $grKey : $groups[$grKey];
                                $gridData[$index]['date'] = $date;
                                //遍历所有指标
                                foreach ($grValue as $itemKey => $itemValue) {
                                    //处理示忙、X秒接通量、X秒放弃量指标
                                    switch ($itemKey) {
                                        case 'busyTotalNum':
                                        case 'busyTotalSecs':
                                        case 'busyAvgSecs':
                                        case 'busyRate'://示忙指标处理
                                            if (empty($reasons)) {
                                                //未设置示忙原因，则只处理示忙
                                                if (is_array($itemValue)) {
                                                    //统计数据不为空时，返回的示忙结果为数组
                                                    $gridData[$index][$itemKey] = isset($itemValue[0]) ?
                                                        $itemValue[0] : 0;
                                                } else {
                                                    //统计数据为空时，则只返回0，非数组格式
                                                    $gridData[$index][$itemKey] = $itemValue;
                                                }
                                            } else {
                                                //设置了示忙原因，则处理示忙原因的数据
                                                $buyReason = array();
                                                foreach ($reasons as $reason) {
                                                    $reasonId = empty($reason['id']) ? '' : $reason['id'];
                                                    $buyReason[] = $reasonId;
                                                    $reasonKey = $itemKey.$reasonId;
                                                    $gridData[$index][$reasonKey] = isset($itemValue[$reasonId]) ?
                                                        $itemValue[$reasonId] : 0;
                                                }
                                                $gridData[$index][$itemKey.'other'] = 0;
                                                $itemvalues = '0';
                                                if (is_array($itemValue)) {
                                                    foreach ($itemValue as $item => $value) {
                                                        if (!in_array($item, $buyReason)) {
                                                            if ($value[strlen($value) - 1] == '%') {
                                                                $itemvalues += $value;
                                                                $gridData[$index][$itemKey.'other'] = $itemvalues.'%';
                                                            } else {
                                                                $gridData[$index][$itemKey.'other'] += $value;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                        case 'inboundConnInXSecsNum'://X秒接通量
                                        case 'inboundConnInXSecsRate'://X秒接通率
                                            //设置了查询X秒接通量，则处理
                                            if (!empty($connSecsArray)) {
                                                foreach ($connSecsArray as $secs) {
                                                    $itemKeySecs = $itemKey.$secs;
                                                    $gridData[$index][$itemKeySecs] = isset($itemValue[$secs]) ?
                                                        $itemValue[$secs] : 0;
                                                }
                                            }
                                            break;
                                        case 'inboundAbandonInXSecsNum'://X秒放弃量
                                        case 'inboundAbandonInXSecsRate'://X秒放弃率
                                            //设置了查询X秒接通量，则处理
                                            if (!empty($abandonSecsArray)) {
                                                foreach ($abandonSecsArray as $secs) {
                                                    $itemKeySecs = $itemKey.$secs;
                                                    $gridData[$index][$itemKeySecs] = isset($itemValue[$secs]) ?
                                                        $itemValue[$secs] : 0;
                                                }
                                            }
                                            break;
                                        default://其他指标
                                            $gridData[$index][$itemKey] = $itemValue;
                                            break;
                                    }
                                }
                                if (!in_array('date', $items)) {
                                    array_unshift($items, 'group', 'date');
                                }
                                unset($result['data'][$date][$grKey]);
                                $index++;
                            }
                        }
                    }
                }
                break;

            default:
                break;
        }

        //导出功能
        if (!empty($export)) {
            switch ($export) {
                case 'csv':
                    return $this->exportCsv($gridData, $title);
                case 'excel':
                    return $this->exportExcel($gridData, $title);
                default:
                    exit;
            }
        }

        return new JsonResponse($gridData);
    }

    /**
     * 导出CSV文件
     *
     * @param array  $data  需要导出的数据
     * @param string $title 表头字符串，逗号分隔
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function exportCsv(array $data, $title)
    {
        $translator = $this->get('translator');
        if (empty($title) || !is_string($title)) {
            $param = array(
                'data' => array(
                    'msg_detail' => $translator->trans(
                        'Title is empty or is not string, the type is %type%',
                        array('%type%' => gettype($title))
                    ),
                    'link' => array(
                        array(
                            'text' => $translator->trans('Custom Report'),
                            'href' => $this->generateUrl('icsoc_custom_report_home'),
                        ),
                    ),
                    'type' => 'danger',
                ),
            );

            return $this->redirect($this->generateUrl('system_message', $param));
        }

        $fields = array();
        $titleArray = json_decode($title, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            $data = array(
                'data' => array(
                    'msg_detail' => $translator->trans(
                        'Parse Json Error: %error%',
                        array('%error%' => json_last_error())
                    ),
                    'link' => array(
                        array(
                            'text' => $translator->trans('Custom Report'),
                            'href' => $this->generateUrl('icsoc_custom_report_home'),
                        ),
                    ),
                    'type' => 'danger',
                ),
            );

            return $this->redirect($this->generateUrl('system_message', $data));
        }

        $datas = array();
        foreach ($data as $k => $v) {
            foreach ($titleArray as $key => $val) {
                if (!isset($v[$key])) {
                    $v[$key] = '';
                }
                $datas[$k][$key] = $v[$key];
            }
        }

        foreach ($titleArray as $item => $itemText) {
            $fields[$item] = array(
                'name' => $translator->trans($itemText),
                'type' => 'string',
            );
        }

        try {
            $source = new ArrayType($datas, $fields);
            $csv = $this->get('icsoc_core.export.csv');
            $csv->export($source);
        } catch (\Exception $e) {
            $data = array(
                'data' => array(
                    'msg_detail' => $translator->trans(
                        'Export Csv Error: %error%',
                        array('%error%' => $e->getMessage())
                    ),
                    'link' => array(
                        array(
                            'text' => $translator->trans('Custom Report'),
                            'href' => $this->generateUrl('icsoc_custom_report_home'),
                        ),
                    ),
                    'type' => 'danger',
                ),
            );

            return $this->redirect($this->generateUrl('system_message', $data));
        }

        return new Response();
    }

    /**
     * 导出EXCEL文件
     *
     * @param array  $data  需要导出的数据
     * @param string $title 表头字符串，逗号分隔
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function exportExcel(array $data, $title)
    {
        $translator = $this->get('translator');
        if (empty($title) || !is_string($title)) {
            $param = array(
                'data' => array(
                    'msg_detail' => $translator->trans(
                        'Title is empty or is not string, the type is %type%',
                        array('%type%' => gettype($title))
                    ),
                    'link' => array(
                        array(
                            'text' => $translator->trans('Custom Report'),
                            'href' => $this->generateUrl('icsoc_custom_report_home'),
                        ),
                    ),
                    'type' => 'danger',
                ),
            );

            return $this->redirect($this->generateUrl('system_message', $param));
        }

        $fields = array();
        $titleArray = json_decode($title, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            $data = array(
                'data' => array(
                    'msg_detail' => $translator->trans(
                        'Parse Json Error: %error%',
                        array('%error%' => json_last_error())
                    ),
                    'link' => array(
                        array(
                            'text' => $translator->trans('Custom Report'),
                            'href' => $this->generateUrl('icsoc_custom_report_home'),
                        ),
                    ),
                    'type' => 'danger',
                ),
            );

            return $this->redirect($this->generateUrl('system_message', $data));
        }

        foreach ($titleArray as $item => $itemText) {
            $fields[$item] = array(
                'name' => $translator->trans($itemText),
                'type' => 'string',
            );
        }

        try {
            $source = new ArrayType($data, $fields);
            $excel = $this->get('icsoc_core.export.excel');
            $excel->export($source);
        } catch (\Exception $e) {
            $data = array(
                'data' => array(
                    'msg_detail' => $translator->trans(
                        'Export Excel Error: %error%',
                        array('%error%' => $e->getMessage())
                    ),
                    'link' => array(
                        array(
                            'text' => $translator->trans('Custom Report'),
                            'href' => $this->generateUrl('icsoc_custom_report_home'),
                        ),
                    ),
                    'type' => 'danger',
                ),
            );

            return $this->redirect($this->generateUrl('system_message', $data));
        }

        return new Response();
    }

    /**
     * 保存报表
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function saveReportAction(Request $request)
    {
        $name = $request->get('name', '');
        $items = $request->get('items', '');
        $reportType = $request->get('report_type', '');
        $roleId = $request->get('role_id', 0);

        $em = $this->getDoctrine()->getManager();

        $report = new Report();
        $report->setName($name);
        $report->setItems($items);
        $report->setVccId($this->getUser()->getVccId());
        $report->setReportType($reportType);
        $report->setRoleId($roleId);
        $em->persist($report);
        $em->flush();

        return new JsonResponse();
    }

    /**
     * 获取我的报表
     *
     * @return JsonResponse
     */
    public function getReportAction(Request $request)
    {
        $roleId = $request->get('roleId');
        $em = $this->getDoctrine()->getManager();
        $reports = $em->getRepository('IcsocCustomReportBundle:Report')
            ->findAllByVccId($this->getUser()->getVccId(), $roleId);

        return new JsonResponse($reports);
    }

    /**
     * 删除我的报表；
     * @param Request $request
     * @return JsonResponse
     */
    public function delTagAction(Request $request)
    {
        $id = $request->get('id', 0);
        $vccId = $this->getUser()->getVccId();
        $em = $this->getDoctrine()->getManager();
        $res = $em->createQuery(
            "DELETE FROM IcsocCustomReportBundle:Report r WHERE r.id = :id AND r.vccId = :vcc_id"
        )->setParameters(array('id' => $id, 'vcc_id' => $vccId))->execute();
        if ($res) {
            return new JsonResponse(array('error' => false));
        }

        return new JsonResponse(array('error' => true, 'message' => $this->get('translator')->trans('Delete fail')));
    }

    /**
     * 根据角色获取报表配置
     *
     * @param Request $request
     * @return array|JsonResponse
     */
    public function getReportItemsByRoleAction(Request $request)
    {
        $roleId = $request->get('role_id', '');

        $conn = $this->container->get('doctrine.dbal.default_connection');
        $result = $conn->fetchColumn(
            "SELECT report_config FROM cc_roles WHERE role_id = :role_id",
            array('role_id' => $roleId,
            )
        );

        $vccId = $this->getVccId();
        $globalConfig = $conn->fetchAll("select report_config from cc_global_report_config where vcc_id = :vcc_id",
            array('vcc_id' => $vccId));
        $globalConfigs = array();
        if (!empty($globalConfig)) {
            $globalConfigs = json_decode($globalConfig[0]['report_config'], true);
        }
        $reportConfigs = json_decode($result, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            $this->container->get('logger')->error(json_last_error());

            return new JsonResponse(array());
        }

        $cusReportItems = array();
        if (!empty($reportConfigs)) {
            foreach ($reportConfigs as $key => $reportConfig) {
                if (!empty($reportConfig['custom'])) {
                    $customItems = $this->processConfigCustomItems($reportConfig, $key);
                    $cusReportItems[$key] = $customItems;
                } else {
                   if (!empty($globalConfigs)) {
                       if (isset($globalConfigs[$key]['showValues']['custom'])) {
                           $globalShowValues = $globalConfigs[$key]['showValues']['custom'];
                       } else {
                           $globalShowValues = array();
                       }

                       $globalCustoms = array();
                       foreach ($globalConfigs[$key]['custom'] as $item ) {
                           $globalCustoms[$item]['name'] = isset($globalShowValues[$item]['name']) ? $globalShowValues[$item]['name'] : $this->reportItems[$key][$item]['text'];
                           $globalCustoms[$item]['sort'] = isset($globalShowValues[$item]['sort']) ? $globalShowValues[$item]['name'] : 100;
                       }
                       $globalRepCustoms = $this->container->get('icsoc_data.model.role')->getReportConfigOrder($globalCustoms, 100);
                       $customItems = array();
                       if (!empty($globalRepCustoms)) {
                           foreach ($globalRepCustoms as $k => $v) {
                               $customItems[$k] = array('text' => $v['name'],
                                   'parent' => isset($this->reportItems[$key][$k]['parent']) ? $this->reportItems[$key][$k]['parent'] : '');
                           }
                       }
                       $cusReportItems[$key] = $customItems;
                   }
                }
            }
        }

        return new JsonResponse($cusReportItems);
    }

    public function processConfigCustomItems($reportConfig, $reportType = '')
    {
        if (isset($reportConfig['showValues'])) {
            $repCustomItems = array();

            if (!empty($reportConfig['showValues']['custom'])) {
                $repShowValues = $reportConfig['showValues']['custom'];
                foreach ($reportConfig['custom'] as $item) {
                    $repCustomItems[$item]['name'] = $repShowValues[$item]['name'];
                    $repCustomItems[$item]['sort'] = $repShowValues[$item]['sort'];
                }
            }

            $repCustomItems = $this->container->get('icsoc_data.model.role')->getReportConfigOrder($repCustomItems, 100);
            $customItems = array();
            if (!empty($repCustomItems)) {
                foreach ($repCustomItems as $k => $customItem) {
                    $customItems[$k] = array('text' => $customItem['name'], 'parent' => '');
                }
            }
        } else {
            $customItems = array();
            $translator = $this->get('translator');
            foreach ($reportConfig['custom'] as $item) {
                $itemName = isset($this->reportItems[$reportType][$item]['text']) ? $this->reportItems[$reportType][$item]['text'] : $item;
                $customItems[$item] = array('text' => $translator->trans($itemName),
                                            'parent' => isset($this->reportItems[$reportType][$item]['parent']) ? $this->reportItems[$reportType][$item]['parent'] : '');
            }
        }

        return $customItems;
    }
}
