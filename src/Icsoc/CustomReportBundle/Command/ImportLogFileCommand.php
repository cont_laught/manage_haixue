<?php

namespace Icsoc\CustomReportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class ImportLogFileCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('report:import:logfile')
            ->setDescription('导入报表日志文件到数据库')
            ->addOption('dir', 'd', InputOption::VALUE_REQUIRED, '日志文件的目录')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '2048M');
        $dir = $input->getOption('dir');
        $finder = new Finder();
        $finder->in($dir);

        /** @var \SplFileInfo $file */
        foreach ($finder as $file) {
            $conn = $this->getContainer()->get('doctrine.dbal.default_connection');
            $logFile = $file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename();
            $contents = file($logFile);
            if (strstr($logFile, 'amount')) {
                //量临时表数据
                foreach ($contents as $line => $row) {
                    $row = substr($row, 21);
                    $row = str_replace("\r\n", "", $row);
                    $row = explode(' ', $row);
                    $rowData = array(
                        'vcc_id' => $row[1],
                        'que_id' => $row[2],
                        'ag_id' => $row[3],
                        'type' => $row[4],
                        'starttime' => strtotime($row[5].' '.$row[6]),
                        'group_id' => $row[7],
                        'call_time' => strtotime($row[8].' '.$row[9]),
                    );

                    $conn->insert('rep_tmp_amount', $rowData);
                    unset($rowData);
                    unset($contents[$line]);
                }
            } else {
                foreach ($contents as $line => $row) {
                    $row = substr($row, 21);
                    $row = str_replace("\r\n", "", $row);
                    $row = explode(' ', $row);
                    $rowData = array(
                        'vcc_id' => $row[1],
                        'call_id' => $row[2],
                        'que_id' => $row[3],
                        'ag_id' => $row[4],
                        'type' => $row[5],
                        'ext_type' => $row[6],
                        'starttime' => strtotime($row[7].' '.$row[8]),
                        'dealtime' => strtotime($row[9].' '.$row[10]),
                        'endtime' => strtotime($row[11].' '.$row[12]),
                        'group_id' => $row[13],
                        'call_time' => strtotime($row[14].' '.$row[15]),
                    );
                    $conn->insert('rep_tmp_time', $rowData);
                    unset($rowData);
                    unset($contents[$line]);
                }
            }

            unset($contents);
            $conn->close();
        }
    }
}
