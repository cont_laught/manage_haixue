<?php

namespace Icsoc\NameListBundle\Controller;

use Icsoc\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * 黑名单
 * Class BlackController
 * @package Icsoc\NameListBundle\Controller
 */
class BlackController extends BaseController
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        $vccId = $this->getVccId();
        $types = $this->get('icsoc_core.common.class')->getBlacTypes($vccId);
        $black_phone_type = $this->container->getParameter("black_phone_type");
        ksort($black_phone_type);
        return $this->render('IcsocNameListBundle:Black:index.html.twig', array(
                'types' => $types,
                'black_types' => $black_phone_type,
            ));
    }

    /**
     * 获取黑名单
     * @param Request $request
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'asc');
        $sidx = $request->get('sidx', 'id');
        $fliter = $request->get('fliter', '');
        $info = array(
            'pagination'=>array('rows'=>$rows, 'page'=>$page),
            'filter'=> !empty($fliter) ? json_decode($fliter, true) : array(),
            'sort'=>array('order'=>$sord,'field'=>$sidx),
        );
        $vcc_code = $this->getUser()->getVccCode();
        $list = $this->get('icsoc_data.model.black')
            ->getBlackList(array('vcc_code'=>$vcc_code, 'info'=>json_encode($info)));
        $data = array();
        if ($list['code'] == 200 && isset($list['data'])) {
            foreach ($list['data'] as $key=>$val) {
                $list['data'][$key]['handle_time'] = date('Y-m-d H:i:s', $val['handle_time']);
            }
        }
        $data['rows'] = (isset($list['data']) && !empty($list['data'])) ? $list['data'] : array();
        $data['page'] = $list['page'];
        $data['total'] = isset($list['totalPage']) ? $list['totalPage'] : 1;
        $data['records'] = isset($list['total']) ? $list['total'] : 0;

        return new JsonResponse($data);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function delAction(Request $request)
    {
        $opr = $request->get('oper', '');
        if ($opr == 'del') {
            $ids = $request->get('id', '');
            $arr = array('vcc_code'=>$this->getUser()->getVccCode(),'ids'=>$ids);
            $message = $this->get('icsoc_data.model.black')->delBlack($arr);
            if (isset($message['code']) && $message['code'] == 200) {
                return new JsonResponse(array('error'=>0, 'message'=> $this->get('translator')->trans('Success')));
            } else {
                return new JsonResponse(array('error'=>1, 'message'=>$message['message']));
            }
        }
        return new Response('');
    }

    /**
     * 添加黑名单
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $queForm = $this->createForm('black_info_form');
        if ($request->getMethod() == 'POST') {
            $user = $this->getUser();
            $arr = $request->get('black_info_form');
            $arr['phones'] = empty($arr['phone_num']) ? array() : explode("\n", $arr['phone_num']);
            $arr['vcc_code'] = $user->getVccCode();
            $arr['call_type'] = empty($arr['call_type']) ? 1: $arr['call_type'];
            $arr['black_type'] = empty($arr['black_type']) ? 1: $arr['black_type'];
            $arr['remark'] = empty($arr['remark']) ? '': $arr['remark'];
            $flag = $user->getFlag();
            if ($flag == 'ccadmin' || $flag == 'ccod') {
                $arr['handle_name'] = empty($user->getAdminName()) ? '' : $user->getAdminName();
            } else {
                $arr['handle_name'] = empty($user->getAgNum()) ? '' : $user->getAgNum();
            }
            $msg = $this->get('icsoc_data.model.black')->blacklistAdd($arr);
            if (isset($msg['code']) && ($msg['code'] == 200)) {
                $type = "success";
                $message = $this->get('translator')->trans('Add black list of success');
            } else {
                $type = "danger";
                $message = $this->get('translator')->trans('Add black list of success').
                isset($msg['message']) ? $msg['message'] : '';
            }
            $data = array(
                'data'=>array(
                    'msg_detail'=>$message,
                    'type' => $type,
                    'link' => array(
                        array('text'=>$this->get('translator')->trans('Continue to add'),
                            'href'=>$this->generateUrl('icsoc_namelist_black_add')),
                        array('text'=>$this->get('translator')->trans('Black name list'),
                            'href'=>$this->generateUrl('icsoc_namelist_black_index')),
                    )
                )
            );
            return $this->redirect($this->generateUrl('icsoc_ui_message', $data));
        }
        return $this->render('IcsocNameListBundle:Black:blankInfo.html.twig', array(
            'form' => $queForm->createView()
        ));
    }
}
