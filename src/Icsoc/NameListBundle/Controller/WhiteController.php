<?php

namespace Icsoc\NameListBundle\Controller;

use Icsoc\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * 白名单
 * Class WhiteController
 * @package Icsoc\NameListBundle\Controller
 */
class WhiteController extends BaseController
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('IcsocNameListBundle:White:index.html.twig');
    }

    /**
     * 获取白名单
     * @param Request $request
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'asc');
        $sidx = $request->get('sidx', 'id');
        $fliter = $request->get('fliter', '');

        $info = array(
            'pagination'=>array('rows'=>$rows, 'page'=>$page),
            'filter'=> !empty($fliter) ? json_decode($fliter, true) : array(),
            'sort'=>array('order'=>$sord,'field'=>$sidx),
        );
        $vcc_code = $this->getUser()->getVccCode();
        $list = $this->get('icsoc_data.model.white')
            ->getWhiteList(array('vcc_code'=>$vcc_code, 'info'=>json_encode($info)));
        $data['rows'] = (isset($list['data']) && !empty($list['data'])) ? $list['data'] : array();
        $data['page'] = $list['page'];
        $data['total'] = isset($list['totalPage']) ? $list['totalPage'] : 1;
        $data['records'] = isset($list['total']) ? $list['total'] : 0;
        return new JsonResponse($data);
    }

    /**
     * 操作白名单
     * @param Request $request
     * @return JsonResponse
     */
    public function oprAction(Request $request)
    {
        $opr = $request->get('oper', '');
        if ($opr == 'del') {
            $ids = $request->get('id', '');
            $arr = array('vcc_code'=>$this->getUser()->getVccCode(),'ids'=>$ids);
            $message = $this->get('icsoc_data.model.white')->delWhite($arr);
            if (isset($message['code']) && $message['code'] == 200) {
                return new JsonResponse(array('error'=>0, 'message'=> $this->get('translator')->trans('Success')));
            } else {
                return new JsonResponse(array('error'=>1, 'message'=>$message['message']));
            }
        }
        return new Response('');
    }

    /**
     * 添加白名单
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $queForm = $this->createForm('white_info_form');
        if ($request->getMethod() == 'POST') {
            $arr = $request->get('white_info_form');
            $arr['phones'] = empty($arr['phone_num']) ? array() : explode("\n", $arr['phone_num']);
            $arr['vcc_code'] = $this->getUser()->getVccCode();
            $msg = $this->get('icsoc_data.model.white')->whiteAdd($arr);
            if (isset($msg['code']) && ($msg['code'] == 200)) {
                $message = $this->get('translator')->trans('Add white list of success');
            } else {
                $message = $this->get('translator')->trans('Add white list of success').
                isset($msg['message']) ? $msg['message'] : '';
            }
            $data = array(
                'data'=>array(
                    'msg_detail'=>$message,
                    'type'=>'success',
                    'link' => array(
                        array('text'=>$this->get('translator')->trans('Continue to add'),
                            'href'=>$this->generateUrl('icsoc_namelist_white_add')),
                        array('text'=>$this->get('translator')->trans('White name list'),
                            'href'=>$this->generateUrl('icsoc_namelist_white_index')),
                    )
                )
            );
            return $this->redirect($this->generateUrl('icsoc_ui_message', $data));
        }
        return $this->render('IcsocNameListBundle:White:whiteInfo.html.twig', array(
            'form' => $queForm->createView()
        ));
    }
}
