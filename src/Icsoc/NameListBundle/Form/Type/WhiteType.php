<?php
namespace Icsoc\NameListBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class WhiteType extends AbstractType
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        $vccCode = $this->container->get('security.token_storage')->getToken()->getUser()->getVccCode();
        $res = $this->container->get('icsoc_data.model.relaynumber')->getphone(array('vcc_code'=>$vccCode));
//        $params = array('vcc_code'=>$vccCode,'info'=>array('filter'=>array('sound_type'=>3)));
//        $sound = $this->container->get("icsoc_data.model.sound")->soundList($params);
        $phones = $soundList = array();
        if ($res['code'] && $res['code'] == 200 && is_array($res['data'])) {
            foreach ($res['data'] as $val) {
                $phones[$val['phone']] = $val['phone'];
            }
        }
//        if ($sound['code'] && $sound['code'] == 200 && is_array($sound['data'])) {
//            foreach ($sound['data'] as $val) {
//                $soundList[$val['id']] = $val['name'];
//            }
//        }
        $builder
            ->add('trunk_num', 'choice', array(
                'attr' => array(
                    'class' => 'col-xs-10 col-sm-3'
                ),
                'label' => 'The relay number',
                'choices' => $phones,
                'data' => isset($data['que_type']) ? $data['que_type'] : 1,
            ))
//            ->add('white_sound', 'choice', array(
//                    'label' => 'Choice file',
//                    'placeholder'=>'Please select',
//                    'choices'=> $soundList,
//                    'attr'=>array('class'=>'col-xs-10 col-sm-3'),
//                    'data'=>isset($data['white_sound']) ? $data['white_sound'] : 0,
//                ))
            ->add('phone_num', 'textarea', array(
                'attr' => array('class' => 'col-xs-10 col-sm-3',
                    'placeholder'=>$this->container->get('translator')->trans('Each line a number')),
                'label' => 'White list number',
            ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'white_info_form';
    }
}
