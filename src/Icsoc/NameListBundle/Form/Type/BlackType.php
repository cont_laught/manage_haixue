<?php
namespace Icsoc\NameListBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BlackType extends AbstractType
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $blackType  = $this->container->getParameter("black_phone_type");
        $vccCode = $user->getVccCode();
        $callType  = $this->container->get('icsoc_core.common.class')->getBlacTypes($user->getVccId());
        unset($callType[0]);//删除全部
        $res = $this->container->get('icsoc_data.model.relaynumber')->getphone(array('vcc_code'=>$vccCode));
        $phones[0] = $this->container->get('translator')->trans('all');
        if ($res['code'] && $res['code'] == 200 && is_array($res['data'])) {
            foreach ($res['data'] as $val) {
                $phones[$val['phone']] = $val['phone'];
            }
        }
        //暂时先屏蔽呼出的模糊匹配
        if ((isset($data['call_type']) && $data['call_type'] == 2) || (!isset($data['call_type']) && !isset($callType[1]))) {
            unset($blackType[2]);
        }
        $builder
            ->add('trunk_num', 'choice', array(
                'attr' => array(
                    'class' => 'col-xs-10 col-sm-3'
                ),
                'label' => 'The relay number',
                'choices' => $phones,
                'data' => isset($data['que_type']) ? $data['que_type'] : 1,
            ))
            ->add('call_type', 'choice', array(
                    'label'=>'Black type',
                    'attr'=>array('class'=>'col-xs-10 col-sm-5','onclick'=>'switchCallType()'),
                    'choices'=>$callType,
                    'data'=>isset($data['call_type']) ? $data['call_type'] : 1,
                    'expanded'=>true,
                    'multiple'=>false
                ))
            ->add('black_type', 'choice', array(
                'label'=>'The number type',
                'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                'choices'=>$blackType,
                'data'=>isset($data['black_type']) ? $data['black_type'] : 1,
                'help'=>'Fixed number format is 01012345678, the fuzzy matching format 1381234****: said 1381234 at the beginning and a length of 11 bit number',
                'expanded'=>true,
                'multiple'=>false
            ))
            ->add('phone_num', 'textarea', array(
                'attr' => array('class' => 'col-xs-10 col-sm-3',
                    'placeholder'=>$this->container->get('translator')->trans('Each line a number')),
                'label' => 'Black list number',
            ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'black_info_form';
    }
}
