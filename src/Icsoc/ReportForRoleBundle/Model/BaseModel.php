<?php

namespace Icsoc\ReportForRoleBundle\Model;

/**
 * Class BaseModel
 *
 * @package Icsoc\ReportForRoleBundle\Model
 */
class BaseModel
{
    /**
     * 统计的所有指标
     * text为指标的名称
     *
     * @var array
     */
    protected $reportItems = array(
        'system'=>array(
            'fixed_report' => array(
                'ivrInboundTotalNum' => array('text' => 'Ivr Num', 'field' => 'ivr_num', 'sortable' => true, 'width' => 60, 'default_show' => true),
                'inboundTotalNum'=>array('text'=>'System In Num', 'field'=>'in_num', 'sortable'=>true, 'width'=>100, 'default_show'=> true),
                'inboundRate'=>array('text'=>'inbound Rate', 'field'=>'in_rate', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'inboundConnNum'=>array('text'=>'Conn Num', 'field'=>'conn_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'inboundConnRate'=>array('text'=>'Conn Rate', 'field'=>'conn_rate', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'inboundConnTotalSecs'=>array('text'=>'System Conn Secs', 'field'=>'conn_secs', 'sortable'=>true, 'width'=>100, 'default_show'=> false),
                'inboundConnAvgSecs'=>array('text'=>'Avg Conn Secs', 'field'=>'avg_connsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'inboundAbandonTotalNum'=>array('text'=>'Lost Num', 'field'=>'lost_num', 'sortable'=>true, 'width'=>60, 'default_show'=> true),
                //'inboundAbandonTotalSecs'=>array('text'=>'Lost Secs', 'field'=>'lost_secs', 'sortable'=>true, 'width'=>110, 'default_show'=> false),
                //'inboundAbandonAvgSecs'=>array('text'=>'Avg Lost Secs', 'field'=>'avg_lostsecs', 'sortable'=>false, 'width'=>120, 'default_show'=> true),
                'queueTotalSecs'=>array('text'=>'Queue Secs', 'field'=>'queue_secs', 'sortable'=>true, 'width'=>100, 'default_show'=> false),
                'queueAvgSecs'=>array('text'=>'Avg Queue Secs', 'field'=>'avg_queuesecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'ringTotalNum'=>array('text'=>'Ring Num', 'field'=>'ring_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'ringTotalSecs'=>array('text'=>'Ring Secs', 'field'=>'ring_secs', 'sortable'=>true, 'width'=>100, 'default_show'=> false),
                'ringAvgSecs'=>array('text'=>'Avg Ring Secs', 'field'=>'avg_ringsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'loginTotalSecs'=>array('text'=>'Login Total Secs', 'field'=>'login_secs', 'sortable'=>true, 'width'=>100, 'default_show'=> false),
                'waitTotalSecs'=>array('text'=>'Wait Total Secs', 'field'=>'wait_secs', 'sortable'=>true, 'width'=>110, 'default_show'=> false),
                'WatchAgents'=>array('text'=>'Duty Agents', 'field'=>'agents', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'workRate'=>array('text'=>'Work Rate', 'field'=>'work_rate', 'sortable'=>false, 'width'=>60, 'default_show'=> true),
                //'inboundAbandonRate' => array('text' => 'inbound Abandon Rate', 'field' => 'abandon_rate', 'sortable'=>false, 'width'=>60, 'default_show'=> true),
            ),
            'custom_report'=>array(
                'inboundAbandonTotalNum' => array('text' => 'Lost Num', 'monitor' => true),
                'ivrInboundTotalNum' => array('text' => 'ivr Inbound Total Num', 'monitor' => true),
                'inboundTotalNum' => array('text' => 'inbound Total Num', 'monitor' => true),
                'inboundRate' => array('text' => 'inbound Rate', 'monitor' => true),
                'inboundConnNum' => array('text' => 'inbound Conn Num', 'monitor' => true),
                'inboundConnRate' => array('text' => 'inbound Conn Rate', 'monitor' => true),
                'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs'),
                'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs'),
                'outboundTotalNum' => array('text' => 'outbound Total Num', 'monitor' => true),
                'outboundConnNum' => array('text' => 'outbound Conn Num', 'monitor' => true),
                'outboundConnRate' => array('text' => 'outbound Conn Rate', 'monitor' => true),
                'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs'),
                'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs'),
                'validConnTotalNum' => array('text' => 'valid Conn Total Num', 'monitor' => true),
                'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num', 'monitor' => true),
                'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num', 'monitor' => true),
                'validConnTotalSecs' => array('text' => 'valid Conn Total Secs'),
                'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs'),
                'inboundValidConnTotalSecs' => array('text' => 'inbound Valid Conn Total Secs'),
                'inboundValidConnAvgSecs' => array('text' => 'inbound Valid Conn Avg Secs'),
                'outboundValidConnTotalSecs' => array('text' => 'outbound Valid Conn Total Secs'),
                'outboundValidConnAvgSecs' => array('text' => 'outbound Valid Conn Avg Secs'),
                'inboundConnInXSecsNum' => array('text' => 'inbound Conn In %X% Secs Num', 'monitor' => true),
                'inboundConnInXSecsRate' => array('text' => 'inbound Conn In %X% Secs Rate', 'monitor' => true),
                'queueTotalNum' => array('text' => 'queue Total Num', 'monitor' => true),
                'queueTotalSecs' => array('text' => 'queue Total Secs'),
                'queueAvgSecs' => array('text' => 'queue Avg Secs'),
                'connQueueTotalNum' => array('text' => 'conn Queue Total Num', 'monitor' => true),
                'connQueueTotalSecs' => array('text' => 'conn Queue Total Secs'),
                'connQueueAvgSecs' => array('text' => 'conn Queue Avg Secs'),
                'ringTotalNum' => array('text' => 'ring Total Num', 'monitor' => true),
                'ringTotalSecs' => array('text' => 'ring Total Secs'),
                'ringAvgSecs' => array('text' => 'ring Avg Secs'),
                'loginTotalNum' => array('text' => 'login Total Num'),
                'loginTotalSecs' => array('text' => 'login Total Secs'),
                'readyTotalSecs' => array('text' => 'ready Total Secs'),
                'busyTotalNum' => array('text' => 'busy Total Num', 'monitor' => true),
                'busyTotalSecs' => array('text' => 'busy Total Secs'),
                'busyAvgSecs' => array('text' => 'busy Avg Secs'),
                'waitTotalNum' => array('text' => 'wait Total Num', 'monitor' => true),
                'waitTotalSecs' => array('text' => 'wait Total Secs'),
                'waitAvgSecs' => array('text' => 'wait Avg Secs'),
                'workRate' => array('text' => 'work Rate', 'monitor' => true),
            ),

        ),
        'queue'=>array(
            'fixed_report'=>array(
                'inboundTotalNum' => array('text'=>'Queue In Num', 'field'=>'in_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'inboundAbandonTotalNum' => array('text'=>'Lost Num', 'field'=>'lost_num', 'sortable'=>true, 'width'=>60, 'default_show'=> true),
                'inboundAbandonTotalSecs' => array('text'=>'Lost Secs', 'field'=>'lost_secs', 'sortable'=>true, 'width'=>60, 'default_show'=> false),
                'inboundAbandonAvgSecs' => array('text'=>'Avg Lost Secs', 'field'=>'avg_lostsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'outboundTotalNum'=>array('text'=>'Out Num', 'field'=>'out_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'outboundConnTotalNum'=>array('text'=>'outbound Conn Total Num', 'field'=>'out_calls', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'outboundSecs'=>array('text'=>'out Secs', 'field'=>'out_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'outboundConnRate'=> array('text'=>'outbound Conn Rate', 'field'=>'outconn_rate', 'sortable'=>true, 'width'=>100, 'default_show'=> true),
                'outboundAvgSecs' => array('text'=>'avg outbound Secs', 'field'=>'avg_outboundsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'inboundConnNum' => array('text'=>'Conn Num', 'field'=>'conn_num', 'sortable'=>true, 'width'=>60, 'default_show'=> true),
                'inboundConnRate' => array('text'=>'Conn Rate', 'field'=>'conn_rate', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'inboundConnTotalSecs' => array('text'=>'Conn Secs', 'field'=>'conn_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> false),
                'inboundConnAvgSecs' => array('text'=>'Avg Conn Secs', 'field'=>'avg_connsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'queueTotalSecs' => array('text'=>'Queue Secs', 'field'=>'queue_secs', 'sortable'=>true, 'width'=>100, 'default_show'=> false),
                'ringTotalNum' =>array('text'=>'Ring Num', 'field'=>'ring_num', 'sortable'=>true, 'width'=>70, 'default_show'=> true),
                'ringTotalSecs' =>array('text'=>'Ring Secs', 'field'=>'ring_secs', 'sortable'=>true, 'width'=>70, 'default_show'=> false),
                'ringAvgSecs' => array('text'=>'Avg Ring Secs', 'field'=>'avg_ringsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'callerAbandonTotalNum' => array('text'=>'Caller Lost Num', 'field'=>'lost1_num', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'callerAbandonTotalSecs' => array('text'=>'Caller Lost Secs', 'field'=>'lost1_secs', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'callerAbandonAvgSecs' => array('text'=>'AVG Caller Lost Secs', 'field'=>'avg_lost1_secs', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'timeoutTotalNum' => array('text'=>'Queue Timeout Lost Num', 'field'=>'lost3_num', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'timeoutTotalSecs' => array('text'=>'Queue Timeout Lost Secs', 'field'=>'lost3_secs', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'timeoutAvgSecs' => array('text'=>'AVG Queue Timeout Secs', 'field'=>'avg_lost3_secs', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'fullTotalNum' => array('text'=>'Queue Full Num', 'field'=>'lost4_num', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'fullTotalSecs' => array('text'=>'Queue Full Secs', 'field'=>'lost4_secs', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'fullAvgSecs' => array('text'=>'AVG Queue Full Secs', 'field'=>'avg_lost4_secs', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'agentTotalNum' => array('text'=>'None Agent Num', 'field'=>'lost5_num', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'agentTotalSecs' => array('text'=>'None Agent Secs', 'field'=>'lost5_secs', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'agentAvgSecs' => array('text'=>'AVG None Agent Secs', 'field'=>'avg_lost5_secs', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'evaluate_-4'=>array('text'=>'Evaluate-4', 'field'=>'evaluate_-4', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'evaluate_-3'=>array('text'=>'Evaluate-3', 'field'=>'evaluate_-3', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'evaluate_-1'=>array('text'=>'Evaluate-1', 'field'=>'evaluate_-1', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'evaluate_0'=>array('text'=>'Evaluate0', 'field'=>'evaluate_0', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_1'=>array('text'=>'Evaluate1', 'field'=>'evaluate_1', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_2'=>array('text'=>'Evaluate2', 'field'=>'evaluate_2', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_3'=>array('text'=>'Evaluate3', 'field'=>'evaluate_3', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_4'=>array('text'=>'Evaluate4', 'field'=>'evaluate_4', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_5'=>array('text'=>'Evaluate5', 'field'=>'evaluate_5', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_6'=>array('text'=>'Evaluate6', 'field'=>'evaluate_6', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_7'=>array('text'=>'Evaluate7', 'field'=>'evaluate_7', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_8'=>array('text'=>'Evaluate8', 'field'=>'evaluate_8', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_9'=>array('text'=>'Evaluate9', 'field'=>'evaluate_9', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'first_conn_num'=>array('text'=>'First Conn Num', 'field'=>'first_conn_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),

            ),
            'custom_report'=>array(
                'inboundTotalNum' => array('text' => 'inbound Total Num', 'monitor' => true),
                'inboundConnNum' => array('text' => 'inbound Conn Num', 'monitor' => true),
                'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs'),
                'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs'),
                'inboundConnRate' => array('text' => 'inbound Conn Rate', 'monitor' => true),
                'outboundTotalNum' => array('text' => 'outbound Total Num'),
                'outboundConnNum' => array('text' => 'outbound Conn Num'),
                'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs'),
                'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs'),
                'outboundConnRate' => array('text' => 'outbound Conn Rate'),
                'validConnTotalNum' => array('text' => 'valid Conn Total Num'),
                'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num', 'monitor' => true),
                'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num'),
                'validConnTotalSecs' => array('text' => 'valid Conn Total Secs'),
                'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs'),
                'inboundValidConnTotalSecs' => array('text' => 'inbound Valid Conn Total Secs'),
                'inboundValidConnAvgSecs' => array('text' => 'inbound Valid Conn Avg Secs'),
                'outboundValidConnTotalSecs' => array('text' => 'outbound Valid Conn Total Secs'),
                'outboundValidConnAvgSecs' => array('text' => 'outbound Valid Conn Avg Secs'),
                'inboundConnInXSecsNum' => array('text' => 'inbound Conn In %X% Secs Num', 'monitor' => true),
                'inboundConnInXSecsRate' => array('text' => 'inbound Conn In %X% Secs Rate', 'monitor' => true),
                'inboundAbandonTotalNum' => array('text' => 'inbound Abandon Total Num', 'monitor' => true),
                'inboundAbandonTotalSecs' => array('text' => 'inbound Abandon Total Secs'),
                'inboundAbandonAvgSecs' => array('text' => 'inbound Abandon Avg Secs'),
                'abandonReasonTotalNum1' => array('text' => 'Inbound Abandon Reason1 Total Num', 'monitor' => true),
                'abandonReasonTotalSecs1' => array('text' => 'Inbound Abandon Reason1 Total Secs'),
                'abandonReasonAvgSecs1' => array('text' => 'Inbound Abandon Reason1 Avg Secs'),
                'abandonReasonTotalNum3' => array('text' => 'Inbound Abandon Reason3 Total Num', 'monitor' => true),
                'abandonReasonTotalSecs3' => array('text' => 'Inbound Abandon Reason3 Total Secs'),
                'abandonReasonAvgSecs3' => array('text' => 'Inbound Abandon Reason3 Avg Secs'),
                'abandonReasonTotalNum4' => array('text' => 'Inbound Abandon Reason4 Total Num', 'monitor' => true),
                'abandonReasonTotalSecs4' => array('text' => 'Inbound Abandon Reason4 Total Secs'),
                'abandonReasonAvgSecs4' => array('text' => 'Inbound Abandon Reason4 Avg Secs'),
                'abandonReasonTotalNum5' => array('text' => 'Inbound Abandon Reason5 Total Num', 'monitor' => true),
                'abandonReasonTotalSecs5' => array('text' => 'Inbound Abandon Reason5 Total Secs'),
                'abandonReasonAvgSecs5' => array('text' => 'Inbound Abandon Reason5 Avg Secs'),
                'inboundAbandonInXSecsNum' => array('text' => 'inbound Abandon In %X% Secs Num', 'monitor' => true),
                'inboundAbandonInXSecsRate' => array('text' => 'inbound Abandon In %X% Secs Rate', 'monitor' => true),
                'queueTotalNum' => array('text' => 'queue Total Num', 'monitor' => true),
                'queueTotalSecs' => array('text' => 'queue Total Secs'),
                'queueAvgSecs' => array('text' => 'queue Avg Secs'),
                'connQueueTotalNum' => array('text' => 'conn Queue Total Num', 'monitor' => true),
                'connQueueTotalSecs' => array('text' => 'conn Queue Total Secs'),
                'connQueueAvgSecs' => array('text' => 'conn Queue Avg Secs'),
                'ringTotalNum' => array('text' => 'ring Total Num', 'monitor' => true),
                'ringTotalSecs' => array('text' => 'ring Total Secs'),
                'ringAvgSecs' => array('text' => 'ring Avg Secs'),
                'waitTotalNum' => array('text' => 'wait Total Num', 'monitor' => true),
                'waitTotalSecs' => array('text' => 'wait Total Secs'),
                'waitAvgSecs' => array('text' => 'wait Avg Secs'),
            ),

        ),
        'group'=>array(
            'fixed_report'=>array(
                'inboundTotalNum'=>array('text'=>'Group In Num', 'field'=>'in_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'outboundTotalNum'=>array('text'=>'Out Num', 'field'=>'out_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'outboundConnNum' => array('text' => 'Group Conn num', 'field' => 'out_calls', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'outboundConnTotalSecs'=>array('text'=>'outbound Conn Total Secs', 'field'=>'out_secs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'outboundConnAvgSecs'=>array('text'=>'outbound Conn Avg Secs', 'field'=>'avg_calls', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'connTotalSecs'=>array('text'=>'Conn Secs', 'field'=>'conn_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'connAvgSecs'=>array('text'=>'Avg Connsecs', 'field'=>'avg_connsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'internalConnNum'=>array('text'=>'Internal Num', 'field'=>'internal_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'ringTotalNum'=>array('text'=>'Ring Num', 'field'=>'ring_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'ringTotalSecs'=>array('text'=>'Ring Secs', 'field'=>'ring_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> false),
                'ringAvgSecs'=>array('text'=>'Avg Ringsecs', 'field'=>'avg_ringsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'consultNum'=>array('text'=>'Consult Num', 'field'=>'consult_num', 'sortable'=>true, 'width'=>70, 'default_show'=> true),
                'consultTotalSecs'=>array('text'=>'Consult Secs', 'field'=>'consult_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> false),
                'consultAvgSecs'=>array('text'=>'Avg Consultsecs', 'field'=>'avg_consultsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'holdNum'=>array('text'=>'Hold Num', 'field'=>'hold_num', 'sortable'=>true, 'width'=>70, 'default_show'=> true),
                'hlodTotalSecs'=>array('text'=>'Hold Secs', 'field'=>'hold_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> false),
                'holdAvgSecs'=>array('text'=>'Avg Holdsecs', 'field'=>'avg_holdsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'conferenceNum'=>array('text'=>'Conference Num', 'field'=>'conference_num', 'sortable'=>true, 'width'=>60, 'default_show'=> true),
                'conferenceTotalSecs'=>array('text'=>'Conference Secs', 'field'=>'conference_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> false),
                'conferenceAvgSecs'=>array('text'=>'Avg Consecs', 'field'=>'avg_consecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'transferTotalNum'=>array('text'=>'Shift Num', 'field'=>'shift_num', 'sortable'=>true, 'width'=>70, 'default_show'=> true),
                'waitTotalSecs'=>array('text'=>'wait Total Secs', 'field'=>'wait_secs', 'sortable'=>true, 'width'=>110, 'default_show'=> false),
                'waitTotalNum'=>array('text'=>'wait Total Num', 'field'=>'wait_num', 'sortable'=>true, 'width'=>110, 'default_show'=> false),
                'waitAvgSecs'=>array('text'=>'Avg Waitsecs', 'field'=>'avg_waitsecs', 'sortable'=>false, 'width'=>120, 'default_show'=> true),
                'loginTotalSecs'=>array('text'=>'Login Secs', 'field'=>'login_secs', 'sortable'=>true, 'width'=>90, 'default_show'=> true),
                'readyTotalSecs'=>array('text'=>'Ready Secs', 'field'=>'ready_secs', 'sortable'=>true, 'width'=>90, 'default_show'=> true),
                'busyTotalSecs'=>array('text'=>'Busy Secs', 'field'=>'busy_secs', 'sortable'=>true, 'width'=>90, 'default_show'=> true),
                'busyRate'=>array('text'=>'Busy Rate', 'field'=>'busy_rate', 'sortable'=>false, 'width'=>90, 'default_show'=> true),
                'workRate'=>array('text'=>'Work Rate', 'field'=>'work_rate', 'sortable'=>false, 'width'=>90, 'default_show'=> true),
                'evaluate_-4'=>array('text'=>'Evaluate-4', 'field'=>'evaluate_-4', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'evaluate_-3'=>array('text'=>'Evaluate-3', 'field'=>'evaluate_-3', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'evaluate_-1'=>array('text'=>'Evaluate-1', 'field'=>'evaluate_-1', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'evaluate_0'=>array('text'=>'Evaluate0', 'field'=>'evaluate_0', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_1'=>array('text'=>'Evaluate1', 'field'=>'evaluate_1', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_2'=>array('text'=>'Evaluate2', 'field'=>'evaluate_2', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_3'=>array('text'=>'Evaluate3', 'field'=>'evaluate_3', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_4'=>array('text'=>'Evaluate4', 'field'=>'evaluate_4', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_5'=>array('text'=>'Evaluate5', 'field'=>'evaluate_5', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_6'=>array('text'=>'Evaluate6', 'field'=>'evaluate_6', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_7'=>array('text'=>'Evaluate7', 'field'=>'evaluate_7', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_8'=>array('text'=>'Evaluate8', 'field'=>'evaluate_8', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_9'=>array('text'=>'Evaluate9', 'field'=>'evaluate_9', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'refuse_num'=>array('text'=>'Refuse Num', 'field'=>'refuse_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'WatchAgents'=>array('text'=>'Duty Agents', 'field'=>'agents', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
            ),
            'custom_report' => array(
                'inboundConnNum' => array('text' => 'inbound Conn Num', 'parent' => '', 'monitor' => true),
                'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs', 'parent' => 'inboundConnNum'),
                'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs', 'parent' => 'inboundConnNum'),
                'outboundTotalNum' => array('text' => 'outbound Total Num', 'parent' => ''),
                'outboundConnNum' => array('text' => 'outbound Conn Num', 'parent' => '', 'monitor' => true),
                'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs', 'parent' => 'outboundConnNum'),
                'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs', 'parent' => 'outboundConnNum'),
                'validConnTotalNum' => array('text' => 'valid Conn Total Num', 'parent' => '', 'monitor' => true),
                'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num', 'parent' => ''),
                'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num', 'parent' => ''),
                'connTotalNum' => array('text' => 'conn Total Num', 'parent' => '', 'monitor' => true),
                'connTotalSecs' => array('text' => 'conn Total Secs', 'parent' => 'connTotalNum'),
                'connAvgSecs' => array('text' => 'conn Avg Secs', 'parent' => 'connTotalNum'),
                'validConnTotalSecs' => array('text' => 'valid Conn Total Secs', 'parent' => 'validConnTotalNum'),
                'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs', 'parent' => 'validConnTotalNum'),
                'inboundValidConnTotalSecs' => array(
                    'text' => 'inbound Valid Conn Total Secs', 'parent' => 'inboundValidConnTotalNum',
                ),
                'inboundValidConnAvgSecs' => array(
                    'text' => 'inbound Valid Conn Avg Secs', 'parent' => 'inboundValidConnTotalNum',
                ),
                'outboundValidConnTotalSecs' => array(
                    'text' => 'outbound Valid Conn Total Secs', 'parent' => 'outboundValidConnTotalNum',
                ),
                'outboundValidConnAvgSecs' => array(
                    'text' => 'outbound Valid Conn Avg Secs', 'parent' => 'outboundValidConnTotalNum',
                ),
                'internalConnNum' => array('text' => 'internal Conn Num', 'parent' => '', 'monitor' => true),
                'internalConnTotalSecs' => array('text' => 'internal Conn Total Secs', 'parent' => 'internalConnNum'),
                'internalConnAvgSecs' => array('text' => 'internal Conn Avg Secs', 'parent' => 'internalConnNum'),
                'waitTotalNum' => array('text' => 'wait Total Num', 'parent' => '', 'monitor' => true),
                'waitTotalSecs' => array('text' => 'wait Total Secs', 'parent' => 'waitTotalNum'),
                'waitAvgSecs' => array('text' => 'wait Avg Secs', 'parent' => 'waitTotalNum'),
                'ringTotalNum' => array('text' => 'ring Total Num', 'parent' => '', 'monitor' => true),
                'ringTotalSecs' => array('text' => 'ring Total Secs', 'parent' => 'ringTotalNum'),
                'ringAvgSecs' => array('text' => 'ring Avg Secs', 'parent' => 'ringTotalNum'),
                'consultNum' => array('text' => 'consult Num', 'parent' => ''),
                'consultTotalSecs' => array('text' => 'consult Total Secs', 'parent' => 'consultNum'),
                'consultAvgSecs' => array('text' => 'consult Avg Secs', 'parent' => 'consultNum'),
                'holdNum' => array('text' => 'hold Num', 'parent' => '', 'monitor' => true),
                'hlodTotalSecs' => array('text' => 'hlod Total Secs', 'parent' => 'holdNum'),
                'holdAvgSecs' => array('text' => 'hold Avg Secs', 'parent' => 'holdNum'),
                'conferenceNum' => array('text' => 'conference Num', 'parent' => '', 'monitor' => true),
                'conferenceTotalSecs' => array('text' => 'conference Total Secs', 'parent' => 'conferenceNum'),
                'conferenceAvgSecs' => array('text' => 'conference Avg Secs', 'parent' => 'conferenceNum'),
                'transferTotalNum' => array('text' => 'transfer Total Num', 'parent' => ''),
                'transferedConnNum' => array('text' => 'transfered Conn Num', 'parent' => '', 'monitor' => true),
                'transferedConnTotalSecs' => array('text' => 'transfered Conn Total Secs', 'parent' => 'transferedConnNum'),
                'transferedConnAvgSecs' => array('text' => 'transfered Conn Avg Secs', 'parent' => 'transferedConnNum'),
                'loginTotalNum' => array('text' => 'login Total Num', 'parent' => '', 'monitor' => true),
                'loginTotalSecs' => array('text' => 'login Total Secs', 'parent' => 'loginTotalNum'),
                'readyTotalSecs' => array('text' => 'ready Total Secs', 'parent' => ''),
                'busyTotalNum' => array('text' => 'busy Total Num', 'parent' => '', 'monitor' => true),
                'busyTotalSecs' => array('text' => 'busy Total Secs', 'parent' => 'busyTotalNum'),
                'busyAvgSecs' => array('text' => 'busy Avg Secs', 'parent' => 'busyTotalNum'),
                'busyRate' => array('text' => 'busy Rate', 'parent' => '', 'monitor' => true),
                'workRate' => array('text' => 'work Rate', 'parent' => '', 'monitor' => true),
            ),
        ),
        'agent'=>array(
            'fixed_report'=>array(
                'inboundTotalNum'=>array('text'=>'Agent In Num', 'field'=>'in_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'outboundTotalNum'=>array('text'=>'Out Num', 'field'=>'out_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'outboundConnNum' => array('text' => 'Agent out calls' , 'field' => 'out_calls', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'outboundConnTotalSecs'=>array('text'=>'outbound Conn Total Secs', 'field'=>'out_secs', 'sortable'=>true, 'width'=>110, 'default_show'=> false),
                'outboundConnAvgSecs'=>array('text'=>'outbound Conn Avg Secs', 'field'=>'avg_calls', 'sortable'=>false, 'width'=>120, 'default_show'=> false),
                'connTotalSecs'=>array('text'=>'Conn Secs', 'field'=>'conn_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'connAvgSecs'=>array('text'=>'Avg Connsecs', 'field'=>'avg_connsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'internalConnNum'=>array('text'=>'Internal Num', 'field'=>'internal_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'ringTotalNum'=>array('text'=>'Ring Num', 'field'=>'ring_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'ringTotalSecs'=>array('text'=>'Ring Secs', 'field'=>'ring_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> false),
                'ringAvgSecs'=>array('text'=>'Avg Ringsecs', 'field'=>'avg_ringsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'consultNum'=>array('text'=>'Consult Num', 'field'=>'consult_num', 'sortable'=>true, 'width'=>70, 'default_show'=> true),
                'consultTotalSecs'=>array('text'=>'Consult Secs', 'field'=>'consult_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> false),
                'consultAvgSecs'=>array('text'=>'Avg Consultsecs', 'field'=>'avg_consultsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'holdNum'=>array('text'=>'Hold Num', 'field'=>'hold_num', 'sortable'=>true, 'width'=>70, 'default_show'=> true),
                'hlodTotalSecs'=>array('text'=>'Hold Secs', 'field'=>'hold_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> false),
                'holdAvgSecs'=>array('text'=>'Avg Holdsecs', 'field'=>'avg_holdsecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'conferenceNum'=>array('text'=>'Conference Num', 'field'=>'conference_num', 'sortable'=>true, 'width'=>60, 'default_show'=> true),
                'conferenceTotalSecs'=>array('text'=>'Conference Secs', 'field'=>'conference_secs', 'sortable'=>true, 'width'=>80, 'default_show'=> false),
                'conferenceAvgSecs'=>array('text'=>'Avg Consecs', 'field'=>'avg_consecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'transferTotalNum'=>array('text'=>'Shift Num', 'field'=>'shift_num', 'sortable'=>true, 'width'=>70, 'default_show'=> true),
                'waitTotalSecs'=>array('text'=>'wait Total Secs', 'field'=>'wait_secs', 'sortable'=>true, 'width'=>110, 'default_show'=> false),
                'waitTotalNum'=>array('text'=>'wait Total Num', 'field'=>'wait_num', 'sortable'=>true, 'width'=>110, 'default_show'=> false),
                'waitAvgSecs'=>array('text'=>'Avg Waitsecs', 'field'=>'avg_waitsecs', 'sortable'=>false, 'width'=>120, 'default_show'=> true),
                'loginTotalSecs'=>array('text'=>'Login Secs', 'field'=>'login_secs', 'sortable'=>true, 'width'=>90, 'default_show'=> true),
                'readyTotalSecs'=>array('text'=>'Ready Secs', 'field'=>'ready_secs', 'sortable'=>true, 'width'=>90, 'default_show'=> true),
                'busyTotalSecs'=>array('text'=>'Busy Secs', 'field'=>'busy_secs', 'sortable'=>true, 'width'=>90, 'default_show'=> true),
                'busyRate'=>array('text'=>'Busy Rate', 'field'=>'busy_rate', 'sortable'=>false, 'width'=>90, 'default_show'=> true),
                'workRate'=>array('text'=>'Work Rate', 'field'=>'work_rate', 'sortable'=>false, 'width'=>90, 'default_show'=> true),
                'evaluate_-4'=>array('text'=>'Evaluate-4', 'field'=>'evaluate_-4', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'evaluate_-3'=>array('text'=>'Evaluate-3', 'field'=>'evaluate_-3', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'evaluate_-1'=>array('text'=>'Evaluate-1', 'field'=>'evaluate_-1', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'evaluate_0'=>array('text'=>'Evaluate0', 'field'=>'evaluate_0', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_1'=>array('text'=>'Evaluate1', 'field'=>'evaluate_1', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_2'=>array('text'=>'Evaluate2', 'field'=>'evaluate_2', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_3'=>array('text'=>'Evaluate3', 'field'=>'evaluate_3', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_4'=>array('text'=>'Evaluate4', 'field'=>'evaluate_4', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_5'=>array('text'=>'Evaluate5', 'field'=>'evaluate_5', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_6'=>array('text'=>'Evaluate6', 'field'=>'evaluate_6', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_7'=>array('text'=>'Evaluate7', 'field'=>'evaluate_7', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_8'=>array('text'=>'Evaluate8', 'field'=>'evaluate_8', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'evaluate_9'=>array('text'=>'Evaluate9', 'field'=>'evaluate_9', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'refuse_num'=>array('text'=>'Refuse Num', 'field'=>'refuse_num', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
            ),
            'custom_report'=>array(
                'inboundConnNum' => array('text' => 'inbound Conn Num'),
                'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs'),
                'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs'),
                'outboundTotalNum' => array('text' => 'outbound Total Num'),
                'outboundConnNum' => array('text' => 'outbound Conn Num'),
                'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs'),
                'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs'),
                'validConnTotalNum' => array('text' => 'valid Conn Total Num'),
                'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num'),
                'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num'),
                'connTotalNum' => array('text' => 'conn Total Num'),
                'connTotalSecs' => array('text' => 'conn Total Secs'),
                'connAvgSecs' => array('text' => 'conn Avg Secs'),
                'validConnTotalSecs' => array('text' => 'valid Conn Total Secs'),
                'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs'),
                'inboundValidConnTotalSecs' => array('text' => 'inbound Valid Conn Total Secs'),
                'inboundValidConnAvgSecs' => array('text' => 'inbound Valid Conn Avg Secs'),
                'outboundValidConnTotalSecs' => array('text' => 'outbound Valid Conn Total Secs'),
                'outboundValidConnAvgSecs' => array('text' => 'outbound Valid Conn Avg Secs'),
                'internalConnNum' => array('text' => 'internal Conn Num'),
                'internalConnTotalSecs' => array('text' => 'internal Conn Total Secs'),
                'internalConnAvgSecs' => array('text' => 'internal Conn Avg Secs'),
                'waitTotalNum' => array('text' => 'wait Total Num'),
                'waitTotalSecs' => array('text' => 'wait Total Secs'),
                'waitAvgSecs' => array('text' => 'wait Avg Secs'),
                'ringTotalNum' => array('text' => 'ring Total Num'),
                'ringTotalSecs' => array('text' => 'ring Total Secs'),
                'ringAvgSecs' => array('text' => 'ring Avg Secs'),
                'consultNum' => array('text' => 'consult Num'),
                'consultTotalSecs' => array('text' => 'consult Total Secs'),
                'consultAvgSecs' => array('text' => 'consult Avg Secs'),
                'holdNum' => array('text' => 'hold Num'),
                'hlodTotalSecs' => array('text' => 'hlod Total Secs'),
                'holdAvgSecs' => array('text' => 'hold Avg Secs'),
                'conferenceNum' => array('text' => 'conference Num'),
                'conferenceTotalSecs' => array('text' => 'conference Total Secs'),
                'conferenceAvgSecs' => array('text' => 'conference Avg Secs'),
                'transferTotalNum' => array('text' => 'transfer Total Num'),
                'transferedConnNum' => array('text' => 'transfered Conn Num'),
                'transferedConnTotalSecs' => array('text' => 'transfered Conn Total Secs'),
                'transferedConnAvgSecs' => array('text' => 'transfered Conn Avg Secs'),
                'loginTotalNum' => array('text' => 'login Total Num'),
                'loginTotalSecs' => array('text' => 'login Total Secs'),
                'readyTotalSecs' => array('text' => 'ready Total Secs'),
                'busyTotalNum' => array('text' => 'busy Total Num'),
                'busyTotalSecs' => array('text' => 'busy Total Secs'),
                'busyAvgSecs' => array('text' => 'busy Avg Secs'),
                'busyRate' => array('text' => 'busy Rate'),
                'workRate' => array('text' => 'work Rate'),
            ),
        ),
    );

    /**
     * 需要计算的指标项
     * text为指标的名称
     *
     * @var array
     */
    protected $calculateItems = array(
        'system'=>array(
            'inboundRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundTotalNum'),
                ),
                'denominator' => array(
                    'plus'=> array('ivrInboundTotalNum'),//array('ivrInboundNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'inboundConnRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundTotalNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'queueAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('queueTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundTotalNum'),
                ),
            ),
            'ringAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('ringTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('ringTotalNum'),
                ),
            ),
            'inboundConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
            ),
            'workRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnTotalSecs', 'waitTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('loginTotalSecs'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'inboundConnInXSecsRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnInXSecsNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
        ),
        'queue'=>array(
            'inboundAbandonAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundAbandonTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundAbandonTotalNum'),
                ),
            ),
            'inboundConnRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundTotalNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'queueAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('queueTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundTotalNum'),
                ),
            ),
            'ringAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('ringTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('ringTotalNum'),
                ),
            ),
            'inboundConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
            ),
            'callerAbandonAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('callerAbandonTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('callerAbandonTotalNum'),
                ),
            ),
            'timeoutAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('timeoutTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('timeoutTotalNum'),
                ),
            ),
            'fullAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('fullTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('fullTotalNum'),
                ),
            ),
            'agentAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('agentTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('agentTotalNum'),
                ),
            ),
            'inboundConnInXSecsRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnInXSecsNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'inboundAbandonInXSecsRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundAbandonInXSecsNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundTotalNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'outboundConnRate' => array(
                'numerator' => array(
                    'plus'=> array('outboundTotalNum'),
                ),
                'denominator' => array(
                    'plus'=> array('outboundConnTotalNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'outboundAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('outboundSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('outboundTotalNum'),
                ),
            ),
        ),
        'agent'=>array(
            'connAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('connTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundTotalNum', 'outboundTotalNum'),
                ),
            ),
            'outboundConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('outboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('outboundTotalNum'),
                ),
            ),
            'waitAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('waitTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('waitTotalNum'),
                ),
            ),
            'ringAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('ringTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('ringTotalNum'),
                ),
            ),
            'consultAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('consultTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('consultNum'),
                ),
            ),
            'holdAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('hlodTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('holdNum'),
                ),
            ),
            'conferenceAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('conferenceTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('conferenceNum'),
                ),
            ),
            'busyRate' => array(
                'numerator' => array(
                    'plus'=> array('busyTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('loginTotalSecs'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'workRate' => array(
                'numerator' => array(
                    'plus'=> array('connTotalSecs', 'waitTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('loginTotalSecs'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
        ),
        'group'=>array(
            'connAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('connTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundTotalNum', 'outboundTotalNum'),
                ),
            ),
            'outboundConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('outboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('outboundTotalNum'),
                ),
            ),
            'waitAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('waitTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('waitTotalNum'),
                ),
            ),
            'ringAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('ringTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('ringTotalNum'),
                ),
            ),
            'consultAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('consultTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('consultNum'),
                ),
            ),
            'holdAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('hlodTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('holdNum'),
                ),
            ),
            'conferenceAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('conferenceTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('conferenceNum'),
                ),
            ),
            'busyRate' => array(
                'numerator' => array(
                    'plus'=> array('busyTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('loginTotalSecs'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'workRate' => array(
                'numerator' => array(
                    'plus'=> array('connTotalSecs', 'waitTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('loginTotalSecs'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
        ),
    );
}
