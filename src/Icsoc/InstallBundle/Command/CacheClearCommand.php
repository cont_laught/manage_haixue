<?php
/**
 * This file is part of easycrm, created by PhpStorm.
 * Author: LouXin
 * Date: 2015/3/17 11:43
 * File: InstallCommand.php
 */

namespace Icsoc\InstallBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Icsoc\InstallBundle\Composer\PermissionsHandler;

class CacheClearCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('icsoc:cache:clear')
            ->setDescription('清除系统缓存并修改缓存目录权限')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $container->get('cache_clearer')->clear($container->get('kernel')->getCacheDir());
        $permission = new PermissionsHandler();
        $permission->setPermissions($container->get('kernel')->getCacheDir());
        $output->writeln('<info>Clear all the cache.</info>');
    }
}
