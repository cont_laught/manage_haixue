-- MySQL dump 10.13  Distrib 5.6.22, for Linux (x86_64)
--
-- Host: 192.168.1.36    Database: ccod
-- ------------------------------------------------------
-- Server version	5.1.73-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `ccod`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `ccod` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ccod`;

--
-- Temporary view structure for view `坐席监控`
--


/*!50001 DROP VIEW IF EXISTS `坐席监控`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `坐席监控` AS SELECT 
 1 AS `id`,
 1 AS `vcc_id`,
 1 AS `ag_id`,
 1 AS `ag_num`,
 1 AS `ag_name`,
 1 AS `ag_sta`,
 1 AS `ag_sta_reason`,
 1 AS `ag_sta_time`,
 1 AS `ag_sta_time_db`,
 1 AS `pho_id`,
 1 AS `pho_num`,
 1 AS `pho_sta`,
 1 AS `pho_sta_reason`,
 1 AS `pho_sta_time`,
 1 AS `pho_sta_time_db`,
 1 AS `login_ip`,
 1 AS `time_firlogin`,
 1 AS `time_login`,
 1 AS `secs_login`,
 1 AS `secs_ready`,
 1 AS `secs_busy`,
 1 AS `secs_call`,
 1 AS `secs_ring`,
 1 AS `secs_wait`,
 1 AS `times_call`,
 1 AS `times_busy`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `时长统计记录`
--


/*!50001 DROP VIEW IF EXISTS `时长统计记录`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `时长统计记录` AS SELECT 
 1 AS `id`,
 1 AS `企业`,
 1 AS `技能组`,
 1 AS `坐席`,
 1 AS `呼叫ID`,
 1 AS `类型`,
 1 AS `附加类型`,
 1 AS `开始时间`,
 1 AS `结束时间`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `量统计记录`
--


/*!50001 DROP VIEW IF EXISTS `量统计记录`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `量统计记录` AS SELECT 
 1 AS `id`,
 1 AS `企业`,
 1 AS `技能组`,
 1 AS `类型`,
 1 AS `时间`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ac_cdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `ac_cdr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `vcc_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '企业ID',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `pro_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '项目ID',
  `task_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '任务ID',
  `caller` varchar(64) NOT NULL COMMENT '主叫',
  `called` varchar(64) NOT NULL COMMENT '被叫',
  `record_mark` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `record_file` varchar(256) NOT NULL DEFAULT '' COMMENT '录音地址',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `ring_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '振铃时间',
  `ans_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '应答时间',
  `conn_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '接通时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `bill_sec` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '计费时长',
  `all_sec` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总时长',
  `result` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '呼叫结果',
  `asr_int` smallint(6) NOT NULL DEFAULT '0' COMMENT 'ASR识别结果',
  `asr_str` varchar(64) NOT NULL DEFAULT '' COMMENT 'ASR识别关键字',
  `start_year` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '年份',
  `start_month` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '月份',
  `start_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `start_qua` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '季度',
  `start_week` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '周',
  `start_hour` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '小时',
  `start_minute` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分钟',
  `data_source` int(11) NOT NULL COMMENT '数据来源',
  `plan_id` int(11) NOT NULL COMMENT '批次号',
  `task_cusresult` varchar(150) NOT NULL COMMENT '按键结果',
  `date_time` time NOT NULL COMMENT '时间',
  `is_billed` tinyint(1) NOT NULL COMMENT '是否已计费（0 未计费 1已计费）',
  `con_bill` float(10,3) NOT NULL DEFAULT '0.000' COMMENT '是否已计费（0 未计费 1已计费）',
  `is_processed` tinyint(1) NOT NULL COMMENT '是否已整理（0 未整理 1已整理）',
  ` is_adapter` tinyint(2) NOT NULL COMMENT '是否是转电话（0 不是 1是）',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`),
  KEY `call_id` (`call_id`),
  KEY `pro_id` (`pro_id`,`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='自动外呼通话记录表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `win_accdr_time` BEFORE INSERT ON `ac_cdr` FOR EACH ROW BEGIN
SET @now = FROM_UNIXTIME(NEW.start_time);
SET NEW.start_year = DATE_FORMAT(@now,'%Y');
SET NEW.start_month = DATE_FORMAT(@now,'%m');
SET NEW.start_week = DATE_FORMAT(@now,'%U');
SET NEW.start_day = DATE_FORMAT(@now,'%d');
SET NEW.start_qua =  EXTRACT(QUARTER FROM @now);
SET NEW.start_hour = DATE_FORMAT(@now,'%H');
SET NEW.start_minute= DATE_FORMAT(@now,'%i');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ac_project`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `ac_project` (
  `pro_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '项目ID',
  `vcc_id` int(10) unsigned NOT NULL COMMENT '企业ID',
  `pro_name` varchar(64) NOT NULL COMMENT '项目名称',
  `pro_task` varchar(64) NOT NULL COMMENT '项目任务表',
  `pro_caller` varchar(64) NOT NULL COMMENT '外呼主叫',
  `pro_prefix` varchar(32) NOT NULL COMMENT '外呼被叫前缀',
  `pro_lineuse` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外呼线路数',
  `pro_linereserve` int(10) unsigned NOT NULL DEFAULT '20' COMMENT '线路保留数',
  `pro_priority` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '项目优先级',
  `pro_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '项目状态',
  `pro_type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '呼叫类型',
  `pro_context` varchar(64) NOT NULL COMMENT '接通流程',
  `pro_exten` varchar(64) NOT NULL COMMENT '接通流程',
  `pro_ctimeout` tinyint(3) unsigned NOT NULL DEFAULT '30' COMMENT '呼叫超时时长',
  `pro_queid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '转坐席技能组',
  `pro_channel` tinyint(4) NOT NULL DEFAULT '0' COMMENT '外呼通道',
  `pro_cusflag` int(10) unsigned NOT NULL DEFAULT '7' COMMENT '功能标志',
  `pro_maxrate` float unsigned NOT NULL DEFAULT '5' COMMENT '最大呼叫比例',
  `remark` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pro_id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='外呼项目表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ac_task_test`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `ac_task_test` (
  `task_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `vcc_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '企业ID',
  `pro_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '项目ID',
  `pro_record` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '项目任务ID',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `priority` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '任务优先级',
  `task_num` varchar(32) NOT NULL COMMENT '外呼号码',
  `task_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '未呼叫，呼叫中，完成等',
  `task_priority` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '越大优先级越高',
  `task_result` tinyint(3) unsigned NOT NULL COMMENT '0接通，其他未接通',
  `task_cusresult` char(1) NOT NULL DEFAULT '' COMMENT '业务结果(按键)',
  `task_cusresult2` char(1) NOT NULL DEFAULT '' COMMENT '二层按键结果',
  `task_load_user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `insert_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '插入时间',
  `effect_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '生效时间',
  `valid_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '有效时间',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `ans_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '应答时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `task_customint` int(11) NOT NULL DEFAULT '0' COMMENT '自定义参数',
  `task_customstr` varchar(512) NOT NULL COMMENT '自定义参数',
  `cus_callid` varchar(128) NOT NULL COMMENT '电话编号',
  `cus_agent` varchar(128) NOT NULL COMMENT '坐席ID',
  `cus_queue` varchar(128) NOT NULL COMMENT '组名',
  `recall_mark` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '重呼标志位',
  `port_id` int(11) NOT NULL COMMENT '数据端口ID',
  PRIMARY KEY (`task_id`),
  KEY `vcc_id_2` (`vcc_id`,`task_state`,`pro_id`,`port_id`,`insert_time`),
  KEY `vcc_id` (`pro_id`,`task_state`,`insert_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='华硕外呼任务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_accounts`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_accounts` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) NOT NULL COMMENT '公司名称',
  `superior_agent` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级代理商',
  `user_account` varchar(30) NOT NULL COMMENT '用户账号',
  `province` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '省份',
  `city` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '市',
  `county` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '地区',
  `address` varchar(100) NOT NULL COMMENT '地址',
  `represention` varchar(20) NOT NULL COMMENT '法人代表',
  `contactor` varchar(20) NOT NULL COMMENT '联系人',
  `contact_phone` varchar(20) NOT NULL COMMENT '联系人电话',
  `office_phone` varchar(20) NOT NULL COMMENT '办公电话',
  `contact` varchar(30) NOT NULL COMMENT '联系人',
  `mobile_phone` varchar(20) NOT NULL COMMENT '移动电话',
  `fax` varchar(20) NOT NULL COMMENT '传真',
  `post_code` char(6) NOT NULL COMMENT '邮政编码',
  `email` varchar(50) NOT NULL COMMENT '电子邮件',
  `aptitude1` tinyint(1) NOT NULL COMMENT '资质1',
  `aptitude2` tinyint(1) NOT NULL COMMENT '资质2',
  `aptitude3` tinyint(1) NOT NULL COMMENT '资质3',
  `aptitude4` tinyint(1) NOT NULL COMMENT '资质4',
  `add_time` int(11) NOT NULL COMMENT '添加时间',
  `company_address` varchar(250) NOT NULL COMMENT '公司地址',
  `if_bind` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已绑定虚拟呼叫中心',
  `open_time` datetime NOT NULL COMMENT '开通时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户账户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_admins`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_admins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `agent_grade` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '代理商级别',
  `agent_account` varchar(30) NOT NULL COMMENT '代理商账号',
  `company_name` varchar(50) NOT NULL COMMENT '公司名称',
  `account` varchar(20) NOT NULL COMMENT '登录账号',
  `password` char(32) NOT NULL COMMENT '登录密码',
  `superior_agent` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级代理商',
  `agent_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '代理商状态',
  `province` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '省份',
  `city` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '市',
  `county` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '地区',
  `address` varchar(100) NOT NULL COMMENT '地址',
  `represention` varchar(20) NOT NULL COMMENT '法人代表',
  `contact_phone` varchar(20) NOT NULL COMMENT '联系电话',
  `contactor` varchar(20) NOT NULL COMMENT '联系人',
  `office_phone` varchar(20) NOT NULL COMMENT '办公电话',
  `mobile_phone` varchar(20) NOT NULL COMMENT '移动电话',
  `fax` varchar(20) NOT NULL COMMENT '传真',
  `post_code` char(6) NOT NULL COMMENT '邮政编码',
  `email` varchar(50) NOT NULL COMMENT '电子邮件',
  `aptitude1` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '资质1',
  `aptitude2` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '资质2',
  `aptitude3` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '资质3',
  `aptitude4` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '资质4',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `local_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '市话计费方式（1分钟2前N分钟N元，后每分钟N元）',
  `local_pre_minutes` tinyint(1) unsigned NOT NULL COMMENT '计费方式2中的前N分钟',
  `local_pre_fee` decimal(10,3) NOT NULL COMMENT '计费方式2中的前N分钟N元',
  `remark` varchar(250) NOT NULL COMMENT '备注',
  `local_after_rate` decimal(10,3) NOT NULL COMMENT '计费方式2中的后每分钟N元',
  `local_rate` decimal(10,3) NOT NULL COMMENT '市话费率',
  `long_rate` decimal(10,3) NOT NULL COMMENT '长途费率',
  `long_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '长途计费方式（1分钟2前N分钟N元，后每分钟N元）',
  `long_pre_minutes` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '计费方式2中的前N分钟',
  `long_pre_fee` decimal(10,3) NOT NULL COMMENT '计费方式2中的前N分钟N元',
  `long_after_rate` decimal(10,3) NOT NULL COMMENT '计费方式2中的后每分钟N元',
  `special_rate` decimal(10,3) NOT NULL COMMENT '特殊费率',
  `special_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '特殊计费方式（1分钟2前N分钟N元，后每分钟N元）',
  `special_pre_minutes` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '计费方式2中的前N分钟',
  `special_pre_fee` decimal(10,3) NOT NULL COMMENT '计费方式2中的前N分钟N元',
  `special_after_rate` decimal(10,3) NOT NULL COMMENT '计费方式2中的后每分钟N元',
  `phone_monthly` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '号码月租',
  `agent_monthly` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '坐席月租',
  `trunk_monthly` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '中继月租',
  `last_login` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次登录时间',
  `last_ip` varchar(15) NOT NULL COMMENT '上次登录IP',
  `action_list` text NOT NULL COMMENT '权限列表',
  `system_admin` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有系统权限',
  PRIMARY KEY (`id`),
  KEY `agent_status` (`agent_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代理商信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_announcement`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_announcement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `system_type` tinyint(1) unsigned NOT NULL,
  `add_time` int(11) unsigned NOT NULL,
  `visittimes` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='ϵͳ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_blacklist`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_blacklist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL COMMENT '企业ID',
  `trunk_num` varchar(20) NOT NULL COMMENT '中继号码，空代表全部',
  `phone_num` varchar(20) NOT NULL COMMENT '黑名单号码',
  `black_type` tinyint(1) NOT NULL COMMENT '1、表示固定号码 2、表示模糊查询',
  `group_id` int(10) unsigned NOT NULL COMMENT '所属区域id',
  `call_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1呼入,2呼出',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`,`phone_num`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='黑名单号码表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_callerloc`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_callerloc` (
  `num` int(11) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `cardtype` varchar(2) DEFAULT NULL,
  `code` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`num`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_ccod_configs`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_ccod_configs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '企业ID',
  `is_enable_black` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用呼出黑名单功能（0未启用1已启用）',
  `is_incoming_enable_black` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否启用呼入黑名单功能（0未启用1已启用）',
  `is_enable_white` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否启用白名单功能（0未启用1已启用）',
  `white_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '白名单类型（1白名单优先2只允许白名单）',
  `white_sound` varchar(100) NOT NULL DEFAULT '' COMMENT '非白名单提示语音',
  `white_sound_address` varchar(100) NOT NULL DEFAULT '' COMMENT '通信服务器语音存放目录',
  `is_enable_group` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用业务组功能（0未启用1已启用）',
  `conn_num` varchar(30) NOT NULL DEFAULT '' COMMENT 'N秒接通量',
  `lost_num` varchar(30) NOT NULL DEFAULT '' COMMENT 'N秒放弃量统计',
  `work_hour` tinyint(4) NOT NULL DEFAULT '24' COMMENT '工作制小时',
  `work_day` tinyint(4) NOT NULL DEFAULT '30' COMMENT '工作制天',
  `tool_bar` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启toolBar参数设置(1 开启,0禁用)',
  `statistical` tinyint(1) DEFAULT '0' COMMENT '是否开启统计参数(1,开启，0，禁用)',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='企业设置信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_ccods`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_ccods` (
  `vcc_id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_code` varchar(30) NOT NULL COMMENT '虚拟呼叫中心代码',
  `admin_name` varchar(30) NOT NULL COMMENT '管理账号名称',
  `admin_password` char(32) NOT NULL COMMENT '管理账号密码',
  `remote_password` char(32) NOT NULL DEFAULT 'e10adc3949ba59abbe56e057f20f883e' COMMENT '远程请求密码',
  `auth_key` char(10) NOT NULL DEFAULT '' COMMENT '秘钥',
  `ip_limit` varchar(20) NOT NULL DEFAULT '' COMMENT 'ip限定',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态0未启用1启用2暂停3禁用',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `superior_agent` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级代理商',
  `phone` varchar(20) NOT NULL COMMENT '绑定的中继号码',
  `if_bind` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否绑定(0否1是)',
  `phone400` varchar(15) NOT NULL COMMENT '绑定的400号码',
  `company_name` varchar(100) NOT NULL COMMENT '公司名称',
  `caller_num` varchar(20) NOT NULL COMMENT '设置显示的主叫号码',
  `system_name` varchar(100) NOT NULL COMMENT '系统名称',
  `system_logo` varchar(200) NOT NULL COMMENT '系统logo',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否使用默认评价语音（1是0否）',
  `evaluate_sound` varchar(100) NOT NULL COMMENT '满意度评价语音',
  `system_sound` varchar(100) NOT NULL COMMENT '通信服务器语音地址',
  `if_out` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用外呼',
  `user_account` varchar(30) NOT NULL COMMENT '用户账号',
  `groups` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '技能组数(0为不限制)',
  `agents` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '坐席数',
  `agent_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `out_flat` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '外呼费率',
  `agent_logins` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '最多登录数',
  `agent_fee` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '坐席功能费',
  `trunk_in` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '呼入中继总数',
  `trunk_out` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '呼出中继数',
  `trunk_fee` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '呼入中继月租费',
  `phone_nums` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '号码总数',
  `phone_fee` decimal(10,2) NOT NULL COMMENT '号码总月租',
  `ivr_lines` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'ivr线路数',
  `balance` decimal(10,3) NOT NULL DEFAULT '0.000',
  `overdraft` decimal(10,3) NOT NULL DEFAULT '100.000' COMMENT '透支额度',
  `alarm_phone` varchar(80) NOT NULL COMMENT '余额不足时报警号码（短信，电话）',
  `if_alarm` tinyint(1) NOT NULL DEFAULT '127' COMMENT '是否已发生（100已发送低于100提醒、-100正在发送中，其他类似）',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '特权级别',
  `endsms_type` int(10) unsigned NOT NULL COMMENT '通话结束短信类型0无1未接发送2接听发送3全部发送',
  `endsms_phone` varchar(20) NOT NULL COMMENT '结束短信固定号码，空为不指定',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  `record_lines` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '录音路数',
  `record_fee` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '录音费用',
  `ivr_fee` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'ivr费用',
  `fee_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '计费方式（1后付费2预付费）',
  `open_date` date NOT NULL COMMENT '开通时间',
  `due_date` date NOT NULL COMMENT '到期时间',
  `remark` varchar(100) NOT NULL COMMENT '备注',
  `index_page` varchar(150) NOT NULL COMMENT '首页地址',
  `inpop_page` varchar(150) NOT NULL COMMENT '来电弹屏地址',
  `outpop_page` varchar(150) NOT NULL COMMENT '去电弹屏地址',
  `action_list` text NOT NULL COMMENT 'Ȩ',
  `win_ip` varchar(15) NOT NULL COMMENT '通信服务器IP',
  `sms_rate` decimal(10,2) unsigned NOT NULL DEFAULT '0.10' COMMENT '短信费率',
  `sms_channel` tinyint(3) unsigned DEFAULT '0' COMMENT '信短通道：1、红叶通道 2、漫道通道 3、曼联95',
  `synchronous_call` tinyint(1) NOT NULL DEFAULT '0' COMMENT '通话更新标记',
  `synchronous_sms` tinyint(1) NOT NULL DEFAULT '0' COMMENT '短信更新标记',
  `synchronous_agent` tinyint(1) NOT NULL DEFAULT '0' COMMENT '坐席更新标记',
  `synchronous_queue` tinyint(1) NOT NULL DEFAULT '0' COMMENT '队列更新标记',
  `synchronous_lostcall` tinyint(1) NOT NULL DEFAULT '0' COMMENT '未接来电更新标记',
  `db_main_ip` varchar(20) DEFAULT '' COMMENT '主数据库IP',
  `db_slave_ip` varchar(20) DEFAULT '' COMMENT '从数据库IP',
  `db_name` varchar(50) DEFAULT '' COMMENT '数据库名称',
  `db_user` varchar(20) DEFAULT '' COMMENT '据库数用户名',
  `db_password` varchar(50) DEFAULT '' COMMENT '数据库密码',
  `db_system` enum('','ekt','easycall') DEFAULT '',
  `role_action` text COMMENT '功能权限',
  `sync_call` tinyint(1) NOT NULL DEFAULT '0',
  `sync_sms` tinyint(1) NOT NULL DEFAULT '0',
  `sync_agent` tinyint(1) NOT NULL DEFAULT '0',
  `sync_que` tinyint(1) NOT NULL DEFAULT '0',
  `sync_lostcall` tinyint(1) NOT NULL DEFAULT '0',
  `is_effective_rule` tinyint(1) NOT NULL DEFAULT '1' COMMENT '生效的日程',
  `business_type` tinyint(1) unsigned NOT NULL COMMENT '业务类型（1爱卡汽车2DM）',
  `trunk_limit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '中继数限制',
  `user_limit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户数限制',
  `task_limit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '任务数限制',
  `rates` float(10,3) NOT NULL DEFAULT '0.000' COMMENT '费率',
  `function_fee` float(10,3) NOT NULL DEFAULT '0.000' COMMENT '功能费',
  `monthly_fee` float(10,3) NOT NULL DEFAULT '0.000' COMMENT '月租费',
  `is_card` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'DM开通方式：1代表正常开通，2代表充值卡开通',
  `if_total_record` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否开通全程录音：1不是，2是',
  PRIMARY KEY (`vcc_id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `vcc_code` (`vcc_code`) USING BTREE,
  KEY `business_type` (`business_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='虚拟呼叫中心信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_cti_change_logs`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_cti_change_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '企业id',
  `log_type` enum('ivr','sound') NOT NULL COMMENT '日志类型',
  `log_action` enum('create','update','delete') NOT NULL COMMENT '操作',
  `table_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '对应的表的记录ID',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='中间件ivr、语音更新日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_logs`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_num` varchar(60) NOT NULL,
  `vcc_id` int(11) NOT NULL COMMENT '企业id',
  `vcc_code` varchar(30) NOT NULL COMMENT '企业代码',
  `action` tinyint(1) NOT NULL COMMENT '操作类型（1-login 2-logout 3-add 4-update 5-delete）',
  `content` varchar(200) NOT NULL COMMENT '操作内容',
  `acttime` int(10) NOT NULL COMMENT '操作时间',
  `actip` varchar(15) NOT NULL COMMENT '操作IP',
  PRIMARY KEY (`log_id`),
  KEY `vcc_id` (`vcc_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统操作日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_monitor_config`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_monitor_config` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(8) unsigned NOT NULL COMMENT '企业id',
  `user_id` int(8) unsigned NOT NULL COMMENT '用户id',
  `monitor_queues` text COMMENT '监控的技能组，多个技能组之间逗号分隔',
  `separate_queues` text COMMENT '单独监控显示的技能组，多个技能组之间逗号分隔',
  `if_monitor_ivr_info` tinyint(4) DEFAULT '0' COMMENT '是否显示IVR监控信息1(显示)0(不显示)',
  `trunk_phones` text COMMENT 'ivr监控所属的中继号码',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `vccid_user_id` (`user_id`,`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='坐席监控页面添加权限和配置功能';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_phone400s`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_phone400s` (
  `phone_id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `phone_type` tinyint(1) NOT NULL COMMENT '号码类型(1系统中继2系统特殊3用户中继4用户特殊)',
  `phone` varchar(20) NOT NULL COMMENT '号码',
  `phone400` varchar(15) NOT NULL COMMENT '特殊号码',
  `phone_ager` varchar(20) NOT NULL DEFAULT '' COMMENT '坐席显示主叫',
  `display_type` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '外呼显示方式 1中继 2400 3中继和400',
  `vcc_code` varchar(50) NOT NULL COMMENT '企业代码',
  `monthly_fee` decimal(10,2) NOT NULL COMMENT '月租费',
  `local_rate` decimal(10,3) NOT NULL COMMENT '市话费率',
  `local_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '市话计费方式（1分钟2前N分钟N元，后每分钟N元）',
  `local_pre_minutes` tinyint(1) unsigned NOT NULL COMMENT '计费方式2中的前N分钟',
  `local_pre_fee` decimal(10,3) NOT NULL COMMENT '计费方式2中的前N分钟N元',
  `local_after_rate` decimal(10,3) NOT NULL COMMENT '计费方式2中的后每分钟N元',
  `long_rate` decimal(10,3) NOT NULL COMMENT '长途费率',
  `long_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '长途计费方式（1分钟2前N分钟N元，后每分钟N元）',
  `long_pre_minutes` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '计费方式2中的前N分钟',
  `long_pre_fee` decimal(10,3) NOT NULL COMMENT '计费方式2中的前N分钟N元',
  `long_after_rate` decimal(10,3) NOT NULL COMMENT '计费方式2中的后每分钟N元',
  `special_rate` decimal(10,3) NOT NULL COMMENT '特殊费率',
  `special_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '特殊计费方式（1分钟2前N分钟N元，后每分钟N元）',
  `special_pre_minutes` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '计费方式2中的前N分钟',
  `special_pre_fee` decimal(10,3) NOT NULL COMMENT '计费方式2中的前N分钟N元',
  `special_after_rate` decimal(10,3) NOT NULL COMMENT '计费方式2中的后每分钟N元',
  `if_minimum` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否开通最低消费',
  `enable_date` date NOT NULL COMMENT '生效日期',
  `minimum_fee` decimal(10,2) NOT NULL COMMENT '最低消费',
  `trunk_in` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼入中继数',
  `trunk_out` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼出中继数',
  `if_bind` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否绑定',
  `fee_flat` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '费率',
  `add_time` datetime NOT NULL COMMENT '添加时间',
  `bind_time` datetime NOT NULL COMMENT '绑定时间',
  `phone_status`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '号码状态：0,正常号码;1,挂起待删除',
  `remove_date`  date NOT NULL COMMENT '预删除时间',
  PRIMARY KEY (`phone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='虚拟呼叫中心号码表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_roles`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(30) NOT NULL DEFAULT '',
  `role_grade` tinyint(1) NOT NULL DEFAULT '0',
  `action_list` text NOT NULL,
  `group_id` int(10) unsigned DEFAULT NULL COMMENT '区域ID',
  PRIMARY KEY (`role_id`),
  KEY `vcc_id` (`vcc_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_sessions`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_whitelist`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `cc_whitelist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL DEFAULT '0',
  `trunk_num` varchar(20) NOT NULL COMMENT '中继号码，空代表全部',
  `phone_num` varchar(20) NOT NULL COMMENT '白名单号码',
  PRIMARY KEY (`id`),
  KEY `phone_num` (`phone_num`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='白名单（VIP）号码表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_client`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_client` (
  `cle_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '客户ID  系统自增主键',
  `vcc_id` int(11) NOT NULL COMMENT '公司ID',
  `cle_name` varchar(50) NOT NULL COMMENT '客户姓名',
  `phone` varchar(20) NOT NULL COMMENT '主要联系电话',
  `phone2` varchar(20) NOT NULL COMMENT '电话',
  `phone3` varchar(20) NOT NULL DEFAULT '' COMMENT '办公电话',
  `phone4` varchar(20) NOT NULL DEFAULT '' COMMENT '家庭电话',
  `province` int(11) NOT NULL COMMENT '省份',
  `city` int(11) NOT NULL COMMENT '市',
  `area` varchar(30) NOT NULL COMMENT '区域： 省、市',
  `address` varchar(80) NOT NULL COMMENT '详细地址',
  `contacter` varchar(20) NOT NULL COMMENT '联系人',
  `sex` varchar(5) NOT NULL COMMENT '性别',
  `qq` varchar(20) NOT NULL COMMENT 'QQ号码',
  `msn` varchar(50) NOT NULL COMMENT 'MSN',
  `email` varchar(50) NOT NULL COMMENT 'E-mail',
  `remark` text NOT NULL COMMENT '备注',
  `creater` varchar(20) NOT NULL COMMENT '创建人',
  `creater_num` varchar(30) NOT NULL COMMENT '创建人工号',
  `creattime` varchar(30) NOT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志  0未删除    1已删除',
  `stu_1` varchar(50) DEFAULT NULL COMMENT '自定义字段1',
  `stu_2` varchar(50) DEFAULT NULL COMMENT '自定义字段2',
  `stu_3` varchar(50) DEFAULT NULL COMMENT '自定义字段3',
  `stu_4` varchar(50) DEFAULT NULL COMMENT '自定义字段4',
  `stu_5` varchar(50) DEFAULT NULL COMMENT '自定义字段5',
  `stu_6` varchar(50) DEFAULT NULL COMMENT '自定义字段6',
  `stu_7` varchar(50) DEFAULT NULL COMMENT '自定义字段7',
  `stu_8` varchar(50) DEFAULT NULL COMMENT '自定义字段8',
  `stu_9` varchar(50) DEFAULT NULL COMMENT '自定义字段9',
  `stu_10` varchar(50) DEFAULT NULL COMMENT '自定义字段10',
  `stu_11` varchar(50) DEFAULT NULL COMMENT '自定义字段11',
  `stu_12` varchar(50) DEFAULT NULL COMMENT '自定义字段12',
  `stu_13` varchar(50) DEFAULT NULL COMMENT '自定义字段13',
  `stu_14` varchar(50) DEFAULT NULL COMMENT '自定义字段14',
  `stu_15` varchar(50) DEFAULT NULL COMMENT '自定义字段15',
  PRIMARY KEY (`cle_id`),
  KEY `vcc_id` (`vcc_id`,`is_del`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='客户管理';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_creattime` BEFORE INSERT ON `est_client` FOR EACH ROW begin
set new.creattime=now();
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_record_cle_name` AFTER UPDATE ON `est_client` FOR EACH ROW BEGIN
IF old.cle_name !=  new.cle_name
THEN
update est_record set cle_name =new.cle_name  where cle_id=new.cle_id;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `est_field_confirm`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_field_confirm` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统id',
  `name` varchar(50) DEFAULT '' COMMENT '可配置字段名称',
  `fields` varchar(30) DEFAULT '' COMMENT '数据表字段名称',
  `type` tinyint(1) DEFAULT NULL COMMENT '1是客户的自定义字段2联系记录自定义字段',
  `stat` tinyint(1) DEFAULT NULL COMMENT '字段状态0是未激活1是激活',
  `if_write` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否可手动输入0不可以1可以',
  `class` tinyint(1) NOT NULL DEFAULT '2' COMMENT '字段类型:1.基础信息；2，自定义信息',
  `vcc_id` varchar(50) DEFAULT NULL COMMENT '公司id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_field_confirm_detail`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_field_confirm_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统id',
  `p_id` int(22) NOT NULL COMMENT '父节点索引',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '选项里对应的字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_fields`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统自增id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '列表显示名称',
  `display_name` varchar(20) NOT NULL COMMENT '短信模板字段显示名称',
  `fields` varchar(30) NOT NULL DEFAULT '' COMMENT '字段名称',
  `if_use` int(1) NOT NULL DEFAULT '0' COMMENT '是否启用0不显示，1显示',
  `poz` int(2) NOT NULL COMMENT '排序',
  `state` tinyint(1) NOT NULL COMMENT '字段是否可用0不可用1可用',
  `class` tinyint(1) NOT NULL DEFAULT '2' COMMENT '字段类型：1，客户基础信息字段，2客户自定义字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='客户表字段';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_hangupsms`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_hangupsms` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `caller` varchar(20) NOT NULL COMMENT '主叫号码',
  `called` varchar(20) NOT NULL DEFAULT '' COMMENT '被叫号码',
  `conn_time` int(11) NOT NULL COMMENT '来电时间',
  `send_type` char(1) DEFAULT '' COMMENT '0接通非0未接通',
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `vcc_code` varchar(30) NOT NULL COMMENT '企业代码',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '短信状态 0：未发送；1：发送成功 2：发送失败',
  `result` varchar(30) NOT NULL DEFAULT '' COMMENT '短信结果',
  `sent_time` int(11) NOT NULL COMMENT '短信发送时间',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:给坐席发送短信；1：给客户发送短信',
  PRIMARY KEY (`id`),
  KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='挂机短信预发号码表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_knowledge_article`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_knowledge_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '内容id',
  `vcc_id` int(10) NOT NULL COMMENT '公司id',
  `list_id` int(11) NOT NULL,
  `update_person` varchar(10) NOT NULL COMMENT '修改人',
  `boutique` tinyint(2) NOT NULL DEFAULT '2' COMMENT '1为精品2为普通',
  `update_date` int(11) NOT NULL COMMENT '修改时间',
  `create_person` varchar(10) NOT NULL COMMENT '创建人',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `content` longtext NOT NULL COMMENT '内容',
  `title` varchar(30) NOT NULL DEFAULT ''' ''' COMMENT '标题',
  `click_rate` int(11) NOT NULL DEFAULT '0' COMMENT '点击率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_knowledge_class`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_knowledge_class` (
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父类id',
  `name` varchar(20) NOT NULL DEFAULT ''' ''' COMMENT '知识库名称',
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '知识库id',
  `vcc_id` int(10) NOT NULL COMMENT '公司id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_list_confirm`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_list_confirm` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统自增id',
  `name` varchar(30) NOT NULL COMMENT '列表显示名称',
  `fields` varchar(30) NOT NULL COMMENT '字段名称',
  `if_use` int(1) NOT NULL DEFAULT '0' COMMENT '是否启用0不显示，1显示',
  `poz` int(2) NOT NULL DEFAULT '0' COMMENT '排序',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否可用0不可用1可用',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '列表类型1客户2联系记录',
  `vcc_id` int(11) NOT NULL COMMENT '公司id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='显示列表设置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_nbattribute`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_nbattribute` (
  `id` int(11) NOT NULL DEFAULT '0',
  `num` int(11) DEFAULT '0',
  `code` varchar(5) DEFAULT '',
  `city` varchar(20) DEFAULT '',
  `cardtype` varchar(20) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `num` (`num`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_operation_log`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_operation_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) DEFAULT NULL,
  `table_name` varchar(50) DEFAULT '',
  `operation` varchar(100) DEFAULT '',
  `oper_date` varchar(20) DEFAULT '',
  `ag_id` int(11) DEFAULT '0',
  `ip_address` varchar(50) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_phone_location`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_phone_location` (
  `num` int(11) NOT NULL COMMENT '段号',
  `code` varchar(10) DEFAULT '' COMMENT '区号',
  `city` varchar(50) DEFAULT '' COMMENT '城市',
  `cardtype` varchar(50) DEFAULT '',
  PRIMARY KEY (`num`),
  KEY `pl_code` (`code`),
  KEY `pl_city` (`city`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_record`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统id',
  `vcc_id` int(11) NOT NULL COMMENT '企业id',
  `callid` int(11) NOT NULL COMMENT '录音id',
  `cle_id` int(11) NOT NULL COMMENT '客户id',
  `cle_name` varchar(50) NOT NULL COMMENT '客户名',
  `phone` varchar(20) NOT NULL COMMENT '电话',
  `content` text NOT NULL COMMENT '记录内容',
  `ag_id` int(11) NOT NULL COMMENT '坐席id',
  `ag_num` varchar(20) NOT NULL COMMENT '坐席工号',
  `record_time` varchar(20) NOT NULL COMMENT '记录时间',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '联系记录类型',
  `record_time_int` int(11) NOT NULL COMMENT '记录时间int',
  `conrecord_15` varchar(200) NOT NULL COMMENT '自定义字段15',
  `conrecord_14` varchar(200) NOT NULL COMMENT '自定义字段14',
  `conrecord_13` varchar(200) NOT NULL COMMENT '自定义字段13',
  `conrecord_12` varchar(200) NOT NULL COMMENT '自定义字段12',
  `conrecord_11` varchar(200) NOT NULL COMMENT '自定义字段11',
  `conrecord_10` varchar(200) NOT NULL COMMENT '自定义字段10',
  `conrecord_9` varchar(200) NOT NULL COMMENT '自定义字段9',
  `conrecord_8` varchar(200) NOT NULL COMMENT '自定义字段8',
  `conrecord_7` varchar(200) NOT NULL COMMENT '自定义字段7',
  `conrecord_6` varchar(200) NOT NULL COMMENT '自定义字段6',
  `conrecord_5` varchar(200) NOT NULL COMMENT '自定义字段5',
  `conrecord_4` varchar(200) NOT NULL COMMENT '自定义字段4',
  `conrecord_3` varchar(200) NOT NULL COMMENT '自定义字段3',
  `conrecord_2` varchar(200) NOT NULL COMMENT '自定义字段2',
  `conrecord_1` varchar(200) NOT NULL COMMENT '自定义字段1',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `INSERT_record_time` BEFORE INSERT ON `est_record` FOR EACH ROW BEGIN
 SET new.record_time =now();
SET new.record_time_int = UNIX_TIMESTAMP();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `est_region`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_region` (
  `region_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `region_name` varchar(120) NOT NULL DEFAULT '',
  `region_type` tinyint(1) NOT NULL DEFAULT '2',
  `agency_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`region_id`),
  KEY `parent_id` (`parent_id`),
  KEY `region_type` (`region_type`),
  KEY `agency_id` (`agency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_remind`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_remind` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '醒提自增id',
  `cle_id` int(11) DEFAULT NULL COMMENT '客户id',
  `name` varchar(200) DEFAULT NULL COMMENT '客户姓名',
  `remind_time_int` int(11) DEFAULT NULL COMMENT '提醒时间戳',
  `remind_time` varchar(20) DEFAULT NULL COMMENT '提醒时间',
  `content` text COMMENT '提醒内容',
  `creater` int(11) DEFAULT NULL COMMENT '提醒人',
  `create_time` varchar(20) DEFAULT NULL COMMENT '创建时间',
  `create_name` varchar(20) DEFAULT NULL COMMENT '提醒人id',
  `if_deal` tinyint(1) DEFAULT '0' COMMENT '是否已处理0未处理1已处理',
  `if_sendsms` tinyint(1) DEFAULT NULL COMMENT '是否短信提醒0否1是',
  `vcc_id` int(11) NOT NULL COMMENT '公司id',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标记0未删除1已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_sessions`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_sessions` (
  `sesskey` char(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `expiry` int(10) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ip` char(15) NOT NULL DEFAULT '',
  `user_name` varchar(60) NOT NULL,
  `data` char(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`sesskey`),
  KEY `expiry` (`expiry`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_sessions_data`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_sessions_data` (
  `sesskey` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `expiry` int(11) unsigned NOT NULL DEFAULT '0',
  `data` longtext NOT NULL,
  PRIMARY KEY (`sesskey`),
  KEY `expiry` (`expiry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_sms`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_sms` (
  `ser_id` int(11) NOT NULL auto_increment COMMENT '服务器短信id',
  `client_sms_id` int(11) NOT NULL COMMENT '客户短信id',
  `sender_id` int(11) NOT NULL COMMENT '发送人id',
  `sender_name` varchar(30) NOT NULL COMMENT '发送人姓名',
  `receiver_id` int(11) NOT NULL COMMENT '接收客户id',
  `receiver_name` varchar(30) NOT NULL COMMENT '接收客户姓名',
  `receiver_phone` varchar(15) NOT NULL COMMENT '接收人的短信号码',
  `send_time` datetime NOT NULL COMMENT '发送时间',
  `send_time_int` int(11) NOT NULL COMMENT '发送时间戳',
  `sms_contents` text NOT NULL COMMENT '短信内容',
  `result_code` int(11) NOT NULL default '0' COMMENT '结果代码  默认是0，成功1，失败2',
  `result` varchar(100) NOT NULL COMMENT '结果原因',
  `vcc_code` varchar(64) NOT NULL COMMENT 'vcc_code',
  `vcc_id` int(11) NOT NULL COMMENT '公司id',
  `authority` int(11) NOT NULL COMMENT '短信发送优先级，1的级别最高',
  `synchronous_sms` tinyint(1) NOT NULL default '0' COMMENT '标记数据，1未同步 0已同步',
  `sync_sms` tinyint(1) NOT NULL default '0' COMMENT '标记数据，1未同步 0已同步',
  `update_time` int(11) NOT NULL default '0' COMMENT '客户提取回执的时间',
  `insert_time` int(11) NOT NULL COMMENT '从rpc中接收到客户数据的时间（短信插入时间）',
  `send_ToAgent_time` int(11) NOT NULL default '0' COMMENT '服务器把数据提交给代理商的时间',
  `receive_Agent_time` int(11) NOT NULL default '0' COMMENT '收到代理商回执的时间',
  `sms_numbers` int(11) NOT NULL default '0' COMMENT '短信长度（条数)',
  `data_state` tinyint(4) default '0',
  `sms_receipt` tinyint(1) NOT NULL COMMENT '是否短信回执0否1是',
  `sms_port` tinyint(1) NOT NULL default '0' COMMENT '短信类型：1.挂机短信',
  PRIMARY KEY (`ser_id`),
  KEY `send_time_int` (`send_time_int`),
  KEY `vcc_code` (`vcc_code`),
  KEY `vcc_id` (`vcc_id`),
  KEY `result_code` (`result_code`),
  KEY `synchronous_sms` (`synchronous_sms`,`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_vccid` BEFORE INSERT ON `est_sms` FOR EACH ROW begin

set new.vcc_id=ifnull((select vcc_id from cc_ccods where vcc_code=ifnull(new.vcc_code,0)),0);

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `synchronous_sms` BEFORE UPDATE ON `est_sms` FOR EACH ROW BEGIN
IF  NEW.synchronous_sms=1 && NEW.synchronous_sms!=OLD.synchronous_sms
THEN
SET NEW.sync_sms=1;
UPDATE cc_ccods set synchronous_sms=1,sync_sms=1 where vcc_id=NEW.vcc_id;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `est_sms_error`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_sms_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `error_time` datetime DEFAULT NULL COMMENT '错误插入时间',
  `error_message` text COMMENT '错误',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_smsmo`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_smsmo` (
  `mo_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键自增ID',
  `mo_mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '发送手机号码',
  `mo_content` text NOT NULL COMMENT '回复内容',
  `mo_time` varchar(20) NOT NULL DEFAULT '' COMMENT '回复时间',
  `mo_service` varchar(30) NOT NULL DEFAULT '' COMMENT '特服号码',
  `mo_ext` varchar(10) NOT NULL DEFAULT '' COMMENT '扩展码',
  `moid` varchar(11) NOT NULL DEFAULT '' COMMENT 'moid',
  `synchronous_sms` tinyint(1) NOT NULL DEFAULT '1' COMMENT '同步标记：1未同步，2以同步',
  `insert_time` datetime NOT NULL COMMENT '数据插入时间',
  PRIMARY KEY (`mo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `est_smsmodel`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_smsmodel` (
  `mod_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '模板id，主键，自增',
  `theme` varchar(50) NOT NULL COMMENT '模板主题',
  `content` text NOT NULL COMMENT '信息内容/模板内容',
  `create_time` varchar(20) NOT NULL COMMENT '创建时间',
  `create_id` int(11) NOT NULL COMMENT '模板创建人id',
  `create_name` varchar(20) NOT NULL COMMENT '模板创建人姓名',
  `update_time` varchar(20) NOT NULL COMMENT '修改时间',
  `update_id` int(11) NOT NULL COMMENT '修改人id',
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '公司id',
  PRIMARY KEY (`mod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='短信模板，用户自己使用自己的模板';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_create_time` BEFORE INSERT ON `est_smsmodel` FOR EACH ROW BEGIN
SET NEW.create_time = now();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_update_time` BEFORE UPDATE ON `est_smsmodel` FOR EACH ROW BEGIN
SET NEW.update_time=now();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `est_smsmodel_confirm`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `est_smsmodel_confirm` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统自增id',
  `name` varchar(30) NOT NULL COMMENT '列表显示名称',
  `display_name` varchar(20) NOT NULL COMMENT '模板中显示的名称',
  `fields` varchar(30) NOT NULL COMMENT '字段名称',
  `if_use` int(1) NOT NULL DEFAULT '0' COMMENT '是否启用0不显示，1显示',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '字段是否可用  1可用   0不可用',
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '公司id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='短信模板中可添加字段(必须有初始数据，与es';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `menus`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_text` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单文本',
  `menu_url` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单URL',
  `has_children` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否包含子菜单（0否1是）',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父菜单ID',
  `menu_icon` varchar(20) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '菜单图标',
  `sort_weight` int(11) NOT NULL DEFAULT '0' COMMENT '排序权重，数字越小排序越前',
  `help_url`  varchar(50) NULL COMMENT '帮助文档',
  `is_show`  tinyint(1) NULL DEFAULT 1 COMMENT '是否展示在左侧菜单中',
  PRIMARY KEY (`id`),
  KEY `parentId` (`parent_id`),
  KEY `sortWeight` (`sort_weight`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_agent_day`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_agent_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席id',
  `ag_name` varchar(30) NOT NULL COMMENT '坐席名',
  `ag_num` varchar(30) NOT NULL COMMENT '坐席工号',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（1-31）',
  `start_date` varchar(7) NOT NULL COMMENT '日期',
  `nowdate` date NOT NULL COMMENT '当前日期',
  `in_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `out_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼出量',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `internal_num` int(11) NOT NULL DEFAULT '0' COMMENT '内线量',
  `ring_num` int(11) NOT NULL DEFAULT '0' COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `consult_num` int(11) NOT NULL DEFAULT '0' COMMENT '咨询量',
  `consult_secs` int(11) NOT NULL DEFAULT '0' COMMENT '咨询总时长',
  `hold_num` int(11) NOT NULL DEFAULT '0' COMMENT '保持量',
  `hold_secs` int(11) NOT NULL DEFAULT '0' COMMENT '保持总时长',
  `conference_num` int(11) NOT NULL DEFAULT '0' COMMENT '会议量',
  `conference_secs` int(11) NOT NULL DEFAULT '0' COMMENT '会议总时长',
  `shift_num` int(11) NOT NULL DEFAULT '0' COMMENT '转接量',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '等量总时长',
  `ready_secs` int(11) NOT NULL DEFAULT '0' COMMENT '就绪总时长',
  `busy_secs` int(11) NOT NULL DEFAULT '0' COMMENT '置忙总时长',
  `wait_num` int(11) NOT NULL DEFAULT '0' COMMENT '事后次数',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `out_calls` int(11) NOT NULL DEFAULT '0' COMMENT '外呼量',
  `out_secs` int(11) NOT NULL DEFAULT '0' COMMENT '外呼总时长',
  `extra1` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段1',
  `extra2` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段2',
  `extra3` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段3',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`ag_id`,`time_stamp`,`start_date`) USING BTREE,
  KEY `nowdate` (`nowdate`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='坐席个人表现表（天）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_agent_halfhour`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_agent_halfhour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席id',
  `ag_name` varchar(30) NOT NULL COMMENT '坐席名',
  `ag_num` varchar(30) NOT NULL COMMENT '坐席工号',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（0-47）',
  `start_date` date NOT NULL COMMENT '日期',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `out_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼出量',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `internal_num` int(11) NOT NULL DEFAULT '0' COMMENT '内线量',
  `ring_num` int(11) NOT NULL DEFAULT '0' COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `consult_num` int(11) NOT NULL DEFAULT '0' COMMENT '咨询量',
  `consult_secs` int(11) NOT NULL DEFAULT '0' COMMENT '咨询总时长',
  `hold_num` int(11) NOT NULL DEFAULT '0' COMMENT '保持量',
  `hold_secs` int(11) NOT NULL DEFAULT '0' COMMENT '保持总时长',
  `conference_num` int(11) NOT NULL DEFAULT '0' COMMENT '会议量',
  `conference_secs` int(11) NOT NULL DEFAULT '0' COMMENT '会议总时长',
  `shift_num` int(11) NOT NULL DEFAULT '0' COMMENT '转接量',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '等量总时长',
  `ready_secs` int(11) NOT NULL DEFAULT '0' COMMENT '就绪总时长',
  `busy_secs` int(11) NOT NULL DEFAULT '0' COMMENT '置忙总时长',
  `wait_num` int(11) NOT NULL DEFAULT '0' COMMENT '事后次数',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `out_calls` int(11) NOT NULL DEFAULT '0' COMMENT '外呼量',
  `out_secs` int(11) NOT NULL DEFAULT '0' COMMENT '外呼总时长',
  `extra1` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段1',
  `extra2` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段2',
  `extra3` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段3',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`ag_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='坐席个人表现表（半小时）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_agent_hour`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_agent_hour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席id',
  `ag_name` varchar(30) NOT NULL COMMENT '坐席名',
  `ag_num` varchar(30) NOT NULL COMMENT '坐席工号',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（0-23）',
  `start_date` date NOT NULL COMMENT '日期',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `out_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼出量',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `internal_num` int(11) NOT NULL DEFAULT '0' COMMENT '内线量',
  `ring_num` int(11) NOT NULL DEFAULT '0' COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `consult_num` int(11) NOT NULL DEFAULT '0' COMMENT '咨询量',
  `consult_secs` int(11) NOT NULL DEFAULT '0' COMMENT '咨询总时长',
  `hold_num` int(11) NOT NULL DEFAULT '0' COMMENT '保持量',
  `hold_secs` int(11) NOT NULL DEFAULT '0' COMMENT '保持总时长',
  `conference_num` int(11) NOT NULL DEFAULT '0' COMMENT '会议量',
  `conference_secs` int(11) NOT NULL DEFAULT '0' COMMENT '会议总时长',
  `shift_num` int(11) NOT NULL DEFAULT '0' COMMENT '转接量',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '等量总时长',
  `ready_secs` int(11) NOT NULL DEFAULT '0' COMMENT '就绪总时长',
  `busy_secs` int(11) NOT NULL DEFAULT '0' COMMENT '置忙总时长',
  `wait_num` int(11) NOT NULL DEFAULT '0' COMMENT '事后次数',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `out_calls` int(11) NOT NULL DEFAULT '0' COMMENT '外呼量',
  `out_secs` int(11) NOT NULL DEFAULT '0' COMMENT '外呼总时长',
  `extra1` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段1',
  `extra2` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段2',
  `extra3` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段3',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`ag_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='坐席个人表现表（小时）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_agent_month`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_agent_month` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席id',
  `ag_name` varchar(30) NOT NULL COMMENT '坐席名',
  `ag_num` varchar(30) NOT NULL COMMENT '坐席工号',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（1-12）',
  `start_date` varchar(4) NOT NULL COMMENT '年',
  `nowmonth` char(7) NOT NULL COMMENT '当前月份',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `out_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼出量',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `internal_num` int(11) NOT NULL DEFAULT '0' COMMENT '内线量',
  `ring_num` int(11) NOT NULL DEFAULT '0' COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `consult_num` int(11) NOT NULL DEFAULT '0' COMMENT '咨询量',
  `consult_secs` int(11) NOT NULL DEFAULT '0' COMMENT '咨询总时长',
  `hold_num` int(11) NOT NULL DEFAULT '0' COMMENT '保持量',
  `hold_secs` int(11) NOT NULL DEFAULT '0' COMMENT '保持总时长',
  `conference_num` int(11) NOT NULL DEFAULT '0' COMMENT '会议量',
  `conference_secs` int(11) NOT NULL DEFAULT '0' COMMENT '会议总时长',
  `shift_num` int(11) NOT NULL DEFAULT '0' COMMENT '转接量',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '等量总时长',
  `ready_secs` int(11) NOT NULL DEFAULT '0' COMMENT '就绪总时长',
  `busy_secs` int(11) NOT NULL DEFAULT '0' COMMENT '置忙总时长',
  `wait_num` int(11) NOT NULL DEFAULT '0' COMMENT '事后次数',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `out_calls` int(11) NOT NULL DEFAULT '0' COMMENT '外呼时长',
  `out_secs` int(11) NOT NULL DEFAULT '0',
  `extra1` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段1',
  `extra2` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段2',
  `extra3` int(11) NOT NULL DEFAULT '0' COMMENT '额外字段3',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`ag_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='坐席个人表现表（月）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_caller_types`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_caller_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL COMMENT '企业ID',
  `caller_type` tinyint(1) unsigned NOT NULL COMMENT '主叫类型（0手机1固话）',
  `nums` int(11) unsigned NOT NULL COMMENT '数量',
  `start_year` year(4) NOT NULL COMMENT '年',
  `start_month` tinyint(2) unsigned zerofill NOT NULL COMMENT '月',
  `start_day` tinyint(2) unsigned zerofill NOT NULL COMMENT '日',
  `start_date` date NOT NULL COMMENT '日期',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`caller_type`,`start_date`) USING BTREE,
  KEY `vcc_id_2` (`vcc_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='手机固话统计报表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_group_day`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_group_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '业务组id',
  `group_name` varchar(30) NOT NULL COMMENT '业务组名',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（1-31）',
  `start_date` varchar(7) NOT NULL COMMENT '日期',
  `nowdate` date NOT NULL COMMENT '当前日期',
  `in_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `out_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼出量',
  `out_calls` int(11) NOT NULL DEFAULT '0' COMMENT '外呼时长',
  `out_secs` int(11) NOT NULL DEFAULT '0',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `internal_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '内线量',
  `ring_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `consult_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '咨询量',
  `consult_secs` int(11) NOT NULL DEFAULT '0' COMMENT '咨询总时长',
  `hold_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '保持量',
  `hold_secs` int(11) NOT NULL DEFAULT '0' COMMENT '保持总时长',
  `conference_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '会议量',
  `conference_secs` int(11) NOT NULL DEFAULT '0' COMMENT '会议总时长',
  `shift_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '转接量',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '等量总时长',
  `ready_secs` int(11) NOT NULL DEFAULT '0' COMMENT '就绪总时长',
  `busy_secs` int(11) NOT NULL DEFAULT '0' COMMENT '置忙总时长',
  `wait_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '事后次数',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`group_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT=' 业务组工作表现表（天）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_group_halfhour`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_group_halfhour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '业务组id',
  `group_name` varchar(30) NOT NULL COMMENT '业务组名',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（0-47）',
  `start_date` date NOT NULL COMMENT '日期',
  `in_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `out_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼出量',
  `out_calls` int(11) NOT NULL DEFAULT '0' COMMENT '外呼时长',
  `out_secs` int(11) NOT NULL DEFAULT '0',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `internal_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '内线量',
  `ring_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `consult_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '咨询量',
  `consult_secs` int(11) NOT NULL DEFAULT '0' COMMENT '咨询总时长',
  `hold_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '保持量',
  `hold_secs` int(11) NOT NULL DEFAULT '0' COMMENT '保持总时长',
  `conference_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '会议量',
  `conference_secs` int(11) NOT NULL DEFAULT '0' COMMENT '会议总时长',
  `shift_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '转接量',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '等量总时长',
  `ready_secs` int(11) NOT NULL DEFAULT '0' COMMENT '就绪总时长',
  `busy_secs` int(11) NOT NULL DEFAULT '0' COMMENT '置忙总时长',
  `wait_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '事后次数',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`group_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='业务组工作表现表（半小时）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_group_hour`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_group_hour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '业务组id',
  `group_name` varchar(30) NOT NULL COMMENT '业务组名',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（0-23）',
  `start_date` date NOT NULL COMMENT '日期',
  `in_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `out_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼出量',
  `out_calls` int(11) NOT NULL DEFAULT '0' COMMENT '外呼时长',
  `out_secs` int(11) NOT NULL DEFAULT '0',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `internal_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '内线量',
  `ring_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `consult_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '咨询量',
  `consult_secs` int(11) NOT NULL DEFAULT '0' COMMENT '咨询总时长',
  `hold_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '保持量',
  `hold_secs` int(11) NOT NULL DEFAULT '0' COMMENT '保持总时长',
  `conference_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '会议量',
  `conference_secs` int(11) NOT NULL DEFAULT '0' COMMENT '会议总时长',
  `shift_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '转接量',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '等量总时长',
  `ready_secs` int(11) NOT NULL DEFAULT '0' COMMENT '就绪总时长',
  `busy_secs` int(11) NOT NULL DEFAULT '0' COMMENT '置忙总时长',
  `wait_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '事后次数',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`group_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='业务组工作表现表（小时）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_group_month`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_group_month` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '业务组id',
  `group_name` varchar(30) NOT NULL COMMENT '业务组名',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（1-12）',
  `start_date` varchar(4) NOT NULL COMMENT '年',
  `nowmonth` char(7) NOT NULL COMMENT '当前月份',
  `in_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `out_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '呼出量',
  `out_calls` int(11) NOT NULL DEFAULT '0' COMMENT '外呼时长',
  `out_secs` int(11) NOT NULL DEFAULT '0',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `internal_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '内线量',
  `ring_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `consult_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '咨询量',
  `consult_secs` int(11) NOT NULL DEFAULT '0' COMMENT '咨询总时长',
  `hold_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '保持量',
  `hold_secs` int(11) NOT NULL DEFAULT '0' COMMENT '保持总时长',
  `conference_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '会议量',
  `conference_secs` int(11) NOT NULL DEFAULT '0' COMMENT '会议总时长',
  `shift_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '转接量',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '等量总时长',
  `ready_secs` int(11) NOT NULL DEFAULT '0' COMMENT '就绪总时长',
  `busy_secs` int(11) NOT NULL DEFAULT '0' COMMENT '置忙总时长',
  `wait_num` smallint(5) NOT NULL DEFAULT '0' COMMENT '事后次数',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`group_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT=' 业务组工作表现表（月）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_hour_innums`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_hour_innums` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL COMMENT '企业ID',
  `server_num` varchar(20) NOT NULL COMMENT '服务号码',
  `que_id` int(11) unsigned NOT NULL COMMENT '队列ID',
  `que_name` varchar(20) NOT NULL COMMENT '队列名',
  `group_id` int(11) NOT NULL COMMENT '业务组ID',
  `start_hour` tinyint(2) unsigned zerofill NOT NULL COMMENT '小时',
  `nums` int(11) NOT NULL COMMENT '数量',
  `start_year` year(4) NOT NULL COMMENT '年',
  `start_month` tinyint(2) unsigned zerofill NOT NULL COMMENT '月',
  `start_day` tinyint(2) unsigned zerofill NOT NULL COMMENT '日',
  `start_date` date NOT NULL COMMENT '日期',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`que_id`,`start_hour`,`start_date`,`server_num`,`group_id`),
  KEY `vcc_id_2` (`vcc_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='按小时进线统计表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_incdr_area`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_incdr_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `server_num` varchar(20) NOT NULL COMMENT '服务号码',
  `que_id` int(11) unsigned NOT NULL COMMENT '队列ID',
  `que_name` varchar(30) NOT NULL COMMENT '队列名称',
  `group_id` int(11) NOT NULL COMMENT '业务组id',
  `area_code` varchar(4) NOT NULL COMMENT '区号',
  `area_name` varchar(20) NOT NULL COMMENT '地区名',
  `province_name` varchar(20) NOT NULL COMMENT '省份名称',
  `nums` int(11) unsigned NOT NULL COMMENT '个数',
  `start_year` year(4) NOT NULL COMMENT '年',
  `start_month` tinyint(2) unsigned zerofill NOT NULL COMMENT '月',
  `start_day` tinyint(2) unsigned zerofill NOT NULL COMMENT '日',
  `start_date` date NOT NULL COMMENT '日期',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`server_num`,`province_name`,`start_date`,`que_id`,`group_id`),
  KEY `vcc_id_2` (`vcc_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='呼入地区分析报表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_queue_day`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_queue_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `queue_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列id',
  `queue_name` varchar(30) NOT NULL COMMENT '队列名',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（1-31）',
  `start_date` varchar(7) NOT NULL COMMENT '日期',
  `nowdate` date NOT NULL COMMENT '当前日期',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `lost_num` int(11) NOT NULL DEFAULT '0' COMMENT '放弃量',
  `lost_secs` int(11) NOT NULL DEFAULT '0' COMMENT '放弃总时长',
  `lost10_num` int(11) unsigned NOT NULL,
  `lost20_num` int(11) unsigned NOT NULL,
  `lost25_num` int(11) unsigned NOT NULL,
  `lost30_num` int(11) unsigned NOT NULL,
  `lost35_num` int(11) unsigned NOT NULL,
  `lost40_num` int(11) unsigned NOT NULL,
  `conn_num` int(11) NOT NULL DEFAULT '0' COMMENT '接通量',
  `queue_secs` int(11) NOT NULL DEFAULT '0' COMMENT '排队总时长',
  `ring_num` int(11) NOT NULL COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `conn5_num` smallint(5) NOT NULL DEFAULT '0',
  `conn10_num` smallint(5) NOT NULL DEFAULT '0',
  `conn15_num` int(11) NOT NULL DEFAULT '0' COMMENT '15秒内接起量',
  `conn20_num` smallint(5) NOT NULL DEFAULT '0',
  `conn30_num` smallint(5) NOT NULL DEFAULT '0',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `deal_secs` int(11) NOT NULL DEFAULT '0' COMMENT '处理总时长',
  `lost1_num` int(11) unsigned NOT NULL COMMENT '主叫放弃的数量,ext_option=1',
  `lost1_secs` int(11) unsigned NOT NULL COMMENT '主叫放弃的时长,ext_option=1',
  `lost3_num` int(11) unsigned NOT NULL COMMENT '排队超时的数量,ext_option=3',
  `lost3_secs` int(11) unsigned NOT NULL COMMENT '排队超时的时长,ext_option=3',
  `lost4_num` int(11) unsigned NOT NULL COMMENT '技能组满溢出的数量,ext_option=4',
  `lost4_secs` int(11) unsigned NOT NULL COMMENT '技能组满溢出的时长,ext_option=4',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`queue_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='技能组话务报表（天）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_queue_halfhour`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_queue_halfhour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `queue_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列id',
  `queue_name` varchar(30) NOT NULL COMMENT '队列名',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（0-47）',
  `start_date` date NOT NULL COMMENT '日期',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `lost_num` int(11) NOT NULL DEFAULT '0' COMMENT '放弃量',
  `lost_secs` int(11) NOT NULL DEFAULT '0' COMMENT '放弃总时长',
  `lost10_num` int(11) unsigned NOT NULL,
  `lost20_num` int(11) unsigned NOT NULL,
  `lost25_num` int(11) unsigned NOT NULL,
  `lost30_num` int(11) unsigned NOT NULL,
  `lost35_num` int(11) unsigned NOT NULL,
  `lost40_num` int(11) unsigned NOT NULL,
  `conn_num` int(11) NOT NULL DEFAULT '0' COMMENT '接通量',
  `queue_secs` int(11) NOT NULL DEFAULT '0' COMMENT '排队总时长',
  `ring_num` int(11) NOT NULL COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `conn5_num` smallint(5) NOT NULL DEFAULT '0',
  `conn10_num` smallint(5) NOT NULL DEFAULT '0',
  `conn15_num` int(11) NOT NULL DEFAULT '0' COMMENT '15秒内接起量',
  `conn20_num` smallint(5) NOT NULL DEFAULT '0',
  `conn30_num` smallint(5) NOT NULL DEFAULT '0',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `deal_secs` int(11) NOT NULL DEFAULT '0' COMMENT '处理总时长',
  `lost1_num` int(11) unsigned NOT NULL COMMENT '主叫放弃的数量,ext_option=1',
  `lost1_secs` int(11) unsigned NOT NULL COMMENT '主叫放弃的时长,ext_option=1',
  `lost3_num` int(11) unsigned NOT NULL COMMENT '排队超时的数量,ext_option=3',
  `lost3_secs` int(11) unsigned NOT NULL COMMENT '排队超时的时长,ext_option=3',
  `lost4_num` int(11) unsigned NOT NULL COMMENT '技能组满溢出的数量,ext_option=4',
  `lost4_secs` int(11) unsigned NOT NULL COMMENT '技能组满溢出的时长,ext_option=4',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`queue_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='技能组话务报表（半小时）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_queue_hour`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_queue_hour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `queue_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列id',
  `queue_name` varchar(30) NOT NULL COMMENT '队列名',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（0-23）',
  `start_date` date NOT NULL COMMENT '日期',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `lost_num` int(11) NOT NULL DEFAULT '0' COMMENT '放弃量',
  `lost_secs` int(11) NOT NULL DEFAULT '0' COMMENT '放弃总时长',
  `lost10_num` int(11) unsigned NOT NULL,
  `lost20_num` int(11) unsigned NOT NULL,
  `lost25_num` int(11) unsigned NOT NULL,
  `lost30_num` int(11) unsigned NOT NULL,
  `lost35_num` int(11) unsigned NOT NULL,
  `lost40_num` int(11) unsigned NOT NULL,
  `conn_num` int(11) NOT NULL DEFAULT '0' COMMENT '接通量',
  `queue_secs` int(11) NOT NULL DEFAULT '0' COMMENT '排队总时长',
  `ring_num` int(11) NOT NULL COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `conn5_num` smallint(5) NOT NULL DEFAULT '0',
  `conn10_num` smallint(5) NOT NULL DEFAULT '0',
  `conn15_num` int(11) NOT NULL DEFAULT '0' COMMENT '15秒内接起量',
  `conn20_num` smallint(5) NOT NULL DEFAULT '0',
  `conn30_num` smallint(5) NOT NULL DEFAULT '0',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `deal_secs` int(11) NOT NULL DEFAULT '0' COMMENT '处理总时长',
  `lost1_num` int(11) unsigned NOT NULL COMMENT '主叫放弃的数量,ext_option=1',
  `lost1_secs` int(11) unsigned NOT NULL COMMENT '主叫放弃的时长,ext_option=1',
  `lost3_num` int(11) unsigned NOT NULL COMMENT '排队超时的数量,ext_option=3',
  `lost3_secs` int(11) unsigned NOT NULL COMMENT '排队超时的时长,ext_option=3',
  `lost4_num` int(11) unsigned NOT NULL COMMENT '技能组满溢出的数量,ext_option=4',
  `lost4_secs` int(11) unsigned NOT NULL COMMENT '技能组满溢出的时长,ext_option=4',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`queue_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='技能组话务报表（小时）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_queue_month`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_queue_month` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `queue_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列id',
  `queue_name` varchar(30) NOT NULL COMMENT '队列名',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（1-12）',
  `start_date` varchar(4) NOT NULL COMMENT '年份',
  `nowmonth` char(7) NOT NULL COMMENT '当前月份',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `lost_num` int(11) NOT NULL DEFAULT '0' COMMENT '放弃量',
  `lost_secs` int(11) NOT NULL DEFAULT '0' COMMENT '放弃总时长',
  `lost10_num` int(11) unsigned NOT NULL,
  `lost20_num` int(11) unsigned NOT NULL,
  `lost25_num` int(11) unsigned NOT NULL,
  `lost30_num` int(11) unsigned NOT NULL,
  `lost35_num` int(11) unsigned NOT NULL,
  `lost40_num` int(11) unsigned NOT NULL,
  `conn_num` int(11) NOT NULL DEFAULT '0' COMMENT '接通量',
  `queue_secs` int(11) NOT NULL DEFAULT '0' COMMENT '排队总时长',
  `ring_num` int(11) NOT NULL COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `conn5_num` smallint(5) NOT NULL DEFAULT '0',
  `conn10_num` smallint(5) NOT NULL DEFAULT '0',
  `conn15_num` int(11) NOT NULL DEFAULT '0' COMMENT '15秒内接起量',
  `conn20_num` smallint(5) NOT NULL DEFAULT '0',
  `conn30_num` smallint(5) NOT NULL DEFAULT '0',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `deal_secs` int(11) NOT NULL DEFAULT '0' COMMENT '处理总时长',
  `lost1_num` int(11) unsigned NOT NULL COMMENT '主叫放弃的数量,ext_option=1',
  `lost1_secs` int(11) unsigned NOT NULL COMMENT '主叫放弃的时长,ext_option=1',
  `lost3_num` int(11) unsigned NOT NULL COMMENT '排队超时的数量,ext_option=3',
  `lost3_secs` int(11) unsigned NOT NULL COMMENT '排队超时的时长,ext_option=3',
  `lost4_num` int(11) unsigned NOT NULL COMMENT '技能组满溢出的数量,ext_option=4',
  `lost4_secs` int(11) unsigned NOT NULL COMMENT '技能组满溢出的时长,ext_option=4',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`queue_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='技能组话务报表（月）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_system_day`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_system_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（1-31）',
  `start_date` varchar(7) NOT NULL COMMENT '日期',
  `nowdate` date NOT NULL COMMENT '当前日期',
  `ivr_num` int(11) NOT NULL DEFAULT '0' COMMENT 'ivr呼入量',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `lost_num` int(11) NOT NULL DEFAULT '0' COMMENT '放弃量',
  `lost_secs` int(11) NOT NULL DEFAULT '0' COMMENT '放弃总时长',
  `lost10_num` int(11) unsigned NOT NULL,
  `lost20_num` int(11) unsigned NOT NULL,
  `lost25_num` int(11) unsigned NOT NULL,
  `lost30_num` int(11) unsigned NOT NULL,
  `lost35_num` int(11) unsigned NOT NULL,
  `lost40_num` int(11) unsigned NOT NULL,
  `conn_num` int(11) NOT NULL DEFAULT '0' COMMENT '接通量',
  `queue_secs` int(11) NOT NULL DEFAULT '0' COMMENT '排队总时长',
  `ring_num` int(11) NOT NULL COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `conn5_num` smallint(5) NOT NULL DEFAULT '0',
  `conn10_num` smallint(5) NOT NULL DEFAULT '0',
  `conn15_num` int(11) NOT NULL DEFAULT '0' COMMENT '15秒内接起量',
  `conn20_num` smallint(5) NOT NULL DEFAULT '0',
  `conn30_num` smallint(5) NOT NULL DEFAULT '0',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '登录时长',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `deal_secs` int(11) NOT NULL DEFAULT '0' COMMENT '处理总时长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='呼叫中心整体话务报表（天）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_system_halfhour`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_system_halfhour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（0-47）',
  `start_date` date NOT NULL COMMENT '日期',
  `ivr_num` int(11) NOT NULL DEFAULT '0' COMMENT 'ivr呼入量',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `lost_num` int(11) NOT NULL DEFAULT '0' COMMENT '放弃量',
  `lost_secs` int(11) NOT NULL DEFAULT '0' COMMENT '放弃总时长',
  `lost10_num` int(11) unsigned NOT NULL,
  `lost20_num` int(11) unsigned NOT NULL,
  `lost25_num` int(11) unsigned NOT NULL,
  `lost30_num` int(11) unsigned NOT NULL,
  `lost35_num` int(11) unsigned NOT NULL,
  `lost40_num` int(11) unsigned NOT NULL,
  `conn_num` int(11) NOT NULL DEFAULT '0' COMMENT '接通量',
  `queue_secs` int(11) NOT NULL DEFAULT '0' COMMENT '排队总时长',
  `ring_num` int(11) NOT NULL COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `conn5_num` smallint(5) NOT NULL DEFAULT '0',
  `conn10_num` smallint(5) NOT NULL DEFAULT '0',
  `conn15_num` int(11) NOT NULL DEFAULT '0' COMMENT '15秒内接起量',
  `conn20_num` smallint(5) NOT NULL DEFAULT '0',
  `conn30_num` smallint(5) NOT NULL DEFAULT '0',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '登录时长',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `deal_secs` int(11) NOT NULL DEFAULT '0' COMMENT '处理总时长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='呼叫中心整体话务报表（半小时）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_system_hour`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_system_hour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（0-23）',
  `start_date` date NOT NULL COMMENT '日期',
  `ivr_num` int(11) NOT NULL DEFAULT '0' COMMENT 'ivr呼入量',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `lost_num` int(11) NOT NULL DEFAULT '0' COMMENT '放弃量',
  `lost_secs` int(11) NOT NULL DEFAULT '0' COMMENT '放弃总时长',
  `lost10_num` int(11) unsigned NOT NULL,
  `lost20_num` int(11) unsigned NOT NULL,
  `lost25_num` int(11) unsigned NOT NULL,
  `lost30_num` int(11) unsigned NOT NULL,
  `lost35_num` int(11) unsigned NOT NULL,
  `lost40_num` int(11) unsigned NOT NULL,
  `conn_num` int(11) NOT NULL DEFAULT '0' COMMENT '接通量',
  `queue_secs` int(11) NOT NULL DEFAULT '0' COMMENT '排队总时长',
  `ring_num` int(11) NOT NULL COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `conn5_num` smallint(5) NOT NULL DEFAULT '0',
  `conn10_num` smallint(5) NOT NULL DEFAULT '0',
  `conn15_num` int(11) NOT NULL DEFAULT '0' COMMENT '15秒内接起量',
  `conn20_num` smallint(5) NOT NULL DEFAULT '0',
  `conn30_num` smallint(5) NOT NULL DEFAULT '0',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '登录时长',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `deal_secs` int(11) NOT NULL DEFAULT '0' COMMENT '处理总时长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='呼叫中心整体话务报表（小时）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_system_month`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_system_month` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `time_stamp` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '时间点（1-12）',
  `start_date` varchar(4) NOT NULL COMMENT '年份',
  `nowmonth` char(7) NOT NULL COMMENT '当前月份',
  `ivr_num` int(11) NOT NULL DEFAULT '0' COMMENT 'ivr呼入量',
  `in_num` int(11) NOT NULL DEFAULT '0' COMMENT '呼入量',
  `lost_num` int(11) NOT NULL DEFAULT '0' COMMENT '放弃量',
  `lost_secs` int(11) NOT NULL DEFAULT '0' COMMENT '放弃总时长',
  `lost10_num` int(11) unsigned NOT NULL,
  `lost20_num` int(11) unsigned NOT NULL,
  `lost25_num` int(11) unsigned NOT NULL,
  `lost30_num` int(11) unsigned NOT NULL,
  `lost35_num` int(11) unsigned NOT NULL,
  `lost40_num` int(11) unsigned NOT NULL,
  `conn_num` int(11) NOT NULL DEFAULT '0' COMMENT '接通量',
  `queue_secs` int(11) NOT NULL DEFAULT '0' COMMENT '排队总时长',
  `ring_num` int(11) NOT NULL COMMENT '振铃次数',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃总时长',
  `conn5_num` smallint(5) NOT NULL DEFAULT '0',
  `conn10_num` smallint(5) NOT NULL DEFAULT '0',
  `conn15_num` int(11) NOT NULL DEFAULT '0' COMMENT '15秒内接起量',
  `conn20_num` smallint(5) NOT NULL DEFAULT '0',
  `conn30_num` smallint(5) NOT NULL DEFAULT '0',
  `login_secs` int(11) NOT NULL DEFAULT '0' COMMENT '登录时长',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话总时长',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '事后总时长',
  `deal_secs` int(11) NOT NULL DEFAULT '0' COMMENT '处理总时长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`,`time_stamp`,`start_date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='呼叫中心整体话务报表（月）';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_tmp_amount`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_tmp_amount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `que_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列ID',
  `ag_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL COMMENT '业务组',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型',
  `call_time` int(11) NOT NULL DEFAULT '0' COMMENT '呼叫时间',
  `starttime` int(10) NOT NULL DEFAULT '0' COMMENT '开始时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='量日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_tmp_time`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `rep_tmp_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业id',
  `call_id` varchar(20) NOT NULL DEFAULT '0',
  `que_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列id',
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席id',
  `group_id` int(10) unsigned NOT NULL COMMENT '业务组',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型',
  `ext_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '附加类型',
  `call_time` int(11) NOT NULL DEFAULT '0' COMMENT '呼叫时间',
  `starttime` int(10) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `endtime` int(10) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `dealtime` int(10) NOT NULL DEFAULT '0' COMMENT '当前处理时间',
  `ext_option` varchar(50) NOT NULL DEFAULT '' COMMENT '额外属性',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='时长日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业ID',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '报表名称',
  `items` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `report_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT '报表类型（agent、queue、system）',
  PRIMARY KEY (`id`),
  KEY `vccId` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `session`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `session` (
  `session_id` varchar(255) NOT NULL,
  `session_value` blob NOT NULL,
  `session_time` int(11) NOT NULL,
  `sess_lifetime` int(11) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `swj_hangup_sms`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `swj_hangup_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL,
  `vcc_code` varchar(30) NOT NULL COMMENT '企业代码',
  `is_connect` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否开通接通时的挂机短信（0未开通1开通）',
  `connect_sms` text NOT NULL COMMENT '接通挂机短信内容',
  `is_busy` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否开通未接通时的挂机短信（0未开通1开通）',
  `busy_sms` text NOT NULL COMMENT '未接通挂机短信内容',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='挂机短信设置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_menu`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `user_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL COMMENT '菜单id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `user_flag` tinyint(4) NOT NULL COMMENT '用户标识(1,agent,2.ccod,3ccadmin)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_400cdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_400cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业ID',
  `superior_agent` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级代理商',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `server_400` varchar(64) NOT NULL DEFAULT '' COMMENT '400号码',
  `server_num` varchar(64) NOT NULL DEFAULT '' COMMENT '中继号码',
  `caller_num` varchar(64) NOT NULL DEFAULT '' COMMENT '主叫号码',
  `called_num` varchar(64) NOT NULL DEFAULT '' COMMENT '被叫号码',
  `phone_num` varchar(64) NOT NULL DEFAULT '' COMMENT '主叫号码',
  `cdr_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1呼入 2呼出 3三方',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `bill_secs` int(11) NOT NULL DEFAULT '0' COMMENT '计费时长',
  `start_year` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '年份',
  `start_month` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '月份',
  `start_week` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '周',
  `start_day` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `start_qua` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '季度',
  `start_hour` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '小时',
  `start_minute` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '分钟',
  PRIMARY KEY (`id`),
  KEY `callid` (`call_id`) USING BTREE,
  KEY `start_time` (`start_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `win_400cdr_time` BEFORE INSERT ON `win_400cdr` FOR EACH ROW BEGIN
SET @now = FROM_UNIXTIME(NEW.start_time);
SET NEW.start_year = DATE_FORMAT(@now,'%Y');
SET NEW.start_month = DATE_FORMAT(@now,'%m');
SET NEW.start_week = DATE_FORMAT(@now,'%U');
SET NEW.start_day = DATE_FORMAT(@now,'%d');
SET NEW.start_qua =  EXTRACT(QUARTER FROM @now);
SET NEW.start_hour = DATE_FORMAT(@now,'%H');
SET NEW.start_minute= DATE_FORMAT(@now,'%i');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_800cdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_800cdr` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `call_id` varchar(20) NOT NULL,
  `vcc_id` int(11) unsigned NOT NULL,
  `real_caller` varchar(20) NOT NULL COMMENT '真实主叫',
  `caller` varchar(20) NOT NULL,
  `called` varchar(20) NOT NULL,
  `ivr_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'ivr开始时间',
  `start_time` int(11) unsigned NOT NULL,
  `conn_time` int(11) unsigned NOT NULL,
  `end_time` int(11) unsigned NOT NULL,
  `ivr_secs` smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT 'ivr时长',
  `conn_secs` int(11) unsigned NOT NULL,
  `total_secs` int(11) unsigned NOT NULL,
  `call_result` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0未呼通1呼通',
  `result_reason` varchar(30) NOT NULL COMMENT '原因',
  `ivr_time_str` datetime NOT NULL,
  `start_time_str` datetime NOT NULL,
  `conn_time_str` datetime NOT NULL,
  `end_time_str` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`),
  KEY `call_result` (`call_result`,`called`),
  KEY `ivr_time` (`ivr_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='外呼通话记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_agcdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_agcdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL,
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席ID',
  `ag_num` varchar(20) NOT NULL COMMENT '坐席号',
  `ag_name` varchar(20) NOT NULL COMMENT '坐席姓名',
  `que_id` int(11) NOT NULL DEFAULT '0',
  `que_name` varchar(20) NOT NULL COMMENT '队列名',
  `group_id` int(10) unsigned NOT NULL COMMENT '业务组',
  `call_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '通话类型 1呼出 呼入 呼出转接 呼入转接 呼出拦截 呼入拦截 (被)咨询 (被)三方 监听 强插',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `ext_id` int(11) NOT NULL DEFAULT '0' COMMENT '附加ID',
  `ag_phone` varchar(32) NOT NULL COMMENT '坐席电话',
  `cus_phone` varchar(32) NOT NULL COMMENT '客户电话',
  `serv_num` varchar(32) NOT NULL COMMENT '服务号码',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `conn_time` int(11) NOT NULL DEFAULT '0' COMMENT '接通时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '束结时间',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃时长',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话时长',
  `all_secs` int(11) NOT NULL DEFAULT '0' COMMENT '总时长',
  `result` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0接通 1振铃放弃 2未接通',
  `endresult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1振铃放弃 2未接 11用户挂机 12坐席挂断 13转接 14拦截',
  `record_file` varchar(256) NOT NULL COMMENT '录音地址',
  `evaluate` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '-1未转评价 -2超时未评价 -3未评价挂机 >0为评价结果',
  `record_mark` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `start_year` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '年份',
  `start_month` tinyint(2) unsigned zerofill NOT NULL DEFAULT '00' COMMENT '月份',
  `start_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `start_qua` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '季度',
  `start_hour` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '小时',
  `start_minute` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分钟',
  `start_week` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '周',
  `start_date` date NOT NULL COMMENT '当前日期',
  `flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标志位，是否已处理',
  PRIMARY KEY (`id`),
  KEY `ag_id` (`ag_id`),
  KEY `vcc_id` (`vcc_id`,`record_mark`),
  KEY `vcc_id_2` (`vcc_id`,`result`,`call_type`),
  KEY `start_date` (`vcc_id`,`start_date`),
  KEY `call_id` (`call_id`),
  KEY `cus_phone` (`cus_phone`),
  KEY `start_time` (`flag`,`start_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `start_time` BEFORE INSERT ON `win_agcdr` FOR EACH ROW BEGIN
SET @now = FROM_UNIXTIME(NEW.start_time);
SET NEW.start_date = DATE_FORMAT(@now,'%Y-%m-%d');
SET NEW.start_year = DATE_FORMAT(@now,'%Y');
SET NEW.start_month = DATE_FORMAT(@now,'%m');
SET NEW.start_week = DATE_FORMAT(@now,'%U');
SET NEW.start_day = DATE_FORMAT(@now,'%d');
SET NEW.start_qua =  EXTRACT(QUARTER FROM @now);
SET NEW.start_hour = DATE_FORMAT(@now,'%H');
SET NEW.start_minute= DATE_FORMAT(@now,'%i');
UPDATE cc_ccods set synchronous_call=1,sync_call=1 where vcc_id=NEW.vcc_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `add_bill` AFTER INSERT ON `win_agcdr` FOR EACH ROW BEGIN

if new.call_type=12 and new.start_time >0 then
insert into win_bill_cdr(vcc_id,call_id,superior_agent,server_400,server_num,server_type,caller_num,called_num,called_type,cdr_type,caller_starttime,caller_connecttime,called_connecttime,called_endtime,caller_secs,called_secs,call_result) values (new.vcc_id,new.call_id,getSuperioragent(new.vcc_id),getserver400(new.serv_num),new.serv_num,getphonetype(new.serv_num),new.cus_phone,new.ag_phone,IF(LEFT(new.ag_phone,1)=0,2,1),new.call_type,new.start_time,new.start_time,new.conn_time,new.end_time,new.all_secs,new.conn_secs,new.result);
end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_agent`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `vcc_code` varchar(64) NOT NULL COMMENT '企业代码',
  `ag_num` varchar(64) NOT NULL DEFAULT '' COMMENT '坐席号(工号)',
  `user_login_state`  tinyint(1) NULL DEFAULT 1 COMMENT '坐席登录状态  1空闲 2忙碌',
  `ag_caller` varchar(20) DEFAULT NULL,
  `ag_caller_type`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '外呼主叫号码类型 0全部  1指定',
  `ag_password` varchar(256) NOT NULL DEFAULT '202cb962ac59075b964b07152d234b70',
  `ag_name` varchar(64) NOT NULL DEFAULT '' COMMENT '坐席姓名',
  `ag_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '席坐类型 1动态坐席，2静态坐席(分机)',
  `ag_owncaller` varchar(20) NOT NULL DEFAULT '' COMMENT '呼叫坐席主叫',
  `pho_id` int(11) NOT NULL DEFAULT '0' COMMENT '电话ID',
  `pho_num` varchar(20) NOT NULL COMMENT '分机号码',
  `group_id` int(10) unsigned NOT NULL COMMENT '业务组',
  `tellevel` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '权限级别',
  `ac_proid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '自动外呼项目ID',
  `ag_role` tinyint(1) NOT NULL DEFAULT '0' COMMENT '坐席角色（0普通坐席1班长坐席）',
  `user_role` int(11) NOT NULL DEFAULT '0',
  `user_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `user_queues` varchar(150) NOT NULL,
  `crm_datarole` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'crm数据权限（0不限1技能组2坐席）',
  `ag_sta` tinyint(4) NOT NULL DEFAULT '0' COMMENT '坐席状态 0未登录 1签入 2忙 4占用 5事后处理',
  `ag_sta_reason` int(11) NOT NULL DEFAULT '0',
  `ag_sta_time` int(11) NOT NULL DEFAULT '0' COMMENT '状态时间',
  `ag_sta_time_db` int(11) DEFAULT '0',
  `fail_times` int(11) NOT NULL DEFAULT '0',
  `is_del` tinyint(1) NOT NULL DEFAULT '0',
  `login_ip` varchar(64) NOT NULL DEFAULT '' COMMENT '登录IP',
  `if_popin` tinyint(1) NOT NULL COMMENT '是否来电弹屏1是0否',
  `popin_address` varchar(100) NOT NULL COMMENT '来电弹屏地址',
  `if_popout` tinyint(1) NOT NULL COMMENT '是否去电弹屏1是0否',
  `popout_address` varchar(100) NOT NULL COMMENT '去电弹屏地址',
  `time_firlogin` int(11) NOT NULL DEFAULT '0',
  `time_login` int(11) NOT NULL DEFAULT '0',
  `time_lastcall` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最近通话时间',
  `secs_login` int(11) NOT NULL DEFAULT '0',
  `secs_ready` int(11) NOT NULL DEFAULT '0',
  `secs_busy` int(11) NOT NULL DEFAULT '0',
  `secs_call` int(11) NOT NULL DEFAULT '0',
  `secs_ring` int(11) NOT NULL DEFAULT '0',
  `secs_wait` int(11) NOT NULL DEFAULT '0',
  `sec_ans` int(11) NOT NULL DEFAULT '0' COMMENT '回答时间',
  `times_ans` int(11) NOT NULL DEFAULT '0' COMMENT '回答次数',
  `times_call` int(11) NOT NULL DEFAULT '0',
  `times_busy` int(11) NOT NULL DEFAULT '0',
  `synchronous_agent` tinyint(1) DEFAULT '1' COMMENT '同步标记  1为需要同步的数据',
  `sms_phone` varchar(20) DEFAULT NULL COMMENT '短信提醒号码',
  `sync_agent` tinyint(1) DEFAULT '1',
  `btn_str`  varchar(200) NULL COMMENT 'toolbar中控制按钮显示',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`),
  KEY `is_del` (`is_del`,`vcc_code`,`ag_num`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `synchronous_agent_insert` BEFORE INSERT ON `win_agent`
FOR EACH ROW BEGIN
UPDATE cc_ccods set synchronous_agent=1,sync_agent=1 where vcc_id=NEW.vcc_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `synchronous_agent` BEFORE UPDATE ON `win_agent` FOR EACH ROW BEGIN
IF NEW.ag_num !=OLD.ag_num || NEW.ag_password != OLD.ag_password || NEW.ag_name != OLD.ag_name || NEW.is_del != 

OLD.is_del
THEN
SET NEW.synchronous_agent = 1;
SET NEW.sync_agent = 1;
UPDATE cc_ccods set synchronous_agent=1,sync_agent=1 where vcc_id=NEW.vcc_id;

ELSEIF  NEW.synchronous_agent!=OLD.synchronous_agent  &&  NEW.synchronous_agent=1
THEN
UPDATE cc_ccods set synchronous_agent=1,sync_agent=1 where vcc_id=OLD.vcc_id;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_agextphone`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_agextphone` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `ag_id` int(11) NOT NULL COMMENT '坐席ID',
  `pho_num` varchar(64) NOT NULL COMMENT '电话号码',
  `pho_chan` tinyint(4) NOT NULL DEFAULT '0' COMMENT '外呼通道',
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态：0无效，1有效',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ag_id_unique` (`ag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='坐席号码转接';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_agmonitor`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_agmonitor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席ID',
  `ag_num` varchar(64) NOT NULL DEFAULT '' COMMENT '坐席号(工号)',
  `ag_name` varchar(64) NOT NULL DEFAULT '' COMMENT '坐席姓名',
  `ag_sta` tinyint(4) NOT NULL DEFAULT '0' COMMENT '坐席状态 0未登录 1签入 2忙 4占用 5事后处理',
  `ag_sta_reason` int(11) NOT NULL DEFAULT '0',
  `ag_sta_time` int(11) NOT NULL DEFAULT '0' COMMENT '状态时间',
  `ag_sta_time_db` int(11) DEFAULT '0',
  `pho_sta_calltype` tinyint(4) NOT NULL COMMENT '呼叫类型(同AGCDR)',
  `pho_sta_callque` int(11) NOT NULL COMMENT '呼叫技能组',
  `pho_id` int(11) NOT NULL DEFAULT '0' COMMENT '电话ID',
  `pho_num` varchar(20) NOT NULL COMMENT '分机号码',
  `ac_proid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '自动外呼项目ID',
  `pho_sta` tinyint(4) NOT NULL DEFAULT '0' COMMENT '分机状态',
  `pho_sta_reason` int(11) NOT NULL DEFAULT '0',
  `pho_sta_time` int(11) NOT NULL DEFAULT '0' COMMENT '状态时间',
  `pho_sta_time_db` int(11) NOT NULL DEFAULT '0' COMMENT '状态时间',
  `login_ip` varchar(64) NOT NULL DEFAULT '' COMMENT '登录IP',
  `time_firlogin` int(11) NOT NULL DEFAULT '0',
  `time_login` int(11) NOT NULL DEFAULT '0',
  `time_lastcall` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最近通话时间',
  `secs_login` int(11) NOT NULL DEFAULT '0',
  `secs_ready` int(11) NOT NULL DEFAULT '0',
  `secs_busy` int(11) NOT NULL DEFAULT '0',
  `secs_call` int(11) NOT NULL DEFAULT '0',
  `secs_ring` int(11) NOT NULL DEFAULT '0',
  `secs_wait` int(11) NOT NULL DEFAULT '0',
  `times_call` int(11) NOT NULL DEFAULT '0',
  `times_busy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `vcc` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_agqu`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_agqu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `que_id` int(11) NOT NULL COMMENT '队列ID',
  `ag_id` int(11) NOT NULL COMMENT '坐席ID',
  `skill` int(11) NOT NULL DEFAULT '0' COMMENT '技能水平',
  PRIMARY KEY (`id`),
  KEY `que_id` (`que_id`) USING BTREE,
  KEY `ag_id` (`ag_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_agsta_detail`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_agsta_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `ag_id` int(11) NOT NULL COMMENT '坐席ID',
  `ag_num` varchar(64) NOT NULL COMMENT '坐席号',
  `ag_name` varchar(64) NOT NULL COMMENT '坐席姓名',
  `group_id` int(10) unsigned NOT NULL COMMENT '业务组',
  `ag_sta_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '操作 1登录 2示忙',
  `ag_sta_reason` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '原因',
  `ag_login_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '登录IP',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `duration` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '持续时间',
  `bend` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否结束',
  `start_year` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '年份',
  `start_month` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '月份',
  `start_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `start_week` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '周',
  `start_qua` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '季度',
  `start_hour` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '小时',
  `start_minute` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分钟',
  PRIMARY KEY (`id`),
  KEY (`vcc_id`, `ag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `win_agsta_time` BEFORE INSERT ON `win_agsta_detail` FOR EACH ROW BEGIN
SET @now = FROM_UNIXTIME(NEW.start_time);
SET NEW.start_year = DATE_FORMAT(@now,'%Y');
SET NEW.start_month = DATE_FORMAT(@now,'%m');
SET NEW.start_week = DATE_FORMAT(@now,'%U');
SET NEW.start_day = DATE_FORMAT(@now,'%d');
SET NEW.start_qua =  EXTRACT(QUARTER FROM @now);
SET NEW.start_hour = DATE_FORMAT(@now,'%H');
SET NEW.start_minute= DATE_FORMAT(@now,'%i');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_agstat_reason`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_agstat_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业id',
  `ag_stat` tinyint(1) NOT NULL COMMENT '坐席状态 1置忙',
  `stat_reason` varchar(50) NOT NULL DEFAULT '' COMMENT '具体原因',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_bill_cdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_bill_cdr` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL COMMENT '企业id',
  `superior_agent` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级代理商',
  `call_id` varchar(20) NOT NULL COMMENT '呼叫ID',
  `server_type` tinyint(1) unsigned NOT NULL COMMENT '号码类型(1系统中继2系统400)',
  `server_400` varchar(20) NOT NULL COMMENT '400号码',
  `server_num` varchar(20) NOT NULL COMMENT '中继号码',
  `caller_num` varchar(20) NOT NULL COMMENT '主叫号码',
  `called_num` varchar(20) NOT NULL COMMENT '被叫号码',
  `called_type` tinyint(1) unsigned NOT NULL COMMENT '被叫类型(1本地2长途)',
  `cdr_type` tinyint(1) unsigned NOT NULL COMMENT '呼叫类型(1呼出2呼入3三方)',
  `caller_starttime` int(11) unsigned NOT NULL COMMENT '主叫开始时间',
  `caller_connecttime` int(11) unsigned NOT NULL COMMENT '主叫接通时间',
  `called_connecttime` int(11) unsigned NOT NULL COMMENT '被叫接通时间',
  `called_endtime` int(11) unsigned NOT NULL COMMENT '被叫结束时间',
  `caller_secs` int(11) unsigned NOT NULL COMMENT '主叫呼叫时长',
  `called_secs` int(11) unsigned NOT NULL COMMENT '被叫呼叫时长',
  `call_result` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否接通（0接通 >0未接通）',
  `if_deal` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已处理',
  `cdr_fee` decimal(10,3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `call_result` (`call_result`,`if_deal`) USING BTREE,
  KEY `vcc` (`vcc_id`,`caller_starttime`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='计费记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_call_queue`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_call_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `que_id` int(11) NOT NULL COMMENT '队列ID',
  `que_name` varchar(64) NOT NULL DEFAULT '' COMMENT '队列名',
  `queuer_num` varchar(64) NOT NULL DEFAULT '' COMMENT '排队号码',
  `queuer_chan` varchar(64) NOT NULL COMMENT '通道号',
  `queuer_sta` int(11) NOT NULL DEFAULT '0',
  `assign_ag` int(11) NOT NULL DEFAULT '0',
  `in_time` int(11) NOT NULL DEFAULT '0' COMMENT '进队列时间',
  `in_time_db` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `que_id` (`que_id`) USING BTREE,
  KEY `vcc_id` (`vcc_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_callouter_expert`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_callouter_expert` (
  `callid` int(11) NOT NULL,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `callouter_caller` varchar(15) NOT NULL DEFAULT '' COMMENT '咨询主叫',
  `callouter_called` varchar(15) NOT NULL DEFAULT '' COMMENT '咨询号码',
  `caller` varchar(15) NOT NULL DEFAULT '' COMMENT '主叫号码',
  `called` varchar(15) NOT NULL DEFAULT '' COMMENT '被叫号码',
  `experts` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否专家咨询0否(免费咨询)1是(付费咨询)',
  `ag_id` int(11) NOT NULL COMMENT '坐席id',
  `ag_num` varchar(20) NOT NULL DEFAULT '' COMMENT '坐席工号',
  `time` datetime NOT NULL COMMENT '保存时间',
  `fag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '处理标记位0未处理1已处理',
  PRIMARY KEY (`callid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='咨询外线咨询信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_cdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `caller` varchar(20) NOT NULL COMMENT '主叫',
  `called` varchar(20) NOT NULL COMMENT '被叫',
  `server_num` varchar(20) NOT NULL COMMENT '服务号码',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `conn_time` int(11) NOT NULL DEFAULT '0' COMMENT '接通时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '束结时间',
  `bill_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话时长',
  `all_secs` int(11) NOT NULL DEFAULT '0' COMMENT '总时长',
  `call_type` int(11) NOT NULL DEFAULT '0' COMMENT '呼叫类型 1呼出 2呼入 12内部呼叫 3转接 4拦截',
  `call_result` tinyint(1) NOT NULL DEFAULT '0' COMMENT '呼叫结果 0应答 大于0无应答 队列放弃 失败(忙 空号等)',
  `start_year` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '年份',
  `start_month` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '月份',
  `start_week` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '周',
  `start_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `start_qua` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '季度',
  `start_hour` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '小时',
  `start_minute` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分钟',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`) USING BTREE,
  KEY `call_id` (`call_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `win_cdr_time` BEFORE INSERT ON `win_cdr` FOR EACH ROW BEGIN

SET @now = FROM_UNIXTIME(NEW.start_time);

SET NEW.start_year = DATE_FORMAT(@now,'%Y');

SET NEW.start_month = DATE_FORMAT(@now,'%m');

SET NEW.start_week = DATE_FORMAT(@now,'%U');

SET NEW.start_day = DATE_FORMAT(@now,'%d');

SET NEW.start_qua =  EXTRACT(QUARTER FROM @now);

SET NEW.start_hour = DATE_FORMAT(@now,'%H');

SET NEW.start_minute= DATE_FORMAT(@now,'%i');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `add_400cdr` AFTER INSERT ON `win_cdr` FOR EACH ROW 
BEGIN
if (new.call_type=2 and new.all_secs>0) then
insert into win_400cdr(cdr_type, vcc_id,call_id,server_400,server_num,caller_num,called_num,start_time,end_time,bill_secs) values (1,new.vcc_id,new.call_id,getserver400(new.server_num),new.server_num,new.caller,IF(STRCMP(new.called,''),new.called,new.server_num),new.start_time,new.end_time,new.all_secs);
elseif ((new.call_type=1 or new.call_type=12 or new.call_type=11) and new.start_time >0) then
insert into win_400cdr(cdr_type, vcc_id,call_id,server_400,server_num,caller_num,called_num,start_time,end_time,bill_secs) values (2,new.vcc_id,new.call_id,getserver400(new.server_num),new.server_num,new.caller,IF(new.conn_time>0,new.called,new.server_num),new.start_time,new.end_time,new.all_secs);
elseif (new.call_type=14 and new.all_secs>0) then
insert into win_400cdr(cdr_type, vcc_id,call_id,server_400,server_num,caller_num,called_num,start_time,end_time,bill_secs) values (2,new.vcc_id,new.call_id,'',getvccserver(new.vcc_id),new.caller,IF(new.conn_time>0,new.called,getvccserver(new.vcc_id)),new.start_time,new.end_time,new.all_secs);
end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_channel_cdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_channel_cdr` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL COMMENT '企业ID',
  `uniid` int(11) unsigned NOT NULL COMMENT 'UID',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `channel` varchar(128) NOT NULL COMMENT '通道名',
  `chg` varchar(128) NOT NULL COMMENT '通道组',
  `cht` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '通道类型 CH_DTI 1 ;CH_AGDTI 2;CH_SIP 3 ;CH_AG 4  ;其他0',
  `chtrunk` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否外线通道，是的话产生trunkcdr',
  `caller` varchar(64) NOT NULL DEFAULT '' COMMENT '主叫',
  `called` varchar(64) NOT NULL DEFAULT '' COMMENT '被叫',
  `origcaller` varchar(64) NOT NULL DEFAULT '' COMMENT '原始主叫',
  `origcalled` varchar(64) NOT NULL DEFAULT '' COMMENT '原始被叫',
  `direction` tinyint(4) NOT NULL DEFAULT '0' COMMENT '呼叫方向 1呼入 2 呼出',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '始开时间',
  `ring_time` int(11) NOT NULL DEFAULT '0' COMMENT '振铃时间',
  `ans_time` int(11) NOT NULL DEFAULT '0' COMMENT '答应时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `conn_sec` int(11) NOT NULL DEFAULT '0' COMMENT '接通时长',
  `all_sec` int(11) NOT NULL DEFAULT '0' COMMENT '总时长',
  `rst` int(11) NOT NULL DEFAULT '0' COMMENT '呼叫结果0接通，其他未接通原因',
  `endrst` smallint(5) NOT NULL DEFAULT '0' COMMENT '挂机原因',
  `start_year` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '年份',
  `start_month` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '月份',
  `start_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `start_qua` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '季度',
  `start_hour` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '小时',
  `start_minute` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分钟',
  `start_week` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '周',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `win_channel_cdr_time` BEFORE INSERT ON `win_channel_cdr` FOR EACH ROW BEGIN
SET @now = FROM_UNIXTIME(NEW.start_time);
SET NEW.start_year = DATE_FORMAT(@now,'%Y');
SET NEW.start_month = DATE_FORMAT(@now,'%m');
SET NEW.start_week = DATE_FORMAT(@now,'%U');
SET NEW.start_day = DATE_FORMAT(@now,'%d');
SET NEW.start_qua =  EXTRACT(QUARTER FROM @now);
SET NEW.start_hour = DATE_FORMAT(@now,'%H');
SET NEW.start_minute= DATE_FORMAT(@now,'%i');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_did`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_did` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(10) unsigned NOT NULL COMMENT '企业ID',
  `pho_id` int(10) unsigned NOT NULL COMMENT '分机ID',
  `out_id` int(10) unsigned NOT NULL COMMENT '外线号码',
  `did_type` tinyint(3) unsigned NOT NULL DEFAULT '3' COMMENT 'DID类型 1呼入 2呼出 3呼入呼出',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='DID号码表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_exception`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_exception` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eventid` int(10) unsigned NOT NULL COMMENT '事件ID',
  `eventmsg` varchar(256) NOT NULL COMMENT '事件说明',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `db_time` datetime NOT NULL COMMENT '数据库插入时间',
  `mark` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '处理标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='异常事件表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_extcdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_extcdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `ext_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '附加ID 每个通话从1开始',
  `ext_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1咨询三方 2监听强插',
  `server_num` varchar(20) NOT NULL DEFAULT '' COMMENT '服务号码',
  `ext_phone` varchar(20) NOT NULL DEFAULT '' COMMENT '电话号码',
  `call_phone` varchar(20) NOT NULL DEFAULT '' COMMENT '通话中号码',
  `ag_er` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发起方坐席ID',
  `ag_ernum` varchar(64) NOT NULL DEFAULT '' COMMENT '发起方坐席工号',
  `ag_ername` varchar(64) NOT NULL DEFAULT '' COMMENT '发起方坐席姓名',
  `ag_ed` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '被操作方坐席ID',
  `ag_ednum` varchar(64) NOT NULL DEFAULT '' COMMENT '被操作方坐席工号',
  `ag_edname` varchar(64) NOT NULL DEFAULT '' COMMENT '被操作方坐席姓名',
  `que_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列ID',
  `que_name` varchar(128) NOT NULL DEFAULT '' COMMENT '队列名',
  `group_id` int(10) unsigned NOT NULL COMMENT '业务组',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `conn_time` int(11) NOT NULL DEFAULT '0' COMMENT '接通时间',
  `conn2_time` int(11) NOT NULL DEFAULT '0' COMMENT '三方或强插起始时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `conn1_secs` int(11) NOT NULL DEFAULT '0' COMMENT '咨询或监听时间',
  `conn2_secs` int(11) NOT NULL DEFAULT '0' COMMENT '三方或强插时间',
  `conn_secs` tinyint(4) NOT NULL DEFAULT '0' COMMENT '接通时长',
  `all_secs` tinyint(4) NOT NULL DEFAULT '0' COMMENT '总时长',
  `result` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0接通 1振铃放弃 2未接通',
  `endresult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0挂机 1转接 2拦截',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`,`ext_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `add_400extcdr` AFTER INSERT ON `win_extcdr` FOR EACH ROW BEGIN
if new.result=0 then
insert into win_400cdr(cdr_type, vcc_id,superior_agent,call_id,server_400,server_num,caller_num,called_num,start_time,end_time,bill_secs) values (3,new.vcc_id,getSuperioragent(new.vcc_id),new.call_id,getserver400(new.server_num),new.server_num,new.server_num,new.ext_phone,new.conn_time,new.end_time,new.conn_secs);
end if;

if new.ext_type=1 and new.ag_ed=0 and new.result=0 then
insert into win_bill_cdr(vcc_id,call_id,superior_agent,server_400,server_num,server_type,caller_num,called_num,called_type,cdr_type,caller_starttime,caller_connecttime,called_connecttime,called_endtime,caller_secs,called_secs,call_result) values (new.vcc_id,new.call_id,getSuperioragent(new.vcc_id),getserver400(new.server_num),new.server_num,getphonetype(new.server_num),new.call_phone,new.ext_phone,IF(LEFT(new.ext_phone,1)=0,2,1),3,new.start_time,new.conn_time,new.conn2_time,new.end_time,new.conn_secs,new.conn2_secs,new.result);
end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_group`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_group` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_name` varchar(36) NOT NULL DEFAULT '' COMMENT '业务组名称',
  `vcc_id` int(10) unsigned NOT NULL COMMENT '企业ID',
  `is_del` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除 不删除(0),删除(1)',
  `is_local_remote` int(1) DEFAULT '1' COMMENT '标识是本地还是远程（1本地,2远程)',
  `remote_address` varchar(255) DEFAULT NULL COMMENT '远程地址',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_incdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_incdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `server_num` varchar(20) NOT NULL DEFAULT '' COMMENT '服务号码',
  `caller` varchar(20) NOT NULL DEFAULT '' COMMENT '主叫',
  `called` varchar(20) NOT NULL DEFAULT '' COMMENT '被叫',
  `caller_areacode` varchar(4) NOT NULL COMMENT '主叫区号',
  `caller_areaname` varchar(20) NOT NULL COMMENT '主叫地区名',
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席ID',
  `ag_num` varchar(20) NOT NULL DEFAULT '' COMMENT '坐席号',
  `ag_name` varchar(20) NOT NULL DEFAULT '' COMMENT '坐席姓名',
  `que_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列ID',
  `que_name` varchar(20) NOT NULL DEFAULT '' COMMENT '队列名',
  `group_id` int(10) unsigned NOT NULL COMMENT '业务组',
  `ivr_digits` varchar(16) NOT NULL DEFAULT '' COMMENT 'ivr按键',
  `charge_user` varchar(30) NOT NULL,
  `cdr_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `quein_time` int(11) NOT NULL DEFAULT '0' COMMENT '人工服务时间',
  `mesg_time` int(11) NOT NULL DEFAULT '0' COMMENT '留言时间',
  `conn_time` int(11) NOT NULL DEFAULT '0' COMMENT '接通时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '束结时间',
  `ivr_secs` int(11) NOT NULL DEFAULT '0' COMMENT 'IVR时长',
  `wait_secs` int(11) NOT NULL DEFAULT '0' COMMENT '等待时长',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话时长',
  `mesg_secs` int(11) NOT NULL DEFAULT '0' COMMENT '留言时长',
  `all_secs` int(11) NOT NULL DEFAULT '0' COMMENT '总时长',
  `result` tinyint(1) NOT NULL DEFAULT '0' COMMENT '呼叫结果 1IVR挂机 2留言 3未接通 4未接通留言 0接通',
  `endresult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '11用户挂机 12坐席挂断',
  `ivr_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '呼入IVR',
  `trans_mark` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '转接标记 1转技能组 2转坐席',
  `start_year` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '年份',
  `start_month` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '月份',
  `start_week` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '周',
  `start_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `start_qua` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '季度',
  `start_hour` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '小时',
  `start_minute` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分钟',
  `if_done` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已处理',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`) USING BTREE,
  KEY `call_id` (`call_id`) USING BTREE,
  KEY `year_month_day` (`start_year`, `start_month`, `start_day`) USING BTREE,
  KEY `vcc_start_time` (`vcc_id`, `start_time`) USING BTREE,
  KEY `if_done` (`if_done`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `win_incdr` BEFORE INSERT ON `win_incdr` FOR EACH ROW BEGIN
SET @now = FROM_UNIXTIME(NEW.start_time);
SET NEW.start_year = DATE_FORMAT(@now,'%Y');
SET NEW.start_month = DATE_FORMAT(@now,'%m');
SET NEW.start_week = DATE_FORMAT(@now,'%U');
SET NEW.start_day = DATE_FORMAT(@now,'%d');
SET NEW.start_qua =  EXTRACT(QUARTER FROM @now);
SET NEW.start_hour = DATE_FORMAT(@now,'%H');
SET NEW.start_minute= DATE_FORMAT(@now,'%i');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `win_lostcdr` AFTER INSERT ON `win_incdr` FOR EACH ROW 
BEGIN 
IF NEW.result>0 THEN 
INSERT INTO win_lost_cdr(vcc_id,caller,server_num,start_time,que_id,que_name,reason,call_id,if_work,if_trans) 
	values(NEW.vcc_id,NEW.caller,NEW.server_num,NEW.start_time,NEW.que_id,NEW.que_name,NEW.result+100,NEW.call_id,IF(new.ivr_id=1,1,0),IF(new.trans_mark,1,0)); 
END IF; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_ivr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_ivr` (
  `ivr_id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '虚拟呼叫中心ID',
  `vcc_code` varchar(30) NOT NULL COMMENT '虚拟呼叫中心代码',
  `ivr_name` varchar(30) NOT NULL COMMENT 'IVR名称',
  `ivr_code` varchar(20) NOT NULL COMMENT 'IVR编码',
  `ivr_content` mediumtext NOT NULL COMMENT 'IVR内容',
  `ivr_info` mediumtext NOT NULL COMMENT '整理过的IVR内容',
  `remark` varchar(250) NOT NULL COMMENT '备注',
  `add_time` int(10) NOT NULL COMMENT '添加时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `if_refresh` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已重新刷新生效(0否1是)',
  `ivr_ver` int(11) NOT NULL DEFAULT '0' COMMENT '是否新版本IVR(0否1是)',
  PRIMARY KEY (`ivr_id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='IVR信息表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_localphone`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_localphone` (
  `phopart` varchar(12) NOT NULL,
  PRIMARY KEY (`phopart`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='本地号段表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_lost_cdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_lost_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `caller` varchar(64) NOT NULL DEFAULT '' COMMENT '主叫',
  `server_num` varchar(64) NOT NULL DEFAULT '' COMMENT '接入号',
  `charge_user` varchar(30) NOT NULL,
  `cdr_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `call_id` varchar(20) DEFAULT '0',
  `ag_id` int(11) unsigned NOT NULL DEFAULT '0',
  `ag_num` varchar(20) NOT NULL,
  `ag_name` varchar(30) NOT NULL,
  `que_id` int(11) unsigned NOT NULL DEFAULT '0',
  `que_name` varchar(30) NOT NULL,
  `group_id` int(10) unsigned NOT NULL COMMENT '业务组',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `reason` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1ivr超限 2未启用 3过期 4余额不足 5不在接通时间内 6未设置日程 7企业不存在 8黑名单9不在白名单中 101IVR挂机 102留言 103未接通 104未接通留言',
  `if_work` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '是否工作时间',
  `if_trans` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '是否转技能组或坐席',
  `start_year` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '年份',
  `start_month` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '月份',
  `start_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `start_qua` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '季度',
  `start_hour` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '小时',
  `start_minute` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分钟',
  `start_week` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '周',
  `deal_ag_id` int(11) NOT NULL COMMENT '处理人di',
  `state` tinyint(1) NOT NULL COMMENT '0 未处理1已处理',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`),
  KEY `lostcdt_starttime` (`start_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `win_lost_cdr_time` BEFORE INSERT ON `win_lost_cdr` FOR EACH ROW BEGIN
SET @now = FROM_UNIXTIME(NEW.start_time);
SET NEW.start_year = DATE_FORMAT(@now,'%Y');
SET NEW.start_month = DATE_FORMAT(@now,'%m');
SET NEW.start_week = DATE_FORMAT(@now,'%U');
SET NEW.start_day = DATE_FORMAT(@now,'%d');
SET NEW.start_qua =  EXTRACT(QUARTER FROM @now);
SET NEW.start_hour = DATE_FORMAT(@now,'%H');
SET NEW.start_minute= DATE_FORMAT(@now,'%i');
UPDATE cc_ccods set synchronous_lostcall=1,sync_lostcall=1 where vcc_id=NEW.vcc_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_phone`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `vcc_code` varchar(30) NOT NULL COMMENT '企业代码',
  `superior_agent` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级代理商',
  `pho_num` varchar(64) NOT NULL COMMENT '分机号',
  `pho_type` tinyint(4) NOT NULL COMMENT '分机类型 1:PSTN 2:SIP网关 3:SIP分机 4:VPMN座机',
  `port_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '端口',
  `vpmn_num` varchar(20) NOT NULL COMMENT 'vpmn短号',
  `vpmn_name` varchar(30) NOT NULL COMMENT 'vpmn用户姓名',
  `pho_pass` varchar(20) NOT NULL DEFAULT '',
  `pho_chan` varchar(64) NOT NULL COMMENT 'SIP网关通道',
  `pho_sta` tinyint(4) NOT NULL DEFAULT '0' COMMENT '分机状态',
  `pho_sta_num` varchar(64) NOT NULL DEFAULT '' COMMENT '相关联的号码',
  `pho_sta_chan1` varchar(64) NOT NULL DEFAULT '' COMMENT '通道1',
  `pho_sta_chan2` varchar(64) NOT NULL DEFAULT '' COMMENT '通道2',
  `sta_time` int(11) NOT NULL DEFAULT '0' COMMENT '状态时间',
  `sta_time_db` int(11) DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL COMMENT '所属区域id',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_phone_group`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_phone_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(20) NOT NULL COMMENT '号码组名称',
  `phones` varchar(255) NOT NULL COMMENT '号码ID（以逗号形式隔开）',
  `vcc_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='号码组管理';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_procdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_procdr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `vcc_id` int(10) unsigned NOT NULL COMMENT '企业ID',
  `pro_id` int(10) unsigned NOT NULL COMMENT '项目ID',
  `task_id` int(10) unsigned NOT NULL COMMENT '任务ID',
  `ag_id` int(10) unsigned NOT NULL COMMENT '坐席ID',
  `call_id` varchar(20) NOT NULL COMMENT '通话ID',
  `call_type` tinyint(3) unsigned NOT NULL COMMENT '通话类型',
  `ag_phone` varchar(20) NOT NULL COMMENT '坐席电话',
  `cus_phone` varchar(20) NOT NULL COMMENT '客户电话',
  `start_time` int(10) unsigned NOT NULL COMMENT '呼叫时间',
  `start_date` date NOT NULL COMMENT '呼叫日期',
  `conn_secs` int(10) unsigned NOT NULL COMMENT '通话时长',
  `result` int(11) NOT NULL COMMENT '呼叫结果',
  PRIMARY KEY (`id`),
  KEY `pro_id` (`pro_id`),
  KEY `pro_callid` (`call_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='业务通话表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_queue`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `group_id` int(11) unsigned NOT NULL COMMENT '区域ID',
  `que_name` varchar(64) NOT NULL COMMENT '队列名',
  `que_num` varchar(30) NOT NULL COMMENT '技能组号',
  `que_priority`  TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '技能组优先级(数字越小优先级越高)',
  `que_tag` varchar(128) NOT NULL COMMENT '队列描述',
  `que_type` tinyint(4) NOT NULL COMMENT '0呼入呼出 1呼入 2呼出',
  `brecord` tinyint(4) NOT NULL COMMENT '是否录音',
  `que_strategy` tinyint(4) NOT NULL COMMENT '排队策略',
  `overflow_strategy` tinyint(4) NOT NULL COMMENT '溢出策略',
  `que_length` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '最大排队数',
  `que_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排队时长',
  `ring_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '坐席振铃时长',
  `next_wait` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分配等待时长',
  `b_announce` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否报工号',
  `noans_times` int(11) unsigned NOT NULL DEFAULT '3' COMMENT '坐席无应答次数',
  `noans_wait` int(11) unsigned NOT NULL DEFAULT '10' COMMENT '坐席无应答等待时间',
  `noans_action` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '坐席无应答操作',
  `wait_audio` varchar(128) NOT NULL DEFAULT '' COMMENT '队列等待音',
  `tellevel` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '权限级别',
  `is_del` tinyint(4) NOT NULL DEFAULT '0',
  `synchronous_queue` tinyint(1) NOT NULL DEFAULT '1',
  `sync_que` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`),
  KEY `is_del` (`is_del`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `synchronous_queue_insert` AFTER INSERT ON `win_queue` FOR EACH ROW BEGIN
UPDATE cc_ccods set synchronous_queue=1,sync_que=1 where vcc_id=NEW.vcc_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `synchronous_queue` BEFORE UPDATE ON `win_queue` FOR EACH ROW BEGIN
IF NEW.que_name != OLD.que_name || NEW.que_type != OLD.que_type || NEW.is_del !=OLD.is_del
THEN
SET NEW.synchronous_queue=1;
SET NEW.sync_que=1;
UPDATE cc_ccods set synchronous_queue=1,sync_que=1 where vcc_id=NEW.vcc_id;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_queue_cdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_queue_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `que_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列ID',
  `que_name` varchar(20) NOT NULL COMMENT '队列名',
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席ID',
  `ag_num` varchar(20) NOT NULL DEFAULT '' COMMENT '坐席工号',
  `ag_name` varchar(20) NOT NULL DEFAULT '' COMMENT '坐席姓名',
  `ag_phone` varchar(20) NOT NULL DEFAULT '' COMMENT '坐席分机',
  `caller_num` varchar(20) NOT NULL DEFAULT '' COMMENT '主叫号码',
  `server_num` varchar(20) NOT NULL DEFAULT '' COMMENT '服务号码',
  `caller_chan` varchar(20) NOT NULL DEFAULT '' COMMENT '主叫通道',
  `ent_que_time` int(11) NOT NULL DEFAULT '0' COMMENT '进入队列时间',
  `assign_time` int(11) NOT NULL DEFAULT '0' COMMENT '分配坐席时间',
  `link_time` int(11) NOT NULL DEFAULT '0' COMMENT '接通时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `que_secs` int(11) NOT NULL DEFAULT '0' COMMENT '排队时常',
  `ring_secs` int(11) NOT NULL DEFAULT '0' COMMENT '振铃时长',
  `conn_secs` int(11) NOT NULL DEFAULT '0' COMMENT '通话时长',
  `all_secs` int(11) NOT NULL DEFAULT '0' COMMENT '总时长',
  `result` tinyint(4) NOT NULL DEFAULT '0' COMMENT '分配结果0成功，1主叫放弃，2坐席未接，3排队超时 4队列满溢出',
  `endresult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1主叫放弃，2坐席未接，3排队超时 4队列满溢出 11用户挂机 12坐席挂断',
  `bend` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否最终结果',
  `start_year` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '年份',
  `start_month` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '月份',
  `start_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `start_qua` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '季度',
  `start_hour` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '小时',
  `start_minute` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分钟',
  `start_week` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '周',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`,`bend`),
  KEY `call_id` (`call_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `win_queue_cdr_time` BEFORE INSERT ON `win_queue_cdr` FOR EACH ROW BEGIN
SET @now = FROM_UNIXTIME(NEW.ent_que_time);
SET NEW.start_year = DATE_FORMAT(@now,'%Y');
SET NEW.start_month = DATE_FORMAT(@now,'%m');
SET NEW.start_week = DATE_FORMAT(@now,'%U');
SET NEW.start_day = DATE_FORMAT(@now,'%d');
SET NEW.start_qua =  EXTRACT(QUARTER FROM @now);
SET NEW.start_hour = DATE_FORMAT(@now,'%H');
SET NEW.start_minute= DATE_FORMAT(@now,'%i');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_rule_phone_ivr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_rule_phone_ivr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `rule_id` int(11) NOT NULL COMMENT '日程ID',
  `phone_id` varchar(150) NOT NULL COMMENT '号码ID（以逗号分隔）',
  `ivr_id` int(11) NOT NULL COMMENT 'IVR流程',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='号码及IVR流程管理表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_rule_time`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_rule_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` varchar(10) NOT NULL,
  `end_date` varchar(10) NOT NULL,
  `start_time` varchar(8) NOT NULL,
  `end_time` varchar(8) NOT NULL,
  `weeks` varchar(15) NOT NULL,
  `rule_id` int(11) NOT NULL DEFAULT '0',
  `vcc_id` int(11) NOT NULL,
  `is_special` tinyint(1) NOT NULL COMMENT '是否是特殊日期（1是、2不是）',
  `next_rule` int(11) NOT NULL COMMENT '下一级的rule_id（若是特殊日期，则该rule_id是该号码组的上班时间的rule_id）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_rules`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_rules` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '虚拟呼叫中心ID',
  `vcc_code` varchar(30) NOT NULL COMMENT '虚拟呼叫中心代码',
  `phone_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属号码',
  `phone` varchar(20) NOT NULL COMMENT '中继号码',
  `phone400` varchar(20) NOT NULL COMMENT '400号码',
  `rule_name` varchar(30) NOT NULL COMMENT '日程规则名称',
  `rule_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '规则类型(1任意号码2特定号码开头3固定号码)',
  `rule` varchar(30) NOT NULL COMMENT '规则号码',
  `pre` varchar(20) NOT NULL COMMENT '开头号码',
  `len` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '号码长度',
  `ivr_id` int(11) NOT NULL DEFAULT '0' COMMENT '所使用的IVRID',
  `ivr_code` varchar(30) NOT NULL COMMENT 'IVR编码',
  `ivr_name` varchar(30) NOT NULL COMMENT 'IVR名称',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '优先级（数字越小越高）',
  `start_date` varchar(10) NOT NULL COMMENT '开始日期(2011-06-01*为不限)',
  `end_date` varchar(10) NOT NULL COMMENT '结束日期(*为不限)',
  `weeks` varchar(15) NOT NULL COMMENT '星期（0#1#2）',
  `start_time` varchar(5) NOT NULL COMMENT '开始时间(格式：09:00*为不限)',
  `end_time` varchar(5) NOT NULL COMMENT '结束时间(格式：09:00*为不限)',
  `remark` varchar(250) NOT NULL COMMENT '备注',
  `add_time` int(10) NOT NULL COMMENT '添加时间',
  `is_old` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否是老的日程',
  `ivr_ver` int(11) NOT NULL DEFAULT '0' COMMENT '此数字标示IVR其版本，修改则增加',
  `rule_type_select` tinyint(1) NOT NULL COMMENT '日程类型（1代表工作时间 2代表非工作时间 3代表节假日时间）',
  `phone_select` tinyint(1) NOT NULL COMMENT '号码选定方式（1分组号码，2未分组号码）',
  `phone_group` int(11) NOT NULL COMMENT '选定的分组号码的组ID',
  `group_id` int(11) NOT NULL COMMENT '号码组ID',
  PRIMARY KEY (`rule_id`),
  KEY `vcc_id` (`vcc_id`),
  KEY `phone_id` (`phone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='日程规则信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_sessions`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_sounds`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_sounds` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `remark` varchar(100) DEFAULT '',
  `address` varchar(200) DEFAULT '',
  `sounds_address` varchar(200) NOT NULL COMMENT '通信服务器的语音地址',
  `add_time` datetime DEFAULT NULL,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属虚拟呼叫中心id',
  `vcc_code` varchar(30) DEFAULT NULL COMMENT '所属虚拟呼叫中心',
  `superior_agent` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级代理商',
  `verify_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '审核状态（0未审核1审核通过2审核未通过）',
  `verify_time` int(11) NOT NULL COMMENT '审核时间',
  `group_id` int(10) unsigned NOT NULL COMMENT '区域ID',
  `sound_type` tinyint(1) NOT NULL DEFAULT '6' COMMENT '1: 技能语音,2: 满意度语音,3: 白名单语音,4: 按键语音,5: 留言语音,6: 其他',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_trans_call`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_trans_call` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业ID',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `call_id_trans` int(11) NOT NULL DEFAULT '0' COMMENT '转接呼叫ID',
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席ID',
  `ag_num` varchar(20) NOT NULL COMMENT '坐席号',
  `ag_name` varchar(20) NOT NULL COMMENT '坐席姓名',
  `que_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列ID',
  `que_name` varchar(20) NOT NULL COMMENT '队列名',
  `que_id_trans` int(11) NOT NULL DEFAULT '0' COMMENT '队列ID',
  `que_name_trans` varchar(20) NOT NULL COMMENT '队列名',
  `group_id` int(11) NOT NULL COMMENT '业务组',
  `ag_phone` varchar(32) NOT NULL COMMENT '坐席电话',
  `cus_phone` varchar(32) NOT NULL COMMENT '客户电话',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_trans_ivr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_trans_ivr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0' COMMENT '企业ID',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `ag_id` int(11) NOT NULL DEFAULT '0' COMMENT '坐席ID',
  `ag_num` varchar(20) NOT NULL COMMENT '坐席号',
  `ag_name` varchar(20) NOT NULL COMMENT '坐席姓名',
  `que_id` int(11) NOT NULL DEFAULT '0' COMMENT '队列ID',
  `que_name` varchar(20) NOT NULL COMMENT '队列名',
  `group_id` int(11) NOT NULL COMMENT '业务组',
  `ag_phone` varchar(32) NOT NULL COMMENT '坐席电话',
  `cus_phone` varchar(32) NOT NULL COMMENT '客户电话',
  `call_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '通话类型 1呼出 呼入 呼出转接 呼入转接 呼出拦截 呼入拦截 (被)咨询 (被)三方 监听 强插',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `ivr_secs` int(11) NOT NULL DEFAULT '0' COMMENT 'IVR时长',
  `ivr_result` varchar(32) NOT NULL COMMENT 'IVR结果',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_trunk_cdr`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_trunk_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `channel` varchar(64) NOT NULL COMMENT '通道名',
  `group` varchar(64) NOT NULL COMMENT '通道组',
  `chan_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '通道类型 IAX2 1 ;SIP 2;ZAP 3 ;DAHDI 4  ;SS7 5 ;其他0',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `caller` varchar(64) NOT NULL DEFAULT '' COMMENT '主叫',
  `called` varchar(64) NOT NULL DEFAULT '' COMMENT '被叫',
  `direction` tinyint(4) NOT NULL DEFAULT '0' COMMENT '呼叫方向 1呼出 2 呼入',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '始开时间',
  `ring_time` int(11) NOT NULL DEFAULT '0' COMMENT '振铃时间',
  `ans_time` int(11) NOT NULL DEFAULT '0' COMMENT '答应时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `bill_sec` int(11) NOT NULL DEFAULT '0' COMMENT '计费时长',
  `all_sec` int(11) NOT NULL DEFAULT '0' COMMENT '总时长',
  `result` tinyint(4) NOT NULL DEFAULT '0' COMMENT '呼叫结果0接通，其他未接通原因',
  `start_year` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '年份',
  `start_month` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '月份',
  `start_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '日期',
  `start_qua` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '季度',
  `start_hour` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '小时',
  `start_minute` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分钟',
  `start_week` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '周',
  `if_deal` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已处理（0未处理1已处理）',
  PRIMARY KEY (`id`),
  KEY `vccid` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `win_trunk_cdr_time` BEFORE INSERT ON `win_trunk_cdr` FOR EACH ROW BEGIN
SET @now = FROM_UNIXTIME(NEW.start_time);
SET NEW.start_year = DATE_FORMAT(@now,'%Y');
SET NEW.start_month = DATE_FORMAT(@now,'%m');
SET NEW.start_week = DATE_FORMAT(@now,'%U');
SET NEW.start_day = DATE_FORMAT(@now,'%d');
SET NEW.start_qua =  EXTRACT(QUARTER FROM @now);
SET NEW.start_hour = DATE_FORMAT(@now,'%H');
SET NEW.start_minute= DATE_FORMAT(@now,'%i');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `win_trunk_log`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_trunk_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `group` varchar(64) DEFAULT NULL,
  `topnum` int(11) NOT NULL DEFAULT '0',
  `toptime` int(11) NOT NULL DEFAULT '0',
  `logdate` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`,`group`,`logdate`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_trunk_monitor`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_trunk_monitor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `group` varchar(64) NOT NULL DEFAULT '',
  `usednum` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`,`group`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `win_voicemail`
--


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `win_voicemail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业ID',
  `call_id` varchar(20) NOT NULL DEFAULT '0' COMMENT '呼叫ID',
  `caller` varchar(32) NOT NULL DEFAULT '' COMMENT '主叫号码',
  `called` varchar(32) NOT NULL COMMENT '被叫号码',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '留言时间',
  `rec_secs` int(11) NOT NULL DEFAULT '0' COMMENT '留言时长',
  `rec_file` varchar(256) NOT NULL DEFAULT '' COMMENT '留言文件',
  `listen_mark` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否收听',
  `down_mark` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否下载',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'ccod'
--
/*!50003 DROP FUNCTION IF EXISTS `getagqu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getagqu`(`in_agid` int) RETURNS varchar(256) CHARSET utf8
BEGIN
	
declare retstr varchar(256);
declare m_queid int;
declare m_skill int;
declare no_more int;
declare cur1 CURSOR FOR select que_id,skill from win_agqu AS aq LEFT JOIN win_queue AS q on aq.que_id=q.id where aq.ag_id=in_agid ORDER BY q.que_priority asc;
declare continue handler for not found set no_more=1;
set no_more = 0;
set retstr = '';
OPEN cur1;
REPEAT
	FETCH cur1 INTO m_queid,m_skill;
if 0=no_more then
	set retstr = concat(retstr,m_queid,'-',m_skill,',');
end if;
UNTIL no_more
END REPEAT;
CLOSE cur1;
RETURN retstr;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getLocalPhone` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getLocalPhone`(`in_phone` varchar(20)) RETURNS varchar(20) CHARSET utf8
BEGIN
	
declare retstr varchar(20);
declare nozero varchar(20);
declare r int;
if CHAR_LENGTH(in_phone)=11 then
	if LEFT(in_phone, 1)='1' then
	set nozero=in_phone;
	else
	return in_phone;
	end if;
elseif CHAR_LENGTH(in_phone)=12 then
	if LEFT(in_phone,3)='010' then
	return in_phone;
	elseif LEFT(in_phone,2)='01' then
	set nozero=RIGHT(in_phone,11);
	else
	return in_phone;
	end if;
else
return in_phone;
end if;
select count(*) into r from win_localphone where phopart = LEFT(nozero,7);
if r=0 then
	set retstr=concat('0',nozero);
else
	set retstr=nozero;
end if;
	RETURN retstr;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getphoid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getphoid`(`in_vccid` int,`in_phonum` varchar(32)) RETURNS varchar(32) CHARSET utf8
BEGIN
	
declare retstr varchar(32);
declare m_phoid int default 0;
declare m_photype int default 0;
select id, pho_type into m_phoid , m_photype from win_phone where vcc_id=in_vccid and pho_num=in_phonum limit 1;
set retstr = concat(m_phoid,'-',m_photype);
	RETURN retstr;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getphonetype` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getphonetype`(`m_server_num` varchar(32)) RETURNS varchar(1) CHARSET utf8
BEGIN
	
declare retstr varchar(32);
set retstr='';
IF LEFT(m_server_num,3) = '400' THEN
select phone_type into retstr from cc_phone400s where phone400 = m_server_num limit 1;
ELSE
select phone_type into retstr from cc_phone400s where phone = m_server_num limit 1;
END IF;
	RETURN retstr;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getProState` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getProState`(`in_proid` int, `in_prostate` tinyint) RETURNS int(11)
BEGIN
	

	RETURN -1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getserver400` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getserver400`(`m_server_num` varchar(32)) RETURNS varchar(32) CHARSET utf8
BEGIN
	
declare retstr varchar(32);
set retstr='';
select phone400 into retstr from cc_phone400s where phone = m_server_num limit 1;
	RETURN retstr;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getSuperioragent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getSuperioragent`(`in_vcc_id` int(11)) RETURNS int(11)
BEGIN
	
declare retstr int(11);
set retstr=0;
select superior_agent into retstr from cc_ccods where vcc_id = in_vcc_id limit 1;
	RETURN retstr;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getTaskCaller` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getTaskCaller`(`in_pro_id` int) RETURNS varchar(20) CHARSET utf8
BEGIN
declare out_caller varchar(32);
set out_caller = '';
RETURN out_caller;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getVccPho` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getVccPho`(`in_vccid` int) RETURNS varchar(2048) CHARSET utf8
BEGIN
	
declare retstr varchar(2048);
declare m_pho400 varchar(20);
declare m_phoserv varchar(20);
declare m_phoager varchar(20);
declare m_disp tinyint;
declare no_more int;
declare cur1 CURSOR FOR select phone400,phone,display_type,phone_ager from cc_phone400s where vcc_id = in_vccid and if_bind = 1;
declare continue handler for not found set no_more=1;
set no_more = 0;
set retstr = '';

OPEN cur1;
REPEAT
	FETCH cur1 INTO m_pho400,m_phoserv,m_disp,m_phoager;
if 0=no_more then
	set retstr = concat(retstr,m_pho400,',',m_phoserv,',',m_disp,',',m_phoager,';');
end if;
UNTIL no_more
END REPEAT;
CLOSE cur1;

RETURN retstr;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getvccserver` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getvccserver`(`m_vcc_id` int(11)) RETURNS varchar(32) CHARSET utf8
BEGIN
declare retstr varchar(32);
set retstr='';
select phone into retstr from cc_phone400s where vcc_id = m_vcc_id limit 1;
	RETURN retstr;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `init_ag_sta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `init_ag_sta`(IN `in_lastend` int)
BEGIN
	
	
declare m_agid int;
declare m_agsta int;
declare no_more int;
declare cur1 CURSOR FOR select id,ag_sta from win_agent where ag_sta != 0;
declare continue handler for not found set no_more=1;
set no_more = 0;
TRUNCATE TABLE win_agmonitor;
update win_agsta_detail set bend = 1, duration = case when in_lastend<start_time then 0 else in_lastend - start_time end where bend = 0;
OPEN cur1;
REPEAT
	FETCH cur1 INTO m_agid,m_agsta;
	update win_agent set secs_login = secs_login + (in_lastend-time_login) where id = m_agid and time_login != 0;
	
	if 2=m_agsta then
	update win_agent set secs_busy = secs_busy + (in_lastend-ag_sta_time) where id = m_agid and ag_sta_time != 0;
	elseif 1=m_agsta then
	update win_agent set secs_ready = secs_ready + (in_lastend-ag_sta_time) where id = m_agid and ag_sta_time != 0;
	elseif 5=m_agsta then
	update win_agent set secs_wait = secs_wait + (in_lastend-ag_sta_time) where id = m_agid and ag_sta_time != 0;
	end if;
UNTIL no_more
END REPEAT;
CLOSE cur1;
update win_agent set ag_sta = 0, ag_sta_reason = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `init_statistics` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `init_statistics`(IN `in_lastend` int)
BEGIN
	
if in_lastend = 0 then
set in_lastend = UNIX_TIMESTAMP();
end if;
update rep_tmp_time set endtime=in_lastend where endtime = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `set_agpho_secs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `set_agpho_secs`(in in_ag_id int, in in_type tinyint, in in_secs int)
main:BEGIN
declare m_sta_time int;
set m_sta_time = UNIX_TIMESTAMP();
if in_type = 1 then
update win_agent set secs_ring=secs_ring+in_secs where id=in_ag_id;
update win_agent as a,win_agmonitor as b set b.secs_ring=a.secs_ring
       where b.ag_id = a.id;
elseif in_type = 2 then
update win_agent set secs_call=secs_call+in_secs, time_lastcall=m_sta_time where id=in_ag_id;
update win_agent as a,win_agmonitor as b set b.secs_call=a.secs_call, b.time_lastcall=a.time_lastcall
       where a.id=in_ag_id and b.ag_id = a.id;
end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `set_agpho_sta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `set_agpho_sta`(in in_ag_id int, in in_pho_sta tinyint)
main:BEGIN

declare m_sta_time int;
set m_sta_time = UNIX_TIMESTAMP();
update win_agmonitor set pho_sta=in_pho_sta,pho_sta_time=m_sta_time where ag_id=in_ag_id;
if in_pho_sta = 2 then
update win_agent as a,win_agmonitor as b set a.times_call = a.times_call+1,b.times_call = b.times_call+1
       where a.id=in_ag_id and b.ag_id = a.id;
end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `set_agpho_state` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `set_agpho_state`(in in_ag_id int, in in_pho_sta tinyint, in in_pho_calltype tinyint, in in_pho_callque int)
main:BEGIN
declare m_sta_time int;
set m_sta_time = UNIX_TIMESTAMP();
update win_agmonitor set pho_sta=in_pho_sta,pho_sta_time=m_sta_time,pho_sta_calltype=in_pho_calltype,pho_sta_callque=in_pho_callque where ag_id=in_ag_id;
if in_pho_sta = 2 then
update win_agent as a,win_agmonitor as b set a.times_call = a.times_call+1,b.times_call = b.times_call+1
       where a.id=in_ag_id and b.ag_id = a.id;
end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `set_ag_proid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `set_ag_proid`(IN `in_ag_id` int,IN `in_proid` int)
BEGIN
	
update win_agent set ac_proid = in_proid where id = in_ag_id;
update win_agmonitor set ac_proid = in_proid where id = in_ag_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `set_ag_sta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `set_ag_sta`(in in_ag_id int, in in_op_type tinyint, in in_param int, in in_paramstr varchar(64), in in_op_time int)
main:BEGIN
#sta: 0(logoff) 1(login) 2(busy) 3(unbusy) 4(use) 5(wait) 6(call)
declare m_ag_sta int default null;
declare m_ag_sta_reason int;
declare m_ag_flogin int;
declare m_ag_vccid int;
declare m_ag_gid int;
declare m_ag_num varchar(64);
declare m_ag_name varchar(64) character set utf8;
declare m_newstate int;
set m_newstate = in_op_type;
#login busy = busy
if 1=in_op_type and in_param = 2 then
set m_newstate = 2;
end if;
#unbusy unuse = login
if 3=in_op_type then
set m_newstate = 1;
end if;
if in_op_time = 0 then
set in_op_time = UNIX_TIMESTAMP();
end if;
select ag_sta, ag_sta_reason, time_firlogin,vcc_id, ag_num, ag_name, group_id into m_ag_sta, m_ag_sta_reason, m_ag_flogin,m_ag_vccid, m_ag_num, m_ag_name, m_ag_gid from win_agent where id = in_ag_id;
#login
if in_op_type = 1 then
#login-first
if DATE(FROM_UNIXTIME(in_op_time)) <> DATE(FROM_UNIXTIME(m_ag_flogin)) then
update win_agent set time_firlogin = in_op_time where id = in_ag_id;
end if;
update win_agent set time_login=in_op_time, login_ip=in_paramstr where id = in_ag_id;
delete from win_agmonitor where ag_id = in_ag_id;
insert into win_agmonitor(vcc_id,ag_id,ag_num,ag_name,pho_id,pho_num,login_ip,time_firlogin,time_login,
secs_login,secs_ready,secs_busy,secs_call,secs_ring,secs_wait,times_call,times_busy) (
select vcc_id,id,ag_num,ag_name,pho_id,pho_num,login_ip,time_firlogin,time_login,secs_login,secs_ready,secs_busy,
       secs_call,secs_ring,secs_wait,times_call,times_busy from win_agent where id = in_ag_id
);
end if;
#login or busy detail begin
if in_op_type = 1 or in_op_type = 2 then
insert into win_agsta_detail(vcc_id,ag_id, ag_num, ag_name, group_id, ag_sta_type, ag_sta_reason, start_time, ag_login_ip) values(m_ag_vccid,in_ag_id, m_ag_num, m_ag_name, m_ag_gid, in_op_type, in_param, in_op_time, in_paramstr);
end if;
if 1=in_op_type and in_param = 2 then
insert into win_agsta_detail(vcc_id,ag_id, ag_num, ag_name, group_id, ag_sta_type, ag_sta_reason, start_time, ag_login_ip) values(m_ag_vccid,in_ag_id, m_ag_num, m_ag_name, m_ag_gid, 2, 0, in_op_time, in_paramstr);
end if;
#login detail end
if in_op_type = 0 and in_param = 0 then
update win_agsta_detail set bend = 1, duration = in_op_time - start_time where bend = 0 and ag_id = in_ag_id;
update win_agent set secs_login = secs_login + (in_op_time-time_login) where id = in_ag_id and time_login != 0;
delete from win_agmonitor where ag_id = in_ag_id;
end if;
if in_op_type = 0 and in_param = 1 then
update win_agsta_detail set bend = 1, duration = in_op_time - start_time where bend = 0 and ag_id = in_ag_id and ag_sta_reason = 4;
LEAVE main;
end if;
#busy to other sta
if 2 = m_ag_sta then
update win_agent set secs_busy = secs_busy + (in_op_time-ag_sta_time) where id = in_ag_id and ag_sta_time != 0;
#busy detail end
update win_agsta_detail set bend = 1, duration = in_op_time - start_time where bend = 0 and ag_id = in_ag_id and ag_sta_type = 2;
elseif 2 = in_op_type then
update win_agent set times_busy = times_busy+1 where id = in_ag_id;
end if;
#ready to other sta
if 1 = m_ag_sta then
update win_agent set secs_ready = secs_ready + (in_op_time-ag_sta_time) where id = in_ag_id and ag_sta_time != 0;
end if;
#wait to other sta
if 5 = m_ag_sta then
update win_agent set secs_wait = secs_wait + (in_op_time-ag_sta_time) where id = in_ag_id and ag_sta_time != 0;
end if;
#call
if 6 = in_op_type then
update win_agent set times_call = times_call + 1, secs_call=secs_call+in_param where id = in_ag_id;
else
update win_agent set ag_sta = m_newstate, ag_sta_time = in_op_time, ag_sta_reason = in_param where id = in_ag_id;
if in_op_type <> 0 then
update win_agent as a,win_agmonitor as b set b.ag_sta=a.ag_sta,b.ag_sta_time=a.ag_sta_time,b.ag_sta_reason=a.ag_sta_reason,
       b.secs_ready=a.secs_ready,b.secs_wait=a.secs_wait,b.secs_busy=a.secs_busy,b.secs_call=a.secs_call,
       b.times_call=a.times_call,b.times_busy=a.times_busy
       where a.id=in_ag_id and b.ag_id = a.id;
end if;
end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `set_asr_result` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `set_asr_result`(IN `int_callid` INT, IN `in_calltype` SMALLINT, IN `in_asr_int` SMALLINT, IN `in_asr_str` VARCHAR(64))
main:BEGIN
if in_calltype = 13 then
update ac_cdr set asr_int = in_asr_int, asr_str = in_asr_str 
       where call_id = int_callid;
end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `set_trunk_use` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `set_trunk_use`(IN `in_trunktype` tinyint,IN `in_trunkgroup` varchar(64),IN `in_trunkuse` int)
BEGIN
	
declare m_nowdate date;
declare m_topall int;
set m_nowdate=curdate();
insert into win_trunk_monitor(type,`group`,usednum) values(in_trunktype,in_trunkgroup,in_trunkuse) ON DUPLICATE KEY update usednum=in_trunkuse ;
insert into win_trunk_log(type,`group`,logdate) values(in_trunktype,in_trunkgroup,m_nowdate) ON DUPLICATE KEY update vcc_id = 0;
update win_trunk_log set topnum = in_trunkuse, toptime=UNIX_TIMESTAMP() where  logdate=m_nowdate and  type=in_trunktype and `group`=in_trunkgroup and topnum<in_trunkuse;
insert into win_trunk_log(type,`group`,logdate) values(in_trunktype,'A',m_nowdate) ON DUPLICATE KEY update vcc_id = 0;
select sum(usednum) into m_topall from win_trunk_monitor where type=in_trunktype;
update win_trunk_log set topnum = m_topall, toptime=UNIX_TIMESTAMP() where logdate=m_nowdate and  type=in_trunktype and `group`='A' and topnum<m_topall;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `ccod`
--

USE `ccod`;

--
-- Final view structure for view `坐席监控`
--

/*!50001 DROP VIEW IF EXISTS `坐席监控`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `坐席监控` AS select `win_agmonitor`.`id` AS `id`,`win_agmonitor`.`vcc_id` AS `vcc_id`,`win_agmonitor`.`ag_id` AS `ag_id`,`win_agmonitor`.`ag_num` AS `ag_num`,`win_agmonitor`.`ag_name` AS `ag_name`,`win_agmonitor`.`ag_sta` AS `ag_sta`,`win_agmonitor`.`ag_sta_reason` AS `ag_sta_reason`,from_unixtime(`win_agmonitor`.`ag_sta_time`) AS `ag_sta_time`,from_unixtime(`win_agmonitor`.`ag_sta_time_db`) AS `ag_sta_time_db`,`win_agmonitor`.`pho_id` AS `pho_id`,`win_agmonitor`.`pho_num` AS `pho_num`,`win_agmonitor`.`pho_sta` AS `pho_sta`,`win_agmonitor`.`pho_sta_reason` AS `pho_sta_reason`,from_unixtime(`win_agmonitor`.`pho_sta_time`) AS `pho_sta_time`,from_unixtime(`win_agmonitor`.`pho_sta_time_db`) AS `pho_sta_time_db`,`win_agmonitor`.`login_ip` AS `login_ip`,from_unixtime(`win_agmonitor`.`time_firlogin`) AS `time_firlogin`,from_unixtime(`win_agmonitor`.`time_login`) AS `time_login`,`win_agmonitor`.`secs_login` AS `secs_login`,`win_agmonitor`.`secs_ready` AS `secs_ready`,`win_agmonitor`.`secs_busy` AS `secs_busy`,`win_agmonitor`.`secs_call` AS `secs_call`,`win_agmonitor`.`secs_ring` AS `secs_ring`,`win_agmonitor`.`secs_wait` AS `secs_wait`,`win_agmonitor`.`times_call` AS `times_call`,`win_agmonitor`.`times_busy` AS `times_busy` from `win_agmonitor` order by `win_agmonitor`.`vcc_id`,`win_agmonitor`.`ag_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `时长统计记录`
--

/*!50001 DROP VIEW IF EXISTS `时长统计记录`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `时长统计记录` AS select `rep_tmp_time`.`id` AS `id`,`rep_tmp_time`.`vcc_id` AS `企业`,`rep_tmp_time`.`que_id` AS `技能组`,`rep_tmp_time`.`ag_id` AS `坐席`,`rep_tmp_time`.`call_id` AS `呼叫ID`,(case `rep_tmp_time`.`type` when 1 then _utf8'排队时长' when 2 then _utf8'振铃时长' when 3 then _utf8'排队放弃' when 4 then _utf8'通话时长' when 5 then _utf8'咨询时长' when 6 then _utf8'会议时长' when 7 then _utf8'保持时长' when 9 then _utf8'事后时长' when 8 then _utf8'登录' when 10 then _utf8'就绪' when 11 then _utf8'置忙' else `rep_tmp_time`.`type` end) AS `类型`,(case `rep_tmp_time`.`ext_type` when 0 then _utf8'无' when 1 then _utf8'技能组呼入' when 2 then _utf8'呼出' when 3 then _utf8'内线呼叫' when 4 then _utf8'转接呼叫' else `rep_tmp_time`.`ext_type` end) AS `附加类型`,from_unixtime(`rep_tmp_time`.`starttime`) AS `开始时间`,(case `rep_tmp_time`.`endtime` when 0 then 0 else from_unixtime(`rep_tmp_time`.`endtime`) end) AS `结束时间` from `rep_tmp_time` order by `rep_tmp_time`.`id` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `量统计记录`
--

/*!50001 DROP VIEW IF EXISTS `量统计记录`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `量统计记录` AS select `rep_tmp_amount`.`id` AS `id`,`rep_tmp_amount`.`vcc_id` AS `企业`,`rep_tmp_amount`.`que_id` AS `技能组`,(case `rep_tmp_amount`.`type` when 1 then _utf8'IVR呼入' when 2 then _utf8'人工呼入' when 3 then _utf8'放弃' when 4 then _utf8'接通' when 5 then _utf8'技能组呼入' when 6 then _utf8'技能组放弃' when 7 then _utf8'技能组接通' else `rep_tmp_amount`.`type` end) AS `类型`,from_unixtime(`rep_tmp_amount`.`starttime`) AS `时间` from `rep_tmp_amount` order by `rep_tmp_amount`.`id` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-17 14:37:34
