/*!40101 SET NAMES utf8 */;

use ccod;

--
-- Insert into cc_admins the default user
--

INSERT INTO `cc_admins` (`id`, `account`, `password`, `last_login`, `last_ip`, `action_list`, `remark`, `add_time`) VALUES
  (1, 'wintel', '687e6c0950535b6875a265353db6afd3', '', '', 'all', '', NOW());

--
-- 转存表中的数据 `menus`
--

INSERT INTO `menus` (`id`, `menu_text`, `menu_url`, `has_children`, `parent_id`, `menu_icon`, `sort_weight`, `help_url`, `is_show`) VALUES
(1, '系统管理', '', 1, 0, 'fa-home', 1, NULL, 1),
(2, '菜单管理', 'icsoc_menu_homepage', 0, 1, NULL, 1, NULL, 1),
(6, '角色管理', 'icsoc_security_role_info', 0, 1, NULL, 2, '角色管理.pdf', 1),
(7, '技能组管理', 'icsoc_queue_index', 0, 1, NULL, 0, '技能组管理.pdf', 1),
(8, '坐席管理', 'icsoc_agent_index', 0, 1, NULL, 0, '坐席管理.pdf', 1),
(9, '分机管理', 'icsoc_phone_manage_index', 0, 1, NULL, 0, '分机管理.pdf', 1),
(11, '语音管理', 'icsoc_sound_index', 0, 1, NULL, 0, '语音管理.pdf', 1),
(36, 'IVR流程管理', 'icsoc_ivr_list', 0, 1, NULL, 0, 'IVR流程管理.pdf', 1),
(37, '高级设置', '', 1, 0, 'fa-cogs', 9, NULL, 1),
(38, '黑名单', 'icsoc_namelist_black_index', 0, 37, NULL, 0, '黑名单.pdf', 1),
(39, '公告管理', 'icsoc_notice_index', 0, 37, NULL, 0, '公告管理.pdf', 1),
(40, '白名单', 'icsoc_namelist_white_index', 0, 37, NULL, 0, '白名单.pdf', 1),
(41, '系统日志', 'icsoc_log_index', 0, 37, NULL, 0, '公告管理.pdf', 1),
(42, '统计报表', '', 1, 0, 'fa-calculator', 19, NULL, 1),
(44, '自定义报表', 'icsoc_custom_report_home', 0, 42, NULL, 0, '自定义报表.pdf', 1),
(57, '呼叫中心话务报表', 'icsoc_report_system_home', 0, 62, NULL, 1, '呼叫中心话务报表.pdf', 1),
(58, '技能组话务报表', 'icsoc_report_queue_home', 0, 62, NULL, 2, '技能组话务报表.pdf', 1),
(59, '坐席工作表现报表', 'icsoc_report_agent_home', 0, 62, NULL, 3, '坐席工作表现报表.pdf', 1),
(60, '日程规则', 'icsoc_rules_list', 0, 1, NULL, 0, '日程规则.pdf', 1),
(61, '明细报表', '', 1, 42, '', 1, NULL, 1),
(62, '数据报表', '', 1, 42, '', 2, NULL, 1),
(63, '呼入明细报表', 'icsoc_report_callin', 0, 61, NULL, 1, '呼入明细报表.pdf', 1),
(64, '呼出明细报表', 'icsoc_report_callout', 0, 61, NULL, 2, '呼出明细报表.pdf', 1),
(65, '坐席通话明细报表', 'icsoc_report_call', 0, 61, NULL, 3, '坐席通话明细报表.pdf', 1),
(66, '坐席操作明细报表', 'icsoc_report_agentstate', 0, 61, NULL, 4, '坐席操作明细报表.pdf', 1),
(67, '咨询三方通话详单', 'icsoc_report_conference', 0, 61, NULL, 5, '咨询三方通话详单.pdf', 1),
(68, '监听强插通话详单', 'icsoc_report_monitor', 0, 61, NULL, 6, '监听强插通话详单.pdf', 1),
(69, '漏话明细报表', 'icsoc_report_lost', 0, 61, NULL, 7, '漏话明细报表.pdf', 1),
(70, '技能组转移明细报表', 'icsoc_report_queueTranscall', 0, 61, NULL, 8, '技能组转移明细报表.pdf', 1),
(71, '短信记录', 'icsoc_report_sms', 0, 61, NULL, 9, '短信记录.pdf', 1),
(72, '技能组来电分配报表', 'icsoc_report_inallot', 0, 62, NULL, 4, '技能组来电分配报表.pdf', 1),
(73, '图形报表', '', 1, 42, '', 3, NULL, 1),
(74, '坐席通话量图表', 'icsoc_report_charts_agentcall', 0, 73, NULL, 1, '坐席通话量图表.pdf', 1),
(75, '坐席工作量图表', 'icsoc_report_charts_agentwork', 0, 73, NULL, 2, '坐席工作量图表.pdf', 1),
(76, '技能组通话量图表', 'icsoc_report_charts_queuecall', 0, 73, NULL, 3, '技能组通话量图表.pdf', 1),
(77, '呼入地区分析图表', 'icsoc_report_charts_inarea', 0, 73, NULL, 5, '呼入地区分析图表.pdf', 1),
(78, '按小时进线图表', 'icsoc_report_charts_inhour', 0, 73, NULL, 6, '按小时进线图表.pdf', 1),
(79, '手机固话分析图表', 'icsoc_report_charts_calltype', 0, 73, NULL, 7, '手机固话分析图表.pdf', 1),
(81, '系统监控', '', 1, 0, 'fa-bar-chart', 5, NULL, 1),
(82, '系统监控', 'icsoc_monitor_system_monitor', 0, 81, '0', 0, '系统监控.pdf', 1),
(83, '坐席监控', 'icsoc_monitor_agent_monitor', 0, 81, '0', 0, '系统监控.pdf', 1),
(84, '监控配置', 'icsoc_monitor_conf_monitor', 0, 81, '0', 1, '系统监控.pdf', 1),
(85, '满意度评价汇总表', 'icsoc_report_evaluatecollec', 0, 62, NULL, 6, '满意度评价汇总表.pdf', 1),
(86, '满意度评价图表', 'icsoc_report_charts_evaluate', 0, 73, NULL, 4, '满意度评价图表.pdf', 1),
(88, '呼叫中心监控', 'icsoc_monitor_call_center_monitor', 0, 81, '0', 0, '系统监控.pdf', 1),
(89, '录音管理', '', 1, 0, 'fa-music', 5, NULL, 1),
(90, '录音列表', 'icsoc_recording_list', 0, 89, '0', 0, '录音列表.pdf', 1),
(91, '留言列表', 'icsoc_recording_message_lsit', 0, 89, '0', 0, '留言列表.pdf', 1),
(94, '参数设置', 'icsoc_security_setting', 0, 0, '0', 42, '参数设置.pdf', 0),
(96, '帮助向导', '', 0, 0, '0', 41, '登录与帮助.pdf', 0),
(97, '置忙原因设置', 'icsoc_agentstat_index', '0', '37', '', '0', null, '1'),
(98, '业务组管理', 'icsoc_business_group_index', '0', '1', '', '0', null, '1'),
(99, '业务组工作表现报表', 'icsoc_report_group_home', '0', '62', '', '3', null, '1'),
(100, 'IVR轨迹统计报表', 'icsoc_ivr_sta', '0', '62', '', '4', null, '1'),
(101, '技能组通话明细', 'icsoc_queue_call_details', '0', '62', '', '3', null, '1');