<?php

namespace Icsoc\InstallBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('IcsocInstallBundle:Default:index.html.twig', array('name' => $name));
    }
}
