<?php
namespace Icsoc\QueueBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class QueueType extends AbstractType
{
    private $container;
    private $translations;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->translations = $this->container->get('translator');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        $vccCode = $this->container->get('security.token_storage')
            ->getToken()->getUser()->getVccCode();
        $doctrine = $this->container->get('doctrine.orm.entity_manager');
        $winSounds = $doctrine->getRepository("IcsocSecurityBundle:WinSounds")->getSoundNames($vccCode, 1); //获取技能语音
        $builder
            ->add('que_name', 'text', array(
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'label'=>'Skill group name',
                ))
            ->add('que_num', 'text', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                'label'=>'Skill group number',
                'required'=>false,
            ))
            ->add('que_priority', 'text', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                'label'=>'The skill set priority',
                'help'=>'The range of skills set of priorities for 0-255, the smaller the number, the higher the priority',
                'required'=>true,
                'data' => isset($data['que_priority']) ? $data['que_priority'] : 100,
            ))
            ->add('que_type', 'choice', array(
                    'attr' => array(
                        'class' => 'col-xs-10 col-sm-4'
                    ),
                    'label' => 'The skill set of type',
                    'choices' => $this->container->getParameter('que_type'),
                    'data' => isset($data['que_type']) ? $data['que_type'] : 0,
                ))
            ->add('que_strategy', 'choice', array(
                    'attr' => array(
                        'class' => 'col-xs-10 col-sm-4'
                    ),
                    'label' => 'Distribution strategy',
                    'choices' => $this->container->getParameter('que_strategy'),
                    'data' => isset($data['que_strategy']) ? $data['que_strategy'] : 0,
                ))
//            ->add('brecord', 'choice', array(
//                    'label'=>'Is studio',
//                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
//                    'choices'=>array('No', 'Yes'),
//                    'data'=>isset($data['brecord']) ? $data['brecord'] : 1,
//                    'expanded'=>true,
//                    'multiple'=>false,
//                    //'required'=>false,
//                ))
            ->add('que_length', 'text', array(
                    'label'=>'The maximum queue number',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'data'=>isset($data['que_length']) ? $data['que_length'] : 15
                ))
            ->add('que_time', 'text', array(
                    'label'=>'The queue length',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'data'=>isset($data['que_time']) ? $data['que_time'] : 120
                ))
            ->add('ring_time', 'text', array(
                    'label'=>'Seat ringing duration',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'data'=>isset($data['ring_time']) ? $data['ring_time'] : 30
                ))
            ->add('next_wait', 'text', array(
                    'label'=>'Post processing time',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'data'=>isset($data['next_wait']) ? $data['next_wait'] : 5
                ))
            ->add('b_announce', 'choice', array(
                    'label'=>'Whether the reported number',
                    'choices'=>array('No', 'Yes'),
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'data'=>isset($data['b_announce']) ? $data['b_announce'] : 1,
                    'expanded'=>true,
                    'multiple'=>false,
                ))
            /*->add('auto_evaluate', 'choice', array(
                'label'=>'Auto evaluate',
                'choices'=>array('No', 'Yes'),
                'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                'data'=>isset($data['auto_evaluate']) ? $data['auto_evaluate'] : 1,
                'expanded'=>true,
                'multiple'=>false,
                'help' => 'Auto evaluate help'
            ))*/
            ->add('noans_times', 'text', array(
                    'label'=>'No response times',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'data'=>isset($data['noans_times']) ? $data['noans_times'] : 3
                ))
            ->add('noans_wait', 'text', array(
                    'label'=>'No response of waiting time',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'data'=>isset($data['noans_wait']) ? $data['noans_wait'] : 10
                ))
            ->add('noans_action', 'choice', array(
                    'label'=>'No response operation',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'choices'=> $this->container->getParameter('noansweraction'),
                    'data'=>isset($data['noans_action']) ? $data['noans_action'] : 0,
                    'expanded'=>true,
                    'multiple'=>false,
                ))
            ->add('wait_audio', 'choice', array(
                    'label'=>'Queue waiting tone',
                    'placeholder'=>'Default',
                    'choices'=>$winSounds,
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'data'=> isset($data['wait_audio']) ? $data['wait_audio'] : 0,
                ))
            ->add('tellevel', 'choice', array(
                'label'=>'Custom hangup Whether the reported number',
                'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                'choices'=>array('No', 'Yes'),
                'data'=>isset($data['tellevel']) ? $data['tellevel'] : 0,
                'expanded'=>true,
                'multiple'=>false,
            ))
            ->add('que_id', 'hidden')
        ;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'que_info_form';
    }
}
