<?php
namespace Icsoc\GroupBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GroupType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('groupName', 'text', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-3'),
                'label'=>'Group name',
                'required'=>true,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Icsoc\SecurityBundle\Entity\WinGroup',
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'group_info_form';
    }
}
