<?php
namespace Icsoc\AgentBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AgentType
 * @package Icsoc\AgentBundle\Form\Type
 */
class AgentType extends AbstractType
{
    private $container;
    private $translations;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->translations = $this->container->get('translator');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $vccId = $user->getVccId();
        $roleGrade = $user->getRoleGrade();
        $doctrine = $this->container->get('doctrine.orm.entity_manager');
        $queues = $doctrine->getRepository("IcsocSecurityBundle:WinQueue")
            ->getQuesName("q.vccId = :vccId AND q.isDel = 0", array('vccId'=>$vccId));
        //修改和添加显示角色不一样；
        if (!isset($data['agent_id']) || (!empty($data['agent_id']) && $roleGrade < 3)) {
            $roleWhere = "AND r.roleGrade > :roleGrade";
        } else {
            $roleWhere = "AND r.roleGrade = :roleGrade";
        }
        $roles = $doctrine->getRepository("IcsocSecurityBundle:CcRoles")
            ->getRolesName('r.vccId = :vccId '.$roleWhere, array('vccId'=>$vccId, 'roleGrade'=>$roleGrade));
        $selectDataRole =  $this->container->getParameter('select_data_role');
        //$dataRole = $this->container->get("icsoc_core.common.class")->getUserTypes($selectDataRole, $roleGrade);
        $isAdd = isset($data['agent_id']) ? '' : 1;
        $frontRole = $this->container->get("icsoc_core.common.class")->getFrontAgentRole($roleGrade, $isAdd);
        //crm 权限；
        $crmData = array();
        if ($roleGrade < 2) {
            $crmData[0] = $selectDataRole[0];
        }
        if ($roleGrade < 3) {
            $crmData[1] = $selectDataRole[1];
            $crmData[2] = $selectDataRole[2];
        }
        //如果坐席登录了就不能让其修改成非坐席
//        if (isset($data['ag_type']) && $data['ag_type'] <> 1) {
//            unset($frontRole[-1]);
//        }
        //是否启用业务组
        $firstOptions = array(
                'label'=>'new password',
                'help'=>'Used to log in to the system using a password, default is 123456',
        );
        $secondOptions = array('label' => 'confirm password');
        if (isset($data['ag_num'])) {
            $firstOptions['attr'] = $secondOptions['attr'] = array(
                'hide'=>true,
                "class"=>'col-xs-10 col-sm-4',
            );
        }
        $aentNumDisable = isset($data['ag_num']) && !empty($data['ag_num']) ? true : false;
        $agStatus = isset($data['ag_status']) ? $data['ag_status'] : 1;
        $builder
            ->add('ag_num', 'text', array(
                    'attr'=>array('class'=>'col-xs-10 col-sm-4', 'disabled'=>$aentNumDisable),
                    'label'=>$this->translations->trans('Agent number'),
                ))
            ->add('ag_password', 'repeated', array(
                    'options'=>array(
                        'attr'=>array(
                            'class'=>'col-xs-10 col-sm-4',
                        'value'=>isset($data['ag_password']['first'])?$data['ag_password']['first']:'',
                        ),
                    ),
                    'type'=>'password', //判断是否是修改
                    'required'=>true,
                    'mapped' => false,
                    'invalid_message'=>'This value is not valid',
                    'first_options'  =>$firstOptions,
                    'second_options' =>$secondOptions,
                ))
            ->add('ag_name', 'text', array(
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'label'=>'Username',
                    'required'=>false,
                ))
            ->add('ag_nickname', 'text', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                'label'=>'Nickname',
                'required'=>false,))
            ->add('user_role', 'choice', array(
                    'label'=>'User role',
                    'placeholder'=>'Please select',
                    'choices'=> $roles,
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'data'=> isset($data['user_role']) ? $data['user_role'] : 0,
                ))
            //所属技能组（相当于分配）
            ->add('belong_queues', 'choice', array(
                    'label'=>'Belongs to the skill set',
                    'mapped'=>false,
                    'choices'=> $queues,
                    'placeholder'=>'Default',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4 multiple-select'),
                    'multiple'=>true,
                    'expanded'=>false,
                    'required'=>false,
                    'data'=>isset($data['belong_queues']) ? $data['belong_queues'] : array(),
                ))
            ->add('select_data', 'choice', array(
                    'label'=>'See the data authority',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4', 'onclick'=>"switchOption('select_data')"),
                    'choices'=>array('Open', 'Fold'),
                    'data'=>1,
                    'expanded'=>true,
                    'multiple'=>false,
                )) //后台数据权限
            /*->add('user_type', 'choice', array(
                    'label'=>'See the background data authority',
                    'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                    'choices'=>$dataRole,
                    'data'=>isset($data['user_type']) ? $data['user_type'] : 0,
                    'expanded'=>true,
                    'multiple'=>false,
                ))*/ //前数据权限
            ->add('ag_role', 'choice', array(
                    'label'=>'The front seat type',
                    'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                    'choices'=>$frontRole,
                    'data'=>isset($data['ag_role']) ? $data['ag_role'] : 1,
                    'expanded'=>true,
                    'multiple'=>false,
                ))
            ->add('agent_authority', 'choice', array(
                    'label'=>'Agent authority',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4', 'onclick'=>"switchOption('agent_authority')"),
                    'choices'=>array('Open', 'Fold'),
                    'data'=>1,
                    'expanded'=>true,
                    'multiple'=>false,
                ))
            ->add('ag_status', 'choice', array(
                    'label'=>'坐席状态',
                    'attr'=>array('class'=>'col-xs-10 col-sm-4', 'onclick'=>""),
                    'choices'=>array(1 => '在职', 2 => '离职'),
                    'data'=>$agStatus,
                    'expanded'=>true,
                    'multiple'=>false,
                ))
            ->add('crm_datarole', 'choice', array(
                    'label'=>'CRM authority',
                    'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                    'choices'=>$crmData,
                    'data'=>isset($data['crm_datarole']) ? $data['crm_datarole'] : 1,
                    'expanded'=>true,
                    'multiple'=>false,
                ))
            ->add('id', 'hidden');

        //为了保证马蜂窝以前用法特殊处理
//        if ($vccId == '2000135') {
//            $builder->add('user_type', 'choice', array(
//                'label'=>'See the background data authority',
//                'attr'=>array('class'=>'col-xs-10 col-sm-5'),
//                'choices'=>$dataRole,
//                'data'=>isset($data['user_type']) ? $data['user_type'] : 0,
//                'expanded'=>true,
//                'multiple'=>false,
//            )); //前数据权限
//        }
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($doctrine, $vccId, $roleGrade, $queues) {
            //$data = $event->getData();
            $form = $event->getForm();
            //$queDisabled = (isset($data['user_type']) && $data['user_type'] == 1) ? false : true;
            //$groupDisabled = (isset($data['user_type']) && $data['user_type'] == 3) ? false : true;
            $isEnableGroup = $doctrine->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
            $groups = $doctrine->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
            if ($isEnableGroup) {
                $form->add('group_id', 'choice', array(
                        'label'=>'The business group',
                        'placeholder'=>'Please select',
                        'choices'=> $groups,
                        'required' => false,
                        'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                ));
                //管理业务组 8015040801 马蜂窝特殊处理
//                if ($vccId == '2000135') {
//                    $form->add('user_groups', 'choice', array(
//                        'label'=>'Management of business group',
//                        'attr'=>array('class'=>'col-xs-10 col-sm-4 multiple-select', 'disabled'=>$groupDisabled),
//                        'placeholder'=>'Default',
//                        'choices'=> $groups,
//                        'required'=>false,
//                        'expanded'=>false,
//                        'multiple'=>true,
//                    ));
//                }
            }
//            if ($roleGrade < 2) {
//                //管理技能组 8015040801 马蜂窝特殊处理
//                if ($vccId == '2000135') {
//                    $form->add('user_queues', 'choice', array(
//                            'label'=>'Management skill set',
//                            'attr'=>array('class'=>'col-xs-10 col-sm-4 multiple-select', 'disabled'=>$queDisabled),
//                            'placeholder'=>'Default',
//                            'choices'=> $queues,
//                            'required'=>false,
//                            'expanded'=>false,
//                            'multiple'=>true,
//                    ));
//                }
//            }
        });
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'agent_info_form';
    }
}
