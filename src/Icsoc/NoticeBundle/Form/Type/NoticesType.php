<?php
namespace Icsoc\NoticeBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NoticesType extends AbstractType
{
    /**
     * @var ContainerInterface $container
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                    'attr'=>array('class'=>'col-xs-10 col-sm-3'),
                ))
            ->add('startDate', 'text', array(
                    'attr'=>array('class'=>'col-xs-12 col-sm-12','divClass'=>'col-sm-2'),
                    'label'=>'Show time'
                    //'widget'=>'single_text'
                ))
            ->add('endDate', 'text', array(
                    'attr'=>array('class'=>'col-xs-12 col-sm-12','divClass'=>'col-sm-2'),
                ))
            ->add('content', 'hidden', array('attr'=>array('class'=>'wysiwyg-editor')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Icsoc\SecurityBundle\Entity\CcAnnouncement',
            ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'notice_info_form';
    }
}
