<?php

namespace Icsoc\EvaluateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ChangeDataFormatCommand
 *
 * @package Icsoc\SecurityBundle\Command
 */

class ChangeEvaluateConfigCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('role:changeEvaluateConfig')
            ->setDescription('修改满意度配置的数据格式');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $conn = $this->getContainer()->get('doctrine.dbal.default_connection');
        $evaluateConfig = $conn->fetchAll('select * from cc_evaluates ORDER BY play_order ASC ');

        $res = array();
        $tmpArr = array();
        $delIds = array();
        foreach ($evaluateConfig as $k => $config) {
            if (isset($tmpArr[$config['vcc_id']])) {
                if (!empty($config['valid_keys'])) {
                    $tmpConfig = array(
                        'sound_id' => $config['sound_id'],
                        'play_order' => $config['play_order'],
                        'valid_keys' => explode(',', $config['valid_keys']),
                        'vcc_id' => $config['vcc_id']
                    );
                    foreach ($tmpArr[$config['vcc_id']] as $id => $evaluate) {
                        $tmpConfig['id'] = $id;
                        $tmpArr[$config['vcc_id']][$id][] = $tmpConfig;
                    }
                }
                $delIds[] = $config['id'];
            } else {
                if (!empty($config['valid_keys'])) {
                    $tmpArr[$config['vcc_id']][$config['id']][] = array(
                        'id' => $config['id'],
                        'sound_id' => $config['sound_id'],
                        'play_order' => $config['play_order'],
                        'valid_keys' => explode(',', $config['valid_keys']),
                        'vcc_id' => $config['vcc_id']
                    );
                }
            }
        }

        foreach ($tmpArr as $vccId => $config) {
            $index = 0;
            foreach ($config as $id => $val) {
                foreach ($val as $item => $itemConfig) {
                    $index++;
                    $itemEvaluate = array();
                    $itemEvaluate['old_evaluate'] = 1;
                    $itemEvaluate['id'] = 'evaluate-'.$itemConfig['vcc_id'].'-'.$index;
                    $itemEvaluate['sound_id'] = $itemConfig['sound_id'];
                    $itemEvaluate['sound_address'] = $this->getContainer()
                        ->get('icsoc_data.model.evaluate')
                        ->getSoundAddress($itemConfig['sound_id']);
                    $itemEvaluate['play_times'] = 3;
                    $itemEvaluate['valid_keys'] = $itemConfig['valid_keys'];
                    foreach ($itemEvaluate['valid_keys'] as $k => $v) {
                        $itemEvaluate['keys'][$v]['name'] = '按键'.$v;
                        $itemEvaluate['keys'][$v]['id'] = 'evaluate-'.$itemConfig['vcc_id'].'-'.$index.'-'.$v;
                        $itemEvaluate['keys'][$v]['play_times'] = 3;
                    }
                    $res[$id][] = $itemEvaluate;
                }
            }
        }

        try {
            $conn->beginTransaction();
            foreach ($res as $id => $idConfig) {
                $arr['evaluate_config'] = json_encode($idConfig);
                $conn->update('cc_evaluates', $arr, array('id' => $id));
            }
            foreach ($delIds as $delId) {
                $conn->delete('cc_evaluates', array('id' => $delId));
            }
            $conn->commit();

            return array('code'=>200, 'message'=>'ok');
        } catch (\Exception $e) {
            $conn->rollBack();

            return array('code'=>404, 'message'=>'满意度格式修改失败');
        }
    }
}
