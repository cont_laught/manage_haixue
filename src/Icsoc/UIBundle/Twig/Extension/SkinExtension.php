<?php
/**
 * This file is part of easycrm.
 * Author: louxin
 * Date: 14-7-31
 * Time: 21:10
 * File: SkinExtension.php
 */

namespace Icsoc\UIBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

class SkinExtension extends \Twig_Extension
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getGlobals()
    {
        $skin = $this->container->get('request')->cookies->get('ace_skin', 'no-skin');
        return array(
            'skin' => $skin
        );
    }

    public function getName()
    {
        return 'skin_extension';
    }
}
