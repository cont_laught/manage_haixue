ACE插件的使用
=========

主体头部CSS,JS
-------

        {% block stylesheet %}
            {% stylesheets
                'bundles/icsocui/css/bootstrap.min.css'
                'bundles/icsocui/css/font-awesome.min.css'
                'bundles/icsocui/css/ace-fonts.css'
                'bundles/icsocui/css/ace-skins.min.css'
                 filter="cssrewrite"
            %}
            <link rel="stylesheet" href="{{ asset_url }}" />
            {% endstylesheets %}
        {% endblock %}

        {% block stylesheet_footer %}
            {% stylesheets
            'bundles/icsocui/css/ace.min.css'
            filter="cssrewrite"
            %}
            <link rel="stylesheet" href="{{ asset_url }}" />
            {% endstylesheets %}
        {% endblock %}

        {%  block head_script %}
            {% javascripts
            'bundles/icsocui/js/ace-extra.min.js'
            %}
            <script type="text/javascript" src="{{ asset_url }}"></script>
            {% endjavascripts %}
        {% endblock %}

主体底部JS
--------

    {% block javascript %}
        <!--[if !IE]> -->
        <script src="{{ asset('bundles/icsocui/js/jquery.min.js') }}"></script>
        <!-- <![endif]-->
        <!--[if lte IE 9]>
        <script src="{{ asset('bundles/icsocui/js/jquery-1.11.1.min.js') }}"></script>
        <![endif]-->
        {% javascripts
        'bundles/icsocui/js/bootstrap.min.js'
        %}
        <script type="text/javascript" src="{{ asset_url }}"></script>
        {% endjavascripts %}
    {% endblock %}

    {# 始终保持在最下面的javascript文件 #}
    {% block javascript_footer %}
        {% javascripts
        'bundles/icsocui/js/ace-elements.min.js'
        'bundles/icsocui/js/ace.min.js'
        %}
        <script type="text/javascript" src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script type="application/javascript">
            $('#ace-settings-skin').on('change', function() {
                ace.settings.set('skin', this.value);
            });
        </script>
    {% endblock %}

jqgrid列表插件的使用
------
####jqgrid使用到的css
    {% extends 'IcsocUIBundle::base.html.twig' %}
    {% block stylesheet %}
        {{ parent() }} #使用父类的样式
        {% stylesheets
           '@jqgird_css'  #jqgrid使用到的css
           '@datepicker_css' #搜索用到日期插件的css,如果不需要可以去掉
        filter='cssrewrite' filter='uglifycss'
        %}
        <link rel="stylesheet" href="{{ asset_url }}" />
        {% endstylesheets %}
    {% endblock %}
    {% block content %}

####jqgrid使用到的js(demo/jqgird)
    {% block footer %}
       {% javascripts
           '@datepicker_js' #搜索用到的日期插件js 如果不需要可以去掉
           '@jqgird_js'     #jqgrid主要js文件
           filter='uglifyjs'
       %}
       <script src="{{ asset_url }}"></script>
       {% endjavascripts %}
       <script type="text/javascript">
        具体js操作列表查看jqgrid官方文档；
       </script>
    {% endblock %}

datepicker插件的使用
------
####datepicker 所用到的css
    '@datepicker_css' 可以参考上面jqgrid的用法
####datepicker 所用到的js
    '@datepicker_js' 可以参考上面jqgrid的用法

wysiwyg,markdown编辑器(demo/editer)
------
####编辑器 所用到的css
    无需额外css包含了父类就行了
####编辑器 所用到的JS
    {% block footer %}
        {% javascripts
        '@wysiwyg_js'  #wysiwyg所需jS
        '@markdown_js' #markdown所需jS
        filter='uglifyjs's
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script>
          jQuery(function($){
            #id=editor1的textarea元素
            $('#editor1').ace_wysiwyg().prev().addClass('wysiwyg-style2');
         });
        </script>
    {% endblock %}

colorbox相册插件(参考路由 demo/colorbox)
-------
####colorbox 所用到的css
    {% block stylesheet %}
       {{ parent() }}
       {% stylesheets
       '@colorbox_css' #colorbox所用到的css
       filter='cssrewrite' filter='uglifycss'
       %}
       <link rel="stylesheet" href="{{ asset_url }}" />
       {% endstylesheets %}
    {% endblock %}
####colorbox 所用到的js
    {% block footer %}
       {% javascripts
       '@colorbox_js'
       filter='uglifyjs'
       %}
       <script src="{{ asset_url }}"></script>
       {% endjavascripts %}
       <script>
           var colorbox_params = {
               rel: 'colorbox',
               reposition: true,
               scalePhotos: true,
               scrolling: false,
               previous: '<i class="ace-icon fa fa-arrow-left"></i>',
               next: '<i class="ace-icon fa fa-arrow-right"></i>',
               close: '&times;',
               current: '{current} of {total}',
               maxWidth: '100%',
               maxHeight: '100%',
               onComplete: function(){
                   $.colorbox.resize();
               }
           }
           $('[data-rel="colorbox"]').colorbox(colorbox_params);
           $('#cboxLoadingGraphic').append("<i class='ace-icon fa fa-spinner orange'></i>");
       </script>
    {% endblock %}


chosen下拉选择插件(参考路由 demo/chosen)
---------
####chosen 官方文档
<a href="http://harvesthq.github.io/chosen/">官方文档</a>
####chosen 所用到的css
    {% extends 'IcsocUIBundle::base.html.twig' %}
        {% block stylesheet %}
            {{ parent() }}
            {% stylesheets
            '@chosen_css' #chosen的css
            filter='cssrewrite' filter='uglifycss'
            %}
            <link rel="stylesheet" href="{{ asset_url }}" />
            {% endstylesheets %}
        {% endblock %}
####chosen 所用到的js
    {% block footer %}
        {% javascripts
        '@chosen_js' #chosen的js
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script>
            //单选 chosen-select为下拉列表的class
            $('.chosen-select').chosen({allow_single_deselect:true});
            //resize the chosen on window resize
            $(window).on('resize.chosen', function() {
                var w = $('.chosen-select').parent().width();
                console.log($('.chosen-select').next());
                $('.chosen-select').next().css({'width':w});
            }).trigger('resize.chosen');
            //多选 chosen-multiple-style 按钮的ID可以切换不同样式
            $('#chosen-multiple-style').on('click', function(e){
                var target = $(e.target).find('input[type=radio]');
                var which = parseInt(target.val());
                if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
                else $('#form-field-select-4').removeClass('tag-input-style');
            });
        </script>
    {% endblock %}
###maskinput 所用到的js(参考路由 demo/maskinput)
    {% block javascript_footer %}
        {% javascripts
        '@maskInput_js'
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        {{ parent() }}
    {% endblock %}
###limiter,autosize 所用到的js(参考路由 demo/inputlimiter)
    {% block footer %}
        {% javascripts
        '@limiter_js' #输入最大值的控制
        '@autosize_js' #文本域自动随着内容变大
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script>
            jQuery(function($){
                $('textarea[class*=autosize]').autosize({append: "\n"});
                $('textarea.limited').inputlimiter({
                    remText: '%n character%s remaining...',
                    limitText: 'max allowed : %n.'
                });
            });
        </script>
    {% endblock %}
###spinner 所用到的js(参考路由 demo/spinner)
    {% block footer %}
        {% javascripts
        '@spinner_js'
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script>
            jQuery(function($){
                $('#spinner1').ace_spinner({value:0,min:0,max:200,step:10, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
                        .on('change', function(){
                            ///alert(this.value)
                        });
                $('#spinner2').ace_spinner({value:0,min:0,max:10000,step:100, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up', icon_down:'ace-icon fa fa-caret-down'});
                $('#spinner3').ace_spinner({value:0,min:-100,max:100,step:10, on_sides: true, icon_up:'ace-icon fa fa-plus smaller-75', icon_down:'ace-icon fa fa-minus smaller-75', btn_up_class:'btn-success' , btn_down_class:'btn-danger'});

            });
        </script>
    {% endblock %}
###spinner 所用到的js(参考路由 demo/tree)
    {% block footer %}
        {% javascripts
        '@tree_js'
        '@IcsocUIBundle/Resources/public/plugin/fuelux/fuelux.tree-sample-demo-data.js' #这个是测试数据,不用包含进来
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script>
            $('#tree1').ace_tree({
                dataSource : treeDataSource ,
                multiSelect : true,
                loadingHTML : '<div class="tree-loading"><i class="ace-icon fa fa-refresh fa-spin blue"></i></div>',
                'open-icon' : 'ace-icon tree-minus',
                'close-icon' : 'ace-icon tree-plus',
                'selectable' : true,
                'selected-icon' : 'ace-icon fa fa-check',
                'unselected-icon' : 'ace-icon fa fa-times'
            });
        </script>
    {% endblock %}
###validate 所用到的js(参考路由 demo/validate)
    {% block footer %}
        {% javascripts
        '@jqvalidate_js'
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script>
            $("#commentForm").validate();
        </script>
    {% endblock %}
dropzone文件上传预览插件(参考路由 demo/chosen)
---------
####dropzone 官方文档
<a href="http://www.dropzonejs.com/">官方文档</a>
####dropzone 所用到的js(参考路由 demo/dropzone)
    {% block footer %}
        {% javascripts
        '@dropzone_js'
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script type="text/javascript">
            jQuery(function($){
                Dropzone.autoDiscover = false;
                try {
                    var myDropzone = new Dropzone("#dropzone" , {
                        paramName: "file", // The name that will be used to transfer the file
                        maxFilesize: 0.5, // MB
                        addRemoveLinks : true,
                        dictDefaultMessage :
                                '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \
                                <span class="smaller-80 grey">(or click)</span> <br /> \
                                <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>'
                        ,
                        dictResponseError: 'Error while uploading file!',
                        //change the previewTemplate to use Bootstrap progress bars
                        previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-details\">\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n    <div class=\"dz-size\" data-dz-size></div>\n    <img data-dz-thumbnail />\n  </div>\n  <div class=\"progress progress-small progress-striped active\"><div class=\"progress-bar progress-bar-success\" data-dz-uploadprogress></div></div>\n  <div class=\"dz-success-mark\"><span></span></div>\n  <div class=\"dz-error-mark\"><span></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>"
                    });
                } catch(e) {
                    alert('Dropzone.js does not support older browsers!');
                }
            });
        </script>
    {% endblock %}
####dropzone 所用到的css
    {% extends 'IcsocUIBundle::base.html.twig' %}
        {% block stylesheet %}
            {{ parent() }}
            {% stylesheets
            '@dropzone_css' #dropzone的css
            filter='cssrewrite' filter='uglifycss'
            %}
            <link rel="stylesheet" href="{{ asset_url }}" />
            {% endstylesheets %}
    {% endblock %}
colorpicker颜色选择器插件(参考路由 demo/colorpicker)
---------
####colorpicker 官方文档
<a href="http://www.eyecon.ro/bootstrap-colorpicker/">官方文档</a>
####colorpicker 所用到的js(参考路由 demo/colorpicker)
    {% block footer %}
        {% javascripts
        '@colorpicker_js'  #colorpicker所用到js
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script type="text/javascript">
            $('#colorpicker1').colorpicker();
        </script>
    {% endblock %}
####colorpicker 所用到的css
    {% block stylesheet %}
        {{ parent() }}
        {% stylesheets
        '@colorpicker_css' #colorpicker所用到css
        filter='cssrewrite' filter='uglifycss'
        %}
        <link rel="stylesheet" href="{{ asset_url }}" />
        {% endstylesheets %}
    {% endblock %}
Bootbox弹框插件(参考路由 demo/bootbox)
---------
####bootbox 官方文档
<a href="http://bootboxjs.com/">官方文档</a>
####bootbox 所用到的js(参考路由 demo/colorpicker)
    {% block footer %}
        {% javascripts
        '@bootbox_js'
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script type="text/javascript">
            $('#some-button2').on('click', function() {
                bootbox.confirm('Are you sure?', function(result) {
                    if(result) {
                        //do something
                    }
                })
            })
        </script>
    {% endblock %}
Gritter消息提醒插件(参考路由 demo/gritter)
---------
####gritter 参考资料
<a href="https://github.com/jboesch/Gritter">参考资料</a>
####gritter 所用到的js(参考路由 demo/gritter)
    {% block footer %}
        {% javascripts
        '@gritter_js' #所用到js
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script type="text/javascript">
            #具体配置看参考资料
            $('#add-gritter-light').click(function(){
                $.gritter.add({
                    // (string | mandatory) the heading of the notification
                    title: 'This is a light notification',
                    // (string | mandatory) the text inside the notification
                    text: 'Just add a "gritter-light" class_name to your $.gritter.add or globally to $.gritter.options.class_name',
                    class_name: 'gritter-light'
                });
                return false;
            });
        </script>
    {% endblock %}
####gritter 所用到的css
    {% block stylesheet %}
        {{ parent() }}
        {% stylesheets
        '@gritter_css'  #gritter所用到css
        filter='cssrewrite' filter='uglifycss'
        %}
        <link rel="stylesheet" href="{{ asset_url }}" />
        {% endstylesheets %}
    {% endblock %}
Nestable Lists可移动列表插件(参考路由 demo/nestable)
---------
####nestable 参考资料
<a href="http://dbushell.github.io/Nestable/">参考资料</a>
####nestable 所用到的js(参考路由 demo/nestable)
    {% block footer %}
        {% javascripts
        '@nestable_js'
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script type="text/javascript">
            $('.dd').nestable();
            $('.dd-handle a').on('mousedown', function(e){
                e.stopPropagation();
            });
        </script>
    {% endblock %}
slimScroll滚动条插件(参考路由 demo/slimscroll)
---------
####slimScroll 参考资料
<a href="https://github.com/rochal/jQuery-slimScroll">参考资料</a>
####slimScroll 所用到的js(参考路由 demo/slimscroll)
    {% block footer %}
        {% javascripts
        '@slimscroll_js'
        filter='uglifyjs'
        %}
        <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        <script type="text/javascript">
            $('#testDiv2').slimscroll({
                height: '200px',
                width: '500px'
            });
        </script>
    {% endblock %}
