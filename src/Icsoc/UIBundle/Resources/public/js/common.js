function jsonToString (obj){
    var THIS = this;
    switch(typeof(obj)){
        case 'string':
            return '"' + obj.replace(/(["\\])/g, '\\$1') + '"';
        case 'array':
            return '[' + obj.map(THIS.jsonToString).join(',') + ']';
        case 'object':
            if(obj instanceof Array){
                var strArr = [];
                var len = obj.length;
                for(var i=0; i<len;i++){
                    strArr.push(THIS.jsonToString(obj[i]));
            }
            return '[' + strArr.join(',') + ']';
        }else if(obj==null){
            return 'null';
        }else{
            var string = [];
            for (var property in obj) string.push(THIS.jsonToString(property) + ':' + THIS.jsonToString(obj[property]));
            return '{' + string.join(',') + '}';
        }
        case 'number':
            return obj;
        case false:
            return obj;
    }
}

function stringToJSON(obj){
    return eval('(' + obj + ')');
}

function gritterAlert(title, msg, type, timeout) {
    var $class_name = '';

    switch (type) {
        case 'success':
            $class_name = 'gritter-success';
            break;
        case 'error':
            $class_name = 'gritter-error';
            break;
        case 'info':
            $class_name = 'gritter-info';
            break;
        case 'warning':
            $class_name = 'gritter-warning';
            break;
        default:
            $class_name = '';
            break;
    }

    $.gritter.add({
        title: title,
        text: msg,
        class_name: $class_name,
        time: timeout || 5000,
        position: 'bottom-right'
    });
}

function datetime_to_int(datetime) {
    var tmp_datetime = datetime.replace(/:/g,'-');
    tmp_datetime = tmp_datetime.replace(/ /g,'-');
    var arr = tmp_datetime.split("-");
    var now = new Date(arr[0],arr[1],arr[2],arr[3],arr[4],arr[5]);
    return parseInt(now.getTime()/1000);
}

/**
 * 取消返回列表
 * @param url
 */
function cancelRedirect(url)
{
    if (url) {
        window.location.href=url;
    } else {
        window.history.go(-1);
    }
}

