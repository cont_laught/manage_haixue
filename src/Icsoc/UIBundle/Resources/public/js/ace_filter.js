/**
 Required. Ace's Basic File to Initiliaze Different Parts & Some Variables.
 */
if( !('ace' in window) ) window['ace'] = {}
if( !('helper' in window['ace']) ) window['ace'].helper = {}
if( !('vars' in window['ace']) ) {
    window['ace'].vars = {
        'icon'	: ' ace-icon ',
        '.icon'	: '.ace-icon'
    }
}
ace.vars['touch']	= 'ontouchstart' in document.documentElement;


jQuery(function($) {
    //sometimes we try to use 'tap' event instead of 'click' if jquery mobile plugin is available
    ace.click_event = ace.vars['touch'] && $.fn.tap ? 'tap' : 'click';

    //sometimes the only good way to work around browser's pecularities is to detect them using user-agents
    //though it's not accurate
    var agent = navigator.userAgent
    ace.vars['webkit'] = !!agent.match(/AppleWebKit/i)
    ace.vars['safari'] = !!agent.match(/Safari/i) && !agent.match(/Chrome/i);
    ace.vars['android'] = ace.vars['safari'] && !!agent.match(/Android/i)
    ace.vars['ios_safari'] = !!agent.match(/OS ([4-8])(_\d)+ like Mac OS X/i) && !agent.match(/CriOS/i)

    ace.vars['non_auto_fixed'] = ace.vars['android'] || ace.vars['ios_safari'];
    // for android and ios we don't use "top:auto" when breadcrumbs is fixed
    if(ace.vars['non_auto_fixed']) {
        $('body').addClass('mob-safari');
    }

    ace.vars['transition'] = 'transition' in document.body.style || 'WebkitTransition' in document.body.style || 'MozTransition' in document.body.style || 'OTransition' in document.body.style

    /////////////////////////////

    //a list of available functions with their arguments
    // >>> null means enable
    // >>> false means disable
    // >>> array means function arguments
    var available_functions = {
        'general_vars' : null,//general_vars should come first

        'handle_side_menu' : null,
        'add_touch_drag' : null,

        'sidebar_scrollable' : [
            //true, //enable only if sidebar is fixed , for 2nd approach only
            true //scroll to selected item? (one time only on page load)
            ,true //true = include shortcut buttons in the scrollbars
            ,false || ace.vars['safari'] || ace.vars['ios_safari'] //true = include toggle button in the scrollbars
            ,200 //> 0 means smooth_scroll, time in ms, used in first approach only, better to be almost the same amount as submenu transition time
            ,false//true && ace.vars['touch'] //used in first approach only, true means the scrollbars should be outside of the sidebar
        ],

        'sidebar_hoverable' : null,//automatically move up a submenu, if some part of it goes out of window

        'general_things' : null,

        'widget_boxes' : null,
        'widget_reload_handler' : null,

        'settings_box' : null,//settings box
        'settings_rtl' : null,
        'settings_skin' : null,

        'enable_searchbox_autocomplete' : null,

        'auto_hide_sidebar' : null,//disable?
        'auto_padding' : null,//disable
        'auto_container' : null//disable
    };

    //enable these functions with related params
    for(var func_name in available_functions) {
        if(!(func_name in ace)) continue;

        var args = available_functions[func_name];
        if(args === false) continue;//don't run this function

        else if(args == null) args = [jQuery];
        else if(args instanceof String) args = [jQuery, args];
        else if(args instanceof Array) args.unshift(jQuery);//prepend jQuery

        ace[func_name].apply(null, args);
    }

})



ace.general_vars = function($) {
    var minimized_menu_class  = 'menu-min';
    var responsive_min_class  = 'responsive-min';
    var horizontal_menu_class = 'h-sidebar';

    var sidebar = $('#sidebar').eq(0);
    //differnet mobile menu styles
    ace.vars['mobile_style'] = 1;//default responsive mode with toggle button inside navbar
    if(sidebar.hasClass('responsive') && !$('#menu-toggler').hasClass('navbar-toggle')) ace.vars['mobile_style'] = 2;//toggle button behind sidebar
    else if(sidebar.hasClass(responsive_min_class)) ace.vars['mobile_style'] = 3;//minimized menu
    else if(sidebar.hasClass('navbar-collapse')) ace.vars['mobile_style'] = 4;//collapsible (bootstrap style)

    //update some basic variables
    $(window).on('resize.ace.vars' , function(){
        ace.vars['window'] = {width: parseInt($(this).width()), height: parseInt($(this).height())}
        ace.vars['mobile_view'] = ace.vars['mobile_style'] < 4 && ace.helper.mobile_view();
        ace.vars['collapsible'] = !ace.vars['mobile_view'] && ace.helper.collapsible();
        ace.vars['nav_collapse'] = (ace.vars['collapsible'] || ace.vars['mobile_view']) && $('#navbar').hasClass('navbar-collapse');

        var sidebar = $(document.getElementById('sidebar'));
        ace.vars['minimized'] =
            (!ace.vars['collapsible'] && sidebar.hasClass(minimized_menu_class))
                ||
                (ace.vars['mobile_style'] == 3 && ace.vars['mobile_view'] && sidebar.hasClass(responsive_min_class))

        ace.vars['horizontal'] = !(ace.vars['mobile_view'] || ace.vars['collapsible']) && sidebar.hasClass(horizontal_menu_class)
    }).triggerHandler('resize.ace.vars');
}

//
ace.general_things = function($) {
    //add scrollbars for user dropdowns
    var has_scroll = !!$.fn.ace_scroll;
    if(has_scroll) $('.dropdown-content').ace_scroll({reset: false, mouseWheelLock: true})
    /**
     //add scrollbars to body
     if(has_scroll) $('body').ace_scroll({size: ace.helper.winHeight()})
     $('body').css('position', 'static')
     */

        //reset scrolls bars on window resize
    $(window).on('resize.reset_scroll', function() {
        /**
         //reset body scrollbars
         if(has_scroll) $('body').ace_scroll('update', {size : ace.helper.winHeight()})
         */
        if(has_scroll) $('.ace-scroll').ace_scroll('reset');
    });
    $(document).on('settings.ace.reset_scroll', function(e, name) {
        if(name == 'sidebar_collapsed' && has_scroll) $('.ace-scroll').ace_scroll('reset');
    });


    //change a dropdown to "dropup" depending on its position
    $(document).on('click.dropdown.pos', '.dropdown-toggle[data-position="auto"]', function() {
        var offset = $(this).offset();
        var parent = $(this.parentNode);

        if ( parseInt(offset.top + $(this).height()) + 50
            >
            (ace.helper.scrollTop() + ace.helper.winHeight() - parent.find('.dropdown-menu').eq(0).height())
            ) parent.addClass('dropup');
        else parent.removeClass('dropup');
    });


    //prevent dropdowns from hiding when a tab is selected
    $(document).on('click', '.dropdown-navbar .nav-tabs', function(e){
        e.stopPropagation();
        var $this , href
        var that = e.target
        if( ($this = $(e.target).closest('[data-toggle=tab]')) && $this.length > 0) {
            $this.tab('show');
            e.preventDefault();
        }
    });

    //prevent dropdowns from hiding when a from is clicked
    /**$(document).on('click', '.dropdown-navbar form', function(e){
		e.stopPropagation();
	});*/


        //disable navbar icon animation upon click
    $('.ace-nav [class*="icon-animated-"]').closest('a').one('click', function(){
        var icon = $(this).find('[class*="icon-animated-"]').eq(0);
        var $match = icon.attr('class').match(/icon\-animated\-([\d\w]+)/);
        icon.removeClass($match[0]);
    });


    //tooltip in sidebar items
    $('.sidebar .nav-list .badge[title],.sidebar .nav-list .badge[title]').each(function() {
        var tooltip_class = $(this).attr('class').match(/tooltip\-(?:\w+)/);
        tooltip_class = tooltip_class ? tooltip_class[0] : 'tooltip-error';
        $(this).tooltip({
            'placement': function (context, source) {
                var offset = $(source).offset();

                if( parseInt(offset.left) < parseInt(document.body.scrollWidth / 2) ) return 'right';
                return 'left';
            },
            container: 'body',
            template: '<div class="tooltip '+tooltip_class+'"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
        });
    });

    //or something like this if items are dynamically inserted
    /**$('.sidebar').tooltip({
		'placement': function (context, source) {
			var offset = $(source).offset();

			if( parseInt(offset.left) < parseInt(document.body.scrollWidth / 2) ) return 'right';
			return 'left';
		},
		selector: '.nav-list .badge[title],.nav-list .label[title]',
		container: 'body',
		template: '<div class="tooltip tooltip-error"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
	});*/




    //the scroll to top button
    var scroll_btn = $('.btn-scroll-up');
    if(scroll_btn.length > 0) {
        var is_visible = false;
        $(window).on('scroll.scroll_btn', function() {
            if(ace.helper.scrollTop() > parseInt(ace.helper.winHeight() / 4)) {
                if(!is_visible) {
                    scroll_btn.addClass('display');
                    is_visible = true;
                }
            } else {
                if(is_visible) {
                    scroll_btn.removeClass('display');
                    is_visible = false;
                }
            }
        }).triggerHandler('scroll.scroll_btn');

        scroll_btn.on(ace.click_event, function(){
            var duration = Math.min(500, Math.max(100, parseInt(ace.helper.scrollTop() / 3)));
            $('html,body').animate({scrollTop: 0}, duration);
            return false;
        });
    }


    //chrome and webkit have a problem here when resizing from 460px to more
    //we should force them redraw the navbar!
    if( ace.vars['webkit'] ) {
        var ace_nav = $('.ace-nav').get(0);
        if( ace_nav ) $(window).on('resize.webkit' , function(){
            ace.helper.redraw(ace_nav);
        });
    }

}




//some functions
ace.helper.collapsible = function() {
    var toggle
    return (document.querySelector('#sidebar.navbar-collapse') != null)
        && ((toggle = document.querySelector('.navbar-toggle[data-target*=".sidebar"]')) != null)
        &&  toggle.scrollHeight > 0
    //sidebar is collapsible and collapse button is visible?
}
ace.helper.mobile_view = function() {
    var toggle
    return ((toggle = document.getElementById('menu-toggler')) != null	&& toggle.scrollHeight > 0)
}

ace.helper.redraw = function(elem) {
    var saved_val = elem.style['display'];
    elem.style.display = 'none';
    elem.offsetHeight;
    elem.style.display = saved_val;
}

ace.helper.scrollTop = function() {
    return document.scrollTop || document.documentElement.scrollTop || document.body.scrollTop
    //return $(window).scrollTop();
}
ace.helper.winHeight = function() {
    return window.innerHeight || document.documentElement.clientHeight;
    //return $(window).innerHeight();
}
ace.helper.camelCase = function(str) {
    return str.replace(/-([\da-z])/gi, function(match, chr) {
        return chr ? chr.toUpperCase() : '';
    });
}
ace.helper.removeStyle =
    'removeProperty' in document.body.style
        ?
        function(elem, prop) { elem.style.removeProperty(prop) }
        :
        function(elem, prop) { elem.style[ace.helper.camelCase(prop)] = '' }


ace.helper.hasClass =
    'classList' in document.documentElement
        ?
        function(elem, className) { return elem.classList.contains(className); }
        :
        function(elem, className) { return elem.className.indexOf(className) > -1; };

/**
 <b>Custom drag event for touch devices</b> used in scrollbars.
 For better touch event handling and extra options a more advanced solution such as <u>Hammer.js</u> is recommended.
 */

//based on but not dependent on jQuery mobile
/*
 * jQuery Mobile v1.3.2
 * http://jquerymobile.com
 *
 * Copyright 2010, 2013 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 */
ace.add_touch_drag = function($) {
    if(!ace.vars['touch']) return;

    var touchStartEvent = "touchstart MSPointerDown pointerdown",// : "mousedown",
        touchStopEvent  =  "touchend touchcancel MSPointerUp MSPointerCancel pointerup pointercancel",// : "mouseup",
        touchMoveEvent  =  "touchmove MSPointerMove MSPointerHover pointermove";// : "mousemove";


    $.event.special.ace_drag = {
        setup: function() {
            var min_threshold = 0;

            var $this = $(this);
            $this.on(touchStartEvent, function(event) {
                var data = event.originalEvent.touches ?
                        event.originalEvent.touches[ 0 ] :
                        event,
                    start = {
                        //time: Date.now(),
                        coords: [ data.pageX, data.pageY ],
                        origin: $(event.target)
                    },
                    stop;
                start.origin.trigger({'type' : 'ace_dragStart', 'start':(start || [-1,-1])});

                var direction = false, dx = 0, dy = 0;

                function moveHandler(event) {
                    if (!start) {
                        return;
                    }
                    var data = event.originalEvent.touches ?
                        event.originalEvent.touches[ 0 ] :
                        event;
                    stop = {
                        coords: [ data.pageX, data.pageY ]
                    };

                    // prevent scrolling
                    //if ( Math.abs(start.coords[1] - stop.coords[1]) > 0 || Math.abs(start.coords[0] - stop.coords[01]) > 0 ) {
                    //event.preventDefault();
                    //}


                    if (start && stop) {
                        dx = 0; dy = 0;

                        direction =
                            (
                                Math.abs(dy = start.coords[ 1 ] - stop.coords[ 1 ]) > min_threshold
                                    &&
                                    Math.abs(dx = start.coords[ 0 ] - stop.coords[ 0 ]) <= Math.abs(dy)
                                )
                                ?
                                (dy > 0 ? 'up' : 'down')
                                :
                                (
                                    Math.abs(dx = start.coords[ 0 ] - stop.coords[ 0 ]) > min_threshold
                                        &&
                                        Math.abs( dy ) <= Math.abs(dx)
                                    )
                                    ?
                                    (dx > 0 ? 'left' : 'right')
                                    :
                                    false;


                        if( direction !== false ) {
                            var retval = {}
                            start.origin.trigger({
                                'type': 'ace_drag',
                                //'start': start.coords,
                                //'stop': stop.coords,
                                'direction': direction,
                                'dx': dx,
                                'dy': dy,
                                'retval': retval
                            })

                            // prevent scrolling?
                            //if(retval.cancel === true || event.cancel === undefined) {
                            event.preventDefault();
                            //}
                        }
                    }
                    start.coords[0] = stop.coords[0];
                    start.coords[1] = stop.coords[1];
                }

                $this
                    .on(touchMoveEvent, moveHandler)
                    .one(touchStopEvent, function(event) {
                        $this.off(touchMoveEvent, moveHandler);
                        start.origin.trigger({'type' : 'ace_dragEnd', 'stop':(stop || [-1,-1])});

                        start = stop = undefined;

                    });
            });
        }
    }
};

/**
 <b>Widget boxes</b>
 */
ace.widget_boxes = function($) {
    //bootstrap collapse component icon toggle
    $(document).on('hide.bs.collapse show.bs.collapse', function (ev) {
        var hidden_id = ev.target.getAttribute('id')
        $('[href*="#'+ hidden_id+'"]').find(ace.vars['.icon']).each(function(){
            var $icon = $(this)

            var $match
            var $icon_down = null
            var $icon_up = null
            if( ($icon_down = $icon.attr('data-icon-show')) ) {
                $icon_up = $icon.attr('data-icon-hide')
            }
            else if( $match = $icon.attr('class').match(/fa\-(.*)\-(up|down)/) ) {
                $icon_down = 'fa-'+$match[1]+'-down'
                $icon_up = 'fa-'+$match[1]+'-up'
            }

            if($icon_down) {
                if(ev.type == 'show') $icon.removeClass($icon_down).addClass($icon_up)
                else $icon.removeClass($icon_up).addClass($icon_down)

                return false;//ignore other icons that match, one is enough
            }

        });
    })


    var Widget_Box = function(box, options) {
        this.$box = $(box);
        var that = this;
        //this.options = $.extend({}, $.fn.widget_box.defaults, options);

        this.reload = function() {
            var $box = this.$box;
            var $remove_position = false;
            if($box.css('position') == 'static') {
                $remove_position = true;
                $box.addClass('position-relative');
            }
            $box.append('<div class="widget-box-overlay"><i class="'+ ace.vars['icon'] + 'loading-icon fa fa-spinner fa-spin fa-2x white"></i></div>');

            $box.one('reloaded.ace.widget', function() {
                $box.find('.widget-box-overlay').remove();
                if($remove_position) $box.removeClass('position-relative');
            });
        }

        this.close = function() {
            var $box = this.$box;
            var closeSpeed = 300;
            $box.fadeOut(closeSpeed , function(){
                    $box.trigger('closed.ace.widget');
                    $box.remove();
                }
            )
        }

        this.toggle = function(type, button) {
            var $box = this.$box;
            var $body = $box.find('.widget-body');
            var $icon = null;

            var event_name = typeof type !== 'undefined' ? type : ($box.hasClass('collapsed') ? 'show' : 'hide');
            var event_complete_name = event_name == 'show' ? 'shown' : 'hidden';

            if(typeof button === 'undefined') {
                button = $box.find('> .widget-header a[data-action=collapse]').eq(0);
                if(button.length == 0) button = null;
            }

            if(button) {
                $icon = button.find(ace.vars['.icon']).eq(0);

                var $match
                var $icon_down = null
                var $icon_up = null
                if( ($icon_down = $icon.attr('data-icon-show')) ) {
                    $icon_up = $icon.attr('data-icon-hide')
                }
                else if( $match = $icon.attr('class').match(/fa\-(.*)\-(up|down)/) ) {
                    $icon_down = 'fa-'+$match[1]+'-down'
                    $icon_up = 'fa-'+$match[1]+'-up'
                }
            }

            var $body_inner = $body.find('.widget-body-inner')
            if($body_inner.length == 0) {
                $body = $body.wrapInner('<div class="widget-body-inner"></div>').find(':first-child').eq(0);
            } else $body = $body_inner.eq(0);


            var expandSpeed   = 300;
            var collapseSpeed = 200;

            if( event_name == 'show' ) {
                if($icon) $icon.removeClass($icon_down).addClass($icon_up);
                $box.removeClass('collapsed');
                $body.slideUp(0 , function(){//do it once
                        $body.slideDown(expandSpeed, function(){
                            $box.trigger(event_complete_name+'.ace.widget')})
                    }
                )
            }
            else {
                if($icon) $icon.removeClass($icon_up).addClass($icon_down);
                $body.slideUp(collapseSpeed, function(){
                        $box.addClass('collapsed')
                        $box.trigger(event_complete_name+'.ace.widget')
                    }
                );
            }
        }

        this.hide = function() {
            this.toggle('hide');
        }
        this.show = function() {
            this.toggle('show');
        }


        this.fullscreen = function() {
            var $icon = this.$box.find('> .widget-header a[data-action=fullscreen]').find(ace.vars['.icon']).eq(0);
            var $icon_expand = null
            var $icon_compress = null
            if( ($icon_expand = $icon.attr('data-icon1')) ) {
                $icon_compress = $icon.attr('data-icon2')
            }
            else {
                $icon_expand = 'fa-expand';
                $icon_compress = 'fa-compress';
            }


            if(!this.$box.hasClass('fullscreen')) {
                $icon.removeClass($icon_expand).addClass($icon_compress);
                this.$box.addClass('fullscreen');
            }
            else {
                $icon.addClass($icon_expand).removeClass($icon_compress);
                this.$box.removeClass('fullscreen');
            }

            this.$box.trigger('fullscreened.ace.widget')
        }

    }

    $.fn.widget_box = function (option, value) {
        var method_call;

        var $set = this.each(function () {
            var $this = $(this);
            var data = $this.data('widget_box');
            var options = typeof option === 'object' && option;

            if (!data) $this.data('widget_box', (data = new Widget_Box(this, options)));
            if (typeof option === 'string') method_call = data[option](value);
        });

        return (method_call === undefined) ? $set : method_call;
    };


    $(document).on('click.ace.widget', '.widget-header a[data-action]', function (ev) {
        ev.preventDefault();

        var $this = $(this);
        var $box = $this.closest('.widget-box');
        if( $box.length == 0 || $box.hasClass('ui-sortable-helper') ) return;

        var $widget_box = $box.data('widget_box');
        if (!$widget_box) {
            $box.data('widget_box', ($widget_box = new Widget_Box($box.get(0))));
        }

        var $action = $this.data('action');
        if($action == 'collapse') {
            var event_name = $box.hasClass('collapsed') ? 'show' : 'hide';

            var event
            $box.trigger(event = $.Event(event_name+'.ace.widget'))
            if (event.isDefaultPrevented()) return

            $widget_box.toggle(event_name, $this);
        }
        else if($action == 'close') {
            var event
            $box.trigger(event = $.Event('close.ace.widget'))
            if (event.isDefaultPrevented()) return

            $widget_box.close();
        }
        else if($action == 'reload') {
            $this.blur();
            var event
            $box.trigger(event = $.Event('reload.ace.widget'))
            if (event.isDefaultPrevented()) return

            $widget_box.reload();
        }
        else if($action == 'fullscreen') {
            var event
            $box.trigger(event = $.Event('fullscreen.ace.widget'))
            if (event.isDefaultPrevented()) return

            $widget_box.fullscreen();
        }
        else if($action == 'settings') {
            $box.trigger('setting.ace.widget')
        }

    });

};

/**
 <b>Settings box</b>. It's good for demo only. You don't need this.
 */
ace.settings_box = function($) {
    $('#ace-settings-btn').on(ace.click_event, function(e){
        e.preventDefault();

        $(this).toggleClass('open');
        $('#ace-settings-box').toggleClass('open');
    });

    $('#ace-settings-navbar').on('click', function(){
        ace.settings.navbar_fixed(this.checked);//@ ace-extra.js
        //$(window).triggerHandler('resize.navbar');

        //force redraw?
        //if(ace.vars['webkit']) ace.helper.redraw(document.body);
    }).each(function(){this.checked = ace.settings.is('navbar', 'fixed')})

    $('#ace-settings-sidebar').on('click', function(){
        ace.settings.sidebar_fixed(this.checked);//@ ace-extra.js

        //if(ace.vars['webkit']) ace.helper.redraw(document.body);
    }).each(function(){this.checked = ace.settings.is('sidebar', 'fixed')})

    $('#ace-settings-breadcrumbs').on('click', function(){
        ace.settings.breadcrumbs_fixed(this.checked);//@ ace-extra.js

        //if(ace.vars['webkit']) ace.helper.redraw(document.body);
    }).each(function(){this.checked = ace.settings.is('breadcrumbs', 'fixed')})

    $('#ace-settings-add-container').on('click', function(){
        ace.settings.main_container_fixed(this.checked);//@ ace-extra.js

        //if(ace.vars['webkit']) ace.helper.redraw(document.body);
    }).each(function(){this.checked = ace.settings.is('main-container', 'fixed')})



    $('#ace-settings-compact').removeAttr('checked').on('click', function(){
        if(this.checked) {
            $('#sidebar').addClass('compact');
            var hover = $('#ace-settings-hover');
            if( hover.length > 0 && !hover.get(0).checked ) {
                hover.removeAttr('checked').trigger('click');
            }
        }
        else {
            $('#sidebar').removeClass('compact');
            if('sidebar_scroll' in ace.helper) ace.helper.sidebar_scroll.reset();
        }
    });


    $('#ace-settings-highlight').removeAttr('checked').on('click', function(){
        if(this.checked) $('#sidebar .nav-list > li').addClass('highlight');
        else $('#sidebar .nav-list > li').removeClass('highlight');
    });


    $('#ace-settings-hover').removeAttr('checked').on('click', function(){
        if($('.sidebar').hasClass('h-sidebar')) return;
        if(this.checked) {
            ace.vars['no-scroll'] = true;

            $('#sidebar li').addClass('hover')
                .filter('.open').removeClass('open').find('> .submenu').css('display', 'none');
            //and remove .open items
        }
        else {
            ace.vars['no-scroll'] = false;
            $('#sidebar li.hover').removeClass('hover');

            var compact = $('#ace-settings-compact');
            if( compact.length > 0 && compact.get(0).checked ) {
                compact.trigger('click');
            }

            if('sidebar_hover' in ace.helper) ace.helper.sidebar_hover.reset();
        }

        if('sidebar_scroll' in ace.helper) ace.helper.sidebar_scroll.reset();
    });

};

/**
 <b>RTL</b> (right-to-left direction for Arabic, Hebrew, Persian languages).
 It's good for demo only.
 You should hard code RTL-specific changes inside your HTML/server-side code.
 Dynamically switching to RTL using Javascript is not a good idea.
 Please refer to documentation for more info.
 */


ace.settings_rtl = function($) {
    //Switching to RTL (right to left) Mode
    $('#ace-settings-rtl').removeAttr('checked').on('click', function(){
        ace.switch_direction(jQuery);
    });
}

//>>> you should hard code changes inside HTML for RTL direction
//you shouldn't use this function to switch direction
//this is only for dynamically switching for demonstration
//take a look at this function to see what changes should be made
//also take a look at docs for some tips
ace.switch_direction = function($) {
    var $body = $(document.body);
    $body
        .toggleClass('rtl')
        //toggle pull-right class on dropdown-menu
        .find('.dropdown-menu:not(.datepicker-dropdown,.colorpicker)').toggleClass('dropdown-menu-right')
        .end()
        //swap pull-left & pull-right
        .find('.pull-right:not(.dropdown-menu,blockquote,.profile-skills .pull-right)').removeClass('pull-right').addClass('tmp-rtl-pull-right')
        .end()
        .find('.pull-left:not(.dropdown-submenu,.profile-skills .pull-left)').removeClass('pull-left').addClass('pull-right')
        .end()
        .find('.tmp-rtl-pull-right').removeClass('tmp-rtl-pull-right').addClass('pull-left')
        .end()

        .find('.chosen-select').toggleClass('chosen-rtl').next().toggleClass('chosen-rtl');


    function swap_classes(class1, class2) {
        $body
            .find('.'+class1).removeClass(class1).addClass('tmp-rtl-'+class1)
            .end()
            .find('.'+class2).removeClass(class2).addClass(class1)
            .end()
            .find('.tmp-rtl-'+class1).removeClass('tmp-rtl-'+class1).addClass(class2)
    }

    swap_classes('align-left', 'align-right');
    swap_classes('no-padding-left', 'no-padding-right');
    swap_classes('arrowed', 'arrowed-right');
    swap_classes('arrowed-in', 'arrowed-in-right');
    swap_classes('tabs-left', 'tabs-right');
    swap_classes('messagebar-item-left', 'messagebar-item-right');//for inbox page

    //mirror all icons and attributes that have a "fa-*-right|left" attrobute
    $('.fa').each(function() {
        if(this.className.match(/ui-icon/) || $(this).closest('.fc-button').length > 0) return;
        //skip mirroring icons of plugins that have built in RTL support

        var l = this.attributes.length;
        for(var i = 0 ; i < l ; i++) {
            var val = this.attributes[i].value;
            if(val.match(/fa\-(?:[\w\-]+)\-left/))
                this.attributes[i].value = val.replace(/fa\-([\w\-]+)\-(left)/i , 'fa-$1-right')
            else if(val.match(/fa\-(?:[\w\-]+)\-right/))
                this.attributes[i].value = val.replace(/fa\-([\w\-]+)\-(right)/i , 'fa-$1-left')
        }
    });

    //browsers are incosistent with horizontal scroll and RTL
    //so let's make our scrollbars LTR and wrap the content inside RTL
    var rtl = $body.hasClass('rtl');
    if(rtl)	{
        $('.scroll-hz').addClass('make-ltr')
            .find('.scroll-content')
            .wrapInner('<div class="make-rtl" />');
    }
    else {
        //remove the wrap
        $('.scroll-hz').removeClass('make-ltr')
            .find('.make-rtl').children().unwrap();
    }
    if($.fn.ace_scroll) $('.scroll-hz').ace_scroll('reset') //to reset scrollLeft

    //redraw the traffic pie chart on homepage with a different parameter
    try {
        var placeholder = $('#piechart-placeholder');
        if(placeholder.length > 0) {
            var pos = $(document.body).hasClass('rtl') ? 'nw' : 'ne';//draw on north-west or north-east?
            placeholder.data('draw').call(placeholder.get(0) , placeholder, placeholder.data('chart'), pos);
        }
    }catch(e) {}


    //force redraw(because of webkit)
    /**setTimeout(function() {
		ace.helper.redraw(document.body);
		ace.helper.redraw($('.main-content').get(0));
	}, 10);*/
}
;

/**
 The widget box reload button/event handler. You should use your own handler. An example is available at <i class="text-info">examples/widgets.html</i>.
 <u><i class="glyphicon glyphicon-flash"></i> You don't need this. Used for demo only</u>
 */

ace.widget_reload_handler = function($) {
    //***default action for reload in this demo
    //you should remove this and add your own handler for each specific .widget-box
    //when data is finished loading or processing is done you can call $box.trigger('reloaded.ace.widget')
    $(document).on('reload.ace.widget', '.widget-box', function (ev) {
        var $box = $(this);

        //trigger the reloaded event to remove the spinner icon after 1-2 seconds
        setTimeout(function() {
            $box.trigger('reloaded.ace.widget');
        }, parseInt(Math.random() * 1000 + 1000));
    });

    //you may want to do something like this:
    /**
     $('#my-widget-box').on('reload.ace.widget', function(){
		//load new data here
		//and when finished trigger "reloaded" event
		$(this).trigger('reloaded.ace.widget');
	});
     */
};

/**
 The autocomplete dropdown when typing inside search box.
 <u><i class="glyphicon glyphicon-flash"></i> You don't need this. Used for demo only</u>
 */
ace.enable_searchbox_autocomplete = function($) {
    ace.vars['US_STATES'] = ["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]
    try {
        $('#nav-search-input').typeahead({
            source: ace.vars['US_STATES'],
            updater:function (item) {
                //when an item is selected from dropdown menu, focus back to input element
                $('#nav-search-input').focus();
                return item;
            }
        });
    } catch(e) {}
}
;

/**
 <b>Auto content padding on fixed navbar &amp; breadcrumbs</b>.
 Navbar's content and height is often predictable and when navbar is fixed we can add appropriate padding to content area using CSS.

 But when navbar is fixed and its content size and height is not predictable we may need to add the necessary padding to content area dynamically using Javascript.

 It's not often needed and you can have good results using CSS3 media queries to add necessary paddings based on window size.
 */
ace.auto_padding = function($) {
    var navbar = $('.navbar').eq(0);
    var navbar_container = $('.navbar-container').eq(0);

    var sidebar = $('.sidebar').eq(0);

    var main_container = $('.main-container').get(0);

    var breadcrumbs = $('.breadcrumbs').eq(0);
    var page_content = $('.page-content').get(0);

    var default_padding = 8

    if(navbar.length > 0) {
        $(window).on('resize.padding', function() {
            if( navbar.css('position') == 'fixed' ) {
                var padding1 = !ace.vars['nav_collapse'] ? navbar.outerHeight() : navbar_container.outerHeight();
                padding1 = parseInt(padding1);
                main_container.style.paddingTop = padding1 + 'px';

                if(ace.vars['non_auto_fixed'] && sidebar.length > 0) {
                    if(sidebar.css('position') == 'fixed') {
                        sidebar.get(0).style.top = padding1 + 'px';
                    }
                    else sidebar.get(0).style.top = '';
                }

                if( breadcrumbs.length > 0 ) {
                    if(breadcrumbs.css('position') == 'fixed') {
                        var padding2 = default_padding + breadcrumbs.outerHeight() + parseInt(breadcrumbs.css('margin-top'));
                        padding2 = parseInt(padding2);
                        page_content.style['paddingTop'] = padding2 + 'px';

                        if(ace.vars['non_auto_fixed']) breadcrumbs.get(0).style.top = padding1 + 'px';
                    } else {
                        page_content.style.paddingTop = '';
                        if(ace.vars['non_auto_fixed']) breadcrumbs.get(0).style.top = '';
                    }
                }
            }
            else {
                main_container.style.paddingTop = '';
                page_content.style.paddingTop = '';

                if(ace.vars['non_auto_fixed']) {
                    if(sidebar.length > 0) {
                        sidebar.get(0).style.top = '';
                    }
                    if(breadcrumbs.length > 0) {
                        breadcrumbs.get(0).style.top = '';
                    }
                }
            }
        }).triggerHandler('resize.padding');

        $(document).on('settings.ace', function(ev, event_name, event_val) {
            if(event_name == 'navbar_fixed' || event_name == 'breadcrumbs_fixed') {
                if(ace.vars['webkit']) {
                    //force new 'css position' values to kick in
                    navbar.get(0).offsetHeight;
                    if(breadcrumbs.length > 0) breadcrumbs.get(0).offsetHeight;
                }
                $(window).triggerHandler('resize.padding');
            }
        });

        /**$('#skin-colorpicker').on('change', function() {
		$(window).triggerHandler('resize.padding');
	  });*/
    }

};
