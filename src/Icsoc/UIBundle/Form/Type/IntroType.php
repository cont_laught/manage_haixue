<?php
namespace Icsoc\UIBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IntroType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('ag_content', 'textarea', array('attr'=>array('class'=>'col-xs-10 col-sm-5')))
            ->add('teste', null, array('mapped' => false, 'label'=>'我是谁', 'attr'=>array('class'=>'col-xs-10 col-sm-5','placeholder'=>'teste')))
            ->add('select', 'choice', array('mapped' => false, 'label'=>'单选', 'attr'=>array('class'=>'col-xs-10 col-sm-5'),'choices'=>array('选我','选他')))
            ->add('checkbox', 'choice', array('mapped' => false, 'label'=>'多选', 'attr'=>array('class'=>'col-xs-10 col-sm-5'),'choices'=>array('选我','选他'), 'expanded'=>true, 'multiple'=>true))
            ->add('radio', 'choice', array('mapped' => false, 'label'=>'radio', 'attr'=>array('class'=>'col-xs-10 col-sm-5'),'choices'=>array('选我','选他'), 'expanded'=>true, 'multiple'=>false))
            ->add('file', 'file', array('mapped' => false, 'required'=>false, 'attr'=>array('class'=>'col-xs-10 col-sm-5')))
            ->add('save', 'submit',array('attr'=>array('class'=>'btn btn-info')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Icsoc\UIBundle\Entity\Intro',
        ));

    }
    public function getName()
    {
        return 'intro';
    }
}
