<?php
namespace Icsoc\UIBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WinAgentType extends AbstractType
{
    /**
     * @var
     * 服务
     */
    private $server;
    public function __construct(ContainerInterface $container)
    {
        $this->server = $container;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = $this->server->get('doctrine.orm.entity_manager')
            ->getRepository('IcsocUIBundle:CcRoles')->getRoles(272);
        $queue = $this->server->get('doctrine.orm.entity_manager')
            ->getRepository('IcsocUIBundle:WinQueue')->getQueue(272);
        $builder
            ->add('ag_num', 'text', array(
                    'attr'=>array('class'=>'col-xs-10 col-sm-10'),
                    'label'=>'用户工号',
            ))
            ->add('ag_password', 'repeated', array(
                'options'=>array('attr'=>array('class'=>'col-xs-10 col-sm-10')),
                'type'=>'password',
                'required'=>true,
                'first_options'  => array('label' => '用户密码'),
                'second_options' => array('label' => '确认密码'),
            ))
            ->add('ag_name', 'text', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-10'),
                'label'=>'用户姓名',
            ))
            ->add('user_role', 'choice', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-10'),
                'choices' => $roles,
                'label'=>'用户角色'
            ))
            ->add('user_type', 'choice', array(
                'choices'=>array('0'=>'不限', '1'=>'技能组', '2'=>'坐席'),
                'expanded'=>true,
                'multiple'=>false,
                'label'=>'后台数据权限'
            ))
            ->add('user_queues', 'choice', array(
                'choices'=>$queue,
                'expanded'=>true,
                'multiple'=>true,
                'label'=>'管理技能组'
            ))
            ->add('ag_role', 'choice', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-10'),
                'choices'=>array('0'=>'非坐席 ', '1'=>'普通坐席', '2'=>'班长坐席 '),
                'label'=>'前台坐席类型'
            ))
            ->add('crm_datarole', 'choice', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-10'),
                'choices'=>array('0'=>'不限', '1'=>'技能组', '2'=>'坐席'),
                'label'=>'CRM数据权限'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Icsoc\UIBundle\Entity\WinAgent',
        ));

    }

    public function getName()
    {
        return 'winagent';
    }
}
