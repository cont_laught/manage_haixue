<?php

namespace Icsoc\UIBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CcRoles
 */
class CcRoles
{
    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $roleGrade;

    /**
     * @var string
     */
    private $actionList;

    /**
     * @var integer
     */
    private $groupId;

    /**
     * @var integer
     */
    private $roleId;

    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return CcRoles
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CcRoles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set roleGrade
     *
     * @param boolean $roleGrade
     * @return CcRoles
     */
    public function setRoleGrade($roleGrade)
    {
        $this->roleGrade = $roleGrade;

        return $this;
    }

    /**
     * Get roleGrade
     *
     * @return boolean 
     */
    public function getRoleGrade()
    {
        return $this->roleGrade;
    }

    /**
     * Set actionList
     *
     * @param string $actionList
     * @return CcRoles
     */
    public function setActionList($actionList)
    {
        $this->actionList = $actionList;

        return $this;
    }

    /**
     * Get actionList
     *
     * @return string 
     */
    public function getActionList()
    {
        return $this->actionList;
    }

    /**
     * Set groupId
     *
     * @param integer $groupId
     * @return CcRoles
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return integer 
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Get roleId
     *
     * @return integer 
     */
    public function getRoleId()
    {
        return $this->roleId;
    }
}
