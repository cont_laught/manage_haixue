<?php

namespace Icsoc\UIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Intro
 */
class Intro
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $category;

    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $agContent;

    /**
     * @var integer
     */
    private $addtime;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set title
     *
     * @param string $title
     * @return Intro
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Intro
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Intro
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set addtime
     *
     * @param integer $addtime
     * @return Intro
     */
    public function setAddtime($addtime)
    {
        $this->addtime = $addtime;

        return $this;
    }

    /**
     * Get addtime
     *
     * @return integer
     */
    public function getAddtime()
    {
        return $this->addtime;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set agContent
     *
     * @param string $agContent
     * @return Intro
     */
    public function setAgContent($agContent)
    {
        $this->agContent = $agContent;

        return $this;
    }

    /**
     * Get agContent
     *
     * @return string
     */
    public function getAgContent()
    {
        return $this->agContent;
    }
}
