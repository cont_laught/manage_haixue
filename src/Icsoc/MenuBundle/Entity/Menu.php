<?php

namespace Icsoc\MenuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Menu
 */
class Menu
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $menuText;

    /**
     * @var string
     */
    private $menuUrl;

    /**
     * @var boolean
     */
    private $hasChildren = 0;

    /**
     * @var integer
     */
    private $parentId = 0;

    /** @var array 存放查询结果   */
    private $childData = array();

    /**
     * @var string
     */
    private $helpUrl;

    private $file;

    /** @var  integer */
    private $isShow;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set menuText
     *
     * @param string $menuText
     * @return Menu
     */
    public function setMenuText($menuText)
    {
        $this->menuText = $menuText;

        return $this;
    }

    /**
     * Get menuText
     *
     * @return string 
     */
    public function getMenuText()
    {
        return $this->menuText;
    }

    /**
     * Set menuUrl
     *
     * @param string $menuUrl
     * @return Menu
     */
    public function setMenuUrl($menuUrl)
    {
        $this->menuUrl = $menuUrl;

        return $this;
    }

    /**
     * Get menuUrl
     *
     * @return string 
     */
    public function getMenuUrl()
    {
        return $this->menuUrl;
    }

    /**
     * Set hasChildren
     *
     * @param boolean $hasChildren
     * @return Menu
     */
    public function setHasChildren($hasChildren)
    {
        $this->hasChildren = $hasChildren;

        return $this;
    }

    /**
     * Get hasChildren
     *
     * @return boolean 
     */
    public function getHasChildren()
    {
        return $this->hasChildren;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return Menu
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }
    /**
     * @var string
     */
    private $menuIcon = 0;


    /**
     * Set menuIcon
     *
     * @param string $menuIcon
     * @return Menu
     */
    public function setMenuIcon($menuIcon)
    {
        $this->menuIcon = $menuIcon;

        return $this;
    }

    /**
     * Get menuIcon
     *
     * @return string 
     */
    public function getMenuIcon()
    {
        return $this->menuIcon;
    }
    /**
     * @var boolean
     */
    private $menuLevel;


    /**
     * Set menuLevel
     *
     * @param boolean $menuLevel
     * @return Menu
     */
    public function setMenuLevel($menuLevel)
    {
        $this->menuLevel = $menuLevel;

        return $this;
    }

    /**
     * Get menuLevel
     *
     * @return boolean 
     */
    public function getMenuLevel()
    {
        return $this->menuLevel;
    }

    /**
     *  持久化前调用的操作方法
     */
    public function setCreatedAtValue()
    {
        $menuUrl = $this->getMenuUrl();
        $this->setMenuUrl(!empty($menuUrl) ? $menuUrl : "");
    }

    /**
     * @var integer
     */
    private $sortWeight = 0;


    /**
     * Set sortWeight
     *
     * @param integer $sortWeight
     * @return Menu
     */
    public function setSortWeight($sortWeight)
    {
        $this->sortWeight = $sortWeight;

        return $this;
    }

    /**
     * Get sortWeight
     *
     * @return integer 
     */
    public function getSortWeight()
    {
        return $this->sortWeight;
    }

    /**
     * @param array $array
     * @return $this
     */
    public function setChildData(array $array)
    {
        $this->childData = $array;
        return $this;
    }

    /**
     * @return array
     */
    public function getChildData()
    {
        return $this->childData;
    }

    /**
     * @param $helpUrl
     * @return $this
     */
    public function setHelpUrl($helpUrl)
    {
        $this->helpUrl = $helpUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getHelpUrl()
    {
        return $this->helpUrl;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * 上传文件；
     */
    public function upload()
    {
        if (null == $this->getFile()) {
            return ;
        }
        $this->getFile()->move($this->getUploadRootDir(), $this->getFile()->getClientOriginalName());
        $this->setHelpUrl($this->getFile()->getClientOriginalName());

        $this->file = null;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/documents';
    }

    public function getAbsolutePath()
    {
        return null === $this->helpUrl
            ? null
            : $this->getUploadRootDir().'/'.$this->helpUrl;
    }


    /**
     * @param $isShow
     * @return $this
     */
    public function setIsShow($isShow)
    {
        $this->isShow = $isShow;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsShow()
    {
        return $this->isShow;
    }

}
