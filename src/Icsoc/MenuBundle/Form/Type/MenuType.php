<?php
namespace Icsoc\MenuBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\VarDumper\VarDumper;

class MenuType extends AbstractType
{
    /**
     * @var ContainerInterface $container
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tree = $this
            ->container
            ->get('doctrine.orm.entity_manager')
            ->getRepository('IcsocMenuBundle:Menu')
            ->getTree(0);
        $data = $builder->getData();
        $list = array('0'=>'The top class');
        foreach ($tree as $v) {
            $list[$v['id']] = $v['html'].$v['menu_text'];
        }
        $builder
            ->add('menuText', 'text', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                'label'=>'Menu name'))
            ->add('menuUrl', 'text', array(
                'required'=>false,
                'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                'label_attr'=>array('class'=>'col-sm-3 control-label no-padding-right'),
                'label'=>'Menu url'))
            ->add('menuIcon', 'text', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                'label'=>'Menu icon'))
            ->add('sortWeight', 'text', array(
                  'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                  'help'=>'The menu by ASC order',
                  'label'=>'Weight value'))
            ->add('parentId', 'choice', array(
                'choices' => $list,
                'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                'label'=>'The superior classification',
            ))->add('file','file',array(
                'label'=>'Help documentation',
                'attr'=>array('divClass'=>'col-xs-10 col-sm-4'),
                'required'=>false,
            ))->add('isShow','choice',array(
                'choices'=>array('1'=>'yes','0'=>'no'),
                'attr'=>array('class'=>'col-xs-10 col-sm-5', 'onclick'=>'switchOption("pho_type")'),
                'expanded'=>true,
                'multiple'=>false,
                'data'=> $data->getIsShow() === 0 ? 0 : 1,
                'label'=>'Whether you need to show',
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Icsoc\MenuBundle\Entity\Menu',
        ));
    }

    public function getName()
    {
        return 'menu';
    }
}
