<?php
/**
 * This file is part of easycrm, created by PhpStorm.
 * Author: LouXin
 * Date: 2014/12/25 15:34
 * File: NodesType.php
 */

namespace Icsoc\IvrBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SoundType extends AbstractType
{
    /** 语音列表 */
    const SOUND_TYPE_SOUND_LIST = 1;
    /** 数字 */
//    const SOUND_TYPE_NUMBER = 2;
    /** 数字变量 */
    const SOUND_TYPE_NUMBER_VARIABLE = 3;

    /** @var  ContainerInterface */
    private $container;

    /** @var array 语音类型 */
    private $types = array(
        self::SOUND_TYPE_SOUND_LIST => 'Sound List',
//        self::SOUND_TYPE_NUMBER => 'Numbers',
        self::SOUND_TYPE_NUMBER_VARIABLE => 'Numbers Variable',
    );

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $vcc_id = $this->container->get('security.token_storage')->getToken()->getUser()->getVccId();
        $sounds = $this->container->get('icsoc_data.model.sound')->getSoundsArray($vcc_id);
        $builder
            ->add('sound', 'choice', array(
                'choices' => $this->types,
                'sound_list' => $sounds,
            ))
        ;
    }

    public function getName()
    {
        return 'sound';
    }
}
