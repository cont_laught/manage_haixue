<?php
/**
 * This file is part of easycrm, created by PhpStorm.
 * Author: LouXin
 * Date: 2014/12/25 15:34
 * File: NodesType.php
 */

namespace Icsoc\IvrBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class NodesType extends AbstractType
{
    /** 根节点 */
    const NODE_TYPE_ROOT = 1;
    /** 放音节点 */
    const NODE_TYPE_SOUND = 2;
    /** 按键导航节点 */
    const NODE_TYPE_NAVIGATION = 3;
    const NODE_TYPE_NAVIGATION_KEY = 30;
    /** 转技能组节点 */
    const NODE_TYPE_QUEUE = 5;
    const NODE_TYPE_QUEUE_CHILDREN = 50;
    /** 留言节点 */
    const NODE_TYPE_MESSAGE = 6;
    /** 挂机节点 */
    const NODE_TYPE_HANGUP = 7;
    /** 收键节点 */
    const NODE_TYPE_KEYS = 8;
    const NODE_TYPE_KEYS_CHILDREN = 80;
    /** http接口交互节点 */
    const NODE_TYPE_HTTP = 9;
    const NODE_TYPE_HTTP_CHILDREN = 90;
    /** 来电记忆节点 */
    const NODE_TYPE_CALL_MEMORY = 10;
    const NODE_TYPE_CALL_MEMORY_CHILDREN = 100;
    /** 转电话节点 */
    const NODE_TYPE_PHONE = 11;
    /** 转坐席节点 */
    const NODE_TYPE_AGENT = 12;
    /** 判断节点 */
    const NODE_TYPE_JUDGE = 13;
    const NODE_TYPE_JUDGE_CHILDREN = 130;
    /** 已存在节点类型 */
    const NODE_TYPE_EXISTS = 999;
    /** 转IVR节点 */
    const NODE_TYPE_IVR = 14;

    /** @var array 所有的节点类型 */
    private $nodes = array(
        self::NODE_TYPE_SOUND => 'Play Sound',
        self::NODE_TYPE_NAVIGATION => 'Navigation',
        self::NODE_TYPE_QUEUE => 'Go To Queue',
        self::NODE_TYPE_MESSAGE => 'Leave Message',
        self::NODE_TYPE_HANGUP => 'Hangup',
        self::NODE_TYPE_KEYS => 'Receive Keys',
        self::NODE_TYPE_HTTP => 'Http Api',
        self::NODE_TYPE_CALL_MEMORY => 'Call Memory',
        self::NODE_TYPE_PHONE => 'Go To Phone',
        self::NODE_TYPE_AGENT => 'Go To Agent',
        self::NODE_TYPE_IVR => 'Go To Ivr',
    );

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nodes', 'choice', array(
            'attr' => array(
                'class' => 'chosen-select'
            ),
            'label' => 'Go To',
            'choices' => array('New Nodes' => $this->nodes),
            'placeholder' => 'Choose a Node',
        ));
    }

    public function getName()
    {
        return 'nodes';
    }
}
