<?php

namespace Icsoc\ReportBundle\Model;

use Icsoc\DataBundle\Model\BaseModel;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class QueueModel
 *
 * @package Icsoc\ReportBundle\Model
 */
class QueueModel extends BaseModel
{
    /** @var \Doctrine\DBAL\Connection */
    private $dbal;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->dbal = $this->container->get('doctrine.dbal.default_connection');
    }

    /**
     * 技能组话务报表月数据
     *
     * @param array $param
     *
     * @return array
     */
    public function getQueueMonthData(array $param = array())
    {
        $vccCode = empty($param['vcc_code']) ? 0 : $param['vcc_code'];
        $infos = empty($param['info']) ? '' : $this->purifyHtml($param['info']);
        $reg = "/^[1-9]\d{3}-(0[1-9]|1[0-2])$/"; //2015-05 格式判断
        /** @var array $msg 验证vcc_code是否正确 */
        $msg = $vccId = $this->container->get('icsoc_data.validator')->checkVccCode($vccCode);
        if (!empty($msg) && is_array($msg)) {
            return $msg;
        }

        //分页搜索相关信息；
        $info = array();
        if (!empty($infos)) {
            $info = json_decode($infos, true);
            if (json_last_error()) {
                return array('code' => 403, 'message' => 'info格式非json');
            }
        }

        $where = 'vcc_id = :vcc_id ';
        $condition = array('vcc_id' => $vccId);
        if (isset($info['filter'])) {
            //开始时间
            if (isset($info['filter']['start_date']) && !empty($info['filter']['start_date'])) {
                $startDate = $info['filter']['start_date'];
                $msg = $this->container->get('icsoc_data.helper')->regexRormat($reg, $startDate, '开始日期不正确', 404);
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }
                $where .= "AND start_date >= :start_date ";
                $condition['start_date'] = $startDate;
            }
            //结束时间
            if (isset($info['filter']['end_date']) && !empty($info['filter']['end_date'])) {
                $endDate = $info['filter']['end_date'];
                $msg = $this->container->get('icsoc_data.helper')->regexRormat($reg, $endDate, '结束日期不正确', 405);
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }
                $where .= "AND start_date <= :end_date ";
                $condition['end_date'] = $endDate;
            }

            if (isset($info['filter']['que_id']) && !empty($info['filter']['que_id'])) {
                $where .= " AND queue_id IN (".$info['filter']['que_id'].")";
            }
        }

        /** @var  $authority (获取数据权限) */
        $authority = $this->container->get("icsoc_core.common.class")->getAuthorityCondition('', 'queue_id', 1);
        if ($authority === false) {
            return array(
                'code' => 406,
                'message' => '权限内无任何数据',
            );
        } else {
            $where .= $authority;
        }

        $total = count($this->dbal->fetchAll(
            'SELECT count(*) FROM rep_queue_day WHERE '.$where.'GROUP BY start_date,queue_id ',
            $condition
        ));

        $page = $this->container->get("icsoc_data.helper")->getPageInfoExt($total, 'rep_queue_day', $info);

        $field = array(
            'in_num', 'lost_num', 'lost_secs', 'lost10_num', 'lost20_num', 'lost25_num', 'lost30_num',
            'lost35_num', 'lost40_num', 'conn_num', 'queue_secs', 'ring_num', 'ring_secs', 'conn5_num',
            'conn10_num', 'conn15_num', 'conn20_num', 'conn30_num', 'conn_secs', 'conn_secs', 'wait_secs',
            'deal_secs', 'lost1_num', 'lost1_secs', 'lost3_num', 'lost3_secs', 'lost4_num', 'lost4_secs',
            'lost5_num', 'lost5_secs', 'out_num', 'out_calls', 'out_secs', '`evaluate_-4`', '`evaluate_-3`',
            '`evaluate_-2`', '`evaluate_-1`', 'evaluate_0', 'evaluate_1', 'evaluate_2', 'evaluate_3', 'evaluate_4',
            'evaluate_5', 'evaluate_6', 'evaluate_7', 'evaluate_8', 'evaluate_9', 'first_conn_num',
        );

        $fields = array();
        foreach ($field as $v) {
            $fields[] = "SUM($v) AS $v";
        }

        $fields = implode(',', $fields);

        if (empty($info['export'])) {
            $limit = ($page['limit'] == -1) ? '' : ' LIMIT '.$page['start'].','.$page['limit'];
            $result = $this->dbal->fetchAll(
                "SELECT $fields,queue_id,queue_name,start_date FROM rep_queue_day WHERE ".$where." GROUP BY start_date,queue_id ORDER BY ".$page['sort'].$limit,
                $condition
            );
        } else {
            $result = $this->dbal->fetchAll(
                "SELECT $fields,start_date,queue_id,queue_name,start_date FROM rep_queue_day WHERE ".$where." GROUP BY start_date,queue_id ORDER BY ".$page['sort'],
                $condition
            );
        }

        $data = array();

        foreach ($result as $key => $v) {
            $data[$key] = $v;
            $data[$key]['date'] = $v['start_date'];
        }
        $data = $this->processData($data);

        return array(
            'code' => 200,
            'total' => $page['totalPage'],
            'page' => $page['page'],
            'records' => $total,
            'rows' => $data,
        );
    }

    /**
     * 技能组话务报表天数据
     *
     * @param array $param
     *
     * @return array
     */
    public function getQueueDayData(array $param = array())
    {
        $vccCode = empty($param['vcc_code']) ? 0 : $param['vcc_code'];
        $infos = empty($param['info']) ? '' : $this->purifyHtml($param['info']);
        /** @var array $msg 验证vcc_code是否正确 */
        $msg = $vccId = $this->container->get('icsoc_data.validator')->checkVccCode($vccCode);
        if (!empty($msg) && is_array($msg)) {
            return $msg;
        }

        //分页搜索相关信息；
        $info = array();
        if (!empty($infos)) {
            $info = json_decode($infos, true);
            if (json_last_error()) {
                return array('code' => 403, 'message' => 'info格式非json');
            }
        }

        $where = 'vcc_id = :vcc_id ';
        $condition = array('vcc_id' => $vccId);
        if (isset($info['filter'])) {
            //开始时间
            if (isset($info['filter']['start_date']) && !empty($info['filter']['start_date'])) {
                $startDate = $info['filter']['start_date'];
                $msg = $this->container->get('icsoc_data.helper')->isDate('开始日期不正确', $startDate, 404);
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }
                $where .= "AND nowdate >= :start_date ";
                $condition['start_date'] = $startDate;
            }
            //结束时间
            if (isset($info['filter']['end_date']) && !empty($info['filter']['end_date'])) {
                $endDate = $info['filter']['end_date'];
                $msg = $this->container->get('icsoc_data.helper')->isDate('结束日期不正确', $endDate, 405);
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }
                $where .= "AND nowdate <= :end_date ";
                $condition['end_date'] = $endDate;
            }

            if (isset($info['filter']['que_id']) && !empty($info['filter']['que_id'])) {
                $where .= " AND queue_id IN (".$info['filter']['que_id'].")";
            }
        }

        /** @var  $authority (获取数据权限) */
        $authority = $this->container->get("icsoc_core.common.class")->getAuthorityCondition('', 'queue_id', 1);
        if ($authority === false) {
            return array(
                'code' => 406,
                'message' => '权限内无任何数据',
            );
        } else {
            $where .= $authority;
        }


        $total = $this->dbal->fetchColumn(
            'SELECT count(*) '.
            'FROM rep_queue_day '.
            'WHERE '.$where,
            $condition
        );

        $page = $this->container->get("icsoc_data.helper")->getPageInfoExt($total, 'rep_queue_day', $info);
        if (empty($info['export'])) {
            $limit = ($page['limit'] == -1) ? '' : ' LIMIT '.$page['start'].','.$page['limit'];
            $result = $this->dbal->fetchAll(
                'SELECT * '.
                'FROM rep_queue_day '.
                'WHERE '.$where.
                ' ORDER BY '.$page['sort'].
                $limit,
                $condition
            );
        } else {
            $result = $this->dbal->fetchAll(
                'SELECT * '.
                'FROM rep_queue_day '.
                'WHERE '.$where.
                ' ORDER BY '.$page['sort'],
                $condition
            );
        }
        $data = array();
        foreach ($result as $key => $v) {
            $data[$key] = $v;
            $data[$key]['date'] = $v['nowdate'];
        }
        $data = $this->processData($data);

        return array(
            'code' => 200,
            'total' => $page['totalPage'],
            'page' => $page['page'],
            'records' => $total,
            'rows' => $data,
        );
    }

    /**
     * 技能组话务报表小时数据
     *
     * @param array $param
     *
     * @return array
     */
    public function getQueueHourData(array $param = array())
    {
        $vccCode = empty($param['vcc_code']) ? 0 : $param['vcc_code'];
        $infos = empty($param['info']) ? '' : $this->purifyHtml($param['info']);
        /** @var array $msg 验证vcc_code是否正确 */
        $msg = $vccId = $this->container->get('icsoc_data.validator')->checkVccCode($vccCode);
        if (!empty($msg) && is_array($msg)) {
            return $msg;
        }

        //分页搜索相关信息；
        $info = array();
        if (!empty($infos)) {
            $info = json_decode($infos, true);
            if (json_last_error()) {
                return array('code' => 403, 'message' => 'info格式非json');
            }
        }

        $where = 'vcc_id = :vcc_id ';
        $condition = array('vcc_id' => $vccId);
        if (isset($info['filter'])) {
            //开始时间
            if (isset($info['filter']['start_date']) && !empty($info['filter']['start_date'])) {
                $startDate = $info['filter']['start_date'];
                $msg = $this->container->get('icsoc_data.helper')->isDate('开始日期不正确', $startDate, 404);
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }
                $where .= "AND start_date >= :start_date ";
                $condition['start_date'] = $startDate;
            }
            //结束时间
            if (isset($info['filter']['end_date']) && !empty($info['filter']['end_date'])) {
                $endDate = $info['filter']['end_date'];
                $msg = $this->container->get('icsoc_data.helper')->isDate('结束日期不正确', $endDate, 405);
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }
                $where .= "AND start_date <= :end_date ";
                $condition['end_date'] = $endDate;
            }

            if (isset($info['filter']['que_id']) && !empty($info['filter']['que_id'])) {
                $where .= " AND queue_id IN (".$info['filter']['que_id'].")";
            }
        }
        /** @var  $authority (获取数据权限) */
        $authority = $this->container->get("icsoc_core.common.class")->getAuthorityCondition('', 'queue_id', 1);
        if ($authority === false) {
            return array(
                'code' => 406,
                'message' => '权限内无任何数据',
            );
        } else {
            $where .= $authority;
        }


        $total = $this->dbal->fetchColumn(
            'SELECT count(*) '.
            'FROM rep_queue_hour '.
            'WHERE '.$where,
            $condition
        );
        $page = $this->container->get("icsoc_data.helper")->getPageInfoExt($total, 'rep_queue_hour', $info);
        if (empty($info['export'])) {
            $limit = ($page['limit'] == -1) ? '' : ' LIMIT '.$page['start'].','.$page['limit'];
            $result = $this->dbal->fetchAll(
                'SELECT * '.
                'FROM rep_queue_hour '.
                'WHERE '.$where.
                ' ORDER BY '.$page['sort'].
                $limit,
                $condition
            );
        } else {
            $result = $this->dbal->fetchAll(
                'SELECT * '.
                'FROM rep_queue_hour '.
                'WHERE '.$where.
                ' ORDER BY '.$page['sort'],
                $condition
            );
        }

        $data = array();

        foreach ($result as $key => $v) {
            $data[$key] = $v;
            $data[$key]['date'] = $v['start_date'].' '.sprintf("%02d", $v['time_stamp']).'时';
        }
        $data = $this->processData($data);

        return array(
            'code' => 200,
            'total' => $page['totalPage'],
            'page' => $page['page'],
            'records' => $total,
            'rows' => $data,
        );
    }

    /**
     * 技能组话务报表半小时数据
     *
     * @param array $param
     *
     * @return array
     */
    public function getQueueHalfHourData(array $param = array())
    {
        $vccCode = empty($param['vcc_code']) ? 0 : $param['vcc_code'];
        $infos = empty($param['info']) ? '' : $this->purifyHtml($param['info']);
        /** @var array $msg 验证vcc_code是否正确 */
        $msg = $vccId = $this->container->get('icsoc_data.validator')->checkVccCode($vccCode);
        if (!empty($msg) && is_array($msg)) {
            return $msg;
        }

        //分页搜索相关信息；
        $info = array();
        if (!empty($infos)) {
            $info = json_decode($infos, true);
            if (json_last_error()) {
                return array('code' => 403, 'message' => 'info格式非json');
            }
        }

        $where = 'vcc_id = :vcc_id ';
        $condition = array('vcc_id' => $vccId);
        if (isset($info['filter'])) {
            //开始时间
            if (isset($info['filter']['start_date']) && !empty($info['filter']['start_date'])) {
                $startDate = $info['filter']['start_date'];
                $msg = $this->container->get('icsoc_data.helper')->isDate('开始日期不正确', $startDate, 404);
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }
                $where .= "AND start_date >= :start_date ";
                $condition['start_date'] = $startDate;
            }
            //结束时间
            if (isset($info['filter']['end_date']) && !empty($info['filter']['end_date'])) {
                $endDate = $info['filter']['end_date'];
                $msg = $this->container->get('icsoc_data.helper')->isDate('结束日期不正确', $endDate, 405);
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }
                $where .= "AND start_date <= :end_date ";
                $condition['end_date'] = $endDate;
            }

            if (isset($info['filter']['que_id']) && !empty($info['filter']['que_id'])) {
                $where .= " AND queue_id IN (".$info['filter']['que_id'].")";
            }
        }

        /** @var  $authority (获取数据权限) */
        $authority = $this->container->get("icsoc_core.common.class")->getAuthorityCondition('', 'queue_id', 1);
        if ($authority === false) {
            return array(
                'code' => 406,
                'message' => '权限内无任何数据',
            );
        } else {
            $where .= $authority;
        }

        $total = $this->dbal->fetchColumn(
            'SELECT count(*) '.
            'FROM rep_queue_halfhour '.
            'WHERE '.$where,
            $condition
        );
        $page = $this->container->get("icsoc_data.helper")->getPageInfoExt($total, 'rep_queue_halfhour', $info);
        if (empty($info['export'])) {
            $limit = ($page['limit'] == -1) ? '' : ' LIMIT '.$page['start'].','.$page['limit'];
            $result = $this->dbal->fetchAll(
                'SELECT * '.
                'FROM rep_queue_halfhour '.
                'WHERE '.$where.
                ' ORDER BY '.$page['sort'].
                $limit,
                $condition
            );
        } else {
            $result = $this->dbal->fetchAll(
                'SELECT * '.
                'FROM rep_queue_halfhour '.
                'WHERE '.$where.
                ' ORDER BY '.$page['sort'],
                $condition
            );
        }

        $data = array();

        foreach ($result as $key => $v) {
            $data[$key] = $v;
            $hour = sprintf("%02d", floor($v['time_stamp'] / 2)).":";
            $minute = $v['time_stamp'] % 2 == 1 ? "30" : "00";
            $data[$key]['date'] = $v['start_date']." ".$hour.$minute;
        }
        $data = $this->processData($data);

        return array(
            'code' => 200,
            'total' => $page['totalPage'],
            'page' => $page['page'],
            'records' => $total,
            'rows' => $data,
        );
    }

    /**
     * 公共处理数据
     *
     * @param $data
     *
     * @return array
     */
    private function processData($data)
    {
        $param = array(
            'data' => $data,
            'report_name' => 'queue',
        );
        $gridData = array();
        $rst = $this->container->get('icsoc_report.model.report')->getDataReport($param);
        if (isset($rst['code']) && $rst['code'] == 200) {
            $gridData = $rst['data'];
        }

        return $gridData;
    }
}
