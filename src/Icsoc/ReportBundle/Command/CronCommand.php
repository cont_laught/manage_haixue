<?php

namespace Icsoc\ReportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronCommand extends ContainerAwareCommand
{
    const SHELL_COMMAND = 'ps aux|grep "report:cron"';

    protected function configure()
    {
        $this
            ->setName('report:cron')
            ->setDescription('报表整理定时执行服务')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //检查报表服务进程是否在运行
        @exec(self::SHELL_COMMAND, $result);
        $count = isset($result[0]) ? count($result) : 0;
        if ($count > 4) {
            //进程正在执行
            $output->writeln(sprintf('[%s] 报表服务进程正在执行，无法执行该命令，结果为【%s】.', date('Y-m-d H:i:s'), var_export($result, true)));

            return 1;
        }

        $this->dealTmpAmount();
        $this->dealTmpTime();

        return 0;
    }

    protected function dealTmpAmount()
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->getContainer()->get('doctrine.dbal.default_connection');
        $logger = $this->getContainer()->get('report_amount_logger');
        $debugLogger = $this->getContainer()->get('report_debug_logger');

        $agents = $this->getAllAgent();
        $queues = $this->getAllQueue();
        $groups = $this->getAllGroup();

        //查询量临时表数据
        $tmpAmount = $conn->fetchAll(
            "SELECT * " .
            "FROM rep_tmp_amount"
        );

        //循环处理每一条数据
        foreach ($tmpAmount as $row) {
            $agInfo = empty($agents[$row['ag_id']]) ? array() : $agents[$row['ag_id']];
            $agNum = empty($agInfo['ag_num']) ? '' : $agInfo['ag_num'];
            $agName = empty($agInfo['ag_name']) ? '' : $agInfo['ag_name'];
            $queName = empty($queues[$row['que_id']]) ? '' : $queues[$row['que_id']];
            $groupName = empty($groups[$row['group_id']]) ? '' : $groups[$row['group_id']];

            $startDate = date("Y-m-d", $row['call_time']);
            $startMonth = date("Y-m", $row['call_time']);
            $startYear = date("Y", $row['call_time']);
            $startHour = date("H", $row['call_time']);
            $startMinute = date("i", $row['call_time']);
            $halfHour = floor(($startHour*60+$startMinute)/30);
            $hour = $startHour;
            $day = date("d", $row['call_time']);
            $month = date("m", $row['call_time']);

            $type = empty($row['type']) ? 0 : $row['type'];
            switch ($type) {
                case 1:/** 整体话务 ivr呼入  */
                    $updateValue = array('ivr_num'=>1);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,'ivr_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'ivr_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                        'ivr_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                        'nowmonth'=>$startMonth,'ivr_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                    break;
                case 2:/** 整体话务 人工呼入  */
                    $updateValue = array('in_num'=>1);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,'in_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'in_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                        'in_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                        'nowmonth'=>$startMonth,'in_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                    break;
                case 3:/** 整体话务 放弃  */
                    $updateValue = array('lost_num'=>1);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,'lost_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'lost_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                        'lost_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                        'nowmonth'=>$startMonth,'lost_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                    break;
                case 4:/** 整体话务 接通  */
                    $updateValue = array('conn_num'=>1);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,'conn_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'conn_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                        'nowdate'=>$startDate,'conn_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                        'nowmonth'=>$startMonth,'conn_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                    break;
                case 5:/** 技能组 呼入  */
                    $updateValue = array('in_num'=>1);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$halfHour,'start_date'=>$startDate,'in_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$hour,'start_date'=>$startDate,'in_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'in_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'in_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                    break;
                case 6:/** 技能组 放弃  */
                    $updateValue = array('lost_num'=>1);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$halfHour,'start_date'=>$startDate,'lost_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$hour,'start_date'=>$startDate,'lost_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'lost_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'lost_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                    break;
                case 8:/** 坐席、业务组 转接量  */
                    $updateValue = array('shift_num'=>1);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$halfHour,'start_date'=>$startDate,'shift_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$hour,'start_date'=>$startDate,'shift_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'shift_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'shift_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$halfHour,'start_date'=>$startDate,'shift_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$hour,'start_date'=>$startDate,'shift_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'shift_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'shift_num'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    break;
                case 9:/** 坐席、业务组 外呼量  */
                    $updateValue = array('out_calls'=>1);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$halfHour,'start_date'=>$startDate,'out_calls'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$hour,'start_date'=>$startDate,'out_calls'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'out_calls'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'out_calls'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$halfHour,'start_date'=>$startDate,'out_calls'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$hour,'start_date'=>$startDate,'out_calls'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'out_calls'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'out_calls'=>1
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    break;
            }

            /** 将记录写到日志文件中并删除此记录 */
            $data = array(
                'id' => $row['id'],
                'vcc_id' => $row['vcc_id'],
                'que_id' => $row['que_id'],
                'ag_id' => $row['ag_id'],
                'group_id' => $row['group_id'],
                'type' => $row['type'],
                'starttime' => date("Y-m-d H:i:s", $row['starttime']),
                'call_time' => date("Y-m-d H:i:s", $row['call_time'])
            );
            $str = implode(',', $data);
            $logger->info($str);
            $conn->query("DELETE FROM rep_tmp_amount WHERE id = '" . $row['id'] . "' LIMIT 1");

            //添加数据到Elasticsearch中
//            $id = 'amount'.$row['id'];
//            $data['starttime'] = $row['starttime'];
//            $data['call_time'] = $row['call_time'];
//            $data['month'] = date('Y-m-01', $row['call_time']);
//            $data['day'] = date('Y-m-d', $row['call_time']);
//            $data['log_type'] = 'amount';
//            $response = $this->getContainer()->get('icsoc_custom_report.model.index_data')
//                ->addDataToElasticsearch($id, $data);
//            if ($response === false) {
//                $debugLogger->alert(sprintf('Add data [%s] to elasticsearch fails.', @json_encode($data)));
//            } else {
//                $debugLogger->info(sprintf(
//                    'Add data [%s] to elasticsearch success, the result is [%s]',
//                    @json_encode($data),
//                    @json_encode($response)
//                ));
//            }
        }
    }

    protected function dealTmpTime()
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->getContainer()->get('doctrine.dbal.default_connection');
        $logger = $this->getContainer()->get('report_time_logger');
        $debugLogger = $this->getContainer()->get('report_debug_logger');

        $agents = $this->getAllAgent();
        $queues = $this->getAllQueue();
        $groups = $this->getAllGroup();

        $tmpTime = $conn->fetchAll(
            "SELECT id,vcc_id,call_id,que_id,group_id,ag_id,type,ext_type,ext_option,starttime,endtime,dealtime," .
            "UNIX_TIMESTAMP() AS nowtime,call_time " .
            "FROM rep_tmp_time"
        );

        foreach ($tmpTime as $row) {
            $agInfo = empty($agents[$row['ag_id']]) ? array() : $agents[$row['ag_id']];
            $agNum = empty($agInfo['ag_num']) ? '' : $agInfo['ag_num'];
            $agName = empty($agInfo['ag_name']) ? '' : $agInfo['ag_name'];
            $queName = empty($queues[$row['que_id']]) ? '' : $queues[$row['que_id']];
            $groupName = empty($groups[$row['group_id']]) ? '' : $groups[$row['group_id']];

            /** @var  $ifStart （是否未处理过，用于统计量）*/
            $ifStart = empty($row['dealtime']) ? 1 : 0;
            /** @var  $ifEnd （是否结束，用于判断是否删除记录）*/
            $ifEnd = empty($row['endtime']) ? 0 : 1;
            /** @var  $startTime （开始时间） */
            $startTime = empty($row['dealtime']) ? $row['call_time'] : $row['dealtime'];
            /** @var  $endTime （结束时间） */
            $endTime = empty($row['endtime']) ? $row['nowtime'] : $row['endtime'];
            /** @var  $callTime (呼叫时间用于计算时长) */
            $callTime = empty($row['dealtime']) ? $row['starttime'] : $row['dealtime'];

            $startDate   = date("Y-m-d", (int)$startTime);
            $startMonth  = date("Y-m", (int)$startTime);
            $startYear   = date("Y", (int)$startTime);
            $startHour = date("H", (int)$startTime);
            $startMinute = date("i", (int)$startTime);
            $startSec = date("s", (int)$startTime);
            $month = date("m", (int)$startTime);
            $day = date("d", (int)$startTime);
            $startHalfHour = floor(($startHour*60+$startMinute)/30);
            $halfHour = $startHalfHour;
            $hour = $startHour;
            $startDay = $day;

            $endDay = date("d", (int)$endTime);
            $endHour = date("H", (int)$endTime);
            $endMinute = date("i", (int)$endTime);
            $endHalfHour = floor(($endHour*60+$endMinute)/30);
            $endSec = date("s", (int)$endTime);

            $callHour = date("H", (int)$callTime);
            $callMinute = date("i", (int)$callTime);
            $callHalfHour = floor(($callHour*60+$callMinute)/30);
            //$callSec = date("s", (int)$callTime);

            $logs = '';
            /** 如果结束时间小于开始时间需要减去相应的时长 */
            if ($endTime < $startTime) {
                /** 计算俩个时间相差多少个半小时及没半个小时相应的秒数 */
                $skipHalfHour = ($startDay == $endDay) ? $startHalfHour-$endHalfHour : 48-$endHalfHour+$startHalfHour;
                $skipHalfHours = array();

                $logs .= 'HalfHours【';

                for ($i=0; $i<=$skipHalfHour; $i++) {
                    if ($skipHalfHour == 0) {
                        $sec = -($startTime-$endTime);
                    } elseif ($i == 0) {
                        $sec = -(1800-($endMinute%30*60+$endSec));
                    } elseif ($i == $skipHalfHour) {
                        $sec = -($startMinute%30*60+$startSec);
                    } else {
                        $sec = -1800;
                    }

                    $nowTime = $endTime+$i*1800;
                    $nowDate = date("Y-m-d", (int)$nowTime);
                    $nowHalfHour = floor((date("H", (int)$nowTime)*60+date("i", (int)$nowTime))/30);
                    $skipHalfHours[$nowDate]["$nowHalfHour"] = $sec;

                    $logs .= $nowDate . '-' . $nowHalfHour . '-' . $sec . '    ';
                }

                $logs .= '】';

                /** 计算俩个时间相差多少小时及每小时相应的秒数 */
                $skipHour = ($startDay == $endDay) ? $startHour-$endHour : 24-$endHour+$startHour;
                $skipHours = array();

                $logs .= 'Hours【';

                for ($i=0; $i<=$skipHour; $i++) {
                    if ($skipHour == 0) {
                        $sec = -($startTime-$endTime);
                    } elseif ($i == 0) {
                        $sec = -(3600-($endMinute*60+$endSec));
                    } elseif ($i == $skipHour) {
                        $sec = -($startMinute*60+$startSec);
                    } else {
                        $sec = -3600;
                    }

                    $nowTime = $endTime+$i*3600;
                    $nowDate = date("Y-m-d", (int)$nowTime);
                    $nowHour = date("H", (int)$nowTime);
                    $skipHours[$nowDate][$nowHour] = $sec;

                    $logs .= $nowDate . '-' . $nowHour . '-' . $sec . '    ';
                }
                $logs .= '】';
            } else {
                /** 计算俩个时间相差多少个半小时及每半个小时相应的秒数 */
                $skipHalfHour = ($startDay == $endDay) ? $endHalfHour-$startHalfHour : 48-$startHalfHour+$endHalfHour;
                $skipHalfHours = array();
                $logs .= 'HalfHours【';
                for ($i=0; $i<=$skipHalfHour; $i++) {
                    if ($skipHalfHour == 0) {
                        $sec = $endTime-$callTime;
                    } elseif ($i == 0 && $startHalfHour == $callHalfHour) {
                        $sec = 1800-($callMinute%30*60+date("s", (int)$callTime));
                    } elseif ($i == 0 && $startHalfHour != $callHalfHour) {
                        $sec = 0;
                    } elseif ($i == $skipHalfHour) {
                        $sec = $endMinute%30*60+$endSec;
                    } elseif ($i == 1 && $startHalfHour != $callHalfHour) {
                        $sec = 1800-($callMinute%30*60+date("s", (int)$callTime));
                    } else {
                        $sec = 1800;
                    }

                    $nowTime = $startTime+$i*1800;
                    $nowDate = date("Y-m-d", (int)$nowTime);
                    $nowHalfHour = floor((date("H", (int)$nowTime)*60+date("i", (int)$nowTime))/30);
                    $skipHalfHours[$nowDate]["$nowHalfHour"] = $sec;

                    $logs .= $nowDate . '-' . $nowHalfHour . '-' . $sec . '    ';
                }
                $logs .= '】';
                /** 计算俩个时间相差多少小时及每小时相应的秒数 */
                $skipHour = ($startDay == $endDay) ? $endHour-$startHour : 24-$startHour+$endHour;
                $skipHours = array();
                $logs .= 'Hours【';
                for ($i=0; $i<=$skipHour; $i++) {
                    if ($skipHour == 0) {
                        $sec = $endTime-$callTime;
                    } elseif ($i == 0 && $startHour == $callHour) {
                        $sec = 3600 - ($callMinute*60 + date("s", (int)$callTime));
                    } elseif ($i == 0 && $startHour != $callHour) {
                        $sec = 0;
                    } elseif ($i == $skipHour) {
                        $sec = $endMinute*60 + date("s", (int)$endTime);
                    } elseif ($i == 1 && $startHour != $callHour) {
                        $sec = 3600-($callMinute*60+date("s", (int)$callTime));
                    } else {
                        $sec = 3600;
                    }

                    $nowTime = $startTime+$i*3600;
                    $nowDate = date("Y-m-d", (int)$nowTime);
                    $nowHour = date("H", (int)$nowTime);
                    $skipHours[$nowDate][$nowHour] = $sec;

                    $logs .= $nowDate . '-' . $nowHour . '-' . $sec . '    ';
                }
                $logs .= '】';
            }

            /** 没有结束则更新处理时间，结束记录日志并删除记录 */
            if (empty($ifEnd)) {
                $conn->query("UPDATE rep_tmp_time SET dealtime='".$row['nowtime']."' WHERE id='".$row['id']."' LIMIT 1");
            } else {
                //$extOption = empty($row['ext_option']) ? 0 : $row['ext_option'];
                $data = array(
                    'id' => $row['id'],
                    'vcc_id' => $row['vcc_id'],
                    'call_id' => $row['call_id'],
                    'que_id' => $row['que_id'],
                    'ag_id' => $row['ag_id'],
                    'group_id' => $row['group_id'],
                    'type' => $row['type'],
                    'ext_type' => $row['ext_type'],
                    'starttime' => date("Y-m-d H:i:s", $row['starttime']),
                    'dealtime' => date("Y-m-d H:i:s", $row['dealtime']),
                    'endtime' => date("Y-m-d H:i:s", $row['endtime']),
                    'call_time' => date("Y-m-d H:i:s", $row['call_time']),
                    //'ext_option' => $extOption <= 11 ? '0' : $extOption,
                    'ext_option' => $row['ext_option'],
                );
                $str = implode(',', $data);
                $logger->info($str);
                $conn->query("DELETE FROM rep_tmp_time WHERE id = '" . $row['id'] . "' LIMIT 1");

                //添加数据到Elasticsearch中
//                $id = 'time'.$row['id'];
//                $data['starttime'] = $row['starttime'];
//                $data['endtime'] = $row['endtime'];
//                $data['call_time'] = $row['call_time'];
//                $data['secs'] = $row['endtime'] - $row['starttime'];
//                $data['month'] = date('Y-m-01', $row['call_time']);
//                $data['day'] = date('Y-m-d', $row['call_time']);
//                $data['log_type'] = 'time';
//                unset($data['dealtime']);
//                $response = $this->getContainer()->get('icsoc_custom_report.model.index_data')
//                    ->addDataToElasticsearch($id, $data);
//                if ($response === false) {
//                    $debugLogger->alert(sprintf('Add data [%s] to elasticsearch fails.', @json_encode($data)));
//                } else {
//                    $debugLogger->info(sprintf(
//                        'Add data [%s] to elasticsearch success, the result is [%s]',
//                        @json_encode($data),
//                        @json_encode($response)
//                    ));
//                }
            }

            $str = $logs." ".$row['id']." ".$row['vcc_id']." ".$row['call_id']." ".
                $row['que_id']." ".$row['ag_id']." ".$row['group_id']." "." ".$row['type']." ".$row['ext_type']." ".
                date("Y-m-d H:i:s", $row['starttime'])." ".
                date("Y-m-d H:i:s", $row['dealtime'])." ".
                date("Y-m-d H:i:s", $row['endtime']) ." ".
                date("Y-m-d H:i:s", $row['nowtime']) ." ".
                date("Y-m-d H:i:s", $row['call_time']);
            $debugLogger->info($str);

            $type = empty($row['type']) ? 0 : $row['type'];

            switch($type)
            {
                case 1:/** 排队时长 （整体话务、技能组话务） */
                    /** 半小时 */
                    foreach ($skipHalfHours as $key => $halfHours) {
                        foreach ($halfHours as $k => $v) {
                            $updateValue = array('queue_secs'=>$v);
                            /** 技能组话务半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$k,'start_date'=>$key,'queue_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            /** 整体话务半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'queue_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                        }
                    }

                    /** 小时 */
                    $secMonth = 0;
                    foreach ($skipHours as $key => $hours) {
                        $secDay = 0;
                        foreach ($hours as $k => $v) {
                            $secDay += $v;
                            $updateValue = array('queue_secs'=>$v);
                            /** 技能组话务小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$k,'start_date'=>$key,'queue_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            /** 整体话务小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'queue_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                        }

                        /** 天 */
                        $secMonth += $secDay;
                        $day = date('d', strtotime($key));
                        $startMonth = date('Y-m', strtotime($key));
                        $startDate = date('Y-m-d', strtotime($key));
                        $updateValue = array('queue_secs'=>$secDay);

                        /** 技能组话务天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'queue_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                        /** 整体话务天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                            'nowdate'=>$startDate,'queue_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                    }

                    /** 月 */
                    $updateValue = array('queue_secs'=>$secMonth);
                    /** 技能组话务月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'queue_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                    /** 整体话务月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                        'nowmonth'=>$startMonth,'queue_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                    break;
                case 2:/**  振铃时长、振铃次数 （整体话务、技能组话务、坐席、业务组）*/
                    if ($ifStart == 1) {
                        $updateValue = array('ring_num'=>1);
                        /** 整体话务报表 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                            'nowmonth'=>$startMonth,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                        /** 技能组话务报表 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                            'time_stamp'=>$halfHour,'start_date'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                            'time_stamp'=>$hour,'start_date'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                            'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                        /** 坐席工作表现 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$halfHour,'start_date'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$hour,'start_date'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                        /** 业务组工作表现 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$halfHour,'start_date'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$hour,'start_date'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'ring_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    }

                    /** 半小时 */
                    foreach ($skipHalfHours as $key => $halfHours) {
                        foreach ($halfHours as $k => $v) {
                            $updateValue = array('ring_secs'=>$v);
                            /** 坐席工作表现半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'ring_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                            /** 业务组工作表现半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'ring_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                            /** 坐技能组话务半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$k,'start_date'=>$key,'ring_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            /** 整体话务半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'ring_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                        }
                    }

                    /** 小时 */
                    $secMonth = 0;
                    foreach ($skipHours as $key => $hours) {
                        $secDay = 0;
                        foreach ($hours as $k => $v) {
                            $secDay += $v;
                            $updateValue = array('ring_secs'=>$v);
                            /** 坐席工作表现小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'ring_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                            /** 业务组工作表现小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'ring_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                            /** 技能组话务小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$k,'start_date'=>$key,'ring_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            /** 整体话务小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'ring_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                        }

                        /** 天 */
                        $secMonth += $secDay;
                        $day = date('d', strtotime($key));
                        $startMonth = date('Y-m', strtotime($key));
                        $startDate = date('Y-m-d', strtotime($key));
                        $updateValue = array('ring_secs'=>$secDay);

                        /** 坐席工作表现天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'ring_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                        /** 业务组工作表现天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'ring_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                        /** 技能组话务天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'ring_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                        /** 整体话务天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                            'nowdate'=>$startDate,'ring_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                    }

                    /** 月 */
                    $updateValue = array('ring_secs'=>$secMonth);
                    /** 坐席工作表现月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'ring_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                    /** 业务组工作表现月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'ring_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    /** 技能组话务月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'ring_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                    /** 整体话务月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                        'nowmonth'=>$startMonth,'ring_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                    break;
                case 3:/** 排队放弃时长 几秒放弃量 （整体话务、技能组话务） */
                    $lostSec = $endTime-$callTime;
                    switch(true) {
                        case $lostSec<=10:
                            $updateValue = array(
                                'lost10_num'=>1,'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,
                                'lost40_num'=>1
                            );
                            /** 整体话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,
                                'lost10_num'=>1,'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,
                                'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,
                                'lost10_num'=>1,'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,
                                'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'lost10_num'=>1,'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,
                                'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'lost10_num'=>1,'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,
                                'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'lost10_num'=>1,'lost20_num'=>1,
                                'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'lost10_num'=>1,'lost20_num'=>1,
                                'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'lost10_num'=>1,
                                'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'lost10_num'=>1,
                                'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                        case $lostSec<=20:
                            $updateValue = array(
                                'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            /** 整体话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,
                                'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,
                                'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,
                                'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'lost20_num'=>1,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,
                                'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'lost20_num'=>1,'lost25_num'=>1,
                                'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'lost20_num'=>1,'lost25_num'=>1,
                                'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'lost20_num'=>1,
                                'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'lost20_num'=>1,
                                'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                        case $lostSec<=25:
                            $updateValue = array('lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1);
                            /** 整体话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,
                                'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'lost25_num'=>1,
                                'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'lost25_num'=>1,'lost30_num'=>1,
                                'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'lost25_num'=>1,'lost30_num'=>1,
                                'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'lost25_num'=>1,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                        case $lostSec<=30:
                            $updateValue = array(
                                'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            /** 整体话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,
                                'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,
                                'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'lost30_num'=>1,'lost35_num'=>1,
                                'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'lost30_num'=>1,'lost35_num'=>1,
                                'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'lost30_num'=>1,'lost35_num'=>1,
                                'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                'lost30_num'=>1,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'lost30_num'=>1,
                                'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                        case $lostSec<=35:
                            $updateValue = array('lost35_num'=>1,'lost40_num'=>1);
                            /** 整体话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,
                                'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,
                                'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'lost35_num'=>1,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                        case $lostSec<=40:
                            $updateValue = array('lost40_num'=>1);
                            /** 整体话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,
                                'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'lost40_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                    }

                    /** 技能组放弃具体原因 */
                    $fieldSecs = '';$fieldNum= '';
                    if (in_array($row['ext_option'], array(1, 3, 4, 5))) {
                        $fieldSecs = sprintf('lost%s_secs', $row['ext_option']);
                        $fieldNum = sprintf('lost%s_num', $row['ext_option']);
                    }

                    /** 半小时 */
                    foreach ($skipHalfHours as $key => $halfHours) {
                        foreach ($halfHours as $k => $v) {
                            $updateValue = array('lost_secs'=>$v);
                            if (!empty($fieldSecs)) {
                                $updateValue[$fieldSecs] = $v;
                            }
                            /** 坐技能组话务半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,'time_stamp'=>$k,'start_date'=>$key
                            );
                            $fieldValue = array_merge($fieldValue, $updateValue);
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            /** 整体话务半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'lost_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                        }
                    }

                    /** 小时 */
                    $secMonth = 0;
                    foreach ($skipHours as $key => $hours) {
                        $secDay = 0;
                        foreach ($hours as $k => $v) {
                            $secDay += $v;
                            $updateValue = array('lost_secs'=>$v);
                            if (!empty($fieldSecs)) {
                                $updateValue[$fieldSecs] = $v;
                            }
                            /** 技能组话务小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,'time_stamp'=>$k,'start_date'=>$key
                            );
                            $fieldValue = array_merge($fieldValue, $updateValue);
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            /** 整体话务小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'lost_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                        }

                        /** 天 */
                        $secMonth += $secDay;
                        $day = date('d', strtotime($key));
                        $startMonth = date('Y-m', strtotime($key));
                        $startDate = date('Y-m-d', strtotime($key));
                        $updateValue = array('lost_secs'=>$secDay);
                        if (!empty($fieldSecs)) {
                            $updateValue[$fieldSecs] = $secDay;
                        }
                        /** 技能组话务天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                        );
                        $fieldValue = array_merge($fieldValue, $updateValue);
                        $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                        /** 整体话务天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                            'nowdate'=>$startDate,'lost_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                    }

                    /** 月 */
                    $updateValue = array('lost_secs'=>$secMonth);
                    if (!empty($fieldSecs)) {
                        $updateValue[$fieldSecs] = $secMonth;
                    }
                    /** 技能组话务月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                    );
                    $fieldValue = array_merge($fieldValue, $updateValue);
                    $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                    /** 整体话务月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                        'nowmonth'=>$startMonth,'lost_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);

                    if (!empty($fieldNum)) {
                        $updateValue = array($fieldNum => 1);
                        /** 技能组话务 */
                        $fieldValue = array('vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,'time_stamp'=>$halfHour,'start_date'=>$startDate);
                        $fieldValue = array_merge($fieldValue, $updateValue);
                        $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                            'time_stamp'=>$hour,'start_date'=>$startDate
                        );
                        $fieldValue = array_merge($fieldValue, $updateValue);
                        $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,

                        );
                        $fieldValue = array_merge($fieldValue, $updateValue);
                        $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                            'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,

                        );
                        $fieldValue = array_merge($fieldValue, $updateValue);
                        $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                    }
                    break;
                case 4:/**  */
                    switch($row['ext_type'])
                    {
                        case 1:/** 通话时长-技能组呼入 （整体话务、技能组话务、坐席工作表现、业务组工作表现） 技能组接通量 坐席、业务组接听量  */
                            if ($ifStart == 1) {
                                $updateValue = array('in_num'=>1);
                                /** 坐席工作表现 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'in_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$hour,'start_date'=>$startDate,'in_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'in_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'in_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                                /** 业务组工作表现 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name' =>$groupName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'in_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$hour,'start_date'=>$startDate,'in_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'in_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'in_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                                /** 技能组话务 */
                                $updateValue = array('conn_num'=>1);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'conn_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                    'time_stamp'=>$hour,'start_date'=>$startDate,'conn_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'conn_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'conn_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            }

                            /** 半小时 */
                            foreach ($skipHalfHours as $key => $halfHours) {
                                foreach ($halfHours as $k => $v) {
                                    $updateValue = array('conn_secs'=>$v);
                                    /** 坐席工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                    /** 业务组工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                    /** 坐技能组话务半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                        'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                                    /** 整体话务半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                                }
                            }

                            /** 小时 */
                            $secMonth = 0;
                            foreach ($skipHours as $key => $hours) {
                                $secDay = 0;
                                foreach ($hours as $k => $v) {
                                    $secDay += $v;
                                    $updateValue = array('conn_secs'=>$v);
                                    /** 坐席工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                    /** 业务组工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                    /** 技能组话务小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                        'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                                    /** 整体话务小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                                }

                                /** 天 */
                                $secMonth += $secDay;
                                $day = date('d', strtotime($key));
                                $startMonth = date('Y-m', strtotime($key));
                                $startDate = date('Y-m-d', strtotime($key));
                                $updateValue = array('conn_secs'=>$secDay);

                                /** 坐席工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'conn_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                /** 业务组工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'conn_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                                /** 技能组话务天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'conn_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                                /** 整体话务天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                    'nowdate'=>$startDate,'conn_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            }

                            /** 月 */
                            $updateValue = array('conn_secs'=>$secMonth);
                            /** 坐席工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'conn_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                            /** 业务组工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'conn_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                            /** 技能组话务月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'conn_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            /** 整体话务月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'conn_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                            break;
                        case 2:/** 通话时长-呼出、外呼总时长、坐席，业务组外呼接通量（坐席工作表现、业务组工作表现）  */
                            if ($ifStart == 1) {
                                $updateValue = array('out_num'=>1);
                                /** 坐席工作表现 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'out_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$hour,'start_date'=>$startDate,'out_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'out_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                    'out_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                                /** 业务组工作表现 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'out_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array('vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],
                                    'group_name'=>$groupName,'time_stamp'=>$hour,'start_date'=>$startDate,
                                    'out_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'out_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                    'out_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                            }

                            /** 半小时 */
                            foreach ($skipHalfHours as $key => $halfhours) {
                                foreach ($halfhours as $k => $v) {
                                    $updateValue = array('conn_secs'=>$v,'out_secs'=>$v);
                                    /** 坐席工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v,
                                        'out_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                    /** 业务组工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v,'out_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                }
                            }

                            /** 小时 */
                            $secMonth = 0;
                            foreach ($skipHours as $key => $hours) {
                                $secDay = 0;
                                foreach ($hours as $k => $v) {
                                    $secDay += $v;
                                    $updateValue = array('conn_secs'=>$v,'out_secs'=>$v);
                                    /** 坐席工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v,
                                        'out_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                    /** 业务组工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v,'out_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                }

                                /** 天 */
                                $secMonth += $secDay;
                                $day = date('d', strtotime($key));
                                $startMonth = date('Y-m', strtotime($key));
                                $startDate = date('Y-m-d', strtotime($key));
                                $updateValue = array('conn_secs'=>$secDay,'out_secs'=>$secDay);

                                /** 坐席工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'conn_secs'=>$secDay,'out_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                /** 业务组工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'conn_secs'=>$secDay,'out_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                            }

                            /** 月 */
                            $updateValue = array('conn_secs'=>$secMonth,'out_secs'=>$secMonth);
                            /** 坐席工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'conn_secs'=>$secMonth,'out_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                            /** 业务组工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'conn_secs'=>$secMonth,'out_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                            break;
                        case 3:/** 内线量、总通话时长（坐席工作表现、业务组工作表现）  */
                            if ($ifStart == 1) {
                                $updateValue = array('internal_num'=>1);
                                /** 坐席工作表现 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'internal_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$hour,'start_date'=>$startDate,'internal_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'internal_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                    'internal_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                                /** 业务组工作表现 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'internal_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array('vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],
                                    'group_name'=>$groupName,'time_stamp'=>$hour,'start_date'=>$startDate,
                                    'internal_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'internal_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                    'internal_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                            }

                            /** 半小时 */
                            foreach ($skipHalfHours as $key => $halfhours) {
                                foreach ($halfhours as $k => $v) {
                                    $updateValue = array('conn_secs'=>$v);
                                    /** 坐席工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                    /** 业务组工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                }
                            }

                            /** 小时 */
                            $secMonth = 0;
                            foreach ($skipHours as $key => $hours) {
                                $secDay = 0;
                                foreach ($hours as $k => $v) {
                                    $secDay += $v;
                                    $updateValue = array('conn_secs'=>$v);
                                    /** 坐席工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                    /** 业务组工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                }

                                /** 天 */
                                $secMonth += $secDay;
                                $day = date('d', strtotime($key));
                                $startMonth = date('Y-m', strtotime($key));
                                $startDate = date('Y-m-d', strtotime($key));
                                $updateValue = array('conn_secs'=>$secDay);

                                /** 坐席工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'conn_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                /** 业务组工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'conn_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                            }

                            /** 月 */
                            $updateValue = array('conn_secs'=>$secMonth);
                            /** 坐席工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'conn_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                            /** 业务组工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'conn_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                            break;
                        case 4:/** 通话时长-转接呼叫 （坐席工作表现、业务组工作表现） */
                            /** 半小时 */
                            foreach ($skipHalfHours as $key => $halfhours) {
                                foreach ($halfhours as $k => $v) {
                                    $updateValue = array('conn_secs'=>$v);
                                    /** 坐席工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                    /** 业务组工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                }
                            }

                            /** 小时 */
                            $secMonth = 0;
                            foreach ($skipHours as $key => $hours) {
                                $secDay = 0;
                                foreach ($hours as $k => $v) {
                                    $secDay += $v;
                                    $updateValue = array('conn_secs'=>$v);
                                    /** 坐席工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                    /** 业务组工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'conn_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                }

                                /** 天 */
                                $secMonth += $secDay;
                                $day = date('d', strtotime($key));
                                $startMonth = date('Y-m', strtotime($key));
                                $startDate = date('Y-m-d', strtotime($key));
                                $updateValue = array('conn_secs'=>$secDay);

                                /** 坐席工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'conn_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                /** 业务组工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'conn_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                            }

                            /** 月 */
                            $updateValue = array('conn_secs'=>$secMonth);
                            /** 坐席工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'conn_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                            /** 业务组工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'conn_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                            break;
                    }
                    break;
                case 5:/** 咨询时长、咨询量 （坐席工作表现、业务组工作表现） */
                    if ($ifStart == 1) {
                        $updateValue = array('consult_num'=>1);
                        /** 坐席工作表现 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$halfHour,'start_date'=>$startDate,'consult_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$hour,'start_date'=>$startDate,'consult_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'consult_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'consult_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                        /** 业务组工作表现 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$halfHour,'start_date'=>$startDate,'consult_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array('vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],
                            'group_name'=>$groupName,'time_stamp'=>$hour,'start_date'=>$startDate,'consult_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'consult_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'consult_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    }

                    /** 半小时 */
                    foreach ($skipHalfHours as $key => $halfhours) {
                        foreach ($halfhours as $k => $v) {
                            $updateValue = array('consult_secs'=>$v);
                            /** 坐席工作表现半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'consult_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                            /** 业务组工作表现半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'consult_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                        }
                    }

                    /** 小时 */
                    $secMonth = 0;
                    foreach ($skipHours as $key => $hours) {
                        $secDay = 0;
                        foreach ($hours as $k => $v) {
                            $secDay += $v;
                            $updateValue = array('consult_secs'=>$v);
                            /** 坐席工作表现小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'consult_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                            /** 业务组工作表现小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'consult_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                        }

                        /** 天 */
                        $secMonth += $secDay;
                        $day = date('d', strtotime($key));
                        $startMonth = date('Y-m', strtotime($key));
                        $startDate = date('Y-m-d', strtotime($key));
                        $updateValue = array('consult_secs'=>$secDay);

                        /** 坐席工作表现天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'consult_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                        /** 业务组工作表现天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'consult_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                    }

                    /** 月 */
                    $updateValue = array('consult_secs'=>$secMonth);
                    /** 坐席工作表现月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'consult_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                    /** 业务组工作表现月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'consult_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    break;
                case 6:/** 会议时长、会议量 （坐席工作表现、业务组工作表现） */
                    if ($ifStart == 1) {
                        $updateValue = array('conference_num'=>1);
                        /** 坐席工作表现 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$halfHour,'start_date'=>$startDate,'conference_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$hour,'start_date'=>$startDate,'conference_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'conference_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'conference_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                        /** 业务组工作表现 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$halfHour,'start_date'=>$startDate,'conference_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array('vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],
                            'group_name'=>$groupName,'time_stamp'=>$hour,'start_date'=>$startDate,'conference_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'conference_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'conference_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    }

                    /** 半小时 */
                    foreach ($skipHalfHours as $key => $halfhours) {
                        foreach ($halfhours as $k => $v) {
                            $updateValue = array('conference_secs'=>$v);
                            /** 坐席工作表现半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'conference_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                            /** 业务组工作表现半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'conference_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                        }
                    }

                    /** 小时 */
                    $secMonth = 0;
                    foreach ($skipHours as $key => $hours) {
                        $secDay = 0;
                        foreach ($hours as $k => $v) {
                            $secDay += $v;
                            $updateValue = array('conference_secs'=>$v);
                            /** 坐席工作表现小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'conference_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                            /** 业务组工作表现小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'conference_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                        }

                        /** 天 */
                        $secMonth += $secDay;
                        $day = date('d', strtotime($key));
                        $startMonth = date('Y-m', strtotime($key));
                        $startDate = date('Y-m-d', strtotime($key));
                        $updateValue = array('conference_secs'=>$secDay);

                        /** 坐席工作表现天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'conference_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                        /** 业务组工作表现天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'conference_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                    }

                    /** 月 */
                    $updateValue = array('conference_secs'=>$secMonth);
                    /** 坐席工作表现月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'conference_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                    /** 业务组工作表现月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'conference_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    break;
                case 7:/** 保持量、保持时长 （坐席工作表现、业务组工作表现） */
                    if ($ifStart == 1) {
                        $updateValue = array('hold_num'=>1);
                        /** 坐席工作表现 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$halfHour,'start_date'=>$startDate,'hold_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$hour,'start_date'=>$startDate,'hold_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'hold_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'hold_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                        /** 业务组工作表现 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$halfHour,'start_date'=>$startDate,'hold_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                        $fieldValue = array('vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],
                            'group_name'=>$groupName,'time_stamp'=>$hour,'start_date'=>$startDate,'hold_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'hold_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'hold_num'=>1
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    }

                    /** 半小时 */
                    foreach ($skipHalfHours as $key => $halfhours) {
                        foreach ($halfhours as $k => $v) {
                            $updateValue = array('hold_secs'=>$v);
                            /** 坐席工作表现半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'hold_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                            /** 业务组工作表现半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'hold_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                        }
                    }

                    /** 小时 */
                    $secMonth = 0;
                    foreach ($skipHours as $key => $hours) {
                        $secDay = 0;
                        foreach ($hours as $k => $v) {
                            $secDay += $v;
                            $updateValue = array('hold_secs'=>$v);
                            /** 坐席工作表现小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'hold_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                            /** 业务组工作表现小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'hold_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                        }

                        /** 天 */
                        $secMonth += $secDay;
                        $day = date('d', strtotime($key));
                        $startMonth = date('Y-m', strtotime($key));
                        $startDate = date('Y-m-d', strtotime($key));
                        $updateValue = array('hold_secs'=>$secDay);

                        /** 坐席工作表现天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'hold_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                        /** 业务组工作表现天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'hold_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                    }

                    /** 月 */
                    $updateValue = array('hold_secs'=>$secMonth);
                    /** 坐席工作表现月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'hold_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                    /** 业务组工作表现月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'hold_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    break;
                case 8:/** 登录时长  （坐席工作表现、业务组工作表现）*/
                    /** 半小时 */
                    foreach ($skipHalfHours as $key => $halfhours) {
                        foreach ($halfhours as $k => $v) {
                            $updateValue = array('login_secs'=>$v);
                            /** 整体话务半小时  */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'login_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            /** 坐席工作表现半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'login_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                            /** 业务组工作表现半小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'login_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                        }
                    }

                    /** 小时 */
                    $secMonth = 0;
                    foreach ($skipHours as $key => $hours) {
                        $secDay = 0;
                        foreach ($hours as $k => $v) {
                            $secDay += $v;
                            $updateValue = array('login_secs'=>$v);
                            /** 整体话务小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'login_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            /** 坐席工作表现小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'login_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                            /** 业务组工作表现小时 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'login_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                        }

                        /** 天 */
                        $secMonth += $secDay;
                        $day = date('d', strtotime($key));
                        $startMonth = date('Y-m', strtotime($key));
                        $startDate = date('Y-m-d', strtotime($key));
                        $updateValue = array('login_secs'=>$secDay);

                        /** 整体话务天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'login_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                        /** 坐席工作表现天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'login_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                        /** 业务组工作表现天 */
                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                            'login_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                    }

                    /** 月 */
                    $updateValue = array('login_secs'=>$secMonth);
                    /** 整体话务月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'login_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                    /** 坐席工作表现月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'login_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                    /** 业务组工作表现月 */
                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                        'login_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    break;
                case 9:/** 事后时长 */
                    switch($row['ext_type']) {
                        case 1:
                            /** 事后工作次数 （坐席工作表现、业务组工作表现） */
                            if ($ifStart == 1) {
                                $updateValue = array('wait_num'=>1);
                                /** 坐席工作表现 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$hour,'start_date'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                                /** 业务组工作表现 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array('vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],
                                    'group_name'=>$groupName,'time_stamp'=>$hour,'start_date'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                            }

                            /** 半小时 */
                            foreach ($skipHalfHours as $key => $halfhours) {
                                foreach ($halfhours as $k => $v) {
                                    $updateValue = array('wait_secs'=>$v);
                                    /** 整体话务半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,
                                        'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                                    /** 技能组话务半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                        'time_stamp'=>$halfHour,'start_date'=>$startDate,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                                    /** 坐席工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                    /** 业务组工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                }
                            }

                            /** 小时 */
                            $secMonth = 0;
                            foreach ($skipHours as $key => $hours) {
                                $secDay = 0;
                                foreach ($hours as $k => $v) {
                                    $secDay += $v;
                                    $updateValue = array('wait_secs'=>$v);
                                    /** 整体话务小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$k,'start_date'=>$key,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                                    /** 技能组话务小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                        'time_stamp'=>$k,'start_date'=>$key,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                                    /** 坐席工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                    /** 业务组工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                }

                                /** 天 */
                                $secMonth += $secDay;
                                $day = date('d', strtotime($key));
                                $startMonth = date('Y-m', strtotime($key));
                                $startDate = date('Y-m-d', strtotime($key));
                                $updateValue = array('wait_secs'=>$secDay);

                                /** 整体话务天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                    'nowdate'=>$startDate,'wait_secs'=>$secMonth
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                                /** 技能组话务天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'wait_secs'=>$secMonth
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                                /** 坐席工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'wait_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                /** 业务组工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'wait_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                            }

                            /** 月 */
                            $updateValue = array('wait_secs'=>$secMonth);
                            /** 整体话务月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'wait_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);
                            /** 技能组话务月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'wait_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            /** 坐席工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'wait_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                            /** 业务组工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'wait_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                            break;
                        case 2:
                            /** 事后工作次数 （坐席工作表现、业务组工作表现） */
                            if ($ifStart == 1) {
                                $updateValue = array('wait_num'=>1);
                                /** 坐席工作表现 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$hour,'start_date'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                                /** 业务组工作表现 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$halfHour,'start_date'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                $fieldValue = array('vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],
                                    'group_name'=>$groupName,'time_stamp'=>$hour,'start_date'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'wait_num'=>1
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                            }

                            /** 半小时 */
                            foreach ($skipHalfHours as $key => $halfhours) {
                                foreach ($halfhours as $k => $v) {
                                    $updateValue = array('wait_secs'=>$v);
                                    /** 整体话务半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,
                                        'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                                    /** 技能组话务半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                        'time_stamp'=>$halfHour,'start_date'=>$startDate,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                                    /** 坐席工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);
                                    /** 业务组工作表现半小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                                }
                            }

                            /** 小时 */
                            $secMonth = 0;
                            foreach ($skipHours as $key => $hours) {
                                $secDay = 0;
                                foreach ($hours as $k => $v) {
                                    $secDay += $v;
                                    $updateValue = array('wait_secs'=>$v);
                                    /** 坐席工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,
                                        'ag_name'=>$agName,'time_stamp'=>$k,'start_date'=>$key,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);
                                    /** 业务组工作表现小时 */
                                    $fieldValue = array(
                                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                        'time_stamp'=>$k,'start_date'=>$key,'wait_secs'=>$v
                                    );
                                    $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                                }

                                /** 天 */
                                $secMonth += $secDay;
                                $day = date('d', strtotime($key));
                                $startMonth = date('Y-m', strtotime($key));
                                $startDate = date('Y-m-d', strtotime($key));
                                $updateValue = array('wait_secs'=>$secDay);

                                /** 坐席工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'wait_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);
                                /** 业务组工作表现天 */
                                $fieldValue = array(
                                    'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                    'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,
                                    'wait_secs'=>$secDay
                                );
                                $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                            }

                            /** 月 */
                            $updateValue = array('wait_secs'=>$secMonth);
                            /** 坐席工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'wait_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);
                            /** 业务组工作表现月 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,
                                'wait_secs'=>$secMonth
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                            break;
                    }
                    break;
                case 10:/** 就绪时长  （坐席工作表现、业务组工作表现）*/
                    /** 半小时 */
                    foreach ($skipHalfHours as $key => $halfhours) {
                        foreach ($halfhours as $k => $v) {
                            $updateValue = array('ready_secs'=>$v);

                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                'time_stamp'=>$k,'start_date'=>$key,'ready_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);

                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'ready_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                        }
                    }

                    /** 小时 */
                    $secMonth = 0;
                    foreach ($skipHours as $key => $hours) {
                        $secDay = 0;
                        foreach ($hours as $k => $v) {
                            $secDay += $v;
                            $updateValue = array('ready_secs'=>$v);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                'time_stamp'=>$k,'start_date'=>$key,'ready_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);

                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'ready_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                        }

                        /** 天 */
                        $secMonth += $secDay;
                        $day = date('d', strtotime($key));
                        $startMonth = date('Y-m', strtotime($key));
                        $startDate = date('Y-m-d', strtotime($key));
                        $updateValue = array('ready_secs'=>$secDay);

                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'ready_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);

                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'ready_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                    }

                    /** 月 */
                    $updateValue = array('ready_secs'=>$secMonth);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'ready_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'ready_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    break;
                case 11:/** 置忙时长 （坐席工作表现、业务组工作表现）*/
                    /** 半小时 */
                    foreach ($skipHalfHours as $key => $halfhours) {
                        foreach ($halfhours as $k => $v) {
                            $updateValue = array('busy_secs'=>$v);

                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                'time_stamp'=>$k,'start_date'=>$key,'busy_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_halfhour', $fieldValue, $updateValue);

                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'busy_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_halfhour', $fieldValue, $updateValue);
                        }
                    }

                    /** 小时 */
                    $secMonth = 0;
                    foreach ($skipHours as $key => $hours) {
                        $secDay = 0;
                        foreach ($hours as $k => $v) {
                            $secDay += $v;
                            $updateValue = array('busy_secs'=>$v);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                                'time_stamp'=>$k,'start_date'=>$key,'busy_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_agent_hour', $fieldValue, $updateValue);

                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                                'time_stamp'=>$k,'start_date'=>$key,'busy_secs'=>$v
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_group_hour', $fieldValue, $updateValue);
                        }

                        /** 天 */
                        $secMonth += $secDay;
                        $day = date('d', strtotime($key));
                        $startMonth = date('Y-m', strtotime($key));
                        $startDate = date('Y-m-d', strtotime($key));
                        $updateValue = array('busy_secs'=>$secDay);

                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'busy_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_agent_day', $fieldValue, $updateValue);

                        $fieldValue = array(
                            'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                            'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'busy_secs'=>$secDay
                        );
                        $this->insertOnDuplicateKeyUpdate('rep_group_day', $fieldValue, $updateValue);
                    }

                    /** 月 */
                    $updateValue = array('busy_secs'=>$secMonth);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'ag_id'=>$row['ag_id'],'ag_num'=>$agNum,'ag_name'=>$agName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'busy_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_agent_month', $fieldValue, $updateValue);

                    $fieldValue = array(
                        'vcc_id'=>$row['vcc_id'],'group_id'=>$row['group_id'],'group_name'=>$groupName,
                        'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'busy_secs'=>$secMonth
                    );
                    $this->insertOnDuplicateKeyUpdate('rep_group_month', $fieldValue, $updateValue);
                    break;
                case 12:/** 几秒内接通量 (技能组话务、整体话务) */
                    $secs = $endTime-$callTime;
                    switch (true) {
                        case $secs <= 5:
                            $updateValue = array(
                                'conn5_num'=>1,'conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );

                            /** 整体话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,'start_date'=>$startDate,
                                'conn5_num'=>1,'conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,
                                'conn5_num'=>1,'conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'conn5_num'=>1,'conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,
                                'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'conn5_num'=>1,'conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,
                                'conn30_num'=>1);
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);

                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'conn5_num'=>1,'conn10_num'=>1,
                                'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'conn5_num'=>1,'conn10_num'=>1,
                                'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'conn5_num'=>1,
                                'conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'conn5_num'=>1,
                                'conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                        case $secs <= 10:
                            $updateValue = array('conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1);

                            /** 整体话务 */
                            $fieldValue = array('vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,
                                'start_date'=>$startDate,'conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'conn10_num'=>1,
                                'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'conn10_num'=>1,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);

                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'conn10_num'=>1,'conn15_num'=>1,
                                'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'conn10_num'=>1,'conn15_num'=>1,
                                'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'conn10_num'=>1,
                                'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'conn10_num'=>1,
                                'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                        case $secs <= 15:
                            $updateValue = array('conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1);

                            /** 整体话务 */
                            $fieldValue = array('vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,
                                'start_date'=>$startDate,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'conn15_num'=>1,
                                'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'conn15_num'=>1,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);

                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'conn15_num'=>1,'conn20_num'=>1,
                                'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'conn15_num'=>1,'conn20_num'=>1,
                                'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'conn15_num'=>1,
                                'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'conn15_num'=>1,
                                'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                        case $secs <= 20:
                            $updateValue = array('conn20_num'=>1,'conn30_num'=>1);

                            /** 整体话务 */
                            $fieldValue = array('vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,
                                'start_date'=>$startDate,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'conn20_num'=>1,
                                'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);

                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'conn20_num'=>1,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'conn20_num'=>1,
                                'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'conn20_num'=>1,
                                'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                        case $secs <= 30:
                            $updateValue = array('conn30_num'=>1);

                            /** 整体话务 */
                            $fieldValue = array('vcc_id'=>$row['vcc_id'],'time_stamp'=>$halfHour,
                                'start_date'=>$startDate,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$hour,'start_date'=>$startDate,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$day,'start_date'=>$startMonth,
                                'nowdate'=>$startDate,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'time_stamp'=>$month,'start_date'=>$startYear,
                                'nowmonth'=>$startMonth,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_system_month', $fieldValue, $updateValue);

                            /** 技能组话务 */
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$halfHour,'start_date'=>$startDate,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_halfhour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$hour,'start_date'=>$startDate,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_hour', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$day,'start_date'=>$startMonth,'nowdate'=>$startDate,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_day', $fieldValue, $updateValue);
                            $fieldValue = array(
                                'vcc_id'=>$row['vcc_id'],'queue_id'=>$row['que_id'],'queue_name'=>$queName,
                                'time_stamp'=>$month,'start_date'=>$startYear,'nowmonth'=>$startMonth,'conn30_num'=>1
                            );
                            $this->insertOnDuplicateKeyUpdate('rep_queue_month', $fieldValue, $updateValue);
                            break;
                    }
                    break;
            }
        }
    }

    /**
     * 获取所有坐席信息，键值为坐席id
     *
     * @return array
     */
    public function getAllAgent()
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->getContainer()->get('doctrine.dbal.default_connection');
        $result = $conn->fetchAll(
            "SELECT id,ag_num,ag_name " .
            "FROM win_agent WHERE is_del = 0"
        );

        $agents = array();
        foreach ($result as $agent) {
            $agents[$agent['id']] = $agent;
        }

        return $agents;
    }

    /**
     * 获取所有技能组信息，键值为技能组id
     *
     * @return array
     */
    protected function getAllQueue()
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->getContainer()->get('doctrine.dbal.default_connection');
        $result = $conn->fetchAll(
            "SELECT id,que_name " .
            "FROM win_queue WHERE is_del = 0"
        );

        $queues = array();
        foreach ($result as $queue) {
            $queues[$queue['id']] = $queue['que_name'];
        }

        return $queues;
    }

    /**
     * 获取所有业务组信息，键值为业务组id
     *
     * @return array
     */
    protected function getAllGroup()
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->getContainer()->get('doctrine.dbal.default_connection');
        $result = $conn->fetchAll(
            "SELECT group_id,group_name " .
            "FROM win_group WHERE is_del = 0"
        );

        $groups = array();
        foreach ($result as $group) {
            $groups[$group['group_id']] = $group['group_name'];
        }

        return $groups;
    }

    /**
     * 使用ON DUPLICATE KEY判断是插入还是更新
     *
     * @param $tableName
     * @param array $insertValue
     * @param array $updateValue
     *
     * @return bool
     */
    protected function insertOnDuplicateKeyUpdate($tableName, array $insertValue, array $updateValue = array())
    {
        if (empty($tableName)) {
            return false;
        }

        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->getContainer()->get('doctrine.dbal.default_connection');

        $values = array();
        $sets = array();
        foreach ($insertValue as $key => $value) {
            $values[$key] = "'" . $value . "'";
        }

        foreach ($updateValue as $key => $value) {
            if (array_key_exists($key, $insertValue) == true) {
                if (is_int($value) || is_float($value)) {
                    $sets[] = $key . ' = ' . $key . ' + ' . $value;
                } else {
                    $sets[] = $key . " = '" . $value . "'";
                }
            }
        }

        if (empty($values) || empty($sets)) {
            return false;
        }

        $conn->query(
            'INSERT INTO ' . $tableName . ' (' . implode(', ', array_keys($insertValue)) . ')' .
            ' VALUES (' . implode(', ', $values) . ')' .
            ' ON DUPLICATE KEY UPDATE ' . implode(', ', $sets)
        );

        /*$conn->query(
            'INSERT INTO ' . $tableName . ' (' . implode(', ', array_keys($insertValue)) . ')' .
            ' VALUES (' . array_values($insertValue) . ')' .
            ' ON DUPLICATE KEY UPDATE ' . implode(', ', $sets)
        );*/

        return true;
    }
}
