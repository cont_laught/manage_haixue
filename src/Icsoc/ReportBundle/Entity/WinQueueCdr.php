<?php

namespace Icsoc\QueueBundle\Entity;

/**
 * WinQueueCdr
 */
class WinQueueCdr
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var integer
     */
    private $callId = '0';

    /**
     * @var integer
     */
    private $queId = '0';

    /**
     * @var string
     */
    private $queName;

    /**
     * @var integer
     */
    private $agId = '0';

    /**
     * @var string
     */
    private $agNum = '';

    /**
     * @var string
     */
    private $agName = '';

    /**
     * @var string
     */
    private $agPhone = '';

    /**
     * @var string
     */
    private $callerNum = '';

    /**
     * @var string
     */
    private $serverNum = '';

    /**
     * @var string
     */
    private $callerChan = '';

    /**
     * @var integer
     */
    private $entQueTime = '0';

    /**
     * @var integer
     */
    private $assignTime = '0';

    /**
     * @var integer
     */
    private $linkTime = '0';

    /**
     * @var integer
     */
    private $endTime = '0';

    /**
     * @var integer
     */
    private $queSecs = '0';

    /**
     * @var integer
     */
    private $ringSecs = '0';

    /**
     * @var integer
     */
    private $connSecs = '0';

    /**
     * @var integer
     */
    private $allSecs = '0';

    /**
     * @var boolean
     */
    private $result = '0';

    /**
     * @var boolean
     */
    private $endresult = '0';

    /**
     * @var boolean
     */
    private $bend = '0';

    /**
     * @var integer
     */
    private $startYear = '0';

    /**
     * @var boolean
     */
    private $startMonth = '0';

    /**
     * @var boolean
     */
    private $startDay = '0';

    /**
     * @var boolean
     */
    private $startQua = '0';

    /**
     * @var boolean
     */
    private $startHour = '0';

    /**
     * @var boolean
     */
    private $startMinute = '0';

    /**
     * @var boolean
     */
    private $startWeek = '0';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vccId
     *
     * @param integer $vccId
     *
     * @return WinQueueCdr
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set callId
     *
     * @param integer $callId
     *
     * @return WinQueueCdr
     */
    public function setCallId($callId)
    {
        $this->callId = $callId;

        return $this;
    }

    /**
     * Get callId
     *
     * @return integer
     */
    public function getCallId()
    {
        return $this->callId;
    }

    /**
     * Set queId
     *
     * @param integer $queId
     *
     * @return WinQueueCdr
     */
    public function setQueId($queId)
    {
        $this->queId = $queId;

        return $this;
    }

    /**
     * Get queId
     *
     * @return integer
     */
    public function getQueId()
    {
        return $this->queId;
    }

    /**
     * Set queName
     *
     * @param string $queName
     *
     * @return WinQueueCdr
     */
    public function setQueName($queName)
    {
        $this->queName = $queName;

        return $this;
    }

    /**
     * Get queName
     *
     * @return string
     */
    public function getQueName()
    {
        return $this->queName;
    }

    /**
     * Set agId
     *
     * @param integer $agId
     *
     * @return WinQueueCdr
     */
    public function setAgId($agId)
    {
        $this->agId = $agId;

        return $this;
    }

    /**
     * Get agId
     *
     * @return integer
     */
    public function getAgId()
    {
        return $this->agId;
    }

    /**
     * Set agNum
     *
     * @param string $agNum
     *
     * @return WinQueueCdr
     */
    public function setAgNum($agNum)
    {
        $this->agNum = $agNum;

        return $this;
    }

    /**
     * Get agNum
     *
     * @return string
     */
    public function getAgNum()
    {
        return $this->agNum;
    }

    /**
     * Set agName
     *
     * @param string $agName
     *
     * @return WinQueueCdr
     */
    public function setAgName($agName)
    {
        $this->agName = $agName;

        return $this;
    }

    /**
     * Get agName
     *
     * @return string
     */
    public function getAgName()
    {
        return $this->agName;
    }

    /**
     * Set agPhone
     *
     * @param string $agPhone
     *
     * @return WinQueueCdr
     */
    public function setAgPhone($agPhone)
    {
        $this->agPhone = $agPhone;

        return $this;
    }

    /**
     * Get agPhone
     *
     * @return string
     */
    public function getAgPhone()
    {
        return $this->agPhone;
    }

    /**
     * Set callerNum
     *
     * @param string $callerNum
     *
     * @return WinQueueCdr
     */
    public function setCallerNum($callerNum)
    {
        $this->callerNum = $callerNum;

        return $this;
    }

    /**
     * Get callerNum
     *
     * @return string
     */
    public function getCallerNum()
    {
        return $this->callerNum;
    }

    /**
     * Set serverNum
     *
     * @param string $serverNum
     *
     * @return WinQueueCdr
     */
    public function setServerNum($serverNum)
    {
        $this->serverNum = $serverNum;

        return $this;
    }

    /**
     * Get serverNum
     *
     * @return string
     */
    public function getServerNum()
    {
        return $this->serverNum;
    }

    /**
     * Set callerChan
     *
     * @param string $callerChan
     *
     * @return WinQueueCdr
     */
    public function setCallerChan($callerChan)
    {
        $this->callerChan = $callerChan;

        return $this;
    }

    /**
     * Get callerChan
     *
     * @return string
     */
    public function getCallerChan()
    {
        return $this->callerChan;
    }

    /**
     * Set entQueTime
     *
     * @param integer $entQueTime
     *
     * @return WinQueueCdr
     */
    public function setEntQueTime($entQueTime)
    {
        $this->entQueTime = $entQueTime;

        return $this;
    }

    /**
     * Get entQueTime
     *
     * @return integer
     */
    public function getEntQueTime()
    {
        return $this->entQueTime;
    }

    /**
     * Set assignTime
     *
     * @param integer $assignTime
     *
     * @return WinQueueCdr
     */
    public function setAssignTime($assignTime)
    {
        $this->assignTime = $assignTime;

        return $this;
    }

    /**
     * Get assignTime
     *
     * @return integer
     */
    public function getAssignTime()
    {
        return $this->assignTime;
    }

    /**
     * Set linkTime
     *
     * @param integer $linkTime
     *
     * @return WinQueueCdr
     */
    public function setLinkTime($linkTime)
    {
        $this->linkTime = $linkTime;

        return $this;
    }

    /**
     * Get linkTime
     *
     * @return integer
     */
    public function getLinkTime()
    {
        return $this->linkTime;
    }

    /**
     * Set endTime
     *
     * @param integer $endTime
     *
     * @return WinQueueCdr
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return integer
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set queSecs
     *
     * @param integer $queSecs
     *
     * @return WinQueueCdr
     */
    public function setQueSecs($queSecs)
    {
        $this->queSecs = $queSecs;

        return $this;
    }

    /**
     * Get queSecs
     *
     * @return integer
     */
    public function getQueSecs()
    {
        return $this->queSecs;
    }

    /**
     * Set ringSecs
     *
     * @param integer $ringSecs
     *
     * @return WinQueueCdr
     */
    public function setRingSecs($ringSecs)
    {
        $this->ringSecs = $ringSecs;

        return $this;
    }

    /**
     * Get ringSecs
     *
     * @return integer
     */
    public function getRingSecs()
    {
        return $this->ringSecs;
    }

    /**
     * Set connSecs
     *
     * @param integer $connSecs
     *
     * @return WinQueueCdr
     */
    public function setConnSecs($connSecs)
    {
        $this->connSecs = $connSecs;

        return $this;
    }

    /**
     * Get connSecs
     *
     * @return integer
     */
    public function getConnSecs()
    {
        return $this->connSecs;
    }

    /**
     * Set allSecs
     *
     * @param integer $allSecs
     *
     * @return WinQueueCdr
     */
    public function setAllSecs($allSecs)
    {
        $this->allSecs = $allSecs;

        return $this;
    }

    /**
     * Get allSecs
     *
     * @return integer
     */
    public function getAllSecs()
    {
        return $this->allSecs;
    }

    /**
     * Set result
     *
     * @param boolean $result
     *
     * @return WinQueueCdr
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return boolean
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set endresult
     *
     * @param boolean $endresult
     *
     * @return WinQueueCdr
     */
    public function setEndresult($endresult)
    {
        $this->endresult = $endresult;

        return $this;
    }

    /**
     * Get endresult
     *
     * @return boolean
     */
    public function getEndresult()
    {
        return $this->endresult;
    }

    /**
     * Set bend
     *
     * @param boolean $bend
     *
     * @return WinQueueCdr
     */
    public function setBend($bend)
    {
        $this->bend = $bend;

        return $this;
    }

    /**
     * Get bend
     *
     * @return boolean
     */
    public function getBend()
    {
        return $this->bend;
    }

    /**
     * Set startYear
     *
     * @param integer $startYear
     *
     * @return WinQueueCdr
     */
    public function setStartYear($startYear)
    {
        $this->startYear = $startYear;

        return $this;
    }

    /**
     * Get startYear
     *
     * @return integer
     */
    public function getStartYear()
    {
        return $this->startYear;
    }

    /**
     * Set startMonth
     *
     * @param boolean $startMonth
     *
     * @return WinQueueCdr
     */
    public function setStartMonth($startMonth)
    {
        $this->startMonth = $startMonth;

        return $this;
    }

    /**
     * Get startMonth
     *
     * @return boolean
     */
    public function getStartMonth()
    {
        return $this->startMonth;
    }

    /**
     * Set startDay
     *
     * @param boolean $startDay
     *
     * @return WinQueueCdr
     */
    public function setStartDay($startDay)
    {
        $this->startDay = $startDay;

        return $this;
    }

    /**
     * Get startDay
     *
     * @return boolean
     */
    public function getStartDay()
    {
        return $this->startDay;
    }

    /**
     * Set startQua
     *
     * @param boolean $startQua
     *
     * @return WinQueueCdr
     */
    public function setStartQua($startQua)
    {
        $this->startQua = $startQua;

        return $this;
    }

    /**
     * Get startQua
     *
     * @return boolean
     */
    public function getStartQua()
    {
        return $this->startQua;
    }

    /**
     * Set startHour
     *
     * @param boolean $startHour
     *
     * @return WinQueueCdr
     */
    public function setStartHour($startHour)
    {
        $this->startHour = $startHour;

        return $this;
    }

    /**
     * Get startHour
     *
     * @return boolean
     */
    public function getStartHour()
    {
        return $this->startHour;
    }

    /**
     * Set startMinute
     *
     * @param boolean $startMinute
     *
     * @return WinQueueCdr
     */
    public function setStartMinute($startMinute)
    {
        $this->startMinute = $startMinute;

        return $this;
    }

    /**
     * Get startMinute
     *
     * @return boolean
     */
    public function getStartMinute()
    {
        return $this->startMinute;
    }

    /**
     * Set startWeek
     *
     * @param boolean $startWeek
     *
     * @return WinQueueCdr
     */
    public function setStartWeek($startWeek)
    {
        $this->startWeek = $startWeek;

        return $this;
    }

    /**
     * Get startWeek
     *
     * @return boolean
     */
    public function getStartWeek()
    {
        return $this->startWeek;
    }
}

