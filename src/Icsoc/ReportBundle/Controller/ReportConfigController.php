<?php

namespace Icsoc\ReportBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ReportConfigController
 * @package Icsoc\ReportBundle\Controller
 */
class ReportConfigController extends ReportController
{
    /** 超级管理员 */
    const ADMINISTRATORS = 0;
    /** 企业管理员 */
    const VCC_MANAGER = 1;
    /** 普通坐席 */
    const VCC_AGENT = 2;

    /**
     * @var array 用户级别
     */
    private $userGrade = array(
        self::ADMINISTRATORS => 'Administrators',
        self::VCC_MANAGER => 'Manager',
        self::VCC_AGENT => 'Agent',
    );

    /**
     * 统计的所有指标
     * text为指标的名称
     *
     * @var array
     */
    /*private $reportItems = array(
        'system'=>array(
            'ivrInboundNum'=>array('text'=>'Ivr Num'),
            'inboundTotalNum'=>array('text'=>'queue Inbound Num'),
            'inboundConnNum'=>array('text'=>'inbound Conn Num'),
            'inboundAbandonTotalNum'=>array('text'=>'Lost Num'),
            'inboundAbandonTotalSecs'=>array('text'=>'inbound Abandon Total Secs'),
            'inboundAbandonAvgSecs'=>array('text'=>'inbound Abandon Avg Secs'),
            'queueTotalNum'=>array('text'=>'queue Total Secs'),
            'queueAvgSecs'=>array('text'=>'queue Avg Secs'),
            'ringTotalNum'=>array('text'=>'ring Total Num'),
            'ringTotalSecs'=>array('text'=>'ring Total Secs'),
            'ringAvgSecs'=>array('text'=>'ring Avg Secs'),
        ),
        'queue'=>array(
            'inboundConnNum'=>array('text'=>'Ivr Num'),
        ),
        'group'=>array(

        ),
        'agent'=>array(
            'inboundConnNum'=>array('text'=>'Ivr Num'),
        ),
    );*/

    /**
     * @return JsonResponse
     */
    public function indexAction()
    {
        /** @var Boolean $isEnableGroup 是否启用业务组 */
        $isEnableGroup = $this->getDoctrine()->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($this->getVccId());
        /** 未启用业务组去掉业务组的指标配置 */
        if (empty($isEnableGroup)) {
            unset($this->reportItems['group']);
        }

        /** 判断用户级别，非超级管理员登录去掉非超级管理员的指标配置 */
        $user = $this->container->get("security.token_storage")->getToken()->getUser();
        $userFlag = $user->getFlag();
        if ($userFlag != 'ccadmin') {
            unset($this->userGrade[self::ADMINISTRATORS]);
        }

        return $this->render("IcsocReportBundle:Default:reportConfig.html.twig", array(
            'userGrade' => $this->userGrade,
            'reportItems' => $this->reportItems,
            'calculateItems' => $this->calculateItems,
        ));
    }
}
