<?php

namespace Icsoc\ReportBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ChartsController
 * @package Icsoc\ReportBundle\Controller
 */
class ChartsController extends ReportController
{
    /**
     * 坐席通话量图表
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function agentCallChartsAction()
    {
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);

        return $this->render('IcsocReportBundle:Charts:index.html.twig', array(
            'action' => 'icsoc_report_charts_agentcall_data',
            'statistical_range' => true,
            'type' => array(
                'agent' => true,
                'group' => $isEnableGroup,
                'date' => true,
            ),
            'date' => array(
                'startDate' => date("Y-m-d"),
                'endDate' => date("Y-m-d"),
                'startMonth' => date("Y-m"),
                'endMonth' => date("Y-m"),
            ),
        ));
    }

    /**
     * 坐席通话量图表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function agentCallChartsDataAction(Request $request)
    {
        $param['vcc_id'] = $this->getUser()->getVccId();
        $param['time_stamp'] = $request->get('time_stamp', '');
        $param['start_date'] = $request->get('start_date', '');
        $param['end_date'] = $request->get('end_date', '');
        $param['agents'] = $request->get('node', '');
        $param['ques'] = $request->get('ques', '');
        $param['type'] = $request->get('type', '');

        $result = array();

        switch($param['type']){
            case 'agent':
                $result = $this->get('icsoc_report.model.charts')->getAgentCallChartsByAgentData($param);
                break;
            case 'date':
                $result = $this->get('icsoc_report.model.charts')->getAgentCallChartsByDateData($param);
                break;
        }

        $translator = $this->get('translator');

        $data['title']['text'] = $translator->trans('Chart Agent Call');
        $data['title']['textStyle']['fontSize'] = '13';
        $data['title']['x']    = 'left';
        $data['tooltip']['trigger'] = 'axis';
        $data['legend']['data']     = array(
            $translator->trans('Chart Call Num'),
            $translator->trans('Chart Callin Num'),
            $translator->trans('Chart Callout Num'),
            $translator->trans('Chart Total Call Secs'),
            $translator->trans('Chart Avg Call Secs')
        );
        $data['legend']['y']        = 'top';
        $data['toolbox']['show'] = true;
        $data['toolbox']['feature']['mark']['show'] = false;
        $data['toolbox']['feature']['dataView']['show'] = true;
        $data['toolbox']['feature']['magicType']['show'] = true;
        $data['toolbox']['feature']['magicType']['type'] = array('line', 'bar');
        $data['toolbox']['feature']['restore']['show'] = true;
        $data['toolbox']['feature']['saveAsImage']['show'] = true;
        $data['series'][0] = array('name'=>$translator->trans('Chart Call Num'),'type'=>'bar','barWidth'=>40);
        $data['series'][1] = array('name'=>$translator->trans('Chart Callin Num'),'type'=>'bar','barWidth'=>40);
        $data['series'][2] = array('name'=>$translator->trans('Chart Callout Num'),'type'=>'bar','barWidth'=>40);
        $data['xAxis']['type']      = 'category';
        $data['yAxis'][0]['type']   = 'value';
        $data['yAxis'][0]['name']   = $translator->trans('Chart Num');
        $data['yAxis'][0]['splitArea']['show'] =true;
        $data['yAxis'][1]['type']   = 'value';
        $data['yAxis'][1]['name']   = $translator->trans('Chart Secs');
        $data['yAxis'][1]['splitLine']['show'] =false;

        $data['series'][3] = array('name'=>$translator->trans('Chart Total Call Secs'),'type'=>'line','yAxisIndex'=>1);
        $data['series'][4] = array('name'=>$translator->trans('Chart Avg Call Secs'),'type'=>'line','yAxisIndex'=>1);

        if ($result['code'] == '200') {

            if (!empty($result['total']) && $result['total'] > 10) {
                $end = floor(1000/$result['total']) > 100 ? 100 : floor(1000/$result['total']);
            } else {
                $end = 100;
            }

            $data['dataZoom'] =  array(
                'show'=>true,
                'realtime'=>true,
                'start'=>0,
                'end'=>$end,
                'fillerColor'=>'rgba(50,205,50,0.4)'
            );

            $agents = $this->get('icsoc_data.model.agent')->getAgentNameKeyedByIdArray($this->getUser()->getVccId());

            foreach ($result['data'] as $k => $v) {
                if ($param['type'] == 'agent') {
                    $data['xAxis']['data'][$k]   = isset($agents[$v['ag_id']]) ? $agents[$v['ag_id']] : '';
                } else {
                    $data['xAxis']['data'][$k]   = $v['start_date'];
                }
                $data['series'][0]['data'][] = $v['total_num'];
                $data['series'][1]['data'][] = $v['in_num'];
                $data['series'][2]['data'][] = $v['out_num'];
                $data['series'][3]['data'][] = $v['total_secs'];
                $data['series'][4]['data'][] = round($v['avg_secs'], 1);
            }
        }

        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * 坐席工作量图表
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function agentWorkChartsAction()
    {
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);

        return $this->render('IcsocReportBundle:Charts:index.html.twig', array(
            'action' => 'icsoc_report_charts_agentwork_data',
            'statistical_range' => true,
            'type' => array(
                'agent' => true,
                'group' => $isEnableGroup,
                'date' => true,
            ),
            'date' => array(
                'startDate' => date("Y-m-d"),
                'endDate' => date("Y-m-d"),
                'startMonth' => date("Y-m"),
                'endMonth' => date("Y-m"),
            ),
        ));
    }

    /**
     * 坐席工作量图表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function agentWorkChartsDataAction(Request $request)
    {
        $param['vcc_id'] = $this->getUser()->getVccId();
        $param['time_stamp'] = $request->get('time_stamp', '');
        $param['start_date'] = $request->get('start_date', '');
        $param['end_date'] = $request->get('end_date', '');
        $param['agents'] = $request->get('node', '');
        $param['type'] = $request->get('type', '');

        $result = array();

        switch($param['type']){
            case 'agent':
                $result = $this->get('icsoc_report.model.charts')->getAgentWorkChartsByAgentData($param);
                break;
            case 'date':
                $result = $this->get('icsoc_report.model.charts')->getAgentWorkChartsByDateData($param);
                break;
        }

        $translator = $this->get('translator');

        $data['title']['text'] = $translator->trans('Chart Agent Work');
        $data['title']['textStyle']['fontSize'] = '13';
        $data['title']['x']    = 'left';
        $data['tooltip']['trigger'] = 'axis';
        $data['legend']['data']     = array(
            $translator->trans('Chart Login Secs'),
            $translator->trans('Chart Call Secs'),
            $translator->trans('Chart Busy Secs'),
            $translator->trans('Chart Unbusy Secs'),
            $translator->trans('Chart Wait Secs'),
            $translator->trans('Chart Callin Num'),
            $translator->trans('Chart Callout Num')
        );
        $data['legend']['y']        = 'top';

        $data['xAxis']['type']      = 'category';
        $data['yAxis'][0]['type']   = 'value';
        $data['yAxis'][0]['name']   = $translator->trans('Chart Secs');
        $data['yAxis'][0]['splitArea']['show'] =true;
        $data['yAxis'][1]['type']   = 'value';
        $data['yAxis'][1]['name']   = $translator->trans('Chart Num');
        $data['yAxis'][1]['splitLine']['show'] =false;

        $data['toolbox']['show'] = true;
        $data['toolbox']['feature']['mark']['show'] = true;
        $data['toolbox']['feature']['dataView']['show'] = true;
        $data['toolbox']['feature']['magicType']['show'] = true;
        $data['toolbox']['feature']['magicType']['type'] = array('line', 'bar');
        $data['toolbox']['feature']['restore']['show'] = true;
        $data['toolbox']['feature']['saveAsImage']['show'] = true;

        $data['series'][0] = array('name'=>$translator->trans('Chart Login Secs'),'type'=>'bar','barWidth'=>40);
        $data['series'][1] = array('name'=>$translator->trans('Chart Call Secs'),'type'=>'bar','barWidth'=>40);
        $data['series'][2] = array('name'=>$translator->trans('Chart Busy Secs'),'type'=>'bar','barWidth'=>40);
        $data['series'][3] = array('name'=>$translator->trans('Chart Unbusy Secs'),'type'=>'bar','barWidth'=>40);
        $data['series'][4] = array('name'=>$translator->trans('Chart Wait Secs'),'type'=>'bar','barWidth'=>40);
        $data['series'][5] = array('name'=>$translator->trans('Chart Callin Num'),'type'=>'line','yAxisIndex'=>1);
        $data['series'][6] = array('name'=>$translator->trans('Chart Callout Num'),'type'=>'line','yAxisIndex'=>1);

        if ($result['code'] == '200') {

            if (!empty($result['total']) && $result['total'] > 10) {
                $end = floor(1000/$result['total']) > 100 ? 100 : floor(1000/$result['total']);
            } else {
                $end = 100;
            }

            $data['dataZoom'] =  array(
                'show'=>true,
                'realtime'=>true,
                'start'=>0,
                'end'=>$end,
                'fillerColor'=>'rgba(50,205,50,0.4)'
            );

            $agents = $this->get('icsoc_data.model.agent')->getAgentNameKeyedByIdArray($this->getUser()->getVccId());

            foreach ($result['data'] as $k => $v) {
                if ($param['type'] == 'agent') {
                    $data['xAxis']['data'][$k] = isset($agents[$v['ag_id']]) ? $agents[$v['ag_id']] : '';
                } else {
                    $data['xAxis']['data'][$k] = $v['nowdate'];
                }
                $data['series'][0]['data'][] = $v['total_login'];
                $data['series'][1]['data'][] = $v['total_conn'];
                $data['series'][2]['data'][] = $v['total_busy'];
                $data['series'][3]['data'][] = $v['total_ready'];
                $data['series'][4]['data'][] = $v['total_wait'];
                $data['series'][5]['data'][] = $v['total_in'];
                $data['series'][6]['data'][] = $v['total_out'];
            }
        }

        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * 技能组通话量图表
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function queueCallChartsAction()
    {
        return $this->render('IcsocReportBundle:Charts:index.html.twig', array(
            'action' => 'icsoc_report_charts_queuecall_data',
            'statistical_range' => true,
            'type' => array(
                'queue' => true,
                'date' => true
            ),
            'date' => array(
                'startDate' => date("Y-m-d"),
                'endDate' => date("Y-m-d"),
                'startMonth' => date("Y-m"),
                'endMonth' => date("Y-m")
            )
        ));
    }

    /**
     * 技能组通话量图表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function queueCallChartsDataAction(Request $request)
    {
        $param['vcc_id'] = $this->getUser()->getVccId();
        $param['time_stamp'] = $request->get('time_stamp', '');
        $param['start_date'] = $request->get('start_date', '');
        $param['end_date'] = $request->get('end_date', '');
        $param['type'] = $request->get('type', '');
        $param['node'] = $request->get('node', '');

        $result = array();

        switch($param['type']){
            case 'queue':
                $result = $this->get('icsoc_report.model.charts')->getQueueCallChartsByQueueData($param);
                break;
            case 'date':
                $result = $this->get('icsoc_report.model.charts')->getQueueCallChartsByDateData($param);
                break;
        }

        $translator = $this->get('translator');

        $data['title']['text'] = $translator->trans('Chart Queue Call Num');
        $data['title']['textStyle']['fontSize'] = '13';
        $data['title']['x']    = 'left';
        $data['tooltip']['trigger'] = 'axis';
        $data['legend']['data']     = array(
            $translator->trans('Chart Callin Num'),
            $translator->trans('Chart Lost Num'),
            $translator->trans('Chart Conn Num'),
            $translator->trans('Chart Total Queue Secs'),
            $translator->trans('Chart Total Call Secs'),
            $translator->trans('Chart Total Wait Secs'),
            $translator->trans('Chart Total Deal Secs'),
        );
        $data['legend']['y']        = 'top';
        $data['xAxis']['type']      = 'category';
        $data['yAxis'][0]['type']   = 'value';
        $data['yAxis'][0]['name']   = $translator->trans('Chart Num');
        $data['yAxis'][0]['splitArea']['show'] =true;
        $data['yAxis'][1]['type']   = 'value';
        $data['yAxis'][1]['name']   = $translator->trans('Chart Secs');
        $data['yAxis'][1]['splitLine']['show'] =false;
        $data['toolbox']['show'] = true;
        $data['toolbox']['feature']['mark']['show'] = false;
        $data['toolbox']['feature']['dataView']['show'] = true;
        $data['toolbox']['feature']['magicType']['show'] = true;
        $data['toolbox']['feature']['magicType']['type'] = array('line', 'bar');
        $data['toolbox']['feature']['restore']['show'] = true;
        $data['toolbox']['feature']['saveAsImage']['show'] = true;
        $data['series'][0] = array('name'=>$translator->trans('Chart Callin Num'),'type'=>'bar','barWidth'=>40);
        $data['series'][1] = array('name'=>$translator->trans('Chart Lost Num'),'type'=>'bar','barWidth'=>40);
        $data['series'][2] = array('name'=>$translator->trans('Chart Conn Num'),'type'=>'bar','barWidth'=>40);
        $data['series'][3] = array('name'=>$translator->trans('Chart Total Queue Secs'),'type'=>'line','yAxisIndex'=>1);
        $data['series'][4] = array('name'=>$translator->trans('Chart Total Call Secs'),'type'=>'line','yAxisIndex'=>1);
        $data['series'][5] = array('name'=>$translator->trans('Chart Total Wait Secs'),'type'=>'line','yAxisIndex'=>1);
        $data['series'][6] = array('name'=>$translator->trans('Chart Total Deal Secs'),'type'=>'line','yAxisIndex'=>1);

        if ($result['code'] == '200') {

            if (!empty($result['total']) && $result['total'] > 10) {
                $end = floor(1000/$result['total']) > 100 ? 100 : floor(1000/$result['total']);
            } else {
                $end = 100;
            }

            $data['dataZoom'] =  array(
                'show'=>true,
                'realtime'=>true,
                'start'=>0,
                'end'=>$end,
                'fillerColor'=>'rgba(50,205,50,0.4)'
            );

            foreach ($result['data'] as $k => $v) {
                if ($param['type'] == 'queue') {
                    $data['xAxis']['data'][$k] = $v['queue_name'];
                } else {
                    $data['xAxis']['data'][$k] = $v['nowdate'];
                }
                $data['series'][0]['data'][] = $v['total_innum'];
                $data['series'][1]['data'][] = $v['total_lost'];
                $data['series'][2]['data'][] = $v['total_conn'];
                $data['series'][3]['data'][] = $v['total_quesecs'];
                $data['series'][4]['data'][] = $v['total_connsecs'];
                $data['series'][5]['data'][] = $v['total_waitsecs'];
                $data['series'][6]['data'][] = $v['total_connsecs']+$v['total_waitsecs'];
            }
        }

        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * 呼入地区分析图表
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function inareaChartsAction()
    {
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);

        return $this->render('IcsocReportBundle:Charts:inarea.html.twig', array(
            'action' => 'icsoc_report_charts_inarea_data',
            'statistical_range' => true,
            'type' => array(
                'number' => true,
                'queue' => true
            ),
            'date' => array(
                'startDate' => date("Y-m-d"),
                'endDate' => date("Y-m-d"),
                'startMonth' => date("Y-m"),
                'endMonth' => date("Y-m")
            ),
            'caption' => '呼入地区分析图表 【%start_date% ~ %end_date%】',
            'isEnableGroup' => $isEnableGroup
        ));
    }

    /**
     * 呼入地区分析图表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function inareaChartsDataAction(Request $request)
    {
        $param['vcc_id'] = $this->getUser()->getVccId();
        $param['time_stamp'] = $request->get('time_stamp', '');
        $param['start_date'] = $request->get('start_date', '');
        $param['end_date'] = $request->get('end_date', '');
        $param['type'] = $request->get('chartType', '');
        $param['chosenVal'] = $request->get('chosenVal', '');
        $param['chosenProvinces'] = $request->get('chosenProvinces', '');
        //$param['node'] = $request->get('node', '');
        $param['magicType'] = $request->get('magicType', '');

        //$result = $this->get('icsoc_report.model.charts')->getInareaChartsData($param);
        $result = $this->get('icsoc_report.model.charts')->getInareaChartsDataFromMongo($param);

        $translator = $this->get('translator');

        if ($param['magicType'] == 'map') {
            $data['title']['text'] = $translator->trans('Chart Inarea');
            $data['title']['textStyle']['fontSize'] = '13';
            $data['title']['x']  = 'center';
            $data['tooltip']['trigger'] = 'item';
            $data['series'][0]['name'] = $translator->trans('Total nums');
            $data['series'][0]['type'] = 'map';
            $data['series'][0]['mapType'] = 'china';
            $data['series'][0]['itemStyle']['normal']['label']['show'] = true;
            $data['series'][0]['itemStyle']['emphasis']['label']['show'] = true;

            $data['toolbox']['show'] = true;
            $data['toolbox']['orient'] = 'vertical';
            $data['toolbox']['x'] = 'right';
            $data['toolbox']['y'] = 'center';
            $data['toolbox']['feature']['mark']['show'] = false;
            $data['toolbox']['feature']['dataView']['show'] = true;
            $data['toolbox']['feature']['myTool']['show'] = true;
            $data['toolbox']['feature']['myTool']['title'] = $translator->trans('Chart Bar Change');
            $data['toolbox']['feature']['myTool']['magicType'] = array('line', 'bar');
            $data['toolbox']['feature']['restore']['show'] = true;
            $data['toolbox']['feature']['saveAsImage']['show'] = true;
            $data['roamController']['show'] = true;
            $data['roamController']['x'] = 'right';
            $data['roamController']['mapTypeControl']['china'] = true;

            $max = 0;
            if ($result['code'] == '200') {
                foreach ($result['data'] as $v) {
                    if ($v['province_name'] == $translator->trans('Chart Other')) {
                        continue;
                    }
                    $province = $v['province_name'];
                    if (strpos($v['province_name'], '省') !== false) {
                        $province = substr($v['province_name'],0,strpos($v['province_name'], '省'));
                    }
                    if (strpos($v['province_name'], '市') !== false) {
                        $province = substr($v['province_name'],0,strpos($v['province_name'], '市'));
                    }
                    $data['series'][0]['data'][] = array('name'=>$v['province_name'],'value'=>$v['total']);
                    $data['series'][0]['nameMap'][$province] = $v['province_name'];
                    if ($max < $v['total']) {
                        $max =  $v['total'];
                    }
                }
            }

            $data['dataRange']['min'] = 0;
            $data['dataRange']['max'] = $max;
            $data['dataRange']['x'] = 'left';
            $data['dataRange']['y'] = 'bottom';
            $data['dataRange']['text'] = array($translator->trans('Chart High'),$translator->trans('Chart Low'));
            $data['dataRange']['calculable'] = true;

        } else {
            $data['title']['text'] = $translator->trans('Chart Inarea');
            $data['title']['textStyle']['fontSize'] = '13';
            $data['title']['x']  = 'center';
            $data['tooltip']['show'] = true;
            $data['tooltip']['trigger'] = 'item';
            $data['xAxis']['type'] = 'category';
            $data['yAxis'][0]['type'] = 'value';
            $data['yAxis'][0]['name'] = $translator->trans('Total nums');
            $data['yAxis'][0]['splitArea']['show'] =true;
            $data['series'][0]['name'] = $translator->trans('Total nums');
            $data['series'][0]['type'] = 'bar';
            $data['series'][0]['barWidth'] = 25;
            $data['series'][0]['itemStyle']['normal']['color'] = '#3B80F8';
            $data['series'][0]['itemStyle']['normal']['label']['show'] = true;
            $data['series'][0]['itemStyle']['normal']['label']['position'] ='top';
            $data['toolbox']['show'] = true;
            $data['toolbox']['feature']['mark']['show'] = false;
            $data['toolbox']['feature']['dataView']['show'] = true;
            $data['toolbox']['feature']['magicType']['show'] = true;
            $data['toolbox']['feature']['magicType']['type'] = array('line', 'bar');
            $data['toolbox']['feature']['myTool']['show'] = true;
            $data['toolbox']['feature']['myTool']['title'] = $translator->trans('Chart Map Change');
            $data['toolbox']['feature']['myTool']['magicType'] = 'map';
            $data['toolbox']['feature']['restore']['show'] = true;
            $data['toolbox']['feature']['saveAsImage']['show'] = true;

            if ($result['code'] == '200') {
                if (count($result['data']) > 10) {
                    $end = floor(1000/count($result['data'])) > 100 ? 100 : floor(1000/count($result['data']));
                } else {
                    $end = 100;
                }

                $data['dataZoom'] =  array(
                    'show'=>true,
                    'realtime'=>true,
                    'start'=>0,
                    'end'=>$end,
                    'fillerColor'=>'rgba(50,205,50,0.4)'
                );

                foreach ($result['data'] as $k => $v) {
                    $data['xAxis']['data'][$k] = $v['province_name'];
                    $data['series'][0]['data'][] = $v['total'];
                }
            }
        }

        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * 呼入地区分析图表获取列表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getDataForInareaListAction(Request $request)
    {
        $param['vcc_id'] = $this->getUser()->getVccId();
        $data = $request->get('data', '');
        $data = json_decode($data, true);
        $param['time_stamp'] = $data['time_stamp'];
        $param['start_date'] = $data['start_date'];
        $param['end_date'] = $data['end_date'];
        $param['type'] = $data['chartType'];
        $param['chosenVal'] = $data['chosenVal'];
        $param['chosenProvinces'] = $data['chosenProvinces'];

        $result = $this->get('icsoc_report.model.charts')->getInareaChartsDataFromMongo($param);
        $footer['province_name'] = $this->container->get("translator")->trans("Total");
        $footer['city'] = '';
        $footer['total'] = 0;
        $footer['mobile_nums'] = 0;
        $footer['tel_nums'] = 0;
        if (!empty($result['data'])) {
            foreach ($result['data'] as $k => $v) {
                $footer['total'] += $v['total'];
                $footer['mobile_nums'] += $v['mobile_nums'];
                $footer['tel_nums'] += $v['tel_nums'];
            }
        }

        $res = array(
            'total' => 1,
            'page' => 1,
            'records' => count($result['data']),
            'rows' => $result['data'],
            'userdata' => $footer,
        );

        return new JsonResponse($res);
    }

    /**
     * 获取省下的所有城市的呼入地区信息
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getCityDatasAction(Request $request)
    {
        $param['name'] = $request->get('name', '');
        $param['vccId'] = $this->getVccId();
        $condition = $request->get('condition', '');
        $condition = json_decode($condition, true);
        $param['time_stamp'] = $condition['time_stamp'];
        $param['start_date'] = $condition['start_date'];
        $param['end_date'] = $condition['end_date'];
        $param['type'] = $condition['chartType'];
        $param['chosenVal'] = $condition['chosenVal'];
        $param['magicType'] = $request->get('magicType', '');

        $result = $this->get('icsoc_report.model.charts')->getCityDataByProvinceName($param);
        $translator = $this->get('translator');

        if ($param['magicType'] == 'map') {
            $province = $param['name'];
            if (strpos($param['name'], '省') !== false) {
                $province = substr($param['name'],0,strpos($param['name'], '省'));
            }
            if (strpos($param['name'], '市') !== false) {
                $province = substr($param['name'],0,strpos($param['name'], '市'));
            }
            $data['title']['text'] = $translator->trans('Chart for city inarea');
            $data['title']['textStyle']['fontSize'] = '13';
            $data['title']['x']  = 'center';
            $data['tooltip']['trigger'] = 'item';
            $data['series'][0]['name'] = $translator->trans('Total nums');
            $data['series'][0]['type'] = 'map';
            $data['series'][0]['mapType'] = $province;
            $data['series'][0]['itemStyle']['normal']['label']['show'] = true;

            $data['toolbox']['show'] = true;
            $data['toolbox']['orient'] = 'vertical';
            $data['toolbox']['x'] = 'right';
            $data['toolbox']['y'] = 'center';
            $data['toolbox']['feature']['mark']['show'] = false;
            $data['toolbox']['feature']['dataView']['show'] = true;
            $data['toolbox']['feature']['myTool']['show'] = true;
            $data['toolbox']['feature']['myTool']['title'] = $translator->trans('Chart Bar Change');
            $data['toolbox']['feature']['myTool']['magicType'] = '';
            $data['toolbox']['feature']['restore']['show'] = true;
            $data['toolbox']['feature']['saveAsImage']['show'] = true;
            $data['roamController']['show'] = true;
            $data['roamController']['x'] = 'right';
            $data['roamController']['mapTypeControl']['china'] = true;

            $max = 0;
            foreach ($result['data'] as $v) {
                if ($v['city'] == $translator->trans('Chart Other')) {
                    continue;
                }
                $data['series'][0]['data'][] = array('name'=>$v['city'],'value'=>$v['total']);
                if ($max < $v['total']) {
                    $max =  $v['total'];
                }
            }

            $data['dataRange']['min'] = 0;
            $data['dataRange']['max'] = $max;
            $data['dataRange']['x'] = 'left';
            $data['dataRange']['y'] = 'bottom';
            $data['dataRange']['text'] = array($translator->trans('Chart High'),$translator->trans('Chart Low'));
            $data['dataRange']['calculable'] = true;

        } else {
            $data['title']['text'] = $translator->trans('Chart for city inarea');
            $data['title']['textStyle']['fontSize'] = '13';
            $data['title']['x']  = 'center';
            $data['tooltip']['show'] = true;
            $data['tooltip']['trigger'] = 'item';
            $data['xAxis']['type'] = 'category';
            $data['yAxis'][0]['type'] = 'value';
            $data['yAxis'][0]['name'] = $translator->trans('Total nums');
            $data['yAxis'][0]['splitArea']['show'] =true;
            $data['series'][0]['name'] = $translator->trans('Total nums');
            $data['series'][0]['type'] = 'bar';
            $data['series'][0]['barWidth'] = 25;
            $data['series'][0]['itemStyle']['normal']['color'] = '#ab78ba';
            $data['series'][0]['itemStyle']['normal']['label']['show'] = true;
            $data['series'][0]['itemStyle']['normal']['label']['position'] ='top';
            $data['toolbox']['show'] = true;
            $data['toolbox']['feature']['mark']['show'] = false;
            $data['toolbox']['feature']['dataView']['show'] = true;
            $data['toolbox']['feature']['magicType']['show'] = true;
            $data['toolbox']['feature']['magicType']['type'] = array('line', 'bar');
            $data['toolbox']['feature']['myTool']['show'] = true;
            $data['toolbox']['feature']['myTool']['title'] = $translator->trans('Chart Map Change');
            $data['toolbox']['feature']['myTool']['magicType'] = 'map';
            $data['toolbox']['feature']['restore']['show'] = true;
            $data['toolbox']['feature']['saveAsImage']['show'] = true;

            if (count($result['data']) > 10) {
                $end = floor(1000/count($result['data'])) > 100 ? 100 : floor(1000/count($result['data']));
            } else {
                $end = 100;
            }

            $data['dataZoom'] =  array(
                'show' => true,
                'realtime' => true,
                'start' => 0,
                'end' => $end,
                'fillerColor' => 'rgba(50,205,50,0.4)'
            );

            foreach ($result['data'] as $k => $v) {
                $data['xAxis']['data'][$k] = $v['city'];
                $data['series'][0]['data'][] = $v['total'];
            }
        }

        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * 获取省份下的城市数据列表
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getCityDatasForListAction(Request $request)
    {
        $param['name'] = $request->get('provincName', '');
        $param['vccId'] = $this->getVccId();
        $condition = $request->get('data', '');
        $condition = json_decode($condition, true);
        $param['time_stamp'] = $condition['time_stamp'];
        $param['start_date'] = $condition['start_date'];
        $param['end_date'] = $condition['end_date'];
        $param['type'] = $condition['chartType'];
        $param['chosenVal'] = $condition['chosenVal'];

        $result = $this->get('icsoc_report.model.charts')->getCityDataByProvinceName($param);

        return new JsonResponse($result['data']);
    }

    /**
     * 按小时进线图表
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function inhourChartsAction()
    {
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);

        return $this->render('IcsocReportBundle:Charts:inhour.html.twig', array(
            'action' => 'icsoc_report_charts_inhour_data',
            'statistical_range' => true,
            'type' => array(
                'number' => true,
                'queue' => true
            ),
            'date' => array(
                'startDate' => date("Y-m-d"),
                'endDate' => date("Y-m-d"),
                'startMonth' => date("Y-m"),
                'endMonth' => date("Y-m")
            ),
            'caption' => '按小时进线图表 【时间级别：小时 统计时段 %start_date% ~ %end_date%】',
            'isEnableGroup' => $isEnableGroup
        ));
    }

    /**
     * 按小时进线分析图表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function inhourChartsDataAction(Request $request)
    {
        $param['vcc_id'] = $this->getUser()->getVccId();
        $param['time_stamp'] = $request->get('time_stamp', '');
        $param['start_date'] = $request->get('start_date', '');
        $param['end_date'] = $request->get('end_date', '');
        $param['type'] = $request->get('chartType', '');
        $param['chosenIds'] = $request->get('chosenVal', '');

        //$result = $this->get('icsoc_report.model.charts')->getInhourChartsData($param);
        $result = $this->get('icsoc_report.model.charts')->getInhourChartsDataFromMongo($param);

        $translator = $this->get('translator');

        $data['title']['text'] = $translator->trans('Chart Inhour');
        $data['title']['textStyle']['fontSize'] = '13';
        $data['title']['x']  = 'center';
        $data['tooltip']['show'] = true;
        $data['tooltip']['trigger'] = 'item';
        $data['xAxis']['type'] = 'category';
        $data['yAxis'][0]['type'] = 'value';
        $data['yAxis'][0]['name'] = $translator->trans('Total nums');
        $data['yAxis'][0]['splitArea']['show'] =true;
        $data['series'][0]['name'] = $translator->trans('Total nums');
        $data['series'][0]['type'] = 'bar';
        $data['series'][0]['barWidth'] = 50;
        //$data['series'][0]['itemStyle']['normal']['color'] = '#3B80F8';
        $data['series'][0]['itemStyle']['normal']['label']['show'] = true;
        $data['series'][0]['itemStyle']['normal']['label']['position'] ='top';
        $data['toolbox']['show'] = true;
        $data['toolbox']['feature']['mark']['show'] = false;
        $data['toolbox']['feature']['dataView']['show'] = true;
        $data['toolbox']['feature']['magicType']['show'] = true;
        $data['toolbox']['feature']['magicType']['type'] = array('line', 'bar');
        $data['toolbox']['feature']['restore']['show'] = true;
        $data['toolbox']['feature']['saveAsImage']['show'] = true;

        if ($result['code'] == '200') {
            foreach ($result['data'] as $k => $v) {
                $data['xAxis']['data'][$k] = $v['start_hour'] . $translator->trans('Chart Hour');
                $data['series'][0]['data'][] = $v['total'];
            }
        }

        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * 获取按小时进线报表中的列表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getDataForInhourListAction(Request $request)
    {
        $param['vcc_id'] = $this->getUser()->getVccId();
        $data = $request->get('data', '');
        $data = json_decode($data, true);
        $param['time_stamp'] = $data['time_stamp'];
        $param['start_date'] = $data['start_date'];
        $param['end_date'] = $data['end_date'];
        $param['type'] = $data['chartType'];
        $param['chosenIds'] = $data['chosenVal'];

        $result = $this->get('icsoc_report.model.charts')->getInhourChartsDataFromMongo($param);
        $footer['start_hour'] = $this->container->get("translator")->trans("Total");
        $footer['total'] = 0;
        $footer['mobile_nums'] = 0;
        $footer['tel_nums'] = 0;
        if (!empty($result['data'])) {
            foreach ($result['data'] as $k => $v) {
                $result['data'][$k]['start_hour'] = $v['start_hour'].'时';
                $footer['total'] += $v['total'];
                $footer['mobile_nums'] += $v['mobile_nums'];
                $footer['tel_nums'] += $v['tel_nums'];
            }
        }

        $res = array(
            'total' => 1,
            'page' => 1,
            'records' => count($result['data']),
            'rows' => $result['data'],
            'userdata' => $footer,
        );

        return new JsonResponse($res);
    }

    /**
     * 手机固话分析图表
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callTypeChartsAction()
    {
        return $this->render('IcsocReportBundle:Charts:index.html.twig', array(
            'action' => 'icsoc_report_charts_calltype_data',
            'date' => array(
                'startDate' => date("Y-m-d"),
                'endDate' => date("Y-m-d"),
                'startMonth' => date("Y-m"),
                'endMonth' => date("Y-m")
            )
        ));
    }

    /**
     * 手机固话分析图表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function callTypeChartsDataAction(Request $request)
    {
        $param['vcc_id'] = $this->getUser()->getVccId();
        $param['time_stamp'] = $request->get('time_stamp', '');
        $param['start_date'] = $request->get('start_date', '');
        $param['end_date'] = $request->get('end_date', '');

        $result = $this->get('icsoc_report.model.charts')->getCallTypeChartsData($param);

        $translator = $this->get('translator');

        $data['title']['text'] = $translator->trans('Chart Calltype');
        $data['title']['textStyle']['fontSize'] = '13';
        $data['title']['x']  = 'center';
        $data['tooltip']['show'] = true;
        $data['tooltip']['trigger'] = 'item';
        $data['tooltip']['formatter'] = "{a} <br/>{b} : {c} ({d}%)";
        $data['legend']['orient'] = 'vertical';
        $data['legend']['x'] = 'left';
        $data['legend']['data'] = array($translator->trans('Chart Mobile'),$translator->trans('Chart Tel'));
        $data['calculable'] = false;
        $data['series'][0]['name'] = $translator->trans('Chart Caller Type');
        $data['series'][0]['type'] = 'pie';
        $data['series'][0]['radius'] = '55%';
        $data['series'][0]['center'] = array('50%',225);
        $data['series'][0]['itemStyle']['normal']['label']['show'] = true;
        $data['series'][0]['itemStyle']['normal']['label']['position'] = 'outer';
        $data['series'][0]['itemStyle']['normal']['label']['formatter'] = "{b}，{c}";
        $data['series'][0]['itemStyle']['normal']['label']['textStyle'] = array('color'=>'#000000');
        $data['toolbox']['show'] = true;
        $data['toolbox']['feature']['mark']['show'] = false;
        $data['toolbox']['feature']['dataView']['show'] = true;
        $data['toolbox']['feature']['magicType']['show'] = true;
        $data['toolbox']['feature']['magicType']['type'] = array('pie', 'funnel');
        $data['toolbox']['feature']['magicType']['option']['funnel'] = array(
            'x'=>'50%', 'funnelAlign'=>'left','max'=>'1548'
        );
        $data['toolbox']['feature']['restore']['show'] = true;
        $data['toolbox']['feature']['saveAsImage']['show'] = true;

        $agents = $this->get('icsoc_data.model.agent')->getAgentNameKeyedByIdArray($this->getUser()->getVccId());

        if ($result['code'] == '200') {
            foreach ($result['data'] as $v) {
                if ($v['caller_type'] == 0) {
                    if (isset($data['series'][0]['data'])) {
                        $data['series'][0]['data'][1] = array(
                            'value'=>$v['total'],
                            'name'=>$translator->trans('Chart Mobile')
                        );
                    } else {
                        $data['series'][0]['data'][0] = array(
                            'value'=>$v['total'],
                            'name'=>$translator->trans('Chart Mobile')
                        );
                    }
                } elseif ($v['caller_type'] == 1) {
                    if (isset($data['series'][0]['data'])) {
                        $data['series'][0]['data'][1] = array(
                            'value'=>$v['total'],
                            'name'=>$translator->trans('Chart Tel')
                        );
                    } else {
                        $data['series'][0]['data'][0] = array(
                            'value'=>$v['total'],
                            'name'=>$translator->trans('Chart Tel')
                        );
                    }
                }
            }
        }

        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * 满意度评价图表
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function evaluateChartsAction()
    {
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);

        return $this->render('IcsocReportBundle:Charts:index.html.twig', array(
            'action' => 'icsoc_report_charts_evaluate_data',
            'statistical_range' => true,
            'type' => array(
                'date' => true,
                'agent' => true,
                'group' => $isEnableGroup,
            ),
            'date' => array(
                'startDate' => date("Y-m-d"),
                'endDate' => date("Y-m-d"),
                'startMonth' => date("Y-m"),
                'endMonth' => date("Y-m"),
            )
        ));
    }

    /**
     * 满意度评价图表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function evaluateChartsDataAction(Request $request)
    {
        $param['vcc_id'] = $this->getUser()->getVccId();
        $param['time_stamp'] = $request->get('time_stamp', '');
        $param['start_date'] = $request->get('start_date', '');
        $param['end_date'] = $request->get('end_date', '');
        $param['type'] = $request->get('type', '');
        $param['agents'] = $request->get('node', '');
        $param['ques'] = $request->get('ques', '');

        $result = array();
        switch($param['type']){
            case 'agent':
                $result = $this->get('icsoc_report.model.charts')->getEvaluateChartsByAgentData($param);
                break;
            case 'date':
                $result = $this->get('icsoc_report.model.charts')->getEvaluateChartsByDateData($param);
                break;
        }

        $translator = $this->get('translator');

        $data['title']['text'] = $translator->trans('Chart Evaluate');
        $data['title']['textStyle']['fontSize'] = '13';
        $data['title']['x']    = 'left';
        $data['tooltip']['trigger'] = 'axis';
        $data['legend']['data']     = array('0', '1','2','3','4','5','6','7','8','9');
        $data['legend']['y']        = 'top';
        $data['toolbox']['show'] = true;
        $data['toolbox']['feature']['mark']['show'] = false;
        $data['toolbox']['feature']['dataView']['show'] = true;
        $data['toolbox']['feature']['magicType']['show'] = true;
        $data['toolbox']['feature']['magicType']['type'] = array('line', 'bar');
        $data['toolbox']['feature']['restore']['show'] = true;
        $data['toolbox']['feature']['saveAsImage']['show'] = true;

        for ($i = 0; $i <= 9; $i++) {
            $data['series'][$i] = array('name'=>"$i",'type'=>'bar','barWidth'=>40,'itemStyle'=>array(
                'normal'=>array('label'=>array('show'=>true,'position'=>'top')))
            );
        }

        $data['xAxis']['type']      = 'category';
        $data['yAxis'][0]['type']   = 'value';
        $data['yAxis'][0]['name']   = $translator->trans('Chart Evaluate Num');
        $data['yAxis'][0]['splitArea']['show'] =true;

        if ($result['code'] == '200') {

            if (!empty($result['total']) && $result['total'] > 10) {
                $end = floor(1000/$result['total']) > 100 ? 100 : floor(1000/$result['total']);
            } else {
                $end = 100;
            }

            $data['dataZoom'] =  array(
                'show'=>true,
                'realtime'=>true,
                'start'=>0,
                'end'=>$end,
                'fillerColor'=>'rgba(50,205,50,0.4)'
            );

            $agents = $this->get('icsoc_data.model.agent')->getAgentNameKeyedByIdArray($this->getUser()->getVccId());

            foreach ($result['data'] as $k => $v) {
                if ($param['type'] == 'agent') {
                    $data['xAxis']['data'][$k]   = isset($agents[$v['ag_id']]) ?
                        $agents[$v['ag_id']] : $v['ag_num'] . ' ' . $v['ag_name'];
                } else {
                    $data['xAxis']['data'][$k]   = $v['start_date'];
                }

                for ($i = 0; $i <= 9; $i++) {
                    if (!empty($v['e'.$i])) {
                        $data['series'][$i]['data'][] = $v['e'.$i];
                    } else {
                        $data['series'][$i]['data'][] = $v['e'.$i];
                    }
                }

            }
        }

        $result['data'] = $data;
        return new JsonResponse($result);
    }

    /**
     * 统计范围
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function treeAction(Request $request)
    {
        $type = $request->get('type', '');
        $param['vcc_id'] = $this->getUser()->getVccId();
        $param['vcc_code'] = $this->getUser()->getVccCode();

        switch($type){
            case 'agent':
                $param['agent'] = true;
                $result = $this->get('icsoc_report.model.charts')->getAgentTree($param);
                break;
            case 'number':
                $result = $this->get('icsoc_report.model.charts')->getNumberTree($param);
                break;
            case 'group':
                $result = $this->get('icsoc_report.model.charts')->getGroupTree($param);
                break;
            case 'queue':
                $result = $this->get('icsoc_report.model.charts')->getAgentTree($param);
                break;
            default:
                $result = $this->get('icsoc_report.model.charts')->getAgentTree($param);
                break;
        }

        return new JsonResponse($result);
    }

    public function getDataForChosenSelectAction(Request $request)
    {
        $type = $request->get('chosenType');
        $param['type'] = $type;
        $vccId = $this->getVccId();
        $param['vccId'] = $vccId;

        $result = $this->get('icsoc_report.model.charts')->getQueuesForChosenSelect($param);

        return new JsonResponse($result);
    }
}
