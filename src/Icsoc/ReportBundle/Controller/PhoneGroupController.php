<?php
/**
 * This file is part of manage.
 * Author: tangzhou
 * Date: 2016/10/20
 * Time: 15:35
 * File: PhoneGroupController.php
 */

namespace Icsoc\ReportBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * 号码组报表
 * Class PhoneGroupController
 * @package Icsoc\ReportBundle\Controller
 */
class PhoneGroupController extends ReportController
{
    public $title = array(
        'server_num'=>array('text'=>'号码组', 'field'=>'server_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'nowmonth'=>array('text'=>'日期', 'field'=>'nowmonth', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'nowdate'=>array('text'=>'日期', 'field'=>'nowdate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'total_num'=>array('text'=>'总来电量', 'field'=>'total_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'in_num'=>array('text'=>'总人工呼入量', 'field'=>'in_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'service'=>array('text'=>'人工服务占比', 'field'=>'service', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'in_conn_num'=>array('text'=>'呼入接通量', 'field'=>'in_conn_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'in_conn_num_rate'=>array('text'=>'呼入接通率', 'field'=>'in_conn_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'lost_num'=>array('text'=>'放弃量', 'field'=>'lost_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'ring_num'=>array('text'=>'振铃次数', 'field'=>'ring_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'ivr_num'=>array('text'=>'ivr呼入量', 'field'=>'ivr_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn5_num'=>array('text'=>'5秒接通量', 'field'=>'conn5_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn5_num_rate'=>array('text'=>'5秒接通率', 'field'=>'conn5_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn10_num'=>array('text'=>'10秒接通量', 'field'=>'conn10_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn10_num_rate'=>array('text'=>'10秒接通率', 'field'=>'conn10_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        //'lost10_num'=>array('text'=>'10秒放弃量', 'field'=>'lost10_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        //'lost10_num_rate'=>array('text'=>'10秒放弃率', 'field'=>'lost10_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn15_num'=>array('text'=>'15秒接通量', 'field'=>'conn15_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn15_num_rate'=>array('text'=>'15秒接通率', 'field'=>'conn15_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn20_num'=>array('text'=>'20秒接通量', 'field'=>'conn20_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn20_num_rate'=>array('text'=>'20秒接通率', 'field'=>'conn20_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        //'lost20_num'=>array('text'=>'20秒放弃量', 'field'=>'lost20_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        //'lost20_num_rate'=>array('text'=>'20秒放弃率', 'field'=>'lost20_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn25_num'=>array('text'=>'25秒接通量', 'field'=>'conn25_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn25_num_rate'=>array('text'=>'25秒接通率', 'field'=>'conn25_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        //'lost25_num'=>array('text'=>'25秒放弃量', 'field'=>'lost25_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        //'lost25_num_rate'=>array('text'=>'25秒放弃率', 'field'=>'lost25_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn30_num'=>array('text'=>'30秒接通量', 'field'=>'conn30_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn30_num_rate'=>array('text'=>'30秒接通率', 'field'=>'conn30_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        //'lost30_num'=>array('text'=>'30秒放弃量', 'field'=>'lost30_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        //'lost30_num_rate'=>array('text'=>'30秒放弃率', 'field'=>'lost30_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn40_num'=>array('text'=>'40秒接通量', 'field'=>'conn40_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        'conn40_num_rate'=>array('text'=>'40秒接通率', 'field'=>'conn40_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        //'lost40_num'=>array('text'=>'40秒放弃量', 'field'=>'lost40_num', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
        //'lost40_num_rate'=>array('text'=>'40秒放弃率', 'field'=>'lost40_num_rate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
    );

    /**
     * 号码组报表
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();
        $phoneGroups = $this->get('doctrine.dbal.default_connection')->fetchAll(
            "SELECT group_name,id FROM win_phone_group WHERE vcc_id = ?",
            array($this->getVccId())
        );

        return $this->render('IcsocReportBundle:Data:phoneGroup.html.twig', array(
            'phoneGroups' => $phoneGroups,
            'date' =>  $this->getData(),
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        $data = $request->get('data', '');
        $data = html_entity_decode(urldecode($data));
        $data = json_decode($data, true);

        $param['report_type'] = empty($data['report_type']) ? '' : $data['report_type'];
        $param['start_date'] = empty($data['start_date']) ? '' : $data['start_date'];
        $param['end_date'] = empty($data['end_date']) ? '' : $data['end_date'];
        $param['pg_id'] = empty($data['pg_id']) ? '' : $data['pg_id'];

        /** @var  $item (列表参数) */
        $item = array();
        $item['title'] = $this->title;
        $item['type'] = '';
        $item['caption'] = '号码组统计报表 【  %start_date% ~ %end_date%】';
        $item['url'] = 'icsoc_report_phone_group_list_data';
        $item['start_date'] = empty($param['start_date']) ? '' : $param['start_date'];
        $item['end_date'] = empty($param['end_date']) ? '' : $param['end_date'];

        switch($param['report_type']) {
            case 'month':
                $item['report_type'] = '月';
                $item['sortname'] = 'nowmonth';
                $item['sortorder'] = 'desc';
                unset($item['title']['nowdate']);
                break;
            case 'day':
                $item['report_type'] = '天';
                $item['sortname'] = 'nowdate';
                $item['sortorder'] = 'desc';
                unset($item['title']['nowmonth']);
                break;
        }

        return $this->render('IcsocReportBundle:Data:list.html.twig', array(
            'item'=>$item,
            'param'=>json_encode($param),
        ));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function dataAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');

        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );
        $list = $this->get('icsoc_data.model.develop')->getPhoneGroupData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
            )
        );

        $gridData = array();
        $translator = $this->get('translator');
        $connNum = array(5, 10, 15, 20, 25, 30, 40);  //接通
        $lostNum = array(10, 20, 25, 30, 40);  //未接通
        //初始化所有字段
        $footer[$sidx] = $translator->trans('Total');
        $footer['total_num'] = 0;
        $footer['in_num'] = 0;
        $footer['service'] = 0;
        $footer['in_conn_num'] = 0;
        $footer['in_conn_num_100'] = 0;
        $footer['lost_num'] = 0;
        $footer['ring_num'] = 0;
        $footer['ivr_num'] = 0;
        foreach ($connNum as $val) {
            $text = 'conn'.$val.'_num';
            $textRate = 'conn'.$val.'_num_rate';
            $footer[$text] = 0;
            $footer[$textRate] = 0;
        }
        foreach ($lostNum as $val) {
            $lost = 'lost'.$val.'_num';
            $lostRate = 'lost'.$val.'_num_rate';
            $footer[$lost] = 0;
            $footer[$lostRate] = 0;
        }
        //合计，计算所有的百分比
        if (isset($list['code']) && $list['code'] == 200) {
            //合计
            foreach ($list['data'] as $k => $v) {
                $gridData[$k] = $v;
                $footer['total_num'] += $v['total_num'];
                $footer['ivr_num'] += $v['ivr_num'];
                $footer['in_num'] += $v['in_num'];
                $footer['in_conn_num'] += $v['in_conn_num'];
                $footer['lost_num'] += $v['lost_num'];
                $footer['ring_num'] += $v['ring_num'];
                foreach ($connNum as $val) {
                    $text = 'conn'.$val.'_num';
                    $footer[$text] += $v[$text];
                }
                foreach ($lostNum as $val) {
                    $lost = 'lost'.$val.'_num';
                    $footer[$lost] += $v[$lost];
                }
            }
            //计算字段的百分比
            foreach ($list['data'] as $key => $val) {
                $list['data'][$key]['service'] = $gridData[$key]['service'] = empty($val['ivr_num']) ? '0%' : (round($val['in_num']/$val['ivr_num'], 4)*100).'%';
                $list['data'][$key]['in_conn_num_rate'] = $gridData[$key]['in_conn_num_rate'] = empty($val['in_num']) ? '0%' : (round($val['in_conn_num']/$val['in_num'], 4)*100).'%';
                foreach ($connNum as $v) {
                    $text = 'conn'.$v.'_num';
                    $textRate = 'conn'.$v.'_num_rate';
                    $list['data'][$key][$textRate] = empty($val['in_conn_num']) ? '0%' : (round($val[$text]/$val['in_conn_num'], 4)*100).'%';
                    $gridData[$key][$textRate] = $list['data'][$key][$textRate];
                }
                foreach ($lostNum as $v) {
                    $lost = 'lost'.$v.'_num';
                    $lostRate = 'lost'.$v.'_num_rate';
                    $list['data'][$key][$lostRate] = empty($val['lost_num']) ? '0%' : (round($val[$lost]/$val['lost_num'], 4)*100).'%';
                    $gridData[$key][$lostRate] = $list['data'][$key][$lostRate];
                }
            }

            //计算合计的百分比
            foreach ($connNum as $v) {
                $text = 'conn'.$v.'_num';
                $textRate = 'conn'.$v.'_num_rate';
                $footer[$textRate] = empty($footer['in_conn_num']) ? '0%' : (round($footer[$text]/$footer['in_conn_num'], 4)*100).'%';
            }
            foreach ($lostNum as $v) {
                $lost = 'lost'.$v.'_num';
                $lostRate = 'lost'.$v.'_num_rate';
                $footer[$lostRate] = empty($footer['lost_num']) ? '0%' : (round($footer[$lost]/$footer['lost_num'], 4)*100).'%';
            }
            $footer['service'] = empty($footer['ivr_num']) ? '0%' : (round($footer['in_num']/$footer['ivr_num'], 4)*100).'%';
            $footer['in_conn_num_rate'] = empty($footer['in_num']) ? '0%' : (round($footer['in_conn_num']/$footer['in_num'], 4)*100).'%';
        }

        if (!empty($export)) {
            /** @var  $title (导出字段) */
            $title = array(
                'server_num' => '号码组',
                $sidx => '日期',
                'total_num' => '总来电量',
                'in_num' => '总人工呼入量',
                'service' => '人工服务占比',
                'in_conn_num' => '呼入接通量',
                'in_conn_num_rate' => '呼入接通率',
                'lost_num' => '放弃量',
                'ring_num' => '振铃次数',
                'ivr_num' => 'ivr呼入量',
            );
            foreach ($connNum as $val) {
                $text = 'conn'.$val.'_num';
                $textName = $val.'秒接通量';
                $textRate = 'conn'.$val.'_num_rate';
                $textRateName = $val.'秒接通率';
                $title[$text] = $textName;
                $title[$textRate] = $textRateName;

            }
            $gridData[] = $footer;

            switch ($export) {
                case 'csv':
                    return $this->exportCsv($gridData, json_encode($title), array(
                        'text' => $translator->trans('Evaluate Collect Report'),
                        'href' => $this->generateUrl('icsoc_report_evaluatecollec'),
                    ));
                case 'excel':
                    return $this->exportExcel($gridData, json_encode($title), array(
                        'text' => $translator->trans('Evaluate Collect Report'),
                        'href' => $this->generateUrl('icsoc_report_evaluatecollec'),
                    ));
                default:
                    exit;
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : 1,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => empty($list['data']) ? '' : $list['data'],
            'userdata' => $footer,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }
}
