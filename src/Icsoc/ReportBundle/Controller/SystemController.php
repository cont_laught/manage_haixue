<?php

namespace Icsoc\ReportBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SystemController
 * @package Icsoc\ReportBundle\Controller
 */
class SystemController extends ReportController
{
    /**
     * 呼叫中心话务报表起始页
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('IcsocReportBundle:Data:index.html.twig', array(
            'action' => 'icsoc_report_system_list',
            'date' =>  $this->getData(),
        ));
    }

    /**
     * 呼叫中心话务报表列表设置
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        $data = $request->get('data', '');
        $data = html_entity_decode(urldecode($data));
        $data = json_decode($data, true);

        $param['report_type'] = empty($data['report_type']) ? '' : $data['report_type'];
        $param['start_date'] = empty($data['start_date']) ? '' : $data['start_date'];
        $param['end_date'] = empty($data['end_date']) ? '' : $data['end_date'];

        $rst = $this->get('icsoc_report.model.report')->getTitleReport(array('report_name' => 'system'));
        $title = array();
        if (isset($rst['code']) && $rst['code'] == 200) {
            $title = $rst['data'];
        }

        $item['title'] = $title;
        $item['type'] = 'system';
        $item['caption'] = 'System Report 【%start_date% ~ %end_date%】';
        $item['url'] = 'icsoc_report_system_data';
        $item['start_date'] = empty($param['start_date']) ? '' : $param['start_date'];
        $item['end_date'] = empty($param['end_date']) ? '' : $param['end_date'];

        switch ($param['report_type']) {
            case 'month':
                $item['report_type'] = '月';
                $item['sortname'] = 'start_date';
                $item['sortorder'] = 'desc';
                break;
            case 'day':
                $item['report_type'] = '天';
                $item['sortname'] = 'nowdate';
                $item['sortorder'] = 'desc';
                break;
            case 'hour':
                $item['report_type'] = '小时';
                $item['sortname'] = 'start_date,time_stamp';
                $item['sortorder'] = 'asc';
                break;
            case 'halfhour':
                $item['report_type'] = '半小时';
                $item['sortname'] = 'id';
                $item['sortorder'] = 'asc';
                break;
        }

        return $this->render('IcsocReportBundle:Data:list.html.twig', array(
            'item' => $item,
            'param' => json_encode($param),
        ));
    }

    /**
     * 列表数据获取
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function dataAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));
        $data = json_decode($data, true);

        $startDate = empty($data['start_date']) ? '' : $data['start_date'];
        $endDate = empty($data['end_date']) ? '' : $data['end_date'];

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', '1');

        /**
         * 处理排序字段
         */
        if ($sidx == 'date') {
            switch ($data['report_type']) {
                case 'month':
                    $sidx = 'start_date';
                    break;
                case 'day':
                    $sidx = 'nowdate';
                    break;
                case 'hour':
                    $sidx = array('start_date', 'time_stamp');
                    break;
                case 'halfhour':
                    $sidx = array('start_date', 'time_stamp');
                    break;
            }
        }

        $export = $request->get('export', '');

        /** @var  $param (数据参数) */
        $param = array(
            'export' => $export,
            'pagination' => array(
                'rows' => $rows,
                'page' => $page,
            ),
            'sort' => array(
                'order' => $sord,
                'field' => $sidx,
            ),
            'filter' => array(
                'start_date' => $startDate,
                'end_date' => $endDate,
            ),
        );
        $param = array(
            'vcc_code' => $this->getVccCode(),
            'info' => json_encode($param),
        );

        switch ($data['report_type']) {
            case 'month':
                $result = $this->get('icsoc_report.model.system')->getSystemMonthData($param);
                break;
            case 'day':
                $result = $this->get('icsoc_report.model.system')->getSystemDayData($param);
                break;
            case 'hour':
                $result = $this->get('icsoc_report.model.system')->getSystemHourData($param);
                break;
            case 'halfhour':
                $result = $this->get('icsoc_report.model.system')->getSystemHalfHourData($param);
                break;
            default:
                $result = $this->get('icsoc_report.model.system')->getSystemHourData($param);
                break;
        }

        $translator = $this->get('translator');
        $footer['date']       = $translator->trans('Total');
        $footer['ivr_num']    = 0;
        $footer['in_num']     = 0;
        $footer['lost_num']   = 0;
        $footer['lost_secs']  = 0;
        $footer['conn_num']   = 0;
        $footer['queue_secs'] = 0;
        $footer['ring_num']   = 0;
        $footer['ring_secs']  = 0;
        $footer['login_secs'] = 0;
        $footer['conn_secs']  = 0;
        $footer['wait_secs']  = 0;
        $footer['deal_secs']  = 0;
        $footer['agents']     = 0;
        $footer['avg_num']    = 0;
        $footer['conn5_num']  = 0;
        $footer['conn10_num'] = 0;
        $footer['conn15_num'] = 0;
        $footer['conn20_num'] = 0;
        $footer['conn30_num'] = 0;
        $footer['conn60_num'] = 0;
        $footer['lost10_num'] = 0;
        $footer['lost20_num'] = 0;
        $footer['lost25_num'] = 0;
        $footer['lost30_num'] = 0;
        $footer['lost35_num'] = 0;
        $footer['lost40_num'] = 0;
        $gridData = array();
        if ($result['code'] == 200) {
            foreach ($result['rows'] as $k => $v) {
                $gridData[$k] = $v;  //在model 中整理了
                /** 合计 */
                $footer['ivr_num'] = $footer['ivr_num']+$v['ivr_num'];
                $footer['in_num'] = $footer['in_num']+$v['in_num'];
                $footer['lost_num'] = $footer['lost_num']+$v['lost_num'];
                $footer['lost_secs'] = $footer['lost_secs']+$v['lost_secs'];
                $footer['conn_num'] = $footer['conn_num'] + $v['conn_num'];
                $footer['queue_secs'] =  $footer['queue_secs'] + $v['queue_secs'];
                $footer['ring_num'] = $footer['ring_num'] + $v['ring_num'];
                $footer['ring_secs']  = $footer['ring_secs'] + $v['ring_secs'];
                $footer['login_secs'] = $footer['login_secs'] + $v['login_secs'];
                $footer['conn_secs'] = $footer['conn_secs'] + $v['conn_secs'];
                $footer['wait_secs'] = $footer['wait_secs'] + $v['wait_secs'];
                $footer['deal_secs'] = $footer['deal_secs'] + $v['deal_secs'];
                $footer['agents'] = $footer['agents'] + $v['agents'];
                $footer['avg_num'] = $footer['avg_num'] + $v['avg_num'];
                $footer['conn5_num']  = $footer['conn5_num'] + $v['conn5_num'];
                $footer['conn10_num'] = $footer['conn10_num'] + $v['conn10_num'];
                $footer['conn15_num'] = $footer['conn15_num'] + $v['conn15_num'];
                $footer['conn20_num'] = $footer['conn20_num'] + $v['conn20_num'];
                $footer['conn30_num'] = $footer['conn30_num'] + $v['conn30_num'];
                $footer['lost10_num'] = $footer['lost10_num'] + $v['lost10_num'];
                $footer['lost20_num'] = $footer['lost20_num'] + $v['lost20_num'];
                $footer['lost25_num'] = $footer['lost25_num'] + $v['lost25_num'];
                $footer['lost30_num'] = $footer['lost30_num'] + $v['lost30_num'];
                $footer['lost35_num'] = $footer['lost35_num'] + $v['lost35_num'];
                $footer['lost40_num'] = $footer['lost40_num'] + $v['lost40_num'];
            }
            $rst = $this->get('icsoc_report.model.report')->getDataReport(
                array(
                    'report_name' => 'system',
                    'work_time' => count($rows),
                    'data' => array($footer),
                )
            );
            $footerData = array();
            if (isset($rst['code']) && $rst['code'] == 200) {
                $footerData = $rst['data'][0];
            }

            $footer['agents'] = count($rows) > 0 ? ceil($footer['agents']/count($rows)) : 0;
            $footer['avg_num'] = count($rows) > 0 ? round($footer['avg_num']/count($rows)) : 0;
            $footer = array_merge($footerData, $footer);
            $result = array(
                'total' => $result['total'],
                'page' => $result['page'],
                'records' => $result['records'],
                'rows' => $gridData,
                'userdata' => $footer,
            );
        } else {
            $result = array(
                'total' => 0,
                'page' => 1,
                'records' => 0,
                'rows' => $gridData,
                'code' => $result['code'],
                'message' => $result['message'],
            );
        }

        /** 导出功能 */
        if (!empty($export)) {
            $gridData[] = $footer;
            $type = $request->get('type', 'system');
            $rst = $this->get('icsoc_report.model.report')->getTitleReport(array('report_name' => $type));
            $title = array();
            if (isset($rst['code']) && $rst['code'] == 200) {
                $title = $rst['data'];
            }
            $titleItem = array();
            foreach ($title as $key => $v) {
                $titleItem[$v['field']] = $v['text'];
            }

            switch ($export) {
                case 'csv':
                    return $this->exportCsv($gridData, json_encode($titleItem), array(
                        'text' => $translator->trans('System Report'),
                        'href' => $this->generateUrl('icsoc_report_system_home'),
                    ));
                case 'excel':
                    return $this->exportExcel($gridData, json_encode($titleItem), array(
                        'text' => $translator->trans('System Report'),
                        'href' => $this->generateUrl('icsoc_report_system_home'),
                    ));
                default:
                    exit;
            }
        }

        return new JsonResponse($result);
    }
}
