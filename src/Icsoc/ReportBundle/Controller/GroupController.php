<?php

namespace Icsoc\ReportBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GroupController
 * @package Icsoc\ReportBundle\Controller
 */
class GroupController extends ReportController
{
    /**
     * 业务组工作表现报表起始页
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);

        return $this->render('IcsocReportBundle:Data:index.html.twig', array(
            'action' => 'icsoc_report_group_list',
            'groups' => $groups,
            'date' =>  $this->getData(),
        ));
    }

    /**
     * 业务组工作表现报表列表设置
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        $data = $request->get('data', '');
        $data = html_entity_decode(urldecode($data));
        $data = json_decode($data, true);

        $param['report_type'] = empty($data['report_type']) ? '' : $data['report_type'];
        $param['start_date'] = empty($data['start_date']) ? '' : $data['start_date'];
        $param['end_date'] = empty($data['end_date']) ? '' : $data['end_date'];
        $param['group_id'] = empty($data['group_id']) ? '' : $data['group_id'];

        $rst = $this->get('icsoc_report.model.report')->getTitleReport(
            array(
                'report_name' => 'group',
                'report_type' => $param['report_type'],
            )
        );
        $title = array();
        if (isset($rst['code']) && $rst['code'] == 200) {
            $title = $rst['data'];
        }

        /** @var  $item (列表参数) */
        $item = array();
        $item['title'] = $title;
        $item['type'] = 'group';
        $item['caption'] = 'Group Caption 【%start_date% ~ %end_date%】';
        $item['url'] = 'icsoc_report_group_data';
        $item['start_date'] = empty($param['start_date']) ? '' : $param['start_date'];
        $item['end_date'] = empty($param['end_date']) ? '' : $param['end_date'];

        switch ($param['report_type']) {
            case 'month':
                $item['report_type'] = '月';
                $item['sortname'] = 'start_date';
                $item['sortorder'] = 'desc';
                break;
            case 'day':
                $item['report_type'] = '天';
                $item['sortname'] = 'nowdate';
                $item['sortorder'] = 'desc';
                break;
            case 'hour':
                $item['report_type'] = '小时';
                $item['sortname'] = 'start_date,time_stamp';
                $item['sortorder'] = 'asc';
                break;
            case 'halfhour':
                $item['report_type'] = '半小时';
                $item['sortname'] = 'start_date,time_stamp';
                $item['sortorder'] = 'asc';
                break;
        }

        return $this->render('IcsocReportBundle:Data:list.html.twig', array(
            'item'=>$item,
            'param'=>json_encode($param),
        ));
    }

    /**
     * 获取业务组工作报表数据
     * @param Request $request
     * @return JsonResponse
     */
    public function dataAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));
        $data = json_decode($data, true);
        $startDate = empty($data['start_date']) ? '' : $data['start_date'];
        $endDate = empty($data['end_date']) ? '' : $data['end_date'];
        $groupId = empty($data['group_id']) ? '' : $data['group_id'];

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', '1');

        /**
         * 处理排序字段
         */
        if ($sidx == 'date') {
            switch ($data['report_type']) {
                case 'month':
                    $sidx = 'start_date';
                    break;
                case 'day':
                    $sidx = 'nowdate';
                    break;
                case 'hour':
                    $sidx = array('start_date', 'time_stamp');
                    break;
                case 'halfhour':
                    $sidx = array('start_date', 'time_stamp');
                    break;
            }
        }

        $export = $request->get('export', '');

        $param = array(
            'export' => $export,
            'pagination' => array(
                'rows' => $rows,
                'page' => $page,
            ),
            'sort' => array(
                'filed' => $sidx,
                'order' => $sord,
            ),
            'filter' => array(
                'start_date' => $startDate,
                'end_date' => $endDate,
                'group_id' => $groupId,
            ),
        );
        $param = array(
            'vcc_code' => $this->getVccCode(),
            'info' => json_encode($param),
        );
        switch ($data['report_type']) {
            case 'month':
                $result = $this->get('icsoc_report.model.group')->getGroupMonthDate($param);
                break;
            case 'day':
                $result = $this->get('icsoc_report.model.group')->getGroupDayDate($param);
                break;
            case 'hour':
                $result = $this->get('icsoc_report.model.group')->getGroupHourDate($param);
                break;
            case 'halfhour':
                $result = $this->get('icsoc_report.model.group')->getGroupHalfHourDate($param);
                break;
            default:
                $result = $this->get('icsoc_report.model.agent')->getAgentHourData($param);
                break;
        }

        $translator = $this->get('translator');
        $footer['date'] = $translator->trans('Total');
        $footer['in_num'] = 0;
        $footer['out_num'] = 0;
        $footer['conn_secs'] = 0;
        $footer['internal_num'] = 0;
        $footer['wait_secs'] = 0;
        $footer['wait_num'] = 0;
        $footer['ring_num'] = 0;
        $footer['out_calls'] = 0;
        $footer['ring_secs'] = 0;
        $footer['consult_num'] = 0;
        $footer['consult_secs'] = 0;
        $footer['hold_num'] = 0;
        $footer['hold_secs'] = 0;
        $footer['conference_num'] = 0;
        $footer['conference_secs'] = 0;
        $footer['shift_num'] = 0;
        $footer['login_secs'] = 0;
        $footer['ready_secs'] = 0;
        $footer['busy_secs'] = 0;
        $footer['total_ringsecs'] = 0;
        $footer['evaluate_-4'] = 0;
        $footer['evaluate_-3'] = 0;
        $footer['evaluate_-2'] = 0;
        $footer['evaluate_-1'] = 0;
        $footer['evaluate_0'] = 0;
        $footer['evaluate_1'] = 0;
        $footer['evaluate_2'] = 0;
        $footer['evaluate_3'] = 0;
        $footer['evaluate_4'] = 0;
        $footer['evaluate_5'] = 0;
        $footer['evaluate_6'] = 0;
        $footer['evaluate_7'] = 0;
        $footer['evaluate_8'] = 0;
        $footer['evaluate_9'] = 0;
        $footer['refuse_num'] = 0;
        $footer['total_waitsecs'] = 0;

        if ($data['report_type'] != 'halfhour') {
            $agentStaReason = $this->get('icsoc_data.model.report')->getAgentStaReason($this->getVccId());
        } else {
            $agentStaReason = array();
        }

        foreach ($agentStaReason as $key => $v) {
            $footer['agstanum'.$key] =  0;
            $footer['agstaduration'.$key] = 0;
        }
        $footer['agstanum_other'] =  0;
        $footer['agstaduration_other'] = 0;

        $gridData = array();
        if ($result['code'] == 200) {
            foreach ($result['rows'] as $k => $v) {
                $gridData[$k] = $v;
                /* 合计 */
                $footer['evaluate_-4'] = $footer['evaluate_-4'] + $v['evaluate_-4'];
                $footer['evaluate_-3'] = $footer['evaluate_-3'] + $v['evaluate_-3'];
                $footer['evaluate_-2'] = $footer['evaluate_-2'] + $v['evaluate_-2'];
                $footer['evaluate_-1'] = $footer['evaluate_-1'] + $v['evaluate_-1'];
                $footer['evaluate_0'] = $footer['evaluate_0'] + $v['evaluate_0'];
                $footer['evaluate_1'] = $footer['evaluate_1'] + $v['evaluate_1'];
                $footer['evaluate_2'] = $footer['evaluate_2'] + $v['evaluate_2'];
                $footer['evaluate_3'] = $footer['evaluate_3'] + $v['evaluate_3'];
                $footer['evaluate_4'] = $footer['evaluate_4'] + $v['evaluate_4'];
                $footer['evaluate_5'] = $footer['evaluate_5'] + $v['evaluate_5'];
                $footer['evaluate_6'] = $footer['evaluate_6'] + $v['evaluate_6'];
                $footer['evaluate_7'] = $footer['evaluate_7'] + $v['evaluate_7'];
                $footer['evaluate_8'] = $footer['evaluate_8'] + $v['evaluate_8'];
                $footer['evaluate_9'] = $footer['evaluate_9'] + $v['evaluate_9'];
                $footer['refuse_num'] = $footer['refuse_num'] + $v['refuse_num'];
                $footer['in_num'] = $footer['in_num']+$v['in_num'];
                $footer['out_num'] = $footer['out_num']+$v['out_num'];
                $footer['conn_secs'] = $footer['conn_secs']+$v['conn_secs'];
                $footer['internal_num'] = $footer['internal_num']+$v['internal_num'];
                $footer['wait_num'] = $footer['wait_num']+$v['wait_num'];
                $footer['wait_secs'] = $footer['wait_secs']+$v['wait_secs'];
                $footer['ring_num'] = $footer['ring_num']+$v['ring_num'];
                $footer['out_calls'] = $footer['out_calls']+$v['out_calls'];
                $footer['ring_secs'] = $footer['ring_secs']+$v['ring_secs'];
                $footer['consult_num'] = $footer['consult_num']+$v['consult_num'];
                $footer['consult_secs'] = $footer['consult_secs']+$v['consult_secs'];
                $footer['hold_num'] = $footer['hold_num']+$v['hold_num'];
                $footer['hold_secs'] = $footer['hold_secs']+$v['hold_secs'];
                $footer['conference_num'] = $footer['conference_num']+$v['conference_num'];
                $footer['conference_secs'] = $footer['conference_secs']+$v['conference_secs'];
                $footer['shift_num'] = $footer['shift_num']+$v['shift_num'];
                $footer['login_secs'] = $footer['login_secs']+$v['login_secs'];
                $footer['busy_secs'] = $footer['busy_secs']+$v['busy_secs'];
                $footer['ready_secs'] = $footer['ready_secs']+$v['ready_secs'];
                $footer['total_ringsecs'] = $footer['total_ringsecs']+$v['total_ringsecs'];
                $footer['total_waitsecs'] = $footer['total_waitsecs']+$v['total_waitsecs'];

                foreach ($agentStaReason as $key => $value) {
                    $footer['agstanum'.$key] = $footer['agstanum'.$key]+$v['agstanum'.$key];
                    $footer['agstaduration'.$key] = $footer['agstaduration'.$key]+$v['agstaduration'.$key];
                }
                $footer['agstanum_other'] =  $footer['agstanum_other'] + (isset($v['agstanum_other']) ? $v['agstanum_other'] : 0);
                $footer['agstaduration_other'] = $footer['agstaduration_other'] + (isset($v['agstaduration_other']) ? $v['agstaduration_other'] : 0);
            }
            $rst = $this->get('icsoc_report.model.report')->getdataReport(
                array(
                    'report_name'=>'group',
                    'data'=>array($footer),
                )
            );
            $footerData = array();
            if (isset($rst['code']) && $rst['code'] == 200) {
                $footerData = $rst['data'][0];
            }
            $footer = array_merge($footerData, $footer);

            $result = array(
                'total' => $result['total'],
                'page' => $result['page'],
                'records' => $result['records'],
                'rows' => $gridData,
                'userdata' => $footer,
            );

        } else {
            $result = array(
                'total' => 0,
                'page' => 1,
                'records' => 0,
                'rows' => $gridData,
                'code' => $result['code'],
                'message' => $result['message'],
            );
        }

        /** 导出功能 */
        if (!empty($export)) {
            $gridData[] = $footer;
            $type = $request->get('type', 'group');
            $rst = $this->get('icsoc_report.model.report')->getTitleReport(
                array(
                    'report_name' => $type,
                    'report_type' => $data['report_type'],
                )
            );
            $title = array();
            if (isset($rst['code']) && $rst['code'] == 200) {
                $title = $rst['data'];
            }
            $titleItem = array();
            foreach ($title as $key => $v) {
                $titleItem[$v['field']] = $v['text'];
            }
            switch ($export) {
                case 'csv':
                    return $this->exportCsv($gridData, json_encode($titleItem), array(
                        'text' => $translator->trans('Group Caption'),
                        'href' => $this->generateUrl('icsoc_report_group_home'),
                    ));
                case 'excel':
                    return $this->exportExcel($gridData, json_encode($titleItem), array(
                        'text' => $translator->trans('Group Caption'),
                        'href' => $this->generateUrl('icsoc_report_group_home'),
                    ));
                default:
                    exit;
            }
        }

        return new JsonResponse($result);
    }
}
