<?php

namespace Icsoc\ReportBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class QueueController
 * @package Icsoc\ReportBundle\Controller
 */
class QueueController extends ReportController
{
    /**
     * 技能组话务报表起始页
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /** @var  $queues (全部技能组数据) */
        $queues = $this->get('icsoc_data.model.queue')->getQueueNameKeyedByIdArray($this->getUser()->getVccId(), true, true);

        return $this->render('IcsocReportBundle:Data:index.html.twig', array(
            'action' => 'icsoc_report_queue_list',
            'queues' => $queues,
            'date' => $this->getData(),
        ));
    }

    /**
     * 技能组话务报表列表设置
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        $data = $request->get('data', '');
        $data = html_entity_decode(urldecode($data));
        $data = json_decode($data, true);

        $param['report_type'] = empty($data['report_type']) ? '' : $data['report_type'];
        $param['start_date'] = empty($data['start_date']) ? '' : $data['start_date'];
        $param['end_date'] = empty($data['end_date']) ? '' : $data['end_date'];
        $param['que_id'] = empty($data['que_id']) ? '' : $data['que_id'];

        $rst = $this->get('icsoc_report.model.report')->getTitleReport(array('report_name' => 'queue'));
        $title = array();
        if (isset($rst['code']) && $rst['code'] == 200) {
            $title = $rst['data'];
        }

        $item['title'] = $title;
        $item['type'] = 'queue';
        $item['caption'] = 'Queue Caption 【%start_date% ~ %end_date%】';
        $item['url'] = 'icsoc_report_queue_data';
        $item['start_date'] = empty($param['start_date']) ? '' : $param['start_date'];
        $item['end_date'] = empty($param['end_date']) ? '' : $param['end_date'];

        switch ($param['report_type']) {
            case 'month':
                $item['report_type'] = '月';
                $item['sortname'] = 'start_date';
                $item['sortorder'] = 'desc';
                break;
            case 'day':
                $item['report_type'] = '天';
                $item['sortname'] = 'nowdate';
                $item['sortorder'] = 'desc';
                break;
            case 'hour':
                $item['report_type'] = '小时';
                $item['sortname'] = 'start_date,time_stamp';
                $item['sortorder'] = 'asc';
                break;
            case 'halfhour':
                $item['report_type'] = '半小时';
                $item['sortname'] = 'start_date,time_stamp';
                $item['sortorder'] = 'asc';
                break;
        }

        return $this->render('IcsocReportBundle:Data:list.html.twig', array(
            'item' => $item,
            'param' => json_encode($param),
        ));
    }

    /**
     * 列表数据获取
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function dataAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));
        $data = json_decode($data, true);

        $startDate = empty($data['start_date']) ? '' : $data['start_date'];
        $endDate = empty($data['end_date']) ? '' : $data['end_date'];
        $queId = empty($data['que_id']) ? '' : $data['que_id'];

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', '1');

        /**
         * 处理排序字段
         */
        if ($sidx == 'date') {
            switch ($data['report_type']) {
                case 'month':
                    $sidx = 'start_date';
                    break;
                case 'day':
                    $sidx = 'nowdate';
                    break;
                case 'hour':
                    $sidx = array('start_date', 'time_stamp');
                    break;
                case 'halfhour':
                    $sidx = array('start_date', 'time_stamp');
                    break;
            }
        }
        $export = $request->get('export', '');

        /** @var  $param (数据参数) */
        $param = array(
            'export' => $export,
            'pagination' => array(
                'rows' => $rows,
                'page' => $page,
            ),
            'sort' => array(
                'order' => $sord,
                'field' => $sidx,
            ),
            'filter' => array(
                'start_date' => $startDate,
                'end_date' => $endDate,
                'que_id' => $queId,
            ),
        );
        $param = array(
            'vcc_code' => $this->getVccCode(),
            'info' => json_encode($param),
        );

        switch ($data['report_type']) {
            case 'month':
                $result = $this->get('icsoc_report.model.queue')->getQueueMonthData($param);
                break;
            case 'day':
                $result = $this->get('icsoc_report.model.queue')->getQueueDayData($param);
                break;
            case 'hour':
                $result = $this->get('icsoc_report.model.queue')->getQueueHourData($param);
                break;
            case 'halfhour':
                $result = $this->get('icsoc_report.model.queue')->getQueueHalfHourData($param);
                break;
            default:
                $result = $this->get('icsoc_report.model.queue')->getQueueHourData($param);
                break;
        }

        $translator = $this->get('translator');
        $footer['date'] = $translator->trans('Total');
        $footer['in_num'] = 0;
        $footer['out_num'] = 0;
        $footer['out_calls'] = 0;
        $footer['out_secs'] = 0;
        $footer['queue_in_num'] = 0;
        $footer['lost_num'] = 0;
        $footer['lost_secs'] = 0;
        $footer['conn_num'] = 0;
        $footer['queue_secs'] = 0;
        $footer['ring_num'] = 0;
        $footer['ring_secs'] = 0;
        $footer['wait_secs'] = 0;
        $footer['conn_secs'] = 0;
        $footer['deal_secs'] = 0;
        $footer['lost1_num'] = 0;
        $footer['lost1_secs'] = 0;
        $footer['lost3_num'] = 0;
        $footer['lost3_secs'] = 0;
        $footer['lost4_num'] = 0;
        $footer['lost4_secs'] = 0;
        $footer['lost5_num'] = 0;
        $footer['lost5_secs'] = 0;
        $footer['conn5_num'] = 0;
        $footer['conn10_num'] = 0;
        $footer['conn15_num'] = 0;
        $footer['conn20_num'] = 0;
        $footer['conn30_num'] = 0;
        $footer['lost10_num'] = 0;
        $footer['lost20_num'] = 0;
        $footer['lost25_num'] = 0;
        $footer['lost30_num'] = 0;
        $footer['lost35_num'] = 0;
        $footer['lost40_num'] = 0;
        $footer['evaluate_-4'] = 0;
        $footer['evaluate_-3'] = 0;
        $footer['evaluate_-2'] = 0;
        $footer['evaluate_-1'] = 0;
        $footer['evaluate_0'] = 0;
        $footer['evaluate_1'] = 0;
        $footer['evaluate_2'] = 0;
        $footer['evaluate_3'] = 0;
        $footer['evaluate_4'] = 0;
        $footer['evaluate_5'] = 0;
        $footer['evaluate_6'] = 0;
        $footer['evaluate_7'] = 0;
        $footer['evaluate_8'] = 0;
        $footer['evaluate_9'] = 0;
        $footer['first_conn_num'] = 0;

        $gridData = array();

        if ($result['code'] == 200) {
            foreach ($result['rows'] as $k => $v) {
                $gridData[$k] = $v;
                /* 合计 */
                $footer['evaluate_-4'] = $footer['evaluate_-4'] + $v['evaluate_-4'];
                $footer['evaluate_-3'] = $footer['evaluate_-3'] + $v['evaluate_-3'];
                $footer['evaluate_-2'] = $footer['evaluate_-2'] + $v['evaluate_-2'];
                $footer['evaluate_-1'] = $footer['evaluate_-1'] + $v['evaluate_-1'];
                $footer['evaluate_0'] = $footer['evaluate_0'] + $v['evaluate_0'];
                $footer['evaluate_1'] = $footer['evaluate_1'] + $v['evaluate_1'];
                $footer['evaluate_2'] = $footer['evaluate_2'] + $v['evaluate_2'];
                $footer['evaluate_3'] = $footer['evaluate_3'] + $v['evaluate_3'];
                $footer['evaluate_4'] = $footer['evaluate_4'] + $v['evaluate_4'];
                $footer['evaluate_5'] = $footer['evaluate_5'] + $v['evaluate_5'];
                $footer['evaluate_6'] = $footer['evaluate_6'] + $v['evaluate_6'];
                $footer['evaluate_7'] = $footer['evaluate_7'] + $v['evaluate_7'];
                $footer['evaluate_8'] = $footer['evaluate_8'] + $v['evaluate_8'];
                $footer['evaluate_9'] = $footer['evaluate_9'] + $v['evaluate_9'];
                $footer['first_conn_num'] = $footer['first_conn_num'] + $v['first_conn_num'];
                $footer['in_num'] = $footer['in_num'] + $v['in_num'];
                $footer['out_num'] = $footer['out_num'] + $v['out_num'];
                $footer['out_calls'] = $footer['out_calls'] + $v['out_calls'];
                $footer['out_secs'] = $footer['out_secs'] + $v['out_secs'];
                $footer['queue_in_num'] = $footer['queue_in_num'] + $v['queue_in_num'];
                $footer['lost_num'] = $footer['lost_num'] + $v['lost_num'];
                $footer['lost_secs'] = $footer['lost_secs'] + $v['lost_secs'];
                $footer['conn_num'] = $footer['conn_num'] + $v['conn_num'];
                $footer['queue_secs'] = $footer['queue_secs'] + $v['queue_secs'];
                $footer['ring_num'] = $footer['ring_num'] + $v['ring_num'];
                $footer['ring_secs'] = $footer['ring_secs'] + $v['ring_secs'];
                $footer['conn_secs'] = $footer['conn_secs'] + $v['conn_secs'];
                $footer['wait_secs'] = $footer['wait_secs'] + $v['wait_secs'];
                $footer['deal_secs'] = $footer['deal_secs'] + $v['deal_secs'];
                $footer['lost1_num'] = $footer['lost1_num'] + $v['lost1_num'];
                $footer['lost1_secs'] = $footer['lost1_secs'] + $v['lost1_secs'];
                $footer['lost3_num'] = $footer['lost3_num'] + $v['lost3_num'];
                $footer['lost3_secs'] = $footer['lost3_secs'] + $v['lost3_secs'];
                $footer['lost4_num'] = $footer['lost4_num'] + $v['lost4_num'];
                $footer['lost4_secs'] = $footer['lost4_secs'] + $v['lost4_secs'];
                $footer['lost5_num'] = $footer['lost5_num'] + $v['lost5_num'];
                $footer['lost5_secs'] = $footer['lost5_secs'] + $v['lost5_secs'];
                $footer['conn5_num'] = $footer['conn5_num'] + $v['conn5_num'];
                $footer['conn10_num'] = $footer['conn10_num'] + $v['conn10_num'];
                $footer['conn15_num'] = $footer['conn15_num'] + $v['conn15_num'];
                $footer['conn20_num'] = $footer['conn20_num'] + $v['conn20_num'];
                $footer['conn30_num'] = $footer['conn30_num'] + $v['conn30_num'];
                $footer['lost10_num'] = $footer['lost10_num'] + $v['lost10_num'];
                $footer['lost20_num'] = $footer['lost20_num'] + $v['lost20_num'];
                $footer['lost25_num'] = $footer['lost25_num'] + $v['lost25_num'];
                $footer['lost30_num'] = $footer['lost30_num'] + $v['lost30_num'];
                $footer['lost35_num'] = $footer['lost35_num'] + $v['lost35_num'];
                $footer['lost40_num'] = $footer['lost40_num'] + $v['lost40_num'];
            }

            $footer['avg_lostsecs'] = $footer['lost_num'] > 0 ?
                round($footer['lost_secs'] / $footer['lost_num']) : $footer['lost_secs'];
            $footer['outconn_rate'] = $footer['out_calls'] > 0 ?
                round($footer['out_num'] / $footer['out_calls'] * 100, 2).'%' : '0%';
            $footer['avg_outboundsecs'] = $footer['out_num'] > 0 ?
                round($footer['out_secs'] / $footer['out_num']) : $footer['out_num'];
            $footer['conn_rate'] = $footer['queue_in_num'] > 0 ?
                round($footer['conn_num'] / $footer['queue_in_num'] * 100, 2)."%" : "0%";
            $footer['avg_queuesecs'] = $footer['in_num'] > 0 ?
                round($footer['queue_secs'] / $footer['in_num']) : $footer['queue_secs'];
            $footer['avg_ringsecs'] = $footer['ring_num'] > 0 ?
                round($footer['ring_secs'] / $footer['ring_num']) : $footer['ring_secs'];
            $footer['avg_connsecs'] = $footer['conn_num'] > 0 ?
                round($footer['conn_secs'] / $footer['conn_num']) : $footer['conn_secs'];
            $footer['avg_waitsecs'] = $footer['conn_num'] > 0 ?
                round($footer['wait_secs'] / $footer['conn_num']) : $footer['wait_secs'];
            $footer['avg_dealsecs'] = $footer['avg_connsecs'] + $footer['avg_waitsecs'];
            $footer['conn5_rate'] = $footer['conn_num'] > 0 ?
                round($footer['conn5_num'] / $footer['conn_num'] * 100, 2)."%" : "0%";
            $footer['conn10_rate'] = $footer['conn_num'] > 0 ?
                round($footer['conn10_num'] / $footer['conn_num'] * 100, 2)."%" : "0%";
            $footer['conn15_rate'] = $footer['conn_num'] > 0 ?
                round($footer['conn15_num'] / $footer['conn_num'] * 100, 2)."%" : "0%";
            $footer['conn20_rate'] = $footer['conn_num'] > 0 ?
                round($footer['conn20_num'] / $footer['conn_num'] * 100, 2)."%" : "0%";
            $footer['conn30_rate'] = $footer['conn_num'] > 0 ?
                round($footer['conn30_num'] / $footer['conn_num'] * 100, 2)."%" : "0%";
            $footer['lost10_rate'] = $footer['lost_num'] > 0 ?
                round($footer['lost10_num'] / $footer['lost_num'] * 100, 2)."%" : "0%";
            $footer['lost20_rate'] = $footer['lost_num'] > 0 ?
                round($footer['lost20_num'] / $footer['lost_num'] * 100, 2)."%" : "0%";
            $footer['lost25_rate'] = $footer['lost_num'] > 0 ?
                round($footer['lost25_num'] / $footer['lost_num'] * 100, 2)."%" : "0%";
            $footer['lost30_rate'] = $footer['lost_num'] > 0 ?
                round($footer['lost30_num'] / $footer['lost_num'] * 100, 2)."%" : "0%";
            $footer['lost35_rate'] = $footer['lost_num'] > 0 ?
                round($footer['lost35_num'] / $footer['lost_num'] * 100, 2)."%" : "0%";
            $footer['lost40_rate'] = $footer['lost_num'] > 0 ?
                round($footer['lost40_num'] / $footer['lost_num'] * 100, 2)."%" : "0%";
            $footer['avg_lost1_secs'] = $footer['lost1_num'] > 0 ?
                round($footer['lost1_secs'] / $footer['lost1_num']) : $footer['lost1_secs'];
            $footer['avg_lost3_secs'] = $footer['lost3_num'] > 0 ?
                round($footer['lost3_secs'] / $footer['lost3_num']) : $footer['lost3_secs'];
            $footer['avg_lost4_secs'] = $footer['lost4_num'] > 0 ?
                round($footer['lost4_secs'] / $footer['lost4_num']) : $footer['lost4_secs'];
            $footer['avg_lost5_secs'] = $footer['lost5_num'] > 0 ?
                round($footer['lost5_secs'] / $footer['lost5_num']) : $footer['lost5_secs'];
            $result = array(
                'total' => $result['total'],
                'page' => $result['page'],
                'records' => $result['records'],
                'rows' => $gridData,
                'userdata' => $footer,
            );

        } else {
            $result = array(
                'total' => 0,
                'page' => 1,
                'records' => 0,
                'rows' => $gridData,
                'code' => $result['code'],
                'message' => $result['message'],
            );
        }

        /** 导出功能 */
        if (!empty($export)) {
            $gridData[] = $footer;
            $type = $request->get('type', 'queue');
            $rst = $this->get('icsoc_report.model.report')->getTitleReport(array('report_name' => $type));
            $title = array();
            if (isset($rst['code']) && $rst['code'] == 200) {
                $title = $rst['data'];
            }
            $titleItem = array();
            foreach ($title as $key => $v) {
                $titleItem[$v['field']] = $v['text'];
            }

            switch ($export) {
                case 'csv':
                    return $this->exportCsv($gridData, json_encode($titleItem), array(
                        'text' => $translator->trans('Queue Report'),
                        'href' => $this->generateUrl('icsoc_report_queue_home'),
                    ));
                case 'excel':
                    return $this->exportExcel($gridData, json_encode($titleItem), array(
                        'text' => $translator->trans('Queue Report'),
                        'href' => $this->generateUrl('icsoc_report_queue_home'),
                    ));
                default:
                    exit;
            }
        }

        return new JsonResponse($result);
    }
}
