<?php

namespace Icsoc\ReportBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Icsoc\ExportLib\Col;

/**
 * 明细报表
 * Class DetailController
 * @package Icsoc\ReportBundle\Controller
 */
class DetailController extends ReportController
{
    /** 根节点 */
    const NODE_TYPE_ROOT = 1;
    /** 放音节点 */
    const NODE_TYPE_SOUND = 2;
    /** 按键导航节点 */
    const NODE_TYPE_NAVIGATION = 3;
    const NODE_TYPE_NAVIGATION_KEY = 30;
    /** 转技能组节点 */
    const NODE_TYPE_QUEUE = 5;
    const NODE_TYPE_QUEUE_CHILDREN = 50;
    /** 留言节点 */
    const NODE_TYPE_MESSAGE = 6;
    /** 挂机节点 */
    const NODE_TYPE_HANGUP = 7;
    /** 收键节点 */
    const NODE_TYPE_KEYS = 8;
    const NODE_TYPE_KEYS_CHILDREN = 80;
    /** http接口交互节点 */
    const NODE_TYPE_HTTP = 9;
    const NODE_TYPE_HTTP_CHILDREN = 90;
    /** 来电记忆节点 */
    const NODE_TYPE_CALL_MEMORY = 10;
    const NODE_TYPE_CALL_MEMORY_CHILDREN = 100;
    /** 转电话节点 */
    const NODE_TYPE_PHONE = 11;
    /** 转坐席节点 */
    const NODE_TYPE_AGENT = 12;
    /** 判断节点 */
    const NODE_TYPE_JUDGE = 13;
    const NODE_TYPE_JUDGE_CHILDREN = 130;


    /** @var array 所有的节点类型 */
    private $nodes = array(
        self::NODE_TYPE_ROOT => '根节点',
        self::NODE_TYPE_SOUND => '播放语音',
        self::NODE_TYPE_NAVIGATION => '按键导航',
        self::NODE_TYPE_NAVIGATION_KEY => '按键导航',
        self::NODE_TYPE_QUEUE => '转技能组',
        self::NODE_TYPE_QUEUE_CHILDREN => '转技能组',
        self::NODE_TYPE_MESSAGE => '留言',
        self::NODE_TYPE_HANGUP => '挂机',
        self::NODE_TYPE_KEYS => '收号',
        self::NODE_TYPE_KEYS_CHILDREN => '收号',
        self::NODE_TYPE_HTTP => 'HTTP接口',
        self::NODE_TYPE_HTTP_CHILDREN => 'HTTP接口',
        self::NODE_TYPE_CALL_MEMORY => '来电记忆',
        self::NODE_TYPE_CALL_MEMORY_CHILDREN => '来电记忆',
        self::NODE_TYPE_PHONE => '转电话',
        self::NODE_TYPE_AGENT => '转坐席',
        self::NODE_TYPE_JUDGE => '判断',
        self::NODE_TYPE_JUDGE_CHILDREN => '判断',
    );

    /**
     * 呼入明细
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callinAction()
    {
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        /** @var  $queues (全部技能组数据) */
        $queues = $this->get('icsoc_data.model.queue')->getQueueNameKeyedByIdArray($this->getUser()->getVccId(), true);
        $options = array(
            'ag_num'=>array('text'=>'Agent', 'type'=>2),
            'callin_result'=>array(
                'text'=>'Call In Result',
                'type'=>2,
                'options'=>$this->container->getParameter('INRESULT'),
            ),
            'caller'=>array('text'=>'Cus Phone', 'type'=>1),
            'called'=>array('text'=>'Agent Phone', 'type'=>1),
            'que_id'=>array('text'=>'Queues', 'type'=>2, 'options'=>$queues),
            'server_num' => array('text' => 'Server Num', 'type' => 1),
        );
        if ($isEnableGroup) {
            $options['group_id'] = array('text'=>'Business Group', 'type'=>2, 'options'=>$groups);
        }

        return $this->render('IcsocReportBundle:Detail:callinDetailList.html.twig', array(
            'options'=>$options,
            'date' => $this->getData(),
            'isEnableGroup'=>$isEnableGroup,
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * 呼入明细数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function callinListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));
        $forPageParam = $request->get('forPage', '');

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');
        $big = $request->get('big', false);
        $sum = $request->get('sum', false);

        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );

        /*$list = $this->get('icsoc_data.model.report')->getCallinListData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
            )
        );*/
        $list = $this->get('icsoc_data.model.report')->getCallinListDataFromElasticsearch(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'forPageParam' => json_decode($forPageParam, true),
                'big' => $big
            )
        );
        if ($sum == true) {
            return new JsonResponse(array('code' => 200, 'count' => $list['total']));
        }
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $gridData = array();
        if ($big == false && isset($list['code']) && $list['code'] == 200) {
            if (empty($info['export'])) {
                $inresult = $this->container->getParameter('RINRESULT');
            } else {
                $inresult = $this->container->getParameter('INRESULT');
            }
            foreach ($list['data'] as $k => $v) {
                $gridData[$k] = $v;
                //$gridData[$k]['ag_num'] = $v['ag_num']." ".$v['ag_name'];
                $gridData[$k]['result'] = isset($inresult[$v['result']]) ? $inresult[$v['result']] : '';
                $gridData[$k]['group_id'] = isset($groups[$v['group_id']]) ? $groups[$v['group_id']] : '';
            }
        }

        /** 导出功能 */
        if (!empty($export) && $list['code'] == 200) {
            /** @var  $title (导出字段) */
            $title = array(
                'server_num' => array(
                    'title' => 'Server Num',
                    'type' => Col::TYPE_STRING,
                ),
                'caller' => array(
                    'title' => 'Callerin Caller',
                    'type' => Col::TYPE_STRING,
                ),
                'caller_areacode' => array(
                    'title' => 'Customer Areacode',
                    'type' => Col::TYPE_STRING,
                ),
                'caller_areaname' => array(
                    'title' => 'Customer Areaname',
                    'type' => Col::TYPE_STRING,
                ),
                'called' => array(
                    'title' => 'Callerin Called',
                    'type' => Col::TYPE_STRING,
                ),
                'called_areacode' => array(
                    'title' => 'AgPhone Areacode',
                    'type' => Col::TYPE_STRING,
                ),
                'called_areaname' => array(
                    'title' => 'AgPhone Areaname',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_num' => array(
                    'title' => 'Agent Num',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_name' => array(
                    'title' => 'Agent Name',
                    'type' => Col::TYPE_STRING,
                ),
                'que_name' => array(
                    'title' => 'Queue',
                    'type' => Col::TYPE_STRING,
                ),
                'start_time' => array(
                    'title' => 'Start Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'quein_time' => array(
                    'title' => 'Quein Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'conn_time' => array(
                    'title' => 'Conn Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'end_time' => array(
                    'title' => 'End time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'ivr_secs' => array(
                    'title' => 'Ivr Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'wait_secs' => array(
                    'title' => 'Wait Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'conn_secs' => array(
                    'title' => 'Detail Conn Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'all_secs' => array(
                    'title' => 'All Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'result' => array(
                    'title' => 'Result',
                    'type' => Col::TYPE_STRING,
                ),
            );
            if ($isEnableGroup) {
                $title = array(
                    'server_num' => array(
                        'title' => 'Server Num',
                        'type' => Col::TYPE_STRING,
                    ),
                    'caller' => array(
                        'title' => 'Callerin Caller',
                        'type' => Col::TYPE_STRING,
                    ),
                    'caller_areacode' => array(
                        'title' => 'Customer Areacode',
                        'type' => Col::TYPE_STRING,
                    ),
                    'caller_areaname' => array(
                        'title' => 'Customer Areaname',
                        'type' => Col::TYPE_STRING,
                    ),
                    'called' => array(
                        'title' => 'Callerin Called',
                        'type' => Col::TYPE_STRING,
                    ),
                    'called_areacode' => array(
                        'title' => 'AgPhone Areacode',
                        'type' => Col::TYPE_STRING,
                    ),
                    'called_areaname' => array(
                        'title' => 'AgPhone Areaname',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_num' => array(
                        'title' => 'Agent Num',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_name' => array(
                        'title' => 'Agent Name',
                        'type' => Col::TYPE_STRING,
                    ),
                    'group_id' => array(
                        'title' => 'Business Group',
                        'type' => Col::TYPE_STRING
                    ),
                    'que_name' => array(
                        'title' => 'Queue',
                        'type' => Col::TYPE_STRING,
                    ),
                    'start_time' => array(
                        'title' => 'Start Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'quein_time' => array(
                        'title' => 'Quein Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'conn_time' => array(
                        'title' => 'Conn Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'end_time' => array(
                        'title' => 'End time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'ivr_secs' => array(
                        'title' => 'Ivr Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'wait_secs' => array(
                        'title' => 'Wait Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'conn_secs' => array(
                        'title' => 'Detail Conn Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'all_secs' => array(
                        'title' => 'All Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'result' => array(
                        'title' => 'Result',
                        'type' => Col::TYPE_STRING,
                    ),
                );
            }
            if ($big == false) {
                switch ($export) {
                    case 'csv':
                        return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                    case 'excel':
                        return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                    default:
                        exit;
                }
            } else if ($big == true) {
                $type = array(
                    'result' => 'enum',
                    'group_id' => 'enum',
                );
                /** @var  $title (导出字段) */
                $endResults['enum']['group_id'] = $groups;
                $endResults['enum']['result'] = $this->container->getParameter('INRESULT');
                /** @var  $title (导出字段) */
                $newTitle = $this->get('icsoc_data.model.export')->getNewTitle($title, $type, $endResults);
                //新版异步导出
                $newData = array(
                    'es_search'=>json_encode($list['data']),
                    'mongo_coll' => 'win_incdr',
                    'cols' =>json_encode($newTitle),
                );
                $re = $this->get('icsoc_data.model.export')->newRecord($export, $newData, '呼入明细');

                return new JsonResponse($re);
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : 1,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 转接分配明细
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function transCallListAction(Request $request)
    {
        $callId = $request->get('call_id', '');
        $callId = $this->container->get('icsoc_data.model.report')->processCallId($callId);
        $list = $this->get('icsoc_data.model.report')->getTranCallListData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'call_id'=>$callId,
            )
        );
        $data = array();
        if (isset($list['code']) && $list['code'] == 200) {
            $data = $list['data'];
        }
        $result = array(
            'total' => 0,
            'page' => 1,
            'records' => 0,
            'rows' => $data,
        );

        return new JsonResponse($result);
    }

    /**
     * 呼出明细
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function calloutAction()
    {
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        /** @var  $queues (全部技能组数据) */
        $queues = $this->get('icsoc_data.model.queue')->getQueueNameKeyedByIdArray($this->getUser()->getVccId(), true);
        $options = array(
            'ag_num'=>array('text'=>'Agent', 'type'=>2),
            'result'=>array(
                'text'=>'Call In Result', 'type'=>2,
                'options'=>$this->container->getParameter('ENDRESULT'),
            ),
            'cus_phone'=>array('text'=>'Cus Phone', 'type'=>1),
            'ag_phone'=>array('text'=>'Agent Phone', 'type'=>1),
            'que_id'=>array('text'=>'Queues', 'type'=>2, 'options'=>$queues),
            'serv_num' => array('text' => 'Server Num', 'type' => 1),
        );
        if ($isEnableGroup) {
            $options['group_id'] = array('text'=>'Business Group', 'type'=>2, 'options'=>$groups);
        }
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();

        return $this->render('IcsocReportBundle:Detail:calloutDetailList.html.twig', array(
            'options'=>$options,
            'date' => $this->getData(),
            'isEnableGroup' => $isEnableGroup,
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * 呼出明细列表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function calloutListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));
        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');
        $big = $request->get('big', false);         //判断用那种方式导出
        $sum = $request->get('sum', false);         //判断只查询数量的情况

        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );

        /*$list = $this->get('icsoc_data.model.report')->getCalloutListData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
                'forPageParam' => json_decode($forPageParam, true),
            )
        );*/

        $list = $this->get('icsoc_data.model.report')->getCalloutListDataFromElasticsearch(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big'=>$big
            )
        );
        if ($sum == true) {
            return new JsonResponse(array('code' => 200, 'count' => $list['total']));
        }
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $gridData = array();

        if ($big == false && isset($list['code']) && $list['code'] == 200) {
            if (empty($info['export'])) {
                $endResult = $this->container->getParameter('RENDRESULT');
            } else {
                $endResult = $this->container->getParameter('ENDRESULT');
            }
            foreach ($list['data'] as $k => $v) {
                $gridData[$k] = $v;
                //$gridData[$k]['ag_num'] = $v['ag_num']." ".$v['ag_name'];
                $gridData[$k]['result'] = isset($endResult[$v['result']]) ? $endResult[$v['result']] : '';
                $gridData[$k]['group_id'] = isset($groups[$v['group_id']]) ? $groups[$v['group_id']] : '';
            }
        }

        /** 导出功能 */
        if (!empty($export) && $list['code'] == 200) {
            /** @var  $title (导出字段) */
            $title = array(
                'ag_num' => array(
                    'title' => 'Agent Num',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_name' => array(
                    'title' => 'Agent Name',
                    'type' => Col::TYPE_STRING,
                ),
                'serv_num' => array(
                    'title' => 'Server Num',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_phone' => array(
                    'title' => 'Agent Phone',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_phone_areacode' => array(
                    'title' => 'AgPhone Areacode',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_phone_areaname' => array(
                    'title' => 'AgPhone Areaname',
                    'type' => Col::TYPE_STRING,
                ),
                'cus_phone' => array(
                    'title' => 'Cus Phone',
                    'type' => Col::TYPE_STRING,
                ),
                'cus_phone_areacode' => array(
                    'title' => 'Customer Areacode',
                    'type' => Col::TYPE_STRING,
                ),
                'cus_phone_areaname' => array(
                    'title' => 'Customer Areaname',
                    'type' => Col::TYPE_STRING,
                ),
                'que_name' => array(
                    'title' => 'Queue',
                    'type' => Col::TYPE_STRING,
                ),
                'start_time' => array(
                    'title' => 'Start Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'end_time' => array(
                    'title' => 'End time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'ring_secs' => array(
                    'title' => 'Ring Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'conn_secs' => array(
                    'title' => 'Detail Conn Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'all_secs' => array(
                    'title' => 'All Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'result' => array(
                    'title' => 'Result',
                    'type' => Col::TYPE_STRING,
                ),
            );
            if ($isEnableGroup) {
                $title = array(
                    'ag_num' => array(
                        'title' => 'Agent Num',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_name' => array(
                        'title' => 'Agent Name',
                        'type' => Col::TYPE_STRING,
                    ),
                    'serv_num' => array(
                        'title' => 'Server Num',
                        'type' => Col::TYPE_STRING,
                    ),
                    'group_id' => array(
                        'title' => 'Business Group',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_phone' => array(
                        'title' => 'Agent Phone',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_phone_areacode' => array(
                        'title' => 'AgPhone Areacode',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_phone_areaname' => array(
                        'title' => 'AgPhone Areaname',
                        'type' => Col::TYPE_STRING,
                    ),
                    'cus_phone' => array(
                        'title' => 'Cus Phone',
                        'type' => Col::TYPE_STRING,
                    ),
                    'cus_phone_areacode' => array(
                        'title' => 'Customer Areacode',
                        'type' => Col::TYPE_STRING,
                    ),
                    'cus_phone_areaname' => array(
                        'title' => 'Customer Areaname',
                        'type' => Col::TYPE_STRING,
                    ),
                    'que_name' => array(
                        'title' => 'Queue',
                        'type' => Col::TYPE_STRING,
                    ),
                    'start_time' => array(
                        'title' => 'Start Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'end_time' => array(
                        'title' => 'End time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'ring_secs' => array(
                        'title' => 'Ring Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'conn_secs' => array(
                        'title' => 'Detail Conn Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'all_secs' => array(
                        'title' => 'All Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'result' => array(
                        'title' => 'Result',
                        'type' => Col::TYPE_STRING,
                    ),
                );
            }

            if ($big == false) {
                switch ($export) {
                    case 'csv':
                        return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                    case 'excel':
                        return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                    default:
                        exit;
                }
            } else if ($big == true) {
                $type = array(
                    'group_id' => 'enum',
                    'result' => 'enum',
                );
                $endResults['enum']['group_id'] = $groups;
                $endResults['enum']['result'] = $this->container->getParameter('ENDRESULT');
                /** @var  $title (导出字段) */
                $newTitle = $this->get('icsoc_data.model.export')->getNewTitle($title, $type, $endResults);
                //新版异步导出
                $newData = array(
                    'es_search'=>json_encode($list['data']),
                    'mongo_coll' => 'win_agcdr',
                    'cols' =>json_encode($newTitle),
                );
                $re = $this->get('icsoc_data.model.export')->newRecord($export, $newData, '呼出明细');

                return new JsonResponse($re);
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : $page,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 获取明细报表坐席搜索项的搜索内容
     *
     * @param Request $request
     * @return Response
     */
    public function getAgentsForChosenSelectAction(Request $request)
    {
        $vccId = $this->getVccId();
        $agentChosenSelect = $request->get('agentChosenSelect', '');
        $agents = '';
        if (!empty($agentChosenSelect)) {
            $agents = $this->get('icsoc_data.model.report')->getAgentsForChosenSelect($vccId);
        }

        return new Response($agents);
    }

    /**
     * 坐席通话明细
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction()
    {
        /** @var  $queues (全部技能组数据) */
        $queues = $this->get('icsoc_data.model.queue')->getQueueNameKeyedByIdArray($this->getUser()->getVccId(), true);
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $options = array(
            'ag_num'=>array('text'=>'Agent', 'type'=>2),
            'result'=>array(
                'text'=>'Call In Result',
                'type'=>2,
                'options'=>$this->container->getParameter('ENDRESULT'),
            ),
            'cus_phone'=>array('text'=>'Cus Phone', 'type'=>1),
            'ag_phone'=>array('text'=>'Agent Phone', 'type'=>1),
            'call_type'=>array('text'=>'Call Type', 'type'=>2, 'options'=>$this->container->getParameter('CALLTYPE')),
            'endresult'=>array('text'=>'End Reason', 'type'=>2, 'options'=>$this->container->getParameter('ENDREASON')),
            'que_id'=>array('text'=>'Queues', 'type'=>2, 'options'=>$queues),
            'call_id'=>array('text'=>'Call ID', 'type'=>1),
        );
        if ($isEnableGroup) {
            $options['group_id'] = array('text'=>'Business Group', 'type'=>2, 'options'=>$groups);
        }
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();

        return $this->render('IcsocReportBundle:Detail:callDetailList.html.twig', array(
            'options' => $options,
            'date' => $this->getData(),
            'isEnableGroup' => $isEnableGroup,
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * 坐席通话明细列表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function callListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');
        $big = $request->get('big', false);
        $sum = $request->get('sum', false);
        $forPageParam = $request->get('forPage', '');

        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );

        /*$list = $this->get('icsoc_data.model.report')->getCallListData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
                'forPageParam' => json_decode($forPageParam, true),
            )
        );*/

        $list = $this->get('icsoc_data.model.report')->getCallListDataFromElasticsearch(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
                'forPageParam' => json_decode($forPageParam, true),
            )
        );
        if ($sum == true) {
            return new JsonResponse(array('code' => 200, 'count' => $list['total']));
        }
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $gridData = array();
        if ($big == false && isset($list['code']) && $list['code'] == 200) {
            /** @var array $endResult 呼叫结果 */
            if (empty($export)) {
                $endResult = $this->container->getParameter('RENDRESULT');
            } else {
                $endResult = $this->container->getParameter('ENDRESULT');
            }
            foreach ($list['data'] as $k => $v) {
                $gridData[$k] = $v;
                //$gridData[$k]['ag_num'] = $v['ag_num']." ".$v['ag_name'];
                $gridData[$k]['result'] = isset($endResult[$v['result']]) ? $endResult[$v['result']] : '';
                $gridData[$k]['group_id'] = isset($groups[$v['group_id']]) ? $groups[$v['group_id']] : '';
            }
        }

        /** 导出功能 */
        if (!empty($export) && $list['code'] == 200) {
            /** @var  $title (导出字段) */
            $title = array(
                'ag_num' => array(
                    'title' => 'Agent Num',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_name' => array(
                    'title' => 'Agent Name',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_phone' => array(
                    'title' => 'Agent Phone',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_phone_areacode' => array(
                    'title' => 'AgPhone Areacode',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_phone_areaname' => array(
                    'title' => 'AgPhone Areaname',
                    'type' => Col::TYPE_STRING,
                ),
                'cus_phone' => array(
                    'title' => 'Cus Phone',
                    'type' => Col::TYPE_STRING,
                ),
                'cus_phone_areacode' => array(
                    'title' => 'Customer Areacode',
                    'type' => Col::TYPE_STRING,
                ),
                'cus_phone_areaname' => array(
                    'title' => 'Customer Areaname',
                    'type' => Col::TYPE_STRING,
                ),
                'que_name' => array(
                    'title' => 'Queue',
                    'type' => Col::TYPE_STRING,
                ),
                'call_type' => array(
                    'title' => 'Call Type',
                    'type' => Col::TYPE_STRING
                ),
                'call_id' => array(
                    'title' => 'Call ID',
                    'type' => Col::TYPE_STRING
                ),
                'start_time' => array(
                    'title' => 'Start Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'end_time' => array(
                    'title' => 'End time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'conn_secs' => array(
                    'title' => 'Detail Conn Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'result' => array(
                    'title' => 'Result',
                    'type' => Col::TYPE_STRING,
                ),
                'endresult' => array(
                    'title' => 'End Reason',
                    'type' => Col::TYPE_STRING,
                ),
            );
            if ($isEnableGroup) {
                $title = array(
                    'ag_num' => array(
                        'title' => 'Agent Num',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_name' => array(
                        'title' => 'Agent Name',
                        'type' => Col::TYPE_STRING,
                    ),
                    'group_id' => array(
                        'title' => 'Business Group',
                        'type' => Col::TYPE_STRING
                    ),
                    'ag_phone' => array(
                        'title' => 'Agent Phone',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_phone_areacode' => array(
                        'title' => 'AgPhone Areacode',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_phone_areaname' => array(
                        'title' => 'AgPhone Areaname',
                        'type' => Col::TYPE_STRING,
                    ),
                    'cus_phone' => array(
                        'title' => 'Cus Phone',
                        'type' => Col::TYPE_STRING,
                    ),
                    'cus_phone_areacode' => array(
                        'title' => 'Customer Areacode',
                        'type' => Col::TYPE_STRING,
                    ),
                    'cus_phone_areaname' => array(
                        'title' => 'Customer Areaname',
                        'type' => Col::TYPE_STRING,
                    ),
                    'que_name' => array(
                        'title' => 'Queue',
                        'type' => Col::TYPE_STRING,
                    ),
                    'call_type' => array(
                        'title' => 'Call Type',
                        'type' => Col::TYPE_STRING
                    ),
                    'call_id' => array(
                        'title' => 'Call ID',
                        'type' => Col::TYPE_STRING
                    ),
                    'start_time' => array(
                        'title' => 'Start Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'end_time' => array(
                        'title' => 'End time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'conn_secs' => array(
                        'title' => 'Detail Conn Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'result' => array(
                        'title' => 'Result',
                        'type' => Col::TYPE_STRING,
                    ),
                    'endresult' => array(
                        'title' => 'End Reason',
                        'type' => Col::TYPE_STRING,
                    ),
                );
            }
            if ($big == false) {
                switch ($export) {
                    case 'csv':
                        return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                    case 'excel':
                        return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                    default:
                        exit;
                }
            } else if ($big == true) {
                $type = array(
                    'call_type' => 'enum',
                    'result' => 'enum',
                    'endresult' => array('enum' => array('unset')),
                    'group_id' => 'enum',
                );
                $endResults['enum']['group_id'] = $groups;
                $endResults['enum']['result'] = $this->container->getParameter('ENDRESULT');
                /** @var array $callType 呼叫类型 */
                $endResults['enum']['call_type'] = $this->container->getParameter('CALLTYPE');
                /** @var array $endReason 结束类型 */
                $endResults['enum']['endresult'] = $this->container->getParameter('ENDREASON');
                $endResults['enum']['endresult']['unset'] = '其他';
                /** @var  $title (导出字段) */
                $newTitle = $this->get('icsoc_data.model.export')->getNewTitle($title, $type, $endResults);
                //新版异步导出
                $newData = array(
                    'es_search'=>json_encode($list['data']),
                    'mongo_coll' => 'win_agcdr',
                    'cols' =>json_encode($newTitle),
                );
                $re = $this->get('icsoc_data.model.export')->newRecord($export, $newData, '坐席通话');

                return new JsonResponse($re);
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : $page,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 坐席操作明细
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function agentStateAction()
    {
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $options = array(
            'ag_num'=>array('text'=>'Agent', 'type'=>1),
            'ag_sta_type'=>array(
                'text'=>'Operation',
                'type'=>2,
                'options'=>$this->container->getParameter('OPERATIONSTATUS'),
            ),
        );
        if ($isEnableGroup) {
            $options['group_id'] = array('text'=>'Business Group', 'type'=>2, 'options'=>$groups);
        }
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();

        return $this->render('IcsocReportBundle:Detail:agentStateDetailList.html.twig', array(
            'options' => $options,
            'date' => $this->getData(),
            'isEnableGroup' => $isEnableGroup,
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * 坐席操作明细列表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function agentStateListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');
        $big = $request->get('big', false);
        $sum = $request->get('sum', false);

        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );

        $list = $this->get('icsoc_data.model.report')->getAgentStateListData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
            )
        );
        if ($sum == true) {
            return new JsonResponse(array('code' => 200, 'count' => $list['total']));
        }
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $gridData = array();
        if ($big == false && isset($list['code']) && $list['code'] == 200) {
            foreach ($list['data'] as $k => $v) {
                $gridData[$k] = $v;
                $gridData[$k]['group_id'] = isset($groups[$v['group_id']]) ? $groups[$v['group_id']] : '';
            }
        }

        /** 导出功能 */
        if (!empty($export) && $list['code'] == 200) {
            /** @var  $title (导出字段) */
            $title = array(
                'ag_num' => array(
                    'title' => 'Agent Num',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_name' => array(
                    'title' => 'Agent Name',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_sta_type' => array(
                    'title' => 'Operation',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_sta_reason' => array(
                    'title' => 'Operation Reason',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_login_ip' => array(
                    'title' => 'Login IP',
                    'type' => Col::TYPE_STRING,
                ),
                'start_time' => array(
                    'title' => 'Start Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'end_time' => array(
                    'title' => 'End time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'duration' => array(
                    'title' => 'Duration',
                    'type' => Col::TYPE_STRING,
                ),
                'bend' => array(
                    'title' => 'Bend',
                    'type' => Col::TYPE_STRING,
                ),
            );
            if ($isEnableGroup) {
                $title = array(
                    'ag_num' => array(
                        'title' => 'Agent Num',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_name' => array(
                        'title' => 'Agent Name',
                        'type' => Col::TYPE_STRING,
                    ),
                    'group_id' => array(
                        'title' => 'Business Group',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_sta_type' => array(
                        'title' => 'Operation',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_sta_reason' => array(
                        'title' => 'Operation Reason',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_login_ip' => array(
                        'title' => 'Login IP',
                        'type' => Col::TYPE_STRING,
                    ),
                    'start_time' => array(
                        'title' => 'Start Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'end_time' => array(
                        'title' => 'End time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'duration' => array(
                        'title' => 'Duration',
                        'type' => Col::TYPE_STRING,
                    ),
                    'bend' => array(
                        'title' => 'Bend',
                        'type' => Col::TYPE_STRING,
                    ),
                );
            }
            if ($big == false) {
                switch ($export) {
                    case 'csv':
                        return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                    case 'excel':
                        return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                    default:
                        exit;
                }
            } else if ($big == true) {
                $type = array(
                    'start_time' => 'datetime',
                    'end_time' => 'datetime',
                    'duration' => 'second',
                    'ag_sta_type' => 'enum',
                    'ag_sta_reason' => 'enum',
                    'bend' => 'enum',
                    'group_id' => 'enum',
                );
                /**获取自定义的操作**/
                $getReason = $this->get('icsoc_data.model.report')->getAgentStaReason($vccId);
                $endResults['enum']['ag_sta_reason'] = $getReason;
                $endResults['enum']['ag_sta_reason'][1] = empty($getReason[1]) ? '系统示忙' : $getReason[1];
                $endResults['enum']['ag_sta_reason'][11] = empty($getReason[11]) ? '坐席主动示忙' : $getReason[11];
                $endResults['enum']['ag_sta_reason'][0] =  '坐席主动示忙';
                $endResults['enum']['ag_sta_reason'][-1] =  '坐席主动示忙';

                /** @var array $operationStaus 操作状态 */
                $endResults['enum']['ag_sta_type'] = $this->container->getParameter('OPERATIONSTATUS');
                $endResults['enum']['bend']['1'] = '结束';
                $endResults['enum']['group_id'] = $groups;
                /** @var  $title (导出字段) */
                $newTitle = $this->get('icsoc_data.model.export')->getOldNewTitle($title, $export, $type, $endResults);
                $newData = array(
                    'vcc_id' => $vccId,
                    'sql' => $list['sql'],
                    'datacount' => $list['total'],
                    'cols' => json_encode($newTitle),
                );
                $re = $this->get('icsoc_data.model.export')->record($export, $newData, '坐席操作');

                return new JsonResponse($re);
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : 1,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 漏话明细
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function lostAction()
    {
        $lostReasons = $this->container->getParameter('INRESULT');
        unset($lostReasons[0]);
        $options = array(
            'caller'=>array('text'=>'Caller', 'type'=>1),
            'server_num'=>array('text'=>'Server Num', 'type'=>1),
            'reason' => array('text' => 'Lost Reason','type' =>2, 'options' => $lostReasons),
        );
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();

        return $this->render('IcsocReportBundle:Detail:lostDetailList.html.twig', array(
            'date' => $this->getData(),
            'options' => $options,
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * 漏话明细列表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function lostListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');
        $big = $request->get('big', false);
        $sum = $request->get('sum', false);

        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );

        /*$list = $this->get('icsoc_data.model.report')->getLostListData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
            )
        );*/
        $list = $this->get('icsoc_data.model.report')->getLostListDataFromElasticsearch(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
            )
        );
        if ($sum == true) {
            return new JsonResponse(array('code' => 200, 'count' => $list['total']));
        }
        $gridData = array();
        if ($big == false && isset($list['code']) && $list['code'] == 200) {
            $gridData = $list['data'];
        }

        /** 导出功能 */
        if (!empty($export) && $list['code'] == 200) {
            /** @var  $title (导出字段) */
            $title = array(
                'caller' => array(
                    'title' => 'Caller',
                    'type' => Col::TYPE_STRING,
                ),
                'caller_areacode' => array(
                    'title' => 'Caller Areacode',
                    'type' => Col::TYPE_STRING,
                ),
                'caller_areaname' => array(
                    'title' => 'Caller Areaname',
                    'type' => Col::TYPE_STRING,
                ),
                'que_name' => array(
                    'title' => 'Queue',
                    'type' => Col::TYPE_STRING,
                ),
                'start_time' => array(
                    'title' => 'Call Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'server_num' => array(
                    'title' => 'Server Num',
                    'type' => Col::TYPE_STRING,
                ),
                'result' => array(
                    'title' => 'Lost Reason',
                    'type' => Col::TYPE_STRING,
                ),
            );
            if ($big == false) {
                switch ($export) {
                    case 'csv':
                        return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                    case 'excel':
                        return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                    default:
                        exit;
                }
            } else if ($big == true) {
                $type = array(
                    'start_time' => 'datetime',
                    'result' => 'enum',
                );
                $endResults['enum']['result'] = $this->container->getParameter('INRESULT');
                /** @var  $title (导出字段) */
                $newTitle = $this->get('icsoc_data.model.export')->getNewTitle($title, $type, $endResults);
                //新版异步导出
                $newData = array(
                    'es_search'=>json_encode($list['data']),
                    'mongo_coll' => 'win_incdr',
                    'cols' =>json_encode($newTitle),
                );
                $re = $this->get('icsoc_data.model.export')->newRecord($export, $newData, '漏话明细');

                return new JsonResponse($re);
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : 1,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 咨询三方通话详单
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function conferenceAction()
    {
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $bendResult = $this->container->getParameter('BENDRESULT');
        unset($bendResult[2]);
        $options = array(
            'ag_ernum'=>array('text'=>'Agent Ernum', 'type'=>1),
            'endresult'=>array('text'=>'End Reason', 'type'=>2, 'options'=>$bendResult),
            'call_phone'=>array('text'=>'Initiator Phone', 'type'=>1),
            'ext_phone'=>array('text'=>'Conference phone', 'type'=>1),
            'max_secs'=>array('text'=>'All Secs larger than', 'type'=>1),
            'min_secs'=>array('text'=>'All Secs less than', 'type'=>1),
        );
        if ($isEnableGroup) {
            $options['group_id'] = array('text'=>'Business Group', 'type'=>2, 'options'=>$groups);
        }
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();

        return $this->render('IcsocReportBundle:Detail:conferenceDetailList.html.twig', array(
            'options' => $options,
            'date' => $this->getData(),
            'isEnableGroup' => $isEnableGroup,
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * 咨询三方通话详单列表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function conferenceListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');
        $big = $request->get('big', false);
        $sum = $request->get('sum', false);
        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );

        $list = $this->get('icsoc_data.model.report')->getConferenceListData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
            )
        );
        if ($sum == true) {
            return new JsonResponse(array('code' => 200, 'count' => $list['total']));
        }
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $gridData = array();

        if ($big == false && isset($list['code']) && $list['code'] == 200) {
            if (empty($info['export'])) {
                $bendResult = $this->container->getParameter('BVENDRESULT');
            } else {
                $bendResult = $this->container->getParameter('BENDRESULT');
            }
            foreach ($list['data'] as $k => $v) {
                $gridData[$k] = $v;
                //$gridData[$k]['ag_ernum'] = $v['ag_ernum']." ".$v['ag_ername'];
                //$gridData[$k]['ag_ednum'] = $v['ag_ednum']." ".$v['ag_edname'];
                $gridData[$k]['endresult'] = isset($bendResult[$v['endresult']]) ? $bendResult[$v['endresult']] : "";
                $gridData[$k]['group_id'] = isset($groups[$v['group_id']]) ? $groups[$v['group_id']] : '';
            }
        }

        /** 导出功能 */
        if (!empty($export) && $list['code'] == 200) {
            /** @var  $title (导出字段) */
            $title = array(
                'ag_ernum' => array(
                    'title' => 'Agent Ernum',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_ername' => array(
                    'title' => 'Agent Ername',
                    'type' => Col::TYPE_STRING,
                ),
                'call_phone' => array(
                    'title' => 'Initiator Phone',
                    'type' => Col::TYPE_STRING,
                ),
                'que_name' => array(
                    'title' => 'Queue',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_ednum' => array(
                    'title' => 'Agent Ednum',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_edname' => array(
                    'title' => 'Agent Edname',
                    'type' => Col::TYPE_STRING,
                ),
                'ext_phone' => array(
                    'title' => 'Conference phone',
                    'type' => Col::TYPE_STRING,
                ),
                'start_time' => array(
                    'title' => 'Conference Start Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'conn2_time' => array(
                    'title' => 'Three Start Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'conn_time' => array(
                    'title' => 'Consult Start Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'conn1_secs' => array(
                    'title' => 'Consult Secs',
                    'type' => Col::TYPE_STRING,
                ),
                'conn2_secs' => array(
                    'title' => 'Three Secs',
                    'type' => Col::TYPE_STRING,
                ),
                'conn_secs' => array(
                    'title' => 'All Secs',
                    'type' => Col::TYPE_STRING,
                ),
                'endresult' => array(
                    'title' => 'End Reason',
                    'type' => Col::TYPE_STRING,
                ),
            );
            if ($isEnableGroup) {
                $title = array(
                    'ag_ernum' => array(
                        'title' => 'Agent Ernum',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_ername' => array(
                        'title' => 'Agent Ername',
                        'type' => Col::TYPE_STRING,
                    ),
                    'group_id' => array(
                        'title' => 'Business Group',
                        'type' => Col::TYPE_STRING,
                    ),
                    'call_phone' => array(
                        'title' => 'Initiator Phone',
                        'type' => Col::TYPE_STRING,
                    ),
                    'que_name' => array(
                        'title' => 'Queue',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_ednum' => array(
                        'title' => 'Agent Ednum',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_edname' => array(
                        'title' => 'Agent Edname',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ext_phone' => array(
                        'title' => 'Conference phone',
                        'type' => Col::TYPE_STRING,
                    ),
                    'start_time' => array(
                        'title' => 'Conference Start Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'conn2_time' => array(
                        'title' => 'Three Start Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'conn_time' => array(
                        'title' => 'Consult Start Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'conn1_secs' => array(
                        'title' => 'Consult Secs',
                        'type' => Col::TYPE_STRING,
                    ),
                    'conn2_secs' => array(
                        'title' => 'Three Secs',
                        'type' => Col::TYPE_STRING,
                    ),
                    'conn_secs' => array(
                        'title' => 'All Secs',
                        'type' => Col::TYPE_STRING,
                    ),
                    'endresult' => array(
                        'title' => 'End Reason',
                        'type' => Col::TYPE_STRING,
                    ),
                );
            }
            if ($big == false) {
                switch ($export) {
                    case 'csv':
                        return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                    case 'excel':
                        return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                    default:
                        exit;
                }
            } else if ($big == true) {
                $type = array(
                    'start_time' => 'datetime',
                    'conn2_time' => 'datetime',
                    'conn_time' => 'datetime',
                    'endresult' => 'enum',
                    'group_id' => 'enum',
                );
                $endResults['enum']['endresult'] = $this->container->getParameter('BENDRESULT');
                $endResults['enum']['group_id'] = $groups;
                /** @var  $title (导出字段) */
                $newTitle = $this->get('icsoc_data.model.export')->getOldNewTitle($title, $export, $type, $endResults);
                $newData = array(
                    'vcc_id' => $vccId,
                    'sql' => $list['sql'],
                    'datacount' => $list['total'],
                    'cols' => json_encode($newTitle),
                );
                $re = $this->get('icsoc_data.model.export')->record($export, $newData, '咨询三方通话');

                return new JsonResponse($re);
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : 1,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 监听强插通话详单
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function monitorAction()
    {
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $bendResult = $this->container->getParameter('BENDRESULT');
        unset($bendResult[1]);
        $options = array(
            'ag_ernum'=>array('text'=>'Agent Ernum', 'type'=>1),
            'endresult'=>array('text'=>'End Reason', 'type'=>2, 'options'=>$bendResult),
            'ext_phone'=>array('text'=>'Initiator Phone', 'type'=>1),
            'call_phone'=>array('text'=>'Monitor phone', 'type'=>1),
            'max_secs'=>array('text'=>'All Secs larger than', 'type'=>1),
            'min_secs'=>array('text'=>'All Secs less than', 'type'=>1),
        );
        if ($isEnableGroup) {
            $options['group_id'] = array('text'=>'Business Group', 'type'=>2, 'options'=>$groups);
        }
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();

        return $this->render('IcsocReportBundle:Detail:monitorDetailList.html.twig', array(
            'options' => $options,
            'date' => $this->getData(),
            'isEnableGroup' => $isEnableGroup,
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * 监听强插通话详单列表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function monitorListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');
        $big = $request->get('big', false);
        $sum = $request->get('sum', false);
        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );

        $list = $this->get('icsoc_data.model.report')->getMonitorListData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
            )
        );
        if ($sum == true) {
            return new JsonResponse(array('code' => 200, 'count' => $list['total']));
        }

        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $gridData = array();
        if ($big == false && isset($list['code']) && $list['code'] == 200) {
            if (empty($info['export'])) {
                $bendResult = $this->container->getParameter('BVENDRESULT');
            } else {
                $bendResult = $this->container->getParameter('BENDRESULT');
            }
            foreach ($list['data'] as $k => $v) {
                $gridData[$k] = $v;
                //$gridData[$k]['ag_ernum'] = $v['ag_ernum']." ".$v['ag_ername'];
                // $gridData[$k]['ag_ednum'] = $v['ag_ednum']." ".$v['ag_edname'];
                $gridData[$k]['endresult'] = isset($bendResult[$v['endresult']]) ? $bendResult[$v['endresult']] : "";
                $gridData[$k]['group_id'] = isset($groups[$v['group_id']]) ? $groups[$v['group_id']] : '';
            }
        }
        /** 导出功能 */
        if (!empty($export) && $list['code'] == 200) {
            /** @var  $title (导出字段) */
            $title = array(
                'ag_ernum' => array(
                    'title' => 'Agent Ernum',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_ername' => array(
                    'title' => 'Agent Ername',
                    'type' => Col::TYPE_STRING,
                ),
                'ext_phone' => array(
                    'title' => 'Initiator Phone',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_ednum' => array(
                    'title' => 'Monitor Agent Ednum',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_edname' => array(
                    'title' => 'Monitor Agent Edname',
                    'type' => Col::TYPE_STRING,
                ),
                'call_phone' => array(
                    'title' => 'Monitor phone',
                    'type' => Col::TYPE_STRING,
                ),
                'start_time' => array(
                    'title' => 'Monitor Start Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'conn2_time' => array(
                    'title' => 'Override Start Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'conn1_secs' => array(
                    'title' => 'Monitor Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'conn2_secs' => array(
                    'title' => 'Override Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'conn_secs' => array(
                    'title' => 'All Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'endresult' => array(
                    'title' => 'End Reason',
                    'type' => Col::TYPE_STRING,
                ),
            );
            if ($isEnableGroup) {
                $title = array(
                    'ag_ernum' => array(
                        'title' => 'Agent Ernum',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_ername' => array(
                        'title' => 'Agent Ername',
                        'type' => Col::TYPE_STRING,
                    ),
                    'group_id' => array(
                        'title' => 'Business Group',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ext_phone' => array(
                        'title' => 'Initiator Phone',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_ednum' => array(
                        'title' => 'Monitor Agent Ednum',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_edname' => array(
                        'title' => 'Monitor Agent Edname',
                        'type' => Col::TYPE_STRING,
                    ),
                    'call_phone' => array(
                        'title' => 'Monitor phone',
                        'type' => Col::TYPE_STRING,
                    ),
                    'start_time' => array(
                        'title' => 'Monitor Start Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'conn2_time' => array(
                        'title' => 'Override Start Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'conn1_secs' => array(
                        'title' => 'Monitor Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'conn2_secs' => array(
                        'title' => 'Override Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'conn_secs' => array(
                        'title' => 'All Secs',
                        'type' => Col::TYPE_NUMBER,
                    ),
                    'endresult' => array(
                        'title' => 'End Reason',
                        'type' => Col::TYPE_STRING,
                    ),
                );
            }

            if ($big == false) {
                $translator = $this->get('translator');
                switch ($export) {
                    case 'csv':
                        return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                    case 'excel':
                        return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                    default:
                        exit;
                }
            } else if ($big == true) {
                $type = array(
                    'start_time' => 'datetime',
                    'conn2_time' => 'datetime',
                    'endresult' => 'enum',
                    'group_id' => 'enum',
                );
                $endResults['enum']['endresult'] = $this->container->getParameter('BENDRESULT');
                $endResults['enum']['group_id'] = $groups;
                /** @var  $title (导出字段) */
                $newTitle = $this->get('icsoc_data.model.export')->getOldNewTitle($title, $export, $type, $endResults);
                $newData = array(
                    'vcc_id' => $vccId,
                    'sql' => $list['sql'],
                    'datacount' => $list['total'],
                    'cols' => json_encode($newTitle),
                );
                $re = $this->get('icsoc_data.model.export')->record($export, $newData, '监听强插通话');

                return new JsonResponse($re);
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : 1,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 技能组呼叫转接明细
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function queueTransCallAction()
    {
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        /** @var  $queues (全部技能组数据) */
        $queues = $this->get('icsoc_data.model.queue')->getQueueNameKeyedByIdArray($this->getUser()->getVccId(), true);
        /** 获取所有的技能组用于转移技能组*/
        $conn = $this->container->get("doctrine.dbal.default_connection");
        $allQueues = $conn->fetchAll(
            "SELECT id,que_name,que_num FROM win_queue WHERE is_del=0 AND vcc_id=:vcc_id",
            array(':vcc_id' => $vccId)
        );
        $transQueue = array();
        foreach ($allQueues as $queue) {
            if (!empty($queue['que_num'])) {
                $queue['que_name'] .= '['.$queue['que_num'].']';
            }
            $transQueue[$queue['id']] = $queue['que_name'];
        }

        $options = array(
            'ag_num'=>array('text'=>'Agent', 'type'=>1),
            'ag_phone'=>array('text'=>'Agent Phone', 'type'=>1),
            'cus_phone'=>array('text'=>'Cus Phone', 'type'=>1),
            'que_id'=>array('text'=>'Queues', 'type'=>2, 'options'=>$queues),
            'que_id_trans'=>array('text'=>'Trans Queues', 'type'=>2, 'options'=>$transQueue),

        );
        if ($isEnableGroup) {
            $options['group_id'] = array('text'=>'Business Group', 'type'=>2, 'options'=>$groups);
        }

        return $this->render('IcsocReportBundle:Detail:queueTransCallDetailList.html.twig', array(
            'options' => $options,
            'date' => $this->getData(),
            'isEnableGroup' => $isEnableGroup,
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * 技能组转移明细数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function queueTransCallListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');
        $big = $request->get('big', false);
        $sum = $request->get('sum', false);
        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );

        $list = $this->get('icsoc_data.model.report')->getqueueTransCallListData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
            )
        );
        if ($sum == true) {
            return new JsonResponse(array('code' => 200, 'count' => $list['total']));
        }
        $em = $this->getDoctrine()->getManager();
        $vccId = $this->getVccId();
        $isEnableGroup = $this->getDoctrine()
            ->getRepository("IcsocSecurityBundle:CcCcodConfigs")->isEnableGroup($vccId);
        /*获取所有业务组*/
        $groups = $em->getRepository("IcsocSecurityBundle:WinGroup")->getGroupsName($vccId);
        $gridData = array();

        if ($big == false && isset($list['code']) && $list['code'] == 200) {
            foreach ($list['data'] as $k => $v) {
                $gridData[$k] = $v;
                $gridData[$k]['group_id'] = isset($groups[$v['group_id']]) ? $groups[$v['group_id']] : '';
            }
        }

        /** 导出功能 */
        if (!empty($export) && $list['code'] == 200) {
            /** @var  $title (导出字段) */
            $title = array(
                'ag_num' => array(
                    'title' => 'Agent Num',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_name' => array(
                    'title' => 'Agent Name',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_phone' => array(
                    'title' => 'Agent Phone',
                    'type' => Col::TYPE_STRING,
                ),
                'cus_phone' => array(
                    'title' => 'Cus Phone',
                    'type' => Col::TYPE_STRING,
                ),
                'que_name' => array(
                    'title' => 'Queue',
                    'type' => Col::TYPE_STRING,
                ),
                'start_time' => array(
                    'title' => 'Trans Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'que_name_trans' => array(
                    'title' => 'Trans Queues',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                )
            );
            if ($isEnableGroup) {
                $title = array(
                    'ag_num' => array(
                        'title' => 'Agent Num',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_name' => array(
                        'title' => 'Agent Name',
                        'type' => Col::TYPE_STRING,
                    ),
                    'group_id' => array(
                        'title' => 'Business Group',
                        'type' => Col::TYPE_STRING,
                    ),
                    'ag_phone' => array(
                        'title' => 'Agent Phone',
                        'type' => Col::TYPE_STRING,
                    ),
                    'cus_phone' => array(
                        'title' => 'Cus Phone',
                        'type' => Col::TYPE_STRING,
                    ),
                    'que_name' => array(
                        'title' => 'Queue',
                        'type' => Col::TYPE_STRING,
                    ),
                    'start_time' => array(
                        'title' => 'Trans Time',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    ),
                    'que_name_trans' => array(
                        'title' => 'Trans Queues',
                        'type' => Col::TYPE_STRING,
                        'filters' => array(
                            array(
                                'filter' => 'datetime',
                                'format' => 'Y-m-d H:i:s',
                            )
                        ),
                    )
                );
            }
            if ($big == false) {
                switch ($export) {
                    case 'csv':
                        return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                    case 'excel':
                        return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                    default:
                        exit;
                }
            } else if ($big == true) {
                $type = array(
                    'start_time' => 'datetime',
                    'que_name_trans' => 'datetime',
                    'group_id' => 'enum',
                );
                $endResults['enum']['group_id'] = $groups;
                /** @var  $title (导出字段) */
                $newTitle = $this->get('icsoc_data.model.export')->getOldNewTitle($title, $export, $type, $endResults);
                $newData = array(
                    'vcc_id' => $vccId,
                    'sql' => $list['sql'],
                    'datacount' => $list['total'],
                    'cols' => json_encode($newTitle),
                );
                $re = $this->get('icsoc_data.model.export')->record($export, $newData, '技能组转移');

                return new JsonResponse($re);
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : 1,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 满意度评价报表
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function evaluateAction()
    {
        return $this->render('IcsocReportBundle:Detail:evaluateDetailList.html.twig', array(
            'evaluate' => $this->container->getParameter('EVALUATES'),
            'date' => $this->getData(),
        ));
    }

    /**
     * 满意度评价列表数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function evaluateListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');

        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );

        $list = $this->get('icsoc_data.model.report')->getEvaluateListData(
            array(
                'vcc_id'=>$this->getUser()->getVccId(),
                'info'=>$info,
            )
        );

        $gridData = array();

        $recordApiUrl = $this->container->getParameter('record_api_url');

        if (isset($list['code']) && $list['code'] == 200) {
            /** @var string $callType 呼叫类型 */
            $callType = $this->container->getParameter('CALLTYPE');
            /** @var string $evaluates 满意度评价结果 */
            $evaluates = $this->container->getParameter('EVALUATES');
            if (empty($export)) {
                $endResult = $this->container->getParameter('RENDRESULT');
            } else {
                $endResult = $this->container->getParameter('ENDRESULT');
            }

            foreach ($list['data'] as $k => $v) {
                $gridData[$k] = $v;
                $gridData[$k]['ag_num'] = $v['ag_num'].' '.$v['ag_name'];
                $gridData[$k]['start_time'] = date("Y-m-d H:i:s", $v['start_time']);
                $gridData[$k]['call_type'] = isset($callType[$v['call_type']]) ? $callType[$v['call_type']] : '';
                $gridData[$k]['evaluate'] = isset($evaluates[$v['evaluate']]) ? $evaluates[$v['evaluate']] : '';
                $gridData[$k]['result'] = isset($endResult[$v['result']]) ? $endResult[$v['result']] : '';
                if (empty($export)) {
                    if (empty($v['ag_id'])) {
                        $gridData[$k]['record_file'] = $recordApiUrl.'/api/record/playrecord/'.$this->getUser()->getVccId().'/'.$v['call_id'];
                    } else {
                        $gridData[$k]['record_file'] = $recordApiUrl.'/api/record/playrecord/'.$this->getUser()->getVccId().'/'.$v['call_id'].'/'.$v['ag_id'];
                    }
                }
            }
        }

        /** 导出功能 */
        if (!empty($export)) {
            /** @var  $title (导出字段) */
            $title = array(
                'ag_num' =>'Agent',
                'ag_phone' =>'Agent Phone',
                'cus_phone' =>'Cus Phone',
                'que_name' =>'Queue',
                'server_num' =>'Server Num',
                'call_type' =>'Call Type',
                'start_time' =>'Start Time',
                'conn_secs' =>'Detail Conn Secs',
                'evaluate' =>'Evaluate Result',
            );

            $translator = $this->get('translator');
            switch ($export) {
                case 'csv':
                    return $this->exportCsv($gridData, json_encode($title), array(
                        'text' => $translator->trans('Call detail Report'),
                        'href' => $this->generateUrl('icsoc_report_call'),
                    ));
                case 'excel':
                    return $this->exportExcel($gridData, json_encode($title), array(
                        'text' => $translator->trans('Call detail Report'),
                        'href' => $this->generateUrl('icsoc_report_call'),
                    ));
                default:
                    exit;
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : 1,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 短信明细
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function smsAction()
    {
        $options = array(
            'phone'=>array('text'=>'Receiver Phone', 'type'=>1),
            'result'=>array('text'=>'Send Result', 'type'=>2, 'options'=>$this->container->getParameter('SMSRESULT')),

        );
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();

        return $this->render('IcsocReportBundle:Detail:smsDetailList.html.twig', array(
            'options' => $options,
            'date' => $this->getData(),
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * 短信明细数据
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function smsListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');

        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
        );

        $list = $this->get('icsoc_data.model.report')->getSmsListData(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
            )
        );

        $gridData = array();

        if (isset($list['code']) && $list['code'] == 200) {
//            foreach ($list['data'] as $k => $v) {
//                $gridData[$k] = $v;
//            }
            $gridData = $list['data'];
        }

        /** 导出功能 */
        if (!empty($export)) {
            /** @var  $title (导出字段) */
            $title = array(
                'phone' => array(
                    'title' => 'Receiver Phone',
                    'type' => Col::TYPE_STRING,
                ),
                'deal_time' => array(
                    'title' => 'Send Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'result' => array(
                    'title' => 'Send Result',
                    'type' => Col::TYPE_STRING,
                ),
                'result_message' => array(
                    'title' => 'Result Message',
                    'type' => Col::TYPE_STRING,
                ),
                'content' => array(
                    'title' => 'Content',
                    'type' => Col::TYPE_STRING,
                ),
            );

            switch ($export) {
                case 'csv':
                    return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                case 'excel':
                    return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                default:
                    exit;
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : 1,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 获取IVR流转轨迹
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getIvrPathAction(Request $request)
    {
        $callId = $request->get('call_id', 0);
        if (empty($callId)) {
            return new JsonResponse(array('error' => 1, 'message' => '呼叫ID为空'));
        }
        $callId = $this->container->get('icsoc_data.model.report')->processCallId($callId);
        //从mongodb读取
        //$res = $this->container->get('icsoc_core.mongodb_common.class')->getDataIvr(array('call_id'=>$callId), 'win_ivr_path');
        $conn = $this->get('doctrine.dbal.default_connection');
        $res = $conn->fetchAssoc(
            "SELECT * FROM win_ivr_path WHERE call_id = :call_id",
            array('call_id'=>$callId)
        );
        if (empty($res)) {
            return new JsonResponse(array('error' => 1, 'message' => 'IVR流程不存在'));
        }
        //先从reids 读取；
//        $ivrInfo = $this->get('snc_redis.default')->hget("ivrPathNodeNameType", $callId);
//        if (empty($ivrInfo)) {
//            $ivrInfo = $conn->fetchColumn(
//                "SELECT ivr_info FROM win_ivr WHERE ivr_code = :ivr_code",
//                array('ivr_code'=>$res['ivr_code'])
//            );
//        }
        //一开始考虑欠佳；有的ivr只有一个节点就没有=>符号
        if (strpos($res['ivr_path'], '=>') !== false || substr_count($res['ivr_path'], '@') >= 3) {
            //新轨迹
            //$infos = $res['ivr_path'];
            $infos = explode('=>', $res['ivr_path']);
            $ivrInfo = array();
            $ivrPath = array();
            foreach ($infos as $nodeValue) {
                $nodeinfos = explode('@', $nodeValue);
                $node = isset($nodeinfos[0]) ? $nodeinfos[0] : false;
                $nodeName = isset($nodeinfos[1]) ? $nodeinfos[1] : false;
                $nodeType = isset($nodeinfos[2]) ? $nodeinfos[2] : false;
                $time = isset($nodeinfos[3]) ? date('Y-m-d H:i:s', $nodeinfos[3]): '';
                $ivrCode = isset($nodeinfos[4]) ? $nodeinfos[4]: '';//用于转ivr节点
                $ivrInfoKey = !empty($ivrCode) ? $ivrCode.$node : $node;
                $nodeName = !empty($ivrCode) ? $nodeName.'('.$ivrCode.')' : $nodeName;
                if (($node !== false) && ($nodeName !== false) && ($nodeType !== false)) {
                    $ivrInfo[$ivrInfoKey] = array('name'=>$nodeName, 'type'=>$nodeType, 'time'=>$time);
                } else {
                    $ivrInfo = array();
                    break;
                }
                array_push($ivrPath, $ivrInfoKey);
            }
        } else {
            $ivrInfo = $conn->fetchColumn(
                "SELECT ivr_info FROM win_ivr WHERE ivr_code = :ivr_code",
                array('ivr_code'=>$res['ivr_code'])
            );
            $ivrInfo = json_decode($ivrInfo, true);
            $ivrPath = explode('-', $res['ivr_path']);
        }

        if (empty($ivrInfo)) {
            return new JsonResponse(array('error' => 1, 'message' => 'IVR流程不存在'));
        }

        $ivr = array();
        $translator = $this->get('translator');

        foreach ($ivrPath as $key => $v) {
            $type = isset($ivrInfo[$v]['type']) ? $ivrInfo[$v]['type'] : 0;
            $name = isset($ivrInfo[$v]['name']) ? $ivrInfo[$v]['name'] : '';
            $time = isset($ivrInfo[$v]['time']) ? $ivrInfo[$v]['time'] : '';

            switch ($type) {
                /** 按键导航子节点处理 */
                case 30:
                    switch ($name) {
                        case 'F':
                            $name = '按键失败';
                            break;
                        case 'multiKeys':
                            $name = $translator->trans('Multi keys');
                            break;
                        default:
                            $name = sprintf("按%s键", $name);
                            break;
                    }
                    break;
                /** 转技能组子节点处理 */
                case 50:
                    switch ($name) {
                        case $translator->trans('None agent'):
                            $type = '50-none_agent';
                            break;
                        case $translator->trans('Queue is full'):
                            $type = '50-queue_is_full';
                            break;
                        case $translator->trans('Queue timeout'):
                            $type = '50-queue_times_exceed';
                            break;
                        default:
                            $type = '50-queue_fail';
                            break;
                    }
                    break;
                /** 收号子节点处理 */
                case 80:
                    switch ($name) {
                        case $translator->trans('Keys timeout'):
                            $type = '80-timeout';
                            break;
                        default:
                            $type = '80-success';
                            break;
                    }
                    break;
                /** HTTP节点处理 */
                case 90:
                    switch ($name) {
                        case $translator->trans('Return failure'):
                            $type = '90-failure';
                            break;
                        default:
                            $type = '90-success';
                            break;
                    }
                    break;
                /** 来电记忆节点处理 */
                case 100:
                    switch ($name) {
                        case $translator->trans('Return failure'):
                            $type = '100-failure';
                            break;
                        default:
                            $type = '100-success';
                            break;
                    }
                    break;
                /** 判断子节点处理 */
                case 130:
                    switch ($name) {
                        case 'judge_fail':
                            $name = "判断失败";
                            $type = '130-failure';
                            break;
                        default:
                            $name = isset($ivrInfo[$v]['show_name']) ? $ivrInfo[$v]['show_name'] : $name;
                            break;
                    }
                    break;
            }

            $ivr[$key]['name'] = $name;
            $ivr[$key]['type'] = $type;
            $ivr[$key]['time'] = $time;
        }

        /*$end = end($ivr);
        if ($end['type'] != 7) {
            $ivr[] = array('name'=>'挂机', 'type'=>7);
        }*/

        return new JsonResponse($ivr);
    }

    /**
     * 导出Ivr轨迹
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function exportIvrPathAction(Request $request)
    {
        $translator = $this->get('translator');
        $data = $request->get('param', '');
        $export = $request->get('export', '');
        $data = html_entity_decode(urldecode($data));
        $info = array(
            'export' => $export,
            'filter' => !empty($data) ? json_decode($data, true) : array(),
        );
        $rst = $this->get('icsoc_data.model.report')->getIvrPathData(
            array(
                'vcc_id'=>$this->getVccId(),
                'data'=>json_encode($info),
            )
        );

        $pathData = array();
        if (isset($rst['code']) && $rst['code'] == 200) {
            foreach ($rst['data'] as $v) {
                $ivrPathInfo = array();
                if (strpos($v['ivr_path'], '=>') !== false) {
                    //新轨迹
                    $infos = explode('=>', $v['ivr_path']);
                    $ivrInfo = array();
                    $ivrPath = array();
                    foreach ($infos as $nodeValue) {
                        $nodeinfos = explode('@', $nodeValue);
                        $node = isset($nodeinfos[0]) ? $nodeinfos[0] : false;
                        $nodeName = isset($nodeinfos[1]) ? $nodeinfos[1] : false;
                        $nodeType = isset($nodeinfos[2]) ? $nodeinfos[2] : false;
                        $time = isset($nodeinfos[3]) ? $nodeinfos[3] : '';
                        $ivrCode = isset($nodeinfos[4]) ? $nodeinfos[4]: '';//用于转ivr节点
                        $ivrInfoKey = !empty($ivrCode) ? $ivrCode.$node : $node;
                        $nodeName = !empty($ivrCode) ? $nodeName.'('.$ivrCode.')' : $nodeName;
                        if (($node !== false) && ($nodeName !== false) && ($nodeType !== false)) {
                            $ivrInfo[$ivrInfoKey] = array('name'=>$nodeName, 'type'=>$nodeType, 'time'=>$time);
                        } else {
                            $ivrInfo = array();
                            break;
                        }
                        array_push($ivrPath, $ivrInfoKey);
                    }
                } else {
                    $ivrInfo = json_decode($v['ivr_info'], true);
                    $ivrPath = explode('-', $v['ivr_path']);
                }

                if (empty($ivrInfo)) {
                    $ivrPathInfo[0] = 'ivr轨迹不存在';
                } else {
                    foreach ($ivrPath as $key => $val) {
                        if (!isset($ivrInfo[$val])) {
                            $ivrPathInfo[$key] = '节点不存在';
                        } else {
                            $type = isset($ivrInfo[$val]['type']) ? $ivrInfo[$val]['type'] : 0;
                            $name = isset($ivrInfo[$val]['name']) ? $ivrInfo[$val]['name'] : '';
                            $time = isset($ivrInfo[$val]['time']) ? '['.date('Y-m-d H:i:s', $ivrInfo[$val]['time']).']' : '';
                            switch ($type) {
                                /** 按键导航子节点处理 */
                                case 30:
                                    switch ($name) {
                                        case 'F':
                                            $name = '按键失败';
                                            break;
                                        case 'multiKeys':
                                            $name = $translator->trans('Multi keys');
                                            break;
                                        default:
                                            $name = sprintf("按%s键", $name);
                                            break;
                                    }
                                    break;
                                /** 判断子节点处理 */
                                case 130:
                                    switch ($name) {
                                        case 'judge_fail':
                                            $name = "判断失败";
                                            break;
                                        default:
                                            $name = isset($ivrInfo[$val]['show_name']) ?
                                                $ivrInfo[$val]['show_name'] : $name;
                                            break;
                                    }
                                    break;
                            }

                            $ivrPathInfo[$key] = $name."[{$this->nodes[$type]}]".$time;
                        }
                    }
                }
                unset($v['ivr_info']);
                unset($v['ivr_path']);
                $pathData[] = array_merge($v, $ivrPathInfo);
            }
        } else {
            $data = array(
                'data'=>array(
                    'msg_detail'=>isset($rst['message']) ? $rst['message'] : '导出文件错误',
                    'type'=>'danger',
                    'link' => array(
                        array(
                            'text'=>$translator->trans('Callin detail Report'),
                            'href'=>$this->generateUrl('icsoc_report_callin'),
                        ),
                    ),
                ),
            );

            return $this->redirect($this->generateUrl('icsoc_ui_message', $data));
        }

        /** @var  $title (导出字段) */
        if (!empty($pathData)) {
            $index = 0;
            foreach ($pathData as $k => $v) {
                if (count($pathData[$index]) < count($v)) {
                    $index = $k;
                }
            }
            foreach ($pathData[$index] as $k => $v) {
                $title[$k] = array(
                    'title' => $k,
                    'type' => Col::TYPE_STRING
                );
                if ($k === 'call_id') {
                    $title[$k] = array(
                        'title' => 'Call ID',
                        'type' => Col::TYPE_STRING,
                    );
                }
            }
        } else {
            $title = array('没有数据');
        }

        switch ($export) {
            case 'csv':
                return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $pathData);
            case 'excel':
                return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $pathData);
            default:
                exit;
        }
    }


    /**
     * 技能组通话明细
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function queueAction()
    {
        /** @var  $queues (全部技能组数据) */
        $queues = $this->get('icsoc_data.model.queue')->getQueueNameKeyedByIdArray($this->getUser()->getVccId(), true, true);

        $options = array(
            'ag_num'=>array('text'=>'Agent', 'type'=>2),
            'end_result'=>array('text'=>'Result', 'type'=>2, 'options'=>$this->container->getParameter('END RESULT')),
            'called'=>array('text'=>'Agent Phone', 'type'=>1),
            'que_id'=>array('text'=>'Queues', 'type'=>2, 'options'=>$queues),
            'server_num' => array('text' => 'Server Num', 'type' => 1),
        );
        $isSpecial = $this->container->get("icsoc_core.common.class")->getIsAdmin();

        return $this->render('IcsocReportBundle:Detail:queueDetailList.html.twig', array(
            'options' => $options,
            'date' => $this->getData(),
            'isSpecial' => $isSpecial
        ));
    }

    /**
     * 技能组通话明细数据
     * @param Request $request
     * @return JsonResponse
     */
    public function queueListAction(Request $request)
    {
        $data = $request->get('param', '');
        $data = html_entity_decode(urldecode($data));

        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'Desc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');
        $big = $request->get('big', false);
        $sum = $request->get('sum', false);
        $info = array(
            'export' => $export,
            'pagination' => array('rows'=>$rows, 'page'=>$page),
            'filter' => !empty($data) ? json_decode($data, true) : array(),
            'sort' => array('order'=>$sord, 'field'=>$sidx),
        );

        /*$list = $this->get('icsoc_data.model.report')->getQueueListData(
           array(
               'vcc_code'=>$this->getVccCode(),
               'info'=>json_encode($info),
               'big' => $big,
           )
       );*/

        $list = $this->get('icsoc_data.model.report')->getQueueListDataFromElasticsearch(
            array(
                'vcc_code'=>$this->getVccCode(),
                'info'=>json_encode($info),
                'big' => $big,
            )
        );
        if ($sum == true) {
            return new JsonResponse(array('code' => 200, 'count' => $list['total']));
        }
        $gridData = array();
        $vccId = $this->getVccId();
        if ($big == false && isset($list['code']) && $list['code'] == 200) {
            $gridData = $list['data'];
        }

        /** 导出功能 */
        if (!empty($export) && $list['code'] == 200) {
            /** @var  $title (导出字段) */
            $title = array(
                'server_num' => array(
                    'title' => 'Server Num',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_num' => array(
                    'title' => 'Agent Num',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_name' => array(
                    'title' => 'Agent Name',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_phone' => array(
                    'title' => 'Agent Phone',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_phone_areacode' => array(
                    'title' => 'AgPhone Areacode',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_phone_areaname' => array(
                    'title' => 'AgPhone Areaname',
                    'type' => Col::TYPE_STRING,
                ),
                'caller_num' => array(
                    'title' => 'Caller',
                    'type' => Col::TYPE_STRING,
                ),
                'caller_areacode' => array(
                    'title' => 'Caller Areacode',
                    'type' => Col::TYPE_STRING,
                ),
                'caller_areaname' => array(
                    'title' => 'Caller Areaname',
                    'type' => Col::TYPE_STRING,
                ),
                'que_name' => array(
                    'title' => 'Queue',
                    'type' => Col::TYPE_STRING,
                ),
                'ent_que_time' => array(
                    'title' => 'Start Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'assign_time' => array(
                    'title' => 'Agent Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'link_time' => array(
                    'title' => 'Conn Time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'end_time' => array(
                    'title' => 'End time',
                    'type' => Col::TYPE_STRING,
                    'filters' => array(
                        array(
                            'filter' => 'datetime',
                            'format' => 'Y-m-d H:i:s',
                        )
                    ),
                ),
                'que_secs' => array(
                    'title' => 'Wait Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'ring_secs' => array(
                    'title' => 'Ring Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'conn_secs' => array(
                    'title' => 'Detail Conn Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'all_secs' => array(
                    'title' => 'All Secs',
                    'type' => Col::TYPE_NUMBER,
                ),
                'endresult' => array(
                    'title' => 'Result',
                    'type' => Col::TYPE_STRING,
                ),
            );
            if ($big == false) {
                switch ($export) {
                    case 'csv':
                        return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                    case 'excel':
                        return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                    default:
                        exit;
                }
            } else if ($big == true) {
                $type = array(
                    'endresult' => 'enum',
                );
                $endResults['enum']['endresult'] = $this->container->getParameter('END RESULT');
                $endResults['enum']['endresult']['21'] = '未接通';
                /** @var  $title (导出字段) */
                $newTitle = $this->get('icsoc_data.model.export')->getNewTitle($title, $type, $endResults);
                //新版异步导出
                $newData = array(
                    'es_search'=>json_encode($list['data']),
                    'mongo_coll' => 'win_queue_cdr',
                    'cols' =>json_encode($newTitle),
                );
                $re = $this->get('icsoc_data.model.export')->newRecord($export, $newData, '技能组通话');

                return new JsonResponse($re);
            }
        }

        $result = array(
            'total' => isset($list['total_pages']) ? $list['total_pages'] : 0,
            'page' => isset($list['page']) ? $list['page'] : 1,
            'records' => isset($list['total']) ? $list['total'] : 0,
            'rows' => $gridData,
            'code' => isset($list['code']) ? $list['code'] : '',
            'message' => isset($list['message']) ? $list['message'] : '',
        );

        return new JsonResponse($result);
    }

    /**
     * 坐席签到明细报表
     * @param Request $request
     * @return Response
     */
    public function agentSignInDetailAction(Request $request)
    {
        if ($request->get('ag_id')) {
            $startDate = $request->get('start_date', '');
            $endDate = $request->get('end_date', '');
            $agId = $request->get('ag_id', -1);
            $item = $this->get('icsoc_report_for_role.model.report')->proccessStaReason(array(), 'day');
            return $this->render('IcsocReportBundle:Detail:agentSignInDetail.html.twig',
                array(
                    'post' => 1,
                    'date' => array('start_date'=>$startDate, 'end_date'=>$endDate),
                    'item' => $item,
                    'ag_id' => $agId,
                )
            );
        }
        $vccId = $this->getVccId();
        /** 获取所有坐席 **/
        $agName = $this->get('icsoc_data.model.agent')->getAgentNameKeyedByIdArray($vccId, true);
        $options = array(
            'ag_id'=>array('text'=>'Username', 'options'=>$agName),
        );
        $item = $this->get('icsoc_report_for_role.model.report')->proccessStaReason(array(), 'day');
        foreach ($item as $key => $val) {
            if (strstr($key, 'agstaduration')) {
                $item[$key]['text'] = $val['text'].' (h)';
            }
        }

        return $this->render('IcsocReportBundle:Detail:agentSignInDetail.html.twig',
            array(
                'post' => 2,
                'options' => $options,
                'item' => $item,
                'date' => $this->getData(),
            )
        );
    }

    /**
     * 坐席签到明细报表数据
     * @param Request $request
     * @return JsonResponse
     */
    public function agentSignInDetailDataAction(Request $request)
    {
        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'asc');
        $sidx = $request->get('sidx', 'id');
        $export = $request->get('export', '');

        $param = $request->get('param', '');

        $info = array(
            'pagination'=>array('rows'=>$rows, 'page'=>$page),
            'filter'=> json_decode($param),
            'sort'=>array('order' => $sord, 'field' => $sidx),
        );
        $vccId = $this->getVccId();
        
        $list = $this->get('icsoc_data.model.report')->getSignInData(
            array(
                'vcc_id'=>$vccId,
                'info'=>json_encode($info),
                'export' => $export,
            )
        );

        $gridData = array();
        if (isset($list['code']) && $list['code'] == 200) {
            $gridData = $list['data'];
        }

        /** 导出功能 */
        if (!empty($export)  && $list['code'] == 200) {
            $item = $this->get('icsoc_report_for_role.model.report')->proccessStaReason(array(), 'day');
            /** @var  $title (导出字段) */
            $title = array(
                'ag_num' => array(
                    'title' => 'Agent number',
                    'type' => Col::TYPE_STRING,
                ),
                'ag_name' => array(
                    'title' => 'Username',
                    'type' => Col::TYPE_STRING,
                ),
                'login_ip' => array(
                    'title' => 'Login IP',
                    'type' => Col::TYPE_STRING,
                ),
                'rep_date' => array(
                    'title' => 'Date',
                    'type' => Col::TYPE_STRING,
                ),
                'login_time' => array(
                    'title' => '最早签入时间',
                    'type' => Col::TYPE_STRING,
                ),
                'logout_time' => array(
                    'title' => '最晚签出时间',
                    'type' => Col::TYPE_STRING,
                ),
                'login_secs' => array(
                    'title' => '登录时长(h)',
                    'type' => Col::TYPE_STRING,
                ),
                'ready_secs' => array(
                    'title' => '空闲时长(h)',
                    'type' => Col::TYPE_STRING,
                ),
                'busy_secs' => array(
                    'title' => '总示忙时长(h)',
                    'type' => Col::TYPE_STRING,
                ),
            );
            if (isset($item) && !empty($item) && is_array($item)) {
                foreach ($item as $key => $val) {
                    if (strstr($key, 'agstaduration')) {
                        $title[$val['field']] =array(
                            'title' => $val['text'].' (h)',
                            'type' => Col::TYPE_STRING,
                        );
                    } else {
                        $title[$val['field']] =array(
                            'title' => $val['text'],
                            'type' => Col::TYPE_STRING,
                        );
                    }
                }
            }
            switch ($export) {
                case 'csv':
                    return $this->container->get('icsoc_data.model.batchExport')->exportCsv($title, $gridData);
                case 'excel':
                    return $this->container->get('icsoc_data.model.batchExport')->exportExcel($title, $gridData);
                default:
                    exit;
            }
        }

        return new JsonResponse(
            array(
                'total' => isset($list['totalPage']) ? $list['totalPage'] : 0,
                'page' => isset($list['page']) ? $list['page'] : 1,
                'records' => isset($list['total']) ? $list['total'] : 0,
                'rows' => $gridData,
                'code' => isset($list['code']) ? $list['code'] : '',
                'message' => isset($list['message']) ? $list['message'] : '',
            )
        );
    }
}
