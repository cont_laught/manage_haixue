<?php
namespace Icsoc\PhoneManageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\Container;

class PhoneType extends AbstractType
{
    private $container;
    private $translations;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->translations = $this->container->get('translator');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        $builder
            ->add('vcc_code', 'text', array(
                    'attr'=>array('class'=>'col-xs-10 col-sm-4'),
                    'label'=>'vcc_code',
                    'disabled' => true,
                    'required' => false,
                    'data'=>$this->container->get('security.token_storage')->getToken()->getUser()->getVccCode()
                ))
            ->add('pho_type', 'choice', array(
                    'label'=>'Phone Type',
                    'attr'=>array('class'=>'col-xs-10 col-sm-5', 'onclick'=>'switchOption("pho_type")'),
                    'choices'=>$this->container->getParameter('phone_type'),
                    'data'=>isset($data['pho_type']) ? $data['pho_type'] : 1,
                    'expanded'=>true,
                    'multiple'=>false
                ))
            ->add('pho_num', 'textarea', array(
                    'attr' => array('class' => 'col-xs-10 col-sm-4',
                    'placeholder'=>$this->container->get('translator')->trans('Each line a number')),
                    'label' => 'Phone Number',
                ))
            ->add('pho_start', 'number', array(
                'attr' => array(
                    'class' => 'col-xs-10 col-sm-5',
                    'data-rel'=>'tooltip',
                    'title'=>$this->container->get('translator')
                            ->trans('SIP extension number 4 digits in the beginning of the 8'),
                    'data-placement' => 'bottom'
                ),
                'label' => 'The extension number',
            ))
            ->add('pho_end', 'number', array(
                    'attr' => array(
                        'class' => 'col-xs-10 col-sm-5',
                        'data-rel'=>'tooltip',
                        'title'=>$this->container->get('translator')
                                ->trans('SIP extension number 4 digits in the beginning of the 8'),
                        'data-placement' => 'bottom'
                    ),
            ))
            ->add('passtype', 'choice', array(
                'label'=>'Extension password',
                'attr'=>array('class'=>'col-xs-10 col-sm-4','onclick'=>'showPassnum()'),
                'choices'=>array(1=>'Random password',2=>'Fixed password'),
                'data'=>isset($data['pho_type']) ? $data['pho_type'] : 1,
                'expanded'=>true,
                'multiple'=>false,
            ))
            ->add('pho_pass', 'number', array(
                'attr' => array('class' => 'col-xs-10 col-sm-4'),
            ))
        ;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'phone_info_form';
    }
}
