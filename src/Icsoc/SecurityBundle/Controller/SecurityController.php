<?php

namespace Icsoc\SecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

/**
 * 登陆逻辑
 * Class SecurityController
 * @package Icsoc\SecurityBundle\Controller
 */
class SecurityController extends Controller
{
    /**
     * 登陆
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        // get the login error if there is one
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                Security::AUTHENTICATION_ERROR
            );

        } elseif (null !== $session && $session->has(Security::AUTHENTICATION_ERROR)) {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }
        $loginVcc = $this->get('session')->get('login_vcc');
        $loginUser = $this->get('session')->get('login_user');
        $copyright = $this->container->getParameter('copyright');
        $system_name = $this->container->getParameter('system_name');
        $version = $this->container->getParameter('version');

        //验证码需要使用
        $key = $this->container->getParameter("gregwar_captcha.config.whitelist_key");
        $this->get("session")->set($key, array('login'));

        return $this->render(
            'IcsocSecurityBundle:Security:newlogin.html.twig',
            array(
                'copyright' => $copyright,
                'system_name' => $system_name,
                'login_vcc' => $loginVcc,
                'login_user' => $loginUser,
                'version' => $version,
                'error' => $error,
            )
        );
    }
}
