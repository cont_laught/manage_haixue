<?php

namespace Icsoc\SecurityBundle\Tests\Command;

use Icsoc\SecurityBundle\Command\RoleTransformationCommand;
use Icsoc\SecurityBundle\Command\TestRouteCommand;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class RouteCommandTest
 * @package Icsoc\SecurityBundle\Tests\Command
 */
class RouteCommandTest extends WebTestCase
{
    public function testRoute()
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new TestRouteCommand());
        $command = $application->find("test:route");
        $commandTester = new CommandTester($command);
        $commandTester->execute(array());
        $str = $commandTester->getDisplay();
        $this->assertTrue(strpos($str, "0000")===false);
    }

    public function testRoleTransformation()
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new RoleTransformationCommand());
        $command = $application->find("role:transformation");
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            '--vcc_code'=>'11xx',
            '--flag'=>'1',
        ));
        $content = $commandTester->getDisplay();
        exit;
    }
}
