<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\VarDumper\VarDumper;

/**
 * UserMenuRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserMenuRepository extends EntityRepository
{

    /**
     * 获取用户自定义菜单个数
     * @param $userId
     * @param $flag
     * @return mixed
     */
    public function getUserCountMenu($userId, $flag)
    {
        try {
            $res = $this->createQueryBuilder('u')
                ->select('count(u)')->where("u.userId = :userId AND u.userFlag = :flag")
                ->setParameters(array('userId'=>$userId,'flag'=>$flag))->getQuery()->getSingleResult();
            return $res[1];
        } catch (NoResultException $e){
            return 0;
        }
    }

    /**
     * 获取用户的menu;
     * @param $userId
     * @param $flag
     * @return array
     */
    public function getUserMenu($userId, $flag)
    {
        $res = $this->createQueryBuilder('u')->select("u.menuId,m.menuText,m.menuUrl,u.id")
            ->leftJoin('IcsocMenuBundle:Menu', 'm', 'WITH', 'u.menuId = m.id')
            ->where("u.userId = :userId AND u.userFlag = :flag")
            ->setParameters(array('userId'=>$userId, 'flag'=>$flag))
            ->getQuery()->getResult();
        return $res;
    }

    /**
     * 获取某一标签的相关信息
     * @param $id
     * @return mixed
     */
    public function getMenuInfo($id)
    {
        try {
            $res = $this->createQueryBuilder('u')->select("u.menuId,m.menuText,m.menuUrl,u.id")
                ->leftJoin('IcsocMenuBundle:Menu', 'm', 'WITH', 'u.menuId = m.id')
                ->where("u.id = :id ")
                ->setParameter('id', $id)
                ->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            $res = array();
        }
        return $res;
    }

    /**
     * 删除自定义菜单；
     * @param $id
     * @param $userId
     * @param $flag
     * @return mixed
     */
    public function deleteMenu($id, $userId, $flag)
    {
        $res = $this->createQueryBuilder('u')
            ->where("u.id = :id AND u.userId = :userId AND u.userFlag = :flag")
            ->setParameters(array('id'=>$id, 'userId'=>$userId, 'flag'=>$flag))
            ->delete()->getQuery()->execute();
        return $res;
    }
}
