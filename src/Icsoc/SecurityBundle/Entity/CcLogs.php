<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CcLogs
 */
class CcLogs
{
    /**
     * @var integer
     */
    private $userId;

    /**
     * @var string
     */
    private $userNum;

    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var string
     */
    private $vccCode;

    /**
     * @var boolean
     */
    private $action;

    /**
     * @var string
     */
    private $content;

    /**
     * @var integer
     */
    private $acttime;

    /**
     * @var string
     */
    private $actip;

    /**
     * @var integer
     */
    private $logId;


    /**
     * Set userId
     *
     * @param integer $userId
     * @return CcLogs
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set userNum
     *
     * @param string $userNum
     * @return CcLogs
     */
    public function setUserNum($userNum)
    {
        $this->userNum = $userNum;

        return $this;
    }

    /**
     * Get userNum
     *
     * @return string 
     */
    public function getUserNum()
    {
        return $this->userNum;
    }

    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return CcLogs
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set vccCode
     *
     * @param string $vccCode
     * @return CcLogs
     */
    public function setVccCode($vccCode)
    {
        $this->vccCode = $vccCode;

        return $this;
    }

    /**
     * Get vccCode
     *
     * @return string 
     */
    public function getVccCode()
    {
        return $this->vccCode;
    }

    /**
     * Set action
     *
     * @param boolean $action
     * @return CcLogs
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return boolean 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return CcLogs
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set acttime
     *
     * @param integer $acttime
     * @return CcLogs
     */
    public function setActtime($acttime)
    {
        $this->acttime = $acttime;

        return $this;
    }

    /**
     * Get acttime
     *
     * @return integer 
     */
    public function getActtime()
    {
        return $this->acttime;
    }

    /**
     * Set actip
     *
     * @param string $actip
     * @return CcLogs
     */
    public function setActip($actip)
    {
        $this->actip = $actip;

        return $this;
    }

    /**
     * Get actip
     *
     * @return string 
     */
    public function getActip()
    {
        return $this->actip;
    }

    /**
     * Get logId
     *
     * @return integer 
     */
    public function getLogId()
    {
        return $this->logId;
    }
}
