<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WinGroup
 */
class WinGroup
{
    /**
     * @var string
     */
    private $groupName;

    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var integer
     */
    private $isDel = 0;

    /**
     * @var integer
     */
    private $isLocalRemote = 0;

    /**
     * @var string
     */
    private $remoteAddress = '';

    /**
     * @var integer
     */
    private $groupId;


    /**
     * Set groupName
     *
     * @param string $groupName
     * @return WinGroup
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;

        return $this;
    }

    /**
     * Get groupName
     *
     * @return string 
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return WinGroup
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set isDel
     *
     * @param integer $isDel
     * @return WinGroup
     */
    public function setIsDel($isDel)
    {
        $this->isDel = $isDel;

        return $this;
    }

    /**
     * Get isDel
     *
     * @return integer 
     */
    public function getIsDel()
    {
        return $this->isDel;
    }

    /**
     * Set isLocalRemote
     *
     * @param integer $isLocalRemote
     * @return WinGroup
     */
    public function setIsLocalRemote($isLocalRemote)
    {
        $this->isLocalRemote = $isLocalRemote;

        return $this;
    }

    /**
     * Get isLocalRemote
     *
     * @return integer 
     */
    public function getIsLocalRemote()
    {
        return $this->isLocalRemote;
    }

    /**
     * Set remoteAddress
     *
     * @param string $remoteAddress
     * @return WinGroup
     */
    public function setRemoteAddress($remoteAddress)
    {
        $this->remoteAddress = $remoteAddress;

        return $this;
    }

    /**
     * Get remoteAddress
     *
     * @return string 
     */
    public function getRemoteAddress()
    {
        return $this->remoteAddress;
    }

    /**
     * Get groupId
     *
     * @return integer 
     */
    public function getGroupId()
    {
        return $this->groupId;
    }
}
