<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CcRoles
 */
class CcRoles
{
    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $roleGrade;

    /**
     * @var string
     */
    private $actionList;

    /**
     * @var integer
     */
    private $groupId = 1;

    /**
     * @var string
     */
    private $reportConfig;

    /**
     * @var integer
     */
    private $roleId;


    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return CcRoles
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CcRoles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set roleGrade
     *
     * @param integer $roleGrade
     * @return integer
     */
    public function setRoleGrade($roleGrade)
    {
        $this->roleGrade = $roleGrade;

        return $this;
    }

    /**
     * Get roleGrade
     *
     * @return integer
     */
    public function getRoleGrade()
    {
        return $this->roleGrade;
    }

    /**
     * Set actionList
     *
     * @param string $actionList
     * @return CcRoles
     */
    public function setActionList($actionList)
    {
        $this->actionList = $actionList;

        return $this;
    }

    /**
     * Get actionList
     *
     * @return string 
     */
    public function getActionList()
    {
        return $this->actionList;
    }

    /**
     * Set groupId
     *
     * @param integer $groupId
     * @return CcRoles
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return integer 
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set reportConfig
     *
     * @param string $reportConfig
     * @return CcRoles
     */
    public function setReportConfig($reportConfig)
    {
        $this->reportConfig = $reportConfig;

        return $this;
    }

    /**
     * Get reportConfig
     *
     * @return string
     */
    public function getReportConfig()
    {
        return $this->reportConfig;
    }

    /**
     * Get roleId
     *
     * @return integer 
     */
    public function getRoleId()
    {
        return $this->roleId;
    }
}
