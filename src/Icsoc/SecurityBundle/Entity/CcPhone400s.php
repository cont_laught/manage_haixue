<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CcPhone400s
 */
class CcPhone400s
{
    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var integer
     */
    private $phoneType;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $phone400;

    /**
     * @var string
     */
    private $phoneAger;

    /**
     * @var integer
     */
    private $displayType;

    /**
     * @var string
     */
    private $vccCode;

    /**
     * @var string
     */
    private $monthlyFee;

    /**
     * @var string
     */
    private $localRate;

    /**
     * @var integer
     */
    private $localType;

    /**
     * @var integer
     */
    private $localPreMinutes;

    /**
     * @var string
     */
    private $localPreFee;

    /**
     * @var string
     */
    private $localAfterRate;

    /**
     * @var string
     */
    private $longRate;

    /**
     * @var integer
     */
    private $longType;

    /**
     * @var integer
     */
    private $longPreMinutes;

    /**
     * @var string
     */
    private $longPreFee;

    /**
     * @var string
     */
    private $longAfterRate;

    /**
     * @var string
     */
    private $specialRate;

    /**
     * @var integer
     */
    private $specialType;

    /**
     * @var integer
     */
    private $specialPreMinutes;

    /**
     * @var string
     */
    private $specialPreFee;

    /**
     * @var string
     */
    private $specialAfterRate;

    /**
     * @var integer
     */
    private $ifMinimum;

    /**
     * @var \DateTime
     */
    private $enableDate;

    /**
     * @var string
     */
    private $minimumFee;

    /**
     * @var integer
     */
    private $trunkIn;

    /**
     * @var integer
     */
    private $trunkOut;

    /**
     * @var integer
     */
    private $ifBind;

    /**
     * @var string
     */
    private $feeFlat;

    /**
     * @var \DateTime
     */
    private $addTime;

    /**
     * @var \DateTime
     */
    private $bindTime;

    /**
     * @var integer
     */
    private $phoneId;


    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return CcPhone400s
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set phoneType
     *
     * @param integer $phoneType
     * @return CcPhone400s
     */
    public function setPhoneType($phoneType)
    {
        $this->phoneType = $phoneType;

        return $this;
    }

    /**
     * Get phoneType
     *
     * @return integer     */
    public function getPhoneType()
    {
        return $this->phoneType;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return CcPhone400s
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone400
     *
     * @param string $phone400
     * @return CcPhone400s
     */
    public function setPhone400($phone400)
    {
        $this->phone400 = $phone400;

        return $this;
    }

    /**
     * Get phone400
     *
     * @return string
     */
    public function getPhone400()
    {
        return $this->phone400;
    }

    /**
     * Set phoneAger
     *
     * @param string $phoneAger
     * @return CcPhone400s
     */
    public function setPhoneAger($phoneAger)
    {
        $this->phoneAger = $phoneAger;

        return $this;
    }

    /**
     * Get phoneAger
     *
     * @return string
     */
    public function getPhoneAger()
    {
        return $this->phoneAger;
    }

    /**
     * Set displayType
     *
     * @param integer $displayType
     * @return CcPhone400s
     */
    public function setDisplayType($displayType)
    {
        $this->displayType = $displayType;

        return $this;
    }

    /**
     * Get displayType
     *
     * @return integer
     */
    public function getDisplayType()
    {
        return $this->displayType;
    }

    /**
     * Set vccCode
     *
     * @param string $vccCode
     * @return CcPhone400s
     */
    public function setVccCode($vccCode)
    {
        $this->vccCode = $vccCode;

        return $this;
    }

    /**
     * Get vccCode
     *
     * @return string
     */
    public function getVccCode()
    {
        return $this->vccCode;
    }

    /**
     * Set monthlyFee
     *
     * @param string $monthlyFee
     * @return CcPhone400s
     */
    public function setMonthlyFee($monthlyFee)
    {
        $this->monthlyFee = $monthlyFee;

        return $this;
    }

    /**
     * Get monthlyFee
     *
     * @return string
     */
    public function getMonthlyFee()
    {
        return $this->monthlyFee;
    }

    /**
     * Set localRate
     *
     * @param string $localRate
     * @return CcPhone400s
     */
    public function setLocalRate($localRate)
    {
        $this->localRate = $localRate;

        return $this;
    }

    /**
     * Get localRate
     *
     * @return string
     */
    public function getLocalRate()
    {
        return $this->localRate;
    }

    /**
     * Set localType
     *
     * @param integer $localType
     * @return CcPhone400s
     */
    public function setLocalType($localType)
    {
        $this->localType = $localType;

        return $this;
    }

    /**
     * Get localType
     *
     * @return integer
     */
    public function getLocalType()
    {
        return $this->localType;
    }

    /**
     * Set localPreMinutes
     *
     * @param integer $localPreMinutes
     * @return CcPhone400s
     */
    public function setLocalPreMinutes($localPreMinutes)
    {
        $this->localPreMinutes = $localPreMinutes;

        return $this;
    }

    /**
     * Get localPreMinutes
     *
     * @return integer
     */
    public function getLocalPreMinutes()
    {
        return $this->localPreMinutes;
    }

    /**
     * Set localPreFee
     *
     * @param string $localPreFee
     * @return CcPhone400s
     */
    public function setLocalPreFee($localPreFee)
    {
        $this->localPreFee = $localPreFee;

        return $this;
    }

    /**
     * Get localPreFee
     *
     * @return string
     */
    public function getLocalPreFee()
    {
        return $this->localPreFee;
    }

    /**
     * Set localAfterRate
     *
     * @param string $localAfterRate
     * @return CcPhone400s
     */
    public function setLocalAfterRate($localAfterRate)
    {
        $this->localAfterRate = $localAfterRate;

        return $this;
    }

    /**
     * Get localAfterRate
     *
     * @return string
     */
    public function getLocalAfterRate()
    {
        return $this->localAfterRate;
    }

    /**
     * Set longRate
     *
     * @param string $longRate
     * @return CcPhone400s
     */
    public function setLongRate($longRate)
    {
        $this->longRate = $longRate;

        return $this;
    }

    /**
     * Get longRate
     *
     * @return string
     */
    public function getLongRate()
    {
        return $this->longRate;
    }

    /**
     * Set longType
     *
     * @param integer $longType
     * @return CcPhone400s
     */
    public function setLongType($longType)
    {
        $this->longType = $longType;

        return $this;
    }

    /**
     * Get longType
     *
     * @return integer
     */
    public function getLongType()
    {
        return $this->longType;
    }

    /**
     * Set longPreMinutes
     *
     * @param integer $longPreMinutes
     * @return CcPhone400s
     */
    public function setLongPreMinutes($longPreMinutes)
    {
        $this->longPreMinutes = $longPreMinutes;

        return $this;
    }

    /**
     * Get longPreMinutes
     *
     * @return integer
     */
    public function getLongPreMinutes()
    {
        return $this->longPreMinutes;
    }

    /**
     * Set longPreFee
     *
     * @param string $longPreFee
     * @return CcPhone400s
     */
    public function setLongPreFee($longPreFee)
    {
        $this->longPreFee = $longPreFee;

        return $this;
    }

    /**
     * Get longPreFee
     *
     * @return string
     */
    public function getLongPreFee()
    {
        return $this->longPreFee;
    }

    /**
     * Set longAfterRate
     *
     * @param string $longAfterRate
     * @return CcPhone400s
     */
    public function setLongAfterRate($longAfterRate)
    {
        $this->longAfterRate = $longAfterRate;

        return $this;
    }

    /**
     * Get longAfterRate
     *
     * @return string
     */
    public function getLongAfterRate()
    {
        return $this->longAfterRate;
    }

    /**
     * Set specialRate
     *
     * @param string $specialRate
     * @return CcPhone400s
     */
    public function setSpecialRate($specialRate)
    {
        $this->specialRate = $specialRate;

        return $this;
    }

    /**
     * Get specialRate
     *
     * @return string
     */
    public function getSpecialRate()
    {
        return $this->specialRate;
    }

    /**
     * Set specialType
     *
     * @param integer $specialType
     * @return CcPhone400s
     */
    public function setSpecialType($specialType)
    {
        $this->specialType = $specialType;

        return $this;
    }

    /**
     * Get specialType
     *
     * @return integer
     */
    public function getSpecialType()
    {
        return $this->specialType;
    }

    /**
     * Set specialPreMinutes
     *
     * @param integer $specialPreMinutes
     * @return CcPhone400s
     */
    public function setSpecialPreMinutes($specialPreMinutes)
    {
        $this->specialPreMinutes = $specialPreMinutes;

        return $this;
    }

    /**
     * Get specialPreMinutes
     *
     * @return integer
     */
    public function getSpecialPreMinutes()
    {
        return $this->specialPreMinutes;
    }

    /**
     * Set specialPreFee
     *
     * @param string $specialPreFee
     * @return CcPhone400s
     */
    public function setSpecialPreFee($specialPreFee)
    {
        $this->specialPreFee = $specialPreFee;

        return $this;
    }

    /**
     * Get specialPreFee
     *
     * @return string
     */
    public function getSpecialPreFee()
    {
        return $this->specialPreFee;
    }

    /**
     * Set specialAfterRate
     *
     * @param string $specialAfterRate
     * @return CcPhone400s
     */
    public function setSpecialAfterRate($specialAfterRate)
    {
        $this->specialAfterRate = $specialAfterRate;

        return $this;
    }

    /**
     * Get specialAfterRate
     *
     * @return string
     */
    public function getSpecialAfterRate()
    {
        return $this->specialAfterRate;
    }

    /**
     * Set ifMinimum
     *
     * @param integer $ifMinimum
     * @return CcPhone400s
     */
    public function setIfMinimum($ifMinimum)
    {
        $this->ifMinimum = $ifMinimum;

        return $this;
    }

    /**
     * Get ifMinimum
     *
     * @return integer
     */
    public function getIfMinimum()
    {
        return $this->ifMinimum;
    }

    /**
     * Set enableDate
     *
     * @param \DateTime $enableDate
     * @return CcPhone400s
     */
    public function setEnableDate($enableDate)
    {
        $this->enableDate = $enableDate;

        return $this;
    }

    /**
     * Get enableDate
     *
     * @return \DateTime
     */
    public function getEnableDate()
    {
        return $this->enableDate;
    }

    /**
     * Set minimumFee
     *
     * @param string $minimumFee
     * @return CcPhone400s
     */
    public function setMinimumFee($minimumFee)
    {
        $this->minimumFee = $minimumFee;

        return $this;
    }

    /**
     * Get minimumFee
     *
     * @return string
     */
    public function getMinimumFee()
    {
        return $this->minimumFee;
    }

    /**
     * Set trunkIn
     *
     * @param integer $trunkIn
     * @return CcPhone400s
     */
    public function setTrunkIn($trunkIn)
    {
        $this->trunkIn = $trunkIn;

        return $this;
    }

    /**
     * Get trunkIn
     *
     * @return integer
     */
    public function getTrunkIn()
    {
        return $this->trunkIn;
    }

    /**
     * Set trunkOut
     *
     * @param integer $trunkOut
     * @return CcPhone400s
     */
    public function setTrunkOut($trunkOut)
    {
        $this->trunkOut = $trunkOut;

        return $this;
    }

    /**
     * Get trunkOut
     *
     * @return integer
     */
    public function getTrunkOut()
    {
        return $this->trunkOut;
    }

    /**
     * Set ifBind
     *
     * @param integer $ifBind
     * @return CcPhone400s
     */
    public function setIfBind($ifBind)
    {
        $this->ifBind = $ifBind;

        return $this;
    }

    /**
     * Get ifBind
     *
     * @return integer
     */
    public function getIfBind()
    {
        return $this->ifBind;
    }

    /**
     * Set feeFlat
     *
     * @param string $feeFlat
     * @return CcPhone400s
     */
    public function setFeeFlat($feeFlat)
    {
        $this->feeFlat = $feeFlat;

        return $this;
    }

    /**
     * Get feeFlat
     *
     * @return string
     */
    public function getFeeFlat()
    {
        return $this->feeFlat;
    }

    /**
     * Set addTime
     *
     * @param \DateTime $addTime
     * @return CcPhone400s
     */
    public function setAddTime($addTime)
    {
        $this->addTime = $addTime;

        return $this;
    }

    /**
     * Get addTime
     *
     * @return \DateTime 
     */
    public function getAddTime()
    {
        return $this->addTime;
    }

    /**
     * Set bindTime
     *
     * @param \DateTime $bindTime
     * @return CcPhone400s
     */
    public function setBindTime($bindTime)
    {
        $this->bindTime = $bindTime;

        return $this;
    }

    /**
     * Get bindTime
     *
     * @return \DateTime 
     */
    public function getBindTime()
    {
        return $this->bindTime;
    }

    /**
     * Get phoneId
     *
     * @return integer
     */
    public function getPhoneId()
    {
        return $this->phoneId;
    }
}
