<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SwjHangupSms
 */
class SwjHangupSms
{
    /**
     * @var string
     */
    private $vccCode;

    /**
     * @var boolean
     */
    private $isConnect;

    /**
     * @var string
     */
    private $connectSms='';

    /**
     * @var boolean
     */
    private $isBusy;

    /**
     * @var string
     */
    private $busySms='';

    /**
     * @var integer
     */
    private $vccId;


    /**
     * Set vccCode
     *
     * @param string $vccCode
     * @return SwjHangupSms
     */
    public function setVccCode($vccCode)
    {
        $this->vccCode = $vccCode;

        return $this;
    }

    /**
     * Get vccCode
     *
     * @return string 
     */
    public function getVccCode()
    {
        return $this->vccCode;
    }

    /**
     * Set isConnect
     *
     * @param boolean $isConnect
     * @return SwjHangupSms
     */
    public function setIsConnect($isConnect)
    {
        $this->isConnect = $isConnect;

        return $this;
    }

    /**
     * Get isConnect
     *
     * @return boolean 
     */
    public function getIsConnect()
    {
        return $this->isConnect;
    }

    /**
     * Set connectSms
     *
     * @param string $connectSms
     * @return SwjHangupSms
     */
    public function setConnectSms($connectSms)
    {
        $this->connectSms = $connectSms;

        return $this;
    }

    /**
     * Get connectSms
     *
     * @return string 
     */
    public function getConnectSms()
    {
        return $this->connectSms;
    }

    /**
     * Set isBusy
     *
     * @param boolean $isBusy
     * @return SwjHangupSms
     */
    public function setIsBusy($isBusy)
    {
        $this->isBusy = $isBusy;

        return $this;
    }

    /**
     * Get isBusy
     *
     * @return boolean 
     */
    public function getIsBusy()
    {
        return $this->isBusy;
    }

    /**
     * Set busySms
     *
     * @param string $busySms
     * @return SwjHangupSms
     */
    public function setBusySms($busySms)
    {
        $this->busySms = $busySms;

        return $this;
    }

    /**
     * Get busySms
     *
     * @return string 
     */
    public function getBusySms()
    {
        return $this->busySms;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    public function setVccId($vccId)
    {
        $this->vccId = $vccId;
        return $this;
    }
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
