<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CcAnnouncement
 */
class CcAnnouncement
{
    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * @var boolean
     */
    private $systemType = 2;

    /**
     * @var integer
     */
    private $addTime;

    /**
     * @var integer
     */
    private $visittimes = 0;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return CcAnnouncement
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CcAnnouncement
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return CcAnnouncement
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return CcAnnouncement
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return CcAnnouncement
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set systemType
     *
     * @param boolean $systemType
     * @return CcAnnouncement
     */
    public function setSystemType($systemType)
    {
        $this->systemType = $systemType;

        return $this;
    }

    /**
     * Get systemType
     *
     * @return boolean 
     */
    public function getSystemType()
    {
        return $this->systemType;
    }

    /**
     * Set addTime
     *
     * @param integer $addTime
     * @return CcAnnouncement
     */
    public function setAddTime($addTime)
    {
        $this->addTime = $addTime;

        return $this;
    }

    /**
     * Get addTime
     *
     * @return integer 
     */
    public function getAddTime()
    {
        return $this->addTime;
    }

    /**
     * Set visittimes
     *
     * @param integer $visittimes
     * @return CcAnnouncement
     */
    public function setVisittimes($visittimes)
    {
        $this->visittimes = $visittimes;

        return $this;
    }

    /**
     * Get visittimes
     *
     * @return integer 
     */
    public function getVisittimes()
    {
        return $this->visittimes;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
