<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WinAgqu
 */
class WinAgqu
{
    /**
     * @var integer
     */
    private $queId;

    /**
     * @var integer
     */
    private $agId;

    /**
     * @var integer
     */
    private $skill = 1;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set queId
     *
     * @param integer $queId
     * @return WinAgqu
     */
    public function setQueId($queId)
    {
        $this->queId = $queId;

        return $this;
    }

    /**
     * Get queId
     *
     * @return integer 
     */
    public function getQueId()
    {
        return $this->queId;
    }

    /**
     * Set agId
     *
     * @param integer $agId
     * @return WinAgqu
     */
    public function setAgId($agId)
    {
        $this->agId = $agId;

        return $this;
    }

    /**
     * Get agId
     *
     * @return integer 
     */
    public function getAgId()
    {
        return $this->agId;
    }

    /**
     * Set skill
     *
     * @param integer $skill
     * @return WinAgqu
     */
    public function setSkill($skill)
    {
        $this->skill = $skill;

        return $this;
    }

    /**
     * Get skill
     *
     * @return integer 
     */
    public function getSkill()
    {
        return $this->skill;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
