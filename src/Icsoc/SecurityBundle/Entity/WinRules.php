<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WinRules
 */
class WinRules
{
    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var string
     */
    private $vccCode;

    /**
     * @var integer
     */
    private $phoneId;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $phone400;

    /**
     * @var string
     */
    private $ruleName;

    /**
     * @var integer
     */
    private $ruleType = 0;

    /**
     * @var string
     */
    private $rule = '';

    /**
     * @var string
     */
    private $pre = '';

    /**
     * @var integer
     */
    private $len = 0;

    /**
     * @var integer
     */
    private $ivrId;

    /**
     * @var string
     */
    private $ivrCode;

    /**
     * @var string
     */
    private $ivrName;

    /**
     * @var integer
     */
    private $priority;

    /**
     * @var string
     */
    private $startDate;

    /**
     * @var string
     */
    private $endDate;

    /**
     * @var string
     */
    private $weeks;

    /**
     * @var string
     */
    private $startTime;

    /**
     * @var string
     */
    private $endTime;

    /**
     * @var string
     */
    private $remark = '';

    /**
     * @var integer
     */
    private $addTime;

    /**
     * @var integer
     */
    private $isOld = 1;

    /**
     * @var integer
     */
    private $ruleId;


    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return WinRules
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set vccCode
     *
     * @param string $vccCode
     * @return WinRules
     */
    public function setVccCode($vccCode)
    {
        $this->vccCode = $vccCode;

        return $this;
    }

    /**
     * Get vccCode
     *
     * @return string 
     */
    public function getVccCode()
    {
        return $this->vccCode;
    }

    /**
     * Set phoneId
     *
     * @param integer $phoneId
     * @return WinRules
     */
    public function setPhoneId($phoneId)
    {
        $this->phoneId = $phoneId;

        return $this;
    }

    /**
     * Get phoneId
     *
     * @return integer 
     */
    public function getPhoneId()
    {
        return $this->phoneId;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return WinRules
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone400
     *
     * @param string $phone400
     * @return WinRules
     */
    public function setPhone400($phone400)
    {
        $this->phone400 = $phone400;

        return $this;
    }

    /**
     * Get phone400
     *
     * @return string 
     */
    public function getPhone400()
    {
        return $this->phone400;
    }

    /**
     * Set ruleName
     *
     * @param string $ruleName
     * @return WinRules
     */
    public function setRuleName($ruleName)
    {
        $this->ruleName = $ruleName;

        return $this;
    }

    /**
     * Get ruleName
     *
     * @return string 
     */
    public function getRuleName()
    {
        return $this->ruleName;
    }

    /**
     * Set ruleType
     *
     * @param integer $ruleType
     * @return WinRules
     */
    public function setRuleType($ruleType)
    {
        $this->ruleType = $ruleType;

        return $this;
    }

    /**
     * Get ruleType
     *
     * @return integer 
     */
    public function getRuleType()
    {
        return $this->ruleType;
    }

    /**
     * Set rule
     *
     * @param string $rule
     * @return WinRules
     */
    public function setRule($rule)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return string 
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Set pre
     *
     * @param string $pre
     * @return WinRules
     */
    public function setPre($pre)
    {
        $this->pre = $pre;

        return $this;
    }

    /**
     * Get pre
     *
     * @return string 
     */
    public function getPre()
    {
        return $this->pre;
    }

    /**
     * Set len
     *
     * @param integer $len
     * @return WinRules
     */
    public function setLen($len)
    {
        $this->len = $len;

        return $this;
    }

    /**
     * Get len
     *
     * @return integer 
     */
    public function getLen()
    {
        return $this->len;
    }

    /**
     * Set ivrId
     *
     * @param integer $ivrId
     * @return WinRules
     */
    public function setIvrId($ivrId)
    {
        $this->ivrId = $ivrId;

        return $this;
    }

    /**
     * Get ivrId
     *
     * @return integer 
     */
    public function getIvrId()
    {
        return $this->ivrId;
    }

    /**
     * Set ivrCode
     *
     * @param string $ivrCode
     * @return WinRules
     */
    public function setIvrCode($ivrCode)
    {
        $this->ivrCode = $ivrCode;

        return $this;
    }

    /**
     * Get ivrCode
     *
     * @return string 
     */
    public function getIvrCode()
    {
        return $this->ivrCode;
    }

    /**
     * Set ivrName
     *
     * @param string $ivrName
     * @return WinRules
     */
    public function setIvrName($ivrName)
    {
        $this->ivrName = $ivrName;

        return $this;
    }

    /**
     * Get ivrName
     *
     * @return string 
     */
    public function getIvrName()
    {
        return $this->ivrName;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return WinRules
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set startDate
     *
     * @param string $startDate
     * @return WinRules
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return string 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param string $endDate
     * @return WinRules
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return string 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set weeks
     *
     * @param string $weeks
     * @return WinRules
     */
    public function setWeeks($weeks)
    {
        $this->weeks = $weeks;

        return $this;
    }

    /**
     * Get weeks
     *
     * @return string 
     */
    public function getWeeks()
    {
        return $this->weeks;
    }

    /**
     * Set startTime
     *
     * @param string $startTime
     * @return WinRules
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return string 
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param string $endTime
     * @return WinRules
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return string 
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set remark
     *
     * @param string $remark
     * @return WinRules
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark
     *
     * @return string 
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set addTime
     *
     * @param integer $addTime
     * @return WinRules
     */
    public function setAddTime($addTime)
    {
        $this->addTime = $addTime;

        return $this;
    }

    /**
     * Get addTime
     *
     * @return integer 
     */
    public function getAddTime()
    {
        return $this->addTime;
    }

    /**
     * Set isOld
     *
     * @param integer $isOld
     * @return WinRules
     */
    public function setIsOld($isOld)
    {
        $this->isOld = $isOld;

        return $this;
    }

    /**
     * Get isOld
     *
     * @return integer 
     */
    public function getIsOld()
    {
        return $this->isOld;
    }

    /**
     * Get ruleId
     *
     * @return integer 
     */
    public function getRuleId()
    {
        return $this->ruleId;
    }
}
