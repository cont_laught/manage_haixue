<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * WinAgent
 */
class WinAgent implements UserInterface, \Serializable
{
    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var string
     */
    private $vccCode;

    /**
     * @var string
     */
    private $agNum;

    /**
     * @var string
     */
    private $agCaller;

    /**
     * @var string
     */
    private $agPassword;

    /**
     * @var string
     */
    private $agName;

    /**
     * @var string
     */
    private $agNickname;
    /**
     * @var boolean
     */
    private $agType;

    /**
     * @var string
     */
    private $agOwncaller;

    /**
     * @var integer
     */
    private $phoId;

    /**
     * @var string
     */
    private $phoNum;

    /**
     * @var integer
     */
    private $groupId;

    /**
     * @var integer
     */
    private $tellevel;

    /**
     * @var integer
     */
    private $acProid;

    /**
     * @var integer
     */
    private $agRole;

    /**
     * @var integer
     */
    private $userRole;

    /**
     * @var boolean
     */
    private $userType;

    /**
     * @var string
     */
    private $userQueues;

    /**
     * @var boolean
     */
    private $crmDatarole;

    /**
     * @var boolean
     */
    private $agSta;

    /**
     * @var integer
     */
    private $agStaReason;

    /**
     * @var integer
     */
    private $agStaTime;

    /**
     * @var integer
     */
    private $agStaTimeDb;

    /**
     * @var integer
     */
    private $failTimes;

    /**
     * @var boolean
     */
    private $isDel;

    /**
     * @var string
     */
    private $loginIp;

    /**
     * @var boolean
     */
    private $ifPopin;

    /**
     * @var string
     */
    private $popinAddress;

    /**
     * @var boolean
     */
    private $ifPopout;

    /**
     * @var string
     */
    private $popoutAddress;

    /**
     * @var integer
     */
    private $timeFirlogin;

    /**
     * @var integer
     */
    private $timeLogin;

    /**
     * @var integer
     */
    private $timeLastcall;

    /**
     * @var integer
     */
    private $secsLogin;

    /**
     * @var integer
     */
    private $secsReady;

    /**
     * @var integer
     */
    private $secsBusy;

    /**
     * @var integer
     */
    private $secsCall;

    /**
     * @var integer
     */
    private $secsRing;

    /**
     * @var integer
     */
    private $secsWait;

    /**
     * @var integer
     */
    private $secAns;

    /**
     * @var integer
     */
    private $timesAns;

    /**
     * @var integer
     */
    private $timesCall;

    /**
     * @var integer
     */
    private $timesBusy;

    /**
     * @var boolean
     */
    private $synchronousAgent;

    /**
     * @var string
     */
    private $smsPhone;

    /**
     * @var boolean
     */
    private $syncAgent;

    /**
     * @var integer
     */
    private $id;

    /** @var  integer */
    private $loginType = 2;

    /** @var  integer */
    private $roleGrade;

    /** @var  string */
    private $actionList;

    /** @var  string */
    private $winIp;


    /** @var \Icsoc\SecurityBundle\Entity\CcCcods */
    private $ccCods;

    /** @var \Icsoc\SecurityBundle\Entity\CcRoles  $agRoleName */
    private $agRoleName;

    private $oldPassword;

    /** @var  integer */
    private $agStatus;

    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return WinAgent
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set vccCode
     *
     * @param string $vccCode
     * @return WinAgent
     */
    public function setVccCode($vccCode)
    {
        $this->vccCode = $vccCode;

        return $this;
    }

    /**
     * Get vccCode
     *
     * @return string
     */
    public function getVccCode()
    {
        return $this->vccCode;
    }

    /**
     * Set agNum
     *
     * @param string $agNum
     * @return WinAgent
     */
    public function setAgNum($agNum)
    {
        $this->agNum = $agNum;

        return $this;
    }

    /**
     * Get agNum
     *
     * @return string
     */
    public function getAgNum()
    {
        return $this->agNum;
    }

    /**
     * Set agCaller
     *
     * @param string $agCaller
     * @return WinAgent
     */
    public function setAgCaller($agCaller)
    {
        $this->agCaller = $agCaller;

        return $this;
    }

    /**
     * Get agCaller
     *
     * @return string
     */
    public function getAgCaller()
    {
        return $this->agCaller;
    }

    /**
     * Set agPassword
     *
     * @param string $agPassword
     * @return WinAgent
     */
    public function setAgPassword($agPassword)
    {
        $this->agPassword = $agPassword;

        return $this;
    }

    /**
     * Get agPassword
     *
     * @return string
     */
    public function getAgPassword()
    {
        return $this->agPassword;
    }

    /**
     * Set agName
     *
     * @param string $agName
     * @return WinAgent
     */
    public function setAgName($agName)
    {
        $this->agName = $agName;

        return $this;
    }

    /**
     * Get agName
     *
     * @return string
     */
    public function getAgName()
    {
        return $this->agName;
    }

    /**
     * Set agNickname
     *
     * @param string $agNickname
     * @return WinAgent
     */
    public function setAgNickname($agNickname)
    {
        $this->agNickname = $agNickname;

        return $this;
    }

    /**
     * Get agNickname
     *
     * @return string
     */
    public function getAgNickname()
    {
        return $this->agNickname;
    }

    /**
     * Set agType
     *
     * @param boolean $agType
     * @return WinAgent
     */
    public function setAgType($agType)
    {
        $this->agType = $agType;

        return $this;
    }

    /**
     * Get agType
     *
     * @return boolean
     */
    public function getAgType()
    {
        return $this->agType;
    }

    /**
     * Set agOwncaller
     *
     * @param string $agOwncaller
     * @return WinAgent
     */
    public function setAgOwncaller($agOwncaller)
    {
        $this->agOwncaller = $agOwncaller;

        return $this;
    }

    /**
     * Get agOwncaller
     *
     * @return string
     */
    public function getAgOwncaller()
    {
        return $this->agOwncaller;
    }

    /**
     * Set phoId
     *
     * @param integer $phoId
     * @return WinAgent
     */
    public function setPhoId($phoId)
    {
        $this->phoId = $phoId;

        return $this;
    }

    /**
     * Get phoId
     *
     * @return integer
     */
    public function getPhoId()
    {
        return $this->phoId;
    }

    /**
     * Set phoNum
     *
     * @param string $phoNum
     * @return WinAgent
     */
    public function setPhoNum($phoNum)
    {
        $this->phoNum = $phoNum;

        return $this;
    }

    /**
     * Get phoNum
     *
     * @return string
     */
    public function getPhoNum()
    {
        return $this->phoNum;
    }

    /**
     * Set groupId
     *
     * @param integer $groupId
     * @return WinAgent
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return integer
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set tellevel
     *
     * @param integer $tellevel
     * @return WinAgent
     */
    public function setTellevel($tellevel)
    {
        $this->tellevel = $tellevel;

        return $this;
    }

    /**
     * Get tellevel
     *
     * @return integer
     */
    public function getTellevel()
    {
        return $this->tellevel;
    }

    /**
     * Set acProid
     *
     * @param integer $acProid
     * @return WinAgent
     */
    public function setAcProid($acProid)
    {
        $this->acProid = $acProid;

        return $this;
    }

    /**
     * Get acProid
     *
     * @return integer
     */
    public function getAcProid()
    {
        return $this->acProid;
    }

    /**
     * @param CcRoles $agRole
     * @return $this
     */
    public function setAgRole($agRole)
    {
        $this->agRole = $agRole;

        return $this;
    }

    /**
     * Get agRole
     *
     * @return boolean
     */
    public function getAgRole()
    {
        return $this->agRole;
    }

    /**
     * Set userRole
     *
     * @param integer $userRole
     * @return WinAgent
     */
    public function setUserRole($userRole)
    {
        $this->userRole = $userRole;

        return $this;
    }

    /**
     * Get userRole
     *
     * @return integer
     */
    public function getUserRole()
    {
        return $this->userRole;
    }

    /**
     * Set userType
     *
     * @param boolean $userType
     * @return WinAgent
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * Get userType
     *
     * @return boolean
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set userQueues
     *
     * @param string $userQueues
     * @return WinAgent
     */
    public function setUserQueues($userQueues)
    {
        $this->userQueues = $userQueues;

        return $this;
    }

    /**
     * Get userQueues
     *
     * @return string
     */
    public function getUserQueues()
    {
        return $this->userQueues;
    }

    /**
     * Set crmDatarole
     *
     * @param boolean $crmDatarole
     * @return WinAgent
     */
    public function setCrmDatarole($crmDatarole)
    {
        $this->crmDatarole = $crmDatarole;

        return $this;
    }

    /**
     * Get crmDatarole
     *
     * @return boolean
     */
    public function getCrmDatarole()
    {
        return $this->crmDatarole;
    }

    /**
     * Set agSta
     *
     * @param boolean $agSta
     * @return WinAgent
     */
    public function setAgSta($agSta)
    {
        $this->agSta = $agSta;

        return $this;
    }

    /**
     * Get agSta
     *
     * @return boolean
     */
    public function getAgSta()
    {
        return $this->agSta;
    }

    /**
     * Set agStaReason
     *
     * @param integer $agStaReason
     * @return WinAgent
     */
    public function setAgStaReason($agStaReason)
    {
        $this->agStaReason = $agStaReason;

        return $this;
    }

    /**
     * Get agStaReason
     *
     * @return integer
     */
    public function getAgStaReason()
    {
        return $this->agStaReason;
    }

    /**
     * Set agStaTime
     *
     * @param integer $agStaTime
     * @return WinAgent
     */
    public function setAgStaTime($agStaTime)
    {
        $this->agStaTime = $agStaTime;

        return $this;
    }

    /**
     * Get agStaTime
     *
     * @return integer
     */
    public function getAgStaTime()
    {
        return $this->agStaTime;
    }

    /**
     * Set agStaTimeDb
     *
     * @param integer $agStaTimeDb
     * @return WinAgent
     */
    public function setAgStaTimeDb($agStaTimeDb)
    {
        $this->agStaTimeDb = $agStaTimeDb;

        return $this;
    }

    /**
     * Get agStaTimeDb
     *
     * @return integer
     */
    public function getAgStaTimeDb()
    {
        return $this->agStaTimeDb;
    }

    /**
     * Set failTimes
     *
     * @param integer $failTimes
     * @return WinAgent
     */
    public function setFailTimes($failTimes)
    {
        $this->failTimes = $failTimes;

        return $this;
    }

    /**
     * Get failTimes
     *
     * @return integer
     */
    public function getFailTimes()
    {
        return $this->failTimes;
    }

    /**
     * Set isDel
     *
     * @param boolean $isDel
     * @return WinAgent
     */
    public function setIsDel($isDel)
    {
        $this->isDel = $isDel;

        return $this;
    }

    /**
     * Get isDel
     *
     * @return boolean
     */
    public function getIsDel()
    {
        return $this->isDel;
    }

    /**
     * Set loginIp
     *
     * @param string $loginIp
     * @return WinAgent
     */
    public function setLoginIp($loginIp)
    {
        $this->loginIp = $loginIp;

        return $this;
    }

    /**
     * Get loginIp
     *
     * @return string
     */
    public function getLoginIp()
    {
        return $this->loginIp;
    }

    /**
     * Set ifPopin
     *
     * @param boolean $ifPopin
     * @return WinAgent
     */
    public function setIfPopin($ifPopin)
    {
        $this->ifPopin = $ifPopin;

        return $this;
    }

    /**
     * Get ifPopin
     *
     * @return boolean
     */
    public function getIfPopin()
    {
        return $this->ifPopin;
    }

    /**
     * Set popinAddress
     *
     * @param string $popinAddress
     * @return WinAgent
     */
    public function setPopinAddress($popinAddress)
    {
        $this->popinAddress = $popinAddress;

        return $this;
    }

    /**
     * Get popinAddress
     *
     * @return string
     */
    public function getPopinAddress()
    {
        return $this->popinAddress;
    }

    /**
     * Set ifPopout
     *
     * @param boolean $ifPopout
     * @return WinAgent
     */
    public function setIfPopout($ifPopout)
    {
        $this->ifPopout = $ifPopout;

        return $this;
    }

    /**
     * Get ifPopout
     *
     * @return boolean
     */
    public function getIfPopout()
    {
        return $this->ifPopout;
    }

    /**
     * Set popoutAddress
     *
     * @param string $popoutAddress
     * @return WinAgent
     */
    public function setPopoutAddress($popoutAddress)
    {
        $this->popoutAddress = $popoutAddress;

        return $this;
    }

    /**
     * Get popoutAddress
     *
     * @return string
     */
    public function getPopoutAddress()
    {
        return $this->popoutAddress;
    }

    /**
     * Set timeFirlogin
     *
     * @param integer $timeFirlogin
     * @return WinAgent
     */
    public function setTimeFirlogin($timeFirlogin)
    {
        $this->timeFirlogin = $timeFirlogin;

        return $this;
    }

    /**
     * Get timeFirlogin
     *
     * @return integer
     */
    public function getTimeFirlogin()
    {
        return $this->timeFirlogin;
    }

    /**
     * Set timeLogin
     *
     * @param integer $timeLogin
     * @return WinAgent
     */
    public function setTimeLogin($timeLogin)
    {
        $this->timeLogin = $timeLogin;

        return $this;
    }

    /**
     * Get timeLogin
     *
     * @return integer
     */
    public function getTimeLogin()
    {
        return $this->timeLogin;
    }

    /**
     * Set timeLastcall
     *
     * @param integer $timeLastcall
     * @return WinAgent
     */
    public function setTimeLastcall($timeLastcall)
    {
        $this->timeLastcall = $timeLastcall;

        return $this;
    }

    /**
     * Get timeLastcall
     *
     * @return integer
     */
    public function getTimeLastcall()
    {
        return $this->timeLastcall;
    }

    /**
     * Set secsLogin
     *
     * @param integer $secsLogin
     * @return WinAgent
     */
    public function setSecsLogin($secsLogin)
    {
        $this->secsLogin = $secsLogin;

        return $this;
    }

    /**
     * Get secsLogin
     *
     * @return integer
     */
    public function getSecsLogin()
    {
        return $this->secsLogin;
    }

    /**
     * Set secsReady
     *
     * @param integer $secsReady
     * @return WinAgent
     */
    public function setSecsReady($secsReady)
    {
        $this->secsReady = $secsReady;

        return $this;
    }

    /**
     * Get secsReady
     *
     * @return integer
     */
    public function getSecsReady()
    {
        return $this->secsReady;
    }

    /**
     * Set secsBusy
     *
     * @param integer $secsBusy
     * @return WinAgent
     */
    public function setSecsBusy($secsBusy)
    {
        $this->secsBusy = $secsBusy;

        return $this;
    }

    /**
     * Get secsBusy
     *
     * @return integer
     */
    public function getSecsBusy()
    {
        return $this->secsBusy;
    }

    /**
     * Set secsCall
     *
     * @param integer $secsCall
     * @return WinAgent
     */
    public function setSecsCall($secsCall)
    {
        $this->secsCall = $secsCall;

        return $this;
    }

    /**
     * Get secsCall
     *
     * @return integer
     */
    public function getSecsCall()
    {
        return $this->secsCall;
    }

    /**
     * Set secsRing
     *
     * @param integer $secsRing
     * @return WinAgent
     */
    public function setSecsRing($secsRing)
    {
        $this->secsRing = $secsRing;

        return $this;
    }

    /**
     * Get secsRing
     *
     * @return integer
     */
    public function getSecsRing()
    {
        return $this->secsRing;
    }

    /**
     * Set secsWait
     *
     * @param integer $secsWait
     * @return WinAgent
     */
    public function setSecsWait($secsWait)
    {
        $this->secsWait = $secsWait;

        return $this;
    }

    /**
     * Get secsWait
     *
     * @return integer
     */
    public function getSecsWait()
    {
        return $this->secsWait;
    }

    /**
     * Set secAns
     *
     * @param integer $secAns
     * @return WinAgent
     */
    public function setSecAns($secAns)
    {
        $this->secAns = $secAns;

        return $this;
    }

    /**
     * Get secAns
     *
     * @return integer
     */
    public function getSecAns()
    {
        return $this->secAns;
    }

    /**
     * Set timesAns
     *
     * @param integer $timesAns
     * @return WinAgent
     */
    public function setTimesAns($timesAns)
    {
        $this->timesAns = $timesAns;

        return $this;
    }

    /**
     * Get timesAns
     *
     * @return integer
     */
    public function getTimesAns()
    {
        return $this->timesAns;
    }

    /**
     * Set timesCall
     *
     * @param integer $timesCall
     * @return WinAgent
     */
    public function setTimesCall($timesCall)
    {
        $this->timesCall = $timesCall;

        return $this;
    }

    /**
     * Get timesCall
     *
     * @return integer
     */
    public function getTimesCall()
    {
        return $this->timesCall;
    }

    /**
     * Set timesBusy
     *
     * @param integer $timesBusy
     * @return WinAgent
     */
    public function setTimesBusy($timesBusy)
    {
        $this->timesBusy = $timesBusy;

        return $this;
    }

    /**
     * Get timesBusy
     *
     * @return integer
     */
    public function getTimesBusy()
    {
        return $this->timesBusy;
    }

    /**
     * Set synchronousAgent
     *
     * @param boolean $synchronousAgent
     * @return WinAgent
     */
    public function setSynchronousAgent($synchronousAgent)
    {
        $this->synchronousAgent = $synchronousAgent;

        return $this;
    }

    /**
     * Get synchronousAgent
     *
     * @return boolean
     */
    public function getSynchronousAgent()
    {
        return $this->synchronousAgent;
    }

    /**
     * Set smsPhone
     *
     * @param string $smsPhone
     * @return WinAgent
     */
    public function setSmsPhone($smsPhone)
    {
        $this->smsPhone = $smsPhone;

        return $this;
    }

    /**
     * Get smsPhone
     *
     * @return string
     */
    public function getSmsPhone()
    {
        return $this->smsPhone;
    }

    /**
     * Set syncAgent
     *
     * @param boolean $syncAgent
     * @return WinAgent
     */
    public function setSyncAgent($syncAgent)
    {
        $this->syncAgent = $syncAgent;

        return $this;
    }

    /**
     * Get syncAgent
     *
     * @return boolean
     */
    public function getSyncAgent()
    {
        return $this->syncAgent;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->agPassword;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->vccCode;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * 返回标识；用来登陆的时候区分
     * @return string
     */
    public function getFlag()
    {
        return 'agent';
    }

    /**
     * @return string
     * 保证和其他两个统一
     */
    public function getAdminName()
    {
        return $this->agNum;
    }

    /**
     * @return int
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @return int
     */
    public function getActionList()
    {
        return $this->actionList;
    }

    /**
     * @param string $acitonList
     * @return $this
     */
    public function setActionList($acitonList)
    {
        $this->actionList = $acitonList;

        return $this;
    }

    /**
     * @return int
     */
    public function getRoleGrade()
    {
        return $this->agRoleName->getRoleGrade();
    }

    /**
     * @param CcCcods|null $ccods
     * @return $this
     */
    public function setCcods(CcCcods $ccods = null)
    {
        $this->ccCods = $ccods;

        return $this;
    }

    /**
     * @param CcRoles $ccRoles
     * @return $this
     */
    public function setAgRoleName(CcRoles $ccRoles)
    {
        $this->agRoleName = $ccRoles;

        return $this;
    }

    /**
     * @return CcRoles
     */
    public function getAgRoleName()
    {
        return $this->agRoleName;
    }

    /**
     * @param $oldpassword
     * @return $this
     */
    public function setOldPassword($oldpassword)
    {
        $this->oldPassword = $oldpassword;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param $agStatus
     * @return $this
     */
    public function setAgStatus($agStatus)
    {
        $this->agStatus = $agStatus;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAgStatus()
    {
        return $this->agStatus;
    }

    /**
     * 统一方法
     * @return int
     */
    public function getUserId()
    {
        return $this->id;
    }

    /**
     * 获取通信IP
     * @return string
     */
    public function getWinIp()
    {
        return $this->ccCods->getWinIp();
    }

    /**
     * @return string
     */
    public function getUserNum()
    {
        return $this->agNum;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize(array(
            $this->vccId,
            $this->vccCode,
            $this->agNum,
            $this->id,
            $this->agName,
            $this->userType,
            $this->loginType,
            $this->userQueues,
            $this->agPassword, //必须要存在session中；
        ));
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        list (
            $this->vccId,
            $this->vccCode,
            $this->agNum,
            $this->id,
            $this->agName,
            $this->userType,
            $this->loginType,
            $this->userQueues,
            $this->agPassword,
            ) = unserialize($serialized);
    }
}
