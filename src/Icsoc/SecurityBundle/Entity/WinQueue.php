<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WinQueue
 */
class WinQueue
{
    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var integer
     */
    private $groupId;

    /**
     * @var string
     */
    private $queName;

    /**
     * @var string
     */
    private $queTag;

    /**
     * @var boolean
     */
    private $queType;

    /**
     * @var boolean
     */
    private $brecord;

    /**
     * @var boolean
     */
    private $queStrategy;

    /**
     * @var boolean
     */
    private $overflowStrategy;

    /**
     * @var boolean
     */
    private $queLength;

    /**
     * @var integer
     */
    private $queTime;

    /**
     * @var integer
     */
    private $ringTime;

    /**
     * @var integer
     */
    private $nextWait;

    /**
     * @var boolean
     */
    private $bAnnounce;

    /**
     * @var integer
     */
    private $noansTimes;

    /**
     * @var integer
     */
    private $noansWait;

    /**
     * @var boolean
     */
    private $noansAction;

    /**
     * @var string
     */
    private $waitAudio;

    /**
     * @var integer
     */
    private $tellevel;

    /**
     * @var boolean
     */
    private $isDel;

    /**
     * @var boolean
     */
    private $synchronousQueue;

    /**
     * @var boolean
     */
    private $syncQue;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $queNum;

    /**
     * @var integer
     */
    private $quePriority;

    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return WinQueue
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set groupId
     *
     * @param integer $groupId
     * @return WinQueue
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return integer 
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set queName
     *
     * @param string $queName
     * @return WinQueue
     */
    public function setQueName($queName)
    {
        $this->queName = $queName;

        return $this;
    }

    /**
     * Get queNum
     *
     * @return string 
     */
    public function getQueNum()
    {
        return $this->queNum;
    }

    /**
     * Set queNum
     *
     * @param string $queNum
     * @return WinQueue
     */
    public function setQueNum($queNum)
    {
        $this->queNum = $queNum;

        return $this;
    }

    /**
     * Get queName
     *
     * @return string
     */
    public function getQueName()
    {
        return $this->queName;
    }

    /**
     * Set queTag
     *
     * @param string $queTag
     * @return WinQueue
     */
    public function setQueTag($queTag)
    {
        $this->queTag = $queTag;

        return $this;
    }

    /**
     * Get queTag
     *
     * @return string 
     */
    public function getQueTag()
    {
        return $this->queTag;
    }

    /**
     * Set queType
     *
     * @param boolean $queType
     * @return WinQueue
     */
    public function setQueType($queType)
    {
        $this->queType = $queType;

        return $this;
    }

    /**
     * Get queType
     *
     * @return boolean 
     */
    public function getQueType()
    {
        return $this->queType;
    }

    /**
     * Set brecord
     *
     * @param boolean $brecord
     * @return WinQueue
     */
    public function setBrecord($brecord)
    {
        $this->brecord = $brecord;

        return $this;
    }

    /**
     * Get brecord
     *
     * @return boolean 
     */
    public function getBrecord()
    {
        return $this->brecord;
    }

    /**
     * Set queStrategy
     *
     * @param boolean $queStrategy
     * @return WinQueue
     */
    public function setQueStrategy($queStrategy)
    {
        $this->queStrategy = $queStrategy;

        return $this;
    }

    /**
     * Get queStrategy
     *
     * @return boolean 
     */
    public function getQueStrategy()
    {
        return $this->queStrategy;
    }

    /**
     * Set overflowStrategy
     *
     * @param boolean $overflowStrategy
     * @return WinQueue
     */
    public function setOverflowStrategy($overflowStrategy)
    {
        $this->overflowStrategy = $overflowStrategy;

        return $this;
    }

    /**
     * Get overflowStrategy
     *
     * @return boolean 
     */
    public function getOverflowStrategy()
    {
        return $this->overflowStrategy;
    }

    /**
     * Set queLength
     *
     * @param boolean $queLength
     * @return WinQueue
     */
    public function setQueLength($queLength)
    {
        $this->queLength = $queLength;

        return $this;
    }

    /**
     * Get queLength
     *
     * @return boolean 
     */
    public function getQueLength()
    {
        return $this->queLength;
    }

    /**
     * Set queTime
     *
     * @param integer $queTime
     * @return WinQueue
     */
    public function setQueTime($queTime)
    {
        $this->queTime = $queTime;

        return $this;
    }

    /**
     * Get queTime
     *
     * @return integer 
     */
    public function getQueTime()
    {
        return $this->queTime;
    }

    /**
     * Set ringTime
     *
     * @param integer $ringTime
     * @return WinQueue
     */
    public function setRingTime($ringTime)
    {
        $this->ringTime = $ringTime;

        return $this;
    }

    /**
     * Get ringTime
     *
     * @return integer 
     */
    public function getRingTime()
    {
        return $this->ringTime;
    }

    /**
     * Set nextWait
     *
     * @param integer $nextWait
     * @return WinQueue
     */
    public function setNextWait($nextWait)
    {
        $this->nextWait = $nextWait;

        return $this;
    }

    /**
     * Get nextWait
     *
     * @return integer 
     */
    public function getNextWait()
    {
        return $this->nextWait;
    }

    /**
     * Set bAnnounce
     *
     * @param boolean $bAnnounce
     * @return WinQueue
     */
    public function setBAnnounce($bAnnounce)
    {
        $this->bAnnounce = $bAnnounce;

        return $this;
    }

    /**
     * Get bAnnounce
     *
     * @return boolean 
     */
    public function getBAnnounce()
    {
        return $this->bAnnounce;
    }

    /**
     * Set noansTimes
     *
     * @param integer $noansTimes
     * @return WinQueue
     */
    public function setNoansTimes($noansTimes)
    {
        $this->noansTimes = $noansTimes;

        return $this;
    }

    /**
     * Get noansTimes
     *
     * @return integer 
     */
    public function getNoansTimes()
    {
        return $this->noansTimes;
    }

    /**
     * Set noansWait
     *
     * @param integer $noansWait
     * @return WinQueue
     */
    public function setNoansWait($noansWait)
    {
        $this->noansWait = $noansWait;

        return $this;
    }

    /**
     * Get noansWait
     *
     * @return integer 
     */
    public function getNoansWait()
    {
        return $this->noansWait;
    }

    /**
     * Set noansAction
     *
     * @param boolean $noansAction
     * @return WinQueue
     */
    public function setNoansAction($noansAction)
    {
        $this->noansAction = $noansAction;

        return $this;
    }

    /**
     * Get noansAction
     *
     * @return boolean 
     */
    public function getNoansAction()
    {
        return $this->noansAction;
    }

    /**
     * Set waitAudio
     *
     * @param string $waitAudio
     * @return WinQueue
     */
    public function setWaitAudio($waitAudio)
    {
        $this->waitAudio = $waitAudio;

        return $this;
    }

    /**
     * Get waitAudio
     *
     * @return string 
     */
    public function getWaitAudio()
    {
        return $this->waitAudio;
    }

    /**
     * Set tellevel
     *
     * @param integer $tellevel
     * @return WinQueue
     */
    public function setTellevel($tellevel)
    {
        $this->tellevel = $tellevel;

        return $this;
    }

    /**
     * Get tellevel
     *
     * @return integer 
     */
    public function getTellevel()
    {
        return $this->tellevel;
    }

    /**
     * Set isDel
     *
     * @param boolean $isDel
     * @return WinQueue
     */
    public function setIsDel($isDel)
    {
        $this->isDel = $isDel;

        return $this;
    }

    /**
     * Get isDel
     *
     * @return boolean 
     */
    public function getIsDel()
    {
        return $this->isDel;
    }

    /**
     * Set synchronousQueue
     *
     * @param boolean $synchronousQueue
     * @return WinQueue
     */
    public function setSynchronousQueue($synchronousQueue)
    {
        $this->synchronousQueue = $synchronousQueue;

        return $this;
    }

    /**
     * Get synchronousQueue
     *
     * @return boolean 
     */
    public function getSynchronousQueue()
    {
        return $this->synchronousQueue;
    }

    /**
     * Set syncQue
     *
     * @param boolean $syncQue
     * @return WinQueue
     */
    public function setSyncQue($syncQue)
    {
        $this->syncQue = $syncQue;

        return $this;
    }

    /**
     * Get syncQue
     *
     * @return boolean 
     */
    public function getSyncQue()
    {
        return $this->syncQue;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $quePriority
     * @return $this
     */
    public function setQuePriority($quePriority)
    {
        $this->quePriority = $quePriority;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuePriority()
    {
        return $this->quePriority;
    }
}
