<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WinIvr
 */
class WinIvr
{
    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var string
     */
    private $vccCode;

    /**
     * @var string
     */
    private $ivrName;

    /**
     * @var string
     */
    private $ivrCode;

    /**
     * @var string
     */
    private $ivrContent;

    /**
     * @var string
     */
    private $ivrInfo;

    /**
     * @var string
     */
    private $remark;

    /**
     * @var integer
     */
    private $addTime;

    /**
     * @var integer
     */
    private $updateTime;

    /**
     * @var boolean
     */
    private $ifRefresh;

    /**
     * @var integer
     */
    private $ivrId;


    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return WinIvr
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set vccCode
     *
     * @param string $vccCode
     * @return WinIvr
     */
    public function setVccCode($vccCode)
    {
        $this->vccCode = $vccCode;

        return $this;
    }

    /**
     * Get vccCode
     *
     * @return string 
     */
    public function getVccCode()
    {
        return $this->vccCode;
    }

    /**
     * Set ivrName
     *
     * @param string $ivrName
     * @return WinIvr
     */
    public function setIvrName($ivrName)
    {
        $this->ivrName = $ivrName;

        return $this;
    }

    /**
     * Get ivrName
     *
     * @return string 
     */
    public function getIvrName()
    {
        return $this->ivrName;
    }

    /**
     * Set ivrCode
     *
     * @param string $ivrCode
     * @return WinIvr
     */
    public function setIvrCode($ivrCode)
    {
        $this->ivrCode = $ivrCode;

        return $this;
    }

    /**
     * Get ivrCode
     *
     * @return string 
     */
    public function getIvrCode()
    {
        return $this->ivrCode;
    }

    /**
     * Set ivrContent
     *
     * @param string $ivrContent
     * @return WinIvr
     */
    public function setIvrContent($ivrContent)
    {
        $this->ivrContent = $ivrContent;

        return $this;
    }

    /**
     * Get ivrContent
     *
     * @return string 
     */
    public function getIvrContent()
    {
        return $this->ivrContent;
    }

    /**
     * Set ivrInfo
     *
     * @param string $ivrInfo
     * @return WinIvr
     */
    public function setIvrInfo($ivrInfo)
    {
        $this->ivrInfo = $ivrInfo;

        return $this;
    }

    /**
     * Get ivrInfo
     *
     * @return string 
     */
    public function getIvrInfo()
    {
        return $this->ivrInfo;
    }

    /**
     * Set remark
     *
     * @param string $remark
     * @return WinIvr
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark
     *
     * @return string 
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set addTime
     *
     * @param integer $addTime
     * @return WinIvr
     */
    public function setAddTime($addTime)
    {
        $this->addTime = $addTime;

        return $this;
    }

    /**
     * Get addTime
     *
     * @return integer 
     */
    public function getAddTime()
    {
        return $this->addTime;
    }

    /**
     * Set updateTime
     *
     * @param integer $updateTime
     * @return WinIvr
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;

        return $this;
    }

    /**
     * Get updateTime
     *
     * @return integer 
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * Set ifRefresh
     *
     * @param boolean $ifRefresh
     * @return WinIvr
     */
    public function setIfRefresh($ifRefresh)
    {
        $this->ifRefresh = $ifRefresh;

        return $this;
    }

    /**
     * Get ifRefresh
     *
     * @return boolean 
     */
    public function getIfRefresh()
    {
        return $this->ifRefresh;
    }

    /**
     * Get ivrId
     *
     * @return integer 
     */
    public function getIvrId()
    {
        return $this->ivrId;
    }
}
