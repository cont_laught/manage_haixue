<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WinSounds
 */
class WinSounds
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $remark;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $soundsAddress;

    /**
     * @var \DateTime
     */
    private $addTime;

    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var string
     */
    private $vccCode;

    /**
     * @var integer
     */
    private $superiorAgent;

    /**
     * @var boolean
     */
    private $verifyStatus;

    /**
     * @var integer
     */
    private $verifyTime;

    /**
     * @var integer
     */
    private $groupId;

    /**
     * @var integer
     */
    private $soundType;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set name
     *
     * @param string $name
     * @return WinSounds
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set remark
     *
     * @param string $remark
     * @return WinSounds
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark
     *
     * @return string 
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return WinSounds
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set soundsAddress
     *
     * @param string $soundsAddress
     * @return WinSounds
     */
    public function setSoundsAddress($soundsAddress)
    {
        $this->soundsAddress = $soundsAddress;

        return $this;
    }

    /**
     * Get soundsAddress
     *
     * @return string 
     */
    public function getSoundsAddress()
    {
        return $this->soundsAddress;
    }

    /**
     * Set addTime
     *
     * @param \DateTime $addTime
     * @return WinSounds
     */
    public function setAddTime($addTime)
    {
        $this->addTime = $addTime;

        return $this;
    }

    /**
     * Get addTime
     *
     * @return \DateTime 
     */
    public function getAddTime()
    {
        return $this->addTime;
    }

    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return WinSounds
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set vccCode
     *
     * @param string $vccCode
     * @return WinSounds
     */
    public function setVccCode($vccCode)
    {
        $this->vccCode = $vccCode;

        return $this;
    }

    /**
     * Get vccCode
     *
     * @return string 
     */
    public function getVccCode()
    {
        return $this->vccCode;
    }

    /**
     * Set superiorAgent
     *
     * @param integer $superiorAgent
     * @return WinSounds
     */
    public function setSuperiorAgent($superiorAgent)
    {
        $this->superiorAgent = $superiorAgent;

        return $this;
    }

    /**
     * Get superiorAgent
     *
     * @return integer 
     */
    public function getSuperiorAgent()
    {
        return $this->superiorAgent;
    }

    /**
     * Set verifyStatus
     *
     * @param boolean $verifyStatus
     * @return WinSounds
     */
    public function setVerifyStatus($verifyStatus)
    {
        $this->verifyStatus = $verifyStatus;

        return $this;
    }

    /**
     * Get verifyStatus
     *
     * @return boolean 
     */
    public function getVerifyStatus()
    {
        return $this->verifyStatus;
    }

    /**
     * Set verifyTime
     *
     * @param integer $verifyTime
     * @return WinSounds
     */
    public function setVerifyTime($verifyTime)
    {
        $this->verifyTime = $verifyTime;

        return $this;
    }

    /**
     * Get verifyTime
     *
     * @return integer 
     */
    public function getVerifyTime()
    {
        return $this->verifyTime;
    }

    /**
     * Set groupId
     *
     * @param integer $groupId
     * @return WinSounds
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return integer 
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get soundType
     *
     * @return integer
     */
    public function getSoundType()
    {
        return $this->getSoundType;
    }


    /**
     * Set soundType
     *
     * @param integer getSoundType
     * @return WinSounds
     */
    public function setSoundType($soundType)
    {
        $this->soundType = $soundType;

        return $this;
    }
}
