<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * CcCcods
 */
class CcCcods implements UserInterface, \Serializable
{
    /**
     * @var string
     */
    private $vccCode;

    /**
     * @var string
     */
    private $adminName;

    /**
     * @var string
     */
    private $adminPassword;

    /**
     * @var string
     */
    private $remotePassword;

    /**
     * @var string
     */
    private $authKey;

    /**
     * @var string
     */
    private $ipLimit;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $superiorAgent;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var boolean
     */
    private $ifBind;

    /**
     * @var string
     */
    private $phone400;

    /**
     * @var string
     */
    private $companyName;

    /**
     * @var string
     */
    private $callerNum;

    /**
     * @var string
     */
    private $systemName;

    /**
     * @var string
     */
    private $systemLogo;

    /**
     * @var boolean
     */
    private $isDefault;

    /**
     * @var string
     */
    private $evaluateSound;

    /**
     * @var string
     */
    private $systemSound;

    /**
     * @var boolean
     */
    private $ifOut;

    /**
     * @var string
     */
    private $userAccount;

    /**
     * @var boolean
     */
    private $groups;

    /**
     * @var integer
     */
    private $agents;

    /**
     * @var boolean
     */
    private $agentType;

    /**
     * @var string
     */
    private $outFlat;

    /**
     * @var integer
     */
    private $agentLogins;

    /**
     * @var string
     */
    private $agentFee;

    /**
     * @var integer
     */
    private $trunkIn;

    /**
     * @var integer
     */
    private $trunkOut;

    /**
     * @var string
     */
    private $trunkFee;

    /**
     * @var boolean
     */
    private $phoneNums;

    /**
     * @var string
     */
    private $phoneFee;

    /**
     * @var integer
     */
    private $ivrLines;

    /**
     * @var string
     */
    private $balance;

    /**
     * @var string
     */
    private $overdraft;

    /**
     * @var string
     */
    private $alarmPhone;

    /**
     * @var boolean
     */
    private $ifAlarm;

    /**
     * @var integer
     */
    private $level;

    /**
     * @var integer
     */
    private $endsmsType;

    /**
     * @var string
     */
    private $endsmsPhone;

    /**
     * @var \DateTime
     */
    private $addTime;

    /**
     * @var integer
     */
    private $recordLines;

    /**
     * @var string
     */
    private $recordFee;

    /**
     * @var string
     */
    private $ivrFee;

    /**
     * @var boolean
     */
    private $feeType;

    /**
     * @var \DateTime
     */
    private $openDate;

    /**
     * @var \DateTime
     */
    private $dueDate;

    /**
     * @var string
     */
    private $remark;

    /**
     * @var string
     */
    private $indexPage;

    /**
     * @var string
     */
    private $inpopPage;

    /**
     * @var string
     */
    private $outpopPage;

    /**
     * @var string
     */
    private $actionList;

    /**
     * @var string
     */
    private $winIp;

    /**
     * @var string
     */
    private $smsRate;

    /**
     * @var boolean
     */
    private $smsChannel;

    /**
     * @var boolean
     */
    private $synchronousCall;

    /**
     * @var boolean
     */
    private $synchronousSms;

    /**
     * @var boolean
     */
    private $synchronousAgent;

    /**
     * @var boolean
     */
    private $synchronousQueue;

    /**
     * @var boolean
     */
    private $synchronousLostcall;

    /**
     * @var string
     */
    private $dbMainIp;

    /**
     * @var string
     */
    private $dbSlaveIp;

    /**
     * @var string
     */
    private $dbName;

    /**
     * @var string
     */
    private $dbUser;

    /**
     * @var string
     */
    private $dbPassword;

    /**
     * @var string
     */
    private $dbSystem;

    /**
     * @var string
     */
    private $roleAction;

    /**
     * @var boolean
     */
    private $syncCall;

    /**
     * @var boolean
     */
    private $syncSms;

    /**
     * @var boolean
     */
    private $syncAgent;

    /**
     * @var boolean
     */
    private $syncQue;

    /**
     * @var boolean
     */
    private $syncLostcall;

    /**
     * @var boolean
     */
    private $isEffectiveRule;

    /**
     * @var boolean
     */
    private $businessType;

    /**
     * @var integer
     */
    private $trunkLimit;

    /**
     * @var integer
     */
    private $userLimit;

    /**
     * @var integer
     */
    private $taskLimit;

    /**
     * @var float
     */
    private $rates;

    /**
     * @var float
     */
    private $functionFee;

    /**
     * @var float
     */
    private $monthlyFee;

    /**
     * @var boolean
     */
    private $isCard;

    /**
     * @var boolean
     */
    private $ifTotalRecord;

    /**
     * @var integer
     */
    private $vccId;

    /** @var integer  */
    private $userType = '';
    /** @var  String */
    private $userQueues = '';
    /** @var  integer */
    private $loginType = 1;
    /** @var  integer */
    private $roleGrade = 0;
    /** @var  string 用于修改密码 */
    private $oldPassword;

    /**
     * Set vccCode
     *
     * @param string $vccCode
     * @return CcCcods
     */
    public function setVccCode($vccCode)
    {
        $this->vccCode = $vccCode;

        return $this;
    }

    /**
     * Get vccCode
     *
     * @return string
     */
    public function getVccCode()
    {
        return $this->vccCode;
    }

    /**
     * Set adminName
     *
     * @param string $adminName
     * @return CcCcods
     */
    public function setAdminName($adminName)
    {
        $this->adminName = $adminName;

        return $this;
    }

    /**
     * Get adminName
     *
     * @return string
     */
    public function getAdminName()
    {
        return $this->adminName;
    }

    /**
     * Set adminPassword
     *
     * @param string $adminPassword
     * @return CcCcods
     */
    public function setAdminPassword($adminPassword)
    {
        $this->adminPassword = $adminPassword;

        return $this;
    }

    /**
     * Get adminPassword
     *
     * @return string
     */
    public function getAdminPassword()
    {
        return $this->adminPassword;
    }

    /**
     * Set remotePassword
     *
     * @param string $remotePassword
     * @return CcCcods
     */
    public function setRemotePassword($remotePassword)
    {
        $this->remotePassword = $remotePassword;

        return $this;
    }

    /**
     * Get remotePassword
     *
     * @return string
     */
    public function getRemotePassword()
    {
        return $this->remotePassword;
    }

    /**
     * Set authKey
     *
     * @param string $authKey
     * @return CcCcods
     */
    public function setAuthKey($authKey)
    {
        $this->authKey = $authKey;

        return $this;
    }

    /**
     * Get authKey
     *
     * @return string
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * Set ipLimit
     *
     * @param string $ipLimit
     * @return CcCcods
     */
    public function setIpLimit($ipLimit)
    {
        $this->ipLimit = $ipLimit;

        return $this;
    }

    /**
     * Get ipLimit
     *
     * @return string
     */
    public function getIpLimit()
    {
        return $this->ipLimit;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return CcCcods
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return CcCcods
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set superiorAgent
     *
     * @param integer $superiorAgent
     * @return CcCcods
     */
    public function setSuperiorAgent($superiorAgent)
    {
        $this->superiorAgent = $superiorAgent;

        return $this;
    }

    /**
     * Get superiorAgent
     *
     * @return integer
     */
    public function getSuperiorAgent()
    {
        return $this->superiorAgent;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return CcCcods
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set ifBind
     *
     * @param boolean $ifBind
     * @return CcCcods
     */
    public function setIfBind($ifBind)
    {
        $this->ifBind = $ifBind;

        return $this;
    }

    /**
     * Get ifBind
     *
     * @return boolean
     */
    public function getIfBind()
    {
        return $this->ifBind;
    }

    /**
     * Set phone400
     *
     * @param string $phone400
     * @return CcCcods
     */
    public function setPhone400($phone400)
    {
        $this->phone400 = $phone400;

        return $this;
    }

    /**
     * Get phone400
     *
     * @return string
     */
    public function getPhone400()
    {
        return $this->phone400;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return CcCcods
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set callerNum
     *
     * @param string $callerNum
     * @return CcCcods
     */
    public function setCallerNum($callerNum)
    {
        $this->callerNum = $callerNum;

        return $this;
    }

    /**
     * Get callerNum
     *
     * @return string
     */
    public function getCallerNum()
    {
        return $this->callerNum;
    }

    /**
     * Set systemName
     *
     * @param string $systemName
     * @return CcCcods
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * Get systemName
     *
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * Set systemLogo
     *
     * @param string $systemLogo
     * @return CcCcods
     */
    public function setSystemLogo($systemLogo)
    {
        $this->systemLogo = $systemLogo;

        return $this;
    }

    /**
     * Get systemLogo
     *
     * @return string
     */
    public function getSystemLogo()
    {
        return $this->systemLogo;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     * @return CcCcods
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set evaluateSound
     *
     * @param string $evaluateSound
     * @return CcCcods
     */
    public function setEvaluateSound($evaluateSound)
    {
        $this->evaluateSound = $evaluateSound;

        return $this;
    }

    /**
     * Get evaluateSound
     *
     * @return string
     */
    public function getEvaluateSound()
    {
        return $this->evaluateSound;
    }

    /**
     * Set systemSound
     *
     * @param string $systemSound
     * @return CcCcods
     */
    public function setSystemSound($systemSound)
    {
        $this->systemSound = $systemSound;

        return $this;
    }

    /**
     * Get systemSound
     *
     * @return string
     */
    public function getSystemSound()
    {
        return $this->systemSound;
    }

    /**
     * Set ifOut
     *
     * @param boolean $ifOut
     * @return CcCcods
     */
    public function setIfOut($ifOut)
    {
        $this->ifOut = $ifOut;

        return $this;
    }

    /**
     * Get ifOut
     *
     * @return boolean
     */
    public function getIfOut()
    {
        return $this->ifOut;
    }

    /**
     * Set userAccount
     *
     * @param string $userAccount
     * @return CcCcods
     */
    public function setUserAccount($userAccount)
    {
        $this->userAccount = $userAccount;

        return $this;
    }

    /**
     * Get userAccount
     *
     * @return string
     */
    public function getUserAccount()
    {
        return $this->userAccount;
    }

    /**
     * Set groups
     *
     * @param boolean $groups
     * @return CcCcods
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;

        return $this;
    }

    /**
     * Get groups
     *
     * @return integer
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set agents
     *
     * @param integer $agents
     * @return CcCcods
     */
    public function setAgents($agents)
    {
        $this->agents = $agents;

        return $this;
    }

    /**
     * Get agents
     *
     * @return integer
     */
    public function getAgents()
    {
        return $this->agents;
    }

    /**
     * Set agentType
     *
     * @param boolean $agentType
     * @return CcCcods
     */
    public function setAgentType($agentType)
    {
        $this->agentType = $agentType;

        return $this;
    }

    /**
     * Get agentType
     *
     * @return boolean
     */
    public function getAgentType()
    {
        return $this->agentType;
    }

    /**
     * Set outFlat
     *
     * @param string $outFlat
     * @return CcCcods
     */
    public function setOutFlat($outFlat)
    {
        $this->outFlat = $outFlat;

        return $this;
    }

    /**
     * Get outFlat
     *
     * @return string
     */
    public function getOutFlat()
    {
        return $this->outFlat;
    }

    /**
     * Set agentLogins
     *
     * @param integer $agentLogins
     * @return CcCcods
     */
    public function setAgentLogins($agentLogins)
    {
        $this->agentLogins = $agentLogins;

        return $this;
    }

    /**
     * Get agentLogins
     *
     * @return integer
     */
    public function getAgentLogins()
    {
        return $this->agentLogins;
    }

    /**
     * Set agentFee
     *
     * @param string $agentFee
     * @return CcCcods
     */
    public function setAgentFee($agentFee)
    {
        $this->agentFee = $agentFee;

        return $this;
    }

    /**
     * Get agentFee
     *
     * @return string
     */
    public function getAgentFee()
    {
        return $this->agentFee;
    }

    /**
     * Set trunkIn
     *
     * @param integer $trunkIn
     * @return CcCcods
     */
    public function setTrunkIn($trunkIn)
    {
        $this->trunkIn = $trunkIn;

        return $this;
    }

    /**
     * Get trunkIn
     *
     * @return integer
     */
    public function getTrunkIn()
    {
        return $this->trunkIn;
    }

    /**
     * Set trunkOut
     *
     * @param integer $trunkOut
     * @return CcCcods
     */
    public function setTrunkOut($trunkOut)
    {
        $this->trunkOut = $trunkOut;

        return $this;
    }

    /**
     * Get trunkOut
     *
     * @return integer
     */
    public function getTrunkOut()
    {
        return $this->trunkOut;
    }

    /**
     * Set trunkFee
     *
     * @param string $trunkFee
     * @return CcCcods
     */
    public function setTrunkFee($trunkFee)
    {
        $this->trunkFee = $trunkFee;

        return $this;
    }

    /**
     * Get trunkFee
     *
     * @return string
     */
    public function getTrunkFee()
    {
        return $this->trunkFee;
    }

    /**
     * Set phoneNums
     *
     * @param boolean $phoneNums
     * @return CcCcods
     */
    public function setPhoneNums($phoneNums)
    {
        $this->phoneNums = $phoneNums;

        return $this;
    }

    /**
     * Get phoneNums
     *
     * @return boolean
     */
    public function getPhoneNums()
    {
        return $this->phoneNums;
    }

    /**
     * Set phoneFee
     *
     * @param string $phoneFee
     * @return CcCcods
     */
    public function setPhoneFee($phoneFee)
    {
        $this->phoneFee = $phoneFee;

        return $this;
    }

    /**
     * Get phoneFee
     *
     * @return string
     */
    public function getPhoneFee()
    {
        return $this->phoneFee;
    }

    /**
     * Set ivrLines
     *
     * @param integer $ivrLines
     * @return CcCcods
     */
    public function setIvrLines($ivrLines)
    {
        $this->ivrLines = $ivrLines;

        return $this;
    }

    /**
     * Get ivrLines
     *
     * @return integer
     */
    public function getIvrLines()
    {
        return $this->ivrLines;
    }

    /**
     * Set balance
     *
     * @param string $balance
     * @return CcCcods
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return string
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set overdraft
     *
     * @param string $overdraft
     * @return CcCcods
     */
    public function setOverdraft($overdraft)
    {
        $this->overdraft = $overdraft;

        return $this;
    }

    /**
     * Get overdraft
     *
     * @return string
     */
    public function getOverdraft()
    {
        return $this->overdraft;
    }

    /**
     * Set alarmPhone
     *
     * @param string $alarmPhone
     * @return CcCcods
     */
    public function setAlarmPhone($alarmPhone)
    {
        $this->alarmPhone = $alarmPhone;

        return $this;
    }

    /**
     * Get alarmPhone
     *
     * @return string
     */
    public function getAlarmPhone()
    {
        return $this->alarmPhone;
    }

    /**
     * Set ifAlarm
     *
     * @param boolean $ifAlarm
     * @return CcCcods
     */
    public function setIfAlarm($ifAlarm)
    {
        $this->ifAlarm = $ifAlarm;

        return $this;
    }

    /**
     * Get ifAlarm
     *
     * @return boolean
     */
    public function getIfAlarm()
    {
        return $this->ifAlarm;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return CcCcods
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set endsmsType
     *
     * @param integer $endsmsType
     * @return CcCcods
     */
    public function setEndsmsType($endsmsType)
    {
        $this->endsmsType = $endsmsType;

        return $this;
    }

    /**
     * Get endsmsType
     *
     * @return integer
     */
    public function getEndsmsType()
    {
        return $this->endsmsType;
    }

    /**
     * Set endsmsPhone
     *
     * @param string $endsmsPhone
     * @return CcCcods
     */
    public function setEndsmsPhone($endsmsPhone)
    {
        $this->endsmsPhone = $endsmsPhone;

        return $this;
    }

    /**
     * Get endsmsPhone
     *
     * @return string
     */
    public function getEndsmsPhone()
    {
        return $this->endsmsPhone;
    }

    /**
     * Set addTime
     *
     * @param \DateTime $addTime
     * @return CcCcods
     */
    public function setAddTime($addTime)
    {
        $this->addTime = $addTime;

        return $this;
    }

    /**
     * Get addTime
     *
     * @return \DateTime
     */
    public function getAddTime()
    {
        return $this->addTime;
    }

    /**
     * Set recordLines
     *
     * @param integer $recordLines
     * @return CcCcods
     */
    public function setRecordLines($recordLines)
    {
        $this->recordLines = $recordLines;

        return $this;
    }

    /**
     * Get recordLines
     *
     * @return integer
     */
    public function getRecordLines()
    {
        return $this->recordLines;
    }

    /**
     * Set recordFee
     *
     * @param string $recordFee
     * @return CcCcods
     */
    public function setRecordFee($recordFee)
    {
        $this->recordFee = $recordFee;

        return $this;
    }

    /**
     * Get recordFee
     *
     * @return string
     */
    public function getRecordFee()
    {
        return $this->recordFee;
    }

    /**
     * Set ivrFee
     *
     * @param string $ivrFee
     * @return CcCcods
     */
    public function setIvrFee($ivrFee)
    {
        $this->ivrFee = $ivrFee;

        return $this;
    }

    /**
     * Get ivrFee
     *
     * @return string
     */
    public function getIvrFee()
    {
        return $this->ivrFee;
    }

    /**
     * Set feeType
     *
     * @param boolean $feeType
     * @return CcCcods
     */
    public function setFeeType($feeType)
    {
        $this->feeType = $feeType;

        return $this;
    }

    /**
     * Get feeType
     *
     * @return boolean
     */
    public function getFeeType()
    {
        return $this->feeType;
    }

    /**
     * Set openDate
     *
     * @param \DateTime $openDate
     * @return CcCcods
     */
    public function setOpenDate($openDate)
    {
        $this->openDate = $openDate;

        return $this;
    }

    /**
     * Get openDate
     *
     * @return \DateTime
     */
    public function getOpenDate()
    {
        return $this->openDate;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     * @return CcCcods
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set remark
     *
     * @param string $remark
     * @return CcCcods
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark
     *
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set indexPage
     *
     * @param string $indexPage
     * @return CcCcods
     */
    public function setIndexPage($indexPage)
    {
        $this->indexPage = $indexPage;

        return $this;
    }

    /**
     * Get indexPage
     *
     * @return string
     */
    public function getIndexPage()
    {
        return $this->indexPage;
    }

    /**
     * Set inpopPage
     *
     * @param string $inpopPage
     * @return CcCcods
     */
    public function setInpopPage($inpopPage)
    {
        $this->inpopPage = $inpopPage;

        return $this;
    }

    /**
     * Get inpopPage
     *
     * @return string
     */
    public function getInpopPage()
    {
        return $this->inpopPage;
    }

    /**
     * Set outpopPage
     *
     * @param string $outpopPage
     * @return CcCcods
     */
    public function setOutpopPage($outpopPage)
    {
        $this->outpopPage = $outpopPage;

        return $this;
    }

    /**
     * Get outpopPage
     *
     * @return string
     */
    public function getOutpopPage()
    {
        return $this->outpopPage;
    }

    /**
     * Set actionList
     *
     * @param string $actionList
     * @return CcCcods
     */
    public function setActionList($actionList)
    {
        $this->actionList = $actionList;

        return $this;
    }

    /**
     * Get actionList
     *
     * @return string
     */
    public function getActionList()
    {
        return $this->actionList;
    }

    /**
     * Set winIp
     *
     * @param string $winIp
     * @return CcCcods
     */
    public function setWinIp($winIp)
    {
        $this->winIp = $winIp;

        return $this;
    }

    /**
     * Get winIp
     *
     * @return string
     */
    public function getWinIp()
    {
        return $this->winIp;
    }

    /**
     * Set smsRate
     *
     * @param string $smsRate
     * @return CcCcods
     */
    public function setSmsRate($smsRate)
    {
        $this->smsRate = $smsRate;

        return $this;
    }

    /**
     * Get smsRate
     *
     * @return string
     */
    public function getSmsRate()
    {
        return $this->smsRate;
    }

    /**
     * Set smsChannel
     *
     * @param boolean $smsChannel
     * @return CcCcods
     */
    public function setSmsChannel($smsChannel)
    {
        $this->smsChannel = $smsChannel;

        return $this;
    }

    /**
     * Get smsChannel
     *
     * @return boolean
     */
    public function getSmsChannel()
    {
        return $this->smsChannel;
    }

    /**
     * Set synchronousCall
     *
     * @param boolean $synchronousCall
     * @return CcCcods
     */
    public function setSynchronousCall($synchronousCall)
    {
        $this->synchronousCall = $synchronousCall;

        return $this;
    }

    /**
     * Get synchronousCall
     *
     * @return boolean
     */
    public function getSynchronousCall()
    {
        return $this->synchronousCall;
    }

    /**
     * Set synchronousSms
     *
     * @param boolean $synchronousSms
     * @return CcCcods
     */
    public function setSynchronousSms($synchronousSms)
    {
        $this->synchronousSms = $synchronousSms;

        return $this;
    }

    /**
     * Get synchronousSms
     *
     * @return boolean
     */
    public function getSynchronousSms()
    {
        return $this->synchronousSms;
    }

    /**
     * Set synchronousAgent
     *
     * @param boolean $synchronousAgent
     * @return CcCcods
     */
    public function setSynchronousAgent($synchronousAgent)
    {
        $this->synchronousAgent = $synchronousAgent;

        return $this;
    }

    /**
     * Get synchronousAgent
     *
     * @return boolean
     */
    public function getSynchronousAgent()
    {
        return $this->synchronousAgent;
    }

    /**
     * Set synchronousQueue
     *
     * @param boolean $synchronousQueue
     * @return CcCcods
     */
    public function setSynchronousQueue($synchronousQueue)
    {
        $this->synchronousQueue = $synchronousQueue;

        return $this;
    }

    /**
     * Get synchronousQueue
     *
     * @return boolean
     */
    public function getSynchronousQueue()
    {
        return $this->synchronousQueue;
    }

    /**
     * Set synchronousLostcall
     *
     * @param boolean $synchronousLostcall
     * @return CcCcods
     */
    public function setSynchronousLostcall($synchronousLostcall)
    {
        $this->synchronousLostcall = $synchronousLostcall;

        return $this;
    }

    /**
     * Get synchronousLostcall
     *
     * @return boolean
     */
    public function getSynchronousLostcall()
    {
        return $this->synchronousLostcall;
    }

    /**
     * Set dbMainIp
     *
     * @param string $dbMainIp
     * @return CcCcods
     */
    public function setDbMainIp($dbMainIp)
    {
        $this->dbMainIp = $dbMainIp;

        return $this;
    }

    /**
     * Get dbMainIp
     *
     * @return string
     */
    public function getDbMainIp()
    {
        return $this->dbMainIp;
    }

    /**
     * Set dbSlaveIp
     *
     * @param string $dbSlaveIp
     * @return CcCcods
     */
    public function setDbSlaveIp($dbSlaveIp)
    {
        $this->dbSlaveIp = $dbSlaveIp;

        return $this;
    }

    /**
     * Get dbSlaveIp
     *
     * @return string
     */
    public function getDbSlaveIp()
    {
        return $this->dbSlaveIp;
    }

    /**
     * Set dbName
     *
     * @param string $dbName
     * @return CcCcods
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;

        return $this;
    }

    /**
     * Get dbName
     *
     * @return string
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * Set dbUser
     *
     * @param string $dbUser
     * @return CcCcods
     */
    public function setDbUser($dbUser)
    {
        $this->dbUser = $dbUser;

        return $this;
    }

    /**
     * Get dbUser
     *
     * @return string
     */
    public function getDbUser()
    {
        return $this->dbUser;
    }

    /**
     * Set dbPassword
     *
     * @param string $dbPassword
     * @return CcCcods
     */
    public function setDbPassword($dbPassword)
    {
        $this->dbPassword = $dbPassword;

        return $this;
    }

    /**
     * Get dbPassword
     *
     * @return string
     */
    public function getDbPassword()
    {
        return $this->dbPassword;
    }

    /**
     * Set dbSystem
     *
     * @param string $dbSystem
     * @return CcCcods
     */
    public function setDbSystem($dbSystem)
    {
        $this->dbSystem = $dbSystem;

        return $this;
    }

    /**
     * Get dbSystem
     *
     * @return string
     */
    public function getDbSystem()
    {
        return $this->dbSystem;
    }

    /**
     * Set roleAction
     *
     * @param string $roleAction
     * @return CcCcods
     */
    public function setRoleAction($roleAction)
    {
        $this->roleAction = $roleAction;

        return $this;
    }

    /**
     * Get roleAction
     *
     * @return string
     */
    public function getRoleAction()
    {
        return $this->roleAction;
    }

    /**
     * Set syncCall
     *
     * @param boolean $syncCall
     * @return CcCcods
     */
    public function setSyncCall($syncCall)
    {
        $this->syncCall = $syncCall;

        return $this;
    }

    /**
     * Get syncCall
     *
     * @return boolean
     */
    public function getSyncCall()
    {
        return $this->syncCall;
    }

    /**
     * Set syncSms
     *
     * @param boolean $syncSms
     * @return CcCcods
     */
    public function setSyncSms($syncSms)
    {
        $this->syncSms = $syncSms;

        return $this;
    }

    /**
     * Get syncSms
     *
     * @return boolean
     */
    public function getSyncSms()
    {
        return $this->syncSms;
    }

    /**
     * Set syncAgent
     *
     * @param boolean $syncAgent
     * @return CcCcods
     */
    public function setSyncAgent($syncAgent)
    {
        $this->syncAgent = $syncAgent;

        return $this;
    }

    /**
     * Get syncAgent
     *
     * @return boolean
     */
    public function getSyncAgent()
    {
        return $this->syncAgent;
    }

    /**
     * Set syncQue
     *
     * @param boolean $syncQue
     * @return CcCcods
     */
    public function setSyncQue($syncQue)
    {
        $this->syncQue = $syncQue;

        return $this;
    }

    /**
     * Get syncQue
     *
     * @return boolean
     */
    public function getSyncQue()
    {
        return $this->syncQue;
    }

    /**
     * Set syncLostcall
     *
     * @param boolean $syncLostcall
     * @return CcCcods
     */
    public function setSyncLostcall($syncLostcall)
    {
        $this->syncLostcall = $syncLostcall;

        return $this;
    }

    /**
     * Get syncLostcall
     *
     * @return boolean
     */
    public function getSyncLostcall()
    {
        return $this->syncLostcall;
    }

    /**
     * Set isEffectiveRule
     *
     * @param boolean $isEffectiveRule
     * @return CcCcods
     */
    public function setIsEffectiveRule($isEffectiveRule)
    {
        $this->isEffectiveRule = $isEffectiveRule;

        return $this;
    }

    /**
     * Get isEffectiveRule
     *
     * @return boolean
     */
    public function getIsEffectiveRule()
    {
        return $this->isEffectiveRule;
    }

    /**
     * Set businessType
     *
     * @param boolean $businessType
     * @return CcCcods
     */
    public function setBusinessType($businessType)
    {
        $this->businessType = $businessType;

        return $this;
    }

    /**
     * Get businessType
     *
     * @return boolean
     */
    public function getBusinessType()
    {
        return $this->businessType;
    }

    /**
     * Set trunkLimit
     *
     * @param integer $trunkLimit
     * @return CcCcods
     */
    public function setTrunkLimit($trunkLimit)
    {
        $this->trunkLimit = $trunkLimit;

        return $this;
    }

    /**
     * Get trunkLimit
     *
     * @return integer
     */
    public function getTrunkLimit()
    {
        return $this->trunkLimit;
    }

    /**
     * Set userLimit
     *
     * @param integer $userLimit
     * @return CcCcods
     */
    public function setUserLimit($userLimit)
    {
        $this->userLimit = $userLimit;

        return $this;
    }

    /**
     * Get userLimit
     *
     * @return integer
     */
    public function getUserLimit()
    {
        return $this->userLimit;
    }

    /**
     * Set taskLimit
     *
     * @param integer $taskLimit
     * @return CcCcods
     */
    public function setTaskLimit($taskLimit)
    {
        $this->taskLimit = $taskLimit;

        return $this;
    }

    /**
     * Get taskLimit
     *
     * @return integer
     */
    public function getTaskLimit()
    {
        return $this->taskLimit;
    }

    /**
     * Set rates
     *
     * @param float $rates
     * @return CcCcods
     */
    public function setRates($rates)
    {
        $this->rates = $rates;

        return $this;
    }

    /**
     * Get rates
     *
     * @return float
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * Set functionFee
     *
     * @param float $functionFee
     * @return CcCcods
     */
    public function setFunctionFee($functionFee)
    {
        $this->functionFee = $functionFee;

        return $this;
    }

    /**
     * Get functionFee
     *
     * @return float
     */
    public function getFunctionFee()
    {
        return $this->functionFee;
    }

    /**
     * Set monthlyFee
     *
     * @param float $monthlyFee
     * @return CcCcods
     */
    public function setMonthlyFee($monthlyFee)
    {
        $this->monthlyFee = $monthlyFee;

        return $this;
    }

    /**
     * Get monthlyFee
     *
     * @return float
     */
    public function getMonthlyFee()
    {
        return $this->monthlyFee;
    }

    /**
     * Set isCard
     *
     * @param boolean $isCard
     * @return CcCcods
     */
    public function setIsCard($isCard)
    {
        $this->isCard = $isCard;

        return $this;
    }

    /**
     * Get isCard
     *
     * @return boolean
     */
    public function getIsCard()
    {
        return $this->isCard;
    }

    /**
     * Set ifTotalRecord
     *
     * @param boolean $ifTotalRecord
     * @return CcCcods
     */
    public function setIfTotalRecord($ifTotalRecord)
    {
        $this->ifTotalRecord = $ifTotalRecord;

        return $this;
    }

    /**
     * Get ifTotalRecord
     *
     * @return boolean
     */
    public function getIfTotalRecord()
    {
        return $this->ifTotalRecord;
    }

    /**
     * Get vccId
     *
     * @return integer
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * @param int $vccId
     * @return $this
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->adminPassword;
        // TODO: Implement getPassword() method.
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->vccCode; //把vcc_code 当成用户名；
        // TODO: Implement getUsername() method.
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return string
     */
    public function getFlag()
    {
        return "ccod"; //用来标识是从那个表来登陆上的；
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->vccId;
    }

    /**
     * @param string $oldpassword
     * @return $this
     */
    public function setOldPassword($oldpassword)
    {
        $this->oldPassword = $oldpassword;

        return $this;
    }

    /**
     * @return string
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * Get userType
     *
     * @return boolean
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @return String
     */
    public function getUserQueues()
    {
        return $this->userQueues;
    }

    /**
     * @return int
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @return int
     */
    public function getRoleGrade()
    {
        return $this->roleGrade;
    }

    /**
     * @return string
     */
    public function getUserNum()
    {
        return $this->adminName;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize(array(
            $this->vccId,
            $this->vccCode,
            $this->winIp,
            $this->adminName,
            $this->userId,
            $this->actionList,
            $this->userType,
            $this->userQueues,
            $this->loginType,
            $this->roleGrade,
            $this->adminPassword,
        ));
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        list (
            $this->vccId,
            $this->vccCode,
            $this->winIp,
            $this->adminName,
            $this->userId,
            $this->actionList,
            $this->userType,
            $this->userQueues,
            $this->loginType,
            $this->roleGrade,
            $this->adminPassword,
            ) = unserialize($serialized);
    }
}
