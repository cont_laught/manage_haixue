<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CcMonitorConfig
 */
class CcMonitorConfig
{
    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var string
     */
    private $monitorQueues;

    /**
     * @var string
     */
    private $separateQueues;

    /**
     * @var boolean
     */
    private $ifMonitorIvrInfo;

    /**
     * @var string
     */
    private $trunkPhones;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return CcMonitorConfig
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return CcMonitorConfig
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set monitorQueues
     *
     * @param string $monitorQueues
     * @return CcMonitorConfig
     */
    public function setMonitorQueues($monitorQueues)
    {
        $this->monitorQueues = $monitorQueues;

        return $this;
    }

    /**
     * Get monitorQueues
     *
     * @return string 
     */
    public function getMonitorQueues()
    {
        return $this->monitorQueues;
    }

    /**
     * Set separateQueues
     *
     * @param string $separateQueues
     * @return CcMonitorConfig
     */
    public function setSeparateQueues($separateQueues)
    {
        $this->separateQueues = $separateQueues;

        return $this;
    }

    /**
     * Get separateQueues
     *
     * @return string 
     */
    public function getSeparateQueues()
    {
        return $this->separateQueues;
    }

    /**
     * Set ifMonitorIvrInfo
     *
     * @param boolean $ifMonitorIvrInfo
     * @return CcMonitorConfig
     */
    public function setIfMonitorIvrInfo($ifMonitorIvrInfo)
    {
        $this->ifMonitorIvrInfo = $ifMonitorIvrInfo;

        return $this;
    }

    /**
     * Get ifMonitorIvrInfo
     *
     * @return boolean 
     */
    public function getIfMonitorIvrInfo()
    {
        return $this->ifMonitorIvrInfo;
    }

    /**
     * Set trunkPhones
     *
     * @param string $trunkPhones
     * @return CcMonitorConfig
     */
    public function setTrunkPhones($trunkPhones)
    {
        $this->trunkPhones = $trunkPhones;

        return $this;
    }

    /**
     * Get trunkPhones
     *
     * @return string 
     */
    public function getTrunkPhones()
    {
        return $this->trunkPhones;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
