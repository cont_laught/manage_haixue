<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CcCcodConfigs
 */
class CcCcodConfigs
{
    /**
     * @var integer
     */
    private $vccId;

    /**
     * @var boolean
     */
    private $isEnableBlack;

    /**
     * @var boolean
     */
    private $isIncomingEnableBlack;

    /**
     * @var boolean
     */
    private $isEnableWhite;

    /**
     * @var boolean
     */
    private $whiteType;

    /**
     * @var string
     */
    private $whiteSound = '';

    /**
     * @var string
     */
    private $whiteSoundAddress = '';

    /**
     * @var boolean
     */
    private $isEnableGroup = '';

    /**
     * @var string
     */
    private $connNum = '';

    /**
     * @var string
     */
    private $lostNum = '';

    /**
     * @var boolean
     */
    private $workHour = '';

    /**
     * @var boolean
     */
    private $workDay = '';

    /**
     * @var integer
     */
    private $id;

    /** @var  CcCcods */
    private $toolBarOptions;

    /** @var  SwjHangupSms */
    private $smsOptions;

    /**
     * @var boolean
     */
    private $toolBar;

    /**
     * @var boolean
     */
    private $statistical;

    /**
     * @var boolean
     */
    private $isEnableEvaluate;

    /**
     * Set vccId
     *
     * @param integer $vccId
     * @return CcCcodConfigs
     */
    public function setVccId($vccId)
    {
        $this->vccId = $vccId;

        return $this;
    }

    /**
     * Get vccId
     *
     * @return integer 
     */
    public function getVccId()
    {
        return $this->vccId;
    }

    /**
     * @param $isEnableBlack
     * @return $this
     */
    public function setIsEnableBlack($isEnableBlack)
    {
        $this->isEnableBlack = $isEnableBlack;

        return $this;
    }

    /**
     * Get isExhaledEnableBlack
     *
     * @return boolean 
     */
    public function getIsEnableBlack()
    {
        return $this->isEnableBlack;
    }

    /**
     * Set isIncomingEnableBlack
     *
     * @param boolean $isIncomingEnableBlack
     * @return CcCcodConfigs
     */
    public function setIsIncomingEnableBlack($isIncomingEnableBlack)
    {
        $this->isIncomingEnableBlack = $isIncomingEnableBlack;

        return $this;
    }

    /**
     * Get isIncomingEnableBlack
     *
     * @return boolean 
     */
    public function getIsIncomingEnableBlack()
    {
        return $this->isIncomingEnableBlack;
    }

    /**
     * Set isEnableWhite
     *
     * @param boolean $isEnableWhite
     * @return CcCcodConfigs
     */
    public function setIsEnableWhite($isEnableWhite)
    {
        $this->isEnableWhite = $isEnableWhite;

        return $this;
    }

    /**
     * Get isEnableWhite
     *
     * @return boolean 
     */
    public function getIsEnableWhite()
    {
        return $this->isEnableWhite;
    }

    /**
     * Set whiteType
     *
     * @param boolean $whiteType
     * @return CcCcodConfigs
     */
    public function setWhiteType($whiteType)
    {
        $this->whiteType = $whiteType;

        return $this;
    }

    /**
     * Get whiteType
     *
     * @return boolean 
     */
    public function getWhiteType()
    {
        return $this->whiteType;
    }

    /**
     * Set whiteSound
     *
     * @param string $whiteSound
     * @return CcCcodConfigs
     */
    public function setWhiteSound($whiteSound)
    {
        $this->whiteSound = $whiteSound;

        return $this;
    }

    /**
     * Get whiteSound
     *
     * @return string 
     */
    public function getWhiteSound()
    {
        return $this->whiteSound;
    }

    /**
     * Set whiteSoundAddress
     *
     * @param string $whiteSoundAddress
     * @return CcCcodConfigs
     */
    public function setWhiteSoundAddress($whiteSoundAddress)
    {
        $this->whiteSoundAddress = $whiteSoundAddress;

        return $this;
    }

    /**
     * Get whiteSoundAddress
     *
     * @return string 
     */
    public function getWhiteSoundAddress()
    {
        return $this->whiteSoundAddress;
    }

    /**
     * Set isEnableGroup
     *
     * @param boolean $isEnableGroup
     * @return CcCcodConfigs
     */
    public function setIsEnableGroup($isEnableGroup)
    {
        $this->isEnableGroup = $isEnableGroup;

        return $this;
    }

    /**
     * Get isEnableGroup
     *
     * @return boolean 
     */
    public function getIsEnableGroup()
    {
        return $this->isEnableGroup;
    }

    /**
     * Set connNum
     *
     * @param string $connNum
     * @return CcCcodConfigs
     */
    public function setConnNum($connNum)
    {
        $this->connNum = $connNum;

        return $this;
    }

    /**
     * Get connNum
     *
     * @return string 
     */
    public function getConnNum()
    {
        return $this->connNum;
    }

    /**
     * Set lostNum
     *
     * @param string $lostNum
     * @return CcCcodConfigs
     */
    public function setLostNum($lostNum)
    {
        $this->lostNum = $lostNum;

        return $this;
    }

    /**
     * Get lostNum
     *
     * @return string 
     */
    public function getLostNum()
    {
        return $this->lostNum;
    }

    /**
     * Set workHour
     *
     * @param boolean $workHour
     * @return CcCcodConfigs
     */
    public function setWorkHour($workHour)
    {
        $this->workHour = $workHour;

        return $this;
    }

    /**
     * Get workHour
     *
     * @return boolean 
     */
    public function getWorkHour()
    {
        return $this->workHour;
    }

    /**
     * Set workDay
     *
     * @param boolean $workDay
     * @return CcCcodConfigs
     */
    public function setWorkDay($workDay)
    {
        $this->workDay = $workDay;

        return $this;
    }

    /**
     * Get workDay
     *
     * @return boolean 
     */
    public function getWorkDay()
    {
        return $this->workDay;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param CcCcods $ccCcods
     * @return $this
     */
    public function setToolBarOptions(CcCcods $ccCcods)
    {
        $this->toolBarOptions = $ccCcods;
        return $this;
    }

    /**
     * @return CcCcods
     */
    public function getToolBarOptions()
    {
        return $this->toolBarOptions;
    }

    /**
     * @param SwjHangupSms $swjHangupSms
     * @return $this
     */
    public function setSmsOptions(SwjHangupSms $swjHangupSms)
    {
        $this->smsOptions = $swjHangupSms;
        return $this;
    }

    /**
     * @return SwjHangupSms
     */
    public function getSmsOptions()
    {
        return $this->smsOptions;
    }

    /**
     * @param $tollBar
     * @return $this
     */
    public function setToolBar($tollBar)
    {
        $this->toolBar = $tollBar;

        return $this;
    }

    /**
     * @return bool
     */
    public function getToolBar()
    {
        return $this->toolBar;
    }

    /**
     * @param $statistical
     * @return $this
     */
    public function setStatistical($statistical)
    {
        $this->statistical = $statistical;

        return $this;
    }

    /**
     * @return bool
     */
    public function getStatistical()
    {
        return $this->statistical;
    }

    /**
     * @param $isEnableEvaluate
     * @return $this
     */
    public function setIsEnableEvaluate($isEnableEvaluate)
    {
        $this->isEnableEvaluate = $isEnableEvaluate;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsEnableEvaluate()
    {
        return $this->isEnableEvaluate;
    }
}
