<?php

namespace Icsoc\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserMenu
 */
class UserMenu
{
    /**
     * @var integer
     */
    private $menuId;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var boolean
     */
    private $userFlag;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set menuId
     *
     * @param integer $menuId
     * @return UserMenu
     */
    public function setMenuId($menuId)
    {
        $this->menuId = $menuId;

        return $this;
    }

    /**
     * Get menuId
     *
     * @return integer 
     */
    public function getMenuId()
    {
        return $this->menuId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return UserMenu
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set userFlag
     *
     * @param boolean $userFlag
     * @return UserMenu
     */
    public function setUserFlag($userFlag)
    {
        $this->userFlag = $userFlag;

        return $this;
    }

    /**
     * Get userFlag
     *
     * @return boolean 
     */
    public function getUserFlag()
    {
        return $this->userFlag;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
