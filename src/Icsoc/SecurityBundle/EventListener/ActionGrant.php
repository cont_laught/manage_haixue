<?php
namespace Icsoc\SecurityBundle\EventListener;

use Icsoc\CoreBundle\Controller\AuthenticatedInteface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * 权限检测
 * Class ActionGrant
 *
 * @package Icsoc\SecurityBundle\EventListener
 */
class ActionGrant
{
    /** @var array 不检查权限的路由 */
    private $allow = array(
        'icsoc_ui_homepage',
        'system_home_dashboard',
        'system_user_menu', //自定义菜单
        'system_user_del_menu', //删除自定义菜单
        'icsoc_security_user_update_password', //修改密码
        'icsoc_security_check_pass', //修改密码检查密码；
        'system_user_down_help', //下载帮助文档
        'system_user_set_lang', //语言切换
        'icsoc_notice_select', //查看权限
        'icsoc_security_develop_download_work_index', //下载任务管理
        'icsoc_security_develop_download_work_data', //下载任务管理
        'icsoc_security_download_upload_data', //下载任务管理
        'icsoc_report_getAgents', //因为多个报表需要这个路由来获取坐席；没法控制
        'icsoc_report_charts_getDataForChosenSelect',//小时进线图表和呼入地区分析图表公用方法
    );

    /** @var array  不是菜单需要检查权限的 */
    private $notMenu = array(
        'icsoc_agent_add',
        'icsoc_agent_edit',
        'icsoc_agent_del',
        'icsoc_queue_batch',
        'icsoc_queue_agqu_index',
        'icsoc_recording_list_batch_down_data',
    );

    /** @var array 三级路由控制，权限一开始设计有问题 */
    private $threeLevel = array(
        'icsoc_namelist', //黑白名单
        'icsoc_monitor', //监控
        'icsoc_recording', //录音列表
        'icsoc_report', //报表
        'icsoc_security', //角色，设置
    );

    /** @var \Symfony\Component\DependencyInjection\Container */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FilterControllerEvent $event
     *
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        if (!is_array($controller)) {
            return;
        }
        if ($controller[0] instanceof AuthenticatedInteface) {
            $request = $event->getRequest();
            /** @var \Symfony\Bundle\FrameworkBundle\Controller\Controller $cobject */
            $cobject = $controller[0];
            $user = $cobject->getUser();
            $actionList = $user->getActionList();
            if ($actionList != 'all') {
                $action = explode(",", $actionList);
                $allowRoute = array();
                foreach ($action as $v) {
                    $res = explode('_', $v);
                    if (count($res) > 2) {
                        $roleStr = $res[0].'_'.$res[1];
                        if (isset($res[2]) && $res[2] == 'charts' && isset($res[3])) {
                            //图形报表需要四级；
                            $allowRoute[] = $roleStr.'_'.$res[2].'_'.$res[3];
                            if (!in_array('icsoc_report_charts_tree', $allowRoute)) {
                                $allowRoute[] = 'icsoc_report_charts_tree'; //所有图形报表都有此权限；
                            }
                        } else {
                            $allowRoute[] = in_array($roleStr, $this->threeLevel) ? $roleStr.'_'.$res[2] : $roleStr;
                        }
                    }
                }
                $route = $request->attributes->get('_route');
                if (!in_array($route, $this->allow)) {
                    if (in_array($route, $this->notMenu) && !in_array($route, $action)) {
                        throw new AccessDeniedHttpException(
                            $this->container->get('translator')->trans('No authorization')
                        );
                    }
                    $routes = explode('_', $route);
                    $route = $routes[0].'_'.$routes[1];
                    if (isset($routes[2]) && $routes[2] == 'charts' && isset($routes[3])) {
                        //图形报表需要四级；
                        $route = $route.'_'.$routes[2].'_'.$routes[3];
                    } else {
                        $route = in_array($route, $this->threeLevel) ? $route.'_'.$routes[2] : $route;
                    }
                    if (!in_array($route, $allowRoute)) {
                        if ($request->isXmlHttpRequest()) {
                            echo json_encode(array('msg'=>'没有权限', 'err'=>true));
                            exit;//直接退出程序;
                        } else {
                            throw new AccessDeniedHttpException(
                                $this->container->get('translator')->trans('No authorization')
                            );
                        }
                    }
                }
            }
        }
    }
}
