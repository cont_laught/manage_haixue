<?php

namespace Icsoc\SecurityBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ChangeMonitorConfigCommand
 * @package Icsoc\SecurityBundle\Command
 */
class ChangeMonitorConfigCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('role:changeMonitorConfig')
            ->setDescription('更改大屏监控的配置');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $conn = $this->getContainer()->get('doctrine.dbal.default_connection');
        $screenInfos = $conn->fetchAll("select id, screen_info from cc_monitor_screen");
        $updateScreenInfos = array();
        foreach ($screenInfos as $screenInfo) {
            $screenId = $screenInfo['id'];
            $info = json_decode($screenInfo['screen_info'], true);
            $updateScreenInfos[$screenId] = $info;
            foreach ($info['mainFields'] as $key => $value) {
                //呼叫中心监控配置
                if ($key == 0) {
                    foreach ($value['fields'] as $k => $monitorItemConfig) {
                        //去掉30秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum30') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        if ($monitorItemConfig['id'] == 'inboundValidConnTotalNum30') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        if ($monitorItemConfig['id'] == 'outboundValidConnTotalNum30') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉45秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum45') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        if ($monitorItemConfig['id'] == 'inboundValidConnTotalNum45') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        if ($monitorItemConfig['id'] == 'outboundValidConnTotalNum45') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉60秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum60') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        if ($monitorItemConfig['id'] == 'inboundValidConnTotalNum60') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        if ($monitorItemConfig['id'] == 'outboundValidConnTotalNum60') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉75秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum75') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        if ($monitorItemConfig['id'] == 'inboundValidConnTotalNum75') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        if ($monitorItemConfig['id'] == 'outboundValidConnTotalNum75') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入5秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnIn5SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉5秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnIn5SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入10秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnIn10SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入10秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnIn10SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入15秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnIn15SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入15秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnIn15SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入20秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnIn20SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入20秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnIn20SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入30秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnIn30SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入30秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnIn30SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉有效通话量，如果以前配置的有效通话量在现在的范围内，那么加上
                        if ($monitorItemConfig['id'] == 'validConnTotalNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉呼入有效通话量
                        if ($monitorItemConfig['id'] == 'inboundValidConnTotalNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉外呼有效通话量
                        if ($monitorItemConfig['id'] == 'outboundValidConnTotalNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉总放弃通话量
                        if ($monitorItemConfig['id'] == 'inboundAbandonTotalNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉呼入放弃率
                        if ($monitorItemConfig['id'] == 'inboundAbandonRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉呼入X秒放弃量
                        if ($monitorItemConfig['id'] == 'inboundAbandonInXSecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉呼入X秒放弃率
                        if ($monitorItemConfig['id'] == 'inboundAbandonInXSecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉呼入X秒接通量,如果以前保存过的X秒在现在的指标范围内，那么新加到配置中
                        if ($monitorItemConfig['id'] == 'inboundConnInXSecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉呼入X秒接通率,如果以前保存过的X秒在现在的指标范围内，那么新加到配置中
                        if ($monitorItemConfig['id'] == 'inboundConnInXSecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                    }
                }

                //技能组监控配置
                if ($key == 1) {
                    foreach ($value['fields'] as $k => $monitorItemConfig) {
                        //呼入接通量改成总呼入接通量
                        if ($monitorItemConfig['id'] == 'inboundConnNum') {
                            $updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]['name'] = '总呼入接通量';
                        }
                        //呼入接通率改成总呼入接通率
                        if ($monitorItemConfig['id'] == 'inboundConnRate') {
                            $updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]['name'] = '总呼入接通率';
                        }
                        //去掉30秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum30') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉45秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum45') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉60秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum60') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉75秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum75') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入5秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnIn5SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉5秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnIn5SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入10秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnIn10SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入10秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnIn10SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入15秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnIn15SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入15秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnIn15SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入20秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnIn20SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入20秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnIn20SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入30秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnIn30SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入30秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnIn30SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入10秒放弃量
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn10SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入10放弃率
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn10SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入20秒放弃量
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn20SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入20放弃率
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn20SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入25秒放弃量
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn25SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入25放弃率
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn25SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入30秒放弃量
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn30SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入30放弃率
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn30SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入35秒放弃量
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn35SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入35放弃率
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn35SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入40秒放弃量
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn40SecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉呼入40放弃率
                        if ($monitorItemConfig['id'] == 'inboundAbandonIn40SecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉呼入有效通话量
                        if ($monitorItemConfig['id'] == 'inboundValidConnTotalNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉呼入X秒接通量
                        if ($monitorItemConfig['id'] == 'inboundConnInXSecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉X秒接通率
                        if ($monitorItemConfig['id'] == 'inboundConnInXSecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉X秒放弃量
                        if ($monitorItemConfig['id'] == 'inboundAbandonInXSecsNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉X秒放弃率
                        if ($monitorItemConfig['id'] == 'inboundAbandonInXSecsRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                    }
                }

                if ($key == 2) {
                    foreach ($value['fields'] as $k => $monitorItemConfig) {
                        //去掉30秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum30') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉45秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum45') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉60秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum60') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉75秒有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum75') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                        //去掉有效通话量
                        if ($monitorItemConfig['id'] == 'validConnTotalNum') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉示忙占比
                        if ($monitorItemConfig['id'] == 'busyRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }

                        //去掉工作率
                        if ($monitorItemConfig['id'] == 'workRate') {
                            unset($updateScreenInfos[$screenId]['mainFields'][$key]['fields'][$k]);
                        }
                    }
                }
            }
        }

        foreach ($updateScreenInfos as $screenId => $screenConfig) {
            $screenInfo = json_encode($screenConfig);
            $conn->update("cc_monitor_screen", array('screen_info' => $screenInfo), array('id' => $screenId));
        }
    }
}
