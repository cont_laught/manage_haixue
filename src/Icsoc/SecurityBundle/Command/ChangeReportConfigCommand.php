<?php

namespace Icsoc\SecurityBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ChangeReportConfigCommand
 *
 * @package Icsoc\SecurityBundle\Command
 */
class ChangeReportConfigCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('role:changeReportConfig')
            ->setDescription('修改报表指标配置信息');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $conn = $this->getContainer()->get('doctrine.dbal.default_connection');
        $reportConfigs = $conn->fetchAll(
            "select role_id, vcc_id, report_config from cc_roles WHERE vcc_id= 2000004"
        );
        $globalReportConfigs = $conn->fetchAll(
            "select * from cc_global_report_config WHERE vcc_id= 2000004"
        );
        $res = array();
        $global = array();

        //处理全局报表配置
        foreach ($globalReportConfigs as $globalReportConfig) {
            if (strpos($globalReportConfig['report_config'], 'queueInNum') !== false) {
                //将计算的指标中的技能组来电量(queueInNum)改成总呼入量(inboundTotalNum)
                $globalReportConfig['report_config'] = str_replace('queueInNum','inboundTotalNum', $globalReportConfig['report_config']);
            }
            $globalrep = json_decode($globalReportConfig['report_config'], true);
            if (!empty($globalrep)) {
                $global[$globalReportConfig['id']] = $globalrep;
                foreach ($globalrep as $report => $globalrepConfig) {
                    $global[$globalReportConfig['id']][$report]['calculateItems']['inboundConnInXSecsRate'] = array(
                        'numerator' => array(
                            'plus'=> array('inboundConnInXSecsNum'),
                        ),
                        'denominator' => array(
                            'plus'=> array('inboundConnNum'),
                        ),
                        'percent' => 1,
                        'editable' => true,
                    );
                    if ($report == 'agent' || $report == 'group') {
                        //咨询时长改成会议总时长
                        $global[$globalReportConfig['id']][$report]['showValues']['fixed']['conferenceTotalSecs'] = array(
                            'name' => '会议总时长',
                            'sort' => isset($globalrepConfig['showValues']['fixed']['conferenceTotalSecs']['sort']) ? $globalrepConfig['showValues']['fixed']['conferenceTotalSecs']['sort']: 100,
                            'globalConfig' => isset($globalrepConfig['showValues']['fixed']['conferenceTotalSecs']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['conferenceTotalSecs']['globalConfig'] : 0,
                        );
                    }
                    foreach ($globalrepConfig['fixed'] as $key => $val) {
                        if ($report == 'agent' || $report == 'queue' || $report == 'group') {
                            //去掉超时未评价量
                            if ($val == 'evaluate_-2') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                                unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['evaluate_-2']);
                            }

                            //修改未评价挂机量为未评价量,
                            $global[$globalReportConfig['id']][$report]['showValues']['fixed']['evaluate_-3'] = array(
                                'name' => '未评价量',
                                'sort' => isset($globalrepConfig['showValues']['fixed']['evaluate_-3']['sort']) ? $globalrepConfig['showValues']['fixed']['evaluate_-3']['sort']: 100,
                                'globalConfig' => isset($globalrepConfig['showValues']['fixed']['evaluate_-3']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['evaluate_-3']['globalConfig'] : 0,
                            );
                        }

                        if ($report == 'group' || $report == 'agent') {
                            //去掉坐席工作表现报表中的振铃总时长
                            if ($val == 'totalRingsecs') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                                unset($global[$globalReportConfig['id']][$report]['calculateItems']['totalRingsecs']);
                                unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['totalRingsecs']);
                            }

                            //去掉事后总时长
                            if ($val == 'totalWaitsecs') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                                unset($global[$globalReportConfig['id']][$report]['calculateItems']['totalWaitsecs']);
                                unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['totalWaitsecs']);
                            }
                        }

                        if ($report == 'system') {
                            $waive = array(10, 20, 25, 30,35, 40);
                            foreach ($waive as $lostNum) {
                                if ($val == 'lost'.$lostNum.'_rate') {
                                    unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                                }

                                if ($val == 'lost'.$lostNum.'_num') {
                                    unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                                }

                                unset($global[$globalReportConfig['id']][$report]['calculateItems']['lost'.$lostNum.'_rate']);
                                unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['lost'.$lostNum.'_num']);
                                unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['lost'.$lostNum.'_rate']);
                            }
                        }
                    }

                    //坐席工作表现报表中人工呼入量改成呼入接通量，修改显示的值，默认显示值已在程序中改过了
                    if ($report == 'agent') {
                        $global[$globalReportConfig['id']][$report]['showValues']['fixed']['inboundTotalNum'] = array(
                            'name' => '呼入接通量',
                            'sort' => isset($globalrepConfig['showValues']['fixed']['inboundTotalNum']['sort']) ? $globalrepConfig['showValues']['fixed']['inboundTotalNum']['sort']: 100,
                            'globalConfig' => isset($globalrepConfig['showValues']['fixed']['inboundTotalNum']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['inboundTotalNum']['globalConfig'] : 0,
                        );

                        //会议量改成总会议通话量
                        $global[$globalReportConfig['id']][$report]['showValues']['fixed']['conferenceNum'] = array(
                            'name' => '总会议通话量',
                            'sort' => isset($globalrepConfig['showValues']['fixed']['conferenceNum']['sort']) ? $globalrepConfig['showValues']['fixed']['conferenceNum']['sort']: 100,
                            'globalConfig' => isset($globalrepConfig['showValues']['fixed']['conferenceNum']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['conferenceNum']['globalConfig'] : 0,
                        );

                        //外呼接通量改成总外呼接通量
                        $global[$globalReportConfig['id']][$report]['showValues']['fixed']['outboundTotalNum'] = array(
                            'name' => '呼入接通量',
                            'sort' => isset($globalrepConfig['showValues']['fixed']['outboundTotalNum']['sort']) ? $globalrepConfig['showValues']['fixed']['outboundTotalNum']['sort']: 100,
                            'globalConfig' => isset($globalrepConfig['showValues']['fixed']['outboundTotalNum']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['outboundTotalNum']['globalConfig'] : 0,
                        );
                    }

                    if ($report == 'system') {
                        //总排队时长改成会议总时长
                        $global[$globalReportConfig['id']][$report]['showValues']['fixed']['waitTotalSecs'] = array(
                            'name' => '事后总时长',
                            'sort' => isset($globalrepConfig['showValues']['fixed']['waitTotalSecs']['sort']) ? $globalrepConfig['showValues']['fixed']['waitTotalSecs']['sort']: 100,
                            'globalConfig' => isset($globalrepConfig['showValues']['fixed']['waitTotalSecs']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['waitTotalSecs']['globalConfig'] : 0,
                        );

                        //总通话时长改为总呼入通话时长
                        $global[$globalReportConfig['id']][$report]['showValues']['fixed']['inboundConnTotalSecs'] = array(
                            'name' => '总呼入通话时长',
                            'sort' => isset($globalrepConfig['showValues']['fixed']['inboundConnTotalSecs']['sort']) ? $globalrepConfig['showValues']['fixed']['inboundConnTotalSecs']['sort']: 100,
                            'globalConfig' => isset($globalrepConfig['showValues']['fixed']['inboundConnTotalSecs']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['inboundConnTotalSecs']['globalConfig'] : 0,
                        );

                        //技能组来电量改成人工呼入量
                        $global[$globalReportConfig['id']][$report]['showValues']['fixed']['inboundTotalNum'] = array(
                            'name' => '人工呼入量',
                            'sort' => isset($globalrepConfig['showValues']['fixed']['inboundTotalNum']['sort']) ? $globalrepConfig['showValues']['fixed']['inboundTotalNum']['sort']: 100,
                            'globalConfig' => isset($globalrepConfig['showValues']['fixed']['inboundTotalNum']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['inboundTotalNum']['globalConfig'] : 0,
                        );

                        //更改呼入接通率的计算方式
                        $global[$globalReportConfig['id']][$report]['calculateItems']['inboundConnRate'] = array(
                            'numerator' => array(
                                'plus'=> array('inboundConnNum'),
                            ),
                            'denominator' => array(
                                'plus'=> array('inboundTotalNum'),
                            ),
                            'percent' => 1,
                            'editable' => true,
                        );

                        //人工占比计算方式
                        $global[$globalReportConfig['id']][$report]['calculateItems']['inboundRate'] = array(
                            'numerator' => array(
                                'plus'=> array('inboundTotalNum'),
                            ),
                            'denominator' => array(
                                'plus'=> array('ivrInboundTotalNum'),//array('ivrInboundNum'),
                            ),
                            'percent' => 1,
                            'editable' => true,
                        );

                        foreach ($globalrepConfig['fixed'] as $key => $val) {
                            //去掉人均话务处理量
                            if ($val == 'inboundAvgNum') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                            }

                            //去掉呼入放弃率
                            if ($val == 'inboundAbandonRate') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                            }

                            //去掉总放弃通话时长
                            if ($val == 'inboundAbandonTotalSecs') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                            }

                            //平均放弃时长
                            if ($val == 'inboundAbandonAvgSecs') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                            }

                            if ($val == 'ivrInboundNum') {
                                $global[$globalReportConfig['id']][$report]['fixed'][$key] = 'ivrInboundTotalNum';
                            }
                        }

                        //去掉自定义报表指标中的配置
                        foreach ($globalrepConfig['custom'] as $key => $val) {
                            //去掉呼入放弃率
                            if ($val == 'inboundAbandonRate') {
                                unset($global[$globalReportConfig['id']][$report]['custom'][$key]);
                            }

                            //去掉X秒放弃量
                            if ($val == 'inboundAbandonInXSecsNum') {
                                unset($global[$globalReportConfig['id']][$report]['custom'][$key]);
                            }

                            //去掉X秒放弃率
                            if ($val == 'inboundAbandonInXSecsRate') {
                                unset($global[$globalReportConfig['id']][$report]['custom'][$key]);
                            }
                        }

                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['inboundAvgNum']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['inboundAvgNum']);

                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['inboundAbandonRate']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['inboundAbandonRate']);

                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['inboundAbandonTotalSecs']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['inboundAbandonTotalSecs']);

                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['inboundAbandonAvgSecs']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['inboundAbandonAvgSecs']);

                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['inboundAbandonInXSecsNum']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['custom']['inboundAbandonInXSecsNum']);

                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['inboundAbandonInXSecsRate']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['custom']['inboundAbandonInXSecsRate']);

                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['inboundAbandonRate']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['custom']['inboundAbandonRate']);
                    }

                    if ($report == 'queue') {
                        foreach ($globalrepConfig['fixed'] as $key => $val) {
                            //去掉技能组来电量
                            if ($val == 'queueInNum') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                            }

                            //去掉平均排队时长
                            if ($val == 'queueAvgSecs') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                            }

                            //去掉会议总时长
                            if ($val == 'conferenceTotalSecs') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                            }
                        }
                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['queueInNum']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['queueInNum']);

                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['queueAvgSecs']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['queueAvgSecs']);

                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['conferenceTotalSecs']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['conferenceTotalSecs']);

                        //更改呼入接通率的计算方式
                        $global[$globalReportConfig['id']][$report]['calculateItems']['inboundConnRate'] = array(
                            'numerator' => array(
                                'plus'=> array('inboundConnNum'),
                            ),
                            'denominator' => array(
                                'plus'=> array('inboundTotalNum'),
                            ),
                            'percent' => 1,
                            'editable' => true,
                        );

                        //人工呼入改成总呼入量
                        $global[$globalReportConfig['id']][$report]['showValues']['fixed']['inboundTotalNum'] = array(
                            'name' => '总呼入量',
                            'sort' => isset($globalrepConfig['showValues']['fixed']['inboundTotalNum']['sort']) ? $globalrepConfig['showValues']['fixed']['inboundTotalNum']['sort']: 100,
                            'globalConfig' => isset($globalrepConfig['showValues']['fixed']['inboundTotalNum']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['inboundTotalNum']['globalConfig'] : 0,
                        );

                        //放弃率计算方式
                        $global[$globalReportConfig['id']][$report]['calculateItems']['inboundAbandonInXSecsRate'] = array(
                            'numerator' => array(
                                'plus'=> array('inboundAbandonInXSecsNum'),
                            ),
                            'denominator' => array(
                                'plus'=> array('inboundTotalNum'),
                            ),
                            'percent' => 1,
                            'editable' => true,
                        );
                    }

                    if ($report == 'group') {
                        foreach ($globalrepConfig['fixed'] as $key => $val) {
                            //去掉人均话务处理时间
                            if ($val == 'callAvgSecs') {
                                unset($global[$globalReportConfig['id']][$report]['fixed'][$key]);
                            }
                        }

                        unset($global[$globalReportConfig['id']][$report]['calculateItems']['callAvgSecs']);
                        unset($global[$globalReportConfig['id']][$report]['showValues']['fixed']['callAvgSecs']);

                        //人工呼入改成总呼入接通量
                        $global[$globalReportConfig['id']][$report]['showValues']['fixed']['inboundTotalNum'] = array(
                            'name' => '总呼入接通量',
                            'sort' => isset($globalrepConfig['showValues']['fixed']['inboundTotalNum']['sort']) ? $globalrepConfig['showValues']['fixed']['inboundTotalNum']['sort']: 100,
                            'globalConfig' => isset($globalrepConfig['showValues']['fixed']['inboundTotalNum']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['inboundTotalNum']['globalConfig'] : 0,
                        );

                        //会议量改成总会议通话量
                        $global[$globalReportConfig['id']][$report]['showValues']['fixed']['conferenceNum'] = array(
                            'name' => '总会议通话量',
                            'sort' => isset($globalrepConfig['showValues']['fixed']['conferenceNum']['sort']) ? $globalrepConfig['showValues']['fixed']['conferenceNum']['sort']: 100,
                            'globalConfig' => isset($globalrepConfig['showValues']['fixed']['conferenceNum']['globalConfig']) ? $globalrepConfig['showValues']['fixed']['conferenceNum']['globalConfig'] : 0,
                        );
                    }
                }
            }
        }

        foreach ($global as $id => $roleConfig) {
            if (!empty($roleConfig)) {
                $rolcon = json_encode($roleConfig);
                $conn->update('cc_global_report_config', array('report_config' => $rolcon), array('id' => $id));
            }
        }

        //处理各个角色的报表配置
        foreach ($reportConfigs as  $reportConfig) {
            if (strpos( $reportConfig['report_config'], 'queueInNum') !== false) {
                //将计算的指标中的技能组来电量(queueInNum)改成总呼入量(inboundTotalNum)
                $reportConfig['report_config'] = str_replace('queueInNum','inboundTotalNum', $reportConfig['report_config']);
            }
            $config = json_decode($reportConfig['report_config'], true);

            if (!empty($config)) {
                $res[$reportConfig['role_id']] = $config;
                foreach ($config as $report => $repConfig ) {
                    //修改呼入XX秒接通率结算方式
                    $res[$reportConfig['role_id']][$report]['calculateItems']['inboundConnInXSecsRate'] = array(
                        'numerator' => array(
                            'plus'=> array('inboundConnInXSecsNum'),
                        ),
                        'denominator' => array(
                            'plus'=> array('inboundConnNum'),
                        ),
                        'percent' => 1,
                        'editable' => true,
                    );

                    if ($report == 'agent' || $report == 'group') {
                        //咨询时长改成会议总时长
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['conferenceTotalSecs'] = array(
                            'name' => '会议总时长',
                            'sort' => isset($repConfig['showValues']['fixed']['conferenceTotalSecs']['sort']) ?  $repConfig['showValues']['fixed']['conferenceTotalSecs']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['conferenceTotalSecs']['globalConfig']) ?  $repConfig['showValues']['fixed']['conferenceTotalSecs']['globalConfig'] : 0,
                        );
                    }
                    if ($report == 'agent' || $report == 'queue' || $report == 'group') {
                        //去掉超时未评价量
                        foreach ($repConfig['fixed'] as $key => $val) {
                            if ($val == 'evaluate_-2') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }
                        }
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['evaluate_-2']);

                        //修改未评价挂机量为未评价量
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['evaluate_-3'] = array(
                            'name' => '未评价量',
                            'sort' => isset($repConfig['showValues']['fixed']['evaluate_-3']['sort']) ? $repConfig['showValues']['fixed']['evaluate_-3']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['evaluate_-3']['globalConfig']) ? $repConfig['showValues']['fixed']['evaluate_-3']['globalConfig'] : 0,
                        );
                    }

                    if ($report == 'group' || $report == 'agent') {
                        foreach ($repConfig['fixed'] as $key => $val) {
                            //去掉坐席工作表现报表中的振铃总时长
                            if ($val == 'totalRingsecs') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }

                            //去掉事后总时长
                            if ($val == 'totalWaitsecs') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }
                        }
                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['totalRingsecs']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['totalRingsecs']);


                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['totalWaitsecs']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['totalWaitsecs']);
                    }

                    //坐席工作表现报表中人工呼入量改成呼入接通量，修改显示的值，默认显示值已在程序中改过了
                    if ($report == 'agent') {
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['inboundTotalNum'] = array(
                            'name' => '总呼入接通量',
                            'sort' => isset($repConfig['showValues']['fixed']['inboundTotalNum']['sort']) ?  $repConfig['showValues']['fixed']['inboundTotalNum']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['inboundTotalNum']['globalConfig']) ?  $repConfig['showValues']['fixed']['inboundTotalNum']['globalConfig'] : 0,
                        );

                        //会议量改成总会议通话量
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['conferenceNum'] = array(
                            'name' => '总会议通话量',
                            'sort' => isset($repConfig['showValues']['fixed']['conferenceNum']['sort']) ? $repConfig['showValues']['fixed']['conferenceNum']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['conferenceNum']['globalConfig']) ? $repConfig['showValues']['fixed']['conferenceNum']['globalConfig'] : 0,
                        );

                        //外呼接通量改成总外呼接通量
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['outboundTotalNum'] = array(
                            'name' => '总呼入接通量',
                            'sort' => isset($repConfig['showValues']['fixed']['outboundTotalNum']['sort']) ?  $repConfig['showValues']['fixed']['outboundTotalNum']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['outboundTotalNum']['globalConfig']) ?  $repConfig['showValues']['fixed']['outboundTotalNum']['globalConfig'] : 0,
                        );
                    }

                    if ($report == 'system') {
                        //总排队时长改成会议总时长
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['waitTotalSecs'] = array(
                            'name' => '事后总时长',
                            'sort' => isset($repConfig['showValues']['fixed']['waitTotalSecs']['sort']) ? $repConfig['showValues']['fixed']['waitTotalSecs']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['waitTotalSecs']['globalConfig']) ? $repConfig['showValues']['fixed']['waitTotalSecs']['globalConfig'] : 0,
                        );

                        //总通话时长改为总呼入通话时长
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['inboundConnTotalSecs'] = array(
                            'name' => '总呼入通话时长',
                            'sort' => isset($repConfig['showValues']['fixed']['inboundConnTotalSecs']['sort']) ? $repConfig['showValues']['fixed']['inboundConnTotalSecs']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['inboundConnTotalSecs']['globalConfig']) ? $repConfig['showValues']['fixed']['inboundConnTotalSecs']['globalConfig'] : 0,
                        );

                        //人工占比计算方式
                        $res[$reportConfig['role_id']][$report]['calculateItems']['inboundRate'] = array(
                            'numerator' => array(
                                'plus'=> array('inboundTotalNum'),
                            ),
                            'denominator' => array(
                                'plus'=> array('ivrInboundTotalNum'),//array('ivrInboundNum'),
                            ),
                            'percent' => 1,
                            'editable' => true,
                        );

                        //技能组来电量改成人工呼入量
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['inboundTotalNum'] = array(
                            'name' => '人工呼入量',
                            'sort' => isset($repConfig['showValues']['fixed']['inboundTotalNum']['sort']) ? $repConfig['showValues']['fixed']['inboundTotalNum']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['inboundTotalNum']['globalConfig']) ? $repConfig['showValues']['fixed']['inboundTotalNum']['globalConfig'] : 0,
                        );

                        foreach ($repConfig['fixed'] as $key => $val) {
                            //去掉人均话务处理量
                            if ($val == 'inboundAvgNum') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }

                            //去掉呼入放弃率
                            if ($val == 'inboundAbandonRate') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }

                            //去掉总放弃通话时长
                            if ($val == 'inboundAbandonTotalSecs') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }

                            //平均放弃时长
                            if ($val == 'inboundAbandonAvgSecs') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }

                            if ($val == 'ivrInboundNum') {
                                $res[$reportConfig['role_id']][$report]['fixed'][$key] = 'ivrInboundTotalNum';
                            }

                            $waive = array(10, 20, 25, 30, 35, 40);
                            foreach ($waive as $lostNum) {
                                if ($val == 'lost'.$lostNum.'_rate') {
                                    unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                                }

                                if ($val == 'lost'.$lostNum.'_num') {
                                    unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                                }

                                unset($res[$reportConfig['role_id']][$report]['calculateItems']['lost'.$lostNum.'_rate']);
                                unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['lost'.$lostNum.'_num']);
                                unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['lost'.$lostNum.'_rate']);
                            }
                        }

                        $waive = array(10, 20, 25, 30, 35, 40);
                        foreach ($waive as $lostNum) {
                            unset($res[$reportConfig['role_id']][$report]['calculateItems']['lost'.$lostNum.'_rate']);
                            unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['lost'.$lostNum.'_num']);
                            unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['lost'.$lostNum.'_rate']);
                        }

                        //去掉自定义报表指标中的配置
                        foreach ($repConfig['custom'] as $key => $val) {
                            //去掉呼入放弃率
                            if ($val == 'inboundAbandonRate') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }

                            //去掉X秒放弃量
                            if ($val == 'inboundAbandonInXSecsNum') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }

                            //去掉X秒放弃率
                            if ($val == 'inboundAbandonInXSecsRate') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }
                        }


                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['inboundAvgNum']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['inboundAvgNum']);

                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['inboundAbandonRate']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['inboundAbandonRate']);

                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['inboundAbandonTotalSecs']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['inboundAbandonTotalSecs']);

                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['inboundAbandonAvgSecs']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['inboundAbandonAvgSecs']);

                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['inboundAbandonInXSecsNum']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['custom']['inboundAbandonInXSecsNum']);

                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['inboundAbandonInXSecsRate']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['custom']['inboundAbandonInXSecsRate']);

                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['inboundAbandonRate']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['custom']['inboundAbandonRate']);
                    }

                    if ($report == 'queue') {
                        foreach ($repConfig['fixed'] as $key => $val) {
                            //去掉技能组来电量
                            if ($val == 'queueInNum') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }

                            //去掉平均排队时长
                            if ($val == 'queueAvgSecs') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }

                            //去掉会议总时长
                            if ($val == 'conferenceTotalSecs') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }
                        }
                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['queueInNum']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['queueInNum']);

                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['queueAvgSecs']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['queueAvgSecs']);

                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['conferenceTotalSecs']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['conferenceTotalSecs']);

                        //更改呼入接通率的计算方式
                        $res[$reportConfig['role_id']][$report]['calculateItems']['inboundConnRate'] = array(
                            'numerator' => array(
                                'plus'=> array('inboundConnNum'),
                            ),
                            'denominator' => array(
                                'plus'=> array('inboundTotalNum'),
                            ),
                            'percent' => 1,
                            'editable' => true,
                        );

                        //放弃率计算方式
                        $res[$reportConfig['role_id']][$report]['calculateItems']['inboundAbandonInXSecsRate'] = array(
                            'numerator' => array(
                                'plus'=> array('inboundAbandonInXSecsNum'),
                            ),
                            'denominator' => array(
                                'plus'=> array('inboundTotalNum'),
                            ),
                            'percent' => 1,
                            'editable' => true,
                        );

                        //人工呼入改成总呼入量
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['inboundTotalNum'] = array(
                            'name' => '总呼入量',
                            'sort' => isset($repConfig['showValues']['fixed']['inboundTotalNum']['sort']) ? $repConfig['showValues']['fixed']['inboundTotalNum']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['inboundTotalNum']['globalConfig']) ? $repConfig['showValues']['fixed']['inboundTotalNum']['globalConfig'] : 0,
                        );
                    }

                    if ($report == 'group') {
                        foreach ($repConfig['fixed'] as $key => $val) {
                            //去掉人均话务处理时间
                            if ($val == 'callAvgSecs') {
                                unset($res[$reportConfig['role_id']][$report]['fixed'][$key]);
                            }
                        }
                        unset($res[$reportConfig['role_id']][$report]['calculateItems']['callAvgSecs']);
                        unset($res[$reportConfig['role_id']][$report]['showValues']['fixed']['callAvgSecs']);

                        //人工呼入改成总呼入接通量
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['inboundTotalNum'] = array(
                            'name' => '总呼入接通量',
                            'sort' => isset($repConfig['showValues']['fixed']['inboundTotalNum']['sort']) ? $repConfig['showValues']['fixed']['inboundTotalNum']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['inboundTotalNum']['globalConfig']) ? $repConfig['showValues']['fixed']['inboundTotalNum']['globalConfig'] : 0,
                        );

                        //会议量改成总会议通话量
                        $res[$reportConfig['role_id']][$report]['showValues']['fixed']['conferenceNum'] = array(
                            'name' => '总会议通话量',
                            'sort' => isset($repConfig['showValues']['fixed']['conferenceNum']['sort']) ? $repConfig['showValues']['fixed']['conferenceNum']['sort']: 100,
                            'globalConfig' => isset($repConfig['showValues']['fixed']['conferenceNum']['globalConfig']) ? $repConfig['showValues']['fixed']['conferenceNum']['globalConfig'] : 0,
                        );
                    }
                }
            }
        }

        foreach ($res as $roleId => $roleConfig) {

            if (!empty($roleConfig)) {
                $rescon = json_encode($roleConfig);
                $conn->update('cc_roles', array('report_config' => $rescon), array('role_id' => $roleId));
            }
        }
    }
}
