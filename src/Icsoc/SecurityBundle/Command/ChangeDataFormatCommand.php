<?php

namespace Icsoc\SecurityBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ChangeDataFormatCommand
 *
 * @package Icsoc\SecurityBundle\Command
 */

class ChangeDataFormatCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('role:changeDataFormat')
            ->setDescription('修改报表配置的数据格式');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $conn = $this->getContainer()->get('doctrine.dbal.default_connection');
        $reportConfigs = $conn->fetchAll("select role_id, vcc_id, report_config from cc_roles");
        $res = array();

        foreach ($reportConfigs as $reportConfig) {
            $config = json_decode($reportConfig['report_config'], true);
            $res[$reportConfig['role_id']] = $config;
            if (!empty($config)) {
                foreach ($config as $report => $repConfig) {
                    if (!isset($repConfig['showValues'])) {
                        $res[$reportConfig['role_id']][$report]['showValues'] = array(
                            'fixed' => array(),
                            'custom' => array()
                        );
                    } else {
                        if (!isset($repConfig['showValues']['fixed'])) {
                            $res[$reportConfig['role_id']][$report]['showValues']['fixed'] = array();
                        }
                        if (empty($repConfig['showValues']['fixed'])) {
                            $res[$reportConfig['role_id']][$report]['showValues']['fixed'] = array();
                        } else {
                            $res[$reportConfig['role_id']][$report]['showValues']['fixed'] = $repConfig['showValues']['fixed'];
                        }

                        if (!isset($repConfig['showValues']['custom'])) {
                            $res[$reportConfig['role_id']][$report]['showValues']['custom'] = array();
                        }
                        if (empty($repConfig['showValues']['custom'])) {
                            $res[$reportConfig['role_id']][$report]['showValues']['custom'] = array();
                        } else {
                            $res[$reportConfig['role_id']][$report]['showValues']['custom'] = $repConfig['showValues']['custom'];
                        }
                    }

                    if (isset($repConfig['customItems'])) {
                        if (!empty($repConfig['customItems'])) {
                            foreach ($repConfig['customItems'] as $cusItem => $cusItemConfig) {
                                if (in_array($cusItem, array('fixed', 'custom'))) {
                                    foreach ($cusItemConfig as $k => $v) {
                                        $res[$reportConfig['role_id']][$report]['customItems'][$cusItem][$k] = $v;
                                    }
                                } else {
                                    $res[$reportConfig['role_id']][$report]['customItems']['custom'][$cusItem] = $cusItemConfig;
                                    $res[$reportConfig['role_id']][$report]['customItems']['fixed'] = array();
                                }
                            }
                        } else {
                            $res[$reportConfig['role_id']][$report]['customItems'] = array('fixed' => array(), 'custom' => array());
                        }
                    } else {
                        $res[$reportConfig['role_id']][$report]['customItems'] = array('fixed' => array(), 'custom' => array());
                    }

                    if ($report == 'queue') {
                        if (!empty($repConfig['customItems']['fixed'])) {
                            foreach ($repConfig['customItems']['fixed'] as $fixeditem => $fixedItemConfig) {
                                if ($fixeditem == 'inboundTotalNum') {
                                    unset($res[$reportConfig['role_id']][$report]['customItems']['fixed'][$fixeditem]);
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($res as $roleId => $roleConfig) {
            if (!empty($roleConfig)) {
                $rescon = json_encode($roleConfig);
                $conn->update('cc_roles', array('report_config' => $rescon), array('role_id' => $roleId));
            }
        }
    }
}
