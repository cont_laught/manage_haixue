<?php
namespace Icsoc\SecurityBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Class TestRouteCommand
 * @package Icsoc\SecurityBundle\Command
 */
class TestRouteCommand extends ContainerAwareCommand
{
    /**老权限==>新权限
     * @var array
     */
    private $relation = array(
        'account'=>'icsoc_security_setting',
        'role'=>'icsoc_security_role_info',
        'agents'=>'icsoc_agent_index',
        'rule'=>'icsoc_rules_list',
        'laterule'=>'icsoc_rules_list',
        'phones'=>'icsoc_phone_manage_index',
        'ivr'=>'icsoc_ivr_list',
        'sounds'=>'icsoc_sound_index',
        'groups'=>'icsoc_queue_index',
        'agqu'=>'icsoc_queue_agqu_index', //技能组快捷分配；
        'blacklist'=>'icsoc_namelist_black_index',
        'records'=>'icsoc_recording_list',
        'voice'=>'icsoc_recording_message_lsit',
        'msystem'=>'icsoc_monitor_system_monitor',
        'magent_'=>'icsoc_monitor_agent_monitor',
        'magent_conf'=>'icsoc_monitor_call_center_monitor',
        'magent_config'=>'icsoc_monitor_conf_monitor',
        'rincoming'=>'icsoc_report_callin',
        'rout'=>'icsoc_report_callout',
        'calldetail'=>'icsoc_report_call',
        'agentdetail'=>'icsoc_report_agentstate',
        'rconference'=>'icsoc_report_conference',
        'rmonitor'=>'icsoc_report_monitor',
        'sms'=>'icsoc_report_sms',
        'rlost'=>'icsoc_report_lost',
        'inallot'=>'icsoc_report_inallot',
        'trans_call'=>'icsoc_report_queueTranscall',
        'ragent'=>'icsoc_report_agent_home',
        'rqueue'=>'icsoc_report_queue_home',
        'rsystem'=>'icsoc_report_system_home',
        'evaluate'=>'icsoc_recording_list', //满意度满意报表-->录音管理；
        'collect'=>'icsoc_report_evaluatecollec',
        'agentcall'=>'icsoc_report_charts_agentcall',
        'queuecall'=>'icsoc_report_charts_queuecall',
        'evaluate_chart'=>'icsoc_report_charts_evaluate',
        'incoming_area'=>'icsoc_report_charts_inarea',
        'incoming_hour'=>'icsoc_report_charts_inhour',
        'caller_type'=>'icsoc_report_charts_calltype',
        'xtrz'=>'icsoc_log_index',
        'notice'=>'icsoc_notice_index',
        'configs'=>'icsoc_security_setting',
        'agent_add'=>'icsoc_agent_add',
        'agent_edit'=>'icsoc_agent_edit',
        'agent_del'=>'icsoc_agent_del',
        'queue_upload'=>'icsoc_queue_batch',
        'agentwork'=>'icsoc_report_charts_agentwork',
        'records_export'=>'icsoc_recording_list_batch_down_data',
        'mcalls'=>'icsoc_monitor_system_monitor', //排队监控
        'mqueue'=>'icsoc_monitor_system_monitor', //技能组监控
        'magent'=>'icsoc_monitor_system_monitor', //坐席监控
        'hangup_sms'=>'icsoc_security_setting', //参数设计，关机短信
        'agent_login'=>'icsoc_agent_login', //坐席切换,列表中的登陆
    );

    protected function configure()
    {
        $this
            ->setName('test:route')
            ->setDescription('角色权限转化');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $router = $this->getContainer()->get("router");
        foreach ($this->relation as $val) {
            try {
                $url = $router->generate($val);
                $output->writeln($val.'------------'.$url.'-------success');
            } catch (RouteNotFoundException $e) {
                $output->writeln($val.'------------0000-------fail');
                break;
            }
        }
    }
}
