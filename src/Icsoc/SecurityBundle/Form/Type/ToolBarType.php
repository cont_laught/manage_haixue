<?php
namespace Icsoc\SecurityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ToolBarType extends AbstractType
{
    /**
     * @var
     * 服务
     */
    private $server;
    public function __construct(ContainerInterface $container)
    {
        $this->server = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //获取满意度语音
        $user = $this->server->get('security.token_storage')->getToken()->getUser();
        $vccCode = $user->getVccCode();
        $soundList = $this->server->get('doctrine.orm.entity_manager')
            ->getRepository("IcsocSecurityBundle:WinSounds")->getSoundTypeList($vccCode, 2);
        $builder
            ->add('indexPage', 'text', array(
                    'attr'=>array('class'=>'col-xs-10 col-sm-9'),
                    'label'=>'Home address',
                    'required'=>false,
                ))
            ->add('inpopPage', 'text', array(
                    'attr'=>array('class'=>'col-xs-10 col-sm-9'),
                    'label'=>'Screen calls shells',
                    'required'=>false,
                ))
            ->add('outpopPage', 'text', array(
                    'attr'=>array('class'=>'col-xs-10 col-sm-9'),
                    'label'=>'External elastic screen address',
                    'required'=>false,
                ))
            ->add('systemName', 'text', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-9'),
                'label'=>'The system name',
                'required'=>false,
            /*))->add('endsmsType', 'choice', array(
                'label'=>'On hook message',
                'choices'=> array('0'=>'无', '1'=>'未接发送', '2'=>'接听发送', '3'=>'全部发送'),
                'attr'=>array('class'=>'col-xs-10 col-sm-9'),
                'required'=>false,
            ))->add('endsmsPhone', 'text', array(
                'attr'=>array('class'=>'col-xs-10 col-sm-9'),
                'label'=>'SMS number',
                'help'=>'Null default to send agents mobile phone number, setting is sent to the fixed mobile phone number (SMS charges $0.10 / $)',
                'required'=>false,*/
            ))->add('evaluateSound', 'choice', array(
                'label'=>'Satisfaction evaluation of speech',
                'attr'=>array('class'=>'col-xs-10 col-sm-9'),
                'choices'=>$soundList,
                'required'=>false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Icsoc\SecurityBundle\Entity\CcCcods',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'toolBarForm';
    }
}
