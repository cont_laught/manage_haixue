<?php
namespace Icsoc\SecurityBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SettingType extends AbstractType
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        $connNum = $data->getConnNum();
        $connNum = empty($connNum) ? 15 : $connNum;
        $lostNum = $data->getLostNum();
        $lostNum = empty($lostNum) ? 25 : $lostNum;
        $connNum = explode(',', $connNum);
        $lostNum = explode(',', $lostNum);
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $vccCode = $user->getVccCode();
        $whiteSoundList = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository("IcsocSecurityBundle:WinSounds")->getSoundTypeList($vccCode, 3);
        $builder
            ->add('isIncomingEnableBlack', 'choice', array(
                'label'=>'Incoming call blacklist',
                'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                'choices'=>array('Disable','Enable'),
                'data'=>$data->getIsIncomingEnableBlack() ? 1 : 0,
                'expanded'=>true,
                'multiple'=>false
                ))
            ->add('isEnableBlack', 'choice', array(
                'label'=>'Exhaled black list',
                'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                'choices'=>array('Disable', 'Enable'),
                'data'=>$data->getIsEnableBlack() ? 1 : 0,
                'expanded'=>true,
                'multiple'=>false
                ))
            ->add('isEnableWhite', 'choice', array(
                'label'=>'White list',
                'attr'=>array('class'=>'col-xs-10 col-sm-5','onclick'=>"switchOption('isEnableWhite')"),
                'choices'=>array('Disable','Enable'),
                 'data'=>$data->getIsEnableWhite() ? 1 : 0,
                'expanded'=>true,
                'multiple'=>false
                ))
            ->add('whiteType', 'choice', array(
                'label'=>'Access rules',
                'attr'=>array('class'=>'col-xs-10 col-sm-5','onclick'=>'switchWhite()'),
                'choices'=>array('1'=>'Priority access','2'=>'Only allow access'),
                'data'=>$data->getWhiteType() ? $data->getWhiteType() : 1,
                'expanded'=>true,
                'multiple'=>false))
            ->add('whiteSound', 'choice', array(
                'label'=>'Non white list voice prompts',
                'attr'=>array('class'=>'col-xs-10 col-sm-9'),
                'choices'=>$whiteSoundList,
                'required'=>false,
                ))
//            ->add('isEnableGroup', 'choice', array(
//                'label'=>'Business group',
//                'attr'=>array('class'=>'col-xs-10 col-sm-5'),
//                'choices'=>array('Disable','Enable'),
//                'data'=>$data->getIsEnableGroup() ? 1 : 0,
//                'expanded'=>true,
//                'multiple'=>false
//                ))
            ->add('toolBar', 'choice', array(
                'label'=>'Toolbar interface parameters',
                'attr'=>array('onclick'=>"switchOption('toolBar')"),
                'choices'=>array('Hide','Enable'),
                'data'=>$data->getToolBar() ? 1 : 0,
                'expanded'=>true,
                'multiple'=>false
                ))
            ->add('toolBarOptions', new ToolBarType($this->container))
            ->add('statistical', 'choice', array(
                    'label'=>'Statistical parameter setting',
                    'attr'=>array('class'=>'col-xs-10 col-sm-5', 'onclick'=>"switchOption('statistical')"),
                    'choices'=>array('Hide','Enable'),
                    'data'=>$data->getStatistical() ? 1 : 0,
                    'expanded'=>true,
                    'multiple'=>false
                ))
            ->add('connNum', 'choice', array(
                    'label'=>'Through statistics',
                    'attr'=>array('class'=>'col-xs-10 col-sm-7'),
                    'choices'=>$this->container->getParameter('conn_num'),
                    'data'=>$connNum,
                    'expanded'=>true,
                    'multiple'=>true
                ))
            ->add('lostNum', 'choice', array(
                    'label'=>'Give up the statistics',
                    'attr'=>array('class'=>'col-xs-10 col-sm-7'),
                    'choices'=>$this->container->getParameter('lost_num'),
                    'expanded'=>true,
                    'data'=>$lostNum,
                    'multiple'=>true
                ))
            /*->add('smsOptions', new SmsContentType($data->getSmsOptions()))*/
            ->add('isEnableEvaluate', 'choice', array(
                'label'=>'自转满意度',
                'attr'=>array('class'=>'col-xs-10 col-sm-7'),
                'choices'=>array('Disable','Enable'),
                'expanded'=>true,
                'data'=>empty($data->getIsEnableEvaluate()) ? 0 :1,
                'multiple'=>false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Icsoc\SecurityBundle\Entity\CcCcodConfigs',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'settingForm';
    }
}
