<?php
namespace Icsoc\SecurityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SmsContentType extends AbstractType
{
    private $smsData = '';

    /**
     * @param string $data
     */
    public function __construct($data = '')
    {
        $this->smsData = $data;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isConnect = $this->smsData->getIsConnect();
        $isBuy = $this->smsData->getIsBusy();
        $builder
            ->add('isConnect', 'choice', array(
                    'label'=>'Connect the transmit enable',
                    'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                    'choices'=>array('Disable','Enable'),
                    'data'=>empty($isConnect) ? 0 : $isConnect,
                    'expanded'=>true,
                    'multiple'=>false
                ))
            ->add('connectSms', 'textarea', array('attr'=>array('class'=>'col-xs-10 col-sm-5'),'label'=>'Content','required'=>false))
            ->add('isBusy', 'choice', array(
                    'label'=>'Enable is not switched on send',
                    'attr'=>array('class'=>'col-xs-10 col-sm-5 isbusy'),
                    'choices'=>array('Disable','Enable'),
                    'data'=>empty($isBuy) ? 0 : $isBuy,
                    'expanded'=>true,
                    'multiple'=>false
                ))
            ->add('busySms', 'textarea', array('attr'=>array('class'=>'col-xs-10 col-sm-5'),'label'=>'Content', 'required'=>false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Icsoc\SecurityBundle\Entity\SwjHangupSms',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'smsContentForm';
    }
}
