<?php
namespace Icsoc\SecurityBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SecurityWinAgentType extends AbstractType
{
    /**
     * @var
     * 服务
     */
    private $server;
    public function __construct(ContainerInterface $container)
    {
        $this->server = $container;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', 'password', array(
                    'attr'=>array('class'=>'col-xs-10 col-sm-5'),
                    'label'=>'old password',
                ))->add('agPassword', 'repeated', array(
                    'options'=>array('attr'=>array('class'=>'col-xs-10 col-sm-5')),
                    'type'=>'password',
                    'required'=>true,
                    'mapped' => false,
                    'invalid_message'=>'This value is not valid',
                    'first_options'  => array('label' => 'new password'),
                    'second_options' => array('label' => 'confirm password'),
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Icsoc\SecurityBundle\Entity\WinAgent',
            ));

    }

    public function getName()
    {
        return 'securityWinAgent';
    }
}
