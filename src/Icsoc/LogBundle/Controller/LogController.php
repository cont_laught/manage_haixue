<?php

namespace Icsoc\LogBundle\Controller;

use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * 系统日志
 * Class LogController
 * @package Icsoc\LogBundle\Controller
 */
class LogController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        $options = array(
            'keyword'=>array('text'=>'Keyword', 'type'=>1, 'help'=>'The operator or content'),
            'ip'=>array('text'=>'Ip', 'type'=>1),
        );

        return $this->render('IcsocLogBundle:Log:index.html.twig', array(
            'options'=>$options,
        ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'asc');
        $sidx = $request->get('sidx', 'logId');
        $fliter = $request->get('fliter', '');
        /** @var  $em \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();
        $where = ' AND log.vccId = :vcc_id ';
        $parameter['vcc_id']  = $this->getUser()->getVccId();
        if (!empty($fliter)) {
            $json = json_decode($fliter, true);
            if (isset($json['keyword']) && !empty($json['keyword'])) {
                $where.="AND (log.content LIKE '%".$json['keyword']."%' or log.userNum LIKE '%".$json['keyword']."%') ";
            }
            if (isset($json['ip']) && !empty($json['ip'])) {
                $where.="AND log.actip LIKE '%".$json['ip']."%' ";
            }
            if (isset($json['start_time']) && !empty($json['start_time'])) {
                $where.="AND log.acttime >= :start_time ";
                $parameter['start_time'] = strtotime($json['start_time']);
            }
            if (isset($json['end_time']) && !empty($json['end_time'])) {
                $where.="AND log.acttime <= :end_time ";
                $parameter['end_time'] = strtotime($json['end_time']);
            }
        }
        $query = $em->createQuery(
            "SELECT count(log)
            FROM IcsocSecurityBundle:CcLogs log
            WHERE 1=1 ".$where
        )->setParameters($parameter);
        try {
            $single = $query->getSingleResult();
            $count = $single['1'];
        } catch (NoResultException $e) {
            $count = 0;
        }
        if ($count > 0) {
            $totalPage = ceil($count/$rows);
        } else {
            $totalPage = 1;
        }
        if ($page > $totalPage) {
            $page = $totalPage;
        }
        $start = ($page -1 )*$rows;
        /** @var  $query \Doctrine\ORM\Query */
        $query = $em->createQuery(
            "SELECT log
            FROM IcsocSecurityBundle:CcLogs log
            WHERE 1=1 $where
            ORDER BY log.".$sidx.' '.$sord
        )->setParameters($parameter);
        $query->setFirstResult($start);
        $query->setMaxResults($rows);
        $all = $query->getArrayResult();
        $logType = $this->container->getParameter('log_action_type');
        foreach ($all as $k => $v) {
            /** @var  $v  \Icsoc\SecurityBundle\Entity\CcLogs */
            $all[$k]['action'] = isset($logType[$v['action']]) ? $logType[$v['action']] : '';
            $all[$k]['acttime'] = date("Y-m-d H:i:s", $v['acttime']);
        }
        $reponse['rows'] = $all;
        $reponse['total'] = $totalPage;
        $reponse['records'] = $count;

        $serializer = $this->get('serializer');
        $response = $serializer->serialize($reponse, 'json');

        return new Response($response);
    }
}
