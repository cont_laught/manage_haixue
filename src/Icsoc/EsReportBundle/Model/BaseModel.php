<?php
/**
 * This file is part of cdr-bundle.
 * Author: tangzhou
 * Date: 2016/5/4
 * Time: 13:44
 * File: BaseModel.php
 */

namespace Icsoc\EsReportBundle\Model;

/**
 * Class BaseModel
 * @package Icsoc\EsReportBundle\Model
 */
class BaseModel
{
    /**
     * 统计的所有指标
     * text为指标的名称
     *
     * @var array
     */
    protected $reportItems = array(
        'system'=>array(
            'fixed_report'=>array(
                'ivrInboundTotalNum'=>array('text'=>'Ivr Total Num', 'field'=>'ivrInboundTotalNum', 'sortable'=>true, 'width'=>100, 'default_show'=> true),
                'inboundTotalNum'=>array('text'=>'Queue Total Num', 'field'=>'inboundTotalNum', 'sortable'=>true, 'width'=>100, 'default_show'=> true),
                'inboundRate'=>array('text'=>'Queue Service Rate', 'field'=>'inboundRate', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'inboundConnNum'=>array('text'=>'Callin conn Num', 'field'=>'inboundConnNum', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'inboundConnRate'=>array('text'=>'Callin conn Rate', 'field'=>'inboundConnRate', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'inboundConnTotalSecs'=>array('text'=>'Callin Total Secs', 'field'=>'inboundConnTotalSecs', 'sortable'=>true, 'width'=>130, 'default_show'=> true),
                'inboundConnAvgSecs'=>array('text'=>'Callin avg Secs', 'field'=>'inboundConnAvgSecs', 'sortable'=>true, 'width'=>130, 'default_show'=> true),
                //呼入X秒，X秒接通率
                'outboundTotalNum'=>array('text'=>'Callout total Num', 'field'=>'outboundTotalNum', 'sortable'=>false, 'width'=>120, 'default_show'=> true),
                'outboundConnNum'=>array('text'=>'Callout conn Num', 'field'=>'outboundConnNum', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'outboundConnRate'=>array('text'=>'Callout conn Rate', 'field'=>'outboundConnRate', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'outboundConnTotalSecs'=>array('text'=>'Callout total Secs', 'field'=>'outboundConnTotalSecs', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'outboundConnAvgSecs'=>array('text'=>'Callout avg Secs', 'field'=>'outboundConnAvgSecs', 'sortable'=>false, 'width'=>130, 'default_show'=> true),
                'inboundAbandonTotalNum'=>array('text'=>'Callout Lost total Num', 'field'=>'inboundAbandonTotalNum', 'sortable'=>true, 'width'=>130, 'default_show'=> true),
                'inboundAbandonRate'=>array('text'=>'Callout Lost Rate', 'field'=>'inboundAbandonRate', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'queueTotalNum'=>array('text'=>'Wait Total num', 'field'=>'queueTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'queueTotalSecs'=>array('text'=>'Wait Total Secs', 'field'=>'queueTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'queueAvgSecs'=>array('text'=>'Wait Avg Secs', 'field'=>'queueAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'connQueueTotalNum'=>array('text'=>'Wait Conn Total Num', 'field'=>'connQueueTotalNum', 'sortable'=>true, 'width'=>150, 'default_show'=> true),
                'connQueueTotalSecs'=>array('text'=>'Wait Conn Total Secs', 'field'=>'connQueueTotalSecs', 'sortable'=>true, 'width'=>150, 'default_show'=> true),
                'connQueueAvgSecs'=>array('text'=>'Wait Conn Avg Secs', 'field'=>'connQueueAvgSecs', 'sortable'=>true, 'width'=>160, 'default_show'=> true),
                'ringTotalNum'=>array('text'=>'Ring Total Num', 'field'=>'ringTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'ringTotalSecs'=>array('text'=>'Ring Total Secs', 'field'=>'ringTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'ringAvgSecs'=>array('text'=>'Ring Avg Secs', 'field'=>'ringAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'loginTotalSecs'=>array('text'=>'Login Total Secs', 'field'=>'loginTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'loginTotalNum'=>array('text'=>'Login Total Num', 'field'=>'loginTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'readyTotalSecs'=>array('text'=>'Ready Total Secs', 'field'=>'readyTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyTotalNum'=>array('text'=>'Busy Total Num', 'field'=>'busyTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyTotalSecs'=>array('text'=>'Busy Total Secs', 'field'=>'busyTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyAvgSecs'=>array('text'=>'Busy Avg Secs', 'field'=>'busyAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'waitTotalNum'=>array('text'=>'Post Total Num', 'field'=>'waitTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'waitTotalSecs'=>array('text'=>'Post Total Secs', 'field'=>'waitTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'waitAvgSecs'=>array('text'=>'Post Avg Secs', 'field'=>'waitAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'workRate'=>array('text'=>'Work Rate', 'field'=>'workRate', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
            ),
            'custom_report'=>array(
                'ivrInboundTotalNum' => array('text' => 'ivr Inbound Total Num', 'monitor' => true),
                'inboundTotalNum' => array('text' => 'inbound Total Num', 'monitor' => true),
                'inboundRate' => array('text' => 'inbound Rate', 'monitor' => true),
                'inboundConnNum' => array('text' => 'inbound Conn Num', 'monitor' => true),
                'inboundConnRate' => array('text' => 'inbound Conn Rate', 'monitor' => true),
                'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs'),
                'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs'),
                'outboundTotalNum' => array('text' => 'outbound Total Num', 'monitor' => true),
                'outboundConnNum' => array('text' => 'outbound Conn Num', 'monitor' => true),
                'outboundConnRate' => array('text' => 'outbound Conn Rate', 'monitor' => true),
                'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs'),
                'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs'),
                'validConnTotalNum' => array('text' => 'valid Conn Total Num', 'monitor' => true),
                'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num', 'monitor' => true),
                'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num', 'monitor' => true),
                'validConnTotalSecs' => array('text' => 'valid Conn Total Secs'),
                'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs'),
                'inboundValidConnTotalSecs' => array('text' => 'inbound Valid Conn Total Secs'),
                'inboundValidConnAvgSecs' => array('text' => 'inbound Valid Conn Avg Secs'),
                'outboundValidConnTotalSecs' => array('text' => 'outbound Valid Conn Total Secs'),
                'outboundValidConnAvgSecs' => array('text' => 'outbound Valid Conn Avg Secs'),
                'inboundConnInXSecsNum' => array('text' => 'inbound Conn In %X% Secs Num', 'monitor' => true),
                'inboundConnInXSecsRate' => array('text' => 'inbound Conn In %X% Secs Rate', 'monitor' => true),
                'inboundAbandonTotalNum' => array('text' => 'inbound Abandon Total Num', 'monitor' => true),
                'inboundAbandonRate' => array('text' => 'inbound Abandon Rate', 'monitor' => true),
                'inboundAbandonInXSecsNum' => array('text' => 'inbound Abandon In %X% Secs Num', 'monitor' => true),
                'inboundAbandonInXSecsRate' => array('text' => 'inbound Abandon In %X% Secs Rate', 'monitor' => true),
                'queueTotalNum' => array('text' => 'queue Total Num', 'monitor' => true),
                'queueTotalSecs' => array('text' => 'queue Total Secs'),
                'queueAvgSecs' => array('text' => 'queue Avg Secs'),
                'connQueueTotalNum' => array('text' => 'conn Queue Total Num', 'monitor' => true),
                'connQueueTotalSecs' => array('text' => 'conn Queue Total Secs'),
                'connQueueAvgSecs' => array('text' => 'conn Queue Avg Secs'),
                'ringTotalNum' => array('text' => 'ring Total Num', 'monitor' => true),
                'ringTotalSecs' => array('text' => 'ring Total Secs'),
                'ringAvgSecs' => array('text' => 'ring Avg Secs'),
                'loginTotalNum' => array('text' => 'login Total Num'),
                'loginTotalSecs' => array('text' => 'login Total Secs'),
                'readyTotalSecs' => array('text' => 'ready Total Secs'),
                'busyTotalNum' => array('text' => 'busy Total Num', 'monitor' => true),
                'busyTotalSecs' => array('text' => 'busy Total Secs'),
                'busyAvgSecs' => array('text' => 'busy Avg Secs'),
                'waitTotalNum' => array('text' => 'wait Total Num', 'monitor' => true),
                'waitTotalSecs' => array('text' => 'wait Total Secs'),
                'waitAvgSecs' => array('text' => 'wait Avg Secs'),
                'workRate' => array('text' => 'work Rate', 'monitor' => true),
            ),

        ),
        'queue'=>array(
            'fixed_report'=>array(
                'inboundTotalNum' => array('text'=>'Callin Total Num', 'field'=>'inboundTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'inboundConnNum' => array('text'=>'Callin conn Num', 'field'=>'inboundConnNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'inboundConnTotalSecs' => array('text'=>'Callin Total Secs', 'field'=>'inboundConnTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'inboundConnAvgSecs' => array('text'=>'Callin avg Secs', 'field'=>'inboundConnAvgSecs', 'sortable'=>false, 'width'=>120, 'default_show'=> true),
                'inboundConnRate' => array('text'=>'Callin conn Rate', 'field'=>'inboundConnRate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'outboundTotalNum' => array('text'=>'Callout total Num', 'field'=>'outboundTotalNum', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'outboundConnNum' => array('text'=>'Callout Conn Total Num', 'field'=>'outboundConnNum', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'outboundConnTotalSecs' => array('text'=>'Callout total Secs', 'field'=>'outboundConnTotalSecs', 'sortable'=>false, 'width'=>115, 'default_show'=> true),
                'outboundConnAvgSecs' => array('text'=>'Callout avg Secs', 'field'=>'outboundConnAvgSecs', 'sortable'=>false, 'width'=>115, 'default_show'=> true),
                'outboundConnRate' => array('text'=>'Callout conn Rate', 'field'=>'outboundConnRate', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'inboundAbandonTotalNum' => array('text'=>'Lost Total Num', 'field'=>'inboundAbandonTotalNum', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'inboundAbandonTotalSecs' => array('text'=>'Lost Total Secs', 'field'=>'inboundAbandonTotalSecs', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'inboundAbandonAvgSecs' => array('text'=>'Lost Avg Secs', 'field'=>'inboundAbandonAvgSecs', 'sortable'=>false, 'width'=>115, 'default_show'=> true),
                'inboundCallerAbandonTotalNum' => array('text'=>'Caller Lost Total Num', 'field'=>'inboundCallerAbandonTotalNum', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'inboundCallerAbandonTotalSecs' => array('text'=>'Caller Lost Total Secs', 'field'=>'inboundCallerAbandonTotalSecs', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'inboundCallerAbandonAvgSecs' => array('text'=>'Caller Lost Avg Secs', 'field'=>'inboundCallerAbandonAvgSecs', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'inboundQueueTimeoutTotalNum' => array('text'=>'Wait Timeout Total Num', 'field'=>'inboundQueueTimeoutTotalNum', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'inboundQueueTimeoutTotalSecs' => array('text'=>'Wait Timeout Total Secs', 'field'=>'inboundQueueTimeoutTotalSecs', 'sortable'=>false, 'width'=>115, 'default_show'=> true),
                'inboundQueueTimeoutAvgSecs' => array('text'=>'Wait Timeout Avg Secs', 'field'=>'inboundQueueTimeoutAvgSecs', 'sortable'=>false, 'width'=>115, 'default_show'=> true),
                'inboundQueueOverflowTotalNum' => array('text'=>'Queue Overflow Total Num', 'field'=>'inboundQueueOverflowTotalNum', 'sortable'=>false, 'width'=>120, 'default_show'=> true),
                'inboundQueueOverflowTotalSecs' => array('text'=>'Queue Overflow Total Secs', 'field'=>'inboundQueueOverflowTotalSecs', 'sortable'=>false, 'width'=>130, 'default_show'=> true),
                'inboundQueueOverflowAvgSecs' => array('text'=>'Queue Overflow Avg Secs', 'field'=>'inboundQueueOverflowAvgSecs', 'sortable'=>false, 'width'=>140, 'default_show'=> true),
                'inboundNoAgentTotalNum' => array('text'=>'Lost NoAgent Total Num', 'field'=>'inboundNoAgentTotalNum', 'sortable'=>false, 'width'=>120, 'default_show'=> true),
                'inboundNoAgentTotalSecs' => array('text'=>'Lost NoAgent Total Secs', 'field'=>'inboundNoAgentTotalSecs', 'sortable'=>false, 'width'=>125, 'default_show'=> true),
                'inboundNoAgentAvgSecs' => array('text'=>'Lost NoAgent Avg Secs', 'field'=>'inboundNoAgentAvgSecs', 'sortable'=>false, 'width'=>130, 'default_show'=> true),
                'queueTotalNum'=>array('text'=>'Wait Total num', 'field'=>'queueTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'queueTotalSecs'=>array('text'=>'Wait Total Secs', 'field'=>'queueTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'queueAvgSecs'=>array('text'=>'Wait Avg Secs', 'field'=>'queueAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'connQueueTotalNum'=>array('text'=>'Wait Conn Total Num', 'field'=>'connQueueTotalNum', 'sortable'=>true, 'width'=>150, 'default_show'=> true),
                'connQueueTotalSecs'=>array('text'=>'Wait Conn Total Secs', 'field'=>'connQueueTotalSecs', 'sortable'=>true, 'width'=>150, 'default_show'=> true),
                'connQueueAvgSecs'=>array('text'=>'Wait Conn Avg Secs', 'field'=>'connQueueAvgSecs', 'sortable'=>true, 'width'=>160, 'default_show'=> true),
                'ringTotalNum'=>array('text'=>'Ring Total Num', 'field'=>'ringTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'ringTotalSecs'=>array('text'=>'Ring Total Secs', 'field'=>'ringTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'ringAvgSecs'=>array('text'=>'Ring Avg Secs', 'field'=>'ringAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyTotalNum'=>array('text'=>'Busy Total Num', 'field'=>'busyTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyTotalSecs'=>array('text'=>'Busy Total Secs', 'field'=>'busyTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyAvgSecs'=>array('text'=>'Busy Avg Secs', 'field'=>'busyAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
            ),
            'custom_report'=>array(
                'inboundTotalNum' => array('text' => 'inbound Total Num', 'monitor' => true),
                'inboundConnNum' => array('text' => 'inbound Conn Num', 'monitor' => true),
                'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs'),
                'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs'),
                'inboundConnRate' => array('text' => 'inbound Conn Rate', 'monitor' => true),
                'outboundTotalNum' => array('text' => 'outbound Total Num'),
                'outboundConnNum' => array('text' => 'outbound Conn Num'),
                'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs'),
                'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs'),
                'outboundConnRate' => array('text' => 'outbound Conn Rate'),
                'validConnTotalNum' => array('text' => 'valid Conn Total Num'),
                'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num', 'monitor' => true),
                'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num'),
                'validConnTotalSecs' => array('text' => 'valid Conn Total Secs'),
                'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs'),
                'inboundValidConnTotalSecs' => array('text' => 'inbound Valid Conn Total Secs'),
                'inboundValidConnAvgSecs' => array('text' => 'inbound Valid Conn Avg Secs'),
                'outboundValidConnTotalSecs' => array('text' => 'outbound Valid Conn Total Secs'),
                'outboundValidConnAvgSecs' => array('text' => 'outbound Valid Conn Avg Secs'),
                'inboundConnInXSecsNum' => array('text' => 'inbound Conn In %X% Secs Num', 'monitor' => true),
                'inboundConnInXSecsRate' => array('text' => 'inbound Conn In %X% Secs Rate', 'monitor' => true),
                'inboundAbandonTotalNum' => array('text' => 'inbound Abandon Total Num', 'monitor' => true),
                'inboundAbandonTotalSecs' => array('text' => 'inbound Abandon Total Secs'),
                'inboundAbandonAvgSecs' => array('text' => 'inbound Abandon Avg Secs'),
                'abandonReasonTotalNum1' => array('text' => 'Inbound Abandon Reason1 Total Num', 'monitor' => true),
                'abandonReasonTotalSecs1' => array('text' => 'Inbound Abandon Reason1 Total Secs'),
                'abandonReasonAvgSecs1' => array('text' => 'Inbound Abandon Reason1 Avg Secs'),
                'abandonReasonTotalNum3' => array('text' => 'Inbound Abandon Reason3 Total Num', 'monitor' => true),
                'abandonReasonTotalSecs3' => array('text' => 'Inbound Abandon Reason3 Total Secs'),
                'abandonReasonAvgSecs3' => array('text' => 'Inbound Abandon Reason3 Avg Secs'),
                'abandonReasonTotalNum4' => array('text' => 'Inbound Abandon Reason4 Total Num', 'monitor' => true),
                'abandonReasonTotalSecs4' => array('text' => 'Inbound Abandon Reason4 Total Secs'),
                'abandonReasonAvgSecs4' => array('text' => 'Inbound Abandon Reason4 Avg Secs'),
                'abandonReasonTotalNum5' => array('text' => 'Inbound Abandon Reason5 Total Num', 'monitor' => true),
                'abandonReasonTotalSecs5' => array('text' => 'Inbound Abandon Reason5 Total Secs'),
                'abandonReasonAvgSecs5' => array('text' => 'Inbound Abandon Reason5 Avg Secs'),
                'inboundAbandonInXSecsNum' => array('text' => 'inbound Abandon In %X% Secs Num', 'monitor' => true),
                'inboundAbandonInXSecsRate' => array('text' => 'inbound Abandon In %X% Secs Rate', 'monitor' => true),
                'queueTotalNum' => array('text' => 'queue Total Num', 'monitor' => true),
                'queueTotalSecs' => array('text' => 'queue Total Secs'),
                'queueAvgSecs' => array('text' => 'queue Avg Secs'),
                'connQueueTotalNum' => array('text' => 'conn Queue Total Num', 'monitor' => true),
                'connQueueTotalSecs' => array('text' => 'conn Queue Total Secs'),
                'connQueueAvgSecs' => array('text' => 'conn Queue Avg Secs'),
                'ringTotalNum' => array('text' => 'ring Total Num', 'monitor' => true),
                'ringTotalSecs' => array('text' => 'ring Total Secs'),
                'ringAvgSecs' => array('text' => 'ring Avg Secs'),
                'waitTotalNum' => array('text' => 'wait Total Num', 'monitor' => true),
                'waitTotalSecs' => array('text' => 'wait Total Secs'),
                'waitAvgSecs' => array('text' => 'wait Avg Secs'),
            ),

        ),
        'group'=>array(
            'fixed_report'=>array(
                'inboundConnNum' => array('text'=>'Callin conn Num', 'field'=>'inboundConnNum', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'inboundConnTotalSecs' => array('text'=>'Callin Total Secs', 'field'=>'inboundConnTotalSecs', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'inboundConnAvgSecs' => array('text'=>'Callin avg Secs', 'field'=>'inboundConnAvgSecs', 'sortable'=>false, 'width'=>120, 'default_show'=> true),
                'outboundTotalNum' => array('text'=>'Callout total Num', 'field'=>'outboundTotalNum', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'outboundConnNum' => array('text'=>'Callout Conn Total Num', 'field'=>'outboundConnNum', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'outboundConnTotalSecs' => array('text'=>'Callout total Secs', 'field'=>'outboundConnTotalSecs', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'outboundConnAvgSecs' => array('text'=>'Callout avg Secs', 'field'=>'outboundConnAvgSecs', 'sortable'=>false, 'width'=>115, 'default_show'=> true),
                'connTotalNum' => array('text'=>'Conn Total Num', 'field'=>'connTotalNum', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'connTotalSecs' => array('text'=>'Conn Total Secs', 'field'=>'connTotalSecs', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'connAvgSecs' => array('text'=>'Conn Avg Secs', 'field'=>'connAvgSecs', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'internalConnNum' => array('text'=>'Internal Conn Total Num', 'field'=>'internalConnNum', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'internalConnTotalSecs' => array('text'=>'Internal Conn Total Secs', 'field'=>'internalConnTotalSecs', 'sortable'=>false, 'width'=>115, 'default_show'=> true),
                'internalConnAvgSecs' => array('text'=>'Internal Conn Avg Secs', 'field'=>'internalConnAvgSecs', 'sortable'=>false, 'width'=>115, 'default_show'=> true),
                'waitTotalNum'=>array('text'=>'Post Total Num', 'field'=>'waitTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'waitTotalSecs'=>array('text'=>'Post Total Secs', 'field'=>'waitTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'waitAvgSecs'=>array('text'=>'Post Avg Secs', 'field'=>'waitAvgSecs', 'sortable'=>true, 'width'=>115, 'default_show'=> true),
                'ringTotalNum'=>array('text'=>'Ring Total Num', 'field'=>'ringTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'ringTotalSecs'=>array('text'=>'Ring Total Secs', 'field'=>'ringTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'ringAvgSecs'=>array('text'=>'Ring Avg Secs', 'field'=>'ringAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'consultNum'=>array('text'=>'Consult Total Num', 'field'=>'consultNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'consultTotalSecs'=>array('text'=>'Consult Total Secs', 'field'=>'consultTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'consultAvgSecs'=>array('text'=>'Consult Avg Secs', 'field'=>'consultAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'holdNum'=>array('text'=>'Hold Total Num', 'field'=>'holdNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'hlodTotalSecs'=>array('text'=>'Hold Total Secs', 'field'=>'hlodTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'holdAvgSecs'=>array('text'=>'Hold Avg Secs', 'field'=>'holdAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'conferenceNum'=>array('text'=>'Conference Total Num', 'field'=>'conferenceNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'conferenceTotalSecs'=>array('text'=>'Conference Total Secs', 'field'=>'conferenceTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'conferenceAvgSecs'=>array('text'=>'Conference Avg Secs', 'field'=>'conferenceAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'transferTotalNum'=>array('text'=>'Transfer Total Num', 'field'=>'transferTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'transferedConnNum'=>array('text'=>'Transfer Conn Total Num', 'field'=>'transferedConnNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'transferedConnTotalSecs'=>array('text'=>'Transfer Conn Total Secs', 'field'=>'transferedConnTotalSecs', 'sortable'=>true, 'width'=>115, 'default_show'=> true),
                'transferedConnAvgSecs'=>array('text'=>'Transfer Conn Avg Secs', 'field'=>'transferedConnAvgSecs', 'sortable'=>true, 'width'=>130, 'default_show'=> true),
                'loginTotalSecs'=>array('text'=>'Login Total Secs', 'field'=>'loginTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'loginTotalNum'=>array('text'=>'Login Total Num', 'field'=>'loginTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'readyTotalSecs'=>array('text'=>'Ready Total Secs', 'field'=>'readyTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyTotalNum'=>array('text'=>'Busy Total Num', 'field'=>'busyTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyTotalSecs'=>array('text'=>'Busy Total Secs', 'field'=>'busyTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyAvgSecs'=>array('text'=>'Busy Avg Secs', 'field'=>'busyAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'workRate'=>array('text'=>'Work Rate', 'field'=>'workRate', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
            ),
            'custom_report' => array(
                'inboundConnNum' => array('text' => 'inbound Conn Num', 'parent' => '', 'monitor' => true),
                'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs', 'parent' => 'inboundConnNum'),
                'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs', 'parent' => 'inboundConnNum'),
                'outboundTotalNum' => array('text' => 'outbound Total Num', 'parent' => ''),
                'outboundConnNum' => array('text' => 'outbound Conn Num', 'parent' => '', 'monitor' => true),
                'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs', 'parent' => 'outboundConnNum'),
                'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs', 'parent' => 'outboundConnNum'),
                'validConnTotalNum' => array('text' => 'valid Conn Total Num', 'parent' => '', 'monitor' => true),
                'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num', 'parent' => ''),
                'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num', 'parent' => ''),
                'connTotalNum' => array('text' => 'conn Total Num', 'parent' => '', 'monitor' => true),
                'connTotalSecs' => array('text' => 'conn Total Secs', 'parent' => 'connTotalNum'),
                'connAvgSecs' => array('text' => 'conn Avg Secs', 'parent' => 'connTotalNum'),
                'validConnTotalSecs' => array('text' => 'valid Conn Total Secs', 'parent' => 'validConnTotalNum'),
                'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs', 'parent' => 'validConnTotalNum'),
                'inboundValidConnTotalSecs' => array(
                    'text' => 'inbound Valid Conn Total Secs', 'parent' => 'inboundValidConnTotalNum',
                ),
                'inboundValidConnAvgSecs' => array(
                    'text' => 'inbound Valid Conn Avg Secs', 'parent' => 'inboundValidConnTotalNum',
                ),
                'outboundValidConnTotalSecs' => array(
                    'text' => 'outbound Valid Conn Total Secs', 'parent' => 'outboundValidConnTotalNum',
                ),
                'outboundValidConnAvgSecs' => array(
                    'text' => 'outbound Valid Conn Avg Secs', 'parent' => 'outboundValidConnTotalNum',
                ),
                'internalConnNum' => array('text' => 'internal Conn Num', 'parent' => '', 'monitor' => true),
                'internalConnTotalSecs' => array('text' => 'internal Conn Total Secs', 'parent' => 'internalConnNum'),
                'internalConnAvgSecs' => array('text' => 'internal Conn Avg Secs', 'parent' => 'internalConnNum'),
                'waitTotalNum' => array('text' => 'wait Total Num', 'parent' => '', 'monitor' => true),
                'waitTotalSecs' => array('text' => 'wait Total Secs', 'parent' => 'waitTotalNum'),
                'waitAvgSecs' => array('text' => 'wait Avg Secs', 'parent' => 'waitTotalNum'),
                'ringTotalNum' => array('text' => 'ring Total Num', 'parent' => '', 'monitor' => true),
                'ringTotalSecs' => array('text' => 'ring Total Secs', 'parent' => 'ringTotalNum'),
                'ringAvgSecs' => array('text' => 'ring Avg Secs', 'parent' => 'ringTotalNum'),
                'consultNum' => array('text' => 'consult Num', 'parent' => ''),
                'consultTotalSecs' => array('text' => 'consult Total Secs', 'parent' => 'consultNum'),
                'consultAvgSecs' => array('text' => 'consult Avg Secs', 'parent' => 'consultNum'),
                'holdNum' => array('text' => 'hold Num', 'parent' => '', 'monitor' => true),
                'hlodTotalSecs' => array('text' => 'hlod Total Secs', 'parent' => 'holdNum'),
                'holdAvgSecs' => array('text' => 'hold Avg Secs', 'parent' => 'holdNum'),
                'conferenceNum' => array('text' => 'conference Num', 'parent' => '', 'monitor' => true),
                'conferenceTotalSecs' => array('text' => 'conference Total Secs', 'parent' => 'conferenceNum'),
                'conferenceAvgSecs' => array('text' => 'conference Avg Secs', 'parent' => 'conferenceNum'),
                'transferTotalNum' => array('text' => 'transfer Total Num', 'parent' => ''),
                'transferedConnNum' => array('text' => 'transfered Conn Num', 'parent' => '', 'monitor' => true),
                'transferedConnTotalSecs' => array('text' => 'transfered Conn Total Secs', 'parent' => 'transferedConnNum'),
                'transferedConnAvgSecs' => array('text' => 'transfered Conn Avg Secs', 'parent' => 'transferedConnNum'),
                'loginTotalNum' => array('text' => 'login Total Num', 'parent' => '', 'monitor' => true),
                'loginTotalSecs' => array('text' => 'login Total Secs', 'parent' => 'loginTotalNum'),
                'readyTotalSecs' => array('text' => 'ready Total Secs', 'parent' => ''),
                'busyTotalNum' => array('text' => 'busy Total Num', 'parent' => '', 'monitor' => true),
                'busyTotalSecs' => array('text' => 'busy Total Secs', 'parent' => 'busyTotalNum'),
                'busyAvgSecs' => array('text' => 'busy Avg Secs', 'parent' => 'busyTotalNum'),
                'busyRate' => array('text' => 'busy Rate', 'parent' => '', 'monitor' => true),
                'workRate' => array('text' => 'work Rate', 'parent' => '', 'monitor' => true),
            ),
        ),
        'agent'=>array(
            'fixed_report'=>array(
                'inboundConnNum' => array('text'=>'Callin conn Num', 'field'=>'inboundConnNum', 'sortable'=>true, 'width'=>80, 'default_show'=> true),
                'inboundConnTotalSecs' => array('text'=>'Callin Total Secs', 'field'=>'inboundConnTotalSecs', 'sortable'=>true, 'width'=>120, 'default_show'=> true),
                'inboundConnAvgSecs' => array('text'=>'Callin avg Secs', 'field'=>'inboundConnAvgSecs', 'sortable'=>false, 'width'=>115, 'default_show'=> true),
                'outboundTotalNum' => array('text'=>'Callout total Num', 'field'=>'outboundTotalNum', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'outboundConnNum' => array('text'=>'Callout Conn Total Num', 'field'=>'outboundConnNum', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'outboundConnTotalSecs' => array('text'=>'Callout total Secs', 'field'=>'outboundConnTotalSecs', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'outboundConnAvgSecs' => array('text'=>'Callout avg Secs', 'field'=>'outboundConnAvgSecs', 'sortable'=>false, 'width'=>120, 'default_show'=> true),
                'connTotalNum' => array('text'=>'Conn Total Num', 'field'=>'connTotalNum', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'connTotalSecs' => array('text'=>'Conn Total Secs', 'field'=>'connTotalSecs', 'sortable'=>false, 'width'=>80, 'default_show'=> true),
                'connAvgSecs' => array('text'=>'Conn Avg Secs', 'field'=>'connAvgSecs', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'internalConnNum' => array('text'=>'Internal Conn Total Num', 'field'=>'internalConnNum', 'sortable'=>false, 'width'=>100, 'default_show'=> true),
                'internalConnTotalSecs' => array('text'=>'Internal Conn Total Secs', 'field'=>'internalConnTotalSecs', 'sortable'=>false, 'width'=>110, 'default_show'=> true),
                'internalConnAvgSecs' => array('text'=>'Internal Conn Avg Secs', 'field'=>'internalConnAvgSecs', 'sortable'=>false, 'width'=>115, 'default_show'=> true),
                'waitTotalNum'=>array('text'=>'Post Total Num', 'field'=>'waitTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'waitTotalSecs'=>array('text'=>'Post Total Secs', 'field'=>'waitTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'waitAvgSecs'=>array('text'=>'Post Avg Secs', 'field'=>'waitAvgSecs', 'sortable'=>true, 'width'=>115, 'default_show'=> true),
                'ringTotalNum'=>array('text'=>'Ring Total Num', 'field'=>'ringTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'ringTotalSecs'=>array('text'=>'Ring Total Secs', 'field'=>'ringTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'ringAvgSecs'=>array('text'=>'Ring Avg Secs', 'field'=>'ringAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'consultNum'=>array('text'=>'Consult Total Num', 'field'=>'consultNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'consultTotalSecs'=>array('text'=>'Consult Total Secs', 'field'=>'consultTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'consultAvgSecs'=>array('text'=>'Consult Avg Secs', 'field'=>'consultAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'holdNum'=>array('text'=>'Hold Total Num', 'field'=>'holdNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'hlodTotalSecs'=>array('text'=>'Hold Total Secs', 'field'=>'hlodTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'holdAvgSecs'=>array('text'=>'Hold Avg Secs', 'field'=>'holdAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'conferenceNum'=>array('text'=>'Conference Total Num', 'field'=>'conferenceNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'conferenceTotalSecs'=>array('text'=>'Conference Total Secs', 'field'=>'conferenceTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'conferenceAvgSecs'=>array('text'=>'Conference Avg Secs', 'field'=>'conferenceAvgSecs', 'sortable'=>true, 'width'=>115, 'default_show'=> true),
                'transferTotalNum'=>array('text'=>'Transfer Total Num', 'field'=>'transferTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'transferedConnNum'=>array('text'=>'Transfer Conn Total Num', 'field'=>'transferedConnNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'transferedConnTotalSecs'=>array('text'=>'Transfer Conn Total Secs', 'field'=>'transferedConnTotalSecs', 'sortable'=>true, 'width'=>115, 'default_show'=> true),
                'transferedConnAvgSecs'=>array('text'=>'Transfer Conn Avg Secs', 'field'=>'transferedConnAvgSecs', 'sortable'=>true, 'width'=>125, 'default_show'=> true),
                'loginTotalSecs'=>array('text'=>'Login Total Secs', 'field'=>'loginTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'loginTotalNum'=>array('text'=>'Login Total Num', 'field'=>'loginTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'readyTotalSecs'=>array('text'=>'Ready Total Secs', 'field'=>'readyTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyTotalNum'=>array('text'=>'Busy Total Num', 'field'=>'busyTotalNum', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyTotalSecs'=>array('text'=>'Busy Total Secs', 'field'=>'busyTotalSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'busyAvgSecs'=>array('text'=>'Busy Avg Secs', 'field'=>'busyAvgSecs', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
                'workRate'=>array('text'=>'Work Rate', 'field'=>'workRate', 'sortable'=>true, 'width'=>110, 'default_show'=> true),
            ),
            'custom_report'=>array(
                'inboundConnNum' => array('text' => 'inbound Conn Num'),
                'inboundConnTotalSecs' => array('text' => 'inbound Conn Total Secs'),
                'inboundConnAvgSecs' => array('text' => 'inbound Conn Avg Secs'),
                'outboundTotalNum' => array('text' => 'outbound Total Num'),
                'outboundConnNum' => array('text' => 'outbound Conn Num'),
                'outboundConnTotalSecs' => array('text' => 'outbound Conn Total Secs'),
                'outboundConnAvgSecs' => array('text' => 'outbound Conn Avg Secs'),
                'validConnTotalNum' => array('text' => 'valid Conn Total Num'),
                'inboundValidConnTotalNum' => array('text' => 'inbound Valid Conn Total Num'),
                'outboundValidConnTotalNum' => array('text' => 'outbound Valid Conn Total Num'),
                'connTotalNum' => array('text' => 'conn Total Num'),
                'connTotalSecs' => array('text' => 'conn Total Secs'),
                'connAvgSecs' => array('text' => 'conn Avg Secs'),
                'validConnTotalSecs' => array('text' => 'valid Conn Total Secs'),
                'validConnAvgSecs' => array('text' => 'valid Conn Avg Secs'),
                'inboundValidConnTotalSecs' => array('text' => 'inbound Valid Conn Total Secs'),
                'inboundValidConnAvgSecs' => array('text' => 'inbound Valid Conn Avg Secs'),
                'outboundValidConnTotalSecs' => array('text' => 'outbound Valid Conn Total Secs'),
                'outboundValidConnAvgSecs' => array('text' => 'outbound Valid Conn Avg Secs'),
                'internalConnNum' => array('text' => 'internal Conn Num'),
                'internalConnTotalSecs' => array('text' => 'internal Conn Total Secs'),
                'internalConnAvgSecs' => array('text' => 'internal Conn Avg Secs'),
                'waitTotalNum' => array('text' => 'wait Total Num'),
                'waitTotalSecs' => array('text' => 'wait Total Secs'),
                'waitAvgSecs' => array('text' => 'wait Avg Secs'),
                'ringTotalNum' => array('text' => 'ring Total Num'),
                'ringTotalSecs' => array('text' => 'ring Total Secs'),
                'ringAvgSecs' => array('text' => 'ring Avg Secs'),
                'consultNum' => array('text' => 'consult Num'),
                'consultTotalSecs' => array('text' => 'consult Total Secs'),
                'consultAvgSecs' => array('text' => 'consult Avg Secs'),
                'holdNum' => array('text' => 'hold Num'),
                'hlodTotalSecs' => array('text' => 'hlod Total Secs'),
                'holdAvgSecs' => array('text' => 'hold Avg Secs'),
                'conferenceNum' => array('text' => 'conference Num'),
                'conferenceTotalSecs' => array('text' => 'conference Total Secs'),
                'conferenceAvgSecs' => array('text' => 'conference Avg Secs'),
                'transferTotalNum' => array('text' => 'transfer Total Num'),
                'transferedConnNum' => array('text' => 'transfered Conn Num'),
                'transferedConnTotalSecs' => array('text' => 'transfered Conn Total Secs'),
                'transferedConnAvgSecs' => array('text' => 'transfered Conn Avg Secs'),
                'loginTotalNum' => array('text' => 'login Total Num'),
                'loginTotalSecs' => array('text' => 'login Total Secs'),
                'readyTotalSecs' => array('text' => 'ready Total Secs'),
                'busyTotalNum' => array('text' => 'busy Total Num'),
                'busyTotalSecs' => array('text' => 'busy Total Secs'),
                'busyAvgSecs' => array('text' => 'busy Avg Secs'),
                'busyRate' => array('text' => 'busy Rate'),
                'workRate' => array('text' => 'work Rate'),
            ),
        ),
    );

    /**
     * 需要计算的指标项
     * text为指标的名称
     *
     * @var array
     */
    protected $calculateItems = array(
        'system'=>array(
            'inboundRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundTotalNum'),
                ),
                'denominator' => array(
                    'plus'=> array('ivrInboundTotalNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'inboundConnRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundTotalNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'inboundConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
            ),
            //呼入X秒接通率
            'inboundConnInXSecsRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnInXSecsNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'outboundConnRate'=>array(
                'numerator' => array(
                    'plus'=> array('outboundConnNum'),
                ),
                'denominator' => array(
                    'plus'=> array('outboundTotalNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'outboundConnAvgSecs'=>array(
                'numerator' => array(
                    'plus'=> array('outboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('outboundConnNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'inboundAbandonRate'=>array(
                'numerator' => array(
                    'plus'=> array('inboundAbandonTotalNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundTotalNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'queueAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('queueTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('queueTotalNum'),
                ),
            ),
            'connQueueAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('connQueueTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('connQueueTotalNum'),
                ),
            ),
            'ringAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('ringTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('ringTotalNum'),
                ),
            ),
            'busyAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('busyTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('busyTotalNum'),
                ),
            ),
            'waitAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('waitTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('waitTotalNum'),
                ),
            ),
            'workRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnTotalSecs', 'outboundConnTotalSecs', 'waitTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('loginTotalSecs'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
        ),
        'queue'=>array(
            'inboundConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
            ),
            'inboundConnRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundTotalNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            //呼入X秒接通率
            'inboundConnInXSecsRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnInXSecsNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'outboundConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('outboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('outboundConnNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'outboundConnRate' => array(
                'numerator' => array(
                    'plus'=> array('outboundConnNum'),
                ),
                'denominator' => array(
                    'plus'=> array('outboundTotalNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'inboundAbandonAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundAbandonTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundAbandonTotalNum'),
                ),
            ),
            'inboundCallerAbandonAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundCallerAbandonTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundCallerAbandonTotalNum'),
                ),
            ),
            'inboundQueueTimeoutAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundCallerAbandonTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundQueueTimeoutTotalNum'),
                ),
            ),
            'inboundQueueOverflowAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundQueueOverflowTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundQueueOverflowTotalNum'),
                ),
            ),
            'inboundNoAgentAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundNoAgentTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundNoAgentTotalNum'),
                ),
            ),
            //呼入X秒放弃率
            'inboundAbandonInXSecsRate' => array(
                'numerator' => array(
                    'plus'=> array('inboundAbandonInXSecsNum'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'queueAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('queueTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('queueTotalNum'),
                ),
            ),
            'connQueueAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('connQueueTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('connQueueTotalNum'),
                ),
            ),
            'ringAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('ringTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('ringTotalNum'),
                ),
            ),
            'waitAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('waitTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('waitTotalNum'),
                ),
            ),
        ),
        'agent'=>array(
            'inboundConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
            ),
            'outboundConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('outboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('outboundConnNum'),
                ),
            ),
            'connAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('connTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('connTotalNum'),
                ),
            ),
            'internalConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('internalConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('internalConnNum'),
                ),
            ),
            'waitAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('waitTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('waitTotalNum'),
                ),
            ),
            'ringAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('ringTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('ringTotalNum'),
                ),
            ),
            'consultAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('consultTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('consultNum'),
                ),
            ),
            'holdAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('hlodTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('holdNum'),
                ),
            ),
            'conferenceAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('conferenceTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('conferenceNum'),
                ),
            ),
            'transferedConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('transferedConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('transferedConnNum'),
                ),
            ),
            'busyAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('busyTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('busyTotalNum'),
                ),
            ),
            'busyRate' => array(
                'numerator' => array(
                    'plus'=> array('busyTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('loginTotalSecs'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'workRate' => array(
                'numerator' => array(
                    'plus'=> array('outboundConnTotalSecs', 'waitTotalSecs', 'inboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('loginTotalSecs'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
        ),
        'group'=>array(
            'inboundConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('inboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('inboundConnNum'),
                ),
            ),
            'outboundConnAvgSecs'=>array(
                'numerator' => array(
                    'plus'=> array('outboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('outboundConnNum'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'connAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('connTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('connTotalNum'),
                ),
            ),
            'internalConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('internalConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('internalConnNum'),
                ),
            ),

            'waitAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('waitTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('waitTotalNum'),
                ),
            ),
            'ringAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('ringTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('ringTotalNum'),
                ),
            ),
            'consultAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('consultTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('consultNum'),
                ),
            ),
            'holdAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('hlodTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('holdNum'),
                ),
            ),
            'conferenceAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('conferenceTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('conferenceNum'),
                ),
            ),
            'transferedConnAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('transferedConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('transferedConnNum'),
                ),
            ),
            'busyAvgSecs' => array(
                'numerator' => array(
                    'plus'=> array('busyTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('busyTotalNum'),
                ),
            ),
            'busyRate' => array(
                'numerator' => array(
                    'plus'=> array('busyTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('loginTotalSecs'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
            'workRate' => array(
                'numerator' => array(
                    'plus'=> array('outboundConnTotalSecs', 'waitTotalSecs', 'inboundConnTotalSecs'),
                ),
                'denominator' => array(
                    'plus'=> array('loginTotalSecs'),
                ),
                'percent' => 1,
                'editable' => true,
            ),
        ),
    );
}
