<?php
/**
 * Created by PhpStorm.
 * User: yangyang
 * Date: 2016/12/1
 * Time: 10:42
 */

namespace Icsoc\DataBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Icsoc\ExportLib\CsvExport;
use Icsoc\ExportLib\ExcelExport;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Icsoc\ExportLib\Col;

class BatchExportModel extends BaseModel
{
    private $_path = '';

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->conn = $this->container->get('doctrine.dbal.default_connection');
        $this->_path = $this->container->getParameter('export_dir');
        $this->logger = $this->container->get('logger');
    }

    /**
     * excel导出
     *
     * @param $titles
     * @param $data
     * @return BinaryFileResponse
     */
    public function exportExcel($titles, $data)
    {
        $export = new ExcelExport(array('tmp' => $this->_path));
        $titles = $this->translateTitle($titles);
        $export->setCols($titles);

        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $columns = array();
                foreach ($titles as $title => $name) {
                    $columns[$title] = isset($v[$title]) ? $v[$title] : '';
                }
                $export->writeRow($columns);
            }
        } else {
            $export->writeRow(array());
        }

        $fileName = $export->build();
        $tmpName = $this->_path.'/Data'.date('YmdHis').'.xlsx';
        copy($fileName, $tmpName);
        $response = new BinaryFileResponse($tmpName);
        $response->setContentDisposition('attachment', basename($tmpName));
        $response->deleteFileAfterSend(true);

        return $response;
    }

    /**
     * csv导出
     *
     * @param $titles
     * @param $data
     * @return BinaryFileResponse
     */
    public function exportCsv($titles, $data)
    {
        $export = new CsvExport(array('tmp' => $this->_path));
        $titles = $this->translateTitle($titles);
        $export->setCols($titles);

        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $columns = array();
                foreach ($titles as $title => $name) {
                    $columns[$title] = isset($v[$title]) ? $v[$title] : '';
                }
                $export->writeRow($columns);
            }
        } else {
            $export->writeRow(array());
        }

        $fileName = $export->build();
        $tmpName = $this->_path.'/Data'.date('YmdHis').'.zip';
        copy($fileName, $tmpName);
        $response = new BinaryFileResponse($tmpName);
        $response->setContentDisposition('attachment', basename($tmpName));
        $response->deleteFileAfterSend(true);

        return $response;
    }

    /**
     * 翻译字段
     *
     * @param $titles
     * @return array
     */
    public function translateTitle($titles)
    {
        $arr = array();
        $translator = $this->container->get('translator');
        foreach ($titles as $title => $name) {
            $arr[$title] = array(
                'title' => isset($name['title']) ? $translator->trans($name['title']) : '没有数据',
                'type' => isset($name['type']) ? $name['type'] : Col::TYPE_STRING,
            );
        }

        return $arr;
    }
}