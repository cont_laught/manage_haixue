<?php
/**
 * Created by PhpStorm.
 * User: ZSYK
 * Date: 2015/12/3
 * Time: 15:02
 */

namespace Icsoc\DataBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AgentStatModel
 * @package Icsoc\DataBundle\Model
 */
class AgentStatModel
{
    /** @var \Doctrine\DBAL\Connection  */
    private $conn;
    private $logger;
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->conn = $this->container->get('doctrine.dbal.default_connection');
        $this->logger = $this->container->get('logger');
    }

    /**
     * @param array $param
     * @return array
     */
    public function addAgentStat($param = array())
    {
        $vccId = isset($param['vcc_id']) ? $param['vcc_id'] : '';
        $agStat = isset($param['ag_stat']) ? $param['ag_stat'] : 1;
        $data = isset($param['data']) ? $param['data'] : '';

        if (empty($vccId)) {
            return array('code'=>401, 'message'=>'企业ID不能为空');
        }

        if (empty($data)) {
            return array('code'=>402, 'message'=>'数据不能为空');
        }

        if (!is_array($data)) {
            return array('code'=>403, 'message'=>'数据必须是一维数组');
        }

        $this->conn->beginTransaction();
        $error = 0;

        foreach ($data as $v) {
            $insert = array(
                'vcc_id' => $vccId,
                'ag_stat' => $agStat,
                'stat_reason' => $v,
            );

            try {
                $this->conn->insert('win_agstat_reason', $insert);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
                $error++;
            }
        }

        if (!empty($error)) {
            $this->conn->rollBack();

            return array('code'=>400, 'message'=>'数据库执行错误');
        }

        $this->conn->commit();

        return array('code'=>200, 'message'=>'success');
    }

    /**
     * @param array $param
     * @return array
     */
    public function deleteAgentStat($param = array())
    {
        $vccId = isset($param['vcc_id']) ? $param['vcc_id'] : '';
        $ids = isset($param['ids']) ? $param['ids'] : '';

        if (empty($vccId)) {
            return array('code'=>404, 'message'=>'企业ID不能为空');
        }

        if (empty($ids)) {
            return array('code'=>405, 'message'=>'ids不能为空');
        }

        if (!is_array($ids)) {
            return array('code'=>406, 'message'=>'ids必须是一维数组');
        }

        $this->conn->beginTransaction();
        $error = 0;
        foreach ($ids as $id) {
            $delete = array(
                'vcc_id' => $vccId,
                'id' => $id,
            );

            try {
                $this->conn->delete('win_agstat_reason', $delete);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
                $error++;
            }
        }

        if (!empty($error)) {
            $this->conn->rollBack();

            return array('code'=>400, 'message'=>'数据库执行错误');
        }

        $this->conn->commit();

        return array('code'=>200, 'message'=>'success');
    }

    /**
     * @param int $vccId
     * @return array
     */
    public function getAgentStat($vccId = 0)
    {
        if (empty($vccId)) {
            return array('code'=>407, 'message'=>'企业ID不能为空');
        }

        try {
            $result = $this->conn->fetchAll(
                "SELECT * FROM win_agstat_reason WHERE vcc_id = :vcc_id",
                array('vcc_id' => $vccId)
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return array('code'=>400, 'message'=>'数据库执行错误');
        }

        return array('code'=>200, 'message'=>'success', 'data'=>$result);
    }
}
