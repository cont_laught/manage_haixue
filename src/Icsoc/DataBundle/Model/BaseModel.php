<?php

namespace Icsoc\DataBundle\Model;

/**
 * Class BaseModel
 * @package Icsoc\DataBundle\Model
 */
class BaseModel
{
    /** @var \Symfony\Component\DependencyInjection\Container  */
    public $container;

    /**
     * @param $html
     * @return string
     */
    public function purifyHtml($html)
    {
        return $html;

        if (empty($html)) {
            return '';
        }
        $purifier = $this->container->get("icsoc_core.html.purifier.class")->get();
        return $purifier->purify($html);
    }

    public function purify($html)
    {
        return $html;
    }
}
