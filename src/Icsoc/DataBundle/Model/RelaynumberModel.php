<?php
namespace Icsoc\DataBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface;

class RelaynumberModel
{
    /** @var \Symfony\Component\DependencyInjection\Container  */
    private $container;

    /** @var \Doctrine\DBAL\Connection  */
    private $dbal;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->dbal = $this->container->get('doctrine.dbal.default_connection');
    }

    /**
     * 获取中继号码
     * @param array $param  vcc_code 企业代码， vcc_id 企业id;
     * @return array|string
     */
    public function getphone(array $param = array())
    {
        $vcc_code = empty($param['vcc_code']) ? 0 : $param['vcc_code'];
        $vcc_id = empty($param['vcc_id']) ? 0 : $param['vcc_id'];
        if (empty($vcc_id)) {
            $msg = $vcc_id = $this->container->get('icsoc_data.validator')->checkVccCode($vcc_code);
            if (!empty($msg) && is_array($msg)) {
                return $msg;
            }
        } else {
            if (!is_numeric($vcc_id)) {
                $ret = array(
                    'code' => 403,
                    'message' => '企业ID包含非数字字符',
                );
                return $ret;
            }
            //判断企业ID 是否存在；
            $vcc_code =$this->dbal->fetchColumn(
                "SELECT vcc_code FROM cc_ccods WHERE status=1 AND vcc_id = :vcc_id",
                array('vcc_id' => $vcc_id)
            );
            if (empty($vcc_code)) {
                $ret = array(
                    'code' => 404,
                    'message' => '企业ID不存在',
                );
                return $ret;
            }
        }
        $data = $this->dbal->fetchAll(
            "SELECT `phone_id`,`phone`,`phone400` FROM cc_phone400s WHERE vcc_id= :vcc_id",
            array('vcc_id' => $vcc_id)
        );
        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data
        );
        return $ret;
    }

    /**
     * 获取企业下的所有号码
     *
     * @param int $vcc_id
     *
     * @return array|bool
     */
    public function getAllPhoneArray($vcc_id)
    {
        if (empty($vcc_id)) {
            return false;
        }

        $data = $this->dbal->fetchAll(
            "SELECT `phone_id`,`phone`,`phone400` FROM cc_phone400s WHERE vcc_id= :vcc_id",
            array('vcc_id' => $vcc_id)
        );

        return $data;
    }
}
