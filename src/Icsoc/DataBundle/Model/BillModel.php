<?php
namespace Icsoc\DataBundle\Model;

use Symfony\Component\DependencyInjection\Container;

/**
 * 下面中用到info参数的json格式大概为 ；filter搜素(具体提供搜素字段各个接口不一样)，
 *  pagination分页(rows 每页显示条数,page第几页)，sort排序
 *  {"pagination":{"rows":20,"page":"5"},"filter":{"endresult":"0"},"sort":{"field":"id","order":"desc"}}
 * Class BillModel
 * @package Icsoc\DataBundle\Model
 *
 */
class BillModel
{

    /**
     * @var array
     * 咨询挂断类型
     */
    private $bvendResult = array(
        '0' => '挂断',
        '1' => '转接',
        '2' => '拦截',
    );

    /** @var \Symfony\Component\DependencyInjection\Container  */
    private $container;

    /** @var \Doctrine\DBAL\Connection  */
    private $dbal;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->dbal = $this->container->get('doctrine.dbal.default_connection');
    }

    /**
     * 咨询三方 通话详情
     * @param array $param 参数，格式为
     *                     array(
     *                         vcc_code,//企业代码
     *                         info,//提供搜素的字段有 ag_ernum 发起坐席工号 call_phone 发起号码 ext_phone 咨询三方号码
     *                              //时长 >= ：min_conn_secs 时长 <= ：max_conn_secs
     *                              //结束类型: endresult ( 0=>挂断，1=>转接)
     *                              //开始时间: start_time(YYYY-MM-DD HH:II:SS)
     *                              //结束时间: end_time(YYYY-MM-DD HH:II:SS) 队列ID que_id
     *                     )
     * @return array
     */
    public function conference(array $param = array())
    {
        $vcc_code = empty($param['vcc_code']) ? 0 : $param['vcc_code'];
        $info = empty($param['info']) ? 0 : $param['info'];

        /** @var array $msg  验证vcc_code是否正确 */
        $msg = $vcc_id = $this->container->get('icsoc_data.validator')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return $msg;
        }
        /** @var array $msg  验证info是否是json格式 */
        $msg = $addInfo = $this->container->get('icsoc_data.validator')->checkJson($info);
        if (!empty($msg) && isset($msg['code'])) {
            return $msg;
        }
        $where = 'vcc_id = :vcc_id AND ext_type = 1 ';
        $params =  array('vcc_id' => $vcc_id);
        if (isset($addInfo['filter'])) {
            //坐席工号，发起号码，咨询三方号码
            $where.=$this->container->get('icsoc_data.helper')
                ->likeWhere(array('ag_ernum','call_phone','ext_phone'), $addInfo['filter']);
            //时间，需要验证格式；
            $msg = $this->container->get('icsoc_data.helper')->dateTimeWhere($addInfo['filter']);
            if (!empty($msg) && is_array($msg)) {
                return $msg;
            } else {
                $where.=$msg;
            }
            //时长
            $arr = $this->container->get('icsoc_data.helper')->ltRtWhere(array('conn_secs'), $addInfo['filter']);
            //结束类型
            $enRow = $this->container->get('icsoc_data.helper')->equalWhere(array('endresult','que_id'), $addInfo['filter']);
            $params = array_merge($arr['params'], $enRow['params'], $params);
            $where.=$arr['where'].$enRow['where'];
        }
        $count = $this->dbal->fetchColumn(
            'SELECT count(*) '.
            'FROM win_extcdr '.
            'WHERE '.$where,
            $params
        );
        $page = $this->container->get("icsoc_data.helper")->getPageInfo($count, 'win_extcdr', $addInfo);
        //200 ok
        $list = $this->dbal->fetchAll(
            'SELECT ag_ernum,ag_ername,que_name,ag_edname,ag_ednum,call_phone,ext_phone,start_time,conn2_time,'.
            'conn1_secs,conn2_secs,conn_secs,endresult '.
            'FROM  win_extcdr '.
            'WHERE '.$where.
            'ORDER BY '.$page['sort'].' '.$page['order'].' LIMIT '.$page['start'].','.$page['limit'],
            $params
        );
        $res = array();
        if (!empty($list)) {
            foreach ($list as $v) {
                $v['start_time'] = $v['start_time'] ? date("Y-m-d H:i:s", $v['start_time']) : "";
                $v['conn2_time'] = $v['conn2_time'] ? date("Y-m-d H:i:s", $v['conn2_time']) : "";
                $v['endresult'] = isset($this->bvendResult[$v['endresult']]) ? $this->bvendResult[$v['endresult']] : '';
                $res[] = $v;
            }
        }
        $ret = array('code'=>200, 'message'=>'ok', 'total'=>$count, 'data'=>$res);
        return $ret;
    }
}
