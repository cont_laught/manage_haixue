<?php

namespace Icsoc\DataBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OtherModel
 * @package Icsoc\DataBundle\Model
 */
class OtherModel extends BaseModel
{
    /** @var \Symfony\Component\DependencyInjection\Container  */
    public $container;

    /** @var \Doctrine\DBAL\Connection  */
    private $dbal;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->dbal = $this->container->get('doctrine.dbal.default_connection');
    }

    /**
     * 获取企业绑定的中继号码接口
     * @param $vcc_code
     * @return array|string
     */
    public function getPhone($vcc_code)
    {
        $msg = $vid = $this->container->get('icsoc_data.validator')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return $msg;
        }
        $data = $this->dbal->fetchAll(
            "SELECT `phone_id`,`phone`,`phone400` FROM cc_phone400s WHERE vcc_id= :vcc_id",
            array('vcc_id' => $vid)
        );
        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data
        );
        return $ret;
    }
}
