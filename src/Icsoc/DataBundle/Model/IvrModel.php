<?php

namespace Icsoc\DataBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IvrModel
 * @package Icsoc\DataBundle\Model
 */
class IvrModel extends BaseModel
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    public $container;

    /** @var \Doctrine\DBAL\Connection */
    private $dbal;

    protected $name = array(
        "F" => "按键失败",
        "1" => "按1键",
        "2" => "按2键",
        "3" => "按3键",
        "4" => "按4键",
        "5" => "按5键",
        "6" => "按6键",
        "7" => "按7键",
        "8" => "按8键",
        "9" => "按9键",
        "0" => "按0键",
        "*" => "按*键",
        "multiKeys" => "多按键",
    );

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->dbal = $this->container->get('doctrine.dbal.default_connection');
    }

    /**
     * 通过企业ID后去该企业所有设置的IVR流程
     *
     * @param int $vccId
     *
     * @return array|bool
     */
    public function getIvrInfoByIdArray($vccId)
    {
        if (empty($vccId)) {
            return false;
        }
        $conn = $this->container->get("doctrine.dbal.default_connection");
        $query = $conn->fetchAll(
            "SELECT ivr_id,ivr_name FROM win_ivr WHERE vcc_id = ? AND ivr_info != ''",
            array($vccId)
        );
        $ivrInfo = array();
        if (!empty($query) && is_array($query)) {
            foreach ($query as $row) {
                $ivrInfo[$row['ivr_id']] = $row['ivr_name'];
            }
        }

        return $ivrInfo;
    }

    /**
     * 通过ID获取单个IVR的title
     *
     * @param int $vccId
     * @param int $ivrId
     *
     * @return array
     */
    public function getIvrtitlesById($vccId, $ivrId)
    {
        $titles = array();
        //检查企业ID及ivrId是否存在
        if (empty($vccId) || empty($ivrId)) {
            return $titles;
        }
        $conn = $this->container->get('doctrine.dbal.default_connection');
        $ivrInfo = $conn->fetchColumn(
            "SELECT ivr_info FROM win_ivr WHERE ivr_id = ? AND vcc_id = ? LIMIT 1",
            array($ivrId, $vccId),
            0
        );
        if (!empty($ivrInfo)) {
            $ivrInfo = json_decode($ivrInfo, true);
            if (!empty($ivrInfo) && is_array($ivrInfo)) {
                foreach ($ivrInfo as $key => $val) {
                    if (!empty($val['ivr_count']) && $val['ivr_count'] == true) {
                        if (!empty($val['ivr_count_name'])) {
                            $titles[$val['id']] = $val['ivr_count_name'];
                        } else {
                            if ($val['type'] = 130) {
                                $titles[$val['id']] = '判断失败'; //和按键F冲突一样；
                            } else {
                                $titles[$val['id']] = empty($this->name[$val['name']]) ? $val['name'] : $this->name[$val['name']];
                            }
                        }
                    }
                }
            }
        }

        return $titles;
    }

    /**
     * 获取IVR，keys信息
     *
     * @param int $vccId
     * @param int $ivrId
     *
     * @return array
     */
    public function getIvrInfoById($vccId, $ivrId)
    {
        $keys = array();
        if (empty($vccId) || empty($ivrId)) {
            return $keys;
        }
        $conn = $this->container->get('doctrine.dbal.default_connection');
        $ivrArray = $conn->fetchColumn(
            "SELECT ivr_info FROM win_ivr WHERE vcc_id = ? AND ivr_id = ? ",
            array($vccId, $ivrId),
            0
        );
        if (!empty($ivrArray)) {
            $ivrInfo = json_decode($ivrArray, true);
            $keys = array_keys($ivrInfo);
        }

        return $keys;
    }
}
