<?php
namespace Icsoc\DataBundle\Model;

use Symfony\Component\DependencyInjection\Container;

/**
 * 下面中用到info参数的json格式大概为 ；filter搜素(具体提供搜素字段各个接口不一样)，
 *  pagination分页(rows 每页显示条数,page第几页)，sort排序
 *  {"pagination":{"rows":20,"page":"5"},"filter":{"endresult":"0"},"sort":{"field":"id","order":"desc"}}
 * Class BillModel
 * @package Icsoc\DataBundle\Model
 *
 */
class CallstatusModel
{

    /**
     * 坐席电话呼叫状态
     */
    const FORBIDDEN_CALL_IN_AND_OUT = 0;
    const ALLOWED_CALL_IN = 1;
    const ALLOWED_CALL_OUT = 2;
    const ALLOWED_CALL_IN_AND_OUT = 3;

    /**
     * @var array
     */
    private $callStatus = array(
        self::FORBIDDEN_CALL_IN_AND_OUT,
        self::ALLOWED_CALL_IN,
        self::ALLOWED_CALL_OUT,
        self::ALLOWED_CALL_IN_AND_OUT
    );

    /**
     * 参数对应的要更新的数据库的值
     * @var array
     */
    private $callStatusValue = array(
        self::FORBIDDEN_CALL_IN_AND_OUT => 0x000f,
        self::ALLOWED_CALL_IN => 0x0007,
        self::ALLOWED_CALL_OUT => 0x000b,
        self::ALLOWED_CALL_IN_AND_OUT => 0x0003
    );

    /**
     * 结果
     * @var array
     *
     */
    private $RENDRESULT = array(
        '0' => "接通",
        '1' => "振铃放弃",
        '2' => "未接",
    );

    private $RINRESULT = array(
        '0' => '接通',
        '1' => 'IVR挂机',
        '2' => '留言',
        '3' => '未接通',
        '4' => '未接通留言',
    );
    /**
     * 转接分配结果
     * @var array
     */
    private $TRANSCALL = array(
        '0' => '成功',
        '1' => '主叫放弃',
        '2' => '坐席未接',
        '3' => '排队超时',
        '4' => '排队溢出',
    );

    /**
     * @var array
     */
    private $QENDRESULT = array(
        '1' => '主叫放弃',
        '2' => '坐席未接',
        '3' => '排队超时',
        '4' => '队列满溢出',
        '11' => '用户挂机',
        '12' => '坐席挂断',
        '21' => '未转技能组',
    );

    private $CDRSTATUS = array(
        '0' => '待分配',
        '1' => '已分配',
        '2' => '处理中',
        '3' => '已完成',
    );

    /** @var \Symfony\Component\DependencyInjection\Container  */
    private $container;

    /** @var \Doctrine\DBAL\Connection  */
    private $dbal;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->dbal = $this->container->get('doctrine.dbal.default_connection');
    }

    /**
     * 咨询三方 通话详情
     * @param array $param 参数，格式为
     *                     array(
     *                         vcc_code,//企业代码
     *                         info,//提供搜素的字段有 ag_ernum 发起坐席工号 call_phone 发起号码 ext_phone 咨询三方号码
     *                              //时长 >= ：min_conn_secs 时长 <= ：max_conn_secs
     *                              //结束类型: endresult ( 0=>挂断，1=>转接)
     *                              //开始时间: start_time(YYYY-MM-DD HH:II:SS)
     *                              //结束时间: end_time(YYYY-MM-DD HH:II:SS) 队列ID que_id
     *                     )
     * @return array
     */
    public function setCallstatus(array $param = array())
    {

    }
}
