<?php

namespace Icsoc\DataBundle\Model;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 开发者模式
 *
 * @package Icsoc\DataBundle\Model
 */
class DevelopModel extends BaseModel
{
    /** @var \Doctrine\DBAL\Connection */
    private $conn;
    private $logger;

    //通话类型
    private $callType = array(
        '1' => '呼出',
        '2' => '呼入',
        '3' => '呼出接起',
        '4' => '呼入接起',
    );

    //推送结果
    private $sendRes = array(
        '3' => '推送中',
        '1' => '成功',
        '2' => '失败',
        '4' => '重推中',
    );

    //推送类型
    private $sendType = array(
        '0' => '',
        '1' => '挂机推送',
        '2' => '挂机推送',
        '3' => '接通推送',
        '4' => '接通推送',
    );
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->conn = $this->container->get('doctrine.dbal.default_connection');
        $this->logger = $this->container->get('logger');
    }
    /**
     * 通话结果推送
     * @param array $param
     * @return array
     */
    public function getRecord($param = array())
    {
        $vccId = isset($param['vcc_id']) ? $param['vcc_id'] : '';
        $page = isset($param['page']) ? $param['page'] : 1;
        $rows = isset($param['rows']) ? $param['rows'] : 10;
        $order = isset($param['order']) ? $param['order'] : 'desc';
        $field = isset($param['field']) ? $param['field'] : 'send_start_time';
        $phone = isset($param['phone']) ? $param['phone'] : '';
        $sendRes = isset($param['sendRes']) ? $param['sendRes'] : '';
        $sendType = isset($param['sendType']) ? $param['sendType'] : '';

        if (empty($vccId)) {
            return array('code'=>402, 'message'=>'企业ID不能为空');
        }

        $where = " vcc_id=$vccId ";
        if (!empty($phone)) {
            $where .= " AND cus_phone like '%$phone%' ";
        }
        if (!empty($sendRes)) {
            $where .= " AND send_res=$sendRes ";
        }
        if (!empty($sendType)) {
            if ($sendType == 1) {
                $where .= " AND call_type in(1,2) ";
            } else if($sendType == 2) {
                $where .= " AND call_type=3 ";
            }

        }

        $start = $rows * $page - $rows;
        $start = $start > 0 ? $start : 0;
        try {
            $count = $this->conn->fetchColumn("SELECT COUNT(*) FROM win_push_record WHERE $where");
            $data = $this->conn->fetchAll(
                "SELECT id,send_start_time,send_end_time,call_type,send_res,cus_phone,call_start_time,call_end_time,send_params
                FROM win_push_record
                WHERE {$where}
                ORDER BY {$field} {$order}
                LIMIT {$start},{$rows}"
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return array('code'=>400, 'message'=>'数据库执行错误');
        }
        foreach ($data as $key => $val) {
            $data[$key]['call_type'] = isset($this->callType[$val['call_type']]) ? $this->callType[$val['call_type']] : '';
            $data[$key]['send_res'] = isset($this->sendRes[$val['send_res']]) ? $this->sendRes[$val['send_res']] : '';
            $sendParams = json_decode($data[$key]['send_params']);
            $data[$key]['send_params'] = json_encode($sendParams, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
            $data[$key]['send_start_time'] = !empty($data[$key]['send_start_time']) ?
                date("Y-m-d H:i:s", $data[$key]['send_start_time']) : '';
            $data[$key]['send_end_time'] = !empty($data[$key]['send_end_time']) ?
                date("Y-m-d H:i:s", $data[$key]['send_end_time']) : '';
            $data[$key]['call_start_time'] = !empty($data[$key]['call_start_time']) ?
                date("Y-m-d H:i:s", $data[$key]['call_start_time']) : '';
            $data[$key]['call_end_time'] = !empty($data[$key]['call_end_time']) ?
                date("Y-m-d H:i:s", $data[$key]['call_end_time']) : '';
            $data[$key]['send_type'] = empty($val['call_type']) ? '' : $this->sendType[$val['call_type']];
        }

        return array(
            'code'=>200,
            'message'=>'success',
            'records' => $count,
            'page' => $page,
            'total' => ceil($count/$rows),
            'rows'=>$data,
        );
    }

    /**
     * 下载任务数据
     * @param array $param
     * @return array
     */
    public function getDownload($param = array())
    {
        $vccId = isset($param['vcc_id']) ? $param['vcc_id'] : '';
        $page = isset($param['page']) ? $param['page'] : 1;
        $rows = isset($param['rows']) ? $param['rows'] : 10;
        $order = isset($param['order']) ? $param['order'] : 'desc';
        $field = isset($param['field']) ? $param['field'] : 'submit_time';
        $startTime = isset($param['startTime']) ? strtotime($param['startTime']) : '';
        $endTime = isset($param['endTime']) ? strtotime($param['endTime']) : '';
        if (empty($vccId)) {
            return array('code'=>402, 'message'=>'企业ID不能为空');
        }
        $user = $this->container->get("security.token_storage")->getToken()->getUser();
        $submitUser = $user->getId().'-'.$user->getLoginType();
        $where = " submit_user='".$submitUser."'";
        if (!empty($startTime)) {
            $where .= " AND submit_time > $startTime ";
        }
        if (!empty($endTime)) {
            $where .= " AND submit_time < $endTime ";
        }

        $start = $rows * $page - $rows;
        $start = $start > 0 ? $start : 0;
        try {
            $count = $this->conn->fetchColumn("SELECT COUNT(*) FROM win_download_log WHERE $where");
            $data = $this->conn->fetchAll(
                "SELECT id,token_id,name,progress,download_url,datacount,file_size,use_time,submit_time,submit_user,export_version
                FROM win_download_log
                WHERE {$where}
                ORDER BY {$field} {$order}
                LIMIT {$start},{$rows}"
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return array('code'=>400, 'message'=>'数据库执行错误');
        }
        //文件大小判断
        foreach ($data as $key => $val) {
            if ($val['progress'] == '-100') {
                $data[$key]['file_size'] = 0;
            } elseif ($val['progress'] != '-100' && $val['progress'] != '100') {
                $data[$key]['file_size'] = '正在生成文件';
            }
        }

        return array(
            'code'=>200,
            'message'=>'success',
            'records' => $count,
            'page' => $page,
            'total' => ceil($count/$rows),
            'rows'=>$data,
        );
    }

    /**
     * 获取更新后进度
     * @param string $url
     * @param string $params
     * @return mixed
     */
    public function getProgress($url, $params)
    {
        $url .=  '?'.'token_id='.$params;
        $progress = file_get_contents($url);
        /*print_r($progress);
        exit;*/
        $progress = json_decode($progress);
        $data = array();
        $allUrl = array();  //文件成生后会生成下载路径
        $useTime = array(); //开始生成时间
        $fileSize = array(); //文件大小
        $datacount = array(); //数据量
        $msgTokenId = array();
        foreach ($progress->result as $key => $val) {
            if (empty($val)) {
                continue;
            }
            //任务失败时数据库进度为-100
            if ($val->status == -100) {
                $msgTokenId[] = $key;
                $this->conn->update('win_download_log', array('progress' => -100), array('token_id' => $key));
                continue;
            }
            $data[$key] = floor($val->progress*100);
            $allUrl[$key] = $val->url;
            $useTime[$key] = time() - $val->createdtime;
            if ($val->status == 100) {
                $useTime[$key] = $val->statustime - $val->createdtime;
            }
            $fileSize[$key] =sprintf("%.2f", $val->size / 1024);
            $datacount[$key] = $val->datacount;
            //更新数据库
            $uploadData = array(
                'progress' => $data[$key],
                'download_url' => $allUrl[$key],
                'datacount' => $datacount[$key],
                'file_size' => $fileSize[$key],
                'use_time' => $useTime[$key],
            );
            $this->conn->update('win_download_log', $uploadData, array('token_id' => $key));
        }

        return array(
            'data'=>$data,
            'allUrl' => $allUrl,
            'use_time' => $useTime,
            'file_size' => $fileSize,
            'datacount' => $datacount,
            'msg_token_id' => $msgTokenId,
        );
    }

    /**
     * 新的异步导出
     * 获取更新后进度
     * @param string $url
     * @param string $params
     * @return mixed
     */
    public function getNewProgress($url, $params)
    {
        if (stripos($url, '?')) {
            $url .= '&token_id='.$params;
        } else {
            $url .=  '?'.'token_id='.$params;
        }
        $progress = file_get_contents($url);
        $progress = json_decode($progress);
        $data = array();
        $allUrl = array();  //文件成生后会生成下载路径
        $useTime = array(); //开始生成时间
        $fileSize = array(); //文件大小
        $datacount = array(); //数据量
        $msgTokenId = array();
        foreach ($progress->result as $key => $val) {
            if (empty($val)) {
                continue;
            }
            //任务失败时数据库进度为-100
            if ($val->status == -100) {
                $msgTokenId[] = $key;
                $this->conn->update('win_download_log', array('progress' => -100), array('token_id' => $key));
                continue;
            }
            $data[$key] = floor($val->progress*100);
            $allUrl[$key] = $val->fileurl;
            $useTime[$key] = time() - $val->createdtime;
            if ($val->status == 100) {
                $useTime[$key] = $val->statustime - $val->createdtime;
            }
            $fileSize[$key] =sprintf("%.2f", $val->filesize / 1024);
            $datacount[$key] = $val->datacount;
            //更新数据库
            $uploadData = array(
                'progress' => $data[$key],
                'download_url' => $allUrl[$key],
                'datacount' => $datacount[$key],
                'file_size' => $fileSize[$key],
                'use_time' => $useTime[$key],
            );
            $this->conn->update('win_download_log', $uploadData, array('token_id' => $key));
        }

        return array(
            'data'=>$data,
            'allUrl' => $allUrl,
            'use_time' => $useTime,
            'file_size' => $fileSize,
            'datacount' => $datacount,
            'msg_token_id' => $msgTokenId,
        );
    }

    /**
     * 号码组报表
     *
     * @param array $param
     *
     * @return array
     */
    public function getPhoneGroupData(array $param = array())
    {
        $vccCode = empty($param['vcc_code']) ? 0 : $param['vcc_code'];
        $infos = empty($param['info']) ? '' : $this->purifyHtml($param['info']);
        /** @var array $msg 验证vcc_code是否正确 */
        $msg = $vccId = $this->container->get('icsoc_data.validator')->checkVccCode($vccCode);
        $reg = "/^[1-9]\d{3}-(0[1-9]|1[0-2])$/"; //2015-05 格式判断

        if (!empty($msg) && is_array($msg)) {
            return $msg;
        }
        $where = ' vcc_id = :vcc_id ';
        $condition['vcc_id'] = $vccId;
        $groups = '';   //存放号码组信息;
        //分页搜索相关信息；
        $info = array();
        if (!empty($infos)) {
            $info = json_decode($infos, true);
            if (json_last_error()) {
                return array('code' => 403, 'message' => 'info格式非json');
            }
        }

        $table = empty($info['filter']['report_type']) ? 'rep_phone_group_day' : 'rep_phone_group_'.$info['filter']['report_type'];
        $order = $info['sort']['field'];

        $time = $this->container->get("icsoc_core.common.class")->rolesCanSearchAllReportDatas($info['filter']['start_date'], $info['filter']['end_date'].' 23:59:59', 'date');
        if ($time == 'no') {
            $condition['vcc_id'] = -1;
        }

        if (isset($info['filter'])) {
            //开始时间
            if (isset($info['filter']['start_date']) && !empty($info['filter']['start_date'])) {
                $startDate = $info['filter']['start_date'];
                if (isset($time['startTime'])) {
                    $startDate = $time['startTime'];
                    if ($info['filter']['report_type'] == 'month') {
                        $startDate = substr($startDate, 0, 7);
                    }
                }
                /*$msg = $this->container->get('icsoc_data.helper')->isDate('开始日期不正确', $startDate, 404);
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }*/
                switch ($info['filter']['report_type']) {
                    case 'day':
                        $where .= "AND nowdate >= :start_date ";
                        break;
                    case 'month':
                        $msg = $this->container->get('icsoc_data.helper')->regexRormat($reg, $startDate, '开始日期不正确', 404);
                        $where .= "AND nowmonth >= :start_date ";
                        break;
                }
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }
                $condition['start_date'] = $startDate;
            }
            //结束时间
            if (isset($info['filter']['end_date']) && !empty($info['filter']['end_date'])) {
                $endDate = $info['filter']['end_date'];
                if (isset($time['endTime'])) {
                    $endDate = $time['endTime'];
                    if ($info['filter']['report_type'] == 'month') {
                        $endDate = substr($endDate, 0, 7);
                    }
                }
                /*$msg = $this->container->get('icsoc_data.helper')->isDate('结束日期不正确', $endDate, 405);
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }*/
                switch ($info['filter']['report_type']) {
                    case 'day':
                        $where .= "AND nowdate <= :end_date ";
                        break;
                    case 'month':
                        $msg = $this->container->get('icsoc_data.helper')->regexRormat($reg, $endDate, '结束日期不正确', 405);
                        $where .= "AND nowmonth <= :end_date ";
                        break;
                }
                if (!empty($msg) && is_array($msg)) {
                    return $msg;
                }
                $condition['end_date'] = $endDate;
            }
            /************  号码组ID  *************/
            $groupsWhere = '';
            if (!empty($info['filter']['pg_id'])) {
                $pgId = $info['filter']['pg_id'];
                $groupsWhere = ' AND b.group_id in ('.$pgId.')';
            }
            $groups = $this->conn->fetchAll(
                'SELECT c.group_name,a.phone '.
                ' FROM cc_phone400s as a'.
                ' LEFT JOIN'.
                ' (win_phoid_gid as b LEFT JOIN win_phone_group as c on b.group_id = c.id)on '.
                ' b.phone_id = a.phone_id '.
                ' WHERE b.vcc_id = '.$vccId.$groupsWhere
            );
            if (!empty($info['filter']['pg_id'])) {
                $serNum = '';
                foreach ($groups as $key => $val) {
                    $serNum .= "'".$val['phone']."',";
                }
                $where .= " AND server_num in( ".rtrim($serNum, ',')." ) ";
            }
        }

        /** @var  $authority (获取数据权限)
        $authority = $this->container->get("icsoc_core.common.class")->getAuthorityCondition('ag_id', 'que_id', 2);
        if ($authority === false) {
            return array(
                'code' => 406,
                'message' => '权限内无任何数据',
            );
        } else {
            $where .= $authority;
        }*/

        /** 如果是导出不分页获取数据 */
        if (empty($info['export'])) {
            $rows = $this->conn->fetchAll(
                "SELECT server_num,total_num,ivr_num,in_num,in_conn_num,lost_num,ring_num,conn5_num,conn10_num,conn15_num,conn20_num,conn25_num,conn30_num,conn40_num,".
                "lost10_num,lost20_num,lost25_num,lost30_num,lost40_num,".$order.
                " FROM ".$table.
                " WHERE".$where.
                ' ORDER BY '.$order.' '.$info['sort']['order'],
                $condition
            );
        } else {
            $rows = $this->conn->fetchAll(
                "SELECT server_num,total_num,ivr_num,in_num,in_conn_num,lost_num,ring_num,conn5_num,conn10_num,conn15_num,conn20_num,conn25_num,conn30_num,conn40_num,".
                "lost10_num,lost20_num,lost25_num,lost30_num,lost40_num,".$order.
                " FROM ".$table.
                " WHERE".$where.
                ' ORDER BY '.$order.' '.$info['sort']['order'],
                $condition
            );
        }

        $arr = array();
        foreach ($groups as $val) {
            $arr[$val['group_name']][] = $val['phone'];
        }
        $data = array();
        $a = 0;
        foreach ($arr as $key => $val) {
            foreach ($rows as $k => $v) {
                if (in_array($v['server_num'], $val)) {
                    if (!empty($data[$a]['ivr_num']) && $data[$a][$order] != $v[$order]) {
                        $a += 1;
                    }
                    $data[$a]['server_num'] = $key;
                    $data[$a][$order] = $v[$order];
                    $data[$a]['total_num'] = empty($data[$a]['total_num']) ? $v['total_num'] : $data[$a]['total_num'] + $v['total_num'];
                    $data[$a]['ivr_num'] = empty($data[$a]['ivr_num']) ? $v['ivr_num'] : $data[$a]['ivr_num'] + $v['ivr_num'];
                    $data[$a]['in_num'] = empty($data[$a]['in_num']) ? $v['in_num'] : $data[$a]['in_num'] + $v['in_num'];
                    $data[$a]['in_conn_num'] = empty($data[$a]['in_conn_num']) ? $v['in_conn_num'] : $data[$a]['in_conn_num'] + $v['in_conn_num'];
                    $data[$a]['lost_num'] = empty($data[$a]['lost_num']) ? $v['lost_num'] : $data[$a]['lost_num'] + $v['lost_num'];
                    $data[$a]['ring_num'] = empty($data[$a]['ring_num']) ? $v['ring_num'] : $data[$a]['ring_num'] + $v['ring_num'];
                    $data[$a]['conn5_num'] = empty($data[$a]['conn5_num']) ? $v['conn5_num'] : $data[$a]['conn5_num'] + $v['conn5_num'];
                    $data[$a]['conn10_num'] = empty($data[$a]['conn10_num']) ? $v['conn10_num'] : $data[$a]['conn10_num'] + $v['conn10_num'];
                    $data[$a]['lost10_num'] = empty($data[$a]['lost10_num']) ? $v['lost10_num'] : $data[$a]['lost10_num'] + $v['lost10_num'];
                    $data[$a]['conn15_num'] = empty($data[$a]['conn15_num']) ? $v['conn15_num'] : $data[$a]['conn15_num'] + $v['conn15_num'];
                    $data[$a]['conn20_num'] = empty($data[$a]['conn20_num']) ? $v['conn20_num'] : $data[$a]['conn20_num'] + $v['conn20_num'];
                    $data[$a]['lost20_num'] = empty($data[$a]['lost20_num']) ? $v['lost20_num'] : $data[$a]['lost20_num'] + $v['lost20_num'];
                    $data[$a]['conn25_num'] = empty($data[$a]['conn25_num']) ? $v['conn25_num'] : $data[$a]['conn25_num'] + $v['conn25_num'];
                    $data[$a]['lost25_num'] = empty($data[$a]['lost25_num']) ? $v['lost25_num'] : $data[$a]['lost25_num'] + $v['lost25_num'];
                    $data[$a]['conn30_num'] = empty($data[$a]['conn30_num']) ? $v['conn30_num'] : $data[$a]['conn30_num'] + $v['conn30_num'];
                    $data[$a]['lost30_num'] = empty($data[$a]['lost30_num']) ? $v['lost30_num'] : $data[$a]['lost30_num'] + $v['lost30_num'];
                    $data[$a]['conn40_num'] = empty($data[$a]['conn40_num']) ? $v['conn40_num'] : $data[$a]['conn40_num'] + $v['conn40_num'];
                    $data[$a]['lost40_num'] = empty($data[$a]['lost40_num']) ? $v['lost40_num'] : $data[$a]['lost40_num'] + $v['lost40_num'];
                    unset($rows[$k]);
                }
            }
            if (!empty($data[$a])) {
                $a += 1;
            }
        }
        $data = array_merge($data, $rows);

        return array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data,
        );
    }

    /**
     * 获取sip分机信息
     * @param array $param
     * @return array
     */
    public function getSipPhone(array $param = array())
    {
        $vccCode = empty($param['vcc_code']) ? 0 : $param['vcc_code'];
        $info = empty($param['info']) ? '' : $param['info'];
        $msg = $vid = $this->container->get('icsoc_data.validator')->checkVccCode($vccCode);
        if (!empty($msg) && is_array($msg)) {
            return $msg;
        }

        //分页搜索相关信息；
        $addInfo = array();
        if (!empty($info)) {
            $addInfo = json_decode($info, true);
            if (json_last_error()) {
                return array('code' => 403, 'message' => 'info格式非json');
            }
        }
        $where = 'and pho_type = 5 ';
        if (isset($addInfo['filter'])) {
            $where .= isset($addInfo['filter']['phone']) && !empty($addInfo['filter']['phone']) ?
                " AND pho_num LIKE '%".$addInfo['filter']['phone']."%' " : '' ;
        }

        $dbal = $this->container->get('doctrine.dbal.default_connection');
        $count = $dbal->fetchColumn(
            'SELECT count(*) '.
            'FROM win_phone '.
            'WHERE vcc_id = :vid '.$where,
            array('vid'=>$vid)
        );
        $page = $this->container->get("icsoc_data.helper")->getPageInfo($count, 'win_phone', $addInfo);
        $list = $dbal->fetchAll(
            'SELECT id,pho_num,pho_type '.
            'FROM win_phone '.
            'WHERE vcc_id = :vid '.$where.
            'ORDER BY '.$page['sort'].' '.$page['order'].' LIMIT '.$page['start'].','.$page['limit'],
            array('vid'=>$vid)
        );
        $data = array();
        $redis = $this->container->get('snc_redis.sip');
        foreach ($list as $v) {
            if (empty($v['pho_num'])) {
                continue;
            }
            $redisName =  'ss'.$vid.'ss'.$v['pho_num'];//sip远程分机
            $redisData = $redis->hgetall($redisName);
            if (empty($redisData)) {
                $v['redis']['pho_num'] = $v['pho_num'];
                $v['redis']['cur_now'] = '离线';
                $v['redis']['ag_name'] = '';
                $v['redis']['ag_num'] =  '';
                $v['redis']['cur_time'] = '';
                $v['redis']['realm'] = '';
                $v['redis']['ping_ms'] = '';
                $v['redis']['src_host'] = '';
                $data[] = $v['redis'];
                continue;
            }
            $v['redis']['pho_num'] = $v['pho_num'];
            $redisData['cur_time'] = empty($redisData['cur_time']) ? 0 : $redisData['cur_time'];
            $redisData['realm'] = empty($redisData['realm']) ? '' : $redisData['realm'];
            $redisData['ping_ms'] = empty($redisData['ping_ms']) ? '' : $redisData['ping_ms'];
            $redisData['src_host'] = empty($redisData['src_host']) ? '' : $redisData['src_host'];
            $redisData['ping_at'] = empty($redisData['ping_at']) ? 0 : $redisData['ping_at'];
            $redisData['exp'] = empty($redisData['exp']) ? 0 : $redisData['exp'];
            if (($redisData['cur_time'] + $redisData['exp'] + 60) > time()) {
                $v['redis']['cur_now'] = '在线';
                $agentData = $dbal->fetchAssoc("SELECT ag_num,ag_name FROM win_agent WHERE vcc_id=$vid AND ag_sta != 0 AND pho_num = ".$v['pho_num']);
                if (!empty($agentData)) {
                    $v['redis']['ag_num'] = $agentData['ag_num'];
                    $v['redis']['ag_name'] = $agentData['ag_name'];
                } else {
                    $v['redis']['ag_num'] = '';
                    $v['redis']['ag_name'] = '';
                }
                $v['redis']['ping_ms'] = ($redisData['ping_at'] + 180) > time() ? $redisData['ping_ms'] : '- -';
            } else {
                $v['redis']['cur_now'] = '离线';
                $v['redis']['ag_num'] = '';
                $v['redis']['ag_name'] = '';
                $v['redis']['ping_ms'] = '- -';
            }
            $v['redis']['cur_time'] = $redisData['cur_time'];
            $v['redis']['realm'] = $redisData['realm'];
            $v['redis']['src_host'] = $redisData['src_host'];
            $data[] = $v['redis'];
        }

        $ret = array(
            'code' => 200,
            'message' =>'ok',
            'total' => $count,
            'page' => $page['page'],
            'totalPage' => $page['totalPage'],
            'data' => $data,
        );

        return $ret;
    }

    /**
     * 获取挂机短信发送到客户信息
     * @param int $vccId
     * @return array
     */
    public function getSwjHangupSms($vccId)
    {
        $swjHangupSms = $this->conn->fetchAll(
            "SELECT is_connect,connect_sms,is_busy,busy_sms,vcc_signature FROM swj_hangup_sms WHERE vcc_id=:vcc_id",
            array(
                'vcc_id' => $vccId,
            )
        );
        if (!empty($swjHangupSms)) {
            $swjHangupSms = $swjHangupSms[0];
        }

        return $swjHangupSms;
    }

    /**
     * 获取挂机短信发送到坐席信息
     * @param integer $vccId
     * @return array
     */
    public function getEndsms($vccId)
    {
        $ccCcods = $this->conn->fetchAll(
            "SELECT endsms_type,endsms_phone FROM cc_ccods WHERE vcc_id=:vcc_id",
            array(
                'vcc_id' => $vccId,
            )
        );
        if (!empty($ccCcods)) {
            $ccCcods = $ccCcods[0];
        }

        return $ccCcods;
    }

    /**
     * 修改短信发送到用户配置信息
     * @param int $vccId
     * @param array $info
     * @return array
     */
    public function updateSwjHangupSms($vccId, $info)
    {
        try{
            $this->conn->update(
                'swj_hangup_sms',
                $info,
                array('vcc_id' => $vccId)
            );

            return array('code' => 200);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return array('code' => 500, 'message' => '数据更新失败');
        }
    }

    /**
     * 修改短信发送到坐席配置信息
     * @param $vccId
     * @param $info
     */
    public function updateEndSms($vccId, $info)
    {
        try{
            $this->conn->update(
                'cc_ccods',
                $info,
                array('vcc_id' => $vccId)
            );

            return array('code' => 200);
        }catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return array('code' => 500, 'message' => '数据更新失败');
        }
    }
}
