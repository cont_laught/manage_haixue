<?php

namespace Icsoc\DataBundle\Tests\Model;

use Icsoc\DataBundle\Model\BillModel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AgentControllerTest
 * @package Icsoc\DataBundle\Tests\Controller
 */
class BillModelTest extends WebTestCase
{
    /** @var \Symfony\Component\DependencyInjection\Container $container */
    private $container;

    /** @var  \Icsoc\DataBundle\Model\BillModel $model */
    private $model;

    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
        $this->model = new BillModel($this->container);
    }

    public function testBillConference()
    {
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $response = $this->model->conference($param);
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertEquals($response, $result);

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $response = $this->model->conference($param);
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertEquals($response, $result);

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $response = $this->model->conference($param);
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertEquals($response, $result);

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $response = $this->model->conference($param);
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        // echo $response;
        $this->assertEquals($response, $result);

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $response = $this->model->conference($param);
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertEquals($response, $result);

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"max_conn_secs":"2","endresult":"0","que_id":"123"},"sort":{"field":"id","order":"desc"}}',
        );
        $response = $this->model->conference($param);
        //echo $response;
        $this->assertEquals("200", isset($response['code']) ? $response['code'] : '');
    }
}
