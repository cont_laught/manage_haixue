<?php

namespace Icsoc\DataBundle\Tests\Model;

use Icsoc\DataBundle\Model\AgentModel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AgentControllerTest
 * @package Icsoc\DataBundle\Tests\Controller
 */
class AgentModelTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
    }

    /**
     * 测试签入
     */
    public function testStaticAgentLogin()
    {
        $agentModel = new AgentModel($this->container);
        //param为空
        $param = array();
        $response = $agentModel->staticAgentLogin($param);
        $result = array(
            'code' => 415,
            'message' => '参数param不能为空',
        );
        $this->assertEquals($response, $result);

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'data' => array(
                array(
                    'ag_num' => '8001',
                    'phone' => '13811112222',
                ),
                array(
                    'ag_num' => '8002',
                    'phone' => '13811112223',
                ),
            )
        );
        $response = $agentModel->staticAgentLogin($param);
        $result = array(
            'code' => 401,
            'message' => '企业ID和企业代码都为空',
        );
        $this->assertEquals($response, $result);
        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1x',
            'data' => array(
                array(
                    'ag_num' => '8001',
                    'phone' => '13811112222',
                ),
                array(
                    'ag_num' => '8002',
                    'phone' => '13811112223',
                ),
            )
        );
        $response = $agentModel->staticAgentLogin($param);
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //参数中详细数据data为空
        $param = array(
            'vcc_id' => 1,
            'data' => array()
        );
        $response = $agentModel->staticAgentLogin($param);
        $result = array(
            'code' => 404,
            'message' => '参数中详细数据data为空',
        );
        $this->assertEquals($response, $result);

        //参数中详细数据data格式不对，要求为数组
        $param = array(
            'vcc_id' => 1,
            'data' => 'xx'
        );
        $response = $agentModel->staticAgentLogin($param);
        $result = array(
            'code' => 405,
            'message' => '参数中详细数据data格式不对，要求为数组',
        );
        $this->assertEquals($response, $result);
    }

    /**
     * 测试签出
     */
    public function testStaticAgentLogout()
    {
        $agentModel = new AgentModel($this->container);
        //param为空
        $param = array();
        $response = $agentModel->staticAgentLogout($param);
        $result = array(
            'code' => 415,
            'message' => '参数param不能为空',
        );
        $this->assertEquals($response, $result);

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'data' => array(
                array(
                    'ag_num' => '8001',
                ),
                array(
                    'ag_num' => '8002',
                ),
            )
        );
        $response = $agentModel->staticAgentLogout($param);
        $result = array(
            'code' => 401,
            'message' => '企业ID和企业代码都为空',
        );
        $this->assertEquals($response, $result);

        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1x',
            'data' => array(
                array(
                    'ag_num' => '8001',
                ),
                array(
                    'ag_num' => '8002',
                ),
            )
        );
        $response = $agentModel->staticAgentLogout($param);
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //参数中详细数据data为空
        $param = array(
            'vcc_id' => 1,
            'data' => array()
        );
        $response = $agentModel->staticAgentLogout($param);
        $result = array(
            'code' => 404,
            'message' => '参数中详细数据data为空',
        );
        $this->assertEquals($response, $result);

        //参数中详细数据data格式不对，要求为数组
        $param = array(
            'vcc_id' => 1,
            'data' => 'xxx'
        );
        $response = $agentModel->staticAgentLogout($param);
        $result = array(
            'code' => 405,
            'message' => '参数中详细数据data格式不对，要求为数组',
        );
        $this->assertEquals($response, $result);
    }

    /**
     * 测试删除坐席
     */
    public function testAgentDelete()
    {
        $agentModel = new AgentModel($this->container);
        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'ag_id' => array('1'),
        );
        $response = $agentModel->deleteAgent($param);
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertEquals($response, $result);
        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1xc',
            'ag_id' => array('1'),
        );
        $response = $agentModel->deleteAgent($param);
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符'
        );
        $this->assertEquals($response, $result);

        //坐席ID为空
        $param = array(
            'vcc_id' => '1',
            'ag_id' => array(),
        );
        $response = $agentModel->deleteAgent($param);
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空'
        );
        $this->assertEquals($response, $result);

        //坐席ID包含非数字字符
        $param = array(
            'vcc_id' => '1',
            'ag_id' => array('1xx'),
        );
        $response = $agentModel->deleteAgent($param);
        $result = array(
            'code' => 404,
            'message' => '坐席ID包含非数字字符',
            'ag_id' => '1xx'
        );
        $this->assertContains($result, $response['data']);
    }

    /**
     * 测试编辑坐席
     */
    public function testEditAgent()
    {
        $agentModel = new AgentModel($this->container);

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'ag_id' => '1',
            'ag_name' => 'cc',
        );
        $response = $agentModel->editAgent($param);
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertEquals($response, $result);

        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1xc',
            'ag_id' => '1',
            'ag_name' => 'cc',
        );
        $response = $agentModel->editAgent($param);
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //坐席ID为空
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '0',
            'ag_name' => 'cc',
        );
        $response = $agentModel->editAgent($param);
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空',
        );
        $this->assertEquals($response, $result);

        //坐席ID包含非数字字符
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1xx',
            'ag_name' => 'cc',
        );
        $response = $agentModel->editAgent($param);
        $result = array(
            'code' => 404,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //坐席名称为空
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'ag_name' => '',
        );
        $response = $agentModel->editAgent($param);
        $result = array(
            'code' => 405,
            'message' => '坐席名称为空',
        );
        $this->assertEquals($response, $result);

        //406 坐席类型不正确
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'ag_name' => 'test',
            'ag_role' => 2,
        );
        $response = $agentModel->editAgent($param);
        $result = array(
            'code' => 406,
            'message' => '坐席类型不正确',
        );
        $this->assertEquals($response, $result);

        //407 角色不属于该企业
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'ag_name' => 'test',
            'ag_role' => 1,
            'user_role' => 6,
        );
        $response = $agentModel->editAgent($param);
        $result = array(
            'code' => 407,
            'message' => '角色不属于该企业',
        );
        $this->assertEquals($response, $result);

        //ok
//        $param = array(
//            'vcc_id' => '110',
//            'ag_id' => '2031',
//            'ag_name' => 'admin',
//            'ag_password' => md5('ooxxooxx'),
//            'ag_role' => 0,
//            'user_role' => 7,
//        );
//        $response = $agentModel->editAgent($param);
//        $result = array(
//            'code' => 200,
//            'message' => 'ok'
//        );
//        $this->assertEquals($response, $result);
    }

    /**
     * 测试添加坐席
     */
    public function testAddAgent()
    {
        $agentModel = new AgentModel($this->container);
        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'que_id' => '1',
            'ag_num' => '123456',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertEquals($response, $result);

        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1cc',
            'que_id' => '1',
            'ag_num' => '123456',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //企业ID不存在；
        $param = array(
            'vcc_id' => '20000',
            'que_id' => '1',
            'ag_num' => '123456',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 403,
            'message' => '企业ID不存在',
        );
        $this->assertEquals($response, $result);

        //技能组ID为空
//        $param = array(
//            'vcc_id' => '10',
//            'que_id' => '0',
//            'ag_num' => '123456',
//            'ag_name' => 'tetName',
//            'ag_password' => md5('123456'),
//        );
//        $client->request('POST', '/agent/add', $param);
//        $response = $client->getResponse()->getContent();
//        $result = array(
//            'code' => 404,
//            'message' => '技能组ID为空',
//        );
//        $this->assertEquals($response, $result);

        //技能组ID包含非数字字符
        $param = array(
            'vcc_id' => '10',
            'que_id' => '1cc',
            'ag_num' => '123456',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 405,
            'message' => '技能组ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //技能组ID不存在
        $param = array(
            'vcc_id' => '10',
            'que_id' => '200000',
            'ag_num' => '123456',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 406,
            'message' => '技能组ID不存在',
        );
        $this->assertEquals($response, $result);
        //坐席工号为空
        $param = array(
            'vcc_id' => '10',
            'ag_num' => '',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 407,
            'message' => '坐席工号为空',
        );
        $this->assertEquals($response, $result);

        //坐席工号包含非数字字符
//        $param = array(
//            'vcc_id' => '10',
//            'que_id' => '14',
//            'ag_num' => '10000xx',
//            'ag_name' => 'tetName',
//            'ag_password' => md5('123456'),
//        );
//        $client->request('POST', '/agent/add', $param);
//        $response = $client->getResponse()->getContent();
//        $result = array(
//            'code' => 408,
//            'message' => '坐席工号包含非数字字符',
//        );
//        $this->assertEquals($response, $result);

        //坐席工号已经存在
        $param = array(
            'vcc_id' => '10',
            'ag_num' => '1001',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 409,
            'message' => '坐席工号已经存在',
        );
        $this->assertEquals($response, $result);
        //坐席名称为空
        $param = array(
            'vcc_id' => '10',
            'ag_num' => '100002',
            'ag_name' => '',
            'ag_password' => md5('123456'),
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 410,
            'message' => '坐席名称为空',
        );
        $this->assertEquals($response, $result);
        //坐席密码为空
        $param = array(
            'vcc_id' => '10',
            'ag_num' => '100003',
            'ag_name' => 'testName',
            'ag_password' => '',
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 411,
            'message' => '坐席密码为空',
        );
        $this->assertEquals($response, $result);

        //412 坐席类型不正确；
        $param = array(
            'vcc_id' => '10',
            'ag_num' => '100003',
            'ag_name' => 'testName',
            'ag_password' => md5('123456'),
            'ag_role' => 2,
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 412,
            'message' => '坐席类型不正确',
        );
        $this->assertEquals($response, $result);

        //413  坐席角色不属于该企业
        $param = array(
            'vcc_id' => '110',
            'ag_num' => '100003',
            'ag_name' => 'testName',
            'ag_password' => md5('123456'),
            'ag_role' => 1,
            'user_role' => 6,
        );
        $response = $agentModel->addAgent($param);
        $result = array(
            'code' => 413,
            'message' => '角色不属于该企业',
        );
        $this->assertEquals($response, $result);

        //414 添加坐席失败
        $stub = $this->getMockBuilder('AgentModel')->setMethods(array('addAgent'))->getMock();
        $stub->method('addAgent')->willReturn(array(
            'code' => 414,
            'message' => '添加坐席失败',
        ));
        $param = array(
            'vcc_id' => '1',
            'que_id' => '7',
            'ag_num' => '100005c',
            'ag_name' => 'testName',
            'ag_password' => md5('123456'),
            'ag_role' => 1,
            'user_role' => 3,
        );
        $response = $stub->addAgent($param);
        $this->assertContains('414', $response);

        //ok
        $stub = $this->getMockBuilder('AgentModel')->setMethods(array('addAgent'))->getMock();
        $stub->method('addAgent')->willReturn(array(
            'code' => 200,
            'message' => 'ok',
        ));
        $param = array(
            'vcc_id' => '1',
            'que_id' => '7',
            'ag_num' => '100005c',
            'ag_name' => 'testName',
            'ag_password' => md5('123456'),
            'ag_role' => 1,
            'user_role' => 3,
        );
        $response = $stub->addAgent($param);
        $this->assertContains('200', $response);
    }

    /**
     *
     * 获取企业下的坐席信息接口
     *
     */
    public function testAgentListAction()
    {
        $agentModel = new AgentModel($this->container);
        //企业ID为空
        $param = array(
            'vcc_id' => 0,
        );
        $response = $agentModel->listAgent($param);
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertEquals($response, $result);

        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1xc',
        );
        $response = $agentModel->listAgent($param);
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //坐席ID包含非数字字符
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1cc',
        );
        $response = $agentModel->listAgent($param);
        $result = array(
            'code' => 403,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //200 OK
        $param = array('vcc_id' => '10');
        $response = $agentModel->listAgent($param);
        $this->assertContains('200', $response);

        //200 OK
        $param = array('vcc_id' => '10','ag_id' => '14');
        $response = $agentModel->listAgent($param);
        $this->assertContains('200', $response);

        $param = array('vcc_id' => '110','que_id' => 4);
        $response = $agentModel->listAgent($param);
        $this->assertContains('200', $response);

        //200 OK
        $param = array(
            'vcc_id' => '10',
            'info' => array(
                'pagination' => array(
                    'rows' => 2,
                    'page' => 5
                )
            )
        );
        $response = $agentModel->listAgent($param);
        $this->assertContains('200', $response);
    }

    /**
     *
     * 修改坐席密码
     *
     */
    public function testUpdatepassAction()
    {
        $agentModel = new AgentModel($this->container);
        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'ag_id' => '952',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> md5('1234567'),
        );
        $response = $agentModel->updateAgentPassword($param);
        $result = array(
            'code' => 401,
            'message' => '企业ID为空',
        );
        $this->assertEquals($response, $result);

        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1xc',
            'ag_id' => '952',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password' => md5('1234567'),
        );
        $response = $agentModel->updateAgentPassword($param);
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //坐席ID为空
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '0',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> md5('1234567'),
        );
        $response = $agentModel->updateAgentPassword($param);
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空',
        );
        $this->assertEquals($response, $result);

        //坐席ID不为数字
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '952x',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> md5('1234567'),
        );
        $response = $agentModel->updateAgentPassword($param);
        $result = array(
            'code' => 404,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //旧密码为空
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '952',
            'old_password' => '',
            'new_password'=> md5('1234567'),
        );
        $response = $agentModel->updateAgentPassword($param);
        $result = array(
            'code' => 405,
            'message' => '旧密码为空',
        );
        $this->assertEquals($response, $result);

        //新密码和原始密码一样
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '952',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> md5('123456'),
        );
        $response = $agentModel->updateAgentPassword($param);
        $result = array(
            'code' => 409,
            'message' => '新密码和原始密码一样，无需修改',
        );
        $this->assertEquals($response, $result);

        //旧密码错误
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '952',
            'old_password' => 'afafsafdsafdsafd',
            'new_password'=> md5('1234567'),
        );
        $response = $agentModel->updateAgentPassword($param);
        $result = array(
            'code' => 406,
            'message' => '旧密码错误',
        );
        $this->assertEquals($response, $result);

        //新密码为空
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '2245',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> '',
        );
        $response = $agentModel->updateAgentPassword($param);
        $result = array(
            'code' => 407,
            'message' => '新密码为空',
        );
        $this->assertEquals($response, $result);

        //408 修改失败
        $stub = $this->getMockBuilder('AgentModel')->setMethods(array('updateAgentPassword'))->getMock();
        $stub->method('updateAgentPassword')->willReturn(array(
            'code' => 408,
            'message' => '修改失败',
        ));
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '2245',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> md5("1234567"),
        );
        $response = $stub->updateAgentPassword($param);
        $this->assertContains('408', $response);

        //200 ok
        $stub = $this->getMockBuilder('AgentModel')->setMethods(array('updateAgentPassword'))->getMock();
        $stub->method('updateAgentPassword')->willReturn(array(
            'code' => 200,
            'message' => 'ok',
        ));
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '2245',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> md5("1234567"),
        );
        $response = $stub->updateAgentPassword($param);
        $this->assertContains('200', $response);
    }

    /**
     *
     * 测试坐席登录接口
     *
     */
    public function testSignin()
    {
        $agentModel = new AgentModel($this->container);

        //企业ID为空
        $param = array(
            'vcc_code' => 0,
            'ag_num' => '100',
            'password' => 'aaa',
        );
        $response = $agentModel->signin($param);
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertEquals($response, $result);

        //坐席工号为空
        $param = array(
            'vcc_code' => 'wintel',
            'ag_num' => '',
            'password' => 'fcea920f7412b5da7be0cf42b8c93759',
        );
        $response = $agentModel->signin($param);
        $result = array(
            'code' => 402,
            'message' => '坐席工号为空',
        );
        $this->assertEquals($response, $result);

        //密码为空
        $param = array(
            'vcc_code' => 'wintel',
            'ag_num' => '100004',
            'password' => '',
        );
        $response = $agentModel->signin($param);
        $result = array(
            'code' => 403,
            'message' => '密码为空'
        );
        $this->assertEquals($response, $result);

        //404 验证失败；
        $param = array(
            'vcc_code' => 'wintel',
            'ag_num' => '100004',
            'password' => 'ccafdfadsafds',
        );
        $response = $agentModel->signin($param);
        $result = array(
            'code' => 404,
            'message' => '验证失败,没有匹配到相应数据',
        );
        $this->assertEquals($response, $result);

        //200 OK
        $stub = $this->getMockBuilder('AgentModel')->setMethods(array('signin'))->getMock();
        $stub->method('signin')->willReturn(array(
            'code' => 200,
            'message' => 'ok',
        ));
        $param = array(
            'vcc_code' => 'wintel',
            'ag_num' => '100004',
            'password' => 'fcea920f7412b5da7be0cf42b8c93759',
        );
        $response = $stub->signin($param);
        $this->assertContains("200", $response);
    }

    /**
     * 测试获取空闲坐席接
     */
    public function testGetFreeAgent()
    {
        $agentModel = new AgentModel($this->container);

        //企业ID为空
        $param = array('vcc_id' => 0);
        $response = $agentModel->getFreeAgent($param);
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertEquals($response, $result);

        //企业ID包含非数字字符
        $param = array('vcc_id' => '1xc');
        $response = $agentModel->getFreeAgent($param);
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //200 ok
        $param = array('vcc_id' => 10);
        $response = $agentModel->getFreeAgent($param);
        $this->assertContains("200", $response);
    }

    /**
     *
     * 测试获取当前通话中的坐席
     *
     */
    public function testOnthelineAction()
    {
        $agentModel = new AgentModel($this->container);

        //企业ID为空
        $param = array('vcc_id' => 0);
        $response = $agentModel->getOnlineAgent($param);
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertEquals($response, $result);

        //企业ID包含非数字字符
        $param = array('vcc_id' => '1xc');
        $response = $agentModel->getOnlineAgent($param);
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //200 ok
        $param = array('vcc_id' => 10);
        $response = $agentModel->getOnlineAgent($param);
        $this->assertContains("200", $response);
    }

    /**
     * 测试设置坐席转接电话接口
     */
    public function testSetAgentTransferPhone()
    {
        $agentModel = new AgentModel($this->container);

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'ag_id' => '100',
            'phone' => '18310132160',
            'state' => '1',
        );
        $response = $agentModel->setAgentTransferPhone($param);
        $result = array(
            'code' => 401,
            'message' => '企业ID为空',
        );
        $this->assertEquals($response, $result);

        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1x',
            'ag_id' => '100',
            'phone' => '18310132160',
            'state' => '1',
        );
        $response = $agentModel->setAgentTransferPhone($param);
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //坐席ID为空
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '0',
            'phone' => '18310132160',
            'state' => '1',
        );
        $response = $agentModel->setAgentTransferPhone($param);
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空',
        );
        $this->assertEquals($response, $result);

        //坐席ID包含非数字字符
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1x',
            'phone' => '18310132160',
            'state' => '1',
        );
        $response = $agentModel->setAgentTransferPhone($param);
        $result = array(
            'code' => 404,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //手机号不能为空
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'phone' => '',
            'state' => '1',
        );
        $response = $agentModel->setAgentTransferPhone($param);
        $result = array(
            'code' => 405,
            'message' => '手机号为空',
        );
        $this->assertEquals($response, $result);

        //手机号码含非数字字符
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'phone' => '1xx',
            'state' => '1',
        );
        $response = $agentModel->setAgentTransferPhone($param);
        $result = array(
            'code' => 406,
            'message' => '手机号码含非数字字符',
        );
        $this->assertEquals($response, $result);

        //手机号码不是11位
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'phone' => '183101321601',
            'state' => '1',
        );
        $response = $agentModel->setAgentTransferPhone($param);
        $result = array(
            'code' => 407,
            'message' => '手机号码不是11位',
        );
        $this->assertEquals($response, $result);

        //手机号码不是11位
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'phone' => '18310132160',
            'state' => '3',
        );
        $response = $agentModel->setAgentTransferPhone($param);
        $result = array(
            'code' => 408,
            'message' => '状态值不是0或1',
        );
        $this->assertEquals($response, $result);

        //对应的坐席不存在
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '10000000',
            'phone' => '18310132160',
            'state' => '1',
        );
        $response = $agentModel->setAgentTransferPhone($param);
        $result = array(
            'code' => 409,
            'message' => '对应的坐席不存在',
        );
        $this->assertEquals($response, $result);

        /** @var \PHPUnit_Framework_MockObject_MockObject $stub */
        $stub = $this->getMockBuilder('AgentModel')->setMethods(array('setAgentTransferPhone'))->getMock();
        $stub->method('setAgentTransferPhone')->willReturn(array(
            'code' => 200,
            'message' => 'ok',
        ));
        //200 oK
        $param = array(
            'vcc_id' => '10',
            'ag_id' => '16',
            'phone' => '18310132160',
            'state' => '1',
        );
        $response = $stub->setAgentTransferPhone($param);
        $this->assertContains("200", $response);
    }
}
