<?php

namespace Icsoc\DataBundle\Tests\Model;

use Icsoc\DataBundle\Model\MonitorModel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AgentControllerTest
 * @package Icsoc\DataBundle\Tests\Controller
 */
class MonitorModelTest extends WebTestCase
{
    /** @var \Symfony\Component\DependencyInjection\Container $container */
    private $container;

    /** @var  \Icsoc\DataBundle\Model\MonitorModel $model */
    private $model;

    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
        $this->model = new MonitorModel($this->container);
    }

    /**
     * 测试获取坐席监控数据
     */
    public function testAgentMonitor()
    {
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $response = $this->model->agentMonitor($param);
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertEquals($response, $result);

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $response = $this->model->agentMonitor($param);
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertEquals($response, $result);


        //坐席ID中含有非数字项
        $param = array(
            'vcc_code' => 'wintel',
            'user_ids' => '8001,8002,8003x',
        );
        $response = $this->model->agentMonitor($param);
        $result = array(
            'code' => 403,
            'message' => '坐席ID中含有非数字项',
        );
        $this->assertEquals($response, $result);

        //技能组ID包含非数字字符
        $param = array(
            'vcc_code' => 'wintel',
            'user_ids' => '8001,8002,8003',
            'que_id' => '52x',
        );
        $response = $this->model->agentMonitor($param);
        $result = array(
            'code' => 404,
            'message' => '技能组ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
        );
        $response = $this->model->agentMonitor($param);
        //echo 'xxx--------'.$response.'zzzzz';
        $this->assertEquals("200", isset($response['code']) ? $response['code'] : '');
    }

    /**
     * 测试获取技能组监控数据
     */
    public function testMonitorQueue ()
    {
        //测试企业代码；
        $this->testVccCode();
        //技能组ID中含有非数字项
        $param = array(
            'vcc_code' => 'wintel',
            'que_ids' => '8001,8002,8003x',
        );
        $response = $this->model->queueMonitor($param);
        $result = array(
            'code' => 403,
            'message' => '技能组ID中含有非数字项',
        );
        $this->assertEquals($response, $result);

        //200 Ok
        $param = array(
            'vcc_code' => 'wintel',
        );
        $response = $this->model->queueMonitor($param);
        //echo '------'.$response.'ccccxx';
        $this->assertEquals("200", isset($response['code']) ? $response['code'] : '');
    }

    /**
     * 测试获取排队监控数据
     */
    public function testMonitorCalls()
    {
        $this->testVccCode();
        //技能组ID中含有非数字项
        $param = array(
            'vcc_code' => 'wintel',
            'que_id' => '1x',
        );
        $response = $this->model->callsMonitor($param);
        $result = array(
            'code' => 403,
            'message' => '技能组ID包含非数字字符',
        );
        $this->assertEquals($response, $result);

        //200 Ok
        $param = array(
            'vcc_code' => 'wintel',
            'que_id' => '1',
        );
        $response = $this->model->callsMonitor($param);
        $this->assertEquals("200", isset($response['code']) ? $response['code'] : '');
    }

    /**
     * 测试获取坐席统计数据接口
     */
    public function testMonitorStadata()
    {
        $this->testVccCode();
        //用户ID为空
        $param = array(
            'vcc_code' => 'wintel',
            'user_ids' => '0',
            'start_date'=>'2013-05-20',
            'start_date'=>'2013-06-20',
        );
        $response = $this->model->stadataMonitor($param);
        $result = array(
            'code' => 403,
            'message' => '用户ID为空',
        );
        $this->assertEquals($response, $result);

        //用户ID中含有非数字项
        $param = array(
            'vcc_code' => 'wintel',
            'user_ids' => '1,2x',
            'start_date'=>'2013-05-20',
            'start_date'=>'2013-06-20',
        );
        $response = $this->model->stadataMonitor($param);
        $result = array(
            'code' => 404,
            'message' => '用户ID中含有非数字项',
        );
        $this->assertEquals($response, $result);

        //开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'user_ids' => '1,2',
            'start_date'=>'2013-c5-20x',
            'end_date'=>'2013-06-20',
        );
        $response = $this->model->stadataMonitor($param);
        $result = array(
            'code' => 405,
            'message' => '开始日期格式不正确',
        );
        $this->assertEquals($response, $result);

        //结束日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'user_ids' => '1,2',
            'start_date'=>'2013-05-20',
            'end_date'=>'2013-06c-20',
        );
        $response = $this->model->stadataMonitor($param);
        $result = array(
            'code' => 406,
            'message' => '结束日期格式不正确',
        );
        $this->assertEquals($response, $result);

        //200 Ok
        $param = array(
            'vcc_code' => 'wintel',
            'user_ids' => '14,15,16',
            'start_date'=>'2011-08-30',
            'end_date'=>'2011-08-31',
        );
        $response = $this->model->stadataMonitor($param);
        $this->assertEquals("200", isset($response['code']) ? $response['code'] : '');
    }


    /**
     * 获取坐席操作明细数据接口
     */
    public function testMonitorDetaildata()
    {
        $this->testVccCode();
        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $response = $this->model->detaildataMonitor($param);
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertEquals($response, $result);

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $response = $this->model->detaildataMonitor($param);
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        $this->assertEquals($response, $result);

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $response = $this->model->detaildataMonitor($param);
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertEquals($response, $result);

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"ag_name":"8","end_time":"2013-11-28 00:00:00"},"sort":{"field":"id","order":"desc"}}',
        );
        $response = $this->model->detaildataMonitor($param);
        //echo $response;
        $this->assertEquals("200", isset($response['code']) ? $response['code'] : '');
    }

    /**
     * 测试获取系统监控数据
     */
    public function testMonitorSystem()
    {
        //企业代码不存在
        $this->testVccCode();
        //200 ok
        $response = $this->model->systemMonitor(array('vcc_code'=>'wintel'));
        //echo $response;
        $this->assertEquals("200", isset($response['code']) ? $response['code'] : '');
    }

    /**
     *  测试 获取坐席 ivr 统计；
     */
    public function testGetIvr()
    {
        //企业代码不存在
        $this->testVccCode();
        //200 ok
        $response = $this->model->agentIvr(array('vcc_code'=>'wintel'));
        $this->assertEquals("200", isset($response['code']) ? $response['code'] : '');
    }


    /**
     *  测试 获取坐席 示忙理由；
     */
    public function testGetReason()
    {
        //企业代码不存在
        $this->testVccCode();
        //200 ok
        $response = $this->model->getReason(array('vcc_code'=>'wintel'));
        $this->assertEquals("200", isset($response['code']) ? $response['code'] : '');
    }

    /**
     * 测试企业代码
     */
    public function testVccCode()
    {
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $response = $this->model->agentMonitor($param);
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertEquals($response, $result);

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $response = $this->model->agentMonitor($param);
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertEquals($response, $result);
    }
}
