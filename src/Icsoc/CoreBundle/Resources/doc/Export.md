# 数据导出组件使用说明

## 如何使用

	//excel导出
	$export = $this->get('icsoc_core.export.excel');
	//csv导出
	$export = $this->get('icsoc_core.export.csv');

	//通过SQL语句导出
	$sql = ...;
	//导出字段
	$fields = array(...);
	//生成数据类型对象
	$source = new SqlType($sql, $fields);

	//通过数组导出
	$data = array(...);
	//生成数据类型对象
	$source = new ArrayType($data, $fields);

	//导出文件名称
	$fileName = ...;
	//执行导出
	$export->export($source, $fileName);

## 说明

### 1. SQL语句类型

 * sql语句中不允许带LIMIT，因为导出中需要内部处理分批次导出，即为正常的查询语句

### 2. 数组数据类型

 * 数组为包含所有字段的多维数组

### 3. fields，字段说明

 * fields的键为字段的名称，需要导出多少个字段即有多少个键
 * fields的值包含如下元素
     > 1. name：字段对应的中文名称，为空则使用字段本身的名称
     > 2. type：字段导出的数据类型：string、date、datetime、associatedArray，默认为string格式，不做其他处理，如果为date或datetime格式，则将该字段的值转为日期或日期时间格式，date('Y-m-d'),date('Y-m-d H:i:s')
     > 3. options：字段属性，例如单元格格式，具体格式参考PHPExcel中格式设置 
     > 4. associatedArray：关联数组值，如果type类型为associatedArray的情况下，需要设置该值，格式为二维数组，即字段名称为键值，对应的关联数组为值，如associatedArray = array('direction' => array('0' => '默认', '1' => '呼入', '2' => '呼出'))，如果有多个字段为关联的数组的，则设置多个字段对应的数组，如果没有设置，则使用原始的值

### 异常处理

> 调用函数时，如果失败会抛出异常ExportException，其中包含具体的信息，所以在执行导出时需要使用try、cache来捕获异常，导出成功则直接下载文件，如果捕获到异常就会停止执行下载

## 例子

	$sql = "SELECT * FROM win_trunk_cdr LIMIT 100";
    $fields = array(
        'vcc_id' => array(
            'name' => 'vcc_id',
            'type' => 'string',
            'options' => array()
        ),
        'channel' => array(
            'name' => '通道',
            'type' => 'string',
            'options' => array()
        ),
        'call_id' => array(
            'name' => 'call_id',
            'type' => 'string',
            'options' => array()
        ),
        'group' => array(
            'name' => '通道组',
            'type' => 'string',
            'options' => array()
        ),
        'chan_type' => array(
            'name' => '通道类型',
            'type' => 'string',
            'options' => array()
        ),
        'caller' => array(
            'name' => '主叫',
            'type' => 'string',
            'options' => array()
        ),
        'called' => array(
            'name' => '被叫',
            'type' => 'string',
            'options' => array()
        ),
        'direction' => array(
            'name' => '方向',
            'type' => 'associatedArray',
            'options' => array(),
            'associatedArray' => array(
                'direction' => array(
                    '0' => '默认',
                    '1' => '呼入',
                    '2' => '呼出'
                )
            )
        ),
        'start_time' => array(
            'name' => '开始时间',
            'type' => 'datetime',
            'options' => array()
        ),
        'ring_time' => array(
            'name' => '振铃时间',
            'type' => 'datetime',
            'options' => array()
        ),
        'ans_time' => array(
            'name' => '接通时间',
            'type' => 'datetime',
            'options' => array()
        ),
        'end_time' => array(
            'name' => '接通时间',
            'type' => 'datetime',
            'options' => array()
        ),
        'bill_sec' => array(
            'name' => '计费时长',
            'type' => 'string',
            'options' => array()
        ),
        'all_sec' => array(
            'name' => '总时长',
            'type' => 'string',
            'options' => array()
        ),
        'result' => array(
            'name' => '结果',
            'type' => 'string',
            'options' => array()
        ),
        'start_year' => array(
            'name' => '年',
            'type' => 'string',
            'options' => array()
        ),
        'start_month' => array(
            'name' => '月',
            'type' => 'string',
            'options' => array()
        ),
        'start_day' => array(
            'name' => '日',
            'type' => 'string',
            'options' => array()
        ),
    );
    try {
        $source = new SqlType($sql, $fields);
        $source = new ArrayType($data, $fields);
        $excel->export($source);
    } catch (\Exception $e) {
		//该异常根据实际情况处理
        var_dump($e->getMessage());
    }