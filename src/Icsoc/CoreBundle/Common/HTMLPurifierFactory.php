<?php

namespace Icsoc\CoreBundle\Common;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * html 过滤；
 * Class HTMLPurifierFactory
 * @package Topxia\WebBundle\Util
 */
class HTMLPurifierFactory
{

    protected $container;

    /**
     * @param $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $cacheDir
     */
    public function warmUp($cacheDir)
    {
        if (! is_dir($cacheDir)) {
            mkdir($cacheDir, 0777, true);
        }

        if (! is_writeable($cacheDir)) {
            chmod($cacheDir, 0777);
        }
    }

    /**
     * @return \Icsoc\DataBundle\Model\BaseModel
     */
    public function get()
    {

        return new \Icsoc\DataBundle\Model\BaseModel();
        /*
        $cacheDir = $this->container->getParameter('kernel.cache_dir') . '/htmlpurifier';
        $this->warmUp($cacheDir);

        $config = \HTMLPurifier_Config::createDefault();

        $config->set('Cache.SerializerPath', $cacheDir);
        $config->set('CSS.AllowTricky', true);
        $def = $config->getHTMLDefinition(true);
        $def->addAttribute('a', 'target', 'Enum#_blank,_self,_target,_top');

        return new \HTMLPurifier($config);*/
    }
}
