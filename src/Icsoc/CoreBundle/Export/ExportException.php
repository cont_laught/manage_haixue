<?php
/**
 * This file is part of easycrm, created by PhpStorm.
 * Author: LouXin
 * Date: 2014/12/23 13:36
 * File: ExportException.php
 */

namespace Icsoc\CoreBundle\Export;

/**
 * Exception is the base class for
 * all Exceptions.
 * @link http://php.net/manual/en/class.exception.php
 */
class ExportException extends \Exception
{

}
