<?php
/**
 * This file is part of easycrm, created by PhpStorm.
 * Author: LouXin
 * Date: 2014/12/23 11:36
 * File: ExcelExportTest.php
 */

namespace Icsoc\CoreBundle\Tests\Export;

use Icsoc\CoreBundle\Export\DataType\SqlType;
use Icsoc\CoreBundle\Export\ExcelExport;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExcelExportTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
    }

    public function testExport()
    {
        $excel = new ExcelExport($this->container);
        $sql = "SELECT * FROM win_trunk_cdr LIMIT 100";
        $fields = array(
            'vcc_id' => array(
                'name' => 'vcc_id',
                'type' => 'string',
                'options' => array()
            ),
            'channel' => array(
                'name' => '通道',
                'type' => 'string',
                'options' => array()
            ),
            'call_id' => array(
                'name' => 'call_id',
                'type' => 'string',
                'options' => array()
            ),
            'group' => array(
                'name' => '通道组',
                'type' => 'string',
                'options' => array()
            ),
            'chan_type' => array(
                'name' => '通道类型',
                'type' => 'string',
                'options' => array()
            ),
            'caller' => array(
                'name' => '主叫',
                'type' => 'string',
                'options' => array()
            ),
            'called' => array(
                'name' => '被叫',
                'type' => 'string',
                'options' => array()
            ),
            'direction' => array(
                'name' => '方向',
                'type' => 'associatedArray',
                'options' => array(),
                'associatedArray' => array(
                    'direction' => array(
                        '0' => '默认',
                        '1' => '呼入',
                        '2' => '呼出'
                    )
                )
            ),
            'start_time' => array(
                'name' => '开始时间',
                'type' => 'datetime',
                'options' => array()
            ),
            'ring_time' => array(
                'name' => '振铃时间',
                'type' => 'datetime',
                'options' => array()
            ),
            'ans_time' => array(
                'name' => '接通时间',
                'type' => 'datetime',
                'options' => array()
            ),
            'end_time' => array(
                'name' => '接通时间',
                'type' => 'datetime',
                'options' => array()
            ),
            'bill_sec' => array(
                'name' => '计费时长',
                'type' => 'string',
                'options' => array()
            ),
            'all_sec' => array(
                'name' => '总时长',
                'type' => 'string',
                'options' => array()
            ),
            'result' => array(
                'name' => '结果',
                'type' => 'string',
                'options' => array()
            ),
            'start_year' => array(
                'name' => '年',
                'type' => 'string',
                'options' => array()
            ),
            'start_month' => array(
                'name' => '月',
                'type' => 'string',
                'options' => array()
            ),
            'start_day' => array(
                'name' => '日',
                'type' => 'string',
                'options' => array()
            ),
        );
//        try {
//            $source = new SqlType($sql, $fields);
//            $excel->export($source);
//        } catch (\Exception $e) {
//            var_dump($e->getMessage());
//        }
        $this->assertTrue(true);
    }
}
