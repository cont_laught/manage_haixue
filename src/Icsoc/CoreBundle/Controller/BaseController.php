<?php

namespace Icsoc\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * 基本控制器用于权限控制；
 * Class BaseController
 * @package Icsoc\CoreBundle\Controller
 */
class BaseController extends Controller implements AuthenticatedInteface
{

    /**
     * @param $str
     * @param array $arr
     * @return string
     */
    protected function trans($str, $arr = array())
    {
        return $this->get('translator')->trans($str, $arr);
    }

    /**
     * 获取vccId
     * @return mixed
     */
    protected function getVccId()
    {
        return $this->getUser()->getVccId();
    }

    /**
     * 获取vccCode
     * @return mixed
     */
    protected function getVccCode()
    {
        return $this->getUser()->getVccCode();
    }

    /**
     * 获取role_grade
     * @return mixed
     */
    protected function getRoleGrade()
    {
        return $this->getUser()->getRoleGrade();
    }

    /**
     * 获取doctrine管理
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getEm()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * 获取权限；
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getActionList()
    {
        return $this->getUser()->getActionList();
    }
}
