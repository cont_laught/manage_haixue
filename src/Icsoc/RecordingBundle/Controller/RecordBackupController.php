<?php

namespace Icsoc\RecordingBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Icsoc\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RecordBackupController
 *
 * @package Icsoc\RecordingBundle\Controller
 */
class RecordBackupController extends BaseController
{
    /**
     *录音备份下载页面
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recordBackupListAction()
    {

        return $this->render('IcsocRecordingBundle:Record:recordBackup.html.twig');
    }

    /**
     * 录音备份数据
     * @param Request $request
     * @return JsonResponse
     */
    public function recordBackupDataAction(Request $request)
    {
        $param['vcc_id'] = $this->getVccId();
        $param['page'] = $request->get('page', 1);
        $param['rows'] = $request->get('rows', 10);
        $param['order'] = $request->get('sord', 'desc');
        $param['startTime'] = $request->get('startTime', '');
        $param['endTime'] = $request->get('endTime', '');

        $result =  $this->get('icsoc_data.model.record')->getRecordBackupData($param);

        if (isset($result['code']) && $result['code'] == 200) {
            return new JsonResponse($result);
        }

        return new JsonResponse(
            array(
                'code'=>$result['code'],
                'message'=>$result['message'],
                'records' => 0,
                'page' => 0,
                'total' => 0,
                'rows'=>array(),
            )
        );
    }

    /**
     * 创建录音备份任务
     * @param Request $request
     * @return JsonResponse
     */
    public function recordBackupCreatAction(Request $request)
    {
        $param['vcc_id'] = $this->getVccId();
        $param['startTime'] = $request->get('startTime', '');
        $param['endTime'] = $request->get('endTime', '');

        $result =  $this->get('icsoc_data.model.record')->creatRecordBackup($param);

        return new JsonResponse($result);
    }
}
