<?php

namespace Icsoc\BusinessGroupBundle\Controller;

use Icsoc\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DefaultController
 *
 * @package Icsoc\BusinessGroupBundle\Controller
 */
class DefaultController extends BaseController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('BusinessGroupBundle:Default:index.html.twig');
    }

    /**
     * @param Request $request
     *
     * @return array|JsonResponse
     */
    public function listAction(Request $request)
    {
        $param['vcc_id'] = $this->getVccId();
        $param['page'] = $request->get('page', 1);
        $param['rows'] = $request->get('rows', 10);
        $param['order'] = $request->get('sord', 'desc');
        $param['field'] = $request->get('sidx', 'group_id');
        $fliter = $request->get('fliter', '');
        if (!empty($fliter)) {
            $fliter = json_decode($fliter, true);
            if (json_last_error()) {
                return array('code' => 401, 'message' => 'fliter格式非json');
            }
            if (isset($fliter['keyword']) && !empty($fliter['keyword'])) {
                $param['group_name'] = $fliter['keyword'];
            }
        }

        $result = $this->get('icsoc_data.model.businessgroup')->listBusinessGroup($param);

        if (isset($result['code']) && $result['code'] == 200) {
            return new JsonResponse($result);
        }

        return new JsonResponse(
            array(
                'code' => $result['code'],
                'message' => $result['message'],
                'records' => 0,
                'page' => 0,
                'total' => 0,
                'rows' => array(),
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function allotAction(Request $request)
    {
        $vccId = $this->getVccId();
        $oper = $request->get('oper', '');
        $groupName = $request->get('group_name', '');
        $id = $request->get('id', '');
        $result = array('code' => 403, 'message' => '参数不存在');

        if (!empty($oper)) {
            switch ($oper) {
                case 'add':
                    $result = $this->get('icsoc_data.model.businessgroup')->addBusinessGroup($vccId, $groupName);
                    break;
                case 'edit':
                    $result = $this->get('icsoc_data.model.businessgroup')->editBusinessGroup($id, $groupName, $vccId);
                    break;
                case 'del':
                    $result = $this->get('icsoc_data.model.businessgroup')->deleteBusinessGroup($id, $vccId);
                    break;
            }
        }

        return new JsonResponse($result);
    }

    /**
     * 批量给业务组分配坐席页面
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function batchAction(Request $request)
    {
        $groupId = $request->get("group_id", '');
        $vccId = $this->getVccId();
        $groups = array();
        //业务组
        $rst = $this->get('icsoc_data.model.businessgroup')->getGroupsBusinessGroup($vccId);
        if (isset($rst['code']) && $rst['code'] == 200) {
            $groups = $rst['data'];
        }
        if (empty($groupId) && !empty($groups)) {
            $groupIds = array_keys($groups);
            $groupId = $groupIds[0];
        }

        return $this->render("BusinessGroupBundle:Default:batch.html.twig", array(
            'groups' => $groups,
            'group_id' => $groupId,
        ));
    }

    /**
     * 获取未分配的坐席
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAgentAction(Request $request)
    {
        $vccCode = $this->getVccCode();
        $keyword = $request->get("keyword", '');
        $data = array();
        $arr = array('vcc_code' => $vccCode, 'group_id' => -1, 'ag_role' => true);
        if (!empty($keyword)) {
            $arr['info'] = json_encode(array('filter' => array('assign_keyword' => $keyword)));
        }
        //查询已经分配的坐席
        $list = $this->get('icsoc_data.model.agent')->listAgent($arr);
        if (isset($list['code']) && $list['code'] == 200) {
            $data = $list['data'];
        }

        return new JsonResponse(array('rows' => $data));
    }

    /**
     * 获取已经分配了的坐席
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAssignAgentAction(Request $request)
    {
        $vccCode = $this->getVccCode();
        $groupId = $request->get('group_id');
        $keyword = $request->get("keyword", '');
        $data = array();
        if (!empty($groupId)) {
            $arr = array('vcc_code' => $vccCode, 'group_id' => $groupId, 'ag_role' => true);
            $arr['info'] = json_encode(array('filter' => array('assign_keyword' => $keyword)));
            $list = $this->get('icsoc_data.model.agent')->listAgent($arr);
            if (isset($list['code']) && $list['code'] == 200) {
                $data = $list['data'];
            }
        }

        return new JsonResponse(array('rows' => $data));
    }

    /**
     * 批量给业务组分配坐席
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function allotAgentAction(Request $request)
    {
        $vccId = $this->getVccId();
        $groupId = $request->get('group_id', '');
        $flag = $request->get('flag', 1);
        $param = array('vcc_id' => $vccId, 'group_id' => $groupId, 'flag' => $flag);
        $ids = $request->get("ids", '');
        $ids = explode(",", $ids);
        $param['ids'] = $ids;
        $rst = $this->get('icsoc_data.model.businessgroup')->allotAgentBusinessGroup($param);
        if ($rst['code'] == 200) {
            return new JsonResponse(array('error' => 0));
        }

        return new JsonResponse(array('error' => 1, 'message' => $rst['message']));
    }

//    /**
//     * @param Request $request
//     * @return JsonResponse
//     */
//    public function checkGroupNameAction(Request $request)
//    {
//        $vccId = $this->getVccId();
//        $groupName = $request->get('group_name', '');
//        $id = $request->get('id', '');
//
//        $result = $this->get('icsoc_data.model.businessgroup')->checkBusinessGroup($vccId, $groupName, $id);
//
//        return new JsonResponse($result);
//    }
}
