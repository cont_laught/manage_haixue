<?php

namespace Icsoc\PhoneGroupBundle\Controller;

use Icsoc\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends BaseController
{
    /**
     * 号码组
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        if($request->getMethod() == 'POST') {
            $vccId = $this->getVccId();
            $arr = array('vcc_id'=>$vccId,'join'=>false);
            if (!empty($keyword)) {
                $arr['info'] = json_encode($keyword);
            }
            $id = $request->get('id','');
            $data = array();
            //查询已经分配的坐席
            $arr['id'] = $id;
            $list = $this->get('icsoc_data.model.phonegroup')->listPhone($arr);
            if (isset($list['code']) && $list['code'] == 200) {
                $data = $list['data'];
            }

            return new JsonResponse($data);
        }


        return $this->render('PhoneGroupBundle:Default:index.html.twig');
    }

    /**
     * 号码组列表数据
     * @param Request $request
     * @return array|JsonResponse
     */
    public function listAction(Request $request)
    {
        $param['vcc_id'] = $this->getVccId();
        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);
        $sord = $request->get('sord', 'desc');
        $sidx = $request->get('sidx', 'id');
        $fliter = $request->get('fliter', '');
        $info = array(
            'pagination'=>array('rows'=>$rows, 'page'=>$page),
            'filter'=> !empty($fliter) ? json_decode($fliter, true) : array(),
            'sort'=>array('order'=>$sord, 'field'=>$sidx),
        );
        $param['info'] = json_encode($info);

        $list = $this->get('icsoc_data.model.phonegroup')->listPhoneGroup($param);
        $data  = array();
        if (isset($list['data']) && !empty($list['data'])) {
            $data['rows'] = $list['data'];
        }
        $data['page'] = isset($list['page']) ? $list['page'] : 1;
        $data['total'] = isset($list['totalPage']) ? $list['totalPage'] : 0;
        $data['records'] = isset($list['total']) ? $list['total'] : 0;

        return new JsonResponse($data);
    }

    /**
     * 添加，修改，删除 号码组
     * @param Request $request
     * @return JsonResponse
     */
    public function allotAction(Request $request)
    {
        $vccId = $this->getVccId();
        $oper = $request->get('oper', '');
        $groupName = $request->get('group_name', '');
        $phoneId = $request->get('phoneId', '');
        $remark = $request->get('remark', '');
        $id = $request->get('id', '');
        $result = array('code'=>403, 'message'=>'参数不存在');

        if (!empty($oper)) {
            switch ($oper) {

                case 'add':
                    $result = $this->get('icsoc_data.model.phonegroup')->addPhoneGroup($vccId, $groupName, $phoneId,$remark);
                    break;
                case 'edit':
                    $result = $this->get('icsoc_data.model.phonegroup')->editPhoneGroup($id, $groupName, $vccId, $phoneId,$remark);
                    break;
                case 'del':
                    $result = $this->get('icsoc_data.model.phonegroup')->deletePhoneGroup($id, $vccId);
                    break;
            }
        }

        return new JsonResponse($result);
    }
}
