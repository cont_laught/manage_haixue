<?php

namespace Icsoc\GuideBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class GuideController
 * @package Icsoc\GuideBundle\Controller
 */
class GuideController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('IcsocGuideBundle:Guide:guide.html.twig');
    }
}
