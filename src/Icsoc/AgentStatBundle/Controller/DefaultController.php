<?php

namespace Icsoc\AgentStatBundle\Controller;

use Icsoc\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package Icsoc\AgentStatBundle\Controller
 */
class DefaultController extends BaseController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $vccId = $this->getVccId();
        $result = $this->get('icsoc_data.model.agentstat')->getAgentStat($vccId);
        $code = isset($result['code']) ? $result['code'] : '';

        if ($code == 200) {
            $data = $result['data'];
        } else {
            $data = array();
        }

        return $this->render('AgentStatBundle:Default:index.html.twig', array('data'=>$data));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {
        $translator = $this->get('translator');

        $message = $translator->trans('Save Succeed');
        $type = 'success';

        $vccId = $this->getVccId();
        $option = $request->get("option", '');
        //$oldOption = $request->get("old_option", '');
        //查询原来的置忙原因
        $result = $this->get('icsoc_data.model.agentstat')->getAgentStat($vccId);
        if ($result['code'] != 200) {
            $message = "置忙原因修改失败";
            $type = 'danger';
            goto end;
        }
        $oldOption = array();
        foreach ($result['data'] as $val) {
            $oldOption[$val['id']] = $val['stat_reason'];
        }
        $option = is_array($option) ? $option : array();
        $oldOption = is_array($oldOption) ? $oldOption : array();
        foreach ($option as $k => $v) {
            if (empty($v)) {
                unset($option[$k]);
            }
        }
        $option = array_unique($option);
        $delete = $oldOption;
        $insert = $option;

        if (count($option) > 5) {
            $message = "置忙原因不能超过5个";
            $type = 'danger';
            goto end;
        }

        if (!empty($option) && !empty($oldOption)) {
            $delete = array_diff($oldOption, $option);
            $insert = array_diff($option, $oldOption);
        }

        if (empty($delete) && empty($insert)) {
            $message = $translator->trans("Don't do any changes");
            $type = 'danger';
        }

        /** 删除置忙原因 */
        if (!empty($delete)) {
            $ids = array_keys($delete);
            $result = $this->get('icsoc_data.model.agentstat')->deleteAgentStat(
                array(
                    'vcc_id'=>$vccId,
                    'ids'=>$ids,
                )
            );

            $code = isset($result['code']) ? $result['code'] : '';
            $message = isset($result['message']) ? $result['message'] : '';

            if ($code != '200') {
                $message = $translator->trans('Save Failure').','.$message;
                $type = 'danger';
            }
        }

        /** 添加置忙原因 */
        if (!empty($insert)) {
            $result = $this->get('icsoc_data.model.agentstat')->addAgentStat(
                array(
                    'vcc_id'=>$vccId,
                    'ag_stat'=>1,
                    'data'=>$insert,
                )
            );

            $code = isset($result['code']) ? $result['code'] : '';
            $message = isset($result['message']) ? $result['message'] : '';

            if ($code != '200') {
                $message = $translator->trans('Save Failure').','.$message;
                $type = 'danger';
            }
        }

        end:
        $data = array(
            'data'=>array(
                'msg_detail'=>$message,
                'type'=>$type,
                'link' => array(
                    array(
                        'text'=>$translator->trans('Busy reason Settings page'),
                        'href'=>$this->generateUrl('icsoc_agentstat_index'),
                    ),
                ),
            ),
        );

        return $this->redirect($this->generateUrl('icsoc_ui_message', $data));
    }
}
