<?php
/**
 * This file is part of cdr-bundle.
 * Author: tangzhou
 * Date: 2016/4/28
 * Time: 11:32
 * File: Vcc6016041301Controller.php
 */

namespace Ivr\HttpBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * 百度医生IVR 接口
 * Class Vcc6016041301Controller
 * @package Ivr\HttpBundle\Controller
 */
class Vcc6016041301Controller extends Controller
{
    /**
     * 查询最近15天的cusPhone;
     * @param Request $request
     * @return JsonResponse
     */
    public function getCusPhoneAction(Request $request)
    {
        $vccId = $request->get('vcc_id', '');
        $caller = $request->get('caller', '');
        if (empty($vccId) || empty($caller)) {
            return new JsonResponse(array(
                'result' => 'failure',
            ));
        }
        $date = date("Y-m-d 00:00:00", time());
        //15天以前的
        $endTime = strtotime($date);
        $startTime = $endTime - 15*3600*24;
        $conn = $this->get("doctrine.dbal.default_connection");
        $cusPhone = $conn->fetchColumn(
            "SELECT cus_phone FROM win_agcdr
            WHERE result = 0 AND ag_phone = ? AND start_time >= ? AND start_time <= ?
            ORDER BY start_time DESC LIMIT 1",
            array($caller, $startTime, time())
        );

        if (empty($cusPhone)) {
            return new JsonResponse(array(
                'result' => 'failure',
            ));
        } else {
            return new JsonResponse(array(
                'result' => 'success',
                'data' => array('cus_phone'=>$cusPhone),
            ));
        }
    }
}