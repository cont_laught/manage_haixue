<?php
/**
 * This file is part of manage.
 * Author: tangzhou
 * Date: 2016/9/22
 * Time: 16:38
 * File: Vcc6016091901Controller.php
 */

namespace Ivr\HttpBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


class Vcc6016091901Controller extends Controller
{
    const API_KEY = '8475ca5b1d947b34tyue';
    const URL = 'http://sxyl.it371.cn/weixin/sx/sp?mobile=%s&sign=%s';

    public function vipAction(Request $request)
    {
        $caller = $request->get('caller'); //获取号码

//        if (empty($callerPhone) || empty($msg) || !$this->checkPhoneIsMobile($callerPhone)) {
//            return new JsonResponse(array(
//                'result' => 'failure',
//            ));
//        }
        $signKey = md5(self::API_KEY.$caller);
        $url = sprintf(self::URL, $caller, $signKey);
        $res = file_get_contents($url);
        if (!empty($res)) {
            $responseArray = json_decode($res, true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                $this->get("logger")->error(sprintf('6016091901 响应结果为非json格式，结果为【%s】.', $res));
                return new JsonResponse(array(
                    'result' => 'failure',
                ));
            }

            $result = isset($responseArray['data']['flag']) ? $responseArray['data']['flag'] : '';
            if ($result == 200) {
                return new JsonResponse(array(
                    'result' => 'success',
                    'data'=>array(
                        'isvip'=>true,
                    )
                ));
            } else {
                return new JsonResponse(array(
                    'result' => 'failure',
                ));
            }

        } else {
            return new JsonResponse(array(
                'result' => 'failure',
            ));
        }
    }
}