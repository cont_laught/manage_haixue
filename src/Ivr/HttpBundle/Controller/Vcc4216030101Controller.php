<?php
/**
 * This file is part of manage.
 * Author: tangzhou
 * Date: 2016/5/17
 * Time: 14:09
 * File: Vcc8015101601Controller.php
 */

namespace Ivr\HttpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * 百度医生Ivr验证接口
 * Class Vcc8015101601Controller
 * @package Ivr\HttpBundle\Controller
 */
class Vcc4216030101Controller extends Controller
{
    const CHECK_URL = 'http://docapi.baidu.com/serviceomp/servplatapi/searchappointmentphone';
    const SKEY = 'Cq1Rd7qa3Dev';

    /**
     * 获取号码
     * @param Request $request
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $phone = $request->get("caller", '');
        $called = $request->get('called', '');
        $servType = $request->get('servType', '');
        $captcha = $request->get('captcha', '');

        if (empty($phone) || empty($called) || empty($servType)) {
            $this->get("logger")->error(sprintf("phone或trunknumber或servType为空,phone【%s】,called【%s】,servType【%s】", $phone, $called, $servType));

            return new JsonResponse(array(
                'result' => 'failure',
            ));
        }

        if ($servType == 2 && strlen($captcha) != 5) {
            $this->get("logger")->error(sprintf("captcha格式不正确【%s】", $captcha));

            return new JsonResponse(array(
                'result' => 'failure',
            ));
        }

        $key = $this->getKey();

        if ($servType == 1) {
            $params = array('servtype'=>1, 'phone'=>$phone, 'trunknumber'=>$called, 's'=>$key);
        } else {
            $params = array('servtype'=>2, 'servicephone'=>$phone, 'publicphone'=>$called, 's'=>$key, 'captcha'=>$captcha);
        }
        $this->get("logger")->error("发送params".json_encode($params));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::CHECK_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);

        $response = curl_exec($ch);
        $responseArray = json_decode($response, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            $this->get("logger")->error(sprintf('响应结果为非json格式，结果为【%s】.', $response));

            return new JsonResponse(array(
                'result' => 'failure',
            ));
        }
        //$responseArray = array('result'=>'success', 'data'=>array('servicephone'=>'18310132160', 'publicphone'=>'58452430'));
        $result = isset($responseArray['result']) ? $responseArray['result'] : 'failure';
        if ($servType == 1) {
            $called = isset($responseArray['data']['servicephone']) ? $responseArray['data']['servicephone'] : '';
            $caller = isset($responseArray['data']['publicphone']) ? $responseArray['data']['publicphone'] : '';
        } else {
            $called = isset($responseArray['data']['phone']) ? $responseArray['data']['phone'] : '';
            $caller = isset($responseArray['data']['trunknumber']) ? $responseArray['data']['trunknumber'] : '';
        }

        $this->get("logger")->error(sprintf("获取到caller【%s】called【%s】", $caller, $called));
        if ($result == 'success' && !empty($called) && !empty($caller)) {
            $this->get("logger")->info(sprintf('8015101601获取成功，结果为 %s.', $response));

            return new JsonResponse(array(
                'result' => 'success',
                'data' => array(
                    'code' => '200',
                    'caller_phone' => $caller,
                    'called_phone' => $called,
                ),
            ));
        } else {
            $this->get("logger")->error(sprintf('8015101601获取失败%s', $response));

            return new JsonResponse(array(
                'result' => 'failure',
            ));
        }
    }


    /**
     * 获取key
     * @return string
     */
    private function getKey()
    {
        $currentTimeStamp = time();
        $stringkey = strval(intval($currentTimeStamp/100)).self::SKEY;
        $md5key = md5($stringkey);
        $key = strtolower($md5key);

        return $key;
    }
}
