<?php
/**
 * This file is part of manage.
 * Author: FKL
 * Date: 2017/10/25
 * Time: 10:55
 * File: Vcc6017070301Controller.php
 */

namespace Ivr\HttpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * 区别省份
 * Class Vcc6017070301Controller
 * @package Ivr\HttpBundle\Controller
 */
class Vcc6017070301Controller extends Controller
{
    //河南省的所有区号
    public $area = array(
        '0371',
        '0378',
        '0379',
        '0375',
        '0372',
        '0392',
        '0373',
        '0391',
        '0393',
        '0374',
        '0395',
        '0398',
        '0370',
        '0394',
        '0396',
        '0377',
        '0376',
        '0391',
    );
    /**
     * 区分省份（河南）
     * @param Request $request
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $phone = $request->get('phone', '');

        try {
            //获取归属地
            $res = $this->container->get("icsoc_data.model.location")->getNumberloc($phone);

            if ($res['code'] != 200 || empty($res['data']['code'])) {
                return new JsonResponse(array('result' => 'failure'));
            }
            if (!in_array($res['data']['code'], $this->area)) {
                return new JsonResponse(array('result' => 'failure'));
            }

            return new JsonResponse(array(
                'result' => 'success',
                'data' => array(
                    'is_henan' => 1,
                ),
            ));
        } catch (\Exception $e) {
            $this->get("logger")->error(sprintf("区号查询失败，号码【%s】，报错【%s】", $phone, $e->getMessage()));

            return new JsonResponse(array('result' => 'failure'));
        }
    }
}
