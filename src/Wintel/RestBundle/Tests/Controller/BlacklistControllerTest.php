<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class BlacklistControllerTest
 * @package Wintel\RestBundle\Tests\Controller
 */
class BlacklistControllerTest extends WebTestCase
{
    public function testBlacklistAddAction()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'phones' => '8001,8002,8003',
            'phone_type' => 1
        );
        $client->request('POST', '/blacklist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码为空
        $param = array(
            'vcc_id' => 1,
            'phones' => '',
            'phone_type' => 1
        );
        $client->request('POST', '/blacklist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '号码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //固定号码中包含非数字字符
        $param = array(
            'vcc_id' => 1,
            'phones' => '8001,800d,8003',
            'phone_type' => 1
        );
        $client->request('POST', '/blacklist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '固定号码中包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID为非数字
        $param = array(
            'vcc_id' => 'a',
            'phones' => '8001,800d,8003',
            'phone_type' => 1
        );
        $client->request('POST', '/blacklist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '企业ID为非数字'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码格式不对，要求为多个号码逗号分隔的字符串，但是得到
        $param = array(
            'vcc_id' => 1,
            'phones' => array(8001,8002,8003),
            'phone_type' => 1
        );
        $client->request('POST', '/blacklist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '号码格式不对，要求为多个号码逗号分隔的字符串，但是得到array'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码格式不对，要求为多个号码逗号分隔的字符串，但是得到
        $param = array(
            'vcc_id' => 1,
            'phones' => '8**a,8dd2,80a3',
            'phone_type' => 2
        );
        $client->request('POST', '/blacklist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '模糊匹配号码中包含除*外的非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 中继号码不存在
        $param = array(
            'vcc_id' => 1,
            'phones' => '8001,8002,8003',
            'phone_type' => 1,
            'trunk_num' =>'1234560',
        );
        $client->request('POST', '/blacklist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '中继号码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_id' => 1,
            'phones' => '8001,8002,8003',
            'phone_type' => 1
        );
        $client->request('POST', '/blacklist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_id' => 1,
            'phones' => '8***,8004',
            'phone_type' => 2
        );
        $client->request('POST', '/blacklist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试删除黑名单
     */
    public function testBlacklistDeleteAction()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'phone_ids' => '1,2,3',
        );
        $client->request('POST', '/blacklist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码为空
        $param = array(
            'vcc_id' => 1,
            'phone_ids' => '',
        );
        $client->request('POST', '/blacklist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '号码ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码中包含非数字字符
        $param = array(
            'vcc_id' => 1,
            'phone_ids' => '80a1,800d,a003',
        );
        $client->request('POST', '/blacklist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '号码ID中包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => 'a',
            'phone_ids' => '8001,8002,8003',
        );
        $client->request('POST', '/blacklist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '企业ID包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码ID格式不对，要求为多个号码ID逗号分隔的字符串，但是得到
        $param = array(
            'vcc_id' => 1,
            'phone_ids' => array('8001,8002,8003'),
        );
        $client->request('POST', '/blacklist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '号码ID格式不对，要求为多个号码ID逗号分隔的字符串，但是得到array'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_id' => 1,
            'phone_ids' => '3,4,5',
        );
        $client->request('POST', '/blacklist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试查询黑名单
     */
    public function testBlacklistListAction()
    {
        $client = static::createClient();

        //json中没有vcc_code
        $info = '{"pagination":{"rows":20,"page":"5"},"filter":{"phone":"1025"},"sort":{"field":"id","order":"desc"}}';
        $client->request('GET', '/blacklist/list/'.$info);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => 'json中没有vcc_code'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //info格式非json
        $info = 'cxxa';
        $client->request('GET', '/blacklist/list/'.$info);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //401
        $info = '{"vcc_code":"","pagination":{"rows":20,"page":"5"},"filter":{"phone":"1025"},"sort":{"field":"id","order":"desc"}}';
        $client->request('GET', '/blacklist/list/'.$info);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //402
        $info = '{"vcc_code":"ccxx","pagination":{"rows":20,"page":"5"},"filter":{"phone":"1025"},"sort":{"field":"id","order":"desc"}}';
        $client->request('GET', '/blacklist/list/'.$info);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok  200
        $info = '{"vcc_code":"wintel","pagination":{"rows":1,"page":"2"},"filter":{"phone":"80"},"sort":{"field":"id","order":"desc"}}';
        $client->request('GET', '/blacklist/list/'.$info);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
