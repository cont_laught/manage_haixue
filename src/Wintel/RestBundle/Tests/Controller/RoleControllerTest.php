<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RoleControllerTest extends WebTestCase
{

    /**
     * 测试添加角色
     */
    public function testRoleAddAction()
    {
        $client = static::createClient();

        //402
        $client->request('GET', '/role/add/cc/xx/1');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        $client->request('GET', '/role/add/wintel/xx/1');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '角色已经存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405
        $client->request('GET', '/role/add/wintel/xxcc/cc');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '角色等级有误'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $client->request('GET', '/role/add/wintel/yy/1');
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试编辑角色
     */
    public function testRoleEditAction()
    {
        $client = static::createClient();

        //企业ID为空 401
        $param = array(
            'vcc_code' => '',
            'name' => 'hello',
            'role_grade' => '1',
            'role_id' => '5',
        );
        $client->request('POST', '/role/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID为空 402
        $param = array(
            'vcc_code' => 'cccc',
            'name' => 'hello',
            'role_grade' => '1',
            'role_id' => '5',
        );
        $client->request('POST', '/role/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 角色名称为空
        $param = array(
            'vcc_code' => 'wintel',
            'name' => '',
            'role_grade' => '1',
            'role_id' => '5',
        );
        $client->request('POST', '/role/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '角色名称为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404 角色id 不属于该企业
        $param = array(
            'vcc_code' => 'wintel',
            'name' => 'yy',
            'role_grade' => '1',
            'role_id' => '20',
        );
        $client->request('POST', '/role/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '角色不属于该企业'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405 角色已经存在
        $param = array(
            'vcc_code' => 'wintel',
            'name' => 'xx',
            'role_grade' => '1',
            'role_id' => '5',
        );
        $client->request('POST', '/role/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '角色已经存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //406 角色等级有误
        $param = array(
            'vcc_code' => 'wintel',
            'name' => 'yy',
            'role_grade' => '5',
            'role_id' => '5',
        );
        $client->request('POST', '/role/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '角色等级有误'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_code' => 'wintel',
            'name' => 'hello',
            'role_grade' => '3',
            'role_id' => '5',
        );
        $client->request('POST', '/role/edit', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *测试删除
     */
    public function testRoleDeleteAction()
    {
        $client = static::createClient();

        //402
        $client->request('GET', '/role/delete/1cc/1');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403
        $client->request('GET', '/role/delete/wintel/2');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '角色不属于该企业'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        $client->request('GET', '/role/delete/wintel/5');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '角色下有坐席'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $client->request('GET', '/role/delete/wintel/1');
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试获取列表
     */
    public function testRoleListAction()
    {
        $client = static::createClient();

        $client->request('GET', '/role/list/wintelcc');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $client->request('GET', '/role/list/wintel');
        $response = $client->getResponse()->getContent();
       // echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试分配权限
     */
    public function testRolePermissionsAction()
    {
        $client = static::createClient();

        //企业代码空 401
        $param = array(
            'vcc_code' => '',
            'action_list' => 'gztkhgl,gztwjld,gztwdtx,',
            'role_id' => '5',
        );
        $client->request('POST', '/role/permissions', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在 402
        $param = array(
            'vcc_code' => 'cccc',
            'action_list' => 'gztkhgl,gztwjld,gztwdtx,',
            'role_id' => '5',
        );
        $client->request('POST', '/role/permissions', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403角色不属于该企业
        $param = array(
            'vcc_code' => 'wintel',
            'action_list' => 'gztkhgl,gztwjld,gztwdtx,',
            'role_id' => '20',
        );
        $client->request('POST', '/role/permissions', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '角色不属于该企业'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405 角色已经存在
        $param = array(
            'vcc_code' => 'wintel',
            'action_list' => '',
            'role_id' => '5',
        );
        $client->request('POST', '/role/permissions', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '权限为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_code' => 'wintel',
            'action_list' => 'gztkhgl,gztwjld,gztwdtx,agents',
            'role_id' => '5',
        );
        $client->request('POST', '/role/permissions', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
