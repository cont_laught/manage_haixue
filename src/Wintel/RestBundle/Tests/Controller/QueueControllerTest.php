<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AgentControllerTest
 * @package Wintel\RestBundle\Tests\Controller
 */
class QueueControllerTest extends WebTestCase
{
    /**
     * 测试添加技能组
     */
    public function testAddAction ()
    {
        $client = static::createClient();
        //企业代码为空
        $param = array(
            'vcc_code'=>'',
            'que_name'=>'hello,worldc',
        );
        $client->request('POST', '/queue/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在
        $param = array(
            'vcc_code'=>'ccccxxxx',
            'que_name'=>'hello,world',
        );
        $client->request('POST', '/queue/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组名称为空
        $param = array(
            'vcc_code'=>'wintel',
            'que_name'=>'',
        );
        $client->request('POST', '/queue/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组名称为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组名称重复
        $param = array(
            'vcc_code'=>'wintel',
            'que_name'=>'hello,worldxx',
        );
        $client->request('POST', '/queue/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '技能组已经存在',
        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code'=>'wintel',
            'que_name'=>'我是谁cc',
            'que_tag' => '非常棒vvvvvv',
            'que_type' => '0',
            'que_length' => '21',
            'que_time' => '52',
            'ring_time' => '33',
            'next_wait' => '10',
            'b_announce' => '1',
            'noans_times' => '3',
            'noans_wait' => '24',
            'wait_audio' => '2',
            'que_strategy' => 2
        );
        $client->request('POST', '/queue/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok',
        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试修改技能组
     *
     */
    public function testUpdateAction()
    {
        $client = static::createClient();
        //企业代码为空
        $param = array(
            'vcc_code'=>'',
            'que_name'=>'hello,world',
            'que_id'=> 1,
        );
        $client->request('POST', '/queue/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在
        $param = array(
            'vcc_code'=>'ccccxxxx',
            'que_name'=>'hello,world',
            'que_id'=> 1,
        );
        $client->request('POST', '/queue/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组名称为空
        $param = array(
            'vcc_code'=>'wintel',
            'que_name'=>'',
            'que_id'=> 1,
        );
        $client->request('POST', '/queue/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组名称为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404 技能组不属于该企业
        $param = array(
            'vcc_code'=>'wintel',
            'que_name'=>'cccc',
            'que_id'=> 1,
        );
        $client->request('POST', '/queue/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '技能组不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组名称重复
        $param = array(
            'vcc_code'=>'wintel',
            'que_name'=>'hello,world',
            'que_id'=> 2,
        );
        $client->request('POST', '/queue/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '技能组已经存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //200 ok
        $param = array(
            'vcc_code'=>'wintel',
            'que_name'=>'hello',
            'que_tag' => '非常棒cc',
            'que_type' => '1',
            'que_length' => '121',
            'que_time' => '152',
            'ring_time' => '133',
            'next_wait' => '110',
            'b_announce' => '11',
            'noans_times' => '13',
            'noans_wait' => '124',
            'wait_audio' => '12',
            'que_id'=> 36,
            'que_strategy'=> 1,
        );
        $client->request('POST', '/queue/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok',
        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试删除技能组
     */

    public function testDeleteAction()
    {
        $client = static::createClient();

        //企业代码不存在
        $client->request('GET', '/queue/delete/xxxx/1');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
       // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组不属于该企业
        $client->request('GET', '/queue/delete/wintel/1');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组下是否有坐席
        $client->request('GET', '/queue/delete/wintel/2');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '技能组下面有坐席',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //200 ok
        $client->request('GET', '/queue/delete/wintel/5');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试获得坐席列表
     *
     */
    public function testListAction()
    {
        $client = static::createClient();

        //企业代码不存在
        $client->request('GET', '/queue/list/xxxx/1');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组不属于该企业
        $client->request('GET', '/queue/list/wintel/{"filter":{"que_id":"13"}}');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 Ok 不传技能组
        $client->request('GET', '/queue/list/wintel');
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');

        //200 ok
        $client->request('GET', '/queue/list/wintel/{"filter":{"que_id":"2"}}');
        $response = $client->getResponse()->getContent();
       // echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试技能组分配坐席
     */
    public function testAssignAction()
    {
        $client = static::createClient();
        //企业代码为空
        $param = array(
            'vcc_code'=>'',
            'que_id'=>'1',
            'agents'=> '[{"ag_id":1,"skill":1},{"ag_id":2,"skill":2}]',
        );
        $client->request('POST', '/queue/assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在
        $param = array(
            'vcc_code'=>'cccc',
            'que_id'=>'1',
            'agents'=> '[{"ag_id":1,"skill":1},{"ag_id":2,"skill":2}]',
        );
        $client->request('POST', '/queue/assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组ID为空
        $param = array(
            'vcc_code'=>'wintel',
            'que_id'=>'',
            'agents'=> '[{"ag_id":1,"skill":1},{"ag_id":2,"skill":2}]',
        );
        $client->request('POST', '/queue/assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '技能组ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组不属于该企业
        $param = array(
            'vcc_code'=>'wintel',
            'que_id'=>'1',
            'agents'=> '[{"ag_id":1,"skill":1},{"ag_id":2,"skill":2}]',
        );
        $client->request('POST', '/queue/assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409 agents 为空
        $param = array(
            'vcc_code'=>'wintel',
            'que_id'=>'11',
            'agents'=> '',
        );
        $client->request('POST', '/queue/assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => 'agents参数为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //405 不是 JSON 格式
        $param = array(
            'vcc_code'=>'wintel',
            'que_id'=>'11',
            'agents'=> 'cc',
        );
        $client->request('POST', '/queue/assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'agents非JSON格式',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //总错误 ag_id 不属于 该企业，ag_id 已经分配了
        $param = array(
            'vcc_code'=>'vbill',
            'que_id'=>'13',
            'agents'=> '[{"ag_id":1,"skill":1},{"ag_id":1995,"skill":2},{"ag_id":1994,"skill":12}]',
        );
        $client->request('POST', '/queue/assign', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    public function testCacheAssignAction()
    {
        $client = static::createClient();
        //企业代码为空
        $param = array(
            'vcc_code'=>'',
            'que_id'=>'1',
            'agents'=> '[1,3,2]',
        );
        $client->request('POST', '/queue/cache_assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在
        $param = array(
            'vcc_code'=>'cccc',
            'que_id'=>'1',
            'agents'=> '[1,2,3]',
        );
        $client->request('POST', '/queue/cache_assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组ID为空
        $param = array(
            'vcc_code'=>'wintel',
            'que_id'=>'',
            'agents'=> '[1,2,3]',
        );
        $client->request('POST', '/queue/cache_assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '技能组ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组不属于该企业
        $param = array(
            'vcc_code'=>'wintel',
            'que_id'=>'1',
            'agents'=> '[1,2,3]',
        );
        $client->request('POST', '/queue/cache_assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409 agents 为空
        $param = array(
            'vcc_code'=>'wintel',
            'que_id'=>'11',
            'agents'=> '',
        );
        $client->request('POST', '/queue/cache_assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'agents参数为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405 不是 JSON 格式
        $param = array(
            'vcc_code'=>'wintel',
            'que_id'=>'11',
            'agents'=> 'cc',
        );
        $client->request('POST', '/queue/cache_assign', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => 'agents非JSON格式',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok ;
        $param = array(
            'vcc_code'=>'wintel',
            'que_id'=>'4',
            'agents'=> '[2032,2029,"xx"]',
        );
        $client->request('POST', '/queue/cache_assign', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
