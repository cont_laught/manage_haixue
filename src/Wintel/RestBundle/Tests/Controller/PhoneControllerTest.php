<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PhoneControllerTest extends WebTestCase
{
    /**
     * 测试添加分机号
     */
    public function testAgentLoginAction ()
    {
        $client = static::createClient();

        //vcc_code为空
        $param = array(
            'vcc_code' => '',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在
        $param = array(
            'vcc_code' => 'ccxx',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //pho_type 值不是 125
        $param = array(
            'vcc_code' => 'wintel',
            'pho_type' => '3',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '电话类型值有误(1,2,5)',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //电话号码为空
        $param = array(
            'vcc_code' => 'wintel',
            'pho_type' => '1',
            'pho_num' => '',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '电话号码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //pho_num非json
        $param = array(
            'vcc_code' => 'wintel',
            'pho_type' => '1',
            'pho_num' => 'abcd',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'pho_num格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始不正确
        $param = array(
            'vcc_code' => 'wintel',
            'pho_type' => '5',
            'pho_start' => '',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '开始分机号格式有误',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $param = array(
            'vcc_code' => 'wintel',
            'pho_type' => '5',
            'pho_start' => '0251',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '开始分机号格式有误',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束不正确
        $param = array(
            'vcc_code' => 'wintel',
            'pho_type' => '5',
            'pho_start' => '8021',
            'pho_end' => '2031',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '结束分机号格式有误',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'pho_type' => '1',
            'pho_num' => '[18310132160,1025,4352,"xxx"]',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'pho_type' => '5',
            'pho_start' => '8021',
            'pho_end' => '8025',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'pho_type' => '5',
            'pho_start' => '8025',
            'pho_end' => '8027',
            'passtype' => 2,
            'pho_pass' => 'abcd123',
        );
        $client->request('POST', '/phone/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     *
     * 测试删除
     */
    public function testDeleteAction()
    {
        $client = static::createClient();

        $client->request('GET', '/phone/delete/xxc/[1,2]');
        $response = $client->getResponse()->getContent();
        $result = array('code'=>402, 'message'=>'企业代码不存在');
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $client->request('GET', '/phone/delete/wintel/[1,2,5,6]');
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    public function testListAction()
    {
        $client = static::createClient();

        //vcc_code为空
        $param = array(
            'vcc_code' => '',
        );
        $client->request('POST', '/phone/list', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
       // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在
        $param = array(
            'vcc_code' => 'ccxx',
        );
        $client->request('POST', '/phone/list', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 json
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'xxx,afda',
        );
        $client->request('POST', '/phone/list', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":5,"page":"5"},"filter":{"phone":"80"}}',
        );
        $client->request('POST', '/phone/list', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
