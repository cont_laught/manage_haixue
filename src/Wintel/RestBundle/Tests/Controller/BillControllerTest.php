<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class BilltControllerTest
 * @package Wintel\RestBundle\Tests\Controller
 *
 */
class BilltControllerTest extends WebTestCase
{
    /**
     * 测试 咨询三方
     */
    public function testConferenceAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/bill/conference', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/bill/conference', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/bill/conference', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/bill/conference', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
       // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/bill/conference', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"max_conn_secs":"2","endresult":"0","que_id":"123"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/bill/conference', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 监听强插通话详单
     */
    public function testMonitorAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/bill/monitor', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/bill/monitor', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/bill/monitor', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/bill/monitor', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/bill/monitor', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"ag_ernum":9},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/bill/monitor', $param);
        $response = $client->getResponse()->getContent();
       // echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
