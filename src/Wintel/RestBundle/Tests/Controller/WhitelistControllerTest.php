<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WhitelistControllerTest extends WebTestCase
{
    /**
     * 测试添加白名单
     */
    public function testWhitelistAddAction()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'phones' => '8001,8002,8003',
        );
        $client->request('POST', '/whitelist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码为空
        $param = array(
            'vcc_id' => 1,
            'phones' => '',
        );
        $client->request('POST', '/whitelist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '号码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码中包含非数字字符
        $param = array(
            'vcc_id' => 1,
            'trunk_num'=>'110',
            'phones' => '8001,800d,8003',
        );
        $client->request('POST', '/whitelist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '号码中包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID为非数字
        $param = array(
            'vcc_id' => 'a',
            'phones' => '8001,800d,8003',
        );
        $client->request('POST', '/whitelist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '企业ID为非数字'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码格式不对，要求为多个号码逗号分隔的字符串，但是得到
        $param = array(
            'vcc_id' => 1,
            'phones' => array(8001,8002,8003),
        );
        $client->request('POST', '/whitelist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '号码格式不对，要求为多个号码逗号分隔的字符串，但是得到array'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_id' => 1,
            'trunk_num'=>'110',
            'phones' => '8001,8002,8003',
        );
        $client->request('POST', '/whitelist/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试删除白名单
     */
    public function testWhitelistDeleteAction()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'phone_ids' => '1,2,3',
        );
        $client->request('POST', '/whitelist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码为空
        $param = array(
            'vcc_id' => 1,
            'phone_ids' => '',
        );
        $client->request('POST', '/whitelist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '号码ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码中包含非数字字符
        $param = array(
            'vcc_id' => 1,
            'phone_ids' => '8001,800d,8003',
        );
        $client->request('POST', '/whitelist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '号码ID中包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => 'a',
            'phone_ids' => '8001,8002,8003',
        );
        $client->request('POST', '/whitelist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '企业ID包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码ID格式不对，要求为多个号码ID逗号分隔的字符串，但是得到
        $param = array(
            'vcc_id' => 1,
            'phone_ids' => array('8001,8002,8003'),
        );
        $client->request('POST', '/whitelist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '号码ID格式不对，要求为多个号码ID逗号分隔的字符串，但是得到array'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_id' => 1,
            'phone_ids' => '1,2,3',
        );
        $client->request('POST', '/whitelist/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试查询黑名单
     */
    public function testWhitelistListAction()
    {
        $client = static::createClient();

        //企业ID为空
        $vcc_id = '1a';
        $client->request('GET', '/whitelist/list/'.$vcc_id);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $vcc_id = 1;
        $client->request('GET', '/whitelist/list/'.$vcc_id);
        $response = $client->getResponse()->getContent();
	    $this->assertEquals("200",isset(json_decode($response)->code)?json_decode($response)->code:'');
    }
}
