<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ReportControllerTest
 * @package Wintel\RestBundle\Tests\Controller
 */
class ReportControllerTest extends WebTestCase
{
    /**
     * 测试 半小时 坐席工作表现报表
     */
    public function testReportAgentHalfhourAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/report/agent/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/report/agent/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/agent/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"ag_num":"6019","end_date":"2013-12-24"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/halfhour', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 小时 坐席工作表现报表
     */
    public function testReportAgentHourAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/report/agent/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
       // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/report/agent/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/agent/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"ag_num":"6061","end_date":"2014-01-09"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/hour', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 天 坐席工作表现报表
     */
    public function testReportAgentDayAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/report/agent/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/report/agent/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/agent/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"end_date":"2013-12-12"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/day', $param);
        $response = $client->getResponse()->getContent();
       // echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 月 坐席工作表现报表
     */
    public function testReportAgentMonthAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/report/agent/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/report/agent/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/agent/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始日期不正确'
        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束日期不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"ag_num":"8001","end_date":"2013-11"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/agent/month', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 半小时 技能组话务报表
     */
    public function testReportQueueHalfhourAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/report/queue/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/report/queue/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/queue/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":"1","page":"5"},"filter":{"queue_id":"256","end_date":"2013-11-28"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/halfhour', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 小时 技能组话务报表
     */
    public function testReportQueueHourAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/report/queue/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/report/queue/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/queue/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code'=>'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code'=>'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":"1","page":"5"},"filter":{"queue_id":"257","end_date":"2013-11-27"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/hour', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 天 技能组话务报表
     */
    public function testReportQueueDayAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/report/queue/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/report/queue/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/queue/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //开始日期不正确
        $param = array(
            'vcc_code'=>'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code'=>'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":"1","page":"5"},"filter":{"queue_id":"257","end_date":"2013-11-28"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/day', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 月 技能组话务报表
     */
    public function testReportQueueMonthAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/report/queue/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/report/queue/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/queue/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code'=>'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始日期不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code'=>'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束日期不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"queue_id":"257","end_date":"2014-02"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/queue/month', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 天 呼叫中心整体话务报表
     */
    public function testReportSystemDayAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/report/system/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/report/system/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/system/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/system/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/system/day', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"end_date":"2013-11-27"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/system/day', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 小时 呼叫中心整体话务报表
     */
    public function testReportSystemHourAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code' => '',
        );
        $client->request('POST', '/report/system/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code' => 'wraqeafdsa',
        );
        $client->request('POST', '/report/system/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/system/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/system/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}'
        );
        $client->request('POST', '/report/system/hour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"2014-07-07"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/system/hour', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 半小时 呼叫中心整体话务报表
     */
    public function testReportSystemHalfHour()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code' => '',
        );
        $client->request('POST', '/report/system/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code' => 'wraqeafdsa',
        );
        $client->request('POST', '/report/system/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/system/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/system/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/system/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"end_date":"2014-05-06"},"sort":{"field":"id","order":"desc"}}'
        );
        $client->request('POST', '/report/system/halfhour', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 月 呼叫中心整体话务报表
     */
    public function testReportSystemMonth()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code' => '',
        );
        $client->request('POST', '/report/system/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code' => 'wraqeafdsa',
        );
        $client->request('POST', '/report/system/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/system/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/system/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始日期不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_date":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/system/month', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束日期不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"end_date":"2014-07"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/system/month', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 坐席通话明细报表
     */
    public function testReportCallDetailAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code' => '',
        );
        $client->request('POST', '/report/calldetail', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code' => 'wraqeafdsa',
        );
        $client->request('POST', '/report/calldetail', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/calldetail', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/calldetail', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/calldetail', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //406 技能组
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"que_id":"3362"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/calldetail', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '技能组不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"end_time":"2014-01-29 00:00:00","end_reason":"11"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/calldetail', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 漏话明细报表
     */
    public function testGetLostAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code' => '',
        );
        $client->request('POST', '/report/lost', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code' => 'wraqeafdsa',
        );
        $client->request('POST', '/report/lost', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/lost', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/lost', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/lost', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407 if_work字段只能为0或1
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"if_work":"xx"},"sort":{"field":"id","order":"desc"}}',
        );
//        $client->request('POST', '/report/lost', $param);
//        $response = $client->getResponse()->getContent();
//        $result = array(
//            'code' => 407,
//            'message' => 'if_work字段只能为0或1',
//        );
//        //echo $response;
//        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
//
//        //408
//        $param = array(
//            'vcc_code' => 'wintel',
//            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"if_trans":"xx"},"sort":{"field":"id","order":"desc"}}',
//        );
//        $client->request('POST', '/report/lost', $param);
//        $response = $client->getResponse()->getContent();
//        $result = array(
//            'code' => 408,
//            'message' => 'if_trans值只能为0或1',
//        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409 server_num非数字；
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"server_num":"xx"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/lost', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => 'server_num 不是数字',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":2,"page":"1"},"filter":{"end_time":"2014-12-20 00:00:00","caller":"013","server_num":"58452444"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/lost', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试
     * 技能组来电分配报表
     */
    public function testInallotAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code' => '',
        );
        $client->request('POST', '/report/inallot', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code' => 'wraqeafdsa',
        );
        $client->request('POST', '/report/inallot', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/inallot', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/inallot', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/inallot', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":2,"page":"1"},"filter":{"end_time":"2014-06-11 00:00:00"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/inallot', $param);
        $response = $client->getResponse()->getContent();
       // echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试
     * 满意度评价报表
     */
    public function testEvaluateAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code' => '',
        );
        $client->request('POST', '/report/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
       // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code' => 'wraqeafdsa',
        );
        $client->request('POST', '/report/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":2,"page":"1"},"filter":{"evaluate":"2"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/evaluate', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试
     * 技能组呼叫转移 明细接口
     */
    public function testTransAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code' => '',
        );
        $client->request('POST', '/report/trans', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code' => 'wraqeafdsa',
        );
        $client->request('POST', '/report/trans', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/report/trans', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/trans', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //end-date不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/trans', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"1"},"filter":{"ag_num":"1"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/report/trans', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
