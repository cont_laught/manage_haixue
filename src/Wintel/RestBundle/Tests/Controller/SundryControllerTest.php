<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class CommonControllerTest
 * @package Wintel\RestBundle\Tests\Controller
 */
class SundryControllerTest extends WebTestCase
{
    public function testGetnumberloc()
    {
        $client = static::createClient();
        $client->request('GET', 'wintelapi/api/common/getnumberloc/13545678901');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '请输入完整的号码，固定电话请带上区号',
        );
        echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $client = static::createClient();
        $client->request('GET', 'wintelapi/api/common/getnumberloc/12345678901');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '找不到号码段',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $client = static::createClient();
        $client->request('GET', 'wintelapi/api/common/getnumberloc/18310132160');
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
