<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AgentControllerTest
 * @package Wintel\RestBundle\Tests\Controller
 */
class SoundControllerTest extends WebTestCase
{
    /**
     * 测试获取语音列表
     */
    public function testListAction()
    {
        $client = static::createClient();
        //企业代码不存在
        $client->request('GET', '/sound/list/xxcccx');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $client->request('GET', '/sound/list/wintel');
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');

        //200 ok page
        $client->request('GET', '/sound/list/wintel/{"pagination":{"rows":20,"page":"5"},"sort":{"field":"id","order":"desc"}}');
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试删除语音；
     */
    public function testDeleteAction()
    {
        $client = static::createClient();
        //企业代码不存在
        $client->request('GET', '/sound/delete/xxcccx/11');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //sound_id 不为数字
        $client->request('GET', '/sound/delete/wintel/11cc');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'sound_id不是数字',
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //记录不存在
        $client->request('GET', '/sound/delete/wintel/100000');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => 'sound_id:100000 记录不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $client->request('GET', '/sound/delete/wintel/11');
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
