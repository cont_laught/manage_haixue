<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MonitorControllerTest extends WebTestCase
{
    /**
     * 测试获得坐席监控数据
     */
    public function testMonitorAgentAction ()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'user_ids' => '8001,8002,8003',
        );
        $client->request('POST', '/monitor/agent', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID非数字
        $param = array(
            'vcc_id' => '1x',
            'user_ids' => '8001,8002,8003',
        );
        $client->request('POST', '/monitor/agent', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //坐席ID中含有非数字项
        $param = array(
            'vcc_id' => '1',
            'user_ids' => '8001,8002,8003x',
        );
        $client->request('POST', '/monitor/agent', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID中含有非数字项',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //技能组ID包含非数字字符
        $param = array(
            'vcc_id' => '1',
            'user_ids' => '8001,8002,8003',
            'que_id' => '52x',
        );
        $client->request('POST', '/monitor/agent', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '技能组ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_id' => '1',
        );
        $client->request('POST', '/monitor/agent', $param);
        $response = $client->getResponse()->getContent();
        //echo 'xxx--------'.$response.'zzzzz';
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试获取技能组监控数据
     */
    public function testMonitorQueueAction ()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'que_ids' => '8001,8002,8003',
        );
        $client->request('POST', '/monitor/queue', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID非数字
        $param = array(
            'vcc_id' => '1x',
            'que_ids' => '8001,8002,8003',
        );
        $client->request('POST', '/monitor/queue', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组ID中含有非数字项
        $param = array(
            'vcc_id' => '1',
            'que_ids' => '8001,8002,8003x',
        );
        $client->request('POST', '/monitor/queue', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组ID中含有非数字项',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 Ok
        $param = array(
            'vcc_id' => '1',
        );
        $client->request('POST', '/monitor/queue', $param);
        $response = $client->getResponse()->getContent();
        //echo '------'.$response.'ccccxx';
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试获取排队监控数据
     */
    public function testMonitorCallsAction()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'que_id' => '1',
        );
        $client->request('POST', '/monitor/calls', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID非数字
        $param = array(
            'vcc_id' => '1x',
            'que_id' => '1',
        );
        $client->request('POST', '/monitor/calls', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组ID中含有非数字项
        $param = array(
            'vcc_id' => '1',
            'que_id' => '1x',
        );
        $client->request('POST', '/monitor/calls', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 Ok
        $param = array(
            'vcc_id' => '1',
            'que_id' => '1',
        );
        $client->request('POST', '/monitor/calls', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试获取坐席统计数据接口
     */
    public function testMonitorStadataAction()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'user_ids' => '1',
            'start_date'=>'2013-05-20',
            'start_date'=>'2013-06-20',
        );
        $client->request('POST', '/monitor/stadata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID非数字
        $param = array(
            'vcc_id' => '1x',
            'user_ids' => '1',
            'start_date'=>'2013-05-20',
            'start_date'=>'2013-06-20',
        );
        $client->request('POST', '/monitor/stadata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //用户ID为空
        $param = array(
            'vcc_id' => '1',
            'user_ids' => '0',
            'start_date'=>'2013-05-20',
            'start_date'=>'2013-06-20',
        );
        $client->request('POST', '/monitor/stadata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '用户ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //用户ID中含有非数字项
        $param = array(
            'vcc_id' => '1',
            'user_ids' => '1,2x',
            'start_date'=>'2013-05-20',
            'start_date'=>'2013-06-20',
        );
        $client->request('POST', '/monitor/stadata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '用户ID中含有非数字项',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始日期格式不正确
        $param = array(
            'vcc_id' => '1',
            'user_ids' => '1,2',
            'start_date'=>'2013-c5-20x',
            'end_date'=>'2013-06-20',
        );
        $client->request('POST', '/monitor/stadata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '开始日期格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束日期格式不正确
        $param = array(
            'vcc_id' => '1',
            'user_ids' => '1,2',
            'start_date'=>'2013-05-20',
            'end_date'=>'2013-06c-20',
        );
        $client->request('POST', '/monitor/stadata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '结束日期格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 Ok
        $param = array(
            'vcc_id' => '10',
            'user_ids' => '14,15,16',
            'start_date'=>'2011-08-30',
            'end_date'=>'2011-08-31',
        );
        $client->request('POST', '/monitor/stadata', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 获取坐席操作明细数据接口
     */
    public function testMonitorDetaildataAction()
    {
        $client = static::createClient();

        //企业代码为空
        $param = array(
            'vcc_code' => 0,
        );
        $client->request('POST', '/monitor/detaildata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID不存在
        $param = array(
            'vcc_code' => 'xafdsafdsa',
        );
        $client->request('POST', '/monitor/detaildata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/monitor/detaildata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/monitor/detaildata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/monitor/detaildata', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"ag_name":"8","end_time":"2013-11-28 00:00:00"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/monitor/detaildata', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }


    /**
     *   测试获取系统监控数据
     */
    public function testMonitorSystemAction()
    {
        $client = static::createClient();
        //企业代码不存在
        $client->request('GET', '/monitor/system/xxxx');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $client->request('GET', '/monitor/system/wintel');
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');

    }

    /**
     *  测试 获取坐席 ivr 统计；
     */
    public function testGetIvrAction()
    {
        $client = static::createClient();
        //企业代码不存在
        $client->request('GET', '/monitor/ivr/xxxx');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $client->request('GET', '/monitor/ivr/wintel');
        $response = $client->getResponse()->getContent();
       // echo "xxxxxxxxxxxxxxxxx".$response.'YYYYYYYYY';
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *  测试 获取坐席 示忙理由；
     */
    public function testGetReasonAction()
    {
        $client = static::createClient();
        //企业代码不存在
        $client->request('GET', '/monitor/getreason/xxxx');
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $client->request('GET', '/monitor/getreason/wintel');
        $response = $client->getResponse()->getContent();
        //echo "xxxxxxxxxxxxxxxxx".$response.'YYYYYYYYY';
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
