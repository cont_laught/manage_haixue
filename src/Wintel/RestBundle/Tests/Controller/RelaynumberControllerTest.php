<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RelaynumberControllerTest extends WebTestCase
{

    /**
     * 测试查询中继号
     */
    public function testGetphoneAction()
    {
        $client = static::createClient();
        $vcc_id = 0;
        $client->request('GET', '/relaynumber/getphone/'.$vcc_id);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID为空
        $vcc_id = '10a';
        $client->request('GET', '/relaynumber/getphone/'.$vcc_id);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //ok
        $vcc_id = 10;
        $client->request('GET', '/relaynumber/getphone/'.$vcc_id);
        $response = $client->getResponse()->getContent();
        //var_dump($response);
        $this->assertEquals("200", isset(json_decode($response)->code)?json_decode($response)->code:'');
    }
}
