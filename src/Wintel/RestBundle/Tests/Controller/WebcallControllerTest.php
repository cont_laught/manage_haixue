<?php
namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WebcallControllerTest extends WebTestCase
{
    /**
     * 测试400
     */
    public function testWebcall400Action ()
    {
        $client = static::createClient();

        $param = array(
            'vcc_id' => 0,
            'start_time' => '1318061668',
        );
        $client->request('POST', '/webcall/webcall400', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        // var_dump($response);
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID为空
        $param = array(
            'vcc_id' => '1a',
            'start_time' => '1318061668',
        );
        $client->request('POST', '/webcall/webcall400', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //时间为空
        $param = array(
            'vcc_id' => '1',
            'start_time' => '',
        );
        $client->request('POST', '/webcall/webcall400', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '开始时间为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //ok
        $param = array(
            'vcc_id' => '13',
            'start_time' => '1318061668',
            'end_time' => '1318065301',
        );
        $client->request('POST', '/webcall/webcall400', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }


    /**
     * 测试400
     */
    public function testWebcall800Action ()
    {
        $client = static::createClient();

        $param = array(
            'vcc_id' => 0,
            'start_time' => '1318061668',
            'end_time' => '1318065301',
        );
        $client->request('POST', '/webcall/webcall800', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        // var_dump($response);
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID为空
        $param = array(
            'vcc_id' => '1a',
            'start_time' => '1318061668',
        );
        $client->request('POST', '/webcall/webcall800', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //时间为空
        $param = array(
            'vcc_id' => '1',
            'start_time' => '',
            'end_time' => '1318061668',
        );
        $client->request('POST', '/webcall/webcall800', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '开始时间为空'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_id' => '36',
            'start_time' => '1318061668',
        );
        $client->request('POST', '/webcall/webcall800', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
