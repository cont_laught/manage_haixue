<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CallstatusControllerTest extends WebTestCase
{
    public function testCallstatusAction()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'ag_id' => '1',
            'call_status' => 1
        );
        $client->request('POST', '/callstatus', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //坐席ID为空
        $param = array(
            'vcc_id' => 1,
            'ag_id' => 0,
            'call_status' => 1
        );
        $client->request('POST', '/callstatus', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '坐席ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //呼叫状态的值不在允许的范围[0,1,2,3]内
        $param = array(
            'vcc_id' => 1,
            'ag_id' => 751,
            'call_status' => 6
        );
        $client->request('POST', '/callstatus', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '呼叫状态的值不在允许的范围[0,1,2,3]内'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_id' => 1,
            'ag_id' => 751,
            'call_status' => 0
        );
        $client->request('POST', '/callstatus', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     *
     * 呼入接口测试
     */
    public function testCalloutAction()
    {
        $client = static::createClient();

        //企业代码为空
        $param = array(
            'vcc_code' => 0,
        );
        $client->request('POST', '/callout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID不存在
        $param = array(
            'vcc_code' => 'xafdsafdsa',
        );
        $client->request('POST', '/callout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/callout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/callout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/callout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"result":"1","end_time":"2014-02-28 00:00:00"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/callout', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *
     * 呼出接口测试
     */

    public function testCallinAction()
    {
        $client = static::createClient();

        //企业代码为空
        $param = array(
            'vcc_code' => 0,
        );
        $client->request('POST', '/callin', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID不存在
        $param = array(
            'vcc_code' => 'xafdsafdsa',
        );
        $client->request('POST', '/callin', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/callin', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/callin', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/callin', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"caller":"59","start_time":"2014-01-28 00:00:00"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/callin', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *
     * 测试转接查询接口
     *
     */
    public function testTranscallAction()
    {
        $client = static::createClient();

        //企业代码为空
        $param = array(
            'vcc_code' => 0,
            'call_id' => '610716',
        );
        $client->request('POST', '/callin', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID不存在
        $param = array(
            'vcc_code' => 'xafdsafdsa',
            'call_id' => '610716',
        );
        $client->request('POST', '/transcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //call_id 不存在；
        $param = array(
            'vcc_code' => 'ekttest',
            'call_id' => 0,
        );
        $client->request('POST', '/transcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'call_id为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //call_id 非数字；
        $param = array(
            'vcc_code' => 'ekttest',
            'call_id' => 'ccadfaf',
        );
        $client->request('POST', '/transcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => 'call_id包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok ；
        $param = array(
            'vcc_code' => 'wintel',
            'call_id' => '610716',
        );
        $client->request('POST', '/transcall', $param);
        $response = $client->getResponse()->getContent();
       // echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *
     * 测试获取自动外呼
     *
     */
    public function testAccdrAction()
    {
        $client = static::createClient();

        //企业代码为空
        $param = array(
            'vcc_code' => 0,
        );
        $client->request('POST', '/accdr', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID不存在
        $param = array(
            'vcc_code' => 'xafdsafdsa',
        );
        $client->request('POST', '/accdr', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //开始时间不正确
        $param = array(
            'vcc_code' => 'zyzxdl',
            'start_time' => '2013-05-27',
        );
        $client->request('POST', '/accdr', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '开始时间不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //借宿时间不正确
        $param = array(
            'vcc_code' => 'zyzxdl',
            'start_time' => '2013-05-27 10:28:29',
            'end_time' => '2013-06-27',
        );
        $client->request('POST', '/accdr', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '结束时间不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //200 ok ；
        $param = array(
            'vcc_code' => 'zyzxdl',
            'start_time' => '2014-05-27 10:28:29',
            'end_time' => '2014-05-28 23:28:29',
        );
        $client->request('POST', '/accdr', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
