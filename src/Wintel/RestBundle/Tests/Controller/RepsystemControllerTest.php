<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RepsystemControllerTest extends WebTestCase
{

    /**
     * 测试查询半小时
     */
    public function testHalfhourAction()
    {
        $client = static::createClient();

        $param = array(
            'vcc_id' => 0,
        );
        $client->request('POST', '/repsystem/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
       // var_dump($response);
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID为空
        $param = array(
            'vcc_id' => '1a',
            'date' => '',
        );
        $client->request('POST', '/repsystem/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //日期格式不正确
        $vcc_id = 1;
        $param = array(
            'vcc_id'=>'1',
            'date'=>'2012-cc-xx',
        );
        $client->request('POST', '/repsystem/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '日期格式不正确'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //ok
        $param = array(
            'vcc_id'=>'10',
            'date'=>'2011-08-29',
        );
        $client->request('POST', '/repsystem/halfhour', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
