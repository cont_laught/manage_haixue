<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RecordControllerTest extends WebTestCase
{
    /**
     *
     * 录音列表测试
     */
    public function testRecordlistAction()
    {
        $client = static::createClient();

        //企业代码为空
        $param = array(
            'vcc_code' => 0,
        );
        $client->request('POST', '/record/recordlist', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID不存在
        $param = array(
            'vcc_code' => 'xafdsafdsa',
        );
        $client->request('POST', '/record/recordlist', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 非json
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/record/recordlist', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //开始时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/record/recordlist', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //结束时间格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"end_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/record/recordlist', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"5"},"filter":{"ssecs":"60","start_time":"2014-01-28 00:00:00"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/record/recordlist', $param);
        $response = $client->getResponse()->getContent();
        echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }
}
