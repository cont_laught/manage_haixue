<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class BilltControllerTest
 * @package Wintel\RestBundle\Tests\Controller
 *
 */
class GrapicControllerTest extends WebTestCase
{
    /**
     * 测试 咨询三方
     */
    public function testConferenceAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数sta_type
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => 'filter中缺少参数sta_type'
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数ag_id
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'filter中缺少参数ag_id'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //410 ag_id格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","ag_id":"xxx"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 410,
            'message' => 'ag_id格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //filter中缺少参数time_stamp
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => 'filter中缺少参数time_stamp'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407 日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","date":"aa","time_stamp":1,"ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"aa","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '开始日期格式不正确'
        );
       // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"2014-05-26","end_date":"ccc","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '结束日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //411 time_stamp值不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":5,"start_date":"aa","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 411,
            'message' => 'time_stamp值不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //412
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"}}',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 412,
            'message' => 'json中缺少filter'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"1"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"2011-06-22","end_date":"2014-07-28","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentcall', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 坐席工作量图表接口；
     */
    public function testAgentWorkAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数sta_type
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => 'filter中缺少参数sta_type'
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数ag_id
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'filter中缺少参数ag_id'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //410 ag_id格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","ag_id":"xxx"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 410,
            'message' => 'ag_id格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //filter中缺少参数time_stamp
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => 'filter中缺少参数time_stamp'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407 日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","date":"aa","time_stamp":1,"ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"aa","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '开始日期格式不正确'
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"2014-05-26","end_date":"ccc","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '结束日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //411 time_stamp值不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":5,"start_date":"aa","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 411,
            'message' => 'time_stamp值不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //412
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"}}',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 412,
            'message' => 'json中缺少filter'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":1,"page":"1"},"filter":{"sta_type":"2","time_stamp":3,"start_date":"2011-06-22","end_date":"2014-07-28","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/agentwork', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }


    /**
     * 测试 满意度评价
     */
    public function testEvaluateAction ()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();

        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();

        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数sta_type
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();

        $result = array(
            'code' => 404,
            'message' => 'filter中缺少参数sta_type'
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数ag_id
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();

        $result = array(
            'code' => 405,
            'message' => 'filter中缺少参数ag_id'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //410 ag_id格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","ag_id":"xxx"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 410,
            'message' => 'ag_id格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //filter中缺少参数time_stamp
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => 'filter中缺少参数time_stamp'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407 日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","date":"aa","time_stamp":1,"ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();

        $result = array(
            'code' => 407,
            'message' => '日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"aa","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '开始日期格式不正确'
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"2014-05-26","end_date":"ccc","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '结束日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //411 time_stamp值不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":5,"start_date":"aa","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 411,
            'message' => 'time_stamp值不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //412
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"}}',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 412,
            'message' => 'json中缺少filter'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"filter":{"sta_type":"1","time_stamp":3,"start_date":"2011-06-21","end_date":"2014-07-28","ag_id":[0,1,2]}}',
        );
        $client->request('POST', '/grapic/evaluate', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *测试 技能组通话量
     */
    public function testQueueCallAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数sta_type
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => 'filter中缺少参数sta_type'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数queue_id
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'filter中缺少参数queue_id'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //410 queue_id格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","queue_id":"xxx"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 410,
            'message' => 'queue_id格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数time_stamp
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","queue_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => 'filter中缺少参数time_stamp'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407 日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","date":"aa","time_stamp":1,"queue_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"aa","queue_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '开始日期格式不正确'
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"2014-05-26","end_date":"ccc","queue_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '结束日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //411 time_stamp值不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":5,"start_date":"aa","queue_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 411,
            'message' => 'time_stamp值不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //412 json中缺少filter
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"}}',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 412,
            'message' => 'json中缺少filter'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"filter":{"sta_type":"1","time_stamp":3,"start_date":"2011-06-22","end_date":"2014-07-28","queue_id":[0,1,2]}}',
        );
        $client->request('POST', '/grapic/queuecall', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 手机固话分析图表接口
     */
    public function testCallerTypeAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/grapic/callertype', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
//         echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/grapic/callertype', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/grapic/callertype', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数time_stamp
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/callertype', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => 'filter中缺少参数time_stamp'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405 日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","date":"aa","time_stamp":1,"ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/callertype', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //406 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"aa","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/callertype', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '开始日期格式不正确'
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"2014-05-26","end_date":"ccc","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/callertype', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '结束日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 time_stamp值不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":5,"start_date":"aa","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/callertype', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => 'time_stamp值不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"}}',
        );
        $client->request('POST', '/grapic/callertype', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => 'json中缺少filter'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":"2","page":"1"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"2011-06-22","end_date":"2014-07-28","ag_id":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/callertype', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 按小时进线分析图表接口
     */
    public function testIncomingHourAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
//         echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数sta_type
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => 'filter中缺少参数sta_type'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数data
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'filter中缺少参数data'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //410 data格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","data":"xxx"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 410,
            'message' => 'data格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数time_stamp
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => 'filter中缺少参数time_stamp'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407 日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","date":"aa","time_stamp":1,"data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"aa","data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '开始日期格式不正确'
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409 结束日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"2014-05-26","end_date":"ccc","data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '结束日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //411 time_stamp值不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":5,"start_date":"aa","data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 411,
            'message' => 'time_stamp值不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //412
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"}}',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 412,
            'message' => 'json中缺少filter'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"1"},"filter":{"sta_type":"2","time_stamp":3,"start_date":"2011-06-22","end_date":"2014-07-28","data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incominghour', $param);
        $response = $client->getResponse()->getContent();
//        echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 按地区分析图表接口
     */
    public function testIncomingAreaAction()
    {
        $client = static::createClient();
        //401代码为空
        $param = array(
            'vcc_code'=>'',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
//         echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业代码不存在；
        $param = array(
            'vcc_code'=>'wraqeafdsa',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 info 格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'cc'
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数sta_type
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"start_time":"1025"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => 'filter中缺少参数sta_type'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数data
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'filter中缺少参数data'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //410 data格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","data":"xxx"},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 410,
            'message' => 'data格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //filter中缺少参数time_stamp
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => 'filter中缺少参数time_stamp'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407 日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","date":"aa","time_stamp":1,"data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 开始日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"aa","data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '开始日期格式不正确'
        );
        // echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409 结束日期格式不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":3,"start_date":"2014-05-26","end_date":"ccc","data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '结束日期格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //411 time_stamp值不正确
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"},"filter":{"sta_type":"1","time_stamp":5,"start_date":"aa","data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 411,
            'message' => 'time_stamp值不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //412
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":20,"page":"5"}}',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 412,
            'message' => 'json中缺少filter'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'vbill',
            'info' => '{"pagination":{"rows":1,"page":"1"},"filter":{"sta_type":"2","time_stamp":3,"start_date":"2011-06-22","end_date":"2014-07-28","data":[0,1,2]},"sort":{"field":"id","order":"desc"}}',
        );
        $client->request('POST', '/grapic/incomingarea', $param);
        $response = $client->getResponse()->getContent();
//        echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }}
