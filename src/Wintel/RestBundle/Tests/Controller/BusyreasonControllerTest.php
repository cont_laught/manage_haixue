<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BusyreasonControllerTest extends WebTestCase
{
    /**
     * 测试添加
     */
    public function testBusyreasonAddAction()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'stat' => 1,
            'reason' => 'fasdf'
        );
        $client->request('POST', '/busyreason/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID为非数字
        $param = array(
            'vcc_id' => 'aa',
            'stat' => 1,
            'reason' => 'fasdf'
        );
        $client->request('POST', '/busyreason/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID为非数字'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //坐席状态为空
        $param = array(
            'vcc_id' => 1,
            'stat' => 0,
            'reason' => 'fasdf'
        );
        $client->request('POST', '/busyreason/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席状态不能为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //置忙原因为空
        $param = array(
            'vcc_id' => 1,
            'stat' => 1,
            'reason' => ''
        );
        $client->request('POST', '/busyreason/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '置忙原因不能为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

	$param = array(
            'vcc_id' => 1,
            'stat' => 1,
            'reason' => '测试添加',
        );
        $client->request('POST', '/busyreason/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '置忙原因格式不对，要求格式为数组，但是得到string',
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //正确执行
        $param = array(
            'vcc_id' => 1,
            'stat' => 1,
            'reason' => array('测试添加'),
        );
        $client->request('POST', '/busyreason/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

    }

    /**
     * 测试删除
     */
    public function testBusyreasonDeleteAction()
    {
        $client = static::createClient();

        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'ids' => '1,2,3,4,5'
        );
        $client->request('POST', '/busyreason/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID为非数字
        $param = array(
            'vcc_id' => 'aa',
            'ids' => '1,2,3,4,5'
        );
        $client->request('POST', '/busyreason/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID为非数字'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //置忙ID为空
        $param = array(
            'vcc_id' => 1,
            'ids' => ''
        );
        $client->request('POST', '/busyreason/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '置忙ID不能为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //置忙ID格式不是
        $param = array(
            'vcc_id' => 1,
            'ids' => array(1,2,3),
        );
        $client->request('POST', '/busyreason/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '置忙ID格式不对，要求为多个ID逗号分隔的字符串，但是得到array'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //正确执行
        $param = array(
            'vcc_id' => 1,
            'ids' => '1,2,3,4,5',
        );
        $client->request('POST', '/busyreason/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

    }

    /**
     * 测试获取列表数据
     */
    public function testBusyreasonListAction()
    {
        $client = static::createClient();

        //企业ID为空
        $vcc_id = '0';
        $client->request('GET', '/busyreason/list/'.$vcc_id);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID为非数字
        $vcc_id = 'aa';
        $client->request('GET', '/busyreason/list/'.$vcc_id);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //正确执行
        $vcc_id = 1;
        $client->request('GET', '/blacklist/list/'.$vcc_id);
        $response = $client->getResponse()->getContent();
     
        $this->assertEquals("200",isset(json_decode($response)->code)?json_decode($response)->code:'');
    }
}