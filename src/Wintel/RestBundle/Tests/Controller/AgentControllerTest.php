<?php

namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AgentControllerTest
 * @package Wintel\RestBundle\Tests\Controller
 */
class AgentControllerTest extends WebTestCase
{
    /**
     * 测试签入
     */
    public function testAgentLoginAction ()
    {
        $client = static::createClient();
        //param为空
        $param = array();
        $client->request('POST', '/agent/login', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 415,
            'message' => '参数param不能为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //'参数param不是json格式，无法解析
        $param = array(
            'param'=>'xx',
        );
        $client->request('POST', '/agent/login', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '参数param不是json格式，无法解析',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID为空
        $param = array(
            'param' => '{"vcc_id":0, "data": [{"ag_num":"8001","phone":"13811112222"},{"ag_num":"8002","phone":"13811112223"}]}',
        );
        $client->request('POST', '/agent/login', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID和企业代码都为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID包含非数字字符
        $param = array(
            'param' => '{"vcc_id":"1x", "data": [{"ag_num":"8001","phone":"13811112222"},{"ag_num":"8002","phone":"13811112223"}]}',
        );
        $client->request('POST', '/agent/login', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //参数中详细数据data为空
        $param = array(
            'param' => '{"vcc_id":"1", "data": ""}',
        );
        $client->request('POST', '/agent/login', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '参数中详细数据data为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //参数中详细数据data格式不对，要求为数组
        $param = array(
            'param' => '{"vcc_id":"1", "data": "xxa"}',
        );
        $client->request('POST', '/agent/login', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '参数中详细数据data格式不对，要求为数组',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //500总错误；
        $param = array(
            'param' => '{"vcc_id":"1", "data": [{"phone":"13811112222"},{"ag_num":"8002","phone":"13811112223"}]}',
        );
        $client->request('POST', '/agent/login', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试签出接口
     *
     */
    public function testLogoutAction()
    {
        $client = static::createClient();
        //param为空
        $param = array();
        $client->request('POST', '/agent/logout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 415,
            'message' => '参数param不能为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //'参数param不是json格式，无法解析
        $param = array(
            'param'=>'xx',
        );
        $client->request('POST', '/agent/logout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '参数param不是json格式，无法解析',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID为空
        $param = array(
            'param' => '{"vcc_id":0, "data": [{"ag_num":"8001"},{"ag_num":"8002"}]}',
        );
        $client->request('POST', '/agent/logout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID和企业代码都为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID包含非数字字符
        $param = array(
            'param' => '{"vcc_id":"1x", "data": [{"ag_num":"8001","phone":"13811112222"},{"ag_num":"8002","phone":"13811112223"}]}',
        );
        $client->request('POST', '/agent/logout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //参数中详细数据data为空
        $param = array(
            'param' => '{"vcc_id":"1", "data": ""}',
        );
        $client->request('POST', '/agent/logout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '参数中详细数据data为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //参数中详细数据data格式不对，要求为数组
        $param = array(
            'param' => '{"vcc_id":"1", "data": "xxa"}',
        );
        $client->request('POST', '/agent/logout', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '参数中详细数据data格式不对，要求为数组',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //500总错误；
        $param = array(
            'param' => '{"vcc_code":"wintel", "data": [{"ag_num":"1001"},{"ag_num":"1010"},{"ag_num":"9002"}]}',
        );
        $client->request('POST', '/agent/logout', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *
     * 测试删除坐席
     *
     */
    public function testAgentDeleteAction()
    {
        $client = static::createClient();
        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'ag_id' => '1',
        );
        $client->request('POST', '/agent/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1xc',
            'ag_id' => '1',
        );
        $client->request('POST', '/agent/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席ID为空
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '0',
        );
        $client->request('POST', '/agent/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席ID包含非数字字符
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1xx',
        );
        $client->request('POST', '/agent/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '坐席ID包含非数字字符'
        );
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
        //ok
        $param = array(
            'vcc_id' => '10',
            'ag_id' => '14',
        );
        $client->request('POST', '/agent/delete', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');

        //批量删除
        $param = array(
            'vcc_id' => '110',
            'ag_id' => '[2029,2031]',
        );
        $client->request('POST', '/agent/delete', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *
     * 测试编辑坐席
     *
     */
    public function testAgentEditAction()
    {
        $client = static::createClient();
        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'ag_id' => '1',
            'ag_name' => 'cc',
        );
        $client->request('POST', '/agent/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1xc',
            'ag_id' => '1',
            'ag_name' => 'cc',
        );
        $client->request('POST', '/agent/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席ID为空
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '0',
            'ag_name' => 'cc',
        );
        $client->request('POST', '/agent/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席ID包含非数字字符
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1xx',
            'ag_name' => 'cc',
        );
        $client->request('POST', '/agent/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席名称为空
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'ag_name' => '',
        );
        $client->request('POST', '/agent/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '坐席名称为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //406 坐席类型不正确
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'ag_name' => 'test',
            'ag_role' => 2,
        );
        $client->request('POST', '/agent/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '坐席类型不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407 角色不属于该企业
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'ag_name' => 'test',
            'ag_role' => 1,
            'user_role' => 6,
        );
        $client->request('POST', '/agent/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '角色不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_id' => '110',
            'ag_id' => '2031',
            'ag_name' => 'admin',
            'ag_password' => md5('ooxxooxx'),
            'ag_role' => 0,
            'user_role' => 7,
        );
        $client->request('POST', '/agent/edit', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

    }

    /**
     *
     * 测试添加坐席
     *
     */
    public function testAgentAddAction()
    {
        $client = static::createClient();
        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'que_id' => '1',
            'ag_num' => '123456',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1cc',
            'que_id' => '1',
            'ag_num' => '123456',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID不存在；
        $param = array(
            'vcc_id' => '20000',
            'que_id' => '1',
            'ag_num' => '123456',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '企业ID不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组ID为空
//        $param = array(
//            'vcc_id' => '10',
//            'que_id' => '0',
//            'ag_num' => '123456',
//            'ag_name' => 'tetName',
//            'ag_password' => md5('123456'),
//        );
//        $client->request('POST', '/agent/add', $param);
//        $response = $client->getResponse()->getContent();
//        $result = array(
//            'code' => 404,
//            'message' => '技能组ID为空',
//        );
//        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //技能组ID包含非数字字符
        $param = array(
            'vcc_id' => '10',
            'que_id' => '1cc',
            'ag_num' => '123456',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '技能组ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //技能组ID不存在
        $param = array(
            'vcc_id' => '10',
            'que_id' => '200000',
            'ag_num' => '123456',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '技能组ID不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席工号为空
        $param = array(
            'vcc_id' => '10',
            'ag_num' => '',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '坐席工号为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席工号包含非数字字符
//        $param = array(
//            'vcc_id' => '10',
//            'que_id' => '14',
//            'ag_num' => '10000xx',
//            'ag_name' => 'tetName',
//            'ag_password' => md5('123456'),
//        );
//        $client->request('POST', '/agent/add', $param);
//        $response = $client->getResponse()->getContent();
//        $result = array(
//            'code' => 408,
//            'message' => '坐席工号包含非数字字符',
//        );
//        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席工号已经存在
        $param = array(
            'vcc_id' => '10',
            'ag_num' => '1001',
            'ag_name' => 'tetName',
            'ag_password' => md5('123456'),
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '坐席工号已经存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席名称为空
        $param = array(
            'vcc_id' => '10',
            'ag_num' => '100002',
            'ag_name' => '',
            'ag_password' => md5('123456'),
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 410,
            'message' => '坐席名称为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席密码为空
        $param = array(
            'vcc_id' => '10',
            'ag_num' => '100003',
            'ag_name' => 'testName',
            'ag_password' => '',
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 411,
            'message' => '坐席密码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //412 坐席类型不正确；
        $param = array(
            'vcc_id' => '10',
            'ag_num' => '100003',
            'ag_name' => 'testName',
            'ag_password' => md5('123456'),
            'ag_role' => 2,
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 412,
            'message' => '坐席类型不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //413  坐席角色不属于该企业
        $param = array(
            'vcc_id' => '110',
            'ag_num' => '100003',
            'ag_name' => 'testName',
            'ag_password' => md5('123456'),
            'ag_role' => 1,
            'user_role' => 6,
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 413,
            'message' => '角色不属于该企业',
        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_id' => '110',
            'que_id' => '4',
            'ag_num' => '100005c',
            'ag_name' => 'testName',
            'ag_password' => md5('123456'),
            'ag_role' => 1,
            'user_role' => 5,
        );
        $client->request('POST', '/agent/add', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     *
     * 获取企业下的坐席信息接口
     *
     */
    public function testAgentListAction()
    {
        $client = static::createClient();
        //企业ID为空
        $param = array(
            'vcc_id' => 0,
        );
        $client->request('POST', '/agent/list', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1xc',
        );
        $client->request('POST', '/agent/list', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //坐席ID包含非数字字符
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1cc',
        );
        $client->request('POST', '/agent/list', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 OK
        $param = array('vcc_id' => '10');
        $client->request('POST', '/agent/list', $param);
        $response = $client->getResponse()->getContent();
        //echo $response.'----';
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
        //200 OK
        $param = array('vcc_id' => '10','ag_id' => '14');
        $client->request('POST', '/agent/list', $param);
        $response = $client->getResponse()->getContent();
       // echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');

        $param = array('vcc_id' => '110','que_id'=>4);
        $client->request('POST', '/agent/list', $param);
        $response = $client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');

        //200 OK
        $param = array('vcc_id' => '10', 'info'=>'{"pagination":{"rows":2,"page":"5"}}');
        $client->request('POST', '/agent/list', $param);
        $response = $client->getResponse()->getContent();
        //echo $response.'----';
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *
     * 修改坐席密码
     *
     */
    public function testUpdatepassAction()
    {
        $client = static::createClient();
        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'ag_id' => '952',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> md5('1234567'),
        );
        $client->request('POST', '/password/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1xc',
            'ag_id' => '952',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password' => md5('1234567'),
        );
        $client->request('POST', '/password/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //坐席ID为空
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '0',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> md5('1234567'),
        );
        $client->request('POST', '/password/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //坐席ID不为数字
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '952x',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> md5('1234567'),
        );
        $client->request('POST', '/password/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //旧密码为空
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '952',
            'old_password' => '',
            'new_password'=> md5('1234567'),
        );
        $client->request('POST', '/password/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '旧密码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //旧密码错误
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '952',
            'old_password' => 'afafsafdsafdsafd',
            'new_password'=> md5('1234567'),
        );
        $client->request('POST', '/password/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '旧密码错误',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //新密码为空
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '952',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> '',
        );
        $client->request('POST', '/password/update', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '新密码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //200 ok
        $param = array(
            'vcc_id' => 10,
            'ag_id' => '952',
            'old_password' => 'e10adc3949ba59abbe56e057f20f883e',
            'new_password'=> md5("1234567"),
        );
        $client->request('POST', '/password/update', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *
     * 测试坐席登录接口
     *
     */
    public function testSigninAction()
    {
        $client = static::createClient();
        //企业ID为空
        $param = array(
            'vcc_code' => 0,
            'ag_num' => '100',
            'password' => 'aaa',
        );
        $client->request('POST', '/agent/signin', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业代码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //坐席工号为空
        $param = array(
            'vcc_code' => 'wintel',
            'ag_num' => '',
            'password' => 'fcea920f7412b5da7be0cf42b8c93759',
        );
        $client->request('POST', '/agent/signin', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '坐席工号为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //密码为空
        $param = array(
            'vcc_code' => 'wintel',
            'ag_num' => '100004',
            'password' => '',
        );
        $client->request('POST', '/agent/signin', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '密码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //404 验证失败；
        $param = array(
            'vcc_code' => 'wintel',
            'ag_num' => '100004',
            'password' => 'ccafdfadsafds',
        );
        $client->request('POST', '/agent/signin', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '验证失败,没有匹配到相应数据',
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //200 OK
        $param = array(
            'vcc_code' => 'wintel',
            'ag_num' => '100004',
            'password' => 'fcea920f7412b5da7be0cf42b8c93759',
        );
        $client->request('POST', '/agent/signin', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *
     * 测试获取空闲坐席接口
     *
    */

    public function testFreeAction()
    {
        $client = static::createClient();
        //企业ID为空
        $vcc_id = 0;
        $client->request('GET', '/agent/free/'.$vcc_id);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID包含非数字字符
        $vcc_id = '1xc';
        $client->request('GET', '/agent/free/'.$vcc_id);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //200 ok
        $vcc_id = 10;
        $client->request('GET', '/agent/free/'. $vcc_id);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *
     * 测试获取当前通话中的坐席
     *
     */
    public function testOnthelineAction()
    {
        $client = static::createClient();
        //企业ID为空
        $vcc_id = 0;
        $client->request('GET', '/agent/ontheline/'.$vcc_id);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //企业ID包含非数字字符
        $vcc_id = '1xc';
        $client->request('GET', '/agent/ontheline/'.$vcc_id);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //200 ok
        $vcc_id = 10;
        $client->request('GET', '/agent/ontheline/'. $vcc_id);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     *
     * 测试设置坐席转接电话接口
     *
     */
    public function testAgextphoneAction()
    {
        $client = static::createClient();
        //企业ID为空
        $param = array(
            'vcc_id' => 0,
            'ag_id' => '100',
            'phone' => '18310132160',
            'state' => '1',
        );
        $client->request('POST', '/agent/agextphone', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID包含非数字字符
        $param = array(
            'vcc_id' => '1x',
            'ag_id' => '100',
            'phone' => '18310132160',
            'state' => '1',
        );
        $client->request('POST', '/agent/agextphone', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //坐席ID为空
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '0',
            'phone' => '18310132160',
            'state' => '1',
        );
        $client->request('POST', '/agent/agextphone', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //坐席ID包含非数字字符
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1x',
            'phone' => '18310132160',
            'state' => '1',
        );
        $client->request('POST', '/agent/agextphone', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //手机号不能为空
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'phone' => '',
            'state' => '1',
        );
        $client->request('POST', '/agent/agextphone', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '手机号为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //手机号码含非数字字符
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'phone' => '1xx',
            'state' => '1',
        );
        $client->request('POST', '/agent/agextphone', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '手机号码含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //手机号码不是11位
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'phone' => '183101321601',
            'state' => '1',
        );
        $client->request('POST', '/agent/agextphone', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '手机号码不是11位',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //手机号码不是11位
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '1',
            'phone' => '18310132160',
            'state' => '3',
        );
        $client->request('POST', '/agent/agextphone', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '状态值不是0或1',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //对应的坐席不存在
        $param = array(
            'vcc_id' => '1',
            'ag_id' => '10000000',
            'phone' => '18310132160',
            'state' => '1',
        );
        $client->request('POST', '/agent/agextphone', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '对应的坐席不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 oK
        $param = array(
            'vcc_id' => '10',
            'ag_id' => '16',
            'phone' => '18310132160',
            'state' => '1',
        );
        $client->request('POST', '/agent/agextphone', $param);
        $response = $client->getResponse()->getContent();
       // var_dump($response);
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

}
