<?php
namespace Wintel\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Wintel\RestBundle\Controller;

/**
 * Class CtiControllerTest
 * @package Wintel\RestBundle\Tests\Controller
 */
class CtiControllerTest extends WebTestCase
{
    public function testWebcallAction ()
    {
        $client = static::createClient();

        $param = array(
            'vcc_id' => 0,
            'caller' => '18310132160',
            'called' => '03167492563',
            'timeout' => '50',
        );
        $client->request('POST', '/cti/webcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 401,
            'message' => '企业ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //企业ID为空
        $param = array(
            'vcc_id' => '1xx',
            'caller' => '12',
            'called' => '1212',
            'timeout' => '50',
        );
        $client->request('POST', '/cti/webcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //主叫号码为空
        $param = array(
            'vcc_id' => '1',
            'caller' => '',
            'called' => '1212',
            'timeout' => '50',
        );
        $client->request('POST', '/cti/webcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '主叫号码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //被叫号码为空
        $param = array(
            'vcc_id' => '1',
            'caller' => '12',
            'called' => '',
            'timeout' => '50',
        );
        $client->request('POST', '/cti/webcall', $param);
        $response = $client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '被叫号码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_id' => '1',
            'caller' => '12',
            'called' => '13456',
            'timeout' => '50',
        );
        $client->request('POST', '/cti/webcall', $param);
        $response = $client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    public function testVerifyPhoneIsValid()
    {
        $cti = new Controller\CtiController();
        $this->assertTrue($cti->verifyPhoneIsValid('13811220681'));
        $this->assertFalse($cti->verifyPhoneIsValid('138117406811'));
        $this->assertFalse($cti->verifyPhoneIsValid('11011740681'));
        $this->assertTrue($cti->verifyPhoneIsValid('01082743150'));
        $this->assertTrue($cti->verifyPhoneIsValid('03162774212'));
        $this->assertTrue($cti->verifyPhoneIsValid('031627742121'));
        $this->assertFalse($cti->verifyPhoneIsValid('aa123123'));
        $this->assertFalse($cti->verifyPhoneIsValid('010111222'));
    }
}
