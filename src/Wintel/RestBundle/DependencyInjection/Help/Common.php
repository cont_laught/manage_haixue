<?php

namespace Wintel\RestBundle\DependencyInjection\Help;

class Common
{
    /**
     * @var
     * 服务
     */
    private $server;

    public function __construct($container)
    {
        $this->server = $container;
    }

    /**
     * @param int $len
     * @return string
     * 随机字符串
     */
    public function randString($len = 8)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $str = '';
        for ($i=0; $i < $len; $i++) {
            $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
        }
        return $str;
    }

    /**
     * @param $filter
     * @param $info
     * @return String
     * 拼凑 like 语句
     */
    public function likeWhere($filter, $info)
    {
        $where = '';
        foreach ($filter as $v) {
            if (isset($info[$v]) && !empty($info[$v])) {
                $where.=" AND $v like '%".$info[$v]."%' ";
            }
        }
        return $where;
    }

    /**
     * @param $filter
     * @param $info
     * @return array
     * 拼凑 >=  <=
     */
    public function ltRtWhere($filter, $info)
    {
        $where = '';
        $params = array();
        foreach ($filter as $v) {
            $min = 'min_'.$v;
            $max = 'max_'.$v;
            if (isset($info[$min]) && !empty($info[$min])) {
                $where.=" AND $v >= :$min ";
                $params[$min] = $info[$min];
            }
            if (isset($info[$max]) && !empty($info[$max])) {
                $where.=" AND $v <= :$max ";
                $params[$max] = $info[$max];
            }
        }
        return array('where'=>$where,'params'=>$params);
    }

    /**
     * @param $filter
     * @param $info
     * @return array
     * 拼凑 =
     */
    public function equalWhere($filter, $info)
    {
        $where = '';
        $params = array();
        foreach ($filter as $v) {
            if (isset($info[$v]) && $info[$v] !== '') {
                $where.=" AND $v = :$v ";
                $params[$v] = $info[$v];
            }
        }
        return array('where'=>$where,'params'=>$params);
    }

    /**
     * @param $addInfo
     * @param $timeField
     * @return string
     * 拼接时间
     */
    public function dateTimeWhere($addInfo, $timeField = 'start_time')
    {
        $where = '';
        if (isset($addInfo['start_time']) && !empty($addInfo['start_time'])) {
            $start_time = $addInfo['start_time'];
            $msg = $this->server->get('validator.extend.custom')->isDateTime('开始时间格式不正确', $start_time, 404);
            if (!empty($msg) && is_array($msg)) {
                return $msg;
            }
            $where.="AND $timeField >= UNIX_TIMESTAMP('$start_time') ";
        }

        //结束时间
        if (isset($addInfo['end_time']) && !empty($addInfo['end_time'])) {
            $end_time = $addInfo['end_time'];
            $msg = $this->server->get('validator.extend.custom')->isDateTime('结束时间格式不正确', $end_time, 405);
            if (!empty($msg) && is_array($msg)) {
                return $msg;
            }
            $where.="AND $timeField <= UNIX_TIMESTAMP('$end_time') ";
        }
        return $where;
    }

    /**
     * @param $file
     * @return string
     * 查询文件类型；
     */
    public function getSoundFileType($file)
    {
        $extend = pathinfo($file);
        $extend = empty($extend['extension']) ? '' : strtolower($extend['extension']);
        if ($extend == 'mp3') {
            return 'audio/mp3';
        } elseif ($extend == 'wav') {
            return 'audio/x-wav';
        } else {
            return '';
        }
    }
}
