<?php

namespace Wintel\RestBundle\Security\Validator;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

class ExtendCustomValidator
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     * 服务
     */
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @param $field
     * @param $value
     * @param $code
     * @return array|string
     * 验证是否为数字
     */
    public function isNumeric($field, $value, $code)
    {
        $type = new Type(array('type'));
        $type->type = 'numeric';
        $type->message = $field." 不是数字";
        $error_list = $this->container->get('validator')->validateValue($value, $type);
        if (count($error_list) > 0) {
            $ret = array('code'=>$code, 'message'=>$error_list[0]->getMessage());
            return $ret;
        }
        return '';
    }

    /**
     * @param $message
     * @param $value
     * @param $code
     * @return array|string
     * 验证是否为日期格式；（eg:2012-05-29）
     */
    public function isDate($message, $value, $code)
    {
        $date = new Date();
        $date->message = $message;
        $error_list = $this->container->get('validator')->validateValue($value, $date);
        if (count($error_list) > 0) {
            $ret = array('code'=>$code, 'message'=>$error_list[0]->getMessage());
            return $ret;
        }
        return '';
    }

    /**
     * @param $message
     * @param $value
     * @param $code
     * @return array|string
     * 验证是否为日期格式；（eg:2012-05-29 05:50:23)
     */
    public function isDateTime($message, $value, $code)
    {
        $date = new DateTime();
        $date->message = $message;
        $error_list = $this->container->get('validator')->validateValue($value, $date);
        if (count($error_list) > 0) {
            $ret = array('code'=>$code, 'message'=>$error_list[0]->getMessage());
            return $ret;
        }
        return '';
    }

    /**
     * @param $reg （正则）
     * @param $value （值）
     * @param $message （错误信息）
     * @param $code （错误代码）
     * @return array|string
     * 正则验证格式的正确性；
     */
    public function regexRormat($reg, $value, $message, $code)
    {
        $regex = new Regex(array('pattern'));
        $regex->pattern = $reg;
        $regex->message = $message;
        $error_list = $this->container->get('validator')->validateValue($value, $regex);
        if (count($error_list) > 0) {
            $ret = array('code'=>$code, 'message'=>$error_list[0]->getMessage());
            return $ret;
        }
        return '';
    }

    /**
     * @param $field
     * @param $table
     * @return bool
     * 验证某一个字段是否属于某张表；
     */
    public function fieldExistTable($field, $table)
    {
        $conn = $this->container->get('doctrine.dbal.default_connection');
        $list = $conn->fetchAll('DESC '.$table);
        if (empty($list)) {
            return false ;
        }
        $fields = array();
        foreach ($list as $v) {
            $fields[] = $v['Field'];
        }
        if (in_array($field, $fields)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $count
     * @param $table
     * @param $addInfo
     * @return array
     * 获取分页信息
     */
    public function getPageInfo($count, $table, $addInfo)
    {
        $limit = isset($addInfo['pagination']['rows']) ?  $addInfo['pagination']['rows'] : 100000;
        $total_pages = ceil($count/$limit);
        $page = isset($addInfo['pagination']['page']) ? $addInfo['pagination']['page'] : 1;
        $page = $page > $total_pages ? $total_pages : $page;
        $start = $limit*$page - $limit;
        $start = $start > 0 ? $start : 0;
        $flag = false;
        if (isset($addInfo['sort']['field'])) {
            //就需要验证传入字段是否属于坐席表
            $flag = $this->fieldExistTable($addInfo['sort']['field'], $table);
        }
        $sort = $flag ? $addInfo['sort']['field'] : 1;
        $order = isset($addInfo['sort']['order']) && in_array(strtolower($addInfo['sort']['order']), array('desc','asc'))
            ? $addInfo['sort']['order'] : 'desc';
        return array('start'=>$start, 'sort'=>$sort, 'order'=>$order,'limit'=>$limit);
    }
}
