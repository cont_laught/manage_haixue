<?php

namespace Wintel\RestBundle\Security\Validator;

use Doctrine\DBAL\Connection;
use Symfony\Bridge\Monolog\Logger;

class CustomValidator
{
    /**
     * @var
     * 连接数据库
     */
    private $conn;

    /**
     * @var
     *  logger  写日志
     */
    private $logger;

    public function __construct(Connection $connection, Logger $logger)
    {
        $this->conn = $connection;
        $this->logger = $logger;
    }


    /**
     * @param string $value
     * @return array|string
     * 验证 企业代码
     */
    public function checkVccCode($value = '')
    {
        if (empty($value)) {
            $ret = array(
                'code' => 401,
                'message' => '企业代码为空',
            );

            return $ret;
        }

        $vcc_id = $this->conn->fetchColumn(
            " SELECT vcc_id FROM cc_ccods WHERE vcc_code = :vcc_code Limit 1 ",
            array('vcc_code'=>$value)
        );

        if (empty($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业代码不存在',
            );

            return $ret;
        }

        return $vcc_id;
    }

    /**
     * @param string $value
     * @param string $code
     * @param string $msg
     * @return array|string
     * 验证名称是否为空,默認是技能組
     */
    public function isEmptyName($value = '', $code = '403', $msg = '技能组名称为空')
    {
        if (empty($value)) {
            $ret = array(
                'code' => $code,
                'message' => $msg,
            );
            return $ret;
        }
        return '';
    }

    /**
     * @param $vcc_id
     * @param $que_id
     * @param $code
     * @return array|string
     * 验证技能组 是否属于该企业
     */
    public function vccQue($vcc_id, $que_id, $code = '404')
    {
        //验证技能组id 是否属于该企业；
        $vid = $this->conn->fetchColumn(
            'SELECT vcc_id '.
            'FROM win_queue '.
            'WHERE id = :que_id',
            array('que_id'=>$que_id)
        );
        //判断节能组是否属于该企业
        if ($vcc_id != $vid) {
            $ret = array(
                'code' => $code,
                'message' => '技能组不属于该企业',
            );
            return $ret;
        }
        return '';
    }

    /**
     * @param $where
     * @param $par_arr
     * @param string $code
     * @return array|string
     * 技能组名称重复
     */

    public function existQue($where, $par_arr, $code = '405')
    {
        $count = $this->conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_queue '.
            'WHERE '.$where,
            $par_arr
        );
        if ($count > 0) {
            $ret = array(
                'code' => $code,
                'message' => '技能组已经存在',
            );
            return $ret;
        }
        return '';
    }

    /**
     * @param $vcc_id
     * @param $group_id
     * @param int $code
     * @return array|string
     * 验证业务组是否属于该企业
     */
    public function vccGroup($vcc_id, $group_id, $code = 404)
    {
        //验证业务组id 是否属于该企业；
        $vid = $this->conn->fetchColumn(
            'SELECT vcc_id '.
            'FROM win_group '.
            'WHERE group_id = :group_id',
            array('group_id'=>$group_id)
        );
        //判断业务组是否属于该企业
        if ($vcc_id != $vid) {
            $ret = array(
                'code' => $code,
                'message' => '业务组不属于该企业',
            );
            return $ret;
        }
        return '';
    }


    /**
     * @param $where
     * @param $par_arr
     * @param string $code
     * @return array|string
     * 验证角色是否已经存在
     */
    public function existRole($where, $par_arr, $code = '405')
    {
        $count = $this->conn->fetchColumn(
            'SELECT count(*) '.
            'FROM cc_roles '.
            'WHERE '.$where,
            $par_arr
        );
        if ($count > 0) {
            $ret = array(
                'code' => $code,
                'message' => '角色已经存在',
            );
            return $ret;
        }
        return '';
    }

    /**
     * @param $vcc_id
     * @param $role_id
     * @param string $code
     * @return array|string
     * 验证角色是否属于该企业
     */
    public function vccRole($vcc_id, $role_id, $code = '404')
    {
        //验证角色id 是否属于该企业；
        $vid = $this->conn->fetchColumn(
            'SELECT vcc_id '.
            'FROM cc_roles '.
            'WHERE role_id = :role_id',
            array('role_id'=>$role_id)
        );
        if ($vcc_id != $vid) {
            $ret = array(
                'code' => $code,
                'message' => '角色不属于该企业',
            );
            return $ret;
        }
        return '';
    }

    /**
     * @param $vcc_id
     * @param $address
     * @param string $port
     * @param string $code
     * @return bool
     *
     */
    public function wintelsReload($vcc_id, $address, $port = '5015', $code = '416')
    {
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($socket === false) {
            $errorCode = socket_last_error();
            $errorMsg = socket_strerror($errorCode);
            $this->logger->debug('无法创建socket：['.$errorCode.']'.$errorMsg);
            $ret = array(
                'code' => $code,
                'message' => '重载失败 [ 无法创建socket:'.$errorCode.$errorMsg.']',
            );
            return $ret;
        }
        $res = @socket_connect($socket, $address, $port);
        //连接失败
        if ($res === false) {
            $errorCode = socket_last_error();
            $errorMsg = socket_strerror($errorCode);
            $this->logger->debug('socket无法连接到['.$address.':'.$port.']：['.$errorCode.']'.$errorMsg);
            $ret  = array(
                'code' => $code,
                'message' => '重载失败 [socket无法连接到:'.$errorCode.$errorMsg.']',
            );
            return $ret;
        }
        $str="reload queue $vcc_id\r\n\r\n";
        $res = @socket_write($socket, $str, strlen($str));
        if ($res === false) {
            $errorCode = socket_last_error();
            $errorMsg = socket_strerror($errorCode);
            $this->logger->debug('socket发送数据失败：['.$errorCode.']'.$errorMsg);
            $ret = array(
                'code' => $code,
                'message' => '重载失败 [socket发送数据失败:'.$errorCode.$errorMsg.']',
            );
            return $ret;
        }
        $responce = @socket_read($socket, 100);
        if (strstr($responce, 'Success')) {
            //记录 成功一个
            $ret = array(
                'code' => 200,
                'message' => 'ok',
            );
            return $ret;
        } else {
            //如果出现无法连接服务器
            $errorCode = socket_last_error();
            $errorMsg = socket_strerror($errorCode);
            $this->logger->debug('socket读取数据失败：['.$errorCode.']'.$errorMsg);
            $ret = array(
                'code' => $code,
                'message' => '重载失败 [socket读取数据失败:'.$errorCode.$errorMsg.']',
            );
            return $ret;
        }
    }

    /**
     * @param $info
     * @return array|mixed
     * 检查是否为 json
     */
    public function checkJson($info)
    {
        $addInfo = array();
        if (!empty($info)) {
            $addInfo = json_decode($info, true);
            if (json_last_error()) {
                return array('code' => 403, 'message'=>'info格式非json');
            }
        }
        return $addInfo;
    }

    /**
     * @param $where
     * @param $par_arr
     * @param string $code
     * @return array|string
     * 日程规则名称重复
     */
    public function existRule($where, $par_arr, $code = '405')
    {
        $count = $this->conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_rules '.
            'WHERE '.$where,
            $par_arr
        );
        if ($count > 0) {
            $ret = array(
                'code' => $code,
                'message' => '日程规则已经存在',
            );
            return $ret;
        }
        return '';
    }

    /**
     * @param $where
     * @param $par_arr
     * @param string $code
     * @return array|string
     * IVR流程名称重复
     */
    public function existIvr($where, $par_arr, $code = '405')
    {
        $count = $this->conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_ivr '.
            'WHERE '.$where,
            $par_arr
        );
        if ($count > 0) {
            $ret = array(
                'code' => $code,
                'message' => 'IVR流程已经存在',
            );
            return $ret;
        }
        return '';
    }
}
