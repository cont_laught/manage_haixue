﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
           string url = "http://192.168.1.36:82/wintelapi/web/app_dev.php/cti/webcall";    
            try {
                //定义request并设置request的路径
                WebRequest request = WebRequest.Create(url);   //定义请求的方式
                request.Method = "POST"; //初始化request参数
                string vcc_code = "wintel";
                string secret = "e10adc3949ba59abbe56e057f20f883e";
                string time = "912345678";
                string noce = "12345678"; //一定要是4的偶数倍;
                string res = Base64Code(SHA1_Encrypt(Base64Decode(noce) + time + secret));//目前不知道为什么有一部分不一样
                Console.WriteLine(res);
                request.Headers.Add("X-WSSE", "UsernameToken Username=\""+vcc_code+"\",PasswordDigest=\""+res+"\", Nonce=\""+noce+"\", Created=\""+time+"\"");
                string postData = "vcc_code=8015091801&called=18310132160&caller=13810280803&display_caller=03162774864&display_called=03162774864";
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);//设置request的MIME类型及内容长度
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;
                //打开request字符流
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse response = request.GetResponse();//获取相应的状态代码
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);//定义response字符流
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();//读取所有
                Console.WriteLine(responseFromServer);//关闭资源
                reader.Close();
                dataStream.Close();
                response.Close();
            } catch (WebException we) {
                Stream dataStream = we.Response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();//读取所有
                Console.WriteLine(responseFromServer);//关闭资源
                Console.WriteLine(we.Status+"---"+we.ToString()+"---");
            }
            Console.Read();
        }

        //Base64加密  
        static public string Base64Code(String str)
        {
            byte[] bytes = Encoding.GetEncoding("GB2312").GetBytes(str);
            return Convert.ToBase64String(bytes);
        }

        //Base64解密
        static public string Base64Decode(String str)
        {
            byte[] bytes = Convert.FromBase64String(str);
            return Encoding.GetEncoding("GB2312").GetString(bytes);
        }

        /// <summary>
        /// 对字符串进行SHA1加密
        /// </summary>
        /// <param name="strIN">需要加密的字符串</param>
        /// <returns>密文</returns>
        static public string SHA1_Encrypt(string Source_String)
        {
            byte[] StrRes = Encoding.Default.GetBytes(Source_String);
            HashAlgorithm iSHA = new SHA1CryptoServiceProvider();
            StrRes = iSHA.ComputeHash(StrRes);
            string str = Encoding.Default.GetString(StrRes);
            return str;
            /*十六进制输出
            StringBuilder EnText = new StringBuilder();
            foreach (byte iByte in StrRes)
            {
                EnText.Append(iByte.ToString());
                //EnText.AppendFormat("{0:x2}", iByte);
            }
            return EnText.ToString();*/
        }
    }
}
