package Hi;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class httpdemo {
	
    public static void main(String[] args) throws Exception
    {
        HttpClient client = new HttpClient();
        client.getHostConfiguration().setHost("60.10.131.82", 80, "http");
        HttpMethod method = getPostMethod();
        client.executeMethod(method);
        System.out.println(method.getStatusLine());
        String response =   new String(method.getResponseBodyAsString().getBytes("utf-8"));
        System.out.println(response);
        method.releaseConnection();
    }
    
	public static String MD5(String str){
		MessageDigest md = null;
		byte[] message = null;
		try {
			 md = MessageDigest.getInstance("MD5");
			 message =  md.digest(str.getBytes());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
		return new BigInteger(1, message).toString(16);	
	}

	public static String encode(String nonce, String created, String secret) throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.reset();
		String decode = decryptBASE64(nonce)+created+secret;
		return new BASE64Encoder().encode(md.digest(decode.getBytes()));
	}
	public static String decryptBASE64(String key){
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			return new String(decoder.decodeBuffer(key));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";	
	}	    
    private static HttpMethod getPostMethod() throws Exception {
        PostMethod post = new PostMethod("/wintelapi/web/callin");
    	String Username = "420110002"; 
		String Nonce = "123456abc";
		String secret = "e10adc3949ba59abbe56e057f20f883e";
		String Created = "123456abc"; 
		String PasswordDigest = encode(Nonce,Created,secret);				
		String headerValue = "UsernameToken Username=\""+ Username +"\",PasswordDigest=\""+PasswordDigest+"\",Nonce=\""+Nonce+"\",Created=\""+Created+"\"";
		post.setRequestHeader("X-WSSE",headerValue);
		String test = "{\"filter\":{\"start_time\":\"2014-05-06 00:00:00\"}}";		
        NameValuePair model0 = new NameValuePair("vcc_code","420110002");
        NameValuePair model1 = new NameValuePair("start_date","2014-10-10 00:00:00");
        post.setRequestBody(new NameValuePair[] { model0,model1});      
        return post;
    }
   
		
		
} 