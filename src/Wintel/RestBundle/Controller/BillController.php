<?php

namespace Wintel\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BillController extends Controller
{
    /**
     * @var array
     * 咨询挂断类型
     */
    private $bvendResult = array(
        '0' => '挂断',
        '1' => '转接',
        '2' => '拦截',
    );

    /**
     * @param Request $request
     * @return JsonResponse
     * 咨询三方 通话详情
     */
    public function conferenceAction(Request $request)
    {
        $vcc_code = $request->get("vcc_code", 0);
        $info = $request->get('info', ''); //分页搜索相关信息；
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");

        $msg = $vcc_id = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //403 json
        $msg = $addInfo = $this->get('validator.custom')->checkJson($info);
        if (!empty($msg) && isset($msg['code'])) {
            return new JsonResponse($msg);
        }

        $where = 'vcc_id = :vcc_id AND ext_type = 1 ';
        $params =  array('vcc_id' => $vcc_id);
        if (isset($addInfo['filter'])) {
            //坐席工号，发起号码，咨询三方号码
            $where.=$this->get('help.function')->likeWhere(array('ag_ernum','call_phone','ext_phone'), $addInfo['filter']);
            //时间，需要验证格式；
            $msg = $this->get('help.function')->dateTimeWhere($addInfo['filter']);
            if (!empty($msg) && is_array($msg)) {
                return new JsonResponse($msg);
            } else {
                $where.=$msg;
            }
            //时长
            $arr = $this->get('help.function')->ltRtWhere(array('conn_secs'), $addInfo['filter']);
            //结束类型
            $enRow = $this->get('help.function')->equalWhere(array('endresult','que_id'), $addInfo['filter']);
            $params = array_merge($arr['params'], $enRow['params'], $params);
            $where.=$arr['where'].$enRow['where'];
        }
        $count = $conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_extcdr '.
            'WHERE '.$where,
            $params
        );
        $page = $this->get("validator.extend.custom")->getPageInfo($count, 'win_extcdr', $addInfo);
        //200 ok
        $list = $conn->fetchAll(
            'SELECT ag_ernum,ag_ername,que_name,ag_edname,ag_ednum,call_phone,ext_phone,start_time,conn2_time,'.
            'conn1_secs,conn2_secs,conn_secs,endresult '.
            'FROM  win_extcdr '.
            'WHERE '.$where.
            'ORDER BY '.$page['sort'].' '.$page['order'].' LIMIT '.$page['start'].','.$page['limit'],
            $params
        );
        $res = array();
        if (!empty($list)) {
            foreach ($list as $v) {
                $v['start_time'] = $v['start_time'] ? date("Y-m-d H:i:s", $v['start_time']) : "";
                $v['conn2_time'] = $v['conn2_time'] ? date("Y-m-d H:i:s", $v['conn2_time']) : "";
                $v['endresult'] = isset($this->bvendResult[$v['endresult']]) ? $this->bvendResult[$v['endresult']] : '';
                $res[] = $v;
            }
        }
        $ret = array('code'=>200, 'message'=>'ok', 'total'=>$count, 'data'=>$res);
        return new JsonResponse($ret);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * 监听强插  通话详情
     */
    public function monitorAction(Request $request)
    {
        $vcc_code = $request->get("vcc_code", 0);
        $info = $request->get('info', ''); //分页搜索相关信息；
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");

        $msg = $vcc_id = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //403 json
        $msg = $addInfo = $this->get('validator.custom')->checkJson($info);
        if (!empty($msg) && isset($msg['code'])) {
            return new JsonResponse($msg);
        }
        $where = 'vcc_id = :vcc_id AND ext_type = 2 ';
        $params =  array('vcc_id' => $vcc_id);
        if (isset($addInfo['filter'])) {
            //坐席工号，发起号码，被监听号码
            $where.=$this->get('help.function')->likeWhere(array('ag_ernum','call_phone','ext_phone'), $addInfo['filter']);
            //时间，需要验证格式；
            $msg = $this->get('help.function')->dateTimeWhere($addInfo['filter']);
            if (!empty($msg) && is_array($msg)) {
                return new JsonResponse($msg);
            } else {
                $where.=$msg;
            }
            //结束类型
            $enRow = $this->get('help.function')->equalWhere(array('endresult','que_id'), $addInfo['filter']);
            $params = array_merge($enRow['params'], $params);
            $where.=$enRow['where'];
        }
        $count = $conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_extcdr '.
            'WHERE '.$where,
            $params
        );
        $page = $this->get("validator.extend.custom")->getPageInfo($count, 'win_extcdr', $addInfo);
        //200 ok
        $list = $conn->fetchAll(
            'SELECT ag_ernum,ag_ername,que_name,ag_edname,ag_ednum,call_phone,ext_phone,start_time,conn2_time,'.
            'conn1_secs,conn2_secs,conn_secs,endresult '.
            'FROM  win_extcdr '.
            'WHERE '.$where.
            'ORDER BY '.$page['sort'].' '.$page['order'].' LIMIT '.$page['start'].','.$page['limit'],
            $params
        );
        $res = array();
        if (!empty($list)) {
            foreach ($list as $v) {
                $v['start_time'] = $v['start_time'] ? date("Y-m-d H:i:s", $v['start_time']) : "";
                $v['conn2_time'] = $v['conn2_time'] ? date("Y-m-d H:i:s", $v['conn2_time']) : "";
                $v['endresult'] = isset($this->bvendResult[$v['endresult']]) ? $this->bvendResult[$v['endresult']] : '';
                $res[] = $v;
            }
        }
        $ret = array('code'=>200, 'message'=>'ok', 'total'=>$count, 'data'=>$res);
        return new JsonResponse($ret);
    }
}
