<?php

namespace Wintel\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BusyReasonController
 * @package Wintel\RestBundle\Controller
 */
class BusyreasonController extends Controller
{
    /**
     * 添加置忙原因
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function busyreasonAddAction(Request $request)
    {
        $vcc_id = $request->get("vcc_id"); //企业id
        $ag_stat = $request->get("stat"); //坐席状态
        $stat_reason = $request->get("reason"); //置忙原因

        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空'
            );
            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业ID为非数字'
            );
            return new JsonResponse($ret);
        }

        if (empty($ag_stat)) {
            $ret = array(
                'code' => 403,
                'message' => '坐席状态不能为空'
            );
            return new JsonResponse($ret);
        }

        if (empty($stat_reason)) {
            $ret = array(
                'code' => 405,
                'message' => '置忙原因不能为空'
            );
            return new JsonResponse($ret);
        }

        $stat_reason = json_decode($stat_reason, true);
        //$error = json_last_error();
        if (json_last_error()) {
            $ret = array(
                'code' => 406,
                'message' => 'JSON格式不正确: '.json_last_error(),
            );
            return new JsonResponse($ret);
        }

        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get('doctrine.dbal.default_connection');
        $conn->beginTransaction();
        foreach ($stat_reason as $reason) {
            $data = array(
                'vcc_id' => $vcc_id,
                'ag_stat' => $ag_stat,
                'stat_reason' => $reason
            );

            $conn->insert('win_agstat_reason', $data); //执行添加
        }

        try {
            $conn->commit();

            $ret = array(
                'code' => 200,
                'message' => 'ok'
            );
            return new JsonResponse($ret);

        } catch (Exception $e) {
            $conn->rollback();

            $ret = array(
                'code' => 404,
                'message' => '添加设置置忙原因失败[' . $e->getMessage() . ']'
            );
            return new JsonResponse($ret);
        }

    }

    /**
     * 删除置忙原因
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function busyreasonDeleteAction(Request $request)
    {
        $vcc_id = $request->get("vcc_id"); //企业id
        $ids = $request->get('ids');

        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空'
            );
            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业ID为非数字'
            );
            return new JsonResponse($ret);
        }

        if (empty($ids)) {
            $ret = array(
                'code' => 405,
                'message' => '置忙ID不能为空'
            );
            return new JsonResponse($ret);
        }

        if (!is_string($ids)) {
            $ret = array(
                'code' => 406,
                'message' => '置忙ID格式不对，要求为多个ID逗号分隔的字符串，但是得到' . gettype($ids)
            );
            return new JsonResponse($ret);
        }

        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get('doctrine.dbal.default_connection');
        $conn->beginTransaction();
        $idsArray = explode(',', $ids);
        foreach ($idsArray as $id) {
            if (!is_numeric($id)) {
                $ret = array(
                    'code' => 403,
                    'message' => '置忙ID中包含非数字字符'
                );

                return new JsonResponse($ret);
            }

            $data = array(
                'vcc_id' => $vcc_id,
                'id' => $id
            );
            $conn->delete('win_agstat_reason', $data);
        }

        try {
            $conn->commit();

            $ret = array(
                'code' => 200,
                'message' => 'ok'
            );
            return new JsonResponse($ret);

        } catch (Exception $e) {
            $conn->rollback();

            $ret = array(
                'code' => 404,
                'message' => '删除置忙原因失败[' . $e->getMessage() . ']'
            );
            return new JsonResponse($ret);
        }
    }

    /**
     * 获取置忙原因列表数据
     *
     * @param int $vcc_id
     *
     * @return JsonResponse|Response
     */
    public function busyreasonListAction($vcc_id)
    {
        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空'
            );
            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业ID包含非数字字符'
            );
            return new JsonResponse($ret);
        }

        // 是否是jsonp调用
        $callback = $this->get('request')->query->get('jsonpcallback', '');

        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get('doctrine.dbal.default_connection');
        $data = $conn->fetchAll(
            "SELECT id,vcc_id,ag_stat,stat_reason FROM win_agstat_reason WHERE vcc_id= ?",
            array($vcc_id)
        );

        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data
        );

        if (empty($callback)) {
            return new JsonResponse($ret);
        } else {
            return new Response(sprintf('%s(%s)', $callback, json_encode($ret)));
        }
    }
}
