<?php

namespace Wintel\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MonitorController
 * @package Wintel\RestBundle\Controller
 */
class MonitorController extends Controller
{
    private $callType = array(
        '1' => '呼出',
        '2' => '呼入',
        '3' => '呼出转接',
        '4' => '呼入转接',
        '5' => '呼出拦截',
        '6' => '呼入拦截',
        '7' => '被咨询',
        '9' => '监听',
    );
    /**
     *  获取坐席监控数据
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     */
    public function agentAction(request $request)
    {
        $vcc_id = $request->get("vcc_id", 0);
        $user_ids = $request->get('user_ids', 0);
        $que_id = $request->get('que_id', 0);
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");

        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空',
            );
            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业ID包含非数字字符',
            );
            return new JsonResponse($ret);
        }
        /*
        if (empty($user_ids)) {
            $ret = array(
                'code' => 405,
                'message' => '坐席ID为空',
            );
            return new JsonResponse($ret);
        }*/
        $user_arr = array();
        if (!empty($user_ids)) {
            $user_arr = explode(",", $user_ids);
            foreach ($user_arr as $val) {
                if (!is_numeric($val)) {
                    $ret = array(
                        'code' => 403,
                        'message' => '坐席ID中含有非数字项',
                    );
                    return new JsonResponse($ret);
                }
            }
        }

        if (!empty($que_id)) {
            if (!is_numeric($que_id)) {
                $ret = array(
                    'code' => 404,
                    'message' => '技能组ID包含非数字字符',
                );
                return new JsonResponse($ret);
            }
            //通过技能组ID获得 坐席ID；
            $list = $conn->fetchAll(
                "SELECT ag_id FROM win_agqu WHERE que_id = :que_id",
                array('que_id'=>$que_id)
            );
            foreach ($list as $value) {
                $user_arr[] = $value['ag_id'];
            }
        }
        $str = implode(',', array_unique($user_arr));
        $where = !empty($user_arr) ? " AND ag_id IN ($str) " : '';
        //获得技能组；
        $que_list = $conn->fetchAll(
            'SELECT a.que_id,q.que_name '.
            'FROM win_agqu as a LEFT JOIN win_queue as q ON a.que_id = q.id '.
            'WHERE 1 '.$where
        );
        $que_row = array();
        if (!empty($que_list)) {
            foreach ($que_list as $v) {
                $que_row[$v['que_id']] = $v['que_name'];
            }
        }
        $now_data = $conn->fetchAll(
            "SELECT ag_id,pho_num,ag_sta,ag_sta_time,UNIX_TIMESTAMP(now()) AS now,UNIX_TIMESTAMP(CURDATE()) AS today, ".
            'ag_num,ag_name,ag_sta_reason,pho_id,pho_num,pho_sta,pho_sta_reason,login_ip,time_firlogin,time_login,'.
            'time_lastcall,secs_login,secs_ready,secs_busy,secs_call,secs_ring,secs_wait,times_call,times_busy,pho_sta_time,'.
            'pho_sta_callque,pho_sta_calltype '.
            " FROM win_agmonitor WHERE vcc_id = :vcc_id ".$where,
            array('vcc_id'=>$vcc_id)
        );

        $result = $temp = array();
        foreach ($now_data as $v) {
            /* 坐席 'ag_id'*/
            $temp['ag_id'] = $v['ag_id'];
            /* 坐席分机号码 'pho_num'*/
            $temp['pho_num'] = $v['pho_num'];
            /*坐席状态状态 'status'*/
            $temp['ag_sta'] = $v['ag_sta'];
            /* 状态持续时长 'status_secs'*/
            $temp['status_secs'] = $v['now'] - ($v['ag_sta_time'] >= $v['today'] ? $v['ag_sta_time'] : $v['today']);
            $temp['ag_num'] = $v['ag_num'];
            $temp['ag_name'] = $v['ag_name'];
            $temp['ag_sta_reason'] = $v['ag_sta_reason'];
            $temp['pho_id'] = $v['pho_id'];
            $temp['pho_num'] = $v['pho_num'];
            $temp['pho_sta'] = $v['pho_sta'];
            $temp['pho_sta_reason'] = $v['pho_sta_reason'];
            $temp['login_ip'] = $v['login_ip'];
            $temp['time_firlogin'] = date('Y-m-d H:i:s', $v['time_firlogin']);
            $temp['time_login'] = $v['time_login'];
            $temp['time_lastcall'] = $v['time_lastcall'];
            /*登陆时长*/
            $temp['secs_login'] = $v['now'] - ($v['time_login'] >= $v['today'] ? $v['time_login'] : $v['today']) + $v['secs_login'];
            /*就绪时长*/
            if ($v['ag_sta'] == 1) {
                $temp['secs_ready'] = $v['now'] - ($v['ag_sta_time'] >= $v['today'] ? $v['ag_sta_time'] : $v['today']) + $v['secs_ready'];
            } else {
                $temp['secs_ready'] = $v['secs_ready'];
            }
            /*置忙时长*/
            if ($v['ag_sta'] == 2) {
                $temp['secs_busy'] = $v['now'] - ($v['ag_sta_time'] >= $v['today'] ? $v['ag_sta_time'] : $v['today']) + $v['secs_busy'];
            } else {
                $temp['secs_busy'] = $v['secs_busy'];
            }
            /*通话时长*/
            if ($v['pho_sta'] == 2) {
                $temp['secs_call'] = $v['now'] - ($v['pho_sta_time'] >= $v['today'] ? $v['pho_sta_time'] : $v['today']) + $v['secs_call'];
            } else {
                $temp['secs_call'] = $v['secs_call'];
            }
            /* 振铃时长 */
            if ($v['pho_sta'] == 1) {
                $temp['secs_ring'] = $v['now'] - ($v['pho_sta_time'] >= $v['today'] ? $v['pho_sta_time'] : $v['today']) + $v['secs_ring'];
            } else {
                $temp['secs_ring'] = $v['secs_ring'];
            }
            /* 整理时长 */
            if ($v['ag_sta'] == 5) {
                $temp['secs_wait'] = $v['now'] - ($v['ag_sta_time'] >= $v['today'] ? $v['ag_sta_time'] : $v['today']) + $v['secs_wait'];
            } else {
                $temp['secs_wait'] = $v['secs_wait'];
            }
            $temp['times_call'] = $v['times_call'];
            $temp['times_busy'] = $v['times_busy'];
            $temp['pho_sta_calltype'] = isset($this->callType[$v['pho_sta_calltype']]) ?
                $this->callType[$v['pho_sta_calltype']] : '';
            $temp['pho_sta_callque'] = isset($que_row[$v['pho_sta_callque']]) ? $que_row[$v['pho_sta_callque']] : '';
            $result[] = $temp;
        }
        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $result,
        );
        return new JsonResponse($ret);
    }

    /**
     *  获取技能组监控数据
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     */
    public function queueAction(request $request)
    {
        $vcc_id = $request->get("vcc_id", 0);
        $que_ids = $request->get('que_ids', 0);
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空',
            );
            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业ID包含非数字字符',
            );
            return new JsonResponse($ret);
        }

        if (!empty($que_ids)) {
            $que_arr = explode(",", $que_ids);
            foreach ($que_arr as $val) {
                if (!is_numeric($val)) {
                    $ret = array(
                        'code' => 403,
                        'message' => '技能组ID中含有非数字项',
                    );
                    return new JsonResponse($ret);
                }
            }
        } else {
            //如果不传就是所有的技能组
            $que_row = $conn->fetchAll(
                'SELECT id '.
                'FROM win_queue '.
                'WHERE vcc_id = :vcc_id AND is_del = 0 ',
                array('vcc_id'=>$vcc_id)
            );
            $que_list = array();
            if (!empty($que_row)) {
                foreach ($que_row as $v) {
                    $que_list[] = $v['id'];
                }
            }
            $que_ids = !empty($que_list) ? implode(',', $que_list) : '0';
        }

        $call_queue = $conn->fetchAll(
            "SELECT que_id,COUNT(*) AS total FROM win_call_queue WHERE vcc_id = :vcc_id ".
            " AND que_id IN ($que_ids) GROUP BY que_id",
            array('vcc_id'=>$vcc_id)
        );
        $new_call_queue = array();
        foreach ($call_queue as $cvalue) {
            $new_call_queue[$cvalue['que_id']] = $cvalue;
        }
        $result_other = $conn->fetchAll(
            "SELECT ag_id,ag_sta,pho_sta FROM win_agmonitor WHERE vcc_id = :vcc_id",
            array('vcc_id'=>$vcc_id)
        );
        $new_result_other = array();
        foreach ($result_other as $rvalue) {
            $new_result_other[$rvalue['ag_id']] = $rvalue;
        }
        $que_row = $conn->fetchAll(
            "SELECT que_id,ag_id FROM win_agqu WHERE que_id IN ($que_ids)"
        );
       //var_dump($que_row);
        $queue_agent = array();
        if (!empty($que_row)) {
            foreach ($que_row as $value) {
                $queue_agent[$value["que_id"]][] = $value["ag_id"];
            }
        }

        //处理数据
        $result = array();
        $data = array();
        foreach ($queue_agent as $queue_id => $agent_ids) {
            $result['que_id']= $queue_id;
            $result['online']= 0;//在线
            $result['queue'] = 0;//排队
            $result['ring']  = 0;//振铃
            $result['call']  = 0;//通话
            $result['rest']  = 0;//事后处理
            $result['ready'] = 0;//就绪
            $result['busy']  = 0;//置忙

            /* 排队 */
            if (!empty($new_call_queue[$queue_id]["total"])) {
                $result['queue'] = $new_call_queue[$queue_id]["total"];
            }

            foreach ($new_result_other as $ag_id => $other) {
                /* 判断该坐席是否在该队列中 */
                if (in_array($ag_id, $agent_ids)) {
                    /* 在线 */
                    if ($other['ag_sta'] != 0) {
                        $result['online']++;
                    }
                    /* 振铃 */
                    if ($other['ag_sta'] == 4 && $other['pho_sta'] == 1) {
                        $result['ring']++;
                    }
                    /* 通话 */
                    if ($other['ag_sta'] == 4 && $other['pho_sta'] == 2) {
                        $result['call']++;
                    }
                    /* 事后处理 */
                    if ($other['ag_sta'] == 5) {
                        $result['rest']++;
                    }
                    /* 就绪 */
                    if ($other['ag_sta'] == 1) {
                        $result['ready']++;
                    }
                    /* 置忙 */
                    if ($other['ag_sta'] == 2) {
                        $result['busy']++;
                    }
                }
            }

            $data[] = $result;
        }

        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data,
        );
        return new JsonResponse($ret);
    }

    /**
     *  获取排队监控数据
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     */
    public function callsAction(request $request)
    {
        $vcc_id = $request->get("vcc_id", 0);
        $que_id = $request->get('que_id', 0);
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空',
            );
            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业ID包含非数字字符',
            );
            return new JsonResponse($ret);
        }
        $where = '';
        if (!empty($que_id)) {
            if (!is_numeric($que_id)) {
                $ret = array(
                    'code' => 403,
                    'message' => '技能组ID包含非数字字符',
                );
                return new JsonResponse($ret);
            }
            $where = "AND que_id = '$que_id'";
        }

        $list = $conn->fetchAll(
            "SELECT *,UNIX_TIMESTAMP(now()) AS now FROM win_call_queue WHERE vcc_id = :vcc_id $where",
            array('vcc_id' => $vcc_id)
        );
        $result = $data = array();
        foreach ($list as $v) {
            /* 队列ID que_id*/
            $result['que_id'] = $v['que_id'];
            /* 主叫号码 queuer_num*/
            $result['queuer_num'] = $v['queuer_num'];
            /* 进入队列时间 in_time*/
            $result['in_time'] = $v['in_time'];
            /* 排队状态 queuer_sta*/
            $result['queuer_sta'] = $v['queuer_sta'];
            /* 排队时长 in_secs*/
            $result['in_secs'] = $v['now'] - $v['in_time'];
            $data[] = $result;
        }
        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data,
        );
        return new JsonResponse($ret);
    }

    /**
     *  获取坐席统计数据接口
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     */
    public function stadataAction(request $request)
    {
        $vcc_id = $request->get("vcc_id", 0);
        $user_ids = $request->get('user_ids', 0);
        $start_date = $request->get('start_date', 0);
        $end_date = $request->get('end_date', 0);
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");

        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空',
            );
            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业ID包含非数字字符',
            );
            return new JsonResponse($ret);
        }

        if (empty($user_ids)) {
            $ret = array(
                'code' => 403,
                'message' => '用户ID为空',
            );
            return new JsonResponse($ret);
        }
        $user_arr = explode(",", $user_ids);
        foreach ($user_arr as $val) {
            if (!is_numeric($val)) {
                $ret = array(
                    'code' => 404,
                    'message' => '用户ID中含有非数字项',
                );
                return new JsonResponse($ret);
            }
        }

        //验证开始日期格式是否正确
        $start = $this->isdate($start_date);
        if (!$start['flag']) {
            $ret = array(
                'code' => 405,
                'message' => '开始日期格式不正确'
            );
            return new JsonResponse($ret);
        } else {
            $start_date = $start['str'];
        }

        //验证结束日期格式是否正确
        $end = $this->isdate($end_date);
        if (!$end['flag']) {
            $ret = array(
                'code' => 406,
                'message' => '结束日期格式不正确'
            );
            return new JsonResponse($ret);
        } else {
            $end_date = $end['str'];
        }
        $data = $conn->fetchAll(
            "SELECT ag_id,login_secs,ready_secs,busy_secs FROM rep_agent_day WHERE vcc_id = :vcc_id ".
            " AND ag_id IN ($user_ids) AND nowdate <= :end_date AND nowdate >= :start_date ",
            array('vcc_id' => $vcc_id,'end_date' => $end_date,'start_date' => $start_date)
        );
        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data,
        );
        return new JsonResponse($ret);
    }

    /**
     *  获取坐席操作明细数据接口
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     */
    public function detaildataAction(request $request)
    {
        $vcc_code = $request->get("vcc_code", 0);
        $info = $request->get('info', ''); //分页搜索相关信息；
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");

        //验证vcc_code;
        $msg = $vcc_id = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //分页搜索相关信息；
        $addInfo = array();
        if (!empty($info)) {
            $addInfo = json_decode($info, true);
            if (json_last_error()) {
                return new JsonResponse(array('code' => 403, 'message'=>'info格式非json'));
            }
        }

        $where = '';
        $params = array();
        if (isset($addInfo['filter'])) {
            //坐席工号；
            $where.= isset($addInfo['filter']['ag_num']) && !empty($addInfo['filter']['ag_num']) ?
                " AND ag_num LIKE '%".$addInfo['filter']['ag_num']."%' " : '';

            //操作，登陆，示忙；
            if (isset($addInfo['filter']['ag_sta_type']) && $addInfo['filter']['ag_sta_type'] !== '') {
                $ag_sta_type = $addInfo['filter']['ag_sta_type'] ;
                $where.=" AND ag_sta_type = :ag_sta_type ";
                $params['ag_sta_type'] = (int)$ag_sta_type;
            }

            //坐席姓名；
            $where.= isset($addInfo['filter']['ag_name']) && !empty($addInfo['filter']['ag_name']) ?
                " AND ag_name LIKE '%".$addInfo['filter']['ag_name']."%' " : '';

            //开始时间
            if (isset($addInfo['filter']['start_time']) && !empty($addInfo['filter']['start_time'])) {
                $start_time = $addInfo['filter']['start_time'];
                $msg = $this->get('validator.extend.custom')->isDateTime('开始时间格式不正确', $start_time, 404);
                if (!empty($msg) && is_array($msg)) {
                    return new JsonResponse($msg);
                }
                $where.="AND start_time >= UNIX_TIMESTAMP('$start_time')";
            }

            //结束时间
            if (isset($addInfo['filter']['end_time']) && !empty($addInfo['filter']['end_time'])) {
                $end_time = $addInfo['filter']['end_time'];
                $msg = $this->get('validator.extend.custom')->isDateTime('结束时间格式不正确', $end_time, 405);
                if (!empty($msg) && is_array($msg)) {
                    return new JsonResponse($msg);
                }
                $where.="AND start_time <= UNIX_TIMESTAMP('$end_time')";
            }
        }
        $params['vcc_id'] = $vcc_id;
        $count = $conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_agsta_detail '.
            'WHERE vcc_id = :vcc_id '.$where,
            $params
        );
        $page = $this->get("validator.extend.custom")->getPageInfo($count, 'win_agsta_detail', $addInfo);
        $data = $conn->fetchAll(
            "SELECT vcc_id,ag_id,ag_name,ag_sta_type,start_time,duration,ag_sta_reason,ag_login_ip,ag_num,bend ".
            "FROM win_agsta_detail ".
            'WHERE vcc_id = :vcc_id '.$where.
            'ORDER BY '.$page['sort'].' '.$page['order'].' LIMIT '.$page['start'].','.$page['limit'],
            $params
        );
        //查看原因
        $reason_list = $conn->fetchAll(
            "SELECT id,stat_reason FROM win_agstat_reason ",
            array('vcc_id' => $vcc_id)
        );
        $reason_detail = array();
        foreach ($reason_list as $v) {
            $reason_detail[$v['id']] = $v['stat_reason'];
        }
        $new_data = $temp =  array();
        foreach ($data as $v) {
            $temp['vcc_id'] = $v['vcc_id'];
            $temp['ag_id'] = $v['ag_id'];
            $temp['ag_name'] = $v['ag_name'];
            $temp['ag_num'] = $v['ag_num'];
            $temp['ag_sta_type'] = $v['ag_sta_type'] == 1 ? '登录' : '示忙';
            $temp['start_time'] = date('Y-m-d H:i:s', $v['start_time']);
            $temp['duration'] = $v['duration'];
            $temp['ag_login_ip'] = $v['ag_login_ip'];
            $temp['bend'] = $v['bend'] ? '结束' : '';
            $temp['ag_sta_reason'] = isset($reason_detail[$v['ag_sta_reason']]) && $v['ag_sta_type'] != 1
                ? $reason_detail[$v['ag_sta_reason']] : '';
            $new_data[] = $temp;
        }
        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'total' => $count,
            'data' => $new_data,
        );
        return new JsonResponse($ret);
    }

    /**
     * @param $vcc_code
     * @return JsonResponse
     * 获取系统监控数据接口
     */
    public function systemAction($vcc_code)
    {
        $GLOBALS['conn'] = $conn = $this->get("doctrine.dbal.default_connection");
        //vcc_code 验证
        $msg = $vid = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }
        $res = array();
        //计算queue_nums 排队数
        $res['queue_nums'] = $conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_call_queue '.
            'WHERE vcc_id = :vcc_id ',
            array('vcc_id'=>$vid)
        );

        $res['ring_nums'] = $this->getCount(" vcc_id = $vid AND ag_sta=4 AND pho_sta=1 "); //ring_nums振铃数
        $res['call_nums'] = $this->getCount(" vcc_id = $vid AND ag_sta=4 AND pho_sta=2 "); //call_nums通话数
        $res['wait_nums'] = $this->getCount(" vcc_id = $vid AND ag_sta=5 "); //wait_nums事后整理数
        $res['ready_nums'] = $this->getCount(" vcc_id = $vid AND ag_sta=1 "); //ready_nums就绪数
        $res['busy_nums'] = $this->getCount(" vcc_id = $vid AND ag_sta=2 "); //busy_nums置忙数
        $ret = array('code'=>200, 'message'=>'ok', 'data'=>$res);
        return new JsonResponse($ret);

    }

    /**
     * @param $vcc_code
     * @return JsonResponse
     * 获取ivr;用于监控坐席
     */
    public function agentIvrAction($vcc_code)
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        $msg = $vid = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //ivr 进线数，人工进线数
        $system_day = $conn->fetchAssoc(
            'SELECT ivr_num,in_num,lost_num,nowdate '.
            'FROM rep_system_day '.
            'WHERE vcc_id = :vcc_id AND nowdate = :nowdate',
            array('vcc_id'=>$vid, 'nowdate'=>date('Y-m-d'))
        );

        //ivr 放弃数
        $ivr_lost_num = $conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_incdr '.
            'WHERE vcc_id = :vcc_id AND result = 1 AND start_year = :start_year AND start_month = :start_month '.
            'AND start_day = :start_day ',
            array('vcc_id'=>$vid, 'start_year'=>date('Y'), 'start_month'=>date('m'), 'start_day'=>date('d'))
        );

        //排队数
        $queue = $conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_call_queue '.
            'WHERE vcc_id = :vcc_id ',
            array('vcc_id'=>$vid)
        );

        $conn_num = $config_number = $conn->fetchColumn(
            'SELECT conn_num '.
            'FROM cc_ccod_configs '.
            'WHERE vcc_id = :vcc_id ',
            array('vcc_id'=>$vid)
        );
        $conn_num = empty($conn_num) ? 'conn15_num' : 'conn'.$conn_num.'_num';

        $result = $conn->fetchAssoc(
            'SELECT '.$conn_num.', conn_num '.
            'FROM rep_system_day '.
            'WHERE vcc_id = :vcc_id AND nowdate = :nowdate ',
            array('vcc_id'=>$vid, 'nowdate'=>date('Y-m-d'))
        );

        if (empty($result['conn_num'])) {
            $result['conn_rate'] = '0.00%';
            $result['conn_num'] = 0;
            $result[$conn_num] = '0';
        } else {
            if ($result[$conn_num] > $result['conn_num']) {
                $result['conn_rate'] = '100.00%';
            } else {
                $result['conn_rate'] = number_format($result[$conn_num]/$result['conn_num'] * 100, 2) . '%' ;
            }
        }

        //IVR进线数
        $result['ivr_num']      = empty($system_day['ivr_num']) ? 0 : $system_day['ivr_num'];
        //人工进线数
        $result['in_num']       = empty($system_day['in_num']) ? 0 : $system_day['in_num'];
        //IVR放弃数
        $result['ivr_lost_num'] = empty($ivr_lost_num) ? 0 : $ivr_lost_num;
        //人工放弃数
        $result['lost_num']     = empty($system_day['lost_num']) ? 0 : $system_day['lost_num'];
        //人工放弃率
        $result['lost_rate']    = empty($result['in_num']) ?
            '0.00%' : number_format($system_day['lost_num']/$result['in_num'] * 100, 2) . '%' ;
        //排队数
        $result['queue']        = empty($queue) ? 0 : $queue;
        //配置数
        $result['config_conn_num'] = empty($config_number) ? 15 : $config_number;
        return new JsonResponse(array('code'=>'200', 'message'=>'ok', 'data'=>$result));
    }

    /**
     * @param $vcc_code
     * @return JsonResponse
     * 获取示忙的  理由
     */
    public function getReasonAction($vcc_code)
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        $msg = $vid = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }
        $result = $conn->fetchAll(
            'SELECT id,stat_reason '.
            'FROM win_agstat_reason '.
            'WHERE vcc_id = :vcc_id ',
            array('vcc_id'=>$vid)
        );
        return new JsonResponse(array('code'=>'200', 'message'=>'ok', 'data'=>$result));
    }

    /**
     * 验证是否是日期格式；
     * @param $str
     * @param string $format
     * @return array|bool
     *
     */
    private function isdate($str, $format = "Y-m-d")
    {
        $strArr = explode("-", $str);
        $newArr = array();
        if (empty($strArr)) {
            return false;
        }
        foreach ($strArr as $val) {
            if (strlen($val) < 2) {
                $val = "0" . $val;
            }
            $newArr[] = $val;
        }
        $str = implode("-", $newArr);
        $unixTime = strtotime($str);
        $checkDate = date($format, $unixTime);
        if ($checkDate == $str) {
            return array('flag' => true, 'str' => $str);
        } else {
            return array('flag' => false);
        }
    }

    /**
     * @param int $where
     * @return mixed
     * 获取不同条件的监控统计数据
     */
    private function getCount($where = 1)
    {
        global $conn;
        $count = $conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_agmonitor WHERE '.$where
        );
        return $count;
    }
}
