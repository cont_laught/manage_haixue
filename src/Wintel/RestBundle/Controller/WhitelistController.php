<?php

namespace Wintel\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WhitelistController
 * @package Wintel\RestBundle\Controller
 */
class WhitelistController extends Controller
{
    /**
     * 添加白名单
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function whitelistAddAction(Request $request)
    {
        $vcc_id = $request->get('vcc_id', 0);
        $phones = $request->get('phones', '');
        $trunk_num = $request->get('trunk_num', '');

        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空'
            );

            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 405,
                'message' => '企业ID为非数字'
            );

            return new JsonResponse($ret);
        }

        if (empty($phones)) {
            $ret = array(
                'code' => 402,
                'message' => '号码为空'
            );

            return new JsonResponse($ret);
        }

        if (!is_string($phones)) {
            $ret = array(
                'code' => 406,
                'message' => '号码格式不对，要求为多个号码逗号分隔的字符串，但是得到'.gettype($phones)
            );

            return new JsonResponse($ret);
        }

        if (empty($trunk_num)) {
            $ret = array(
                'code' => 407,
                'message' => '中继号码为空'
            );

            return new JsonResponse($ret);
        }

        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get('doctrine.dbal.default_connection');
        $conn->beginTransaction();
        $phonesArray = explode(',', $phones);
        foreach ($phonesArray as $phone) {
            if (!is_numeric($phone)) {
                $ret = array(
                    'code' => 403,
                    'message' => '号码中包含非数字字符'
                );

                return new JsonResponse($ret);
            }

            $data = array(
                'vcc_id' => $vcc_id,
                'phone_num' => $phone,
                'trunk_num' => $trunk_num,
            );

            $conn->insert('cc_whitelist', $data);
        }

        try {
            $conn->commit();

            $ret = array(
                'code' => 200,
                'message' => 'ok'
            );

            return new JsonResponse($ret);

        } catch (Exception $e) {
            $conn->rollback();

            $ret = array(
                'code' => 404,
                'message' => '添加白名单失败['.$e->getMessage().']'
            );

            return new JsonResponse($ret);
        }
    }

    /**
     * 删除白名单
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function whitelistDeleteAction(Request $request)
    {
        $vcc_id = $request->get('vcc_id', 0);
        $phone_ids = $request->get('phone_ids', '');

        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空'
            );

            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 405,
                'message' => '企业ID包含非数字字符'
            );

            return new JsonResponse($ret);
        }

        if (empty($phone_ids)) {
            $ret = array(
                'code' => 402,
                'message' => '号码ID为空'
            );

            return new JsonResponse($ret);
        }

        if (!is_string($phone_ids)) {
            $ret = array(
                'code' => 406,
                'message' => '号码ID格式不对，要求为多个号码ID逗号分隔的字符串，但是得到'.gettype($phone_ids)
            );

            return new JsonResponse($ret);
        }

        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get('doctrine.dbal.default_connection');
        $conn->beginTransaction();
        $phonesArray = explode(',', $phone_ids);
        foreach ($phonesArray as $phone) {
            if (!is_numeric($phone)) {
                $ret = array(
                    'code' => 403,
                    'message' => '号码ID中包含非数字字符'
                );

                return new JsonResponse($ret);
            }

            $data = array(
                'vcc_id' => $vcc_id,
                'id' => $phone,
            );

            $conn->delete('cc_whitelist', $data);
        }

        try {
            $conn->commit();

            $ret = array(
                'code' => 200,
                'message' => 'ok'
            );

            return new JsonResponse($ret);

        } catch (Exception $e) {
            $conn->rollback();

            $ret = array(
                'code' => 404,
                'message' => '删除白名单失败['.$e->getMessage().']'
            );

            return new JsonResponse($ret);
        }
    }

    /**
     * 查询黑名单
     *
     * @param $vcc_id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function whitelistListAction($vcc_id)
    {
        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空'
            );

            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业ID包含非数字字符'
            );

            return new JsonResponse($ret);
        }

        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get('doctrine.dbal.default_connection');
        $data = $conn->fetchAll(
            "SELECT id,vcc_id,phone_num,trunk_num FROM cc_whitelist WHERE vcc_id= ?",
            array($vcc_id)
        );

        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data
        );

        return new JsonResponse($ret);
    }
}
