<?php

namespace Wintel\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class SundryController
 * @package Wintel\RestBundle\Controller
 */
class SundryController extends Controller
{

    /**
     * 号码归属地查询
     * @param $number
     * @return JsonResponse
     */
    public function getnumberlocAction($number)
    {
        $res = $this->container->get("icsoc_data.model.location")->getNumberloc($number);
        return new JsonResponse($res);
    }

//    public function getnumberlocAction($number)
//    {
//        /** @var \Doctrine\DBAL\Connection $conn */
//        $conn = $this->get('doctrine.dbal.default_connection');
//        if (empty($number)) {
//            return new JsonResponse(array('code'=>401, 'message'=>'号码不能为空'));
//        }
//        $number = preg_replace('/[^\d|^\-|^\/]/', '', $number);
//        if (strlen($number) < 11) {
//            $res = array('code'=>402, 'message'=>'请输入完整的号码，固定电话请带上区号');
//            return new JsonResponse($res);
//        }
//        $iData = array('number'=>'','city'=>'','code'=>'','type'=>'');
//
//        //先去除86
//        if (strlen($number) > 12 && substr($number, 0, 2) == "86") {
//            $number = substr($number, 2);
//        }
//        if (strlen($number) == 12 && substr($number, 0, 2) == "01") {
//            $number = substr($number, 1, 11);//先截取11个字符，防止是多个手机号码
//        } elseif (strlen($number) == 13 && substr($number, 0, 3) == "001") {
//            $number = substr($number, 2, 11);//先截取11个字符，防止是多个手机号码
//        } else {
//            $number = substr($number, 0, 13);//截取13个字符，防止是多个手机号码
//        }
//        //座机号以0开头
//        if (substr($number, 0, 1) == "0") {
//            if (substr($number, 0, 2) == "01" || substr($number, 0, 2) == "02") {
//                $tel_code = substr($number, 0, 3);//3位区号
//            } else {
//                $tel_code = substr($number, 0, 4);
//            }
//            $tel_info = $conn->fetchAssoc("SELECT * FROM est_phone_location WHERE code =  '".$tel_code."'");
//            if (!empty($tel_info)) {
//                $iData = array('city'=>$tel_info['city'],'code'=>$tel_info['code'],'type'=>'TEL','number'=>$number);
//            }
//        } else {
//            $tel_seg = substr($number, 0, 7);
//            $tel_info = $conn->fetchAssoc("SELECT * FROM est_phone_location WHERE num =  '".$tel_seg."'");
//            if (!empty($tel_info)) {
//                $iData = array('city'=>$tel_info['city'],'code'=>$tel_info['code'],'type'=>'MOBILE','number'=>$number);
//            }
//        }
//        if (empty($iData['code'])) {
//            return new JsonResponse(array('code'=>'403', 'message'=>'找不到号码段'));
//        } else {
//            return new JsonResponse(array('code'=>'200', 'message'=>'ok', 'data'=>$iData));
//        }
//    }
}
