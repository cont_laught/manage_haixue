<?php
namespace Wintel\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RepsystemController
 * @package Wintel\RestBundle\Controller
 */
class RepsystemController extends Controller
{
    /**
     *  获取半小时列表
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function halfhourAction(Request $request)
    {
        $vcc_id = $request->get("vcc_id", 0);
        $date = $request->get('date', '');
        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空'
            );
            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业ID包含非数字字符'
            );
            return new JsonResponse($ret);
        }
        if (!empty($date)) {
            $row = $this->isDate($date);
            if (!$row['flag']) {
                $ret = array(
                    'code' => 403,
                    'message' => '日期格式不正确'
                );
                return new JsonResponse($ret);
            } else {
                $date = $row['str'];
            }
        } else {
            $date = date("Y-m-d");
        }

        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get('doctrine.dbal.default_connection');
        $data = $conn->fetchAll(
            "SELECT * FROM rep_system_halfhour WHERE vcc_id= :vcc_id AND start_date = :start_date",
            array('vcc_id' => $vcc_id, 'start_date' => $date)
        );
        //处理start_time
        foreach ($data as $k => $v) {
            $hour = floor($v['time_stamp'] / 2);
            if (strlen($hour) <= 1) {
                $hour = '0' . $hour;
            }
            $minute = $v['time_stamp'] % 2 == 1 ? "30" : "00";
            $data[$k]['start_time'] = $v['start_date'] . ' ' . $hour . ':' . $minute;
        }
        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data
        );
        return new JsonResponse($ret);
    }

    /**
     * @param $str
     * @param string $format
     * @return array|bool
     */
    private function isDate($str, $format = "Y-m-d")
    {
        $strArr = explode("-", $str);
        $newArr = array();
        if (empty($strArr)) {
            return false;
        }
        foreach ($strArr as $val) {
            if (strlen($val) < 2) {
                $val = "0" . $val;
            }
            $newArr[] = $val;
        }
        $str = implode("-", $newArr);
        $unixTime = strtotime($str);
        $checkDate = date($format, $unixTime);
        if ($checkDate == $str) {
            return array('flag' => true, 'str' => $str);
        } else {
            return array('flag' => false);
        }
    }
}
