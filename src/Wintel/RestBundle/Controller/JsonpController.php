<?php
namespace Wintel\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class JsonpController
 * @package Wintel\RestBundle\Controller
 */
class JsonpController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse|Response
     * tool_bar 中获取坐席基本信息；
     */
    public function agentViewAction(Request $request)
    {
        $vcc_code = $request->get("vcc_code", 0);
        $ag_num= $request->get('ag_num', '');
        $callbak = $request->get('callback', '');
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        $msg = $vcc_id = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new Response($callbak.'('.json_encode($msg).')');
        }
//       if (!is_numeric($ag_id)) {
//            return new Response($callbak.'('.json_encode(array('code'=>403,'message'=>'ag_id非数字')).')');
//        }
        if (empty($callbak)) {
            return new Response($callbak.'('.json_encode(array('code'=>404,'message'=>'callback为空')).')');
        }
        $sql  = "SELECT a.id,a.ag_name,a.vcc_id,a.ag_num,a.pho_num,a.pho_id,p.pho_type,a.ag_password
                FROM win_agent a
                LEFT JOIN win_phone p
                ON a.pho_num = p.pho_num
                WHERE a.ag_num = :ag_num AND a.vcc_id = :vcc_id AND a.is_del = 0";
        $row = $conn->fetchAssoc($sql, array('ag_num'=>$ag_num, 'vcc_id'=>$vcc_id));
        if (empty($row)) {
            return new Response($callbak.'('.json_encode(array('code'=>405, 'message'=>'坐席不存在')).')');
        }
        //获取技能组；
        $que = $conn->fetchAll(
            "SELECT que_id,skill
            FROM win_agqu
            WHERE ag_id = :ag_id",
            array('ag_id'=>$row['id'])
        );
        $que_id = $que1 = array();
        if (!empty($que)) {
            foreach ($que as $value) {
                if (!$value['skill']) {
                    $value['skill'] = 1;
                }
                $que_id[] = $value['que_id'];
                $que1[] = $value['que_id'] . '-' . $value['skill'];
            }
        }
        if (!empty($que1)) {
            $row['que_id'] = implode(',', $que1);
        } else {
            $row['que_id'] = '';
        }
        if (!empty($que_id)) {
            $que_id1 = implode(',', $que_id);
        } else {
            $que_id1 = '';
        }
        //系统信息
        $system = $conn->fetchAssoc(
            "SELECT system_name,index_page,inpop_page,outpop_page
            FROM cc_ccods
            WHERE vcc_id = :vcc_id ",
            array('vcc_id' => $vcc_id)
        );
        $password = '123456';
        if ($system['inpop_page']) {
            $system['inpop_page'] = str_replace('[_caller]', "'+_caller+'", $system['inpop_page']);
            $system['inpop_page'] = str_replace('[_servnum]', "'+_servnum+'", $system['inpop_page']);
            $system['inpop_page'] = str_replace('[phone400]', "'+phone400+'", $system['inpop_page']);
            $system['inpop_page'] = str_replace('[callid]', "'+callid+'", $system['inpop_page']);
            $system['inpop_page'] = str_replace('[ag_num]', $row['ag_num'], $system['inpop_page']);
            $system['inpop_page'] = str_replace('[pw]', $password, $system['inpop_page']);
            $system['inpop_page'] = str_replace('[vcc_code]', $vcc_code, $system['inpop_page']);
            $system['inpop_page'] = str_replace('[que_id]', $que_id1, $system['inpop_page']);
            $row['inpop_page'] = $system['inpop_page'];
        } else {
            $row['inpop_page'] = '';
        }
        if ($system['outpop_page']) {
            $system['outpop_page'] = str_replace('[_called]', "'+_called+'", $system['outpop_page']);
            $system['outpop_page'] = str_replace('[callid]', "'+callid+'", $system['outpop_page']);
            $system['outpop_page'] = str_replace('[ag_num]', $row['ag_num'], $system['outpop_page']);
            $system['outpop_page'] = str_replace('[pw]', $password, $system['outpop_page']);
            $system['outpop_page'] = str_replace('[vcc_code]', $vcc_code, $system['outpop_page']);
            $system['outpop_page'] = str_replace('[que_id]', $que_id1, $system['outpop_page']);
            $row['outpop_page'] = $system['outpop_page'];
        } else {
            $row['outpop_page'] = '';
        }
        //获得外呼队列
        $que = $conn->fetchAll(
            "SELECT win_queue.que_name,win_queue.id FROM win_queue LEFT JOIN win_agqu ON win_queue.id = win_agqu.que_id
		     WHERE win_queue.is_del=0 AND win_queue.vcc_id = :vcc_id
             AND win_queue.que_type in(0,2) AND win_agqu.ag_id= :ag_id",
            array('ag_id'=>$row['id'], 'vcc_id'=>$vcc_id)
        );
        if (!empty($que)) {
            $row['que'] = $que;
        } else {
            $row['que'] = array();
        }
        /*获取400*/
        $cc_phone = array();
        $phones = $conn->fetchAll(
            "SELECT phone,phone400 from cc_phone400s where vcc_id = :vcc_id",
            array('vcc_id'=>$vcc_id)
        );
        foreach ($phones as $value) {
            $cc_phone[$value['phone']] = $value['phone400'];
        }
        $row['cc_phone'] = $cc_phone;
        $data = array('code'=>200, 'message'=>'success', 'data'=>$row);
        return new Response($callbak.'('.json_encode($data).')');
    }

    /**
     * @param Request $request
     * @return Response
     * 内呼；咨询坐席；
     */
    public function callinnerAction (Request $request)
    {
        $vcc_code = $request->get("vcc_code", 0);
        $callbak = $request->get('jsonpcallback', '');
        $que_id = $request->get('que_id', '');
        $ag_id = $request->get('ag_id', '');
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        $msg = $vcc_id = $this->get('validator.custom')->checkVccCode($vcc_code);
        //echo $que_id.'xxcccxx';
        if (!empty($msg) && is_array($msg)) {
            return new Response($callbak.'('.json_encode($msg).')');
        }
        $que = $conn->fetchAll(
            "SELECT id,que_name FROM win_queue WHERE is_del = 0 AND vcc_id = :vcc_id",
            array('vcc_id'=>$vcc_id)
        );
        $row = array();
        if (!empty($que)) {
            $row['que'] = $que;
        } else {
            $row['que'] = array();
        }
        $phones = $conn->fetchAll(
            "SELECT phone400 FROM cc_phone400s WHERE vcc_id = :vcc_id ",
            array('vcc_id'=>$vcc_id)
        );
        $num = array();
        if (!empty($phones)) {
            foreach ($phones as $value) {
                $num[] = $value['phone400'];
            }
        }
        $row['num'] = $num;
        if (!$que_id) {
            $agent_info = $conn->fetchAll(
                "SELECT id,ag_num,ag_name,ag_sta
                 FROM win_agent
                 WHERE ag_sta > 0 AND id <> :ag_id AND vcc_id = :vcc_id AND is_del = 0 ",
                array('ag_id'=>$ag_id, 'vcc_id'=>$vcc_id)
            );
        } else {

            $agent_info = $conn->fetchAll(
                "SELECT win_agent.id,win_agent.ag_num,win_agent.ag_name,win_agent.ag_sta
		         FROM win_agent LEFT JOIN win_agqu ON win_agent.id = win_agqu.ag_id
		         WHERE win_agent.ag_sta>0 AND win_agent.id <> :ag_id AND win_agent.is_del = 0
                 AND win_agent.vcc_id = :vcc_id AND win_agqu.que_id = :que_id ",
                array('ag_id'=>$ag_id, 'vcc_id'=>$vcc_id, 'que_id'=>$que_id)
            );
        }
        $que_info = $conn->fetchAll(
            "SELECT win_agqu.ag_id,win_queue.que_name
             FROM win_agqu
		     LEFT JOIN win_queue
		     ON win_queue.is_del = 0 AND win_agqu.que_id = win_queue.id AND win_queue.vcc_id = :vcc_id ",
            array('vcc_id'=>$vcc_id)
        );
        $arr_que = array();
        foreach ($que_info as $temp_array) {
            $tmp_ag = $temp_array['ag_id'];
            $tmp_que = $temp_array['que_name'];
            if (isset($arr_que[$tmp_ag])) {
                $arr_que[$tmp_ag] .= $tmp_que.",";
            } else {
                $arr_que[$tmp_ag] = $tmp_que;
            }
        }
        if (!empty($agent_info)) {
            foreach ($agent_info as $key => $temp_agent) {
                $agent_info[$key]['ag_queue'] = isset($arr_que[$temp_agent['id']]) ? $arr_que[$temp_agent['id']] : '';
            }
        } else {
            $agent_info = array();
        }
        $row['agent_info'] = $agent_info;
        $data = array('code'=>200, 'message'=>'success', 'data'=>$row);
        return new Response($callbak.'('.json_encode($data).')');
    }

    /**
     * @param Request $request
     * @return Response
     * 外呼
     */
    public function calloutAction(Request $request)
    {
        $vcc_code = $request->get("vcc_code", 0);
        $ag_id = $request->get('ag_id', '');
        $type = $request->get("type", 0);
        $number = $request->get('num', '');
        $callbak = $request->get('jsonpcallback', '');
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        $msg = $vcc_id = $this->get('validator.custom')->checkVccCode($vcc_code);
        //echo $que_id.'xxcccxx';
        if (!empty($msg) && is_array($msg)) {
            return new Response($callbak.'('.json_encode($msg).')');
        }
        $row = array();
        //获得外呼队列
        $que = $conn->fetchAll(
            "SELECT win_queue.que_name,win_queue.id FROM win_queue LEFT JOIN win_agqu ON win_queue.id = win_agqu.que_id
		     WHERE win_queue.is_del=0 AND win_queue.vcc_id = :vcc_id
             AND win_queue.que_type in(0,2) AND win_agqu.ag_id= :ag_id",
            array('ag_id'=>$ag_id, 'vcc_id'=>$vcc_id)
        );
        if (!empty($que)) {
            $row['que'] = $que;
        } else {
            $row['que'] = array();
        }
        //取得外呼的主叫
        $num = $this->phoneNumber($vcc_id);
        $row['num'] = $num;
        if ($type != 1) {
            $res = $this->getPhonenum($number);
            $row['number'] = $res['tel'];
        } else {
            $row['number'] = $number;
        }
        $data = array('code'=>200, 'message'=>'success', 'data'=>$row);
        return new Response($callbak.'('.json_encode($data).')');
    }

    /**
     * @param Request $request
     * @return Response
     * 咨询外线
     */
    public function consultouterAction(Request $request)
    {
        $vcc_code = $request->get("vcc_code", 0);
        $callbak = $request->get('jsonpcallback', '');
        $msg = $vcc_id = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new Response($callbak.'('.json_encode($msg).')');
        }
        $row = array();
        //取得外呼的主叫
        $num = $this->phoneNumber($vcc_id);
        $row['num'] = $num;
        $data = array('code'=>200, 'message'=>'success', 'data'=>$row);
        return new Response($callbak.'('.json_encode($data).')');
    }

    /**
     * @param Request $request
     * @return Response
     *  监听
     */
    public function chanspyAction(Request $request)
    {
        $vcc_code = $request->get("vcc_code", 0);
        $callbak = $request->get('jsonpcallback', '');
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        $msg = $vcc_id = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new Response($callbak.'('.json_encode($msg).')');
        }
        $row = array();
        $agent = $conn->fetchAll(
            "SELECT ag_id,ag_num,ag_name,pho_num,sec_to_time(unix_timestamp()-pho_sta_time)as ag_time
            FROM win_agmonitor
            WHERE pho_sta = 2 AND vcc_id = :vcc_id",
            array('vcc_id'=>$vcc_id)
        );
        if (!empty($agent)) {
            $row['agent_info'] = $agent;
        } else {
            $row['agent_info'] = array();
        }
        $data = array('code'=>200, 'message'=>'success', 'data'=>$row);
        return new Response($callbak.'('.json_encode($data).')');
    }

    /**
     * @param Request $request
     * @return Response
     * 号码归属地；
     */
    public function nbattributeAction(Request $request)
    {
        $num = $request->get('num', '');
        $callbak = $request->get('jsonpcallback', '');
        $city = $this->getPhoneAttribute($num);
        $row['city'] = $city;
        $data = array('code'=>200, 'message'=>'success', 'data'=>$row);
        return new Response($callbak.'('.json_encode($data).')');
    }

    /**
     * @param Request $request
     * @return Response
     * 获取user_id
     */
    public function getUserIdAction(Request $request)
    {
        $vcc_code = $request->get("vcc_code", 0);
        $user_num = $request->get("user_num", 0);
        $callbak = $request->get('jsonpcallback', '');
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        $msg = $vcc_id = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new Response($callbak.'('.json_encode($msg).')');
        }
        if (empty($user_num)) {
            $res = array("code"=>403,"message"=>"user_num不能为空");
            return new Response($callbak.'('.json_encode($res).')');
        }
        $id = $conn->fetchColumn(
            "SELECT id
            FROM win_agent
            WHERE vcc_id = :vcc_id AND ag_num = :ag_num ",
            array('vcc_id'=>$vcc_id,'ag_num'=>$user_num)
        );
        if (!$id) {
            $res = array("code"=>404,"message"=>"坐席工号不存在");
            return new Response($callbak.'('.json_encode($res).')');
        } else {
            $res = array("code"=>200,"data"=>array('user_id'=>$id));
            return new Response($callbak.'('.json_encode($res).')');
        }
    }

    /**
     * @param Request $request
     * @return Response
     * 处理号码 去除本地号码前缀  加上外地手机前缀
     */
    public function dealnumberAction(Request $request)
    {
        $number = $request->get("number", 0);
        $callbak = $request->get('jsonpcallback', '');
        $local_code = $request->get('local_code', '');
        $phone_num = $this->removePrefixZero($number);
        $res = $this->getNumberloc($phone_num);
        if ($res['iRet'] == 0) {
            $data = $res['iData'];
            $code = $data['code'];
            $type = $data['type'];
            $mobile_prefix = $this->container->getParameter('mobile_prefix');
            $dealed_number = '';
            if ($type == 'MOBILE' && $code != $local_code) {
                $dealed_number = $mobile_prefix.$phone_num;
            } elseif ($type == 'TEL' && $code == $local_code) {
                //去除开始处的区号
                if (stripos($phone_num, $local_code) === 0) {
                    $dealed_number = substr($phone_num, strlen($local_code));
                }
            } else {
                $dealed_number = $phone_num;
            }
            $res = array('code'=>200, 'data'=>array('deal_number'=>$dealed_number));
            return new Response($callbak.'('.json_encode($res).')');
        } else {
            $res = array('code'=>401, 'message'=>$res['iMsg']);
            return new Response($callbak.'('.json_encode($res).')');
            //make_json_error($res['iMsg']);
        }

    }

    /**
     * 判断号码归属地，外地手机号码加0，其他号码不处理，本地固定电话去掉区号
     */
    private function getPhonenum($tel)
    {

        $localCode = $this->container->getParameter("local_code");
        $tel=$this->getPhonetype($tel);//规范电话号码格式
        $res = $this->container->get("icsoc_data.model.location")->getNumberloc($tel);
        $data = array();
        if (isset($res['code']) && $res['code'] == 200) {
            $data['attribute'] = $res['data']['code'] != $localCode ? '外地号码' : '本地号码';
            if ($res['data']['code'] != $localCode && $res['data']['type'] == 'MOBILE') {
                $data['tel'] = '0'.$res['data']['realPhone'];
            } else {
                $data['tel'] = $res['data']['code'].$res['data']['realPhone'];
            }
        } else {
            $data['tel'] = $tel;
            $data['attribute'] = "<span color='red'>特殊号码</span>";
        }
        return $data;
    }

    /**
     * 规范电话号码格式去掉特殊字符，手机号码去掉前面的0其他号码不做处理
     */
    private function getPhonetype($tel)
    {
        if (isset($tel)) {
            $tel = $this->makeSemiangle(trim($tel));
            $tel = str_replace('-', '', $tel);
            $tel = str_replace('_', '', $tel);
        } else {
            return false;
        }
        //去掉手机号码前面的0其他号码不做处理
        if (strlen($tel) == 12 && preg_match("/^13[0-9]{9}|15[0-9]{9}|18[0-9]{9}$/i", substr($tel, 1, 11))) {
            $tel = substr($tel, 1, 11);
        }
        return $tel;
    }

    /**
     *  将一个字串中含有全角的数字字符、字母、空格或'%+-()'字符转换为相应半角字符
     *
     * @access  public
     * @param   string       $str         待转换字串
     *
     * @return  string       $str         处理后字串
     */
    private function makeSemiangle($str)
    {
        $arr = array('０' => '0', '１' => '1', '２' => '2', '３' => '3', '４' => '4',
            '５' => '5', '６' => '6', '７' => '7', '８' => '8', '９' => '9',
            'Ａ' => 'A', 'Ｂ' => 'B', 'Ｃ' => 'C', 'Ｄ' => 'D', 'Ｅ' => 'E',
            'Ｆ' => 'F', 'Ｇ' => 'G', 'Ｈ' => 'H', 'Ｉ' => 'I', 'Ｊ' => 'J',
            'Ｋ' => 'K', 'Ｌ' => 'L', 'Ｍ' => 'M', 'Ｎ' => 'N', 'Ｏ' => 'O',
            'Ｐ' => 'P', 'Ｑ' => 'Q', 'Ｒ' => 'R', 'Ｓ' => 'S', 'Ｔ' => 'T',
            'Ｕ' => 'U', 'Ｖ' => 'V', 'Ｗ' => 'W', 'Ｘ' => 'X', 'Ｙ' => 'Y',
            'Ｚ' => 'Z', 'ａ' => 'a', 'ｂ' => 'b', 'ｃ' => 'c', 'ｄ' => 'd',
            'ｅ' => 'e', 'ｆ' => 'f', 'ｇ' => 'g', 'ｈ' => 'h', 'ｉ' => 'i',
            'ｊ' => 'j', 'ｋ' => 'k', 'ｌ' => 'l', 'ｍ' => 'm', 'ｎ' => 'n',
            'ｏ' => 'o', 'ｐ' => 'p', 'ｑ' => 'q', 'ｒ' => 'r', 'ｓ' => 's',
            'ｔ' => 't', 'ｕ' => 'u', 'ｖ' => 'v', 'ｗ' => 'w', 'ｘ' => 'x',
            'ｙ' => 'y', 'ｚ' => 'z',
            '（' => '(', '）' => ')', '〔' => '[', '〕' => ']', '【' => '[',
            '】' => ']', '〖' => '[', '〗' => ']', '“' => '[', '”' => ']',
            '‘' => '[', '’' => ']', '｛' => '{', '｝' => '}', '《' => '<',
            '》' => '>',
            '％' => '%', '＋' => '+', '—' => '-', '－' => '-', '～' => '-',
            '：' => ':', '。' => '.', '、' => ',', '，' => '.', '、' => '.',
            '；' => ',', '？' => '?', '！' => '!', '…' => '-', '‖' => '|',
            '”' => '"', '’' => '`', '‘' => '`', '｜' => '|', '〃' => '"',
            '　' => ' ');
        return strtr($str, $arr);
    }

    /**
     * @param $vcc_id
     * @return array
     * 取得主叫外呼号码；
     */
    private function phoneNumber($vcc_id)
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        //取得外呼的主叫
        $result = $conn -> fetchAll(
            "SELECT phone400,phone,display_type
            FROM cc_phone400s
            WHERE vcc_id = :vcc_id",
            array('vcc_id'=>$vcc_id)
        );
        $phone400 = array();
        $phone = array();
        foreach ($result as $value) {
            switch ($value['display_type']) {
                case 0:
                    break;
                case 1:
                    $phone[] = $value['phone'];
                    break;
                case 2:
                    $phone400[] = $value['phone400'];
                    break;
                case 3:
                    $phone400[] = $value['phone400'];
                    $phone[] = $value['phone'];
                    break;
                default:
                    break;
            }
        }
        $num = array_merge($phone, $phone400);
        return $num;
    }

    /**
     * @param $tel
     * @return string
     */
    private function getPhoneAttribute($tel)
    {
        $res = $this->getNumberloc($tel);
        if (isset($res['code']) && $res['code'] == 200) {
            return $res['data']['city'];
        }
        return "";
    }

    /**
     * 处理电话号码 -  座机号保持不变，手机号去掉前面的0和86
     *
     * @param string $tel   电话号码
     * @return string
     */
    public function removePrefixZero($tel)
    {
        //过滤电话号码，只留下 数字、-和/
        $tel = preg_replace('/[^\d|^\-|^\/]/', '', $tel);

        if (strlen($tel) < 7) {
            return $tel;
        }

        //先去除86
        if (strlen($tel) > 12 && substr($tel, 0, 2) == "86") {
            $tel = substr($tel, 2);
        }
        if (strlen($tel) == 12 && substr($tel, 0, 2) == "01") {
            $tel = substr($tel, 1, 11);//先截取11个字符，防止是多个手机号码
        } elseif (strlen($tel) == 13 && substr($tel, 0, 3) == "001") {
            $tel = substr($tel, 2, 11);//先截取11个字符，防止是多个手机号码
        } else {
            $tel = substr($tel, 0, 13);//截取13个字符，防止是多个手机号码
        }
        return $tel;
    }

    /**
     * 获取号码归属地
     * @param string $number
     * @return mixed
     */
    public function getNumberloc($number = '')
    {
        $res = $this->container->get("icsoc_data.model.location")->getNumberloc($number);
        if ($res['code'] != '200') {
            $retarr['iRet'] = 1;
            $retarr['iMsg'] = $res['message'];
            return $retarr;
        }
        $retarr['iRet'] = 0;
        $retarr['iMsg'] = 'success';
        $retarr['iData'] = $res['data'];
        return $retarr;
    }


    /**
     * @param Request $request
     * @return Response
     * amq签入方式 jsonp;
     */
    public function consumerAction(Request $request)
    {
        $callback = $request->get("callback", '');
        $linkid = $request->get("linkid", '');
        $timeout = $request->get("timeout", 20);
        //$ctiip = $request->get("ctiip", '');
        $queue  = '/queue/wintel.toag';
        $activemq = $this->container->getParameter("activemq");
        /* connection */
        try {
            $stomp = new \Stomp("tcp://".$activemq['hostname'].":".$activemq['port']);
            $stomp->setReadTimeout($timeout);
            if ($linkid == 0) {
                //订阅消息(若linkid为0则先获取linkid)
                $stomp->subscribe($queue, array('selector'=>'linkid=0'));
            } else {
                //否则则订阅该linkid的消息
                $stomp->subscribe($queue, array('selector'=>'linkid='.$linkid));
            }
            if ($frame = $stomp->readFrame()) {
                if ($frame) {
                    $stomp->ack($frame);
                    $linkstate = isset($frame->headers['linkstate']) ? $frame->headers['linkstate'] : '';
                    $str = $callback.'({"linkstate":'.$linkstate.',"body":'.$frame->body.'})';
                    return new Response($str);
                }
            } else {
                $str = $callback.'('.json_encode(array()).')';
                return new Response($str);
            }
            unset($stomp);
        } catch (StompException $e) {
            $this->get("logger")->error('amq错误:['.date('Y-m-d H:i:s').']'.' Connection failed: ' . $e->getMessage());
            return new Response('Connection failed: ' . $e->getMessage());
        }
        return new Response('');
    }

    /**
     * @param Request $request
     * @return Response
     * amq签入方式 jsonp;
     */
    public function producerAction(Request $request)
    {
        $callback = $request->get("callback", '');
        $message = $request->get("content", '');
        $linkid = $request->get("linkid", 20);
        //$ctiip = $request->get("ctiip", '');
        $queue  = '/queue/wintel.toapp';
        $header = array();
        $header['linkip']     = $request->getClientIp();
        $header['linkid']     = $linkid;
        $activemq = $this->container->getParameter("activemq");
        try {
            $stomp = new \Stomp("tcp://".$activemq['hostname'].":".$activemq['port']);
            $stomp->send($queue, $message, $header);
            unset($stomp);
        } catch (StompException $e) {
            $this->get("logger")->error('amq错误:['.date('Y-m-d H:i:s').']'.' Connection failed: ' . $e->getMessage());
            return new Response('Connection failed: ' . $e->getMessage());
        }
        $str =  $callback.'({"ret":0})';
        return new Response($str);
    }
}
