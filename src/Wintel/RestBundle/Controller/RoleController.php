<?php

namespace Wintel\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgentController
 * @package Wintel\RestBundle\Controller
 */
class RoleController extends Controller
{
    /**
     * @var array
     * 角色等级 1=》主管，2=》组长，3=》员工
     */
    protected $role_grade = array(1,2,3);

    /**
     * @param $vcc_code
     * @param $name
     * @param $role_grade
     * @return array|JsonResponse
     * 添加角色
     */
    public function addAction($vcc_code, $name, $role_grade)
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");

        //vcc_code 验证
        $msg = $vid = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证name
        $msg = $this->get('validator.custom')->isEmptyName($name, 403, '角色名称为空');
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证角色名称是否重复
        $where = " vcc_id = :vcc_id AND name = :name ";
        $par_arr = array('vcc_id' => $vid, 'name' => $name);
        $msg = $this->get('validator.custom')->existRole($where, $par_arr, '404');
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证角色等级
        if (!in_array($role_grade, $this->role_grade)) {
            $ret = array('code' => 405, 'message' => '角色等级有误');
            return new JsonResponse($ret);
        }

        //入库
        $data = array('vcc_id' => $vid, 'name' => $name, 'role_grade' => $role_grade);
        $conn->insert('cc_roles', $data);
        $lastId = $conn->lastInsertId();
        return new JsonResponse(array('code' => 200, 'message' => 'ok', 'role_id'=>$lastId));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * 修改角色
     */
    public function editAction(Request $request)
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        $vcc_code = $request->get('vcc_code', '');
        $name = $request->get('name', '');
        $role_grade = $request->get('role_grade');
        $role_id = $request->get('role_id', '');

        //vcc_code 验证
        $msg = $vid = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证name
        $msg = $this->get('validator.custom')->isEmptyName($name, 403, '角色名称为空');
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证角色是否属于 该企业
        $msg = $this->get('validator.custom')->vccRole($vid, $role_id);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证角色名称是否重复
        $where = " vcc_id = :vcc_id AND name = :name AND role_id <> :role_id";
        $par_arr = array('vcc_id' => $vid, 'name' => $name, 'role_id' => $role_id);
        $msg = $this->get('validator.custom')->existRole($where, $par_arr, '405');
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证角色等级
        if (!in_array($role_grade, $this->role_grade)) {
            $ret = array('code' => 406, 'message' => '角色等级有误');
            return new JsonResponse($ret);
        }

        //入库
        try {
            if (empty($role_grade)) {
                $data = array('name' => $name);
            } else {
                $data = array('name' => $name, 'role_grade' => $role_grade);
            }

            $conn->update('cc_roles', $data, array('vcc_id' => $vid, 'role_id' => $role_id));

            return new JsonResponse(array('code' => 200, 'message' => 'ok'));
        } catch (Exception $e) {
            return new JsonResponse(array('code' => 407, 'message' => '修改失败【'.$e->getMessage().'】'));
        }
    }

    /**
     * @param $vcc_code
     * @return JsonResponse
     * 获取角色列表
     */
    public function listAction($vcc_code)
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        //vcc_code 验证
        $msg = $vid = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        $list = $conn->fetchAll(
            'SELECT role_id,name,role_grade,action_list '.
            'FROM cc_roles '.
            'WHERE vcc_id = :vcc_id',
            array('vcc_id' => $vid)
        );
        return new JsonResponse(array('code'=>'200','message'=>'ok','data'=>$list));
    }

    /**
     * @param $vcc_code
     * @param $role_id
     * @return JsonResponse
     * 删除角色
     */
    public function deleteAction($vcc_code, $role_id)
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");

        //vcc_code 验证
        $msg = $vid = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证角色是否属于 该企业
        $msg = $this->get('validator.custom')->vccRole($vid, $role_id, 403);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证角色下有无坐席；
        $count = $conn->fetchColumn(
            'SELECT count(*) '.
            'FROM win_agent '.
            'WHERE vcc_id = :vcc_id AND user_role = :role_id AND is_del = 0',
            array('vcc_id' => $vid, 'role_id' => $role_id)
        );
        if (!empty($count)) {
            $ret = array('code'=>404,'message'=>'角色下有坐席');
            return new JsonResponse($ret);
        }

        $conn->delete('cc_roles', array('vcc_id' => $vid, 'role_id' => $role_id));
        return new JsonResponse(array('code'=>'200', 'message'=>'ok'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * 给角色分配权限；
     */
    public function permissionsAction(Request $request)
    {
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get("doctrine.dbal.default_connection");
        $vcc_code = $request->get('vcc_code', '');
        $role_id = $request->get('role_id', '');
        $action_list = $request->get('action_list', '');

        //vcc_code 验证
        $msg = $vid = $this->get('validator.custom')->checkVccCode($vcc_code);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证角色是否属于 该企业
        $msg = $this->get('validator.custom')->vccRole($vid, $role_id, 403);
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //验证action_list
        $msg = $this->get('validator.custom')->isEmptyName($action_list, 404, '权限为空');
        if (!empty($msg) && is_array($msg)) {
            return new JsonResponse($msg);
        }

        //入库
        $data = array('action_list' => $action_list);
        try {
            $conn->update('cc_roles', $data, array('vcc_id' => $vid, 'role_id' => $role_id));

            return new JsonResponse(array('code' => 200, 'message' => 'ok'));
        } catch (Exception $e) {
            return new JsonResponse(array('code' => 405, 'message' => '分配权限失败【'.$e->getMessage().'】'));
        }
    }
}
