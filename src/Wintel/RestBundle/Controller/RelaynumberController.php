<?php
namespace Wintel\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RepsystemController
 * @package Wintel\RestBundle\Controller
 */
class RelaynumberController extends Controller
{
    /**
     * 获取企业绑定的中继号码接口
     * @param $vcc_id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getphoneAction($vcc_id)
    {
        if (empty($vcc_id)) {
            $ret = array(
                'code' => 401,
                'message' => '企业ID为空'
            );

            return new JsonResponse($ret);
        }

        if (!is_numeric($vcc_id)) {
            $ret = array(
                'code' => 402,
                'message' => '企业ID包含非数字字符'
            );

            return new JsonResponse($ret);
        }

        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get('doctrine.dbal.default_connection');
        $data = $conn->fetchAll(
            "SELECT `phone_id`,`phone`,`phone400` FROM cc_phone400s WHERE vcc_id= :vcc_id",
            array('vcc_id' => $vcc_id)
        );

        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data
        );

        return new JsonResponse($ret);
    }
}
