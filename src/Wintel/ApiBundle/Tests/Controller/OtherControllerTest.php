<?php

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class OtherControllerTest
 * @package Wintel\ApiBundle\Tests\Controller
 */
class OtherControllerTest extends WebTestCase
{

    private $client;

    public function __construct()
    {
        $this->client =  static::createClient();
    }

    /**
     * 测试获取中继号
     */
    public function testGetPhone()
    {
        $url = "/newwintelapi/api/other/relaynumber";
        $this->commonTest('GET', $url);
        $this->client->request('GET', $url, array('vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 公共测试方法
     */
    private function commonTest($type, $url)
    {
        $this->client->request($type, $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
