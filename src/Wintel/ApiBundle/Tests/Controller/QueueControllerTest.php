<?php

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class QueueControllerTest
 * @package Wintel\ApiBundle\Tests\Controller
 */
class QueueControllerTest extends WebTestCase
{
    private $client;

    public function __construct()
    {
        $this->client =  static::createClient();
    }

    /**
     * 测试添加技能组
     */
    public function testAdd()
    {
        $url = "/newwintelapi/api/queue/add";
        $this->commonTest("POST", $url);

        //技能组名称为空
        $param = array(
            'vcc_code'=>'ekt',
            'que_name'=>'',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组名称为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组名称重复
        $param = array(
            'vcc_code'=>'ekt',
            'que_name'=>'hello,worldxx',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '技能组已经存在',
        );
        //echo $response;
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 技能组优先级的值为0-255之间数字
        $param = array(
            'vcc_code'=>'ekt',
            'que_name'=>'hello,worldxxxxyyzz',
            'que_priority'=>'2666',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '技能组优先级的值为0-255之间数字',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code'=>'ekt',
            'que_name'=>'我是谁cc',
            'que_tag' => '非常棒vvvvvv',
            'que_type' => '0',
            'que_length' => '21',
            'que_time' => '52',
            'ring_time' => '33',
            'next_wait' => '10',
            'b_announce' => '1',
            'noans_times' => '3',
            'noans_wait' => '24',
            'wait_audio' => '2',
            'que_strategy' => 2
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试修改
     */
    public function testEdit()
    {
        $url = "/newwintelapi/api/queue/update";
        $this->commonTest("POST", $url);

        //技能组名称为空
        $param = array(
            'vcc_code'=>'ekt',
            'que_name'=>'',
            'que_id'=> 1,
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组名称为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404 技能组不属于该企业
        $param = array(
            'vcc_code'=>'ekt',
            'que_name'=>'cccc',
            'que_id'=> 10000,
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '技能组不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组名称重复
        $param = array(
            'vcc_code'=>'ekt',
            'que_name'=>'客服',
            'que_id'=> 2,
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '技能组已经存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 技能组优先级的值为0-255之间数字
        $param = array(
            'vcc_code'=>'ekt',
            'que_name'=>'hello,worldxxxxyyzz',
            'que_priority'=>'2666',
            'que_id'=> 2,
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '技能组优先级的值为0-255之间数字',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code'=>'ekt',
            'que_name'=>'hello',
            'que_tag' => '非常棒cc',
            'que_type' => '1',
            'que_length' => '121',
            'que_time' => '152',
            'ring_time' => '133',
            'next_wait' => '110',
            'b_announce' => '11',
            'noans_times' => '13',
            'noans_wait' => '124',
            'wait_audio' => '12',
            'que_id'=> 6246,
            'que_strategy'=> 1,
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 其他写过；停电不知道怎么就没了；
     */
    public function testCancelAssign()
    {
        $url = "/newwintelapi/api/queue/cancel_assign";
        $this->commonTest("POST", $url);

        //技能组ID为空
        $param = array(
            'vcc_code'=>'ekt',
            'que_id'=>'',
            'agents'=> '[1,2,3]',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '技能组ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组不属于该企业
        $param = array(
            'vcc_code'=>'ekt',
            'que_id'=>'1111111',
            'agents'=> '[1,2,3]',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409 agents 为空
        $param = array(
            'vcc_code'=>'ekt',
            'que_id'=>'6242',
            'agents'=> '',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'agents参数为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405 不是 JSON 格式
        $param = array(
            'vcc_code'=>'ekt',
            'que_id'=>'6242',
            'agents'=> 'cc',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => 'agents非JSON格式',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok ;
        $param = array(
            'vcc_code'=>'ekt',
            'que_id'=>'6242',
            'agents'=> '[2032,2805
            ,"xx"]',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');

    }

    /**
     * 公共测试方法
     */
    private function commonTest($type, $url)
    {
        $this->client->request($type, $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
