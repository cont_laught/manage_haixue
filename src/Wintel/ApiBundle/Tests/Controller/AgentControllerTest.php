<?php

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AgentControllerTest
 * @package Wintel\ApiBundle\Tests\Controller
 */
class AgentControllerTest extends WebTestCase
{
    private $client;

    public function __construct()
    {
        $this->client =  static::createClient();
    }

    /**
     * 测试签入
     */
    public function testLogin()
    {
        $this->commonTest("POST", "/newwintelapi/api/agent/login");

        //416 json
        $param = array(
            'vcc_code'=>'ekt',
            'data' => 'xx',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/login', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 417,
            'message' => 'data格式非json',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404 data 为空
        $param = array(
            'vcc_code'=>'ekt',
            'data' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/login', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '参数中详细数据data为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //500总错误；
        $param = array(
            'vcc_code'=>'ekt',
            'data' => ' [{"phone":"13811112222"},{"ag_num":"51","phone":"8000"}]',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/login', $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }


    /**
     * 测试签出
     */
    public function testLogout()
    {
        $this->commonTest("POST", "/newwintelapi/api/agent/logout");

        //416 json
        $param = array(
            'vcc_code'=>'ekt',
            'data' => 'xx',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/logout', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 417,
            'message' => 'data格式非json',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404 data 为空
        $param = array(
            'vcc_code'=>'ekt',
            'data' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/logout', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '参数中详细数据data为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //500总错误；
        $param = array(
            'vcc_code'=>'ekt',
            'data' => ' [{"phone":"13811112222"},{"ag_num":"51","phone":"8000"}]',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/logout', $param);
        $response = $this->client->getResponse()->getContent();
        echo $response;
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试删除
     */
    public function testDel()
    {
        $this->client->request("POST", "/newwintelapi/api/agent/delete", array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //416 json
        $param = array(
            'vcc_code'=>'ekt',
            'ag_ids' => 'xx',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/delete', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => 'ag_ids格式非json',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403 data 为空
        $param = array(
            'vcc_code'=>'ekt',
            'ag_ids' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/delete', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ag_ids为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //500总错误；
        $param = array(
            'vcc_code'=>'ekt',
            'ag_ids' => ' [1,"zz",2577]',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/delete', $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试修改；
     */
    public function testEditAgent()
    {
        $this->client->request("POST", "/newwintelapi/api/agent/edit", array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
        //403 坐席id为空
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/edit', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404 data 为空
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => 'xx',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/edit', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //406坐席类型
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'ag_role' => '15', //普通,班长，非
        );
        $this->client->request('POST', '/newwintelapi/api/agent/edit', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '坐席类型不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'ag_role' => '1', //普通,班长，非
            'user_role'=>'1000',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/edit', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '角色不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200；
        $param = array(
            'ag_name' => 'updateName',
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'ag_role' => '1', //普通,班长，非
            'user_role'=>'1',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/edit', $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试添加；
     */
    public function testAddAgent()
    {
        $this->client->request("POST", "/newwintelapi/api/agent/add", array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407
        $param = array(
            'vcc_code'=>'ekt',
            'ag_num' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/add', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '坐席工号为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409
        $param = array(
            'vcc_code'=>'ekt',
            'ag_num' => '51',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/add', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '坐席工号已经存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //411
        $param = array(
            'vcc_code'=>'ekt',
            'ag_num' => '13456',
            'ag_password'=>'',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/add', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 411,
            'message' => '坐席密码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //412
        $param = array(
            'vcc_code'=>'ekt',
            'ag_num' => '13456',
            'ag_password'=>md5('123456'),
            'ag_role' => '15', //普通,班长，非
        );
        $this->client->request('POST', '/newwintelapi/api/agent/add', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 412,
            'message' => '坐席类型不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //413
        $param = array(
            'vcc_code'=>'ekt',
            'ag_num' => '13456',
            'ag_password'=>md5('123456'),
            'ag_role' => '1', //普通,班长，非
            'user_role'=>'1000',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/add', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 413,
            'message' => '角色不属于该企业',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //416
        $param = array(
            'vcc_code'=>'ekt',
            'ag_num' => '13456',
            'ag_password'=>md5('123456'),
            'belong_queues'=>'xx',
            'ag_role' => '1', //普通,班长，非
        );
        $this->client->request('POST', '/newwintelapi/api/agent/add', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 416,
            'message' => 'belong_queues非json格式',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200；
        $param = array(
            'ag_name' => 'addName',
            'vcc_code'=>'ekt',
            'ag_num' => '8000002',
            'ag_password'=>md5('123456'),
            'ag_role' => '1', //普通,班长，非
            'user_role'=>'1',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/add', $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试获取坐席列表
     */
    public function testList()
    {
        $this->client->request("POST", "/newwintelapi/api/agent/list", array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        $param = array(
            'vcc_code'=>'ekt',
            'info' => 'xx00',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/list', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => 'info格式非json',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => 'xx00',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/list', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $param = array(
            'vcc_code'=>'ekt',
            'info'=>'{"pagination":{"rows":2,"page":"5"}}'
        );
        $this->client->request('POST', '/newwintelapi/api/agent/list', $param);
        $response =  $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试修改坐席密码；
     */
    public function testUpdatePass()
    {
        $this->client->request("POST", "/newwintelapi/api/agent/password/update", array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/password/update', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => 'xxoo',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/password/update', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '坐席ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'old_password'=>'',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/password/update', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '旧密码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'old_password'=>md5('123456'),
            'new_password'=>'',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/password/update', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '新密码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'old_password'=>md5('123456'),
            'new_password'=>md5('123456'),
        );
        $this->client->request('POST', '/newwintelapi/api/agent/password/update', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '新密码和原始密码一样，无需修改',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //406
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'old_password'=>md5('1234567'),
            'new_password'=>md5('123456'),
        );
        $this->client->request('POST', '/newwintelapi/api/agent/password/update', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '旧密码错误',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'old_password'=>md5('123456'),
            'new_password'=>md5('1234567'),
        );
        $this->client->request('POST', '/newwintelapi/api/agent/password/update', $param);
        $response =  $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试坐席登陆
     */
    public function testSignin()
    {
        $this->client->request("POST", "/newwintelapi/api/agent/signin", array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403
        $param = array(
            'vcc_code'=>'ekt',
            'ag_num' => '',
            'password'=>md5('123456'),
        );
        $this->client->request('POST', '/newwintelapi/api/agent/signin', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席工号为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        $param = array(
            'vcc_code'=>'ekt',
            'ag_num' => '51',
            'password'=>'',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/signin', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '密码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405
        $param = array(
            'vcc_code'=>'ekt',
            'ag_num' => '51',
            'password'=>'11',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/signin', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '验证失败,没有匹配到相应数据',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $param = array(
            'vcc_code'=>'ekt',
            'ag_num' => '51',
            'password'=>md5('123456'),
        );
        $this->client->request('POST', '/newwintelapi/api/agent/signin', $param);
        $response =  $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 获取空闲坐席
     */
    public function testFree()
    {
        $this->client->request("GET", "/newwintelapi/api/agent/free", array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $this->client->request("GET", "/newwintelapi/api/agent/free", array('vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 获取通话的坐席
     */
    public function testLine()
    {
        $this->client->request("GET", "/newwintelapi/api/agent/ontheline", array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $this->client->request("GET", "/newwintelapi/api/agent/ontheline", array('vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试设置转接转接电话
     */
    public function testAgexPhone()
    {
        $this->client->request("POST", "/newwintelapi/api/agent/agextphone", array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/agextphone', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
//        $param = array(
//            'vcc_code'=>'ekt',
//            'ag_id' => 'xx',
//        );
//        $this->client->request('POST', '/newwintelapi/api/agent/agextphone', $param);
//        $response =  $this->client->getResponse()->getContent();
//        $result = array(
//            'code' => 404,
//            'message' => '坐席ID包含非数字字符',
//        );
//        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'phone' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/agextphone', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '手机号为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //406
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'phone' => '1xxx',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/agextphone', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '手机号码含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'phone' => '123456',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/agextphone', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '手机号码不是11位',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'phone' => '18310132160',
            'state' => 3,
        );
        $this->client->request('POST', '/newwintelapi/api/agent/agextphone', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '状态值不是0或1',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //409
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '26491',
            'phone' => '18310132160',
            'state' => 1,
        );
        $this->client->request('POST', '/newwintelapi/api/agent/agextphone', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 409,
            'message' => '对应的坐席不存在',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id' => '2649',
            'phone' => '18310132160',
            'state' => 1,
        );
        $this->client->request("POST", "/newwintelapi/api/agent/agextphone", $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试设置呼入呼出权限
     */
    public function testSetAgeCallStatus()
    {
        $this->client->request("POST", "/newwintelapi/api/agent/set/callstatus", array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id'=>'1',
            'call_status' => '2649',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/set/callstatus', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '呼叫状态的值不在允许的范围[0,1,2,3]内',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405
        $param = array(
            'vcc_code'=>'ekt',
            'call_status' => '1',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/set/callstatus', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '坐席ID为空或非数字',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $param = array(
            'vcc_code'=>'ekt',
            'ag_id'=>'2649',
            'call_status' => '1',
        );
        $this->client->request('POST', '/newwintelapi/api/agent/set/callstatus', $param);
        $response =  $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 公共测试方法
     */
    private function commonTest($type, $url)
    {
        $this->client->request($type, $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业ID包含非数字字符或企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
