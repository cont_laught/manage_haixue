<?php

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OtherControllerTest
 * @package Wintel\ApiBundle\Tests\Controller
 */
class SoundControllerTest extends WebTestCase
{

    private $client;

    public function __construct()
    {
        $this->client =  static::createClient();
    }

    /**
     * 测试删除
     */
    public function testDel()
    {
        $url = '/newwintelapi/api/sound/delete';
        $this->commonTest("GET", $url);

        //405
        $this->client->request("GET", $url, array('vcc_code'=>'ekt', 'sound_id'=>''));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'sound_id为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //500
        $this->client->request("GET", $url, array('vcc_code'=>'ekt', 'sound_id'=>'[1,23,"2x"]'));
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 获取列表
     */
    public function testList()
    {
        $url = "/newwintelapi/api/sound/list";
        $this->commonTest("POST", $url);

        //200 ok
        $this->client->request('POST', $url, array('vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');

        //200 ok page
        $this->client->request('POST', $url, array(
            'info'=>'{"pagination":{"rows":20,"page":"5"},"sort":{"field":"id","order":"desc"}}',
            'vcc_code'=>'ekt'
        ));
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 公共测试方法
     */
    private function commonTest($type, $url)
    {
        $this->client->request($type, $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
