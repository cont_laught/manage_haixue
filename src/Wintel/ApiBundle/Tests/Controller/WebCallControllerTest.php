<?php

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class QueueControllerTest
 * @package Wintel\ApiBundle\Tests\Controller
 */
class WebCallControllerTest extends WebTestCase
{
    private $client;

    public function __construct()
    {
        $this->client =  static::createClient();
    }

    /**
     * 测试获取400呼入记录
     */
    public function testWbeCall400()
    {
        $url = "/newwintelapi/api/webcall/webcall400";
        $this->commonTest("POST", $url);

        //403
        $param = array(
            'vcc_code' => 'ekt',
            'start_time' => '',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '开始时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        $param = array(
            'vcc_code' => 'ekt',
            'start_time' => '2013-05-26 11:00:00',
            'end_time' => 'xx',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_code' => 'ekt',
            'start_time' => '2013-05-26 11:00:00',
            'end_time' => '2015-05-26 11:00:00',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试获取800外呼记录
     */
    public function testWbeCall800()
    {
        $url = "/newwintelapi/api/webcall/webcall800";
        $this->commonTest("POST", $url);

        //403
        $param = array(
            'vcc_code' => 'ekt',
            'start_time' => '',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '开始时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        $param = array(
            'vcc_code' => 'ekt',
            'start_time' => '2013-05-26 11:00:00',
            'end_time' => 'xx',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '结束时间格式不正确'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_code' => 'ekt',
            'start_time' => '2013-05-26 11:00:00',
            'end_time' => '2015-05-26 11:00:00',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试web呼叫接口
     */
    public function testWebCall()
    {
        $url = "/newwintelapi/api/webcall/cti";
        $this->commonTest("POST", $url);

        //主叫号码为空
        $param = array(
            'vcc_code' => 'ekt',
            'caller' => '',
            'called' => '1212',
            'timeout' => '50',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '主叫号码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //被叫号码为空
        $param = array(
            'vcc_code' => 'ekt',
            'caller' => '18310132160',
            'called' => '',
            'timeout' => '50',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '被叫号码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_code' => 'ekt',
            'caller' => '18310132160',
            'called' => '18310132160',
            'timeout' => '50',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 公共测试方法
     */
    private function commonTest($type, $url)
    {
        $this->client->request($type, $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
