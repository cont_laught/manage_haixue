<?php

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AgentControllerTest
 * @package Wintel\ApiBundle\Tests\Controller
 */
class MonitorControllerTest extends WebTestCase
{
    private $client;

    public function __construct()
    {
        $this->client =  static::createClient();
    }

    /**
     * 测试获取坐席
     */
    public function testAgent()
    {
        $url = "/newwintelapi/api/monitor/agent";
        $this->commonTest("POST", $url);

        //坐席ID中含有非数字项
        $param = array(
            'vcc_code' => 'ekt',
            'user_ids' => '8001,8002,8003x',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '坐席ID中含有非数字项',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //技能组ID包含非数字字符
        $param = array(
            'vcc_code' => 'ekt',
            'user_ids' => '8001,8002,8003',
            'que_id' => '52x',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '技能组ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 ok
        $param = array(
            'vcc_code' => 'ekt',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试获取ivr;
     */
    public function testIvr()
    {
        $url = "/newwintelapi/api/monitor/ivr";
        $this->commonTest("GET", $url);
        //200 ok
        $this->client->request('GET', $url, array('vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试技能组监控
     */
    public function testQue()
    {
        $url = "/newwintelapi/api/monitor/queue";
        $this->commonTest("POST", $url);

        //技能组ID中含有非数字项
        $param = array(
            'vcc_code' => 'ekt',
            'que_ids' => '8001,8002,8003x',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组ID中含有非数字项',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 Ok
        $param = array(
            'vcc_code' => 'ekt',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试排队监控
     */
    public function testCalls()
    {
        $url = "/newwintelapi/api/monitor/calls";
        $this->commonTest("POST", $url);

        //技能组ID中含有非数字项
        $param = array(
            'vcc_code' => 'ekt',
            'que_id' => '1x',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '技能组ID包含非数字字符',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200 Ok
        $param = array(
            'vcc_code' => 'ekt',
            'que_id' => '1',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 系统监控
     */
    public function testSystem()
    {
        $url = "/newwintelapi/api/monitor/system";
        $this->commonTest("GET", $url);

        //200 ok
        $this->client->request('GET', $url, array('vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 获取置忙理由
     */
    public function testGetReason()
    {
        $url = "/newwintelapi/api/monitor/getreason";
        $this->commonTest("GET", $url);
        //200 ok
        $this->client->request('GET', $url, array('vcc_code'=>'ekt'));
        $response =  $this->client->getResponse()->getContent();
        //echo "xxxxxxxxxxxxxxxxx".$response.'YYYYYYYYY';
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }


    /**
     * 公共测试方法
     */
    private function commonTest($type, $url)
    {
        $this->client->request($type, $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
