<?php

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class RoleControllerTest
 * @package Wintel\ApiBundle\Tests\Controller
 */
class RoleControllerTest extends WebTestCase
{
    private $client;

    /**
     *
     */
    public function __construct()
    {
        $this->client =  static::createClient();
    }

    /**
     * 测试添加角色；
     */
    public function testAdd()
    {
        //402
        $this->commonTest();

        //404
        $this->client->request('GET', '/newwintelapi/api/role/add/name/1', array('vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '角色已经存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405
        $this->client->request('GET', '/newwintelapi/api/role/add/namex/xxx', array('vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '角色等级有误'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $this->client->request('GET', '/newwintelapi/api/role/add/testnewRole/1', array('vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试修改；
     */
    public function testEdit()
    {
        //402
        $this->commonTest();

        //403
        $this->client->request(
            'POST',
            '/newwintelapi/api/role/edit',
            array('vcc_code'=>'ekt','name'=>'<script>alert("x")</script>','role_grade'=>1)
        );
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '角色名称为空或非法'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403
        $this->client->request(
            'POST',
            '/newwintelapi/api/role/edit',
            array('vcc_code'=>'ekt','name'=>'','role_grade'=>1)
        );
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '角色名称为空或非法'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        $this->client->request(
            'POST',
            '/newwintelapi/api/role/edit',
            array('vcc_code'=>'ekt','name'=>'name','role_grade'=>1, 'role_id'=>103)
        );
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '角色已经存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //405
        $this->client->request(
            'POST',
            '/newwintelapi/api/role/edit',
            array('vcc_code'=>'ekt','name'=>'testnewRole','role_grade'=>'xxoo', 'role_id'=>'103')
        );
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '角色等级有误'
        );

        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //406
        $this->client->request(
            'POST',
            '/newwintelapi/api/role/edit',
            array('vcc_code'=>'ekt','name'=>'xxoo','role_grade'=>1, 'role_id'=>123456)
        );
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '角色不属于该企业'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //406
        $this->client->request(
            'POST',
            '/newwintelapi/api/role/edit',
            array('vcc_code'=>'ekt','name'=>'xxoo','role_grade'=>1)
        );
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '角色不属于该企业'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //200 ok
        $this->client->request(
            'POST',
            '/newwintelapi/api/role/edit',
            array('vcc_code'=>'ekt','name'=>'testnewRoleUpdate','role_grade'=>'2', 'role_id'=>'103')
        );
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试设置权限；
     */
    public function testSetPermission()
    {
        $this->commonTest();
        //403
        $this->client->request(
            'POST',
            '/newwintelapi/api/role/permissions',
            array('vcc_code'=>'ekt','action_list'=>'xxoo', 'role_id'=>123456)
        );
        $response = $this->client->getResponse()->getContent();
        $result = array(
           'code' => 403,
           'message' => '角色不属于该企业'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $this->client->request(
            'POST',
            '/newwintelapi/api/role/permissions',
            array('vcc_code'=>'ekt','action_list'=>'xxoo', 'role_id'=>'103')
        );
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 删除角色测试
     */
    public function testDel()
    {
        $this->commonTest();
        //403
        $this->client->request(
            'GET',
            '/newwintelapi/api/role/delete/123456',
            array('vcc_code'=>'ekt')
        );
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '角色不属于该企业'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405
        $this->client->request(
            'GET',
            '/newwintelapi/api/role/delete/101',
            array('vcc_code'=>'ekt')
        );
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '删除失败，该角色有坐席占用'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $this->client->request(
            'GET',
            '/newwintelapi/api/role/delete/103',
            array('vcc_code'=>'ekt')
        );
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');

    }

    /**
     * 公共测试方法
     */
    private function commonTest()
    {
        $this->client->request('GET', '/newwintelapi/api/role/add/name/111', array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
