<?php

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class BlacklistControllerTest
 * @package Wintel\RestBundle\Tests\Controller
 */
class NameListControllerTest extends WebTestCase
{

    private $client;

    public function __construct()
    {
        $this->client =  static::createClient();
    }


    /**
     * 测试添加黑名单
     */
    public function testBlacklistAdd()
    {
        $this->commonTest('POST', "/newwintelapi/api/blacklist/add");
        //号码为空
        $param = array(
            'vcc_code' => 'ekt',
            'phones' => '',
            'phone_type' => 1
        );
        $this->client->request('POST', '/newwintelapi/api/blacklist/add', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 411,
            'message' => '号码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //固定号码中包含非数字字符
        $param = array(
            'vcc_code' => 'ekt',
            'phones' => '8001,800d,8003',
            'phone_type' => 1
        );
        $this->client->request('POST', '/newwintelapi/api/blacklist/add', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '固定号码中包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码格式不对，要求为多个号码逗号分隔的字符串，但是得到
        $param = array(
            'vcc_code' => 'ekt',
            'phones' => '8**a,8dd2,80a3',
            'black_type' => 2
        );
        $this->client->request('POST', '/newwintelapi/api/blacklist/add', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '模糊匹配号码中包含除*外的非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //408 中继号码不存在
        $param = array(
            'vcc_code' => 'ekt',
            'phones' => '8001,8002,8003',
            'black_type' => 1,
            'trunk_num' =>'1234560',
        );
        $this->client->request('POST', '/newwintelapi/api/blacklist/add', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '中继号码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_code' => 'ekt',
            'phones' => '8001,8002,8003',
            'black_type' => 1
        );
        $this->client->request('POST', '/newwintelapi/api/blacklist/add', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_code' => 'ekt',
            'phones' => '8***,8004',
            'black_type' => 2
        );
        $this->client->request('POST', '/newwintelapi/api/blacklist/add', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试删除黑名单
     */
    public function testBlacklistDelete()
    {
        $this->client = static::createClient();
        $this->commonTest('POST', "/newwintelapi/api/blacklist/delete");

        //号码为空
        $param = array(
            'vcc_code' => 'ekt',
            'ids' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/blacklist/delete', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '号码ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码中包含非数字字符
        $param = array(
            'vcc_code' => 'ekt',
            'ids' => '80a1,800d,a003',
        );
        $this->client->request('POST', '/newwintelapi/api/blacklist/delete', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '号码ID中包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码ID格式不对，要求为多个号码ID逗号分隔的字符串，但是得到
//        $param = array(
//            'vcc_code' => 'ekt',
//            'ids' => array('8001,8002,8003'),
//        );
//        $this->client->request('POST', '/newwintelapi/api/blacklist/delete', $param);
//        $response = $this->client->getResponse()->getContent();
//        $result = array(
//            'code' => 406,
//            'message' => '号码ID格式不对，要求为多个号码ID逗号分隔的字符串，但是得到array'
//        );
//        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_code' => 'ekt',
            'ids' => '101,104,105',
        );
        $this->client->request('POST', '/newwintelapi/api/blacklist/delete', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试查询黑名单
     */
    public function testBlacklistListAction()
    {
        $this->commonTest("POST", '/newwintelapi/api/blacklist/list');

        //info格式非json
        $info = 'cxxa';
        $this->client->request('POST', '/newwintelapi/api/blacklist/list', array('info'=>$info, 'vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok  200
        $info = '{"vcc_code":"wintel","pagination":{"rows":5,"page":"1"},"filter":{"phone":"80"},"sort":{"field":"id","order":"desc"}}';
        $this->client->request('POST', '/newwintelapi/api/blacklist/list', array('info'=>$info, 'vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试添加白名单
     */
    public function testWhitelistAdd()
    {
        $this->commonTest("POST", "/newwintelapi/api/whitelist/add");

        //号码为空
        $param = array(
            'vcc_code' => 'ekt',
            'phones' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/whitelist/add', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '号码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407中继号为空
        $param = array(
            'vcc_code' => 'ekt',
            'trunk_num'=>'',
            'phones' => '8001,8000,8003',
        );
        $this->client->request('POST', '/newwintelapi/api/whitelist/add', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '中继号码为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));


        //号码中包含非数字字符
        $param = array(
            'vcc_code' => 'ekt',
            'trunk_num'=>'110',
            'phones' => '8001,800d,8003',
        );
        $this->client->request('POST', '/newwintelapi/api/whitelist/add', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 408,
            'message' => '号码中包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_code' => 'ekt',
            'trunk_num'=>'110',
            'phones' => '8001,8002,8003',
        );
        $this->client->request('POST', '/newwintelapi/api/whitelist/add', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试删除白名单
     */
    public function testWhitelistDelete()
    {

        $this->commonTest("POST", "/newwintelapi/api/whitelist/delete");
        //号码为空
        $param = array(
            'vcc_code' => 'ekt',
            'ids' => '',
        );
        $this->client->request('POST', '/newwintelapi/api/whitelist/delete', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '号码ID为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //号码中包含非数字字符
        $param = array(
            'vcc_code' => 'ekt',
            'ids' => '8001,800d,8003',
        );
        $this->client->request('POST', '/newwintelapi/api/whitelist/delete', $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '号码ID中包含非数字字符'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok
        $param = array(
            'vcc_code' => 'ekt',
            'ids' => '51,52,53',
        );
        $this->client->request('POST', '/newwintelapi/api/whitelist/delete', $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 200,
            'message' => 'ok'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 测试查询黑名单
     */
    public function testWhitelistList()
    {
        $this->commonTest("POST", '/newwintelapi/api/whitelist/list');

        //info格式非json
        $info = 'cxxa';
        $this->client->request('POST', '/newwintelapi/api/whitelist/list', array('info'=>$info, 'vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => 'info格式非json'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //ok  200
        $info = '{"vcc_code":"wintel","pagination":{"rows":5,"page":"1"},"filter":{"keyword":""},"sort":{"field":"id","order":"desc"}}';
        $this->client->request('POST', '/newwintelapi/api/whitelist/list', array('info'=>$info, 'vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 公共测试方法
     */
    private function commonTest($type, $url)
    {
        $this->client->request($type, $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
