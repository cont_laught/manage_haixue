<?php
/**
 * This file is part of easycrm.
 * Author: tangzhou
 * Date: 15-6-2
 * Time: 上午11:16
 * File: ReportControllerTest.php
 */

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ReportControllerTest
 * @package Wintel\ApiBundle\Tests\Controller
 */
class ReportControllerTest extends WebTestCase
{
    private $client;

    public function __construct()
    {
        $this->client =  static::createClient();
    }

    /**
     * 测试 坐席操作明细报表
     */
    public function testAgentState()
    {
        $url = "/newwintelapi/api/detail/agentstate";
        $this->commonTest("POST", $url);
        $this->commonTestDateTime($url);
        $this->commonOk($url);
    }

    /**
     * 测试 呼出明细报表
     */
    public function testCalloutAction()
    {
        $url = "/newwintelapi/api/detail/callout";
        $this->commonTest("POST", $url);
        $this->commonTestDateTime($url);
        $this->commonOk($url);
    }

    /**
     * 测试 呼入明细报表
     */
    public function testCallinAction()
    {
        $url = "/newwintelapi/api/detail/callin";
        $this->commonTest("POST", $url);
        $this->commonTestDateTime($url);
        $this->commonOk($url);
    }

    /**
     * 测试呼叫转接
     */
    public function testCallTranscall()
    {
        $url = "/newwintelapi/api/detail/transcall";
        $this->client->request("POST", $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //403
        $this->client->request("POST", $url, array('vcc_code'=>'ekt'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'call_id不能为空'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $param = array(
            'vcc_code'=>'ekt',
            'call_id'=>'1001926',
        );
        $this->client->request('POST', $url, $param);
        $response =  $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试 通话明细
     */
    public function testCallDetail()
    {
        $url = "/newwintelapi/api/detail/call";
        $this->commonTest("POST", $url);
        $this->commonTestDateTime($url);
        $this->commonOk($url);
    }

    /**
     * 测试咨询三方
     */
    public function testConference()
    {
        $url = "/newwintelapi/api/detail/conference";
        $this->commonTest("POST", $url);
        $this->commonTestDateTime($url);
        $this->commonOk($url);
    }

    /**
     * 测试监听强插
     */
    public function testMonitor()
    {
        $url = "/newwintelapi/api/detail/monitor";
        $this->commonTest("POST", $url);
        $this->commonTestDateTime($url);
        $this->commonOk($url);
    }

    /**
     * 漏话明细
     */
    public function testLost()
    {
        $url = "/newwintelapi/api/detail/lost";
        $this->commonTest("POST", $url);
        $this->commonTestDateTime($url);
        $this->commonOk($url);
    }

    /**
     * 测试 技能组转移明细
     */
    public function testQueuetranscall()
    {
        $url = "/newwintelapi/api/detail/queuetranscall";
        $this->commonTest("POST", $url);
        $this->commonTestDateTime($url);
        $this->commonOk($url);
    }

    /**
     * 测试 短信列表
     */
    public function testSms()
    {
        $url = "/newwintelapi/api/detail/sms";
        $this->commonTest("POST", $url);
        $this->commonOk($url);
    }

    /**
     * 满意度评价汇总数据
     */
    public function testEvaluatecollec()
    {
        $url = "/newwintelapi/api/data/evaluatecollec";
        $this->commonTest("POST", $url);
        $this->commonTestDateTime($url);
        $this->commonOk($url);
    }

    /**
     * 技能组来电分配
     */
    public function testInallot()
    {
        $url = "/newwintelapi/api/data/inallot";
        $this->commonTest("POST", $url);
        $this->commonTestDateTime($url);
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(月)
     */
    public function testAgentMonth()
    {
        $url = "/newwintelapi/api/data/agent/month";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'month');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(天)
     */
    public function testAgentDay()
    {
        $url = "/newwintelapi/api/data/agent/day";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'day');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(小时)
     */
    public function testAgentHour()
    {
        $url = "/newwintelapi/api/data/agent/hour";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'hour');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(半小时)
     */
    public function testAgentHalfHour()
    {
        $url = "/newwintelapi/api/data/agent/halfhour";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'halfhour');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(月)
     */
    public function testQueMonth()
    {
        $url = "/newwintelapi/api/data/queue/month";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'month');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(天)
     */
    public function testQueDay()
    {
        $url = "/newwintelapi/api/data/queue/day";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'day');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(小时)
     */
    public function testQueHour()
    {
        $url = "/newwintelapi/api/data/queue/hour";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'hour');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(半小时)
     */
    public function testAgentQueHour()
    {
        $url = "/newwintelapi/api/data/queue/halfhour";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'halfhour');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(月)
     */
    public function testSysMonth()
    {
        $url = "/newwintelapi/api/data/system/month";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'month');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(天)
     */
    public function testSysDay()
    {
        $url = "/newwintelapi/api/data/system/day";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'day');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(小时)
     */
    public function testSysHour()
    {
        $url = "/newwintelapi/api/data/system/hour";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'hour');
        $this->commonOk($url);
    }

    /**
     * 坐席工作表现报表(半小时)
     */
    public function testSysHalfHour()
    {
        $url = "/newwintelapi/api/data/system/halfhour";
        $this->commonTest("POST", $url);
        $this->commonDataDate($url, 'halfhour');
        $this->commonOk($url);
    }


    /**
     * 公共测试方法
     */
    private function commonTest($type, $url)
    {
        $this->client->request($type, $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $param = array(
            'vcc_code'=>'ekt',
            'info' => 'xx',
        );
        $this->client->request('POST', $url, $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 公共测试日期的方法
     */
    private function commonTestDateTime($url)
    {
        $param = array(
            'vcc_code'=>'ekt',
            'info' => json_encode(array(
                'filter'=>array(
                    'start_time'=>'2015-05-05'
                )
            )),
        );
        $this->client->request('POST', $url, $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始日期不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $param = array(
            'vcc_code'=>'ekt',
            'info' => json_encode(array(
                'filter'=>array(
                    'start_time'=>'2015-05-05 12:00:00',
                    'end_time'=>'xxxx'
                )
            )),
        );
        $this->client->request('POST', $url, $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束日期不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }

    /**
     * 公共200 测试
     * @param $url
     */
    private function commonOk($url)
    {
        $param = array(
            'vcc_code'=>'ekt',
            'info' => json_encode(array(
                'filter'=>array(
                    'start_time'=>'2014-04-05 12:00:00',
                    'end_time'=>'2015-04-05 12:00:00'
                )
            )),
        );
        $this->client->request('POST', $url, $param);
        $response =  $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 数据报表公共测试 日期
     */
    private function commonDataDate($url, $type)
    {
        switch ($type) {
            case 'month':
                $trueDate = '2015-05';
                $errorData = '2015-05-02';
                break;
            case 'day':
            case 'hour':
            case 'halfhour':
                $trueDate = '2015-05-05';
                $errorData = '2015-05';
                break;
        }
        $param = array(
            'vcc_code'=>'ekt',
            'info' => json_encode(array(
                'filter'=>array(
                    'start_date'=>$errorData
                )
            )),
        );
        $this->client->request('POST', $url, $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始日期不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $param = array(
            'vcc_code'=>'ekt',
            'info' => json_encode(array(
                'filter'=>array(
                    'start_date'=>$trueDate,
                    'end_date'=>$errorData
                )
            )),
        );
        $this->client->request('POST', $url, $param);
        $response =  $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束日期不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
