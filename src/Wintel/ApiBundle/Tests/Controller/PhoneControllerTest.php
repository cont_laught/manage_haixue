<?php

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class PhoneControllerTest
 * @package Wintel\ApiBundle\Tests\Controller
 */
class PhoneControllerTest extends WebTestCase
{

    private $client;

    public function __construct()
    {
        $this->client =  static::createClient();
    }

    /**
     * 测试添加分机
     */
    public function testAdd()
    {
        $url = "/newwintelapi/api/phone/add";
        $this->commonTest('POST', $url);

        //403
        $param = array(
            'vcc_code'=>'ekt',
            'pho_type'=>'3'
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => '电话类型值有误(1,5)',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        /*
        $param = array(
            'vcc_code'=>'ekt',
            'pho_type'=>'2',
            'pho_num'=>''
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '电话号码为空',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));*/

        //406
        $param = array(
            'vcc_code'=>'ekt',
            'pho_type'=>'5',
            'pho_start'=>'1xx'
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 406,
            'message' => '开始分机号格式有误',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //407
        $param = array(
            'vcc_code'=>'ekt',
            'pho_type'=>'5',
            'pho_start'=>'8001',
            'pho_end'=>'1xx',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 407,
            'message' => '结束分机号格式有误',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        $param = array(
            'vcc_code'=>'ekt',
            'pho_type'=>'5',
            'pho_start'=>'8001',
            'pho_end'=>'8002',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试获取技能组列表
     */
    public function testList()
    {
        $url = "/newwintelapi/api/phone/list";
        $this->commonTest('POST', $url);

        //403 json
        $param = array(
            'vcc_code' => 'wintel',
            'info' => 'xxx,afda',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $param = array(
            'vcc_code' => 'wintel',
            'info' => '{"pagination":{"rows":5,"page":"5"},"filter":{"phone":"80"}}',
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试删除
     */
    public function testDel()
    {
        $url = "/newwintelapi/api/phone/delete";
        $this->commonTest('GET', $url);

        $this->client->request('GET', $url, array('vcc_code'=>'ekt', 'ids'=>'[1,2,3]'));
        $response = $this->client->getResponse()->getContent();
        //echo $response;
        $this->assertEquals("500", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 公共测试方法
     */
    private function commonTest($type, $url)
    {
        $this->client->request($type, $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
