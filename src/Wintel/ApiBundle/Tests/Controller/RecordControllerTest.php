<?php

namespace Wintel\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class RecordControllerTest
 * @package Wintel\ApiBundle\Tests\Controller
 */
class RecordControllerTest extends WebTestCase
{
    private $client;

    public function __construct()
    {
        $this->client =  static::createClient();
    }

    /**
     * 测试获取录音列表
     */
    public function testGetRecord()
    {
        $url =  "/newwintelapi/api/record/recordlist";
        $this->commonTest("POST", $url);

        //技能组ID为空
        $param = array(
            'vcc_code'=>'ekt',
            'info'=>'xxx'
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        $param = array(
            'vcc_code'=>'ekt',
            'info'=>'{"pagination":{"rows":2,"page":"5"},"filter":{"start_time":"xx"}}'
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405
        $param = array(
            'vcc_code'=>'ekt',
            'info'=>'{"pagination":{"rows":2,"page":"5"},"filter":{"end_time":"xx"}}'
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $param = array(
            'vcc_code'=>'ekt',
            'info'=>'{"pagination":{"rows":2,"page":"5"}}'
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 测试语音列表
     */
    public function testVoice()
    {
        $url =  "/newwintelapi/api/voice/list";
        $this->commonTest("POST", $url);

        //技能组ID为空
        $param = array(
            'vcc_code'=>'ekt',
            'info'=>'xxx'
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 403,
            'message' => 'info格式非json',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //404
        $param = array(
            'vcc_code'=>'ekt',
            'info'=>'{"pagination":{"rows":2,"page":"5"},"filter":{"start_time":"xx"}}'
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 404,
            'message' => '开始时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //405
        $param = array(
            'vcc_code'=>'ekt',
            'info'=>'{"pagination":{"rows":2,"page":"5"},"filter":{"end_time":"xx"}}'
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 405,
            'message' => '结束时间格式不正确',
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));

        //200
        $param = array(
            'vcc_code'=>'ekt',
            'info'=>'{"pagination":{"rows":2,"page":"5"}}'
        );
        $this->client->request('POST', $url, $param);
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals("200", isset(json_decode($response)->code) ? json_decode($response)->code : '');
    }

    /**
     * 公共测试方法
     */
    private function commonTest($type, $url)
    {
        $this->client->request($type, $url, array('vcc_code'=>'xxoo'));
        $response = $this->client->getResponse()->getContent();
        $result = array(
            'code' => 402,
            'message' => '企业代码不存在'
        );
        $this->assertJsonStringEqualsJsonString($response, json_encode($result));
    }
}
