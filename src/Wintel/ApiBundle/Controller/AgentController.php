<?php

namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * 坐席相关接口
 * Class AgentController
 * @package Wintel\ApiBundle\Controller
 */
class AgentController extends BaseController
{
    /**
     * 坐席签入
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        $data = $request->get("data", '');
        $data = json_decode($data, true);
        if (json_last_error()) {
            return new JsonResponse(array('code'=>'417', 'message'=>'data格式非json'));
        }
        $vccCode = $this->getVccCode();
        $res = $this->get("icsoc_data.model.agent")->staticAgentLogin(array(
            'vcc_code' => $vccCode,
            'data'=>$data,
        ));
        return new JsonResponse($res);
    }

    /**
     * 坐席签出
     * @param Request $request
     * @return JsonResponse
     */
    public function logoutAction(Request $request)
    {
        $data = $request->get("data", '');
        $data = json_decode($data, true);
        if (json_last_error()) {
            return new JsonResponse(array('code'=>'417', 'message'=>'data格式非json'));
        }
        $vccCode = $this->getVccCode();
        $res = $this->get("icsoc_data.model.agent")->staticAgentLogout(array(
            'vcc_code' => $vccCode,
            'data'=>$data,
        ));
        return new JsonResponse($res);
    }

    /**
     * 删除坐席
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteAction(Request $request)
    {
        $agIds = $request->get("ag_ids", '');
        $data = json_decode($agIds, true);
        if (json_last_error()) {
            return new JsonResponse(array('code'=>'406', 'message'=>'ag_ids格式非json'));
        }
        $vccCode = $this->getVccCode();
        $res = $this->get("icsoc_data.model.agent")->deleteAgent(array(
            'vcc_code' => $vccCode,
            'ag_id'=>$data,
        ));
        return new JsonResponse($res);
    }

    /**
     * 编辑坐席
     * @param Request $request
     * @return JsonResponse
     */
    public function editAction(Request $request)
    {
        $params['ag_id'] = $request->get('ag_id', 0);
        $params['ag_name'] = $request->get('ag_name', 0);
        $params['ag_nickname'] = $request->get('ag_nickname', '');
        $params['ag_password'] = $request->get('ag_password', '');
        $params['ag_role'] = $request->get('ag_role', false); //坐席前台类型
        $params['user_role'] = $request->get('user_role', ''); //坐席角色
        $belongQue = $request->get('belong_queues', '');
        if (!empty($belongQue)) {
            $belongQue = json_decode($belongQue, true);
            if (json_last_error()) {
                return new JsonResponse(array('code'=>'416', 'message'=>'belong_queues非json格式'));
            }
            $params['belong_queues'] = $belongQue;
        }
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.agent")->editAgent($params);
        return new JsonResponse($res);
    }

    /**
     * 添加坐席
     * @param Request $request
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $params['que_id'] = $request->get('que_id', 0);
        $params['ag_num'] = $request->get('ag_num', '');
        $params['ag_name'] = $request->get('ag_name', '');
        $params['ag_password'] = $request->get('ag_password', '');
        $params['ag_nickname'] = $request->get('ag_nickname', '');
        $params['ag_role']= $request->get('ag_role', 0); //坐席前台类型
        $params['user_role'] = $request->get('user_role', ''); //坐席角色
        $belongQue = $request->get('belong_queues', '');
        $belongQue = json_decode($belongQue, true);
        if (json_last_error()) {
            return new JsonResponse(array('code'=>'416', 'message'=>'belong_queues非json格式'));
        }
        $params['belong_queues'] = $belongQue;
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.agent")->addAgent($params);
        return new JsonResponse($res);
    }

    /**
     * 获取坐席列表；
     * @param Request $request
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        $params['que_id'] = $request->get('que_id', 0);
        $params['ag_id'] = $request->get('ag_id', 0);
        $params['info'] = $request->get('info', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.agent")->listAgent($params);
        return new JsonResponse($res);
    }

    /**
     * 修改坐席密码
     * @param Request $request
     * @return JsonResponse
     */
    public function updatepassAction(Request $request)
    {
        $params['ag_id'] = $request->get('ag_id', 0);
        $params['vcc_code'] = $this->getVccCode();
        $params['old_password'] = $request->get('old_password', '');
        $params['new_password'] = $request->get('new_password', '');
        $res = $this->get("icsoc_data.model.agent")->updateAgentPassword($params);
        return new JsonResponse($res);
    }

    /**
     * 坐席登陆（不是静态坐席签入）
     * @param Request $request
     * @return JsonResponse
     */
    public function signinAction(Request $request)
    {
        $params['ag_num'] = $request->get('ag_num', 0);
        $params['vcc_code'] = $this->getVccCode();
        $params['password'] = $request->get('password', '');
        $res = $this->get("icsoc_data.model.agent")->siginAgent($params);
        return new JsonResponse($res);
    }

    /**
     * 获取空闲坐席；
     * @param Request $request
     * @return JsonResponse
     */
    public function getFreeAgentAction(Request $request)
    {
        $callback = $request->query->get('jsonpcallback', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.agent")->freeAgent($params);

        if (empty($callback)) {
            return new JsonResponse($res);
        } else {
            return new Response(sprintf('%s(%s)', $callback, json_encode($res)));
        }
    }

    /**
     * 获取当前通话；
     * @param Request $request
     * @return JsonResponse
     */
    public function getOnLineAction(Request $request)
    {
        $callback = $request->query->get('jsonpcallback', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.agent")->onthelineAgent($params);

        if (empty($callback)) {
            return new JsonResponse($res);
        } else {
            return new Response(sprintf('%s(%s)', $callback, json_encode($res)));
        }
    }

    /**
     * 设置坐席转接电话
     * @param Request $request
     * @return JsonResponse
     */
    public function setAgenetPhoneAction(Request $request)
    {
        $params['vcc_code'] = $this->getVccCode();
        $params['ag_id'] = $request->get('ag_id');
        $params['phone'] = $request->get('phone');
        $params['state'] = $request->get('state', 1);
        $res = $this->get("icsoc_data.model.agent")->agextphoneAgent($params);
        return new JsonResponse($res);
    }

    /**
     * 设置坐席电话呼入呼出权限
     * @param Request $request
     * @return JsonResponse
     */
    public function setAgentCallStatusAction(Request $request)
    {
        $params['vcc_code'] = $this->getVccCode();
        $params['ag_id'] = $request->get('ag_id');
        $params['call_status'] = $request->get('call_status');
        $res = $this->get("icsoc_data.model.agent")->callstatusAgent($params);
        return new JsonResponse($res);
    }
}
