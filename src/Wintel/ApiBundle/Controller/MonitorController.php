<?php

namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * 监控相关接口
 * Class MonitorController
 * @package Wintel\ApiBundle\Controller
 */
class MonitorController extends BaseController
{
    /**
     * 获取坐席监控数据
     * @param Request $request
     * @return JsonResponse
     */
    public function agentAction(Request $request)
    {
        $params['que_id'] = $request->get('que_id', 0);
        $params['user_ids'] = $request->get('user_ids', 0);
        $params['info'] = $request->get('info', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.monitor")->agentMonitor($params);
        return new JsonResponse($res);
    }

    /**
     * 获取ivr 监控数据
     * @return JsonResponse
     */
    public function ivrAction()
    {
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.monitor")->agentIvr($params);
        return new JsonResponse($res);
    }

    /**
     * 技能组监控
     * @param Request $request
     * @return JsonResponse
     */
    public function queueAction(Request $request)
    {
        $params['que_ids'] = $request->get("que_ids", '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.monitor")->queueMonitor($params);
        return new JsonResponse($res);
    }

    /**
     * 排队监控
     * @param Request $request
     * @return JsonResponse
     */
    public function callsAction(Request $request)
    {
        $params['que_id'] = $request->get("que_id", '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.monitor")->callsMonitor($params);
        return new JsonResponse($res);
    }

    /**
     * 系统监控
     * @return JsonResponse
     */
    public function systemAction()
    {
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.monitor")->systemMonitor($params);
        return new JsonResponse($res);
    }

    /**
     * 获取示忙理由接口
     * @return JsonResponse
     */
    public function getReasonAction()
    {
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.monitor")->getReason($params);
        return new JsonResponse($res);
    }
}
