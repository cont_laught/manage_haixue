<?php

namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * 基本controller
 * Class BaseController
 *
 * @package Wintel\ApiBundle\Controller
 */
class OtherController extends BaseController
{
    /**
     * 获取企业绑定的中继号码接口
     *
     * @return JsonResponse
     */
    public function relaynumberAction()
    {
        $res = $this->get("icsoc_data.model.other")->getPhone($this->getVccCode());

        return new JsonResponse($res);
    }

    /**
     * 号码归属地
     *
     * @param string $number
     *
     * @return JsonResponse
     */
    public function getNumberLocAction($number)
    {
        $res = $this->container->get("icsoc_data.model.location")->getNumberloc($number);

        return new JsonResponse($res);
    }

    /**
     * 获取置忙原因列表数据
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getBusyReasonsAction(Request $request)
    {
        // 是否是jsonp调用
        $callback = $this->get('request')->query->get('jsonpcallback', '');
        $vccCode = $request->get("vcc_code", 0);
        if (empty($callback)) {
            $vccCode = $this->getVccCode();
        }

        $callbak = $request->get('jsonpcallback', '');

        $msg = $vccId = $this->get('validator.custom')->checkVccCode($vccCode);
        if (!empty($msg) && is_array($msg)) {
            return new Response($callbak.'('.json_encode($msg).')');
        }

        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->get('doctrine.dbal.default_connection');
        $data = $conn->fetchAll(
            "SELECT id,vcc_id,ag_stat,stat_reason FROM win_agstat_reason WHERE vcc_id= ?",
            array($vccId)
        );

        $ret = array(
            'code' => 200,
            'message' => 'ok',
            'data' => $data,
        );

        if (empty($callback)) {
            return new JsonResponse($ret);
        } else {
            return new Response(sprintf('%s(%s)', $callback, json_encode($ret)));
        }
    }
}
