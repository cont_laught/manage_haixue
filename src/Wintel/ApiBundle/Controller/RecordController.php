<?php

namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class QueueController
 * @package Wintel\ApiBundle\Controller
 */
class RecordController extends BaseController
{
    /**
     * 获取录音列表；
     * @param Request $request
     * @return JsonResponse
     */
    public function getRecordAction(Request $request)
    {
        $params['info'] = $request->get('info', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get('wintel_api.report.model.callin')->getRecordListFromElasticsearch($params);

        return new JsonResponse($res);
    }

    /**
     * 播放留言或者语音
     * @param Request $request
     * @return Response
     */
    public function playRecordAction(Request $request)
    {
        $params['call_id'] = $request->get('call_id', '');
        $params['ag_id'] = $request->get('ag_id', '');
        $resultType = $request->get('result_type', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.record")->newPlayRecord($params);
        if (isset($res['code']) && $res['code'] == 200) {
            $recordFile = $res['file'];
            if ($resultType == 1) {
                return new JsonResponse(array('code' => 200, 'path' => $recordFile));
            } else {
                $content = @file_get_contents($recordFile);
                //$headers = get_headers($recordFile, true);
                //if (empty($recordFile) || !preg_match("/200/", $headers[0])) {
                if (empty($content)) {
                    $ret = array(
                        'code' => 405,
                        'message' => '录音文件不存在',
                    );
                    return new JsonResponse($ret);
                }
                $fileType = $this->get('help.function')->getSoundFileType($recordFile);
                $headerArr = array(
                    'Pragma' => 'Public',
                    'Expires' => 0,
                    'Cache-Component' => 'must-revalidate, post-check=0, pre-check=0',
                    'Content-type' => $fileType,
                    'Content-Length' => strlen($content),
                    'Content-Transfer-Encoding' => 'binary',
                    'Accept-Ranges' => 'bytes',
                );
                if ($resultType != 2) {
                    $headerArr['Content-Disposition'] = 'attachment;filename='.basename($recordFile);
                }

                return new Response($content, 200, $headerArr);
            }
        } else {
            return new JsonResponse($res);
        }
    }

    /**
     * 获取留言列表
     * @param Request $request
     * @return JsonResponse
     */
    public function getVoiceAction(Request $request)
    {
        $params['info'] = $request->get('info', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.record")->voiceList($params);

        return new JsonResponse($res);
    }

    /**
     * 播放留言
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function playVoiceAction(Request $request)
    {
        $params['call_id'] = $request->get('call_id', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.record")->playVoice($params);
        if (isset($res['code']) && $res['code'] == 200) {
            $recordFile = $res['file'];
            $fileType = $this->get('help.function')->getSoundFileType($recordFile);

            return new Response(file_get_contents($recordFile), 200, array(
                'Pragma' => 'Public',
                'Expires' => 0,
                'Cache-Component' => 'must-revalidate, post-check=0, pre-check=0',
                'Content-type' => $fileType,
                'Content-Length' => filesize($recordFile),
                'Content-Transfer-Encoding' => 'binary',
                'Accept-Ranges' => 'bytes',
                'Content-Disposition' => 'attachment;filename='.basename($recordFile),
            ));
        } else {
            return new JsonResponse($res);
        }
    }
}
