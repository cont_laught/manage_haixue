<?php
namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * 语音相关接口
 * Class SoundController
 * @package Wintel\ApiBundle\Controller
 */
class SoundController extends BaseController
{
    /**
     * 删除语音
     * @param Request $request
     * @return JsonResponse
     */
    public function delAction(Request $request)
    {
        $soundIds = $request->get("sound_id", '');
        $arr = json_decode($soundIds);
        if (json_last_error()) {
            $arr  = array();
        }
        $param['sound_id'] = $arr;
        $param['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.sound")->delSound($param);
        return new JsonResponse($res);
    }

    /**
     * 获取语音列表
     * @param Request $request
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        $param['info'] = $request->get("info", '');
        $param['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.sound")->soundList($param);
        return new JsonResponse($res);
    }

    /**
     * 上传语音
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadAction(Request $request)
    {
        $request->query->set('vcc_code', $this->getVccCode());
        $res = $this->get("icsoc_data.model.sound")->upload($request);
        return new JsonResponse($res);
    }

    /**
     * 播放语音
     * @param Request $request
     * @return BinaryFileResponse|JsonResponse
     */
    public function playAction(Request $request)
    {
        $soundId = $request->get('sound_id', '');
        $vccCode = $this->getVccCode();
        $info = $this->get('icsoc_data.model.sound')->getSoundInfo(array('vcc_code'=>$vccCode, 'sound_id'=>$soundId));
        if (isset($info['address']) && file_exists($info['address'])) {
            $response = new BinaryFileResponse($info['address']);
            $response->setContentDisposition('attachment', basename($info['address']));

            return $response;
        } elseif (isset($info['code'])) {
            return new JsonResponse($info);
        } else {
            return new JsonResponse(array('code'=>403,'message'=>'文件不存在'));
        }
    }
}
