<?php

namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * 角色相关接口
 * Class RoleController
 * @package Wintel\ApiBundle\Controller
 */
class RoleController extends BaseController
{
    /**
     * 添加角色接口
     * @param $name
     * @param $role_grade
     * @return JsonResponse
     */
    public function addAction($name, $role_grade)
    {
        $res = $this->get("icsoc_data.model.role")->addRole(
            array(
                'vcc_code' => $this->getVccCode(),
                'name' => $name,
                'role_grade' => $role_grade
            )
        );
        return new JsonResponse($res);
    }

    /**
     * 编辑角色
     * @param Request $request
     * @return JsonResponse
     */
    public function editAction(Request $request)
    {
        $name = $request->get('name', '');
        $role_grade = $request->get('role_grade', 3);
        $role_id = $request->get('role_id', '');
        $res = $this->get("icsoc_data.model.role")->editRole(
            array(
                'vcc_code' => $this->getVccCode(),
                'name' => $name,
                'role_grade' => $role_grade,
                'role_id' => $role_id,
            )
        );
        return new JsonResponse($res);
    }

    /**
     * 删除角色
     * @param $role_id
     * @return JsonResponse
     */
    public function deleteAction($role_id)
    {
        $res = $this->get("icsoc_data.model.role")->delRole(
            array(
                'vcc_code' => $this->getVccCode(),
                'role_id' => $role_id,
            )
        );
        return new JsonResponse($res);
    }

    /**
     * 获取角色列表
     * @param $role_id
     * @return JsonResponse
     */
    public function listAction($role_id)
    {
        $res = $this->get("icsoc_data.model.role")->getRoleList(
            array(
                'vcc_code' => $this->getVccCode(),
                'role_id' => $role_id,
            )
        );
        return new JsonResponse($res);
    }

    /**
     * 给角色设置权限值
     * @param Request $request
     * @return JsonResponse
     */
    public function permissionsAction(Request $request)
    {
        $roleId = $request->get('role_id', '');
        $actionList = $request->get('action_list', '');
        $res = $this->get("icsoc_data.model.role")->setActionList(
            array(
                'vcc_code' => $this->getVccCode(),
                'role_id' => $roleId,
                'action_list' => $actionList,
            )
        );
        return new JsonResponse($res);
    }
}
