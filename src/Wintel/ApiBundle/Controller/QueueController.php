<?php

namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class QueueController
 * @package Wintel\ApiBundle\Controller
 */
class QueueController extends BaseController
{
    /**
     * 添加技能组;
     * @param Request $request
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $params['que_name'] = $request->get('que_name', '');
        $params['que_num'] = $request->get('que_num', '');
        $params['que_tag'] = $request->get('que_tag', '');
        $params['que_type'] = $request->get('que_type', 0);
        $params['que_length'] = $request->get('que_length', 10);
        $params['que_time'] = $request->get('que_time', 120);
        $params['ring_time'] = $request->get('ring_time', 30);
        $params['next_wait'] = $request->get('next_wait', 5);
        $params['b_announce'] = $request->get('b_announce', 0);
        $params['noans_times'] = $request->get('noans_times', 3);
        $params['noans_wait'] = $request->get('noans_wait', 10);
        $params['wait_audio'] = $request->get('wait_audio', '');
        $params['noans_action'] = $request->get('noans_action', '1');
        $params['que_strategy'] = $request->get('que_strategy', 1);
        $params['que_priority'] = $request->get('que_priority', 1);
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.queue")->add($params);
        return new JsonResponse($res);
    }

    /**
     * 修改技能组
     * @param Request $request
     * @return JsonResponse
     */
    public function editAction(Request $request)
    {
        $params['que_name'] = $request->get('que_name', '');
        $params['que_num'] = $request->get('que_num', '');
        $params['que_tag'] = $request->get('que_tag', '');
        $params['que_type'] = $request->get('que_type', 0);
        $params['que_length'] = $request->get('que_length', 10);
        $params['que_time'] = $request->get('que_time', 120);
        $params['ring_time'] = $request->get('ring_time', 30);
        $params['next_wait'] = $request->get('next_wait', 5);
        $params['b_announce'] = $request->get('b_announce', 0);
        $params['noans_times'] = $request->get('noans_times', 3);
        $params['noans_wait'] = $request->get('noans_wait', 10);
        $params['wait_audio'] = $request->get('wait_audio', '');
        $params['noans_action'] = $request->get('noans_action', '1');
        $params['que_strategy'] = $request->get('que_strategy', 1);
        $params['que_priority'] = $request->get('que_priority', 1);
        $params['que_id'] = $request->get('que_id', 0);
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.queue")->update($params);
        return new JsonResponse($res);
    }

    /**
     * 删除技能组
     * @param Request $request
     * @return JsonResponse
     */
    public function delAction(Request $request)
    {
        $params['que_id'] = $request->get('que_id', '');
        $params['flag'] = $request->get('flag', 0);
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.queue")->delQue($params);
        return new JsonResponse($res);
    }

    /**
     * 获取技能组列表；
     * @param Request $request
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        $params['info'] = $request->get('info', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.queue")->getList($params);
        return new JsonResponse($res);
    }

    /**
     * 给技能组设置坐席
     * @param Request $request
     * @return JsonResponse
     */
    public function setQueAgentAction(Request $request)
    {
        $params['que_id'] = $request->get('que_id', '');
        $params['agents'] = $request->get('agents', 0);
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.queue")->assignQueAgent($params);
        return new JsonResponse($res);
    }

    /**
     * 取消已分配的坐席
     * @param Request $request
     * @return JsonResponse
     */
    public function cancelQueAgentAction(Request $request)
    {
        $params['que_id'] = $request->get('que_id', '');
        $params['agents'] = $request->get('agents', 0);
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.queue")->cancelAssign($params);
        return new JsonResponse($res);
    }
}
