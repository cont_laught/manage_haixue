<?php
namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PhoneController
 * @package Wintel\ApiBundle\Controller
 */
class PhoneController extends BaseController
{
    /**
     * 添加分机
     * @param Request $request
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $param['pho_type'] = $request->get("pho_type", '');
        $param['pho_start'] = $request->get("pho_start", '');
        $param['pho_end'] = $request->get("pho_end", '');
        $param['passtype'] = $request->get("passtype", '');
        $param['pho_pass'] = $request->get("pho_pass", '');
        $param['vcc_code'] = $this->getVccCode();
        $param['pho_num'] = explode(',', $request->get('pho_num'));
        $res = $this->get("icsoc_data.model.phone")->add($param);
        return new JsonResponse($res);
    }

    /**
     * 获取分机列表
     * @param Request $request
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        $param['info'] = $request->get("info", '');
        $param['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.phone")->getList($param);
        return new JsonResponse($res);
    }

    /**
     * 删除分机
     * @param Request $request
     * @return JsonResponse
     */
    public function delAction(Request $request)
    {
        $param['vcc_code'] = $this->getVccCode();
        $ids = $request->get("ids", '');
        $param['ids'] = explode(",", $ids);
        $res = $this->get("icsoc_data.model.phone")->delete($param);
        return new JsonResponse($res);
    }
}
