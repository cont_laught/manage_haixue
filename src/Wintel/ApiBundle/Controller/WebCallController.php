<?php

namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/***
 * webCall相关接口
 * Class WebCallController
 * @package Wintel\ApiBundle\Controller
 */
class WebCallController extends BaseController
{
    /**
     * web 400 呼入通话记录
     * @param Request $request
     * @return JsonResponse
     */
    public function webcall400Action(Request $request)
    {
        $params['end_time'] = $request->get('end_time', 0);
        $params['start_time'] = $request->get('start_time', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.webcall")->webCall400($params);
        return new JsonResponse($res);
    }

    /**
     * 800 回呼通话记录
     * @param Request $request
     * @return JsonResponse
     */
    public function webcall800Action(Request $request)
    {
        $params['end_time'] = $request->get('end_time', 0);
        $params['start_time'] = $request->get('start_time', '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.webcall")->webCall800($params);
        return new JsonResponse($res);
    }

    /**
     * web呼叫
     * @param Request $request
     * @return JsonResponse
     */
    public function webCallAction(Request $request)
    {
        $params['called'] = $request->get("called", 0);
        $params['caller'] = $request->get("caller", 0);
        $params['timeout'] = $request->get("timeout", 50);
        $params['display_caller'] = $request->get("display_caller", '');
        $params['display_called'] = $request->get("display_called", '');
        $params['if_transmit'] = $request->get("if_transmit", 0);
        $params['is_showcaller']= $request->get("is_showcaller", 0);
        $params['transfer_sound'] = $request->get("transfer_sound", '');
        $params['call_timeout_file'] = $request->get("call_timeout_file", '');
        $params['call_limit_time'] = $request->get("call_limit_time", '');
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.webcall")->ctiCallAction($params);
        return new JsonResponse($res);
    }

    /**
     * webcall直接转技能组
     * @param Request $request
     * @return JsonResponse
     */
    public function webQueCallAction(Request $request)
    {
        $params['called'] = $request->get("called", 0);
        $params['caller'] = $request->get("caller", 0);
        $params['timeout'] = $request->get("timeout", 50);
        $params['que_id'] = $request->get("que_id", 0);
        $params['vcc_code'] = $this->getVccCode();
        $res = $this->get("icsoc_data.model.webcall")->ctiCalltoQueAction($params);
        return new JsonResponse($res);
    }

    /**
     * 挂断接口
     * @param Request $request
     * @return JsonResponse
     */
    public function hangUpAction(Request $request)
    {
        $callId = $request->get('call_id', '');
        $vccCode = $this->getVccCode();
        $res = $this->get("icsoc_data.model.webcall")->hangUp($vccCode, $callId);

        return new JsonResponse($res);
    }
}
