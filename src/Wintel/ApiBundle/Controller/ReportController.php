<?php
/**
 * This file is part of easycrm.
 * Author: tangzhou
 * Date: 15-6-1
 * Time: 上午9:12
 * File: ReportController.php
 */

namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * 报表相关程序
 * Class ReportController
 * @package Wintel\ApiBundle\Controller
 */
class ReportController extends BaseController
{
    /**
     * 坐席操作明细报表
     * @param Request $request
     * @return JsonResponse
     */
    public function agentStateAction(Request $request)
    {
        return $this->common($request, "getAgentStateListData");
    }

    /**
     * 获取呼出明细报表
     * @param Request $request
     * @return JsonResponse
     */
    public function calloutAction(Request $request)
    {
        $params['vcc_code'] = $this->getVccCode();
        $params['info'] = $request->get("info", '');
        $res = $this->get('wintel_api.report.model.callin')->getCalloutListDataFromElasticsearch($params);
        return new JsonResponse($res);
        //return $this->common($request, "getCalloutListDataFromElasticsearch");
    }

    /**
     * 呼入明细报表
     * @param Request $request
     * @return JsonResponse
     */
    public function callinAction(Request $request)
    {
        $params['vcc_code'] = $this->getVccCode();
        $params['info'] = $request->get("info", '');
        $res = $this->get('wintel_api.report.model.callin')->getCallinListDataFromElasticsearch($params);
        return new JsonResponse($res);
        //return $this->common($request, "getCallinListDataFromElasticsearch");
    }

    /**
     * 呼叫转接接口(用于呼入明细报表下拉查看通话详细)
     * @param Request $request
     * @return JsonResponse
     */
    public function callTranscallAction(Request $request)
    {
        $params['vcc_code'] = $this->getVccCode();
        $params['call_id'] = $request->get("call_id", '');
        $res = $this->get("icsoc_data.model.report")->getTranCallListData($params);
        return new JsonResponse($res);
    }

    /**
     * 坐席通话明细报表
     * @param Request $request
     * @return JsonResponse
     */
    public function callDetailAction(Request $request)
    {
        $params['vcc_code'] = $this->getVccCode();
        $params['info'] = $request->get("info", '');
        $res = $this->get('wintel_api.report.model.callin')->getCallListDataFromElasticsearch($params);
        return new JsonResponse($res);
        //return $this->common($request, "getCallListDataFromElasticsearch");
    }

    /**
     * 咨询三方通话明细接口
     * @param Request $request
     * @return JsonResponse
     */
    public function conferenceAction(Request $request)
    {
        return $this->common($request, "getConferenceListData");
    }

    /**
     * 监听强插通话详单接口
     * @param Request $request
     * @return JsonResponse
     */
    public function monitorAction(Request $request)
    {
        return $this->common($request, "getMonitorListData");
    }

    /**
     * 漏话明细报表接口
     * @param Request $request
     * @return JsonResponse
     */
    public function lostAction(Request $request)
    {
        return $this->common($request, "getLostListData");
    }

    /**
     * 漏话明细报表接口 2.0
     * @param Request $request
     * @return JsonResponse
     */
    public function lost2Action(Request $request)
    {
        return $this->common($request, "getLostListDataFromElasticsearch");
    }

    /**
     * 技能组转移明细报表
     * @param Request $request
     * @return JsonResponse
     */
    public function queuetranscallAction(Request $request)
    {
        return $this->common($request, "getqueueTransCallListData");
    }

    /**
     * 短信记录
     * @param Request $request
     * @return JsonResponse
     */
    public function smsAction(Request $request)
    {
        return $this->common($request, "getSmsListData");
    }

    /**
     * 呼叫中心整体话务(月)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function systemMonthAction(Request $request)
    {
        return $this->common($request, "getSystemMonthData", "icsoc_report.model.system");
    }

    /**
     * 呼叫中心整体话务(日)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function systemDayAction(Request $request)
    {
        return $this->common($request, "getSystemDayData", "icsoc_report.model.system");
    }

    /**
     * 呼叫中心整体话务(小时)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function systemHourAction(Request $request)
    {
        return $this->common($request, "getSystemHourData", "icsoc_report.model.system");
    }

    /**
     * 呼叫中心整体话务(半小时)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function systemHalfHourAction(Request $request)
    {
        return $this->common($request, "getSystemHalfHourData", "icsoc_report.model.system");
    }

    /**
     * 技能组话务(月)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function queMonthAction(Request $request)
    {
        return $this->common($request, "getQueueMonthData", "icsoc_report.model.queue");
    }

    /**
     * 技能组话务(日)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function queDayAction(Request $request)
    {
        return $this->common($request, "getQueueDayData", "icsoc_report.model.queue");
    }

    /**
     * 技能组话务(小时)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function queHourAction(Request $request)
    {
        return $this->common($request, "getQueueHourData", "icsoc_report.model.queue");
    }

    /**
     * 技能组话务(半小时)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function queHalfHourAction(Request $request)
    {
        return $this->common($request, "getQueueHalfHourData", "icsoc_report.model.queue");
    }

    /**
     * 坐席工作表现(月)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function agentMonthAction(Request $request)
    {
        return $this->common($request, "getAgentMonthData", "icsoc_report.model.agent");
    }

    /**
     * 坐席工作表现(日)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function agentDayAction(Request $request)
    {
        return $this->common($request, "getAgentDayData", "icsoc_report.model.agent");
    }

    /**
     * 坐席工作表现(小时)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function agentHourAction(Request $request)
    {
        return $this->common($request, "getAgentHourData", "icsoc_report.model.agent");
    }

    /**
     * 坐席工作表现(半小时)报表
     * @param Request $request
     * @return JsonResponse
     */
    public function agentHalfHourAction(Request $request)
    {
        return $this->common($request, "getAgentHalfHourData", "icsoc_report.model.agent");
    }

    /**
     * 技能组来电分配报表
     * @param Request $request
     * @return JsonResponse
     */
    public function inallotAction(Request $request)
    {
        return $this->common($request, "getInallotListData", "icsoc_data.model.report");
    }

    /**
     * 满意度评价汇总
     * @param Request $request
     * @return JsonResponse
     */
    public function evaluatecollecAction(Request $request)
    {
        return $this->common($request, "getEvaluateCollectListData", "icsoc_data.model.report");
    }

    /**
     * 公共方法
     * @param Request $request
     * @param $functionNme
     * @param string $model
     * @return JsonResponse
     */
    private function common(Request $request, $functionNme, $model = 'icsoc_data.model.report')
    {
        $params['vcc_code'] = $this->getVccCode();
        $params['info'] = $request->get("info", '');
        $res = $this->get($model)->$functionNme($params);
        return new JsonResponse($res);
    }
}
