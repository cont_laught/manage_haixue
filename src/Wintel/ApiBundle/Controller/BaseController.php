<?php

namespace Wintel\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * 基本controller
 * Class BaseController
 * @package Wintel\ApiBundle\Controller
 */
class BaseController extends Controller
{
    /**
     * 获取VccCode
     * @return mixed
     */
    public function getVccCode()
    {
        $request = $this->get("request");
        $path = $request->getUri();
        $record = "/v2\/wintelapi\/record/";
        $common = "/v2\/wintelapi\/api/";
        if (preg_match($record, $path) || preg_match($common, $path)) {
            //匹配到了说明是不验证的
            $vccCode = $request->get("vcc_code", 0);
            $user = $this->get("doctrine.orm.entity_manager")
                ->getRepository("IcsocSecurityBundle:CcCcods")
                ->findOneBy(array('vccCode'=>$vccCode));
            if (!empty($user)) {
                $token = new UsernamePasswordToken($user, $user->getPassword(), "1123", $user->getRoles());
                $this->get("security.token_storage")->setToken($token);
            }
        } else {
            $user = $this->getUser();
            if (!$user instanceof UserInterface) {
                $vccCode = '';
            } else {
                $vccCode = $this->getUser()->getVccCode();
            }
        }
        return $vccCode;
    }
}
