<?php

namespace Wintel\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * 黑名单白名单相关接口
 * Class RoleController
 * @package Wintel\ApiBundle\Controller
 */
class NameListController extends BaseController
{

    /**
     * 添加黑名单
     * @param Request $request
     * @return JsonResponse
     */
    public function addBlackAction(Request $request)
    {
        $phones = $request->get('phones', '');
        $trunkNum = $request->get('trunk_num', '');
        $callType = $request->get('call_type', '');
        $black_type = $request->get('black_type', '');
        $phones = empty($phones) ? array() : explode(",", $phones);
        $res = $this->get("icsoc_data.model.black")->blacklistAdd(
            array(
                'vcc_code' => $this->getVccCode(),
                'phones' => $phones,
                'trunk_num' => $trunkNum,
                'call_type' => $callType,
                'black_type' => $black_type,
            )
        );
        return new JsonResponse($res);
    }

    /**
     * 删除黑名单；
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteBlackAction(Request $request)
    {
        $ids = $request->get("ids", '');
        $res = $this->get("icsoc_data.model.black")->delBlack(
            array(
                'vcc_code' => $this->getVccCode(),
                'ids' => $ids,
            )
        );
        return new JsonResponse($res);
    }

    /**
     * 黑名单列表
     * @param Request $request
     * @return JsonResponse
     */
    public function listBlackAction(Request $request)
    {
        $info = $request->get("info", '');
        $res = $this->get("icsoc_data.model.black")->getBlackList(
            array(
                'vcc_code' => $this->getVccCode(),
                'info' => $info,
            )
        );
        return new JsonResponse($res);
    }

    /**
     * 添加白明单
     * @param Request $request
     * @return JsonResponse
     */
    public function addWitheAction(Request $request)
    {
        $phones = $request->get('phones', '');
        $trunkNum = $request->get('trunk_num', '');
        $whiteSound = $request->get('white_sound', '');
        $phones = empty($phones) ? array() : explode(",", $phones);
        $res = $this->get("icsoc_data.model.white")->whiteAdd(
            array(
                'vcc_code' => $this->getVccCode(),
                'phones' => $phones,
                'trunk_num' => $trunkNum,
                'white_sound' => $whiteSound,
            )
        );
        return new JsonResponse($res);
    }

    /**
     * 删除白名单
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteWhiteAction(Request $request)
    {
        $ids = $request->get("ids", '');
        $res = $this->get("icsoc_data.model.white")->delWhite(
            array(
                'vcc_code' => $this->getVccCode(),
                'ids' => $ids,
            )
        );
        return new JsonResponse($res);
    }

    /**
     * 获取白名单列表；
     * @param Request $request
     * @return JsonResponse
     */
    public function listWhiteAction(Request $request)
    {
        $info = $request->get("info", '');
        $res = $this->get("icsoc_data.model.white")->getWhiteList(
            array(
                'vcc_code' => $this->getVccCode(),
                'info' => $info,
            )
        );
        return new JsonResponse($res);
    }
}
