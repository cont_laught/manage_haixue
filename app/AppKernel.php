<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Icsoc\UIBundle\IcsocUIBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Icsoc\CoreBundle\IcsocCoreBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle($this),
            new Icsoc\MenuBundle\IcsocMenuBundle(),
            new Icsoc\SecurityBundle\IcsocSecurityBundle(),
            new Icsoc\DataBundle\IcsocDataBundle(),
            new Icsoc\IvrBundle\IcsocIvrBundle(),
            new Icsoc\QueueBundle\IcsocQueueBundle(),
            new Icsoc\AgentBundle\IcsocAgentBundle(),
            new Icsoc\PhoneManageBundle\IcsocPhoneManageBundle(),
            new Icsoc\ReportBundle\IcsocReportBundle(),
            new Icsoc\SoundBundle\IcsocSoundBundle(),
            new Icsoc\NameListBundle\IcsocNameListBundle(),
            new Icsoc\NoticeBundle\IcsocNoticeBundle(),
            new Icsoc\LogBundle\IcsocLogBundle(),
            new Icsoc\CustomReportBundle\IcsocCustomReportBundle(),
            new Icsoc\GroupBundle\IcsocGroupBundle(),
            new Icsoc\RulesBundle\IcsocRulesBundle(),
            new Icsoc\MonitorBundle\IcsocMonitorBundle(),
//            new Sonata\CacheBundle\SonataCacheBundle(),
            new Icsoc\RecordingBundle\IcsocRecordingBundle(),
            new Cti\ApiBundle\CtiApiBundle(),
            new Wintel\RestBundle\WintelRestBundle(),
            new Icsoc\InstallBundle\IcsocInstallBundle(),
            new Icsoc\GuideBundle\IcsocGuideBundle(),
            new Wintel\ApiBundle\WintelApiBundle(),
            new Ivr\HttpBundle\IvrHttpBundle(),
            new \Oneup\FlysystemBundle\OneupFlysystemBundle(),
            new Icsoc\AgentStatBundle\AgentStatBundle(),
            new Icsoc\BusinessGroupBundle\BusinessGroupBundle(),
            new Gregwar\CaptchaBundle\GregwarCaptchaBundle(),
            new Icsoc\EvaluateBundle\IcsocEvaluateBundle(),
            new Icsoc\EsReportBundle\IcsocEsReportBundle(),
            new OldSound\RabbitMqBundle\OldSoundRabbitMqBundle(),
            new Snc\RedisBundle\SncRedisBundle(),
            new Icsoc\ReportForRoleBundle\IcsocReportForRoleBundle(),
            new Icsoc\PhoneGroupBundle\PhoneGroupBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    /**
     * @param LoaderInterface $loader
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }

    /**
     * @return string
     */
    public function getLogDir()
    {
        if (stristr(strtolower(php_uname('s')), 'windows')) {
            return $this->rootDir.'/logs';
        } else {
            return '/var/log/manage';
        }
    }
}
