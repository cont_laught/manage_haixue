manage3.0
=======

## 如何安装？

以下是在服务器能上网的情况下执行的安装，如果服务器不能上网，或者上网很慢，则可以使用这个 [安装](OFFLINE-INSTALL.MD)

### 0. 前期准备

#### php环境安装

参考[PHP+Apache安装](http://git.wintels.cn/louxin/docs/blob/master/%E5%AE%89%E8%A3%85%E6%96%87%E6%A1%A3/3-Web%E6%9C%8D%E5%8A%A1%E5%99%A8-PHP+Apache%E5%AE%89%E8%A3%85.md) 或 [PHP+Nginx安装](http://git.wintels.cn/louxin/docs/blob/master/%E5%AE%89%E8%A3%85%E6%96%87%E6%A1%A3/3-Web%E6%9C%8D%E5%8A%A1%E5%99%A8-PHP+Nginx%E5%AE%89%E8%A3%85.md)

#### NodeJS环境安装

    yum --enablerepo=remi,remi-php55 install nodejs
    
#### ElasticSearch安装

ElasticSearch用于报表中的自定义报表的查询和生成，此程序不必安装在业务服务器上，可以安装在其他服务器上，只需要安装在一台服务器上即可
不需要每台服务器都安装，安装完后，在easycrm的app/config/parameters.yml文件中配置的elasticsearch_hosts对应的地址和端口

    wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.4.4.noarch.rpm
    rpm -ivh elasticsearch-1.4.4.noarch.rpm
    service elasticsearch start
    chkconfig elasticsearch on
    
#### redis安装

redis主要用于监控中临时数据的存储

    yum --enablerepo=remi,remi-php55 install redis
    service redis start
    chkconfig redis on
    
#### 创建语音上传目录

此目录主要是使用nfs挂载远程的文件服务器上的语音目录

    mkdir /var/sounds
    chmod 775 /var/sounds
    
#### 创建远程录音目录

此目录为主要是使用nfs挂载远程录音服务器的录音目录

    mkdir /var/record
    
#### 配置php.ini

    vi /etc/php.ini
    #修改如下配置
    date.timezone = Asia/Chongqing
    memory_limit = 512M
    
#### web服务器配置

一下三种根据实际情况选择其中一个配置即可

##### Apache基本配置

默认安装下使用此配置即可

    vi /etc/httpd/conf/httpd.conf
    # 将所有的AllowOverride None修改为如下，存在多个配置，不要只修改完一个就算完了
    # 主要修改<Directory "/var/www/html">下的配置
    AllowOverride All

##### Apache虚拟主机配置

需要设置apache虚拟主机的使用此配置

    vi /etc/httpd/conf/httpd.conf
    <VirtualHost *:80>
        ServerName domain.tld#此项为对应的域名
        ServerAlias www.domain.tld
    
        DocumentRoot /var/www/html/easycrm/web
        <Directory /var/www/html/easycrm/web>
            # enable the .htaccess rewrites
            AllowOverride All
            Order allow,deny
            Allow from All
        </Directory>
    
        # uncomment the following lines if you install assets as symlinks
        # or run into problems when compiling LESS/Sass/CoffeScript assets
        # <Directory /var/www/project>
        #     Option FollowSymlinks
        # </Directory>
    
        ErrorLog /var/log/httpd/easycrm_error.log
        CustomLog /var/log/httpd/easycrm_access.log combined
    </VirtualHost>
    
    # 重启apache
    service httpd restart
    
##### Nginx配置

    # 将下面内容保存到/usr/local/nginx/conf/vhosts/easycrm.conf中
    server {
        server_name domain.tld www.domain.tld;
        root /usr/local/nginx/html/easycrm/web;
    
        location / {
            # try to serve file directly, fallback to app.php
            try_files $uri /app.php$is_args$args;
        }
        # DEV
        # This rule should only be placed on your development environment
        # In production, don't include this and don't deploy app_dev.php or config.php
        location ~ ^/(app_dev|config)\.php(/|$) {
            fastcgi_pass 127.0.0.1:9000;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param HTTPS off;
        }
        # PROD
        location ~ ^/app\.php(/|$) {
            fastcgi_pass 127.0.0.1:9000;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param HTTPS off;
            # Prevents URIs that include the front controller. This will 404:
            # http://domain.tld/app.php/some-path
            # Remove the internal directive to allow URIs like this
            internal;
        }
    
        error_log /usr/local/nginx/log/easycrm_error.log;
        access_log /usr/local/nginx/log/easycrm_access.log;
    }
    
    #重启nginx
    service nginx restart

### 1. 获取源代码

    cd /var/www/html 或者 cd /usr/local/nginx/html
    git clone http://git.wintels.cn/louxin/easycrm.git manage
    
### 2. 安装依赖类库

如下步骤主要视网络情况需要的时间不同，可能会比较长
安装过程中需要输入配置信息，请准确输入配置信息
若配置信息输入有误，则在此命令执行完后，
手动修改app/config/parameters.yml，
注意parameters.yml文件中不允许出现[tab]键，只允许空格
手动修改再重新执行此命令，直到安装完成

    cd manage
    php composer.phar install --prefer-dist
    
此步骤安装成功后显示如下信息

    修改目录权限成功
如果未提示此信息，可能是app/config/parameters.yml配置文件中的数据库配置项不正确，修改后重新执行此命令

    php composer.phar install
直到出现上述成功提示信息
    
### 3. 执行安装

    # 此步骤首先会检查当前的安装环境是否符合要求，如果存在不符合要求的项
    # 则需要手动处理对应的项
    php app/console icsoc:install --env=prod
    
安装成功

**注：现在任务计划会自动安装，具体的安装地址为/etc/cron.d/manage-report，无需再手动安装**

### 4. 报表重跑

此功能用于在发现统计报表出现问题时，可以执行如下命令进行重新整理
命令选项中的startDate和endDate分别为开始日期和结束日期，即需要重新整理的报表的开始日期和结束日期
两个日期相差不能超过7天
**需要注意的是，一定要在没有通话的情况，且先关闭上面的任务计划时再执行，否则会造成当天的数据不准确**
**最好是在下班的时候执行如下命令**
    
    cd /var/www/html/easycrm
    php app/console report:rerun --startDate=2015-01-01 --endData=2015-01-01
 
### 5. 配置注意事项
   
文件上传才用flysystem组件;主要选择适配器(现在配置了alioss_adapter,local_adapter两种,
默认是local_adapter,根据parameters.yml中的select_adapter),
默认满意度语音的上传;根据parameters.yml中的sound_local_upload_path选项配置值
在该目录下上传一个evabegin.wav的默认满意度语音文件(如/var/sounds/evabegin.wav),
使用alioss_adpater的时候也需要上传到(alioss上)对应目录

帮助文档解压到项目 web/uploads目录下

     
### 6. wintelapi

此版本中集成了wintelapi的功能，不需要再单独安装wintelapi，后续的wintelapi将不再维护，只在此版本中维护，
现在此版本中集成了2个版本的wintelapi，老版本的wintelapi的URL为/wintelapi/具体的接口地址，
新版的wintelapi地址为/v2/wintelapi，现在这两个版本将会并存，但是老版本的将不再维护，只为兼容原因的客户，
新接口的开发将都只在/v2/wintelapi中增加
    
安装完成后由于默认没有账号登录，需要安装ccod（如何安装ccod可以看版本库中的ccod中的安装文档）中开户

以下信息注意保密
***************************
默认的管理账号为:
wintel
wintel51971355
**************************