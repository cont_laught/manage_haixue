#[2015-02-15]
#xinjianhua 技能组话务报表、呼叫中心整体话务报表增加振铃次数字段
ALTER TABLE `rep_system_halfhour` ADD COLUMN `ring_num`  int(11) NOT NULL DEFAULT 0 COMMENT '振铃次数' AFTER `queue_secs`;
ALTER TABLE `rep_system_hour` ADD COLUMN `ring_num`  int(11) NOT NULL DEFAULT 0 COMMENT '振铃次数' AFTER `queue_secs`;
ALTER TABLE `rep_system_day` ADD COLUMN `ring_num`  int(11) NOT NULL DEFAULT 0 COMMENT '振铃次数' AFTER `queue_secs`;
ALTER TABLE `rep_system_month` ADD COLUMN `ring_num`  int(11) NOT NULL DEFAULT 0 COMMENT '振铃次数' AFTER `queue_secs`;

ALTER TABLE `rep_queue_halfhour` ADD COLUMN `ring_num`  int(11) NOT NULL DEFAULT 0 COMMENT '振铃次数' AFTER `queue_secs`;
ALTER TABLE `rep_queue_hour` ADD COLUMN `ring_num`  int(11) NOT NULL DEFAULT 0 COMMENT '振铃次数' AFTER `queue_secs`;
ALTER TABLE `rep_queue_day` ADD COLUMN `ring_num`  int(11) NOT NULL DEFAULT 0 COMMENT '振铃次数' AFTER `queue_secs`;
ALTER TABLE `rep_queue_month` ADD COLUMN `ring_num`  int(11) NOT NULL DEFAULT 0 COMMENT '振铃次数' AFTER `queue_secs`;

#[2015-02-16]
#报表表中添加索引
ALTER TABLE `rep_agent_day` ADD INDEX(`nowdate`);
ALTER TABLE `rep_agent_halfhour` ADD INDEX(`start_date`);
ALTER TABLE `rep_agent_hour` ADD INDEX(`start_date`);

ALTER TABLE `rep_queue_day` ADD INDEX(`nowdate`);
ALTER TABLE `rep_queue_halfhour` ADD INDEX(`start_date`);
ALTER TABLE `rep_queue_hour` ADD INDEX(`start_date`);

ALTER TABLE `rep_system_day` ADD INDEX(`nowdate`);
ALTER TABLE `rep_system_halfhour` ADD INDEX(`start_date`);
ALTER TABLE `rep_system_hour` ADD INDEX(`start_date`);

#[2015-02-26]
#新增ivr和语音更新记录表
CREATE TABLE IF NOT EXISTS `cc_cti_change_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '企业id',
  `log_type` enum('ivr','sound') NOT NULL COMMENT '日志类型',
  `log_action` enum('create','update','delete') NOT NULL COMMENT '操作',
  `table_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '对应的表的记录ID',
  PRIMARY KEY (`id`),
  KEY `vcc_id` (`vcc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='中间件ivr、语音更新日志' AUTO_INCREMENT=1 ;

#[2015-03-17]
#新增技能组号
ALTER TABLE `win_queue`
ADD COLUMN `que_num`  varchar(30) NOT NULL COMMENT '技能组号' AFTER `que_name`;

#[2015-03-25]
#新增黑名单类型,
ALTER TABLE `cc_blacklist`
CHANGE COLUMN `call_type` `call_type`  tinyint(1) NOT NULL DEFAULT 1 COMMENT '1呼入,2呼出' AFTER `group_id`;

#[2015-03-26]
#新增时长临时表中的具体示忙原因字段,
ALTER TABLE `rep_tmp_time`
ADD COLUMN `ext_option`  varchar(50) NOT NULL COMMENT '额外属性';

#[2015-04-01]
#挂机短信待发表result字段修改
ALTER TABLE `est_hangupsms`
MODIFY COLUMN `result`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '短信结果' AFTER `state`;

#[2015-04-03]
#增加字段help_url
ALTER TABLE `menus`
ADD COLUMN `help_url`  varchar(50) NULL COMMENT '帮助文档' AFTER `sort_weight`;

#[2015-04-06]
#增加字段is_show
ALTER TABLE `menus`
ADD COLUMN `is_show`  tinyint(1) NULL DEFAULT 1 COMMENT '是否展示在左侧菜单中' AFTER `help_url`;

#[2015-04-08]
#增加字段que_priority
ALTER TABLE `win_queue`
ADD COLUMN `que_priority`  tinyint(1) UNSIGNED NULL COMMENT '技能组优先级(数字越小优先级越高)' AFTER `que_num`;

#[2015-04-09]
#增加唯一索引避免重复
ALTER TABLE `win_agextphone`
ADD UNIQUE INDEX `ag_id_unique` (`ag_id`) ;

#[2015-04-17]
#增加字段user_login_state
ALTER TABLE `win_agent`
ADD COLUMN `user_login_state`  tinyint(1) NULL DEFAULT 1 COMMENT '坐席登录状态  1空闲 2忙碌' AFTER `ag_num`;

#[2015-04-17]
#增加字段ag_caller_type
ALTER TABLE `win_agent`
ADD COLUMN `ag_caller_type`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '外呼主叫号码类型 0全部  1指定' AFTER `ag_caller`;

#[2015-04-20]
#增加字段btn_str
ALTER TABLE `win_agent`
ADD COLUMN `btn_str`  varchar(200) NULL COMMENT 'toolbar中控制按钮显示' AFTER `sync_agent`;

#添加技能组优先级功能
DELIMITER //
DROP FUNCTION `getagqu`; CREATE DEFINER=`root`@`%` FUNCTION `getagqu`(`in_agid` INT) RETURNS VARCHAR(256) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER BEGIN declare retstr varchar(256); declare m_queid int; declare m_skill int; declare no_more int; declare cur1 CURSOR FOR select que_id,skill from win_agqu AS aq LEFT JOIN win_queue AS q on aq.que_id=q.id where aq.ag_id=in_agid ORDER BY q.que_priority asc; declare continue handler for not found set no_more=1; set no_more = 0; set retstr = ''; OPEN cur1; REPEAT FETCH cur1 INTO m_queid,m_skill; if 0=no_more then set retstr = concat(retstr,m_queid,'-',m_skill,','); end if; UNTIL no_more END REPEAT; CLOSE cur1; RETURN retstr; END
//
DELIMITER ;

#[2015-04-24]
#添加语音类型等字段
ALTER TABLE `win_sounds`
ADD  `superior_agent` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级代理商',
ADD  `verify_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '审核状态（0未审核1审核通过2审核未通过）',
ADD  `verify_time` int(11) NOT NULL COMMENT '审核时间',
ADD  `group_id` int(10) unsigned NOT NULL COMMENT '区域ID',
ADD  `sound_type` tinyint(1) NOT NULL DEFAULT '6' COMMENT '1: 技能语音,2: 满意度语音,3: 白名单语音,4: 按键语音,5: 留言语音,6: 其他';

ALTER TABLE win_ivr add `ivr_ver` int(11) NOT NULL DEFAULT '0' COMMENT '是否新版本IVR(0否1是)';

#[2015-12-15]
#新增IVR节点访问记录数报表
CREATE TABLE `win_ivr_sta` (
	`id` INT (11) NOT NULL AUTO_INCREMENT,
	`vcc_id` INT (11) NOT NULL DEFAULT '0' COMMENT '企业ID',
	`ivr_id` INT (11) NOT NULL DEFAULT '0' COMMENT 'IVR编号',
	`deal_date` VARCHAR (50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日期',
 `ivr_sta` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '统计详情',
 PRIMARY KEY (`id`)
) ENGINE = INNODB CHARACTER
SET utf8 COLLATE utf8_general_ci COMMENT = 'IVR节点访问记录数报表';

#IVR节点访问记录数报表插入唯一键
ALTER TABLE `win_ivr_sta` ADD UNIQUE( `vcc_id`, `ivr_id`, `deal_date`);

#插入轨迹菜单
INSERT INTO `menus` (`menu_text`, `menu_url`, `has_children`, `parent_id`, `menu_icon`, `sort_weight`, `help_url`, `is_show`) VALUES
('IVR轨迹统计报表', 'icsoc_ivr_sta', 0, 62, '', 4, NULL, 1);

#[2015-12-14]
#添加主被叫归属地信息
ALTER TABLE win_incdr
ADD `caller_type` ENUM('','TEL','MOBILE') NOT NULL COMMENT '主叫号码类型（TEL-座机,MOBILE-手机）' AFTER caller_areaname,
ADD `caller_vendor` varchar(10) NOT NULL COMMENT '主叫所属运营商（座机该字段为空）' AFTER caller_type,
ADD `called_areacode` varchar(4) NOT NULL COMMENT '被叫区号' AFTER caller_vendor,
ADD `called_areaname` varchar(20) NOT NULL COMMENT '被叫地区名' AFTER called_areacode,
ADD `called_type` ENUM('TEL','MOBILE') NOT NULL COMMENT '被叫号码类型（TEL-座机,MOBILE-手机）' AFTER called_areaname,
ADD `called_vendor` varchar(10) NOT NULL COMMENT '被叫所属运营商（座机该字段为空）' AFTER called_type;

ALTER TABLE win_agcdr
ADD `ag_phone_areacode` varchar(4) NOT NULL COMMENT '坐席号码区号' AFTER cus_phone,
ADD `ag_phone_areaname` varchar(20) NOT NULL COMMENT '坐席号码地区名' AFTER ag_phone_areacode,
ADD `ag_phone_type` ENUM('','TEL','MOBILE') NOT NULL COMMENT '坐席号码类型（TEL-座机,MOBILE-手机）' AFTER ag_phone_areaname,
ADD `ag_phone_vendor` varchar(10) NOT NULL COMMENT '坐席号码所属运营商（座机该字段为空）' AFTER ag_phone_type,
ADD `cus_phone_areacode` varchar(4) NOT NULL COMMENT '客户号码区号' AFTER ag_phone_vendor,
ADD `cus_phone_areaname` varchar(20) NOT NULL COMMENT '客户号码地区名' AFTER cus_phone_areacode,
ADD `cus_phone_type` ENUM('','TEL','MOBILE') NOT NULL COMMENT '客户号码类型（TEL-座机,MOBILE-手机）' AFTER cus_phone_areaname,
ADD `cus_phone_vendor` varchar(10) NOT NULL COMMENT '客户号码所属运营商（座机该字段为空）' AFTER cus_phone_type;

ALTER TABLE win_lost_cdr
ADD `caller_areacode` varchar(4) NOT NULL COMMENT '主叫区号' AFTER caller,
ADD `caller_areaname` varchar(20) NOT NULL COMMENT '主叫地区名' AFTER caller_areacode,
ADD `caller_type` ENUM('','TEL','MOBILE') NOT NULL COMMENT '主叫号码类型（TEL-座机,MOBILE-手机）' AFTER caller_areaname,
ADD `caller_vendor` varchar(10) NOT NULL COMMENT '主叫所属运营商（座机该字段为空）' AFTER caller_type;

ALTER TABLE win_queue_cdr
ADD `ag_phone_areacode` varchar(4) NOT NULL COMMENT '坐席号码区号' AFTER ag_phone,
ADD `ag_phone_areaname` varchar(20) NOT NULL COMMENT '坐席号码地区名' AFTER ag_phone_areacode,
ADD `ag_phone_type` ENUM('','TEL','MOBILE') NOT NULL COMMENT '坐席号码类型（TEL-座机,MOBILE-手机）' AFTER ag_phone_areaname,
ADD `ag_phone_vendor` varchar(10) NOT NULL COMMENT '坐席号码所属运营商（座机该字段为空）' AFTER ag_phone_type,
ADD `caller_areacode` varchar(4) NOT NULL COMMENT '主叫区号' AFTER caller_num,
ADD `caller_areaname` varchar(20) NOT NULL COMMENT '主叫地区名' AFTER caller_areacode,
ADD `caller_type` ENUM('','TEL','MOBILE') NOT NULL COMMENT '主叫号码类型（TEL-座机,MOBILE-手机）' AFTER caller_areaname,
ADD `caller_vendor` varchar(10) NOT NULL COMMENT '主叫所属运营商（座机该字段为空）' AFTER caller_type;

#[2015-12-17]
#技能组话务报表增加具体放弃原因
ALTER TABLE `rep_queue_halfhour`
ADD COLUMN `lost1_num`  int(11) UNSIGNED NOT NULL COMMENT '主叫放弃的数量,ext_option=1' AFTER `deal_secs`,
ADD COLUMN `lost1_secs`  int(11) UNSIGNED NOT NULL COMMENT '主叫放弃的时长,ext_option=1' AFTER `lost1_num`,
ADD COLUMN `lost3_num`  int(11) UNSIGNED NOT NULL COMMENT '排队超时的数量,ext_option=3' AFTER `lost1_secs`,
ADD COLUMN `lost3_secs`  int(11) UNSIGNED NOT NULL COMMENT '排队超时的时长,ext_option=3' AFTER `lost3_num`,
ADD COLUMN `lost4_num`  int(11) UNSIGNED NOT NULL COMMENT '技能组满溢出的数量,ext_option=4' AFTER `lost3_secs`,
ADD COLUMN `lost4_secs`  int(11) UNSIGNED NOT NULL COMMENT '技能组满溢出的时长,ext_option=4' AFTER `lost4_num`,
ADD COLUMN `lost5_num`  int(11) UNSIGNED NOT NULL COMMENT '无坐席放弃的数量,ext_option=5' AFTER `lost4_secs`,
ADD COLUMN `lost5_secs`  int(11) UNSIGNED NOT NULL COMMENT '无坐席放弃的时长,ext_option=5' AFTER `lost5_num`;
ALTER TABLE `rep_queue_hour`
ADD COLUMN `lost1_num`  int(11) UNSIGNED NOT NULL COMMENT '主叫放弃的数量,ext_option=1' AFTER `deal_secs`,
ADD COLUMN `lost1_secs`  int(11) UNSIGNED NOT NULL COMMENT '主叫放弃的时长,ext_option=1' AFTER `lost1_num`,
ADD COLUMN `lost3_num`  int(11) UNSIGNED NOT NULL COMMENT '排队超时的数量,ext_option=3' AFTER `lost1_secs`,
ADD COLUMN `lost3_secs`  int(11) UNSIGNED NOT NULL COMMENT '排队超时的时长,ext_option=3' AFTER `lost3_num`,
ADD COLUMN `lost4_num`  int(11) UNSIGNED NOT NULL COMMENT '技能组满溢出的数量,ext_option=4' AFTER `lost3_secs`,
ADD COLUMN `lost4_secs`  int(11) UNSIGNED NOT NULL COMMENT '技能组满溢出的时长,ext_option=4' AFTER `lost4_num`,
ADD COLUMN `lost5_num`  int(11) UNSIGNED NOT NULL COMMENT '无坐席放弃的数量,ext_option=5' AFTER `lost4_secs`,
ADD COLUMN `lost5_secs`  int(11) UNSIGNED NOT NULL COMMENT '无坐席放弃的时长,ext_option=5' AFTER `lost5_num`;
ALTER TABLE `rep_queue_day`
ADD COLUMN `lost1_num`  int(11) UNSIGNED NOT NULL COMMENT '主叫放弃的数量,ext_option=1' AFTER `deal_secs`,
ADD COLUMN `lost1_secs`  int(11) UNSIGNED NOT NULL COMMENT '主叫放弃的时长,ext_option=1' AFTER `lost1_num`,
ADD COLUMN `lost3_num`  int(11) UNSIGNED NOT NULL COMMENT '排队超时的数量,ext_option=3' AFTER `lost1_secs`,
ADD COLUMN `lost3_secs`  int(11) UNSIGNED NOT NULL COMMENT '排队超时的时长,ext_option=3' AFTER `lost3_num`,
ADD COLUMN `lost4_num`  int(11) UNSIGNED NOT NULL COMMENT '技能组满溢出的数量,ext_option=4' AFTER `lost3_secs`,
ADD COLUMN `lost4_secs`  int(11) UNSIGNED NOT NULL COMMENT '技能组满溢出的时长,ext_option=4' AFTER `lost4_num`,
ADD COLUMN `lost5_num`  int(11) UNSIGNED NOT NULL COMMENT '无坐席放弃的数量,ext_option=5' AFTER `lost4_secs`,
ADD COLUMN `lost5_secs`  int(11) UNSIGNED NOT NULL COMMENT '无坐席放弃的时长,ext_option=5' AFTER `lost5_num`;
ALTER TABLE `rep_queue_month`
ADD COLUMN `lost1_num`  int(11) UNSIGNED NOT NULL COMMENT '主叫放弃的数量,ext_option=1' AFTER `deal_secs`,
ADD COLUMN `lost1_secs`  int(11) UNSIGNED NOT NULL COMMENT '主叫放弃的时长,ext_option=1' AFTER `lost1_num`,
ADD COLUMN `lost3_num`  int(11) UNSIGNED NOT NULL COMMENT '排队超时的数量,ext_option=3' AFTER `lost1_secs`,
ADD COLUMN `lost3_secs`  int(11) UNSIGNED NOT NULL COMMENT '排队超时的时长,ext_option=3' AFTER `lost3_num`,
ADD COLUMN `lost4_num`  int(11) UNSIGNED NOT NULL COMMENT '技能组满溢出的数量,ext_option=4' AFTER `lost3_secs`,
ADD COLUMN `lost4_secs`  int(11) UNSIGNED NOT NULL COMMENT '技能组满溢出的时长,ext_option=4' AFTER `lost4_num`,
ADD COLUMN `lost5_num`  int(11) UNSIGNED NOT NULL COMMENT '无坐席放弃的数量,ext_option=5' AFTER `lost4_secs`,
ADD COLUMN `lost5_secs`  int(11) UNSIGNED NOT NULL COMMENT '无坐席放弃的时长,ext_option=5' AFTER `lost5_num`;

#菜单表增加数据
INSERT INTO `menus` (`id`, `menu_text`, `menu_url`, `has_children`, `parent_id`, `menu_icon`, `sort_weight`, `help_url`, `is_show`) VALUES
(97, '置忙原因设置', 'icsoc_agentstat_index', '0', '37', '', '0', null, '1'),
(98, '业务组管理', 'icsoc_business_group_index', '0', '1', '', '0', null, '1'),
(99, '业务组工作表现报表', 'icsoc_report_group_home', '0', '62', '', '3', null, '1');

#[2015-12-18]
#增加IVR轨迹明细记录表
CREATE TABLE `win_ivr_path` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vcc_id` int(10) unsigned NOT NULL COMMENT '企业ID',
  `call_id` int(10) unsigned NOT NULL COMMENT '呼叫ID',
  `call_time` int(10) unsigned NOT NULL COMMENT '呼叫时间',
  `ivr_code` varchar(20) NOT NULL COMMENT 'ivr编号',
  `ivr_path` text NOT NULL COMMENT 'ivr流转轨迹',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`call_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

#[2015-12-24]
#增加技能组通话明细菜单
INSERT INTO `menus` VALUES ('101', '技能组通话明细', 'icsoc_queue_call_details', '0', '61', '', '0', '', '1');
INSERT INTO `menus` (`menu_text`, `menu_url`, `has_children`, `parent_id`, `menu_icon`, `sort_weight`, `help_url`, `is_show`) VALUES
('满意度评价管理', 'icsoc_evaluate_list', '0', '1', '', '0', null, '1');
INSERT INTO `menus` (`menu_text`, `menu_url`, `has_children`, `parent_id`, `menu_icon`, `sort_weight`, `help_url`, `is_show`) VALUES
('满意度评价明细报表', 'icsoc_evaluate_detail', '0', '61', '', '0', null, '1');

#[2015-12-31]
#大屏监控配置表
CREATE TABLE `cc_monitor_screen` (
	`id` INT (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`vcc_id` INT (11) NOT NULL COMMENT '企业Id',
	`screen_name` VARCHAR (120) CHARACTER
SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '大屏监控名称',
 `screen_info` TEXT CHARACTER
SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '大屏监控配置信息',
 `role_id` INT (11) NOT NULL COMMENT '角色ID',
 `create_time` INT (11) NOT NULL COMMENT '创建时间',
 `refresh_time` INT (11) NOT NULL COMMENT '配置刷新时间（单位：秒）'
) ENGINE = INNODB CHARACTER
SET utf8 COLLATE utf8_general_ci COMMENT = '大屏监控配置表';

INSERT INTO `ccod`.`menus` (`id`, `menu_text`, `menu_url`, `has_children`, `parent_id`, `menu_icon`, `sort_weight`, `help_url`, `is_show`) VALUES (NULL, '大屏监控', 'icsoc_monitor_screen', '0', '81', '', '0', NULL, '1');

#[2016-01-13]
#角色增加报表配置数据权限字段
ALTER TABLE `cc_roles`
ADD COLUMN `report_config`  mediumtext NULL COMMENT '报表配置信息' AFTER `group_id`;

ALTER TABLE `cc_roles`
ADD COLUMN `user_type`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '数据权限:0,不限;1技能组;2坐席;3业务组' AFTER `report_config`,
ADD COLUMN `user_queues`  varchar(150) NOT NULL COMMENT '管理的技能组或者业务组' AFTER `user_type`;

#[2016-03-14]
#自定报表配置增加角色
ALTER TABLE `report`
ADD COLUMN `role_id`  int(11) NOT NULL COMMENT '角色ID' AFTER `report_type`;

#[2016-06-15]
#增加telid配置
ALTER TABLE `cc_ccods`
ADD COLUMN `telid`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'default' AFTER `if_total_record`;

#[2016-07-18]
#增加group_belong配置
ALTER TABLE `win_queue` ADD `group_belong` VARCHAR(300) COLLATE utf8_general_ci NOT NULL COMMENT '所属业务组' AFTER `sync_que`;

#[2016-07-20]
#增加坐席昵称字段
ALTER TABLE `win_agent` ADD `ag_nickname` VARCHAR(32) COLLATE utf8_general_ci NOT NULL COMMENT '坐席昵称' AFTER `ag_name`;

CREATE TABLE `cc_switchboard` (
`id`  int NOT NULL AUTO_INCREMENT ,
`phone400`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '400号码' ,
`servnum`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '中继号' ,
`type`  tinyint NOT NULL COMMENT '总分机(1表示独立号，2总机)' ,
PRIMARY KEY (`id`)
)
;
ALTER TABLE `cc_switchboard`
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;
ALTER TABLE `cc_switchboard`
ADD COLUMN `vcc_id`  int NULL COMMENT '企业id' AFTER `type`;

#[2016-07-25]
#修改字段类型
ALTER TABLE `rep_queue_day` CHANGE `conn5_num` `conn5_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn10_num` `conn10_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn20_num` `conn20_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn30_num` `conn30_num` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `rep_queue_halfhour` CHANGE `conn5_num` `conn5_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn10_num` `conn10_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn20_num` `conn20_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn30_num` `conn30_num` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `rep_queue_hour` CHANGE `conn5_num` `conn5_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn10_num` `conn10_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn20_num` `conn20_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn30_num` `conn30_num` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `rep_queue_month` CHANGE `conn5_num` `conn5_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn10_num` `conn10_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn20_num` `conn20_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn30_num` `conn30_num` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `rep_system_day` CHANGE `conn5_num` `conn5_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn10_num` `conn10_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn20_num` `conn20_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn30_num` `conn30_num` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `rep_system_halfhour` CHANGE `conn5_num` `conn5_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn10_num` `conn10_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn20_num` `conn20_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn30_num` `conn30_num` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `rep_system_hour` CHANGE `conn5_num` `conn5_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn10_num` `conn10_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn20_num` `conn20_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn30_num` `conn30_num` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `rep_system_month` CHANGE `conn5_num` `conn5_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn10_num` `conn10_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn20_num` `conn20_num` INT(11) NOT NULL DEFAULT '0', CHANGE `conn30_num` `conn30_num` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `rep_group_day` CHANGE `in_num` `in_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼入量', CHANGE `out_num` `out_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼出量', CHANGE `internal_num` `internal_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '内线量', CHANGE `ring_num` `ring_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '振铃次数', CHANGE `consult_num` `consult_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '咨询量', CHANGE `hold_num` `hold_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '保持量', CHANGE `conference_num` `conference_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会议量', CHANGE `shift_num` `shift_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '转接量', CHANGE `wait_num` `wait_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '事后次数';
ALTER TABLE `rep_group_halfhour` CHANGE `in_num` `in_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼入量', CHANGE `out_num` `out_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼出量', CHANGE `internal_num` `internal_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '内线量', CHANGE `ring_num` `ring_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '振铃次数', CHANGE `consult_num` `consult_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '咨询量', CHANGE `hold_num` `hold_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '保持量', CHANGE `conference_num` `conference_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会议量', CHANGE `shift_num` `shift_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '转接量', CHANGE `wait_num` `wait_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '事后次数';
ALTER TABLE `rep_group_hour` CHANGE `in_num` `in_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼入量', CHANGE `out_num` `out_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼出量', CHANGE `internal_num` `internal_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '内线量', CHANGE `ring_num` `ring_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '振铃次数', CHANGE `consult_num` `consult_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '咨询量', CHANGE `hold_num` `hold_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '保持量', CHANGE `conference_num` `conference_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会议量', CHANGE `shift_num` `shift_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '转接量', CHANGE `wait_num` `wait_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '事后次数';
ALTER TABLE `rep_group_month` CHANGE `in_num` `in_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼入量', CHANGE `out_num` `out_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼出量', CHANGE `internal_num` `internal_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '内线量', CHANGE `ring_num` `ring_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '振铃次数', CHANGE `consult_num` `consult_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '咨询量', CHANGE `hold_num` `hold_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '保持量', CHANGE `conference_num` `conference_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会议量', CHANGE `shift_num` `shift_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '转接量', CHANGE `wait_num` `wait_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '事后次数';

ALTER TABLE `rep_agent_day` CHANGE `in_num` `in_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼入量';
ALTER TABLE `rep_agent_halfhour` CHANGE `in_num` `in_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼入量';
ALTER TABLE `rep_agent_hour` CHANGE `in_num` `in_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼入量';
ALTER TABLE `rep_agent_month` CHANGE `in_num` `in_num` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼入量';

#[2016-07-26]
#添加企业呼入线路数限制
ALTER TABLE `cc_ccods` ADD `callin_lines` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '呼入线路数限制' AFTER `telid`;

#添加cc_vip_phone表
CREATE TABLE `cc_vip_phone` (`id`  int(11) NOT NULL AUTO_INCREMENT ,`vcc_id`  int(11) NOT NULL ,`phone`  varchar(25) NOT NULL ,PRIMARY KEY (`id`));

#[2016-08-31]
#添加cc_global_report_config(报表全局配置)表
CREATE TABLE `cc_global_report_config` (`id`  int(11) NOT NULL AUTO_INCREMENT ,`vcc_id`  int(11) NOT NULL ,`report_config`  mediumtext NOT NULL ,PRIMARY KEY (`id`));

#[2016-09-18]
#添加号码组(win_phone_group)和cc_phone400s关联表
CREATE TABLE `win_phoid_gid` (
`id`  int NULL AUTO_INCREMENT ,
`group_id`  int NOT NULL COMMENT '号码组id' ,
`phone_id`  int NOT NULL COMMENT '号码id' ,
`vcc_id`  int NOT NULL COMMENT '企业id' ,
PRIMARY KEY (`id`)
);

#[2016-09-27]
#号码组增加remark字段
ALTER TABLE `win_phone_group` ADD `remark` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注' AFTER `vcc_id`;

#[2016-09-27]
#增加开发者管理菜单
INSERT INTO menus(menu_text,menu_url,has_children,parent_id,menu_icon,sort_weight,help_url,is_show)
VALUES('开发者功能', 'icsoc_security_developers_index', 0, 37, '',0, '',1),
('号码组管理', 'icsoc_phone_group_index', '0', '1', '', '0', '', '1');


#[2016-09-18]
#是否开通开发者
ALTER TABLE `cc_ccods`
ADD COLUMN `open_api`  tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否开通api 1开通2不开通' AFTER `callin_lines`;

#[2016-10-11]
#添加转接企业技能组配置表
CREATE TABLE win_trans_vcc_queue
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    vcc_id INT(11) DEFAULT '0' NOT NULL COMMENT '企业ID',
    vcc_code VARCHAR(50) NOT NULL COMMENT '企业代码',
    trans_vcc_id INT(11) DEFAULT '0' NOT NULL COMMENT '转接的企业ID',
    trans_vcc_code VARCHAR(50) NOT NULL COMMENT '转接的企业代码'
);
CREATE INDEX win_trans_vcc_queue_vcc_code_index ON win_trans_vcc_queue (vcc_code);
CREATE INDEX win_trans_vcc_queue_vcc_id_index ON win_trans_vcc_queue (vcc_id);

#[2016-10-09]
#推送日志
CREATE TABLE `win_push_record` (
`id`  int NOT NULL AUTO_INCREMENT ,
`vcc_id`  int NOT NULL ,
`send_start_time`  int NOT NULL COMMENT '推送开始时间' ,
`send_end_time`  int NOT NULL COMMENT '推送结束时间' ,
`call_type`  tinyint NOT NULL COMMENT '呼叫类型(1呼出2呼入)' ,
`send_res`  tinyint NOT NULL COMMENT '推送结果(1成功2失败)' ,
`cus_phone`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户号码' ,
`call_start_time`  int NOT NULL COMMENT '呼叫开始时间' ,
`call_end_time`  int NOT NULL COMMENT '呼叫结束时间' ,
`send_params`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送参数json格式' ,
PRIMARY KEY (`id`)
)
;

#[2016-10-09]
#开通挂机推送
ALTER TABLE `cc_ccods`
ADD COLUMN `hangup_push`  tinyint(1) NOT NULL DEFAULT 2 COMMENT '是否开启挂机推送(1开启,2不开启)' AFTER `open_api`,
ADD COLUMN `push_url`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '挂机推送地址' AFTER `hangup_push`;


#[2016-10-10]
#下载进度日志
CREATE TABLE `win_download_log` (
`id`  int NOT NULL AUTO_INCREMENT ,
`token_id`  bigint NOT NULL COMMENT '下载tokenid 查询进度用' ,
`progress`  tinyint NOT NULL COMMENT '完成进度' ,
`download_url`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '下载url' ,
`submit_time`  int NOT NULL COMMENT '提交时间' ,
`submit_user`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '提交人 id-类型(12-1坐席，企业，ccadmin)' ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `cc_vip_phone`
ADD COLUMN `pho_type`  tinyint NOT NULL DEFAULT 1 COMMENT '号码类型(如1是司机端，2乘客端)' AFTER `phone`;

ALTER TABLE `win_download_log`
MODIFY COLUMN `submit_user`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '提交人 id-类型(12-1坐席，企业，ccadmin)' AFTER `submit_time`,
ADD COLUMN `name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '下载名称' AFTER `submit_user`;

#把以前开发者功能变为一个大菜单;
UPDATE menus SET menu_url = '',has_children=1,menu_text='开发者模式' WHERE id =105;

INSERT INTO menus(menu_text,menu_url,has_children,parent_id,menu_icon,sort_weight,help_url,is_show)
VALUES('API开发接口配置', 'icsoc_security_developers_index', 0, 105, '',0, '',1),
('通话结果推送配置', 'icsoc_security_develop_call_push_index', '0', 105, '', '0', '', '1');

# 增加ekt版本配置
# 2016-10-31
ALTER TABLE cc_ccods ADD system_version TINYINT(4) DEFAULT 1 NOT NULL COMMENT 'ekt系统版本1老版本2新版本';

INSERT INTO menus(menu_text,menu_url,has_children,parent_id,menu_icon,sort_weight,help_url,is_show)
VALUES('号码组统计报表', 'icsoc_report_phone_group', 0, 62, '',5, '',1);

#[2016-11-02]
ALTER TABLE `win_download_log`
ADD COLUMN `datacount`  int(11) NOT NULL COMMENT '总数据量' AFTER `download_url`,
ADD COLUMN `file_size`  varchar(15) NOT NULL COMMENT '文件大小' AFTER `datacount`,
ADD COLUMN `use_time`  int(11) NOT NULL COMMENT '生成下载文件用时' AFTER `file_size`;

#[2016-11-28]
#修改字段用于判断是否是公有云客户
ALTER TABLE `cc_ccods` CHANGE `is_card` `is_card` TINYINT(1) NOT NULL COMMENT '是否已开通私有云服务（1是0否），已开通企业不能登录公有云平台';

#[2016-12-07]
#下载任务列表区分新老接口
ALTER TABLE `win_download_log` ADD COLUMN `export_version`  tinyint NOT NULL DEFAULT 1 COMMENT '1代表老接口2新接口' AFTER `name`;

#[2016-01-10]
#满意度配置修改字段
UPDATE menus set menu_text='满意度设置',parent_id=37 where menu_text='满意度评价管理'
ALTER TABLE `cc_evaluates` ADD COLUMN `evaluate_config`  mediumtext NOT NULL COMMENT '满意度配置' AFTER `valid_keys`;
ALTER TABLE `cc_evaluates` ADD COLUMN `update_time`  INT (11) NOT NULL COMMENT '最后一次修改配置的时间', AFTER `evaluate_config`;

#[2016-12-28]
#添加接起推送
ALTER TABLE `cc_ccods`
ADD COLUMN `conn_push`  tinyint(1) NOT NULL DEFAULT 2 COMMENT '接通推送1开通，2不开通' AFTER `system_version`,
ADD COLUMN `conn_push_url`  varchar(100) NOT NULL DEFAULT '' COMMENT '接通推送地址' AFTER `conn_push`;

#[2017-02-07]
#添加新的满意度配置
CREATE TABLE `cc_evaluates_config` (
`id`  int NOT NULL AUTO_INCREMENT ,
`vcc_id`  int NOT NULL ,
`evaluate_config`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置json' ,
`update_time`  int NOT NULL COMMENT '更新时间' ,
PRIMARY KEY (`id`)
);

ALTER TABLE `cc_ccods`
ADD COLUMN `evaluate_ver`  tinyint(1) NOT NULL DEFAULT 1 COMMENT '满意度新老 1走老的,2是新的' AFTER `conn_push_url`;

#[2017-02-13]
#号码组

ALTER TABLE `rep_phone_group_day`
ADD COLUMN `total_num`  int(11) NOT NULL DEFAULT 0 COMMENT '总来电量' AFTER `server_num`;

ALTER TABLE `rep_phone_group_month`
ADD COLUMN `total_num`  int(11) NOT NULL DEFAULT 0 COMMENT '总来电量' AFTER `server_num`;

#[2017-02-23]
#技能组非工作时间设置
ALTER TABLE `win_queue`
ADD COLUMN `que_time_config`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '技能组非工作时间配置' AFTER `group_belong`;


#[2017-05-25]
#增加菜单 坐席签到明细报表
INSERT INTO menus SET menu_text='坐席签到明细报表',menu_url='icsoc_report_agent_check_in_detail',parent_id=61,sort_weight=10

#[2017-05-23]
#黑名单增加一些记录信息
ALTER TABLE `cc_blacklist`
ADD COLUMN `handle_time`  int NOT NULL COMMENT '操作时间' AFTER `call_type`,
ADD COLUMN `handle_name`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作人' AFTER `handle_time`,
ADD COLUMN `remark`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注' AFTER `handle_name`;

#[2017-08-22]
#增加挂机短信相关配置
ALTER TABLE `swj_hangup_sms`
ADD COLUMN `vcc_signature`  varchar(100) NOT NULL AFTER `busy_sms`;

 INSERT INTO `menus` (`menu_text`, `menu_url`, `parent_id`, `sort_weight`) VALUES ('挂机短信', 'icsoc_security_develop_hang_up_sms', '37', '46')

#[2017-10-10]
#增加监控报警配置

CREATE TABLE `cc_monitor_alarm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vcc_id` int(11) NOT NULL COMMENT '企业id',
  `que_num` varchar(255) NOT NULL DEFAULT '0' COMMENT '排队量',
  `ring_num` varchar(255) NOT NULL DEFAULT '0' COMMENT '振铃量',
  `call_num` varchar(255) NOT NULL DEFAULT '0' COMMENT '通话量',
  `ready_num` varchar(255) NOT NULL DEFAULT '0' COMMENT '就绪量',
  `arrange_num` varchar(255) NOT NULL DEFAULT '0' COMMENT '整理量',
  `busy_num` varchar(255) NOT NULL DEFAULT '0' COMMENT '置忙量',
  `alarm_type` tinyint(4) NOT NULL COMMENT '报警方式 (1短信，2 邮箱)',
  `alarm_value` text NOT NULL COMMENT '具体值(手机号 || 邮箱)',
  `enable_alarm` varchar(300) NOT NULL COMMENT '开通那些选项["que_num","ring_num"]',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `menus` (`menu_text`, `menu_url`, `parent_id`, `sort_weight`) VALUES ('告警配置', 'icsoc_monitor_alarm_index', '81', '2')

#[2017-10-16]
#增加监控报警字段

ALTER TABLE `cc_monitor_alarm`
ADD COLUMN `alarm_name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报警名称' AFTER `vcc_id`,
ADD COLUMN `que_config`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置的技能组' AFTER `enable_alarm`;

ALTER TABLE `cc_monitor_alarm`
ADD COLUMN `alarm_time`  tinyint NOT NULL DEFAULT 10 COMMENT '告警频率' AFTER `alarm_name`;

#[2017-10-30]
#增加坐席状态字段;
ALTER TABLE `win_agent`
ADD COLUMN `ag_status`  tinyint(1) NOT NULL DEFAULT 1 COMMENT '员工状态1,在职，2离职，3冻结' AFTER `fail_times`
